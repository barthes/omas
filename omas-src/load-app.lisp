;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;24/10/15
;;;                L O A D - A P P  (file load-app.lisp)
;;;
;;;===============================================================================
;;; This file contains the functions for loading all the files of an application

;;; (c) Jean-Paul Barthès@HEUDIASYC, CNRS, UTC. See LICENSE file for permissions.

#| 
History
-------
2024
 1015 adapting to Windows 10
2020
 0206 built from pieces of omas-load
|# 

(in-package :omas)

;;;===============================================================================

;;; ------------------------------------------------------------ COMPILE-IF-NEEDED
;;; we want to compile a file, when we have no compiled file or when the compiled
;;; file is older than the source file.

(defun compile-if-needed (source compiled &optional external-format)
  "Compiles source if not compiled, if changed.
Arguments:
   source: file pathname of source code
   compiled: file pathname of compiled result
   external-format (optional): format of source file, e.g. :utf-8
Result:
   unimportant"
  (let ()
    (when (or (not (probe-file compiled))
              (< (file-write-date compiled)
                 (file-write-date source)))
      (apply #'compile-file source :output-file compiled :verbose t
             (and external-format (list :external-format external-format))))))

;;;-------------------------------------------------------------- LOAD-APPLICATION
;;; the function is used to load the application directly and also to load the
;;; application from the init window. In the first case, *omas* must be initialized
;;; prior to call this function (done in load-application-or-open-init-window).

(defun load-application (file-name)
  "central function to load a full application, once the global parameters have ~
   been extracted from the parameters file and stored into *omas*.
Arguments:
  file-name : pathname to the agents file
Return:
   T if load was successful, NIL otherwise"
  
  (let ((site-name (site-name *omas*))
        (appli/coterie-name (coterie-name *omas*))
        )
    
    ;; build a local user name to go with the control panel, e.g. :<ODIN-USER>
    (setf (local-user *omas*)
          (intern (string-upcase (format nil "<~A-USER>" site-name)) 
                  :keyword))
    
    ;;============================================================================
    ;;                  Get list of agents in the application
    ;;============================================================================
       
    ;(format *debug-io*  "~&=== oi-load-on-click; agents.lisp: ~S" file-name)    
    ;; try to load agents file recompiling t if needed    

    (unless (compile-and/or-load file-name :external-format :utf-8)    
      (warn "load-application: Application ~S not present.~2%
*** To change the application if the green init window is not present, exit from ~
the system, press the SHIFT key and hold it while the system is restarted. ~
This will trigger the green init menu for selecting a different application.***"
        file-name)    
      (%beep)
      (return-from load-application nil))
    
    ;; compute the application directory pathname from agents file-name
    ;; and save it for later user by make-agent JPB0912
    (setf (omas-application-directory *omas*) 
          (make-pathname :directory (pathname-directory file-name)))
    
    ;; tell user that everything is OK, print local agent names    
    ;(format *debug-io* "~&;load- /local-coterie-agents: ~S" 
    ;  omas::*local-coterie-agents*)   
    
    ;;============================================================================
    ;;                               load agents
    ;;============================================================================
    
    ;; load each agent files    
    (dolist (agent *local-coterie-agents*)       
      ;; cook up agent file-pathname    
      (setq file-name (string+ (omas-application-directory *omas*) agent ".lisp"))
      ;(format t "~&; load-application /loading agent: ~S" agent)
      (load-agent :agent-file-pathname file-name)
      )
    
    ;;============================================================================
    ;;                            load message file
    ;;============================================================================
    ;; load message file if any (don't complain if none)    
    (setq file-name 
          (string+ (omas-application-directory *omas*) "Z-messages" ".lisp"))   

    (compile-and/or-load file-name :external-format :utf-8)
    
    ;;============================================================================
    ;;                       load manager file (not available)
    ;;============================================================================
    ;; load manager file (use load-manager from the load file)
    ;(load-manager)
    
    ;;============================================================================
    ;;                            create control panel
    ;;============================================================================
    (make-omas-control-panel)
    
    ;;============================================================================
    ;;                               net control
    ;;============================================================================
    
    ;; keep messages local when broadcast net address is blank    
    (setf (net-broadcast *omas*)
          (if (string-equal (local-broadcast-address *omas*) "") nil t))
    
    ;; if using the net, then initialize
    (when (net-broadcast *omas*)    
      ;; This start a receiving process *omas-net-receive*    
      ;; and a dispatching process *omas-net-dispatch* that delivers the messages    
      ;; to the local agents    
      (omas-net-initialize))
    
    ;; here the policy would be that we enter a single event processing loop...    
    ;(%window-close *init-window*)
    
    (when (equal appli/coterie-name "NULL")
      (setq *package* (find-package :cg-user)))
    
    ;;==============================================================================
    ;;      if graphics-window is T open gaphics window
    ;;==============================================================================
    
    (if (graphics-window *omas*)
        (let ((spy-key (key (symbol-value (spy-name *omas*)))))
          ;; if agents were not selected select them all
          (unless (names-of-agents-to-display *omas*)
            (setf (names-of-agents-to-display *omas*) 
              (mapcar #'car (local-agents *omas*))))
          (%send-message (make-instance 'message :type :sys-inform :to spy-key
                           :action :enable)
                         t) ; indicates local message
          ))

    ;;==============================================================================
    ;;                 check init function (from agents.lisp)
    ;;==============================================================================
    (if (fboundp 'init) (funcall 'init))
    
    :done)
  t)


;;;(defun load-application (&optional (folder-name "OMAS-projects"))
;;;  "central function to load a full application, once the global parameters have ~
;;;   been extracted from the parameters file and stored into *omas*.
;;;Arguments:
;;;   :folder-name (opt): name of folder containing the application files 
;;;                       default \"OMAS-projects\"
;;;Return:
;;;   T if load was successful, NIL otherwise"
;;;  
;;;  (let ((site-name (site-name *omas*))
;;;        (appli/coterie-name (coterie-name *omas*))
;;;        file-name)
;;;    
;;;    ;; build a local user name to go with the control panel, e.g. :<ODIN-USER>
;;;    (setf (local-user *omas*)
;;;          (intern (string-upcase (format nil "<~A-USER>" site-name)) 
;;;                  :keyword))
;;;    
;;;    ;;============================================================================
;;;    ;;                  Get list of agents in the application
;;;    ;;============================================================================
;;;    
;;;    ;; build up pathname for the application agent.lisp file    
;;; ;;;    (setq file-name (make-application-file-pathname     
;;; ;;;                     *omas-directory-string* folder-name     
;;; ;;;                     site-name appli/coterie-name "agents"))    
;;;    (setq file-name (make-application-file-pathname     
;;;                     "~/" folder-name     
;;;                     site-name appli/coterie-name "agents"))    
;;;    ;(format *debug-io*  "~&=== oi-load-on-click; agents.lisp: ~S" file-name)    
;;;    ;; try to load agents file recompiling t if needed    
;;;
;;;    (unless (compile-and/or-load file-name :external-format :utf-8)    
;;;      (warn "load-application: Application ~A-~A not present in ~S folder.~2%
;;;*** To change the application if the green init window is not present, exit from ~
;;;the system, press the SHIFT key and hold it while the system is restarted. ~
;;;This will trigger the green init menu for selecting a different application.***"
;;;        site-name appli/coterie-name folder-name)    
;;;      (%beep)
;;;      (return-from load-application nil))
;;;    
;;;    ;; compute the application directory pathname from agents file-name
;;;    ;; and save it for later user by make-agent JPB0912
;;;    (setf (omas-application-directory *omas*) 
;;;          (make-pathname :directory (pathname-directory file-name)))
;;;    
;;;    ;; tell user that everything is OK, print local agent names    
;;;    ;(format *debug-io* "~&;oi-load-on-click /local-coterie-agents: ~S" 
;;;    ;  omas::*local-coterie-agents*)   
;;;    
;;;    ;;============================================================================
;;;    ;;                               load agents
;;;    ;;============================================================================
;;;    
;;;    ;; load each agent files    
;;;    (dolist (agent *local-coterie-agents*)       
;;;      ;; cook up agent file-pathname    
;;;      (setq file-name (make-application-file-pathname     
;;;                       "~/" folder-name site-name appli/coterie-name     
;;;                       (symbol-name agent))) 
;;;      ;(format t "~&; oi-load-on-click /loading agent: ~S" agent)
;;;      (load-agent :agent-file-pathname file-name)
;;;      )
;;;    
;;;    ;;============================================================================
;;;    ;;                            load message file
;;;    ;;============================================================================
;;;    ;; load message file if any (don't complain if none)    
;;;    (setq file-name (make-application-file-pathname     
;;;                     "~/"  folder-name    
;;;                     site-name appli/coterie-name "Z-messages"))    
;;;    ;(format *debug-io*  "~&;oloc; source 3: ~S" file-name)    
;;;
;;;    (compile-and/or-load file-name :external-format :utf-8)
;;;    
;;;    ;;============================================================================
;;;    ;;                       load manager file (not available)
;;;    ;;============================================================================
;;;    ;; load manager file (use load-manager from the load file)
;;;    ;(load-manager)
;;;    
;;;    ;;============================================================================
;;;    ;;                            create control panel
;;;    ;;============================================================================
;;;    (make-omas-control-panel)
;;;    
;;;    ;;============================================================================
;;;    ;;                               net control
;;;    ;;============================================================================
;;;    
;;;    ;; keep messages local when broadcast net address is blank    
;;;    (setf (net-broadcast *omas*)
;;;          (if (string-equal (local-broadcast-address *omas*) "") nil t))
;;;    
;;;    ;; if using the net, then initialize
;;;    (when (net-broadcast *omas*)    
;;;      ;; This start a receiving process *omas-net-receive*    
;;;      ;; and a dispatching process *omas-net-dispatch* that delivers the messages    
;;;      ;; to the local agents    
;;;      (omas-net-initialize))
;;;    
;;;    ;; here the policy would be that we enter a single event processing loop...    
;;;    ;(%window-close *init-window*)
;;;    
;;;    (when (equal appli/coterie-name "NULL")
;;;      (setq *package* (find-package :cg-user)))
;;;    
;;;    ;;==============================================================================
;;;    ;;      if graphics-window is T open gaphics window
;;;    ;;==============================================================================
;;;    
;;;    (if (graphics-window *omas*)
;;;        (let ((spy-key (key (symbol-value (spy-name *omas*)))))
;;;          ;; if agents were not selected select them all
;;;          (unless (names-of-agents-to-display *omas*)
;;;            (setf (names-of-agents-to-display *omas*) 
;;;              (mapcar #'car (local-agents *omas*))))
;;;          (%send-message (make-instance 'message :type :sys-inform :to spy-key
;;;                           :action :enable)
;;;                         t) ; indicates local message
;;;          ))
;;;
;;;    ;;==============================================================================
;;;    ;;                 check init function (from agents.lisp)
;;;    ;;==============================================================================
;;;    (if (fboundp 'init) (funcall 'init))
;;;    
;;;    :done)
;;;  t)

;;;--------------------------------------------------------- LOAD-APPLICATION-FILE
;;; unused when loading OMAS via ASDF

(defun load-application-file (filename &optional (utf8-flag :utf-8))
  "given a string specifying the application file, loads it compiling it if needed. Uses ~
   (omas-application-directory *omas*) locates the directory containing the file.
Argument:
   filename: a string
   utf8-flag (opt): if :utf-8 (default) indicates that the file is utf8 encoded
Return:
   :done"
  (let* ((dir-path (omas-application-directory *omas*))
         (source (merge-pathnames (string+ dir-path filename ".lisp")))
         (compiled (make-pathname
                    :device (pathname-device dir-path)
                    :host (pathname-host dir-path)
                    :directory `(,@(pathname-directory dir-path) "FASL")
                    :name filename
                    :type "fasl"
                    )))
    
    #+compiler ; when compiler is there, compile file if needed
    (cond
     ;; if source file is there, check for possible recompiling
     ;; since patches are available as source file, they will be recompiled
     ;; if needed
     ((probe-file source)
      (compile-if-needed source compiled utf8-flag)
      ;(format *debug-io* "~&Load-pathname 1: ~S" compiled)
      (apply #'load compiled (and utf8-flag (list :external-format :utf-8))))
     ;; if compiled file is there load it
     ((probe-file compiled)
      ;(format *debug-io* "~&Load-pathname 2: ~S" compiled)
      (apply #'load compiled (and utf8-flag (list :external-format :utf-8))))
     ;; otherwise complain
     (t (warn "~&File missing: ~S" filename)))
    
    ;; when compiler is not there (some distributed versions), load source
    ;; file if more recent than compiled file
    #-compiler
    (cond
     ((and (probe-file source) (probe-file compiled)
           (<= (file-write-date compiled)
               (file-write-date source)))
      (apply #'load source (and utf8-flag (list :external-format :utf-8)))
      )
     ;; if source file there, then it is more recent
     ((probe-file source)
      (apply #'load source (and utf8-flag (list :external-format :utf-8))))
     ;; if source not there, check compiled and load it
     ((probe-file compiled)
      (apply #'load compiled (and utf8-flag (list :external-format :utf-8))))
     ;; no file there
     (t (warn "~&File missing: ~S" filename)))
    :file-loaded))

;;;------------------------------------------ LOAD-APPLICATION-OR-OPEN-INIT-WINDOW
;;; used by main to launch the application, by trying first the OMAS-projects folder
;;; then the applications folder, then the sample-applications folder
;;; Here we have only 1 thread and can use special variables safely

(defun load-application-or-open-init-window ()
  "record omas parameters into *omas* table and either load application if ~
   AUTOMATIC is not true and SHIFT key is not pressed, or open INIT window."
  (declare (special *omas* *init-window*))
  (let* ((app-file (string+ (site-name *omas*) "-" (coterie-name *omas*)))
         (app-in-projects 
          (string+ *omas-application-directory* app-file "/agents.lisp"))
         (app-in-applications 
          (string+ (omas-directory *omas*) "applications/" app-file "/agents.lisp"))
         (app-in-sample
          (string+ (omas-directory *omas*) "sample-applications/" app-file "/agents.lisp"))
         )
  ;; check if must open INIT window
  (cond
   ;; if config file contains AUTOMATIC and ALT key is not pressed, load immediately
   ((and  
     (omas-automatic *omas*)
     (not (cg:key-is-down-p cg:vk-shift)))
    ;; now that OMAS is loaded, we can save parameters into *omas* table 
    ;(omas::save-omas-properties-to-globals omas::*omas-parameters*)
    ;; here we assume that the parameters file exists and do not save parameters
    ;; again
    ;; load application from applications folder or sample-applications folder
    (unless (or (load-application app-in-projects) 
                (load-application app-in-applications)
                (load-application app-in-sample)
                )
                )
      ;; if we could not find the application then open INIT window
      (omas::make-omas-init-window)
      (cg:set-foreground-window *init-window*))
   
   ;; otherwise, open the INIT window
   (t    
    (omas::make-omas-init-window)
    (cg:set-foreground-window *init-window*)
    ))
  ))  
  
;;;------------------------------------------------------------- MAKE-NEW-PATHNAME

;;; used by the init window callbacks in particular

(defun make-new-pathname (pathname &key device host directory name type version)
  "build a new pathname from a given one by supplying parts.
Arguments:
   pathname: pathname of a reference file
   device (opt): device
   host (opt): host
   directory (key): a list of directories
   name (key): filemname
   type (key): file type
   version (key): version if it applies
Return:
   updated pathname."
  (make-pathname
   :device (or device (pathname-device pathname))
   :host (or host (pathname-host pathname))
   :directory (or directory (pathname-directory pathname))
   :name (or name (pathname-name pathname))
   :type (or type (pathname-type pathname))
   :version (or version (pathname-version pathname))))

#|
? omas::*omas-directory*
#P"C:\\Program Files\\acl81-express\\OMAS-MOSS 7.0.5\\load-omas-moss.fasl"
? (make-new-pathname omas::*omas-directory* 
      :name "my-file"
      :type "cl")
#P"C:\\Program Files\\acl81-express\\OMAS-MOSS 7.0.5\\my-file.cl"
? omas::*omas-directory-string*
#P"C:\\Program Files\\acl81-express\\OMAS-MOSS 7.0.5\\OMAS\\"
? (make-new-pathname omas::*omas-directory-string* 
      :name "my-file"
      :type "cl")
#P"C:\\Program Files\\acl81-express\\OMAS-MOSS 7.0.5\\OMAS\\my-file.cl"
|#


(format t "~&;*** Agent platform OMAS v ~A loaded ***" *omas-version-number*) 

;;; :EOF