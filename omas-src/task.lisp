﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;                O M A S - T A S K  (file task.lisp)
;;;
;;;===============================================================================
;;; This file contains functions handling agents tasks (representation and memory)

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| 
The principle of recording previous tasks is simple. 
   - when a task is received, then a description of the task is temporarily kept in
     the task description part of the agent
   - when the task is completed and an answer is returned to somebody, then the 
     description of the task is kept in memory with its main characteristics
Thereafter
   - if a task is requested with the same skill and parameters, then the result
     is looked up in the agent memory and the answer returned immediately
   - if the agent receives a call-for-bids for the same task, then it will submit 
     a bid-with-answer containing the result of the processing of the task. The
     caller will then have the choice of cancelling the bidding process right
     away or waiting until enough answers are obtained.
When request or CFB are received for task that are already recorded, we simply 
record the information related to the instance of the new job (for statistics 
purposes).

2020
 0202 restructuring OMAS as an assdf system
|# 

(in-package :omas)

;;;============================== task structure =================================
;;; a task is implemented in 2 parts: a generic one and another one containing
;;; details.
;;; when the task is saved the generic part will become a common part to all
;;; instances of the task kept in the task-instance list.

(defClass TASK ()
  ((agent :accessor agent :initform nil :initarg :agent) ; inverse link
   (skill :accessor skill :initform nil)
   (arg-list :accessor arg-list :initform nil)
   (answer :accessor answer :initform nil)
   (duration :accessor duration :initform nil)
   (quality :accessor quality :initform nil)
   (competitors :accessor competitors :initform nil)
   (task-details :accessor task-details :initform nil :initarg :task-details)
   (task-instances :accessor task-instances :initform nil)
   )
  (:documentation 
   "a task is a model of task to be recorded in the task part of the ~
    agent while the task is being processed. After the task is ~
    finished then it is cleaned and transferred into the agent memory.")
  )

(defMethod print-object ((mm task) stream)
  (format stream "#<TASK ~A ~A ~A ~A ~A ~A ~A>"
          (name (agent mm))(skill mm)
          (format nil "~A" (arg-list mm))
          (cond ((answer mm))("no-R"))    ; result
          (cond ((duration mm))("no-D"))
          (cond ((quality mm))("no-Q"))
          (or (task-details mm)(task-instances mm))))

(defMethod start-date ((task task))
  (start-date (task-details task)))

(defClass TASK-INSTANCE ()
  ((task :accessor task :initarg :task)  ; inverse link
   (task-message :accessor task-message :initform nil :initarg :task-message)
   (task-id :accessor task-id :initform nil)
   (start-date :accessor start-date :initform nil)
   (customer :accessor customer :initform nil)
   (time-limit :accessor time-limit :initform nil)
   (protocol :accessor protocol :initform nil)
   (broadcast-max-answers :accessor broadcast-max-answers :initform nil)
   (continuation :accessor continuation :initform nil)
   (grant-date :accessor grant-date :initform nil)
   (bids :accessor bids :initform nil)
   )
  (:documentation
   "describe an instance of task corresponding to a skill and specific arguments.")
  )

(defMethod print-object ((mm task-instance) stream)
  (format stream "#<TASK-INST ~A ~A ~A ~A bids:~A>"
          (task-id mm)(continuation mm)(start-date mm)
          (cond ((time-limit mm))("no-TL"))
          ;(cond ((repeat-count mm))("no-RC"))
          ;(if (duration mm) (format nil " duration: ~S" (duration mm)) "")
          (length (bids mm))))

;;;===============================================================================

;;;---------------------------------------------- AGENT-CHECK-MEMORY-FOR-SAME-TASK

(defun agent-check-memory-for-same-task (agent message)
  "checks if the task described in the message has already been executed. If so, ~
   returns a pointer onto the task (I.e., the task object).
Arguments:
   agent: agent
   message: message containing task to check."
  ;; checks the skill
  (dolist (task (memory agent))
    ;; skills are represented by a keyword (should be more complex)
    (when (eql (action message) (skill task))
      ;; same skill, check arg-list (must be in the same order
      (when (equal (args message) (arg-list task))
        (agent-trace agent 
                     "...result for the task ~S ~S ~S available from memory" 
                     (task-id message) (action message) (args message))
        ;; if everything checks then return the pointer onto the task
        (return-from agent-check-memory-for-same-task task)))))


;;;---------------------------------------------------- AGENT-RECORD-STARTING-TASK

(defun agent-record-starting-task (agent message)
  "record a task, if not already recorded in memory. Create it, fill the info ~
   from the input message and record it in the waiting-tasks list."
  (let* ((task (make-instance 'task :agent agent))
         (task-instance (make-instance 'task-instance :task task)))
    ;; first initialize generic info
    (setf (skill task)(action message)
          (arg-list task)(args message)
          (task-details task) task-instance)
    ;;...then info for this particular instance of task
    (setf (task-message task-instance) message
          (task-id task-instance) (task-id message)
          (start-date task-instance)(date message)
          (protocol task-instance)(protocol message)
          (customer task-instance)(from! message)
          (time-limit task-instance)(time-limit message))
    ;; add record onto the :waiting-tasks list
    ;; since the agent can do one task at a time the list contains only one item
    (agent-add agent task :waiting-tasks)
    (agent-trace agent  "preparing ~S, saved on the waiting-tasks list" task)
    ))

;;;----------------------------------------------------------- AGENT-REMEMBER-TASK

(defun agent-remember-task (agent message &aux task task-instance)
  "remember a task, moving it from the waiting-tasks to the memory, recording the ~
   result and its quality, and the time it took to complete it.
   Since there is a single task in the waiting-tasks list the following is quite ~
   simple.
Arguments:
   agent: agent
   message: message specifying the task."
  ;; first check if we have already recorded that task
  (setq task 
        (car (member-if #'(lambda (xx) 
                            (and (eql (skill xx) (action message))
                                 (equal (arg-list xx)(args message))))
                        (memory agent))))
  ;(print task)
  ;; if so call an update function
  (when task
    (return-from agent-remember-task
      (agent-update-task agent message)))
  
  ;; get the task description from the waiting-tasks list 
  ;; currently, since an agent can only process a single task at a time the task
  ;; description is bound to be the first element of the list
  ;; if the agent becomes able of multi-tasking, then the process of extracting the
  ;; right task description will have to be more careful
  (setq task (car (waiting-tasks agent)))
  (when task
    ;; get the result and save it
    (setf (answer task) (contents message))
    ;; compute duration and save it
    (setf (duration task)
          (- (date message) (start-date task)))
    ;;...and quality if available
    ;; move the instance part to the instance list
    (setf (task-instances task)(list (task-details task)))
    ;; and clean the details
    (setf (task-details task) nil)
    ;; record the task in memory
    ;; *** we assume that the memory is structured as a list of tasks...
    #+MCL
    (%agent-add agent task :memory)
    #+MICROSOFT-32
    (agent-add agent task :memory)
    (agent-trace agent "recording ~S, in the agent's memory." task)
    ;; erase the task from the waiting-tasks list 
    ;; *** again, currently there is only one task in the waiting list
    (setf (waiting-tasks agent) nil)
    )
  (unless task
    ;; here we want to save the answer but we do not have the task model
    ;; it may come in particular from the fact that we finished the job before
    ;; receiving a cancel-grant message from the emitter.
    ;; create a task object and a task-instance for the details
    (setq task (make-instance 'task :agent agent)
          task-instance (make-instance 'task-instance :task task))
    ;; ...and fill what we can, starting with generic info
    (setf (skill task)(action message))
    (setf (arg-list task)(args message))
    (setf (answer task) (contents message))
    (setf (task-details task) task-instance)
    ;;...then info for this particular instance of task
    (setf (task-id task-instance) (task-id message))
    (setf (protocol task-instance)(protocol message))
    (setf (customer task-instance)(to! message))
    ;; record the task in memory
    #+MCL
    (%agent-add agent task :memory)
    #+MICROSOFT-32
    (agent-add agent task :memory)
    ;; *** we assume that the memory is structured as a list of tasks...
    (agent-trace agent "recording ~S, in the agent's memory (partial info)." task)
    )
  :done)

;;;------------------------------------------------------------- AGENT-UPDATE-TASK

(defun agent-update-task (agent message)
  "we have a task entry in memory containing info for the task being processed by ~
   the agent, and we want to update the entry with new results. This should not ~
   happen if the agent had recognized that the task to do was the same as one ~
   it has done previously. Thus, we do nothing and send a warning.
Arguments:
   agent: agent
   message: message containing answer to the task."
  (declare (ignore message))
  (agent-trace agent
               "already recorded info for the same kind of job, does not do anything.")
  )

;;;===============================================================================

(format t "~&;*** OMAS v~A - task loaded ***" *omas-version-number*)

;;; :EOF