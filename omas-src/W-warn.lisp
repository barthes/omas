﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/04
;;;                O M A S - W A R N  (file W-warn.lisp)
;;;
;;;                copyright JP Barthès @UTC, 2013
;;;
;;;===============================================================================
;;; This file contains function to build a pop up window containing important 
;;; warnings to the user

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#
#|
History
-------
2020
 0204 restructuring OMAS as an asdf system
|#

(in-package :omas)

;;;================================ Globals ======================================
;;; position the trace window rigth under the control panel

(defparameter *warn-window* nil "pop up window for warning user")

(defparameter *warn-left* (+ *panel-left-position* 200))
(defparameter *warn-top* (+ *panel-height-offset* *panel-height* 100))
(defparameter *warn-height* 60)
(defparameter *warn-width* *panel-width*)

;;;------------------------------------------------------- MAKE-WARN-POP-UP-WINDOW
  
(defun make-warn-pop-up-window ()
  (declare (special *warn-window*))
  (setq *warn-window*
        (cg:make-window :modal-dialog-test
          :class 'cg:dialog
          :title "Warning..."
          :pop-up t
          :interior (cg:make-box-relative *warn-left* *warn-top* 
                                          *warn-width* *warn-height*)
          :dialog-items
          (list
           (make-instance 'cg:static-text
             :name :message
             :value ""
             :left 10 :top 10
             :width 180 :height 20)
           ))))

;;;--------------------------------------------------------------------- OMAS-WARN

(defun omas-warn (text)
  (unless *warn-window* (make-warn-pop-up-window))
  (cg:beep)
  (let ((pane (cg:find-component :message *warn-window*)))
    (setf (cg:value pane) text)
    (cg:pop-up-modal-dialog *warn-window*)))

#|
? (omas-warn "L'agent n'est pas persistant...")
|#

(format t "~&;*** OMAS v~A - warn pop up window loaded ***" omas::*omas-version-number*)

;;; :EOF