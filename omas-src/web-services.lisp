﻿;;;-*- Mode: Lisp; Package: "OMAS.WEB" -*-
;;;===============================================================================
;;;20/02/02
;;;              O M A S - W E B (file web.lisp)
;;;
;;;===============================================================================
;;; this file contains functions for handling Web clients, as well as a set of 
;;; functions for editing objects in a client's Web page. 

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;; The interface functions
;;; like get-answer or get-web-answer are executed in a Web Server Worker, i.e. a 
;;; thread controlled by Allegroserve. They use a gate for synchronizing exchanges
;;; which requires the server to be in the same Lisp environment as the PA that
;;; receives the data from the client. When this is not the case, one must use a
;;; PA proxy in charge of sending and receiving messages from the remote PA and
;;; synchronizing with the Web Server Worker.

;;; OMAS.WEB uses "ACL aserve" to serve web pages.

;;; Basic mechanism:
;;;    - start the server, and create a computed entity that will receive the
;;;      external web queries
;;;    - when a query comes in it triggers the function associated with the computed
;;;      entity. the function recovers the query string, creates a message to send
;;;      to :WEB and waits on a variable that will receive the answer
;;;      tells :WEB where to put the answer.
;;;    - it then returns the answer to the requesting browser

;;; Improvement: currently only one server can be active at a time. It would be 
;;; nice to attach a server to an agent, provided aserve allows opening several
;;; servers simultaneously.

;;; 2013
;;; We define a pseudo WEB window having a display-text method that will transform
;;; regular strings into HTML strings and pass HTML strings back to the get-answer
;;; function. This must be done in the OMAS package.

;;; 2015
;;; Trace is activated by adding :web and :gate-answer to (debugging *omas*)
 

#|
2011
 0513 creation
 0517 adding a gate for synchronizing data
 0527 improving get-message with keyword options
2012
 1031 adapting code to make it a default OMAS code
 1128 modified web-open-gate to avoid error when gate is nil
2013
 0705 adding :pattern arg to get-answer
 1126 changing updat-object to make it return an object description
 1230 including web editor functions in this file
2014
 0127 adding wu-rem-user-entry
 0203 separating generic function from user-defined actions
 0218 trying to reorganize editor pages
 0223 improving parm2obdesc and create-object by adding syntax tests
 0223 correcting omas::update-add-del-locate-successor
 0307 -> utf8
 0318 correcting update-trim-instructions
 0322 modifying omas::update-add-del-locate-successor
 0618 correcting a bug in web-edit-user-action
 0624 correcting initial-object2parms to account for multiple value attributes
      improving check-property form multiple value attributes
 0707 Correcting %set-language
 0813 modifying check-property
 0816 modifying update-add-del-locate-successor to accomodate MLNs
      adding user-login to wu-keep for external entries
 0922 adding property option to area-val
 0923 modifying web-edit-commit-update to introduce :modify-object skill
 0924 modifying omas::publish-web-edit-form to keep empty parameters 
 1029 renaming make-row to make-field, row-area to make-field-area, row-val to
      make-field-row to keep things together, and introducing make-button-link
 1031 adding onChange options to area and row
 1103 adding owner option to web-edit-commit-create
2015
 0402 changing (format t to (dformat :gate-answer 0
 0521 adding value-handle to make-field-row
 0522 correcting a bug in web-edit-commit-update, and in web-locator
 0522 adding omas-trace-add
 0527 updating web-edit-commit-update to add IP to args of the modify-object skill
|#

;;; We could set up a page server to access HDSRI information, using forms. A first
;;; part of the page would have a nice header with Heudiasyc logo.

;;; call is http://localhost:80/queryform?/ from machine hosting the server
;;; or http://mikonos/<web page-name>/ from inside UTC or using VPN
;;; or http://nat-omas.utc.fr/<web page name>/ from outside UTC


(in-package :omas.web)

(eval-when (:compile-toplevel :load-toplevel :execute)
  ;; add entries to the list of debugging-tags in the agent structure
  (omas::omas-trace-add :web "traces interactions with the web server")
  (omas::omas-trace-add :gate "traces gate synchronizations")
  )

;;;===============================================================================
;;;                                 Globals
;;;===============================================================================
;;; local to this file

(defparameter *web-gate-list* nil "alist of synchronizing gates")
(defparameter *web-answer-list* nil "alist of answers")
(defparameter *web-gate-counter* 0 "indexing gates and answers")

;;;===============================================================================
;;;                                 Macros
;;;===============================================================================

(defun select-name (input)
  (mln::extract-to-string input :always t))

#|
(select-name omas::*web-edit*)
"Edit new object..."
(setq *language* :fr)
(select-name omas::*web-edit*)
"Editer un  nouvel objet..."
(setq *language* :it)
(select-name omas::*web-edit*)
"Edit new object..."
|#

;;;===============================================================================
;;;                            Service Functions
;;;===============================================================================

(defun omas::su () (print (omas::users *omas*)) :done)

;;;----------------------------------------------------------- OMAS::CREATE-OBJECT

(defun omas::create-object (agent class-ref parms)
  "generic function for creating an object from the parms corresponding to fields ~
   of the edit page.
Arguments:
   agent: current agent
   class-ref: ref of class of the object to be created
   parms: a list of the fields of the edit page
Return
   2 values: id and message-list"
  (dformat :web 0 
           "~2%;---------- ~A Entering create-object function" (omas::name agent))
  (dformat :web 0
           "~%;-- class-ref: ~S~%;-- parms: ~S" class-ref parms)
  (let (obj-l suc-id suc-id-list suc-list id result message-list)
    ;; get the object description, using the page-description
    (setq obj-l (omas::parm2objdesc agent class-ref parms))
    
    ;;=== do a loop on properties
    (dolist (pair obj-l)
      (catch 
       :next
       (cond
        ;;== when attribute just record it
        ((moss::%is-attribute? (car pair))
         (push pair result)
         )
        ;;== if relation, more complex
        ((moss::%is-relation? (car pair))
         
         ;; examine each successor description list, first reset suc-list
         (setq suc-list nil)
         
         (dolist (suc (cdr pair))
           (dformat :web 0 "~%; create-object /suc: ~S" suc)
           ;; check if successor exists
           (dformat :web 0 "~% create-object /query: ~S"
             (cons (car suc)
                   (mapcar #'(lambda (xx)`(,(car xx) :is ,(cadr xx)))
                     (cdr suc))))
           
           ;; if a name of property is nil for some reason, then issue a warning and
           ;; skip the property
           (unless (every #'(lambda(xx) (car xx)) (cdr suc))
             (cg:beep)
             (warn "property in clause ~S should not be NIL. We skip it." pair)
             (throw :next nil))
           
           (setq suc-id-list 
                 (access (cons (car suc)
                               (mapcar #'(lambda (xx)`(,(car xx) :is ,(cadr xx)))
                                 (cdr suc)))))
           (dformat :web 1 "~% create-object /suc-id-list: ~S" suc-id-list)
           ;(break)
           (cond
            ;; if we found something then record it
            (suc-id-list
             (setq suc-list (append suc-id-list suc-list)))
            ;; if not create the object locally
            (t
             (with-transaction agent
               (setq suc-id (apply #'make-individual suc)))
             ;; put warning message
             (push
              (format nil "I created a new object: ~{~S~}" (send suc-id '=summary))
              message-list)
             ;; add to suc-list
             (push suc-id suc-list)
             )
            )
           )
         ;; record successors
         (push `(,(car pair) ,@(reverse suc-list)) result)
         )
        ;; if not attribute or relation, ignore
        )))
    (dformat :web 1 "~% create-object /result:~%  ~S" (reverse result))
    
    ;;=== create news-item
    (with-transaction agent
      (setq id (apply #'make-individual class-ref (reverse result))))
    
    ;; return id and message-list
    (values id message-list)
    ))

;;;----------------------------------------------------------------------- DOTTIFY

(defun dottify (obj-l)
  (cond
   ;; no more, end
   ((null obj-l) nil)
   ;; no value skip it
   ((null (cdar obj-l)) (dottify (cdr obj-l)))
   ;; single value but ""
   ((and (null (cddar obj-l))(equal+ (cadar obj-l) "")) (dottify (cdr obj-l)))
   ;; other values
   (t (cons (cons (caar obj-l) (format nil "~{~A~^, ~}" (cdar obj-l)))
            (dottify (cdr obj-l))))))

#|
(dottify nil)
NIL
(dottify '(("aaa" "")))
NIL
(dottify '(("aaa")))
NIL
(dottify '(("aaa" "a")))
(("aaa" . "a"))
(dottify '(("aaa" "a" "b")))
(("aaa" . "a, b"))
(dottify '(("aaa" "a")("b" "")("ccc" "c" "d" "f")))
(("aaa" . "a") ("ccc" . "c, d, f"))
(dottify '(("aaa" (:fr "q" :en "r"))))
(("aaa" . "(FR q EN r)"))
|#
;;;-------------------------------------------------------------- OMAS::GET-ANSWER
;;; get-answer is called by the function attached to the web page. Because it
;;; is not an agent it is synchronized by using a gate. Hence, when the answer is
;;; ready to be recovered the gate is opened and the answer can be retrieved from
;;; a list of answers in *web-answer-list*. However, if the agent that provides the
;;; answer is not in the same Lisp environment, then it cannot open the gate.
;;; Consequently it is necessary to go through a proxy agent living in the same
;;; environment as the web server.
;;; The get-answer function builds a message for the proxy (here called WEB) so
;;; that the proxy sends the request to another agent (possibly remote). get-answer
;;; sets up a gate and a timer. When the answer comes back to WEB it is put into 
;;; *web-answer-list* and the gate is opened by WEB. If the timer fires before 
;;; the answer had a chance to come back, then an error response page is provided.

(defun omas::get-answer (agent text &key to-agent (timeout 30) (type :request)
                               pattern (action :process-web-text) error-page
                               language ack allow-nil-answer)
  "sends a message to the to-agent. If the message is an INFORM message, then ~
   we send it directly, in which case the sender is nil and wo do not wait.  ~
   If the message requires to wait for an answer, we cannot send it directly ~
   because of synchronization problems when to-agent is not in the same Lisp ~
   environment. We send a message to the agent WEB asking it to send a regular ~
   message to the to-agent. This function is usually run from within a Web Server ~
   worker that is synchronized using a gate.
Arguments:
   agent: the agent involved in the action (used to specify the synchronizing gate)
   text: a string that will be sent as the data part of the protocol
   pattern (key): a pattern to be passed to the final agent
   to-agent (key): a keyword specifying the agent that will receive the message
   timeout (key): the timeout delay (default 30s)
   type (key): type of message to dend (default is :request)
   ack (key): if true then use the acknowledge option in the message
   action (key): the name of the skill to be activated (default :process-web-message)
   allow-nil-answer (key): if t, returns nil instead of error page
   error-page (key): an html page to return in case the server times out
Return:
   an html string representing the answer or a default error message on timeout."
  (declare (special *web-gate-list* *web-answer-list* mp:*current-process*))
  ;; if the type of message to send is :inform, we are not expecting an answer
  ;; thus, we can send the message directly and return something
  (case type
    ;; this amounts to using the function for notifications rather than for getting
    ;; an answer (semantically not in line with the name of the function)
    ((:alert :inform)
     (let ((message
            (make-instance 'omas::message
              :type type
              :to to-agent
              :action action
              :args `(((:data . ,text)
                       ,@(if language `((:language . ,language)))))
              :ack ack)))
       
       (dformat :web 0 "~%; omas::get-answer /alert/inform message:~% ~S" message)
       ;; message will appear to be sent by control-panel (WEB-USER) 
       ;; inform messages ignore the pattern arg
       (send-message message)
       ;; no answer will be returned, so cook up one
       "Message was sent..."))
    
    ;;===== when we have to wait for an answer, we must use the web agent that will
    ;; send a request message and wait for the returned answer
    (otherwise     
     (let (gate-index message answer)
       ;; first create a synchronizing gate in closed state
       (setq gate-index (omas::web-create-gate))
       (dformat :web 1 "~%; get-answer /web gate list: ~S" *web-gate-list*)
       
       ;; then build a message intended for the agent handling the data
       (setq message
             (make-instance 'omas::message
               :type :request 
               :to (omas::key agent) 
               :action :process-web-request 
               :repeat-count 2 ; won't send message again (third message)
               :args `(((:gate . ,gate-index)
                        (:to-agent . ,to-agent)
                        (:action . ,action)
                        (:timeout . ,timeout)
                        (:data . ,text)
                        ,@(if language `((:language . ,language)))
                        ,@(if pattern `((:pattern . ,pattern)))))))
       
       (dformat :web 1 "~%; get-answer /current-process:~%   ~S" mp:*current-process*)
       (dformat :web 0 "~%; omas::get-answer /message:~% ~S" message)
       
       ;; send the message, do not put it on the LAN since message is for an agent
       ;; in the same environment
       (send-message message :locally? t)
       
       ;; wait according to timeout, make sure we always close the gate
       (unwind-protect
           (cond
            ;; the function waits until the gate is opened or until timeout (default 30s)
            ;; the function returns nil on timeout
            ((mp:process-wait-with-timeout "waiting web processing" timeout 
                                           #'mp:gate-open-p 
                                           (cdr (assoc gate-index *web-gate-list*)))
             (dformat :web 1 
                      "~%; omas::get-answer /no timeout, *web-answer-list*:~%  ~S"
                      *web-answer-list*)
             (dformat :web 0 "~%; omas::get-answer /no timeout")
             ;; close gate again for next time around (actually kills it)
             ;; answer should be some sort of html string
             (setq answer (cdr (assoc gate-index *web-answer-list*)))
             (dformat :web 0 "~%; omas::get-answer /length(answer): ~S" 
                      (length answer))
             (dformat :web 1 "~%; omas::get-answer /answer: ~S" answer)
             )
            ;; otherwise we did not get an answer in time
            (t 
             (dformat :web 0 "~%; omas::get-answer /timeout")
             (dformat :web 1 
                      "~%; omas::get-answer /timeout, gate: ~S~%" gate-index)
             ;; will return whatever is the value of error-page if non nil
             ;; e.g. if error-page is :failure, will return :failure. Can't be nil
             (setq answer
                   (unless allow-nil-answer
                     (omas::make-html-error-page error-page)))
             (dformat :web 1 "~%; omas::get-answer /answer: ~S~%" answer)
             ))
         ;; unwind forms always executed
         (dformat :web 1
                  "~%; get-answer /unwind-protect /get-index: ~S~%" gate-index)
         (omas::web-close-gate gate-index)
         (omas::web-pop-answer gate-index)
         (dformat :web 0 "~%; omas::get-answer /answer on exit: ~S" answer)
         ;; return-from is necessary otherwise getting out of case returns nil
         (return-from omas::get-answer answer)) ; JPB1502
       ))))

;;;------------------------------------------------------------ GET-LAYOUT-PATTERN
;;; The keyword representing the to-agent exists in the OMAS environment, which is
;;; why we can cache the page description onto its p-list, even if the agent is
;;; actually on a different machine

(defun get-layout-pattern (agent class-ref to-agent-key language timeout)
  "ask for layout pattern for the page displaying the specified class to the owner ~
   agent, using its key. If it is not cached onto the p-list of the owner agent, ~
   then sends a message to the owner agent using get-answer, and if no error, ~
   caches the result onto the p-list of the owner agent.
Arguments:
   agent: current agent
   class-ref: class reference (a string)
   to-agent-key: key of the owner agent (a keyword)
   language: current language tag
   timeout: timeout for waiting for an answer
Return:
   a page description, or a list starting with :error"
  (dformat :web 1 "~%; get-layout-pattern /*package*: ~S" *package*)
  (let (;(indicator 
        ; (intern (symbol-name (moss::%make-name-for-class class-ref)) :keyword))
        indicator ; JPB 1601
        result)
    
    ;; check that to-agent-key is indeed a symbol
    (when (or (null to-agent-key) (not (symbolp to-agent-key)))
      (error "get-layout-pattern: agent key argument should be a symbol, not ~S"
        to-agent-key))
    
    ;; kludge: cannot use %make-name-for-class because it needs to execute in 
    ;; the agent package
    ;; we assume that class-ref is a string and make up the class name directly
    (setq indicator (intern (string-upcase class-ref) :keyword))
          
    (dformat :web 1 "~%; get-layout-pattern /indicator: ~S" indicator)
    
    ;; try p-list first
    (setq result (get to-agent-key indicator))
    (dformat :web 1 "~%; get-layout-pattern /result: ~S" result)
    ;; if it fails, send message
    (unless result
      (setq result (omas::get-answer agent class-ref :to-agent to-agent-key
                                     :action :get-edit-page-layout
                                     :language language
                                     :timeout timeout))
      ;; if no error, then cache the result
      (unless (and (listp result)(eql (car result) :error))
        ;; cache value onto the p-list of to-agent-key
        (setf (get to-agent-key indicator) result)))
    result))

#|
(ow::get-layout-pattern webnews::sa_webnews "info" :anatole-news :fr 5)
;========== SA_WEBNEWS: PROCESS-WEB-REQUEST ==========

;========== SA_ANATOLE-NEWS: GET-EDIT-PAGE-LAYOUT ==========

;<<< WEBNEWS: dynamic-process-web-request
;---answer:
((:ROW :HEADER "ID (alloué par le système, read-only)" :NAME "id number" :READ-ONLY T :INITFCN
  ANATOLE-NEWS::CREATE-ID)
 (:ROW :HEADER "Titre*" :NAME "titre")
 (:AREA :HEADER "Auteur, ex. Barthès:Jean-Paul, Ramos ..." :NAME "auteur" :ROWS 1 :COLS "60%"
  :READ-ONLY ...)
 (:ROW :SELECT
  ("" "Conférences" "Divers" "Livres" "Missions" "Personnalités" "Publications" "Recherche" "Sports"
   "Visites" ...)
  :HEADER "Catégorie (max 1)" :NAME "catégorie" :PATH ("catégorie" "Catégorie") :IF-DOES-NOT-EXIST ...)
 (:ROW :HEADER "Mots-clés" :NAME "mots clés" :PATH ("mots clés" "Mot Clé") :IF-DOES-NOT-EXIST :CREATE
  :LIST-FCN ...)
 (:AREA :HEADER "Texte" :NAME "texte" :ROWS 3 :COLS "60%")
 (:ROW :HEADER "Date de création (read-only)" :NAME "date de création" :READ-ONLY T :INITFCN
  ANATOLE-NEWS::CREATE-DATE)
 (:ROW :HEADER "Date de modification (read-only)" :NAME "date de modification" :READ-ONLY T :POSTFCN
  ANATOLE-NEWS::CREATE-DATE)
 (:ROW :HEADER "Déjà Publié (read only)" :NAME "publié" :READ-ONLY T)
 (:ROW :HEADER "Archivé (read-only)" :NAME "archivé" :READ-ONLY T) ...)
Second time around the answer is cached onto the p-list of :anatole-news
|#
;;;------------------------------------------------------------------------ GET-PA
;;; The function assumes that the PA is on the same machine as the process that
;;; uses it, which may or may not be true...

(defun get-PA (username)
  "takes a user name and looks for the personal assistant file name <username>.lisp
   in the application directory. If it finds it loads it. Otherwise return nil."
  (let* ((agent-key (intern (string-upcase username) :keyword))
         file-pathname)
    ;; check if agent is part of the local agents
    (when (assoc agent-key (omas::local-agents *omas*))
      ;; OK no need to do anything else
      (dformat :web 0
               "~%; get-PA /agent-key already in local agents: ~S" agent-key)
      (return-from get-PA agent-key))
    
    ;; agent not active, try to locate definition file		 
    (setq file-pathname 
          (merge-pathnames (format nil "~A.lisp" username)
                           (omas::omas-application-directory *omas*)))
    (dformat :web 0 "~%; get-PA /file-pathname: ~S" file-pathname)
    (unless (probe-file file-pathname)
      ;; no file, return failure
      (return-from get-PA nil))
    
    ;; otherwise load agent
    (dformat :web 0 "~%; get-PA /loading agent (file-pathname)")
    (omas::load-agent :agent-file-pathname file-pathname)
    ;; return agent key
    agent-key
    ))
           
;;;---------------------------------------------------------- OMAS::GET-WEB-ANSWER
;;; get-web-answer is used by the WEB2PA agent to communicate with the PA. Thus
;;; it is not called from a Web Server Worker (WEB2PA is used by ONTOCODESIGN 
;;; application).
;;; Its use however in not very clear and it is better to use get-answer. 

;;; Because it
;;; is not an agent, it is synchronized by using a gate. Thus, when the answer is
;;; ready to be recovered the gate is opened and the answer can be retrieved from
;;; a list of answers in *web-answer-list*. However, if the agent that provides the
;;; answer is not in the same Lisp environment, then it cannot open the gate.
;;; Consequently it is necessary to go through a proxy agent living in the same
;;; environment as the web server.
;;; The get-web-answer function builds a message for the proxy (here called WEB) so
;;; that the proxy sends the request to another agent (possibly remote). get-answer
;;; sets up a gate and a timer. When the answer comes back to WEB, it is put into 
;;; *web-answer-list* and the gate is opened by WEB. If the timer fires before 
;;; the answer had a chance to come back, then an error response page is provided
;;; or else NIL is returned.

;;; the normal behavior of get-web-answer is to send WEB a message requiring the
;;; process-web-input skill.
;;; Usually, the argument is a text that should be sent to a specific PA to be 
;;; inserted into its dialog.
;;; However, at login time it can be a request to locate a specific PA.

;;; Thus the arguments of the get-web-answer functions are the following:
;;;  - agent-key: e.g. :WEB
;;;  - action: e.g. :LOCATE-PA (default :PROCESS-WEB-TEXT)
;;;  - args: a list depending on what must be done, e.g.
;;;    - for LOCATE-PA ((:data . :ALBERT))
;;;    - for PROCESS-WEB-TEXT ((:data . <text>)(:to-agent . :PA)(:action ...))
;;;  - timeout: e.g. 10 (delay for letting the answer come in)

;;; Example of calls
;;;   (get-web-answer :web :ALBERT :action :locate-pa :timeout 5 :allow-nil-answer t)
;;;   (get-web-answer :web "Bonjour" :action :process-web-text :to-agent :ALBERT 
;;;                                  :error-page *web-html-error-page*)

;;; The message should be a request message (get-answer means that we want to get
;;; an answer).

;;; One could object that the PA runs normally on the user's machine, which means
;;; that we could send the message directly to the PA. However, this may not be true 
;;; in particular when all PAs are hosted on the same remote machine as in the
;;; TATIN-PIC project. In that case we need a proxy agent, e.g. WEB, on the machine
;;; hosting the server.

(defun omas::get-web-answer (agent-key text &key (timeout 30) to-agent
                                       (action :process-web-text) error-page pattern
                                       ack allow-nil-answer)
  "takes the input and sends a message to the to-agent. Since we are not an agent ~
    the sender is nil. We then wait on a global gate. The :args field of the send ~
    message has the folllowing format:
    ((:data . text)(:answer . answer)) where answer is a global variable in which ~
    to put the answer.
Arguments:
   agent-key: the agent that will process the text (used to specify the synchronizing
         gate)
   text: a string that will be sent as the data part of the protocol, or a list
         of arguments that will be passed to the skill named in :action
   to-agent (key): agent that will process the text in the end
   timeout (key): the timeout delay (default 30s)
   action (key): the name of the skill to be activated (default :process-web-message)
   allow-nil-answer (key): if t, return nil on error instead of error page
   error-page (key): an html page to return in case server times out
   no-repeat (key): if t will not send repeated messages after first timeout
   pattern (key): a pattern specifying the format of the answer
Return:
   ((:text <HTML answer string>)) or a default error page on timeout or NIL."
  (declare (ignore ack)
           (special *web-gate-list* *web-answer-list* mp:*current-process*))
  (let (gate-index message answer)
    ;; first create a synchronizing gate
    (setq gate-index (omas::web-create-gate))
    (dformat :gate-answer 0 
             "~%; get-web-answer /new gate index: ~S" gate-index)
    (dformat :gate-answer 1 
             "~%; get-web-answer /web gate list: ~S" *web-gate-list*)
    
    ;; then build an internal message that will send a message to the PA
    ;; the internal message is processed as a standard task and put into the
    ;; agenda. However, no answer is returned.
    (setq message
          (make-instance 'omas::message
            :type :internal 
            :to agent-key 
            :action action 
            :args `(((:gate . ,gate-index)
                     (:data . ,text)
                     (:to-agent . ,to-agent)
                     ,@(if pattern `((:pattern . ,pattern)))
                     (:timeout . ,timeout)))
            :repeat-count 2))
    
    (dformat :gate-answer 1 
             "~%; get-web-answer /current-process:~%   ~S" mp:*current-process*)
    (dformat :gate-answer 0 
             "~%; get-web-answer /message:~% ~S" message)
    
    ;; send the message, do not put it on the LAN since the message is for an agent
    ;; in the same environment
    ;; we are sending the message, but could put it directly into the input box of
    ;; our WEB proxy agent
    (send-message message :locally? t)
    
    ;; wait according to timeout, make sure we always close the gate
    (unwind-protect
        (cond
         ;; the function waits until the gate is opened or until timeout (default 30s)
         ;; the function returns nil on timeout
         ((mp:process-wait-with-timeout "waiting web processing" timeout 
                                        #'mp:gate-open-p 
                                        (cdr (assoc gate-index *web-gate-list*)))
          (dformat :gate-answer 0 "~%; get-web-answer /no timeout")
          ;; close gate again for next time around (actually kills it)
          ;; answer should be some sort of html string
          (setq answer (cdr (assoc gate-index *web-answer-list*)))
          )
         ;; otherwise we did not get an answer in time
         (t 
          (dformat :gate-answer 0 "~%; get-web-answer /timeout, gate: ~S" gate-index)
          (setq answer (unless allow-nil-answer
                         `((:text . ,(omas::make-html-error-page error-page)))))
          ))
      
      ;; unwind forms (always executed)
      (dformat :gate-answer 1
               "~%; get-web-answer /unwind-protect /get-index: ~S" gate-index)
      (omas::web-close-gate gate-index)
      (omas::web-pop-answer gate-index)
      (dformat :gate-answer 0 
               "~%; get-web-answer /unwind-protect /anwer: ~S" answer)
      ;; answer is an a-list
      answer)
    ))

;;;---------------------------------------------------- OMAS::MAKE-HTML-ERROR-PAGE
	
(defun omas::make-html-error-page (&optional html-page)
  "we return an HTML string corresponding to a page"
  (or html-page
      "<html>
<head>
  <title>OMAS HTTP SERVER</title>
</head>
<body>Sorry, timeout: target agent does not answer.</body>
</html>"))

;;;---------------------------------------------------- OMAS::INITIAL-OBJECT2PARMS

(defun omas::initial-object2parms (initial-object)
  "Quick and dirty function to reduce an object description of an initial object ~
   obtained by using =make-object-description, to a set of parm pairs" 
  (remove
   nil
   (mapcar 
       #'(lambda(xx) 
           (cond 
            ((equal+ (cadr xx) "") nil)
            ;; attribute with multiple values
            ((and (stringp (cadr xx))(cddr xx))
             (cons (car xx) (format nil "~{~A~^, ~}" (cdr xx))))
            ;; multiple values
            ((and (cadr xx)(listp (cadr xx)))
             (cons (car xx) 
                   (make-value (car xx) initial-object)))
            ((stringp (cadr xx)) (cons (car xx)(cadr xx)))
            ))
     initial-object)))

#|
(setq initial-object 
      '("initial-object" ("id" "$E-INFO.15") ("id number" "NOTE-15")
        ("titre" "test 15")
        ("auteur" ("Barthès : Jean-Paul" "auteur" "$E-PERSONNE.1"))
        ("catégorie"
         ("Voyages+Missions+Visites/Travel+Missions" "catégorie"
           "$E-CATÉGORIE.9"))
        ("mots clés" ("Lybie" "mots clés" "$E-MOT-CLÉ.60")
         ("Cyrenaique" "mots clés" "$E-MOT-CLÉ.61"))
        ("texte" "OK") ("date de création" "18/06/2014")
        ("date de modification") ("publié") ("archivé")))
(omas::initial-object2parms (cdr initial-object))
(("id" . "$E-INFO.15") ("id number" . "NOTE-15") ("titre" . "test 15")
 ("auteur" . "Barthès : Jean-Paul")
 ("catégorie" . "Voyages+Missions+Visites/Travel+Missions")
 ("mots clés" . "Lybie, Cyrenaique")
 ("texte" . "OK") ("date de création" . "18/06/2014"))

(omas::initial-object2parms '(("id" "$E-MOT-CLÉ.79") ("label" "banana")
  ("libellé" "banane" "banane plantin") ("publié" "T")
                              ("inverse-links")))
(("id" . "$E-MOT-CLÉ.79") ("label" . "banana") ("libellé" . "banane, banane plantin") 
 ("publié" . "T"))

|#
;;;--------------------------------------------------------- OMAS::PARMS2OBJDESC
;;; starts with something like
;;; "news-item"
;;;  (("id number" . "LN-5") ("title" . "test 5") ("author" . "barthès, DupondJean")
;;;   ("category" . "Travel") ("indexes" . "London, New York") ("text" . "RAS")
;;;   ("creation date" . "19/01/2014"))
;;; should yield
;;;  (("id number" "LN-5")
;;;   ("title" "test 5") 
;;;   ("author" 
;;;      ("Person" ("name" "Barthès")("first-name" "Jean-Paul")("initials" "JPB"))
;;;      ("Person" ("name" "Dupond")("first-name" "Jean")))
;;;   ("category" ("Category" ("label" "Travel")))
;;;   ("indexes" ("Index" ("label" "London"))
;;;              ("Index" ("label" "New York")))
;;;   ("text" "RAS")
;;;   ("creation date" "19/01/2014"))  
;;; 
;;; The rational is that we can build this structure, using the page description 
;;; and the =parse-summary method. The different descriptions for the successors
;;; may correspond to existing objects or objects to create.
;;; Take the string "barthès": it corresponds to an existing object for which we
;;; can recover the description using the =make-object-description method

(defun omas::parm2objdesc (agent class-ref parms)
  "takes the set of pairs from the edit page and transform it into an object ~
   description that can be used to create the object and successors.
Argument:
   agent: current agent
   class-ref: class of the object to describe
   parms: a list of pairs correponding to the edit page fields
Return:
   an object description, e.g. (class-ref (att {value}+)(rel {<suc description>})))"
  (let* ((page-description 
          (get-field (moss::%string-norm class-ref) (omas::edit-layouts agent)))
         prop-ref path suc-id value-list pv-list val-result result field-type
         new-val)
    (unless page-description
      (error (format nil "~S: Can't find page description for class ~S" 
               (omas::name agent) class-ref)))
    
    ;; loop on the page list building the object description progressively
    (dolist (item parms)
      (dformat :web 1 "~%; parm2objdesc /item: ~S" item)
      ;; get first property corresponding to page field
      (setq prop-ref (omas::update-extract-prop-ref (car item) page-description))
      ;; get the type of value corresponding to the form field
      (setq field-type 
            (omas::update-extract-value-type (car item) page-description))
      (dformat :web 1 "~%; parm2objdesc /field-type ~S" field-type)
      (cond 
       ;; if attribute, add value
       ((moss::%is-attribute? prop-ref)
        (dformat :web 1 "~%; parm2objdesc ===== attribute =====")
        ;; must check for mln...
        ; if mln and not mln (cdr item)
        (setq new-val (if (and (eql field-type :mln)
                               (not (mln::mln? (cdr item))))
                          ;(moss::%make-mln-from-ref 
                          (mln::make-mln
                           ;; in case of several values must replace commas
                           (if (stringp (cdr item))
                               (substitute #\; #\, (cdr item))
                             ;; otherwise leave it alone
                             (cdr item)))
                        ;; if not :mln tpe or already mln, keep it as it is
                        (cdr item)))
        (dformat :web 1 "~%; parm2objdesc /new-val ~S" new-val)
        (setq result (alist-add result prop-ref new-val))
        (dformat :web 1 "~%; parm2objdesc /result: ~S" result)
        )
       ;; if relation, build an object description of the successor
       ((moss::%is-relation? prop-ref)
        (dformat :web 1 "~%; parm2objdesc ===== relation=====")
        (catch 
         :syntax-error
         ;; get successor class from page-description, e.g.
         ;;   ("partenaire" "organisation" "sigle")
         (setq path (omas::update-extract-edit-path (car item) page-description))
         (dformat :web 1 "~%; parm2objdesc /path:~%  ~S" path)
         ;; split-values
         (setq value-list (omas::update-split-val (cdr item)))
         
         (setq suc-id (moss::%%get-id-for-class (cadr path)))
         (dformat :web 1 "~%; parm2objdesc /suc-id: ~S" suc-id)
         
         (setq val-result nil)
         ;; "reverse sumarize" each value  
         (dolist (val value-list)
           ;; de-summarize value, should return a list of pairs: prop value
           ;; e.g. "Barthès: Jean-Paul" => (("nom" "Barthès")("prénom" "Jean-Paul"))
           ;; when =parse-summary is not defined the default method returns input value
           (setq pv-list (send suc-id '=parse-summary val))
           (dformat :web 1 "~%; parm2objdesc /pv-list: ~S" pv-list)
           (when (listp pv-list)
             (push (cons (cadr path) pv-list) val-result))
           (when (stringp pv-list)
             ;; check that property name is not NIL
             (unless (caddr path)
               (warn "bad clause format in page description: ~S. Either there is ~
                     a bad value for the property name or there should be a ~
                     =parse-summary method for the class~%"  path)
               (cg:beep)
               (throw :syntax-error nil))
             ;; make entry (<suc-ref> (<att> <val>))
             (push `(,(cadr path) (,(caddr path) ,pv-list)) val-result))
           )
         (dformat :web 1 "~%; parm2objdesc /val-result: ~S" val-result)
         ;; add suc-class-ref in front and add to description
         (setq result (alist-add-values result prop-ref (reverse val-result)))
         ))
        ;; forget anything else
       (t (format t "~%; parm2objdesc /unrecognized prop-ref: ~S" prop-ref))
         )
      )
    ;; add any post-function treatment?

    ;; return the object description
    result))

#|
(setq parms '(("id number" . "LN-5") ("title" . "test 5") 
              ("author" . "barthès:jean-paul, ramos")
   ("category" . "Travel") ("indexes" . "London, New York") ("text" . "RAS")
   ("creation date" . "19/01/2014")))
(omas::parm2objdesc sa_stevens-news "news-item" parms)
(("id number" "LN-5") ("title" "test 5")
 ("author"
  ("person" ("nom" "barthès") ("prénom" "jean-paul")) 
  ("person" ("nom" "ramos")))
 ("category" ("Category" ("label" "Travel")))
 ("indexes" 
  ("Index" ("label" "London")) 
  ("Index" ("label" "New York"))) 
 ("text" "RAS")
 ("creation date" "19/01/2014"))
|#
;;;-------------------------------------------------------- OMAS::START-WEB-SERVER
;;; Start the server

(defun omas::start-web-server (agent port)
  (declare (ignore agent)(special *omas*))
  
  ;; increase the number of agents using the server (from 0 to 1)
  (incf (omas::http-server-reference-count *omas*))
  ;; if Allegroserve is running and on the same port do nothing
  (when (and (omas::http-server *omas*)
             (eql (omas::http-server-port *omas*) port))
    (dformat :web 0
             "~%;*** AllegroServe already up and running on port ~S ***" port)
    (return-from omas::start-web-server))
  
  ;; if server is not running then start it and initialize OMAS globals
  ;; call create only if server is not running on specifyed port
  (unless (omas::http-server *omas*)
    ;; must activate allegroserve
    (eval-when (:compile-toplevel :execute)
      (require :aserve))
    ;; call allegroserve 
    (setf (omas::http-server *omas*)
      (net.aserve:start 
       :port port 
       :external-format (excl:crlf-base-ef :utf-8)))
    (setf (omas::http-server-port *omas*) port)
    (dformat :web 0
             "~%;*** AllegroServe started and running on port ~S ***" port)
    )
  
  ;; if server is running but not on the same port, then call start again to add
  ;; the port to the list of watched ports, but we do not change the reference
  ;; port for OMAS
  (unless (eql (omas::http-server-port *omas*) port)
    (setf (omas::http-server *omas*)
      (net.aserve:start 
       :port port 
       :external-format (excl:crlf-base-ef :utf-8))))
  t)

;;;--------------------------------------------------------- OMAS::STOP-WEB-SERVER
;;; Stop the server

(defun omas::stop-web-server ()
  (declare (special *omas*))
  ;; decrease the number of agents using the server (from 0 to 1)
  (decf (omas::http-server-reference-count *omas*))
  (when (<= (omas::http-server-reference-count *omas*) 0)
    (shutdown)
    ;; record that the server has been shut
    (setf (omas::http-server *omas*) nil)
    (setf (omas::http-server-port *omas*) nil)
    ))

;;;---------------------------------------------------------- OMAS::WEB-CLOSE-GATE

(defun omas::web-close-gate (nn)
  "close the specific gate and remove it from the gate list"
  (declare (special *web-gate-list))
  (let ((gate (cdr (assoc nn *web-gate-list*))))
    ;; if the gate has already been closed and removed, do nothing
    (when gate
      (mp:close-gate gate)
      (setf *web-gate-list*
        (remove nn *web-gate-list* :key #'car)))))

;;;--------------------------------------------------------- OMAS::WEB-CREATE-GATE

(defun omas::web-create-gate ()
  "create a new gate put it into the gate list, return its index (serial nb)"
  (declare (special *web-gate-counter* *web-gate-list*))
  (push (cons (incf *web-gate-counter*) (mp:make-gate nil))
        *web-gate-list*)
  ;; return is an integer
  *web-gate-counter*)

;;;------------------------------------------------------- OMAS::WEB-GET-GATE-DATA

(defun omas::web-get-gate-data (nn)
  "recovers gate from gate list using index"
  (declare (special *web-gate-list*))
  (cdr (assoc nn *web-gate-list*)))

;;;----------------------------------------------------------- OMAS::WEB-OPEN-GATE

(defun omas::web-open-gate (nn)
  "used to indicate that the work was completed"
  (declare (special *web-gate-list*))
  ;; only do that when gate exists
  (let ((gate (cdr (assoc nn *web-gate-list*))))
    (when gate
      (mp:open-gate gate))))

;;;---------------------------------------------------------- OMAS::WEB-POP-RESULT

(defun omas::web-pop-answer (nn)
  "return the result associated with gate nn if there"
  (declare (special *web-answer-list*))
  (let ((answer (cdr (assoc nn *web-answer-list*))))
    (setf *web-answer-list*
      (remove nn *web-answer-list* :key #'car))
    answer))

;;;--------------------------------------------------------- OMAS::WEB-PUSH-ANSWER

(defun omas::web-push-answer (nn result)
  (declare (special *web-answer-list*))
  (push (cons nn result) *web-answer-list*))




;;;===============================================================================
;;;===============================================================================
;;;
;;;                            WEB EDITOR
;;;
;;;===============================================================================
;;;===============================================================================

;;;===============================================================================
;;;                    globals and service functions
;;;===============================================================================

;;;--------------------------------------------------------------------- CHECK-MOD

(defun check-mod (parms old-values)
  "checks the list of parameters for possible modifications, i.e. pairs of parms ~
   \"XXX\" \"M-XXX\" which means that we want to modify the distant value."
  (let ((parms-copy parms)
        tag m-tag result)
    (dolist (item parms)
      ;; examine tag
      (setq tag (car item))
      ;; add M- in front of it
      (setq m-tag (concatenate 'string "M-" tag))
      ;; check if m-tag is part of the parms
      (when (get-field m-tag parms)
        ;; if so cook up a MOD command and save it
        (push `(,tag :MOD ,(cdr item)) result)
        ;; remove tag and m-tag from the copy of parms
        (setq parms-copy
              (remove tag parms-copy :key #'car :test #'string-equal))
        (setq parms-copy
              (remove m-tag parms-copy :key #'car :test #'string-equal))
        (setq old-values 
              (remove tag old-values :key #'car :test #'string-equal))
        )
      )
    ;; return two values
    (values (reverse result) parms-copy old-values)))

#|
(check-mod '(("pays" . "Japon")
             ("correspondant"
              . "Sugawara: Kenji, Fujita: Shigeru")
             ("partenaire" . "CIT")
             ("M-sigle partenaire" . "t")
             ("sigle partenaire" . "CIT")
             ("ville" . "Tsudanuma")
             ("correspondant UTC"
              . "Barthès: Jean-Paul, Moulin: Claude")
             ("type de contact" . "suivi")
             ("date de début" . "2000")
             ("action" . "commit")))
(("sigle partenaire" :MOD "CIT"))
(("pays" . "Japon") ("correspondant" . "Sugawara: Kenji, Fujita: Shigeru") ("partenaire" . "CIT")
 ("ville" . "Tsudanuma") ("correspondant UTC" . "Barthès: Jean-Paul, Moulin: Claude")
 ("type de contact" . "suivi") ("date de début" . "2000") ("action" . "commit"))
|#
;;;---------------------------------------------------------------- CHECK-PROPERTY

(defun check-property (pair new)
  "takes a pair and looks if there is a dotted pair with the same property in a ~
   list of pairs.
Arguments:
   pair: e.g. (\"partenaire\" . \"CIT\")
   new: an a-list of dotted pairs
Return:
   nil if the pair is in new and the values are identical
   a delete triple if the pair is not in new
   a delete and add triples if the pair is in new and values are different"
  (dformat :web 0 "~2%;---------- Entering check-property")
  ;(dformat :web 0 "~%;--- pair: ~S" pair)
  ;(dformat :web 0 "~%;--- new: ~S" new)
  ;; look if property is part of new list of pairs
  (let ((new-pair (assoc (car pair) new :test #'string-equal)))
    (cond
     ;; if there is no pair with the same property remove old values
     ((null new-pair)
      (mapcar #'(lambda(xx)`(,(car pair) :del ,xx))
           (omas::split-val (cdr pair))))
      ;; if there is a pair with the same value do nothing
     ;; won't work for several values in different order (not a problem)
     ((equal+ (cdr pair)(cdr new-pair)) 
      nil)
     ;; if there is a similar pair, add new value remove old one
     (t
      (append
       ;; delete all elements that are in pair but no longer in new-pair
       (mapcar #'(lambda(xx)`(,(car pair) :del ,xx))
         (set-difference (omas::split-val (cdr pair)) 
                         (omas::split-val (cdr new-pair))
                         :test #'equal+))
       ;; add elements that are in new-pair but were not in pair
       (mapcar #'(lambda(xx)`(,(car pair) :add ,xx))
         (set-difference (omas::split-val (cdr new-pair)) 
                         (omas::split-val (cdr pair))
                         :test #'equal+))
       )))))

#|
(check-property '("a" . "aa") '(("b" . "bb")("c" . "cc")))
(("a" :DEL "aa"))

(check-property '("b" . "bb") '(("a" . "aa")("b" . "bb")("c" . "cc")))
NIL

(check-property '("b" . "zz") '(("a" . "aa")("b" . "bb")("c" . "cc")))
(("b" :DEL "zz") ("b" :ADD "bb"))

(check-property '("a" . "aa1,aa2") NIL)
(("a" :DEL "aa1") ("a" :DEL "aa2"))

(check-property '("b" . "zz, bz") '(("a" . "aa")("b" . "bb, zz, bc")("c" . "cc")))
(("b" :DEL "bz") ("b" :ADD "bc") ("b" :ADD "bb"))

(check-property '("b" . "zz, bz") '(("a" . "aa")("b" . "bz, zz")("c" . "cc")))
NIL
|#	
;;;G-------------------------------------------------------------------- DATA-DIFF

(defun data-diff (old new)
  "takes 2 alists and extract the differences "
  (unless (and (listp old)(listp new))
    (error "both arguments should be lists: ~%old: ~S~%new: ~S" old new))
  (let (result)
    (setq new (remove "action" new :test #'string-equal :key #'car))
    (loop
      (unless old (return))
      (unless (string-equal "id" (caar old))
        (setq result (append result (check-property (car old) new))))
      ;; remove pair from new
      (setq new (remove (caar old) new :test #'string-equal :key #'car))
      (if (get-field (caar old) new) (pop new))
      (pop old))
    ;; check if something left in new
    (dolist (item new)
      (setq result (append result `((,(car item) :add ,(cdr item))))))
    result))

#|
(data-diff '(("a" . "aa") ("c" . "cc")("d" . "dd,ee"))
           '(("a" . "zz")("b" . "bb") ("d" . "dd,ee,ff")))
(("a" :DEL "aa") ("a" :ADD "zz") ("c" :DEL "cc") ("b" :ADD "bb"))
(data-diff '(("keyword" . "handball"))'(("keyword" . "handball, football")))
(("keyword" :ADD "football"))
|#
;;;----------------------------------------------------------- INCLUDE-JAVASCRIPTS
;;; pattern has the following format:

(defun include-javascripts (pattern)
  "examines the page description to extract field description starting with :js ~
   and makes a list of the corresponding javascripts.
Argument:
   pattern: the page description for the class of the object-to-edit
Return:
   a list of all the javascripts present in pattern"
  (remove nil
          (mapcar #'(lambda(xx) (if (eql (car xx) :js) 
                                    (format nil "
<SCRIPT>
~A
</SCRIPT>
" 
                                      (getf (cdr xx) :code))))
            (cdr pattern)))
  )

#|
(setq pattern 
      '("CSCWD CONCEPT"
        (:row :header "CSCWD English Concept Name (required)" :name "LABEL"
         :property "LABEL"
         :cols "60%" 
         :format-value select-english-tag)
   (:area :header "English Definition" :name "DEFINITION" :rows 4 :cols "60%"
          :format-value select-english-tag)
   (:row :header "Subsuming Concept" :name "subsuming concept" 
         :path ("subsuming concept" "Concept")
         :if-does-not-exist :ignore :selfcn select-concept :selprop "label")
   (:area :header "Author (read only)" 
          :name "author" :rows 1 :cols "60%" :read-only t
          :path ("author" "Person") :initfcn create-author)
   ;; source is an MLN with a pointer for each language. The link option created a
   ;; pointer in the first second column of the table (should use javascript)
   (:row :header "Source" :name "source" :format-value select-english-source
         :link t)
   (:row :header "Creation Date (read only)" :name "Creation date" :cols "60%"
         :read-only t :initfcn create-date)
   (:row :header "Modification Date (read only)" :name "Modification date" 
         :cols "60%" :read-only t)
   ;;---
   ;; specifying a language different from the PA language sets the *foreign-language*
   ;; variable, used to post data into the following fields. However, refreshing 
   ;; should be done with javascript (?). Possibly it is enough to blank the 
   ;; various fields and issue a message.
   (:js :code "function clearFields() {
	     alert(\"coucou\");
             document.getElementByName(\"FOREIGN-LABEL\").value = \"\";
             document.getElementByName(\"FOREIGN-DEFINITION\").value = \"\";
             document.getElementById(\"fsource\").value = \"\";
	     }")
   (:row :header "Current language" :selfcn select-language :name "foreign-language"
         :selected (symbol-name *language*) :actfcn set-foreign-language
         :on-change "clearFields();")
   (:row :header "CSCWD Concept Name if not English" :name "FOREIGN-LABEL"
         :property "LABEL"
         :cols "60%" 
         :format-value select-current-tag)
   (:area :header "Definition if not English" :name "FOREIGN-DEFINITION" :rows 4 
          :cols "60%" :property "DEFINITION" :format-value select-current-tag)
   ;; we must not include :type (crashes on nil values)
   (:row :header "Source if not English" :name "source" :id "fsource"
         :format-value select-current-source :link t)
   ;;---
   (:user-action :header "View annotations..." 
                 :name "show annotations" :actfcn "annotations")
   (:user-action :header "Add English annotations..." 
                 :name "add-English-annotation" :actfcn "add-english-annotation")
   (:user-action :header "Add non-English annotations..." 
                 :name "add-foreign-annotation" :actfcn "add-foreign-annotation")
   (:user-action :header "Refresh page..."  
                 :name "refresh-page" :actfcn nil) ; nil for current page
        ))

(include-javascripts pattern)
("<script>function clearFields() {
	     //alert(\"coucou\");
             document.getElementByName(\"FOREIGN-LABEL\").value = \"\";
             document.getElementByName(\"FOREIGN-DEFINITION\").value = \"\";
             document.getElementById(\"fsource\").value = \"\";
	     }</script>")
|#
;;;-------------------------------------------------------------------- MAKE-FIELD

(defun make-field (to-agent-key row-pattern object-to-edit class-ref ip)
  "build a row of the table in the edit page.
Arguments:
   to-agent-key: e.g. :CONTACT
   row-pattern: a list describing the row
   object-to-edit: the object to display
   class-ref: ref of the class of the object-to-edit
   ip: index on the user-entries (aka cookie)
Return:
   a list to be inserted into the html macro"
  ;; pattern will contain a list of keyword/value
  (declare (special *language*))
  (let* ((pattern (cdr row-pattern))
         (refresh (wu-get ip "refresh"))
         (language (wu-get ip "language"))
         )
    
    (dformat :web 0 "~2%; make-field /refresh: ~S" refresh)
    (omas::set-language language)
    (dformat :web 0 "~%; make-field /*language*: ~S" *language*)
    
    (cond
     (refresh
      (case (car row-pattern)
        (:row
         (apply #'make-field-row-refresh
                :to-agent-key to-agent-key :object-to-edit object-to-edit 
                :class-ref class-ref :ip ip pattern)
         )
        (:area
         (apply #'make-field-area-refresh
                :to-agent-key to-agent-key :object-to-edit object-to-edit
                :class-ref class-ref :ip ip pattern)
         )
        )
      )
     
     (t	
      (case (car row-pattern)
        (:row
         (apply #'make-field-row 
                :to-agent-key to-agent-key :object-to-edit object-to-edit 
                :class-ref class-ref :ip ip pattern)
         )
        (:area
         (apply #'make-field-area 
                :to-agent-key to-agent-key :object-to-edit object-to-edit
                :class-ref class-ref :ip ip pattern)
         ))
      ))))
  
 #|
(make-field :contact (car pattern) object-to-edit ip)
(:TR (:TD "Nom du pays, ex: Italie*")
     (:PRINC "<a href=select?agent=:CONTACT&data=((\"Japon\"%20\"pays\"%20\"$E-COUNTRY.89\"))>Edit</a>")
     (:TD ((:INPUT :NAME "pays" :VALUE (OR # "") :MAXLENGTH 100))))

(make-field :contact (cadr pattern) object-to-edit ip)
(:TR (:TD "Correspondant(s) UTC, ex: barthès, Moukrim*")
     (:TD
      (:PRINC "<a href=select?agent=:CONTACT&data=((\"Barthès:%20Jean-Paul\"%20\"personne\"%20\"$E-PERSON.3\")
%20(\"Moulin:%20Claude\"%20\"personne\"%20\"$E-PERSON.27\"))>Edit</a>"))
     (:TD
      (:PRINC
       (FORMAT NIL "<textarea name=~S rows=~S cols=~S>~A</textarea>" "correspondant UTC" 1 "60%" (OR # ""))
       )))
|#
;;;--------------------------------------------------------------- MAKE-FIELD-AREA
;;; Should create a value-handle option to replace make-value by a user-defined
;;; function. The rationale for that is when we want to add a new name to a list
;;; of editors of a concept when modifying this concept. We need a function that
;;; will read the existing names and add the name from the user entry.

(defun make-field-area (&key to-agent-key header name rows cols edit object-to-edit 
                      class-ref max path type read-only list-fcn format-value
                      property id ip initfcn)
  "builds a multiline area of the table in the edit page
Arguments:
   to-agent (key): agent to which the value belongs
   header (key): to be printed at the beginning of the line
   ip (key): index to the user entry
   name (key): name of the property to get the associated value
   rows (key); number of rows to assign to the field
   cols (key): percentage taken by the column
   edit (key): if true show a button for editing
   id (key): corresponds to the HTML ID field
   format-value (key): function to format the value
   object-to-edit (key): an alist containing the object values
   property (key): property associated with the field when diffrent from name 
   ref (key): id of the object to edit
   class-ref (key): class of the object to edit
   list-fcn (key): makes the header clickable (popup-name field-name)
   max (key): maximum number of values
   type (key): can be :mln
   read-only (key): if true will be a read-only field
   ---
   initfcn (key): unused
Return:
   a list to be used in the html macro"
  (declare (ignore path type max ip initfcn))
  
  (if list-fcn
      (let* ((popup-name (car list-fcn))
             (field-name (cadr list-fcn))
            )
        ;; recursive call that does not contain list-fcn option
        ;; var-name is the name of the second arg of the provide function
        (make-field-area :header (string+ "<A HREF=\"" popup-name "\" 
   onClick=\"return popup(this,'" field-name "')\">" header "</A>")
                  :object-to-edit object-to-edit
                  ;; we post existing values if any
                  ;:value (car (get-field name object-to-edit))
                  :name name ; name of the parms field
                  :rows 3
                  :format-value format-value
                  :cols "60%"))
    
    ;; otherwise do as usual
    (format nil "<TR>
  <TD>~A</TD>
  <TD>~A</TD>
  <TD><textarea ~A name=~S rows=~S cols=~S~A>~A</textarea></TD>
 </TR>"
      header
      (if edit (make-http-checkbox to-agent-key name object-to-edit class-ref) "")
      (if read-only  "STYLE=\"color: #FF0000\"" "")
      name
      rows
      cols
      (if id (format nil " ID=~S" id) "")
      (make-value (or property name) object-to-edit format-value)
      )))
    
#|
(let (object-to-edit)
  (setq object-to-edit '(("id" "$E-NEWS-ITEM.4") ("id number" "N-4")
                        ("title" "OMAS Release 8.1.3")
                        ("author" ("Barthès : Jean-Paul" "author" "$E-PERSON.1"))
                        ("category" ("OMAS/MOSS" "category" "$E-CATEGORY.4"))
                        ("keywords" ("OMAS" "keywords" "$E-KEYWORD.11"))
                        ("text" "Version 8.1.3 is being prepared.")
                        ("creation date" 3595705200) ("modification date") ("archived")
                        ("published")))
  (make-field-area :header "Text" :object-to-edit object-to-edit :name "text" :rows 3
            :cols "60%"))

"<TR>
  <TD>\"Text\"</TD>
  <TD></TD>
  <TD><textarea name=\"text\" rows=3 cols=\"60%\">Version 8.1.3 is being prepared.</textarea><TD>
 </TR>"

(let (object-to-edit)
  (setq object-to-edit '(("id" "$E-CATEGORY.5")
                        ("label" "Key Figures")
                        ("libellé" "Personnalités")))
  (make-field-area :header "Libellé (sans espace)" :object-to-edit object-to-edit :name "libellé" :rows 3
            :cols "60%" :list-fcn '("categories?val=retour" "libellé" "category")))
"<TR>
  <TD><A HREF=\"categories\" 
   onClick=\"return popup(this,'category')\">Libellé (sans espace)</A></TD>
  <TD></TD>
  <TD><textarea  name=\"category\" rows=3 cols=\"60%\"></textarea></TD>
 </TR>"
|#
;;;------------------------------------------------------- MAKE-FIELD-AREA-REFRESH

(defun make-field-area-refresh (&rest ll 
                                      &key header name rows cols read-only id ip
                                      to-agent-key object-to-edit property
                                      format-value class-ref path initfcn)
  "builds a multiline area of the table in the edit page. When refreshing the ~
   edit page the value to be posted in the field is that which was just read ~
   from the current page, even if it is blank.
Arguments:
   format-value (key):
   header (key): to be printed at the beginning of the line
   ip (key): index to the user entry
   name (key): name of the property to get the associated value
   object-to-edit (key): description of the object to edit
   rows (key); number of rows to assign to the field
   cols (key): percentage taken by the column
   id (key): corresponds to the HTML ID field
   property (key):
   read-only (key): if true will be a read-only field
   ---
   class-ref (key): ignored
   initfcn (key): ignored
   path (key): ignored
   to-agent-key (key): ignored
Return:
   a list to be used in the html macro"
  (declare (ignore to-agent-key class-ref path initfcn))
  (dformat :web 0 "~2%;---------- Entering make-field-area-refresh")
  ;; if there is a parm value of the user entry associated with name, then use it
  (let* ((val (wu-get ip name)))
    (if (equal+ val "")
      (setq val (make-value (or property name) object-to-edit format-value)))
    (dformat :web 0 "~%;--- name: ~S" name)
    (dformat :web 0 "~%;--- val: ~S" val)
    ;; set val to a blank value in case the "name" entry was wiped out by a norm
    ;; action like remove-empty-parameters
    (setq val (or val ""))
    (format nil "
 <TR>
  <TD>~A</TD>
  <TD></TD>
  <TD><textarea ~A name=~S rows=~S cols=~S~A>~A</textarea></TD>
 </TR>"
      header
      (if read-only  "STYLE=\"color: #FF0000\"" "")
      name
      rows
      cols
      (if id (format nil " ID=~S" id) "")
      val
      )
    ))

#|
(OW::MAKE-FIELD-AREA-REFRESH :IP 3232235877
             :header "Author (read only)" 
          :name "author" :rows 1 :cols "60%" :read-only t
          :path '("author" "Person") :initfcn 'create-author)
"
 <TR>
  <TD>Author (read only)</TD>
  <TD></TD>
  <TD><textarea STYLE=\"color: #FF0000\" name=\"author\" rows=1 cols=\"60%\">
                Barthès : Jean-Paul
      </textarea></TD>
 </TR>"
|#
;;;--------------------------------------------------------------- MAKE-FIELD-LINK
;;; the function returns an HTML string that produced a submit button that will
;;; call a new page if the actfcn option is specified (corresponding to a 
;;; formaction option of the submit description). Note that the formaction option
;;; is an HTML5 option and will not work with older browsers

(defun make-field-link (&key to-agent-key object-to-edit class-ref name actfcn ip
                             &allow-other-keys)
  "builds an HTML string so that when the form is submitted, the formaction option ~
   specifies the URL of the file that will process the input control.
Arguments:
   actfcn (key): function that will be executed to determine the URL of the page
                 to be called on submit action (works only for HTML)
   class-ref (key): ref of the object class
   ip (key): index in the agent user structure (works as a cookie)
   object-to-edit (key): description of the object being posted in the web page
   to-agent-key (key): key of the agent owning the object
Return:
   a string describing the button to be posted in the input form."
  (declare (ignore to-agent-key class-ref object-to-edit))
  (let* ()
    (cond
     ;; here we post a different web page
     (actfcn
      ;(format nil "<input type=\"submit\" value=~S formaction=~S>..."
      ;  name (funcall actfcn ip)))
      (format nil 
          "
  <input type=\"hidden\" name=\"ip\" value=~S />
  <button type=\"submit\" formaction=~S>~A</button>
"
        ip
        (funcall actfcn ip) ; should return the HTTP to transfer to
        name)
        )
     ;; here we refresh page with the new field values
     (t
      (format nil "<input type=\"submit\" value=~S>..." name)
      )
     )
    ))

#|  
(make-field-link :value "view annotations" :actfcn "annotations")
"<input type=\"submit\" value=\"view annotations\" formaction=\"annotations\">..."

(make-field-link :value "refresh page")
"<input type=\"submit\" value=\"refresh page\">..."
|#    
;;;---------------------------------------------------------------- MAKE-FIELD-ROW
;;; add an option :getfcn to apply to the object description to post a value

(defun make-field-row (&key to-agent-key header name edit object-to-edit path
                            class-ref read-only type (size "75%") format-value
                            list-fcn select selected property value link language
                            on-change id multiple ip cols if-does-not-exist
                            actfcn initfcn sel-prop value-handle)
  "builds a line for the html line.
Arguments:
   format-value (key): function to apply to values associated to a property
   header(key): expression to print as a header for the corresponding value
   id (key): corresponds to HTML ID tag
   ip (key): index to the user entry
   language (key): specifies the default language for the value
   list-fcn (key): used to produce pop up windows (<popup name><field>)
   link (key): uses the value if http pointer to set up access to the source page
   multiple (key): allows multiple selections in a select clause   
   name (key): parameter for HTTP tranfers (parms)
   object-to-edit (key): a description of the object to edit
   on-change (key): a call to a javascript function to execute on change
   property (key): property associated with the field when diffrent from name 
   read-only (key): if there, makes the field read-only
   select (key): if there, builds a selection list using the list of values from ~
                 the page-description
   selected (key): evaluates to a value that should be selected
   to-agent-key (key): the key of the agent owning the object to display
   value (key): specifies a value to be evaluated, e.g. independent from object
   value-handle (key): specifies a function to apply to compute the value(s)
   ---
   actfcn (key): ignored
   class-ref (key): ignored
   cols (key): ignored
   edit (key): in relation specifies a path to get the value(s) (obsolete)
   if-does-not-exist (key): ignored
   initfcn (key): ignored
   path (key): ignored
   sel-prop (key): ignored
   type (key): qualifies the type of value, e.g. :mln, ignored
Return:
   a list specifying a row in the edit page table for the html macro"
  (declare (ignore path edit class-ref type sel-prop
                   cols if-does-not-exist initfcn actfcn))
  (dformat :web 0 "~2%;---------- ~A: Entering make-field-row" to-agent-key)
  (dformat :web 0 "~%;--- name: ~S" name)
  (dformat :web 0 "~%;--- property: ~S" property)
  (dformat :web 0 "~%;--- format-value: ~S" format-value)
  (dformat :web 0 "~%;--- value-handle: ~S" value-handle)
  (dformat :web 0 "~%;--- object-to-edit: ~S" object-to-edit)
  
  (let (val menu-list new-select)
    
    ;; set language to make sure (if language is nil, will not modify *language*)
    ;; normally *language* has been set in the calling function (building the edit
    ;; page) however we could have a :language option in the field description
    (set-language language)
    
    ;; check if we have a value-handle, i.e. a function to execute to get the value(s)
    ;; compute the value to post
    (setq val 
          (if value-handle
              (funcall value-handle (or property name) object-to-edit format-value)
            (make-value (or property name) object-to-edit format-value)))
    
    ;; make-value always returns a string, which is not convenient
    (setq val (if (equal+ val "") nil val))
    (dformat :web 0 "~%;--- val: ~S" val)
    
    ;; now if select is a series of MLN values, we must make it a list of strings
    (when (and select 
               (every #'(lambda(xx) (or (mln::mln? xx)(mln::%mln? xx))) select))
        (setq new-select (mapcar #'(lambda(xx)(mln::extract xx :always t)) select))
      (setq select (sort (reduce #'append new-select) #'string-lessp))
        )
    (dformat :web 0 "~%;--- select: ~S" select)
    
    (cond
     ;;=== here we want a popup window...
     (list-fcn
      (let* ((popup-name (car list-fcn))
             (field-name (cadr list-fcn))
             )
        ;; recursive call that does not contain list-fcn option
        ;; var-name is the name of the second arg of the provide function
        (make-field-row :header (string+ "<A HREF=\"" popup-name "\" 
   onClick=\"return popup(this,'" field-name "')\">" header "</A>")
                        :object-to-edit object-to-edit
                        ;; we post existing values if any
                        ;:value (car (get-field name object-to-edit))
                        :name name ; name of the parms field
                        :id id
                        :ip ip
                        :property property
                        :format-value format-value
                        )))
     
     ;; if we have an HTTP link, produce the right string
     ;; e.g. :header "test" :name "Barthès" :value "http://www.utc.fr/~barthes    
     ;;=== here we have a value from the object to edit, but no select list
     ((null select)
      ;; format value properly
      ;(setq val (make-value (or property name) object-to-edit format-value))
      (format nil "<TR> 
  <TD>~A</TD>
  <TD>~A</TD>
  <TD><INPUT ~A NAME=~S VALUE=~S SIZE=~S~A ~A></TD>
 </TR>"
        header
        (if (and link val (not (equal val "")))
            (format nil "<A HREF=~S>REF</A>" val) " ")
        (if read-only  "STYLE=\"color: #FF0000\"" "")
        name
        (or (eval value) (or val "")) ; if val is nil, must replace it with ""
        size
        (if id (format nil " ID=~S" id) "")
        (if read-only " READONLY" "")
        ))
     
     ;;=== here we have a value from the object to edit and a select option
     ;; because the select mechanism from the browser does not allow to have spaces
     ;; in the values posted in the menu, we replace spaces with underscores, the
     ;; time to display and select values
     ((and val select)
      ;; we must build a pull down list and show the selected value
      ;(break "val: ~S" val)
      (cond
       ;; if the property is an attribute, then val is a string
       ((stringp val) (setq val (omas::split-val val #\,)))
       ;; if the property is an attribute and the value is an MLN, process the MLN
       ;; the :always option is used when *language* is not represented in the value
       ((or (mln::MLN? val)(mln::%mln? val))
        (setq val (mln::extract val :always t)))
       ;; if the property is a relation and the value is an MLN, e.g.
       ;;  (((:en "Administration, Admin")(:fr "Administration"))
       ;;      "subsuming concept" "$E-CSCWD-CONCEPT.4")
       ((and (listp val)(or (mln::mln? (car val))(mln::%mln? (car val))))
        (setq val (mln::extract (car val) :always t)))
       ;; if the property is a relation then val is a list, e.g.
       ;;  ("Administration, Admin" "subsuming concept" "$E-CSCWD-CONCEPT.4")
       ((listp val)(setq val (omas::split-val (car val) #\,)))
       )
      (dformat :web 0 "~%;--- val for selection: ~S" val)
      (dformat :web 0 "~%;--- select: ~S" select)
      (setq menu-list 
            (mapcar #'(lambda(xx) 
                        (format nil "<option value=~A~A>~A</option>" 
                          (if (equal+ xx " ") " " (substitute #\_ #\space xx))
                          (if (member xx val :test #'equal+) " selected" "") 
                          (if (equal+ xx " ") " " (substitute #\_ #\space xx))))
              select))
      
      (dformat :web 1 "~%; make-field-row /menu-list: ~S" menu-list)
      (format nil "
 <TR> 
  <TD>~A</TD>
  <TD></TD>
  <TD><select name=~S size=1~A~A> ~{~A ~} </select></TD>
 </TR>"
        header
        name
        (if on-change (format nil " onChange=~S" on-change) "")
        (if multiple " multiple" "")
        menu-list
        )
      )
     
     ;;=== here we have no value but a select option to choose a value from
     (t
      ;; check if there is a (static) selected option
      (setq val (eval selected))
      (dformat :web 1 "~%; make-field-row /Static selected option, val: ~S" val)
      
      (setq menu-list 
            (mapcar #'(lambda(xx) 
                        (format nil "<option value=~A~A>~A</option>" 
                          (if (equal+ xx " ") " " (substitute #\_ #\space xx))
                          (if (equal+ xx val) " selected" "")
                          (if (equal+ xx " ") " " (substitute #\_ #\space xx))))
              select))
      (dformat :web 1 "~%; make-field-row /menu-list: ~S" menu-list)
      (format nil "<TR> 
  <TD>~A</TD>
  <TD></TD>
  <TD><select name=~S size=1~A~A> ~{~A ~} </select></TD>
 </TR>"
        header
        name
        (if on-change (format nil " onChange=~S" on-change) "")
        (if multiple " multiple" "")
        menu-list
        )))))

#|
(let ((object-to-edit '(("id" "$E-NEWS-ITEM.4") 
                        ("id number" "N-4")
                        ("title" "OMAS Release 8.1.3")
                        ("author" ("Barthès : Jean-Paul" "author" "$E-PERSON.1"))
                        ("category" ("OMAS/MOSS" "category" "$E-CATEGORY.4"))
                        ("keywords" ("OMAS" "keywords" "$E-KEYWORD.11"))
                        ("text" "Version 8.1.3 is being prepared.")
                        ("creation date" 3595705200) 
                        ("modification date")
                        ("archived")
                        ("published"))))
  (ow::make-field-row :to-agent-key :STEVENS-NEWS 
           :header "keywords" 
           :name "keywords" 
           :object-to-edit object-to-edit
           :read-only t))
"<TR> 
  <TD>keywords</TD>
  <TD> </TD>
  <TD><INPUT STYLE=\"color: #FF0000\" NAME=\"keywords\" VALUE=\"OMAS\" SIZE=\"75%\"
             READONLY></TD>
 </TR>"

(let ((object-to-edit '(("id" "$E-NEWS-ITEM.4") 
                        ("id number" "N-4")
                        ("title" "OMAS Release 8.1.3")
                        ("author" ("Barthès : Jean-Paul" "author" "$E-PERSON.1"))
                        ("category" ("OMAS/MOSS" "category" "$E-CATEGORY.4"))
                        ("keywords" ("OMAS" "keywords" "$E-KEYWORD.11"))
                        ("text" "Version 8.1.3 is being prepared.")
                        ("creation date" 3595705200) ("modification date") ("archived")
                        ("published"))))
  (ow::make-field-row :to-agent-key :STEVENS-NEWS 
           :header "category" 
           :name "category" 
           :object-to-edit object-to-edit
           :select '("Sports" "Travel" "OMAS/MOSS")))
"<TR> 
  <TD>category</TD>
  <TD></TD>
  <TD><select name=\"category\" size=1> 
         <option value=Sports>Sports</option>
         <option value=Travel>Travel</option>
         <option value=OMAS/MOSS>OMAS/MOSS</option>
      </select></TD>
 </TR>"

(let ((object-to-edit '(("id" "$E-NEWS-ITEM.4") 
                        ("id number" "N-4")
                        ("title" "OMAS Release 8.1.3")
                        ("author" ("Barthès : Jean-Paul" "author" "$E-PERSON.1"))
                        ("category" ("OMAS/MOSS" "category" "$E-CATEGORY.4"))
                        ("keywords" ("OMAS" "keywords" "$E-KEYWORD.11"))
                        ("text" "Version 8.1.3 is being prepared.")
                        ("creation date" 3595705200) ("modification date") ("archived")
                        ("published")))
      (*language* :EN))
  (ow::make-field-row :to-agent-key :STEVENS-NEWS 
           :header "category" 
           :name "category" 
           :object-to-edit object-to-edit
                      :select '(((:en "Sports" "Games")) 
                                (:en "Travel;Trip" :fr "Voyages") ; mixture of MLNs
                                ((:en "OMAS/MOSS")))))
"
 <TR> 
  <TD>category</TD>
  <TD></TD>
  <TD><select name=\"category\" size=1>
        <option value=Games>Games</option>
        <option value=OMAS/MOSS selected>OMAS/MOSS</option>
        <option value=Sports>Sports</option>
        <option value=Travel>Travel</option>
        <option value=Trip>Trip</option>
      </select></TD>
 </TR>"

(let ((object-to-edit '(("id" "$E-NEWS-ITEM.4") ("id number" "N-4")
                        ("creation date" 3595705200) ("modification date") 
                        ("archived")("published"))))
  (ow::make-field-row :to-agent-key :STEVENS-NEWS 
           :header "category" 
           :name "category" 
           :object-to-edit object-to-edit
           :select '("Sports" "Key Figures")))
"<TR> 
  <TD>category</TD>
  <TD></TD>
  <TD><select name=\"category\" size=1> 
        <option value=Sports>Sports</option> 
        <option value=Key_Figures>Key_Figures</option>
      </select></TD>
 </TR>"

OW(42): (ow::make-field-row :header "Foreign Language" :name "foreign-language" 
                            :select '(" " "EN" "FR")
                     :selected (symbol-name *language*))
"<TR> 
  <TD>Foreign Language</TD>
  <TD></TD>
  <TD><select name=\"foreign-language\" size=1> <option value= > </option> 
   <option value=EN>EN</option> <option value=FR selected>FR</option>
   </select></TD>
 </TR>"
|#  
;;;-------------------------------------------------------- MAKE-FIELD-ROW-REFRESH

;;; (apply #'make-field-row-refresh
;;;                :to-agent-key to-agent-key :object-to-edit object-to-edit 
;;;                :class-ref class-ref :ip ip pattern)


(defun make-field-row-refresh (&rest ll 
                                     &key to-agent-key object-to-edit class-ref 
                                     property header name on-change multiple link 
                                     (size "75%") id read-only select ip initfcn
                                     format-value cols path if-does-not-exist
                                     selected actfcn sel-prop list-fcn type
                                     value-handle)
  "builds a line for the html line, taking the value from the page that is posted.
Arguments:
   format-value (key): function to apply to values associated to a property
   header(key): expression to print as a header for the corresponding value
   id (key): corresponds to HTML ID tag
   ip (key): index to the user entry
   link (key): uses the value if http pointer to set up access to the source page
   multiple (key): allows multiple selections in a select clause   
   name (key): parameter for HTTP tranfers (parms)
   on-change (key): a call to a javascript function to execute on change 
   read-only (key): if there, makes the field read-only
   select (key): if there, builds a selection list using the list of values from ~
                 the page-description
   value-handle (key): function to compute values directly
   ---
   actfcn (key): unused
   class-ref (key): unused
   cols (key): unused
   if-does-not-exist (key): unused
   initfcn (key): unused
   list-fcn (key): unused
   path (key): unused
   selected (key): unused
   sel-prop (key): unused
   type (key): unused
Return:
   a list specifying a row in the edit page table for the html macro"
  (declare (ignore to-agent-key class-ref cols actfcn class-ref path type
                   if-does-not-exist initfcn selected sel-prop list-fcn value-handle))
  (dformat :web 0 "~2%;---------- Entering make-field-row-refresh")
  (let ((val (or (wu-get ip name) "")) ; value coming from the current posted page
        menu-list new-select)
    
    (if (equal+ val "")
      (setq val (make-value (or property name) object-to-edit format-value)))
    
    (dformat :web 0 "~%;--- name: ~S" name)
    (dformat :web 0 "~%;--- val: ~S" val)
    
    ;; now if select is a series of MLN values, make it into a list of strings
    (when (and select 
               (every #'(lambda(xx) (or (mln::mln? xx)(mln::%mln? xx))) select))
      (setq new-select (mapcar #'(lambda(xx)(mln::extract xx :always t)) select))
      (setq select (sort (reduce #'append new-select) #'string-lessp))
      )
    ;; insert underscores for building the pop down menu
    (setq select (mapcar #'(lambda (xx) (substitute #\_ #\space xx)) select))
    (dformat :web 0 "~%;--- select: ~S" select)
    
    ;; distinguish between selection pull-down menus and other cases
    (cond
     ;;=== pull down menu option
     ;; we recompute the pull down menu with the selected value
     (select
      (setq val (omas::split-val val #\,))
      (setq val (mapcar #'(lambda(xx)(substitute #\_ #\space xx)) val))
      (dformat :web 0 "~%;--- val for selection: ~S" val)
      
      (setq menu-list 
            (mapcar #'(lambda(xx) 
                        (format nil "<option value=~A~A>~A</option>" 
                          xx
                          (if (member xx val :test #'equal+) " selected" "") 
                          xx))
              select))
      (dformat :web 1 "~%; make-field-row /menu-list: ~S" menu-list)
      ;; here we normally have no link nor read-only option
      (format nil "
 <TR> 
  <TD>~A</TD>
  <TD></TD>
  <TD><select name=~S size=1~A~A> ~{~A ~} </select></TD>
 </TR>"
        header
        name
        (if on-change (format nil " onChange=~S" on-change) "")
        (if multiple " multiple" "")
        menu-list
        )
      )
     
     ;;=== otherwise we make a standard output
     (t
      ;; format value properly
      (format nil "
 <TR> 
  <TD>~A</TD>
  <TD>~A</TD>
  <TD><INPUT ~A NAME=~S VALUE=~S SIZE=~S~A ~A></TD>
 </TR>"
        header
        (if link (format nil "<A HREF=~S>REF</A>" val) "")
        (if read-only  "STYLE=\"color: #FF0000\"" "")
        name
        val
        size
        (if id (format nil " ID=~S" id) "")
        (if read-only " READONLY" "")
        ))
     )))
  
#|
(ow::make-field-row-refresh :ip 3232235877 
                        :header "CSCWD English Concept Name (required)" 
                        :name "label"
                        :property "label"
                        :cols "60%" 
                        :format-value select-english-tag)
"
 <TR> 
  <TD>CSCWD English Concept Name (required)</TD>
  <TD> </TD>
  <TD><INPUT  NAME=\"label\" VALUE=\"Computer-Supported Cooperative Work, CSCW\"
       SIZE=NIL ></TD>
 </TR>"
(ow::make-field-row-refresh :ip 3232235877 
                                       :SELECT
     '("" "Administration" "Aerospace" "Agent" "Application"
      "Architecture" "Automotive Industry" "Big Data")
                        :HEADER "Subsuming Concept" :NAME "subsuming concept" :PATH
     '("subsuming concept" "Concept") :SELFCN
     'CSCWD-ONTO::SELECT-CONCEPT :SELPROP "label")
"
 <TR> 
  <TD>Subsuming Concept</TD>
  <TD></TD>
  <TD><select name=\"subsuming concept\" size=1> 
       <option value= selected></option> 
       <option value=Administration>Administration</option>
       <option value=Aerospace>Aerospace</option>
       <option value=Agent>Agent</option> 
       <option value=Application>Application</option>
       <option value=Architecture>Architecture</option>
       <option value=Automotive_Industry>Automotive_Industry</option>
       <option value=Big_Data>Big_Data</option>
     </select></TD>
 </TR>"

(OW::MAKE-FIELD-ROW-REFRESH :IP 3232235877
            :header "Source (URL)" :name "source" :format-value 'select-english-source
         :link t)
 <TR> 
  <TD>Source (URL)</TD>
  <TD><A HREF=\"http://en.wikipedia.org/wiki/Computer-supported_cooperative_work\">
       REF</A></TD>
  <TD><INPUT  NAME=\"source\" 
     VALUE=\"http://en.wikipedia.org/wiki/Computer-supported_cooperative_work\" 
     SIZE=\"75%\" ></TD>
 </TR>"

(OW::MAKE-FIELD-ROW-REFRESH :IP 3232235877
            :select '(" " "BR" "EN" "FR" "JP" "ZH")
            :header "2nd language (if changed, please refresh page)" 
         :name "language"
         :selected (symbol-name *language*) :actfcn 'set-foreign-language
         :on-change "clearFields();")
"
 <TR> 
  <TD>2nd language (if changed, please refresh page)</TD>
  <TD></TD>
  <TD><select name=\"language\" size=1 onChange=\"clearFields();\"> 
        <option value= > </option> 
        <option value=BR>BR</option> 
        <option value=EN>EN</option> 
        <option value=FR selected>FR</option> 
        <option value=JP>JP</option>
        <option value=ZH>ZH</option>
      </select></TD>
 </TR>"
|#
;;;------------------------------------------------------------ MAKE-HTTP-CHECKBOX
    
(defun make-http-checkbox (to-agent prop-name object-to-edit class-ref)
  "make a check box to indicate that we want to modify the text corresponding to ~
   the property and not change the link to another object.
Arguments:
Return:
   a line to include in the html macro with the check-box"
  (declare (ignore class-ref to-agent))
      `((:princ ,(format nil "<input type=\"checkbox\" name=~S value=~S />"
                   (concatenate 'string "M-" prop-name) "t"))))

;;;------------------------------------------------------------- MAKE-HTTP-POINTER

(defun make-http-pointer (to-agent prop-name object-to-edit class-ref)
  "make an HTTP string with reference to the selection page
Arguments:
   to-agent: owner of the data
   prop-name: property a value of which we want to select
   object-to-edit: the description of the object to edit
   class-ref: the ref of the object-to-edit class
Return:
   a line to include into the html macro with the HTTP link"
  (declare (ignore object-to-edit))
  `((:princ ,(format nil "<a href=selectform?agent=~S&data=~A&sender=~A>Edit</a>"
               to-agent
               ;; list of values attached to property
               (net.aserve::uriencode-string
                (format nil "~S" (get-field prop-name object-to-edit)))
               ;; info concerning the sending object (used to go back to the edit
               ;; page when the choice is aborted
               (net.aserve::uriencode-string
                (format nil "~S" 
                  `(,class-ref ,(car (get-field "id" object-to-edit)))))
               ))))

#|
(make-http-pointer :contact "correspondant" object-to-edit "contact")
((:PRINC
  "<a href=selectform?agent=:CONTACT&data=((%22Sugawara%3A%20Kenji%22%20%22personne%22
%20%22%24E-PERSON.39%22)%0A%20(%22Fujita%3A%20Shigeru%22%20%22personne%22%20
%22%24E-PERSON.18%22))&sender=(%22contact%22%20%22%24E-CONTACT.21%22)>Edit</a>"))
|#
;;;-------------------------------------------------------------- MAKE-USER-BUTTON
;;; when the form is submitted, the formaction option Specifies the URL of the file
;;; that will process the input control. 
;;; <input type="radio" name="user-action" value="annotations" 
;;;        formaction="http://localhost/annotations?...">Annotations<br>

(defun make-user-button (to-agent row object-to-edit class-ref ip)
  "converts a user-action description into an HTML spec"
  (declare (ignore to-agent class-ref))
  (cond
   ((member :actfcn (cdr row))
    (apply #'make-field-link :object-to-edit object-to-edit :ip ip (cdr row))
    )
   ((member :radio (cdr row))
    `((:input :type :radio :name ,(getf (cdr row) :name)
              :value ,(getf (cdr row) :value))
      ,(getf (cdr row) :header)))
   (t
    (format nil "<input type=\"submit\" name=\"action\" value=~S>"
      (getf (cdr row) :header))
    )
   ))

#|
(setq ll '(:user-action :header "Publish" :name "user-action" :value "publish"))
(make-user-button :WEBNEWS ll nil nil nil)
"<input type=\"submit\" name=\"action\" value=\"Publish\">"

(setq ll '(:user-action :header "View annotations..." 
                        :name "show annotations" :actfcn "annotations"))
(make-user-button :CSCWD-ONTO ll nil nil)
"<input type=\"submit\" value=\"show annotations\" formaction=\"annotations\">..."

(setq ll '(:user-action :header "Refresh page..." :name "refresh-page" :actfcn nil))
"<input type=\"submit\" value=\"refresh-page\">..."
|#
;;;-------------------------------------------------------------------- MAKE-VALUE
;;; the get-field function returns values associated to a property of 
;;; object-to-edit. values may be:
;;;  - nil  -> ""
;;;  - a list containing a simple string ("albert")  -> "albert"
;;;  - a list containing several strings ("albert" "zoe")  -> "albert, zoe"
;;;  - a list containing an MLN value (((:fr "albert" "Jean")(:en "john"))))  
;;;    -> the mln if no print-format option (:format-value in the field description)
;;;    -> the result of applying the print-format function to the car of the list
;;;  - a list of lists (for a relation)
;;;    make a list with the car of the sublists
;;;    - if each value is a string return the composed string
;;;    - if each value is an MLN
;;;      -> the list of MLNs if there is no format-value option
;;;      -> the result of applying print-format to each mln if it is there

(defun make-value (name object-to-edit &optional print-format)
  "builds a composite value by extracting the info from object-to-edit. When ~
   there are several values they are separated by a comma. When the value is ~
   an MLN, returns it as it unless ther is a print-format argument, in which ~
   case it is applied to the MLN value.
Arguments:
   name: the string naming the property in the HTTP parameters
   object-to-edit: the edit formatted by the owner agent
   print-format (opt): function to apply to each value when there
Return:
   a string to post into the field of the edit page or an MLN for an attribute ~
   or a list of MLNs for a relation."
  (let ((values (get-field name object-to-edit)))
    (dformat :web 1 "~2%;---------- Entering make-value ----------")
    (dformat :web 1 "~%;--- values: ~S" values)
    (dformat :web 1 "~%;--- print-format: ~S" print-format)
    
    (cond
     ;; no value return ""
     ((null values) "")
     ;; all strings return compound string after applying print-format if there
     ((every #'stringp values)
      (format nil "~{~A~^, ~}"
        (if print-format (mapcar print-format values)
          values)))
     ;; single MLN, return it unless print-format is there
     ((or (mln::mln? (car values))(mln::%mln? (car values)))
      (if print-format
          (let ((val (apply print-format values)))
            (cond
             ((listp val)
              (format nil "~{~A~^, ~}" val))
             ((stringp val)
              val)
             (t (error "make-value: format-value function should return string or ~
                        list. format-value: ~S" print-format))))
        (car values)))
     ;; here lists but not MLN, we have a relation
     ((every #'listp values)
      ;; collect the values describing the successors
      (setq values (mapcar #'car values))
      (cond
       ;; if all are strings apply print-format if there
       ((every #'stringp values)
        (format nil "~{~A~^, ~}"
          (if print-format (mapcar print-format values) values)))
       ;; if all are MLNs
       ((every #'(lambda(xx)(or(mln::mln? xx)(mln::%mln? xx))) values)
        (if (not print-format)
            ;; return the list of MLNs
            values
          ;; otherwise make a composite value assuming that print-format returns
          ;; a string
          (format nil "~{~A~^, ~}" (mapcar print-format values))
          )))))))

#|
(defun pf (val) (string-upcase val))
(setq oe '(("prop" "a")))
(make-value "prop" oe)
"a"
(make-value "prop" oe 'pf)
"A"

(setq oe '(("prop" "a" "B" "c")))
(make-value "prop" oe)
"a, B, c"
(make-value "prop" oe 'pf)
"A, B, C"

(setq oe '(("prop" ((: fr "a" "B" "c")(:en "d" "e")))))
(make-value "prop" oe)
((:FR "a" "B" "c") (:EN "d" "e"))

(defun pg (val) (mln::extract-to-string val :always t))
(make-value "prop" oe 'pg)
"a, B, c"

(setq oe '(("prop"
            ("g" 1 2)
            ("h" 3 4)
            ("i" 5 6))))
(make-value "prop" oe)
"g, h, i"
(make-value "prop" oe 'PF)
"G, H, I"

(setq oe '(("prop"
            (((:fr "a" "b" "c")(:en "d" "e")) 1 2)
            (((:fr "i " " j" "k")(:en "l" "m")) 3 4)
            (((:en "n " "o" "  p"))))))
(make-value "prop" oe)
(((:FR "a" "b" "c") (:EN "d" "e")) ((:FR "i " " j" "k") (:EN "l" "m"))
 ((:EN "n " "o" "  p")))

(make-value "prop" oe 'pg)
"a, b, c, i ,  j, k, n , o,   p"

(setq object-to-edit '(("type de contact" "suivi") ("date de début" "2000")   
                       ("id" "$E-CONTACT.21")
                       ("ville" ("Tsudanuma" "ville" "$E-CITY.15"))
                       ("pays" ("Japon" "pays" "$E-COUNTRY.89"))
                       ("partenaire" ("CIT" "organisation" "$E-ORGANIZATION.2"))
                       ("correspondant" ("Sugawara: Kenji" "personne" "$E-PERSON.39")
                        ("Fujita: Shigeru" "personne" "$E-PERSON.18"))
                       ("correspondant UTC" ("Barthès: Jean-Paul" "personne" "$E-PERSON.3")
                        ("Moulin: Claude" "personne" "$E-PERSON.27"))))
(make-value "zoing" object-to-edit)
""
(make-value "pays" object-to-edit)
"Japon"
(make-value "correspondant" object-to-edit)
"Sugawara: Kenji, Fujita: Shigeru"

;; call not done with print-format, hence returns the MLN value
(make-value "keywords" '(("keywords"
                          (((:EN "personal assistant" "PA"))
                           "keywords" 
                           "$E-KEYWORD.2" 
                           "label"))))
(((:EN "personal assistant" "PA")))
|#
;;;--------------------------------------------------- PUBLISH-OMAS-WEB-EDIT-FORMS
;;; exported in globals.lisp

(defun omas::publish-omas-web-edit-forms (agent)
  "publishes the forms needed by the OMAS Web Editor."
  (omas::publish-web-access-form agent)
  (omas::publish-web-access-select agent)
  (omas::publish-web-edit-form agent) 
  (omas::publish-web-error-page agent) 
  (omas::publish-web-external-entry-page agent)
  (omas::publish-web-locate-form agent)
  :done)

;;;------------------------------------------------------- REMOVE-EMPTY-PARAMETERS

(defun remove-empty-parameters (alist)
  "takes an a-list and removes pairs whose cdr is an empty string"
  (remove nil
          (mapcar #'(lambda(xx) (unless (equal "" (cdr xx)) xx)) alist)))
                                 
#|
(remove-empty-parameters '(("pays" . "japon") ("correspondant" . "Inoue")
            ("prénom correspondant" . "")
            ("initiale correspondant" . "T") ("url" . "")
            ("partenaire" . "U of Tsukuba") ("sigle partenaire" . "")
            ("correspondant UTC" . "barthès") ("type de contact" . "R")
                           ("date de début" . "2012")))
(("pays" . "japon") ("correspondant" . "Inoue") ("initiale correspondant" . "T") 
 ("partenaire" . "U of Tsukuba")("correspondant UTC" . "barthès")
 ("type de contact" . "R") ("date de début" . "2012"))
|#
;;;-------------------------------------------------------------------------- ROW

(defun row (&key header name (size 100) read-only value list-fcn object-to-edit
                 &allow-other-keys)
  "produces a single row in an HTML table. This function is used to produce OMAS ~
   windows. For application windows the edit-page patterns are interpreted by ~
   the make-field-row function that has more options.
Arguments:
   header (key): name of the field
   list-fcn (key): used to produce popup windows (<popup name><field>) 
   name (key): name of the HTML parameter
   object-to-edit (key): an alist containing the object values
   size (key): width of the value field (default 100)
   value (key): initial value
   read-only (key): if true declares the field to be read-only (does not work)
Return:
   a string to be inserted into an html macro"
  (if list-fcn
      (let* ((popup-name (car list-fcn))
             (field-name (cadr list-fcn))
             )
        ;; recursive call that does not contain list-fcn option
        ;; var-name is the name of the second arg of the provide function
        (row :header (string+ "<A HREF=\"" popup-name "\" 
   onClick=\"return popup(this,'" field-name "')\">" header "</A>")
             :object-to-edit object-to-edit
             ;; we post existing values if any
             ;:value (car (get-field name object-to-edit))
             :name name ; name of the parms field
             ))
    ;; else do 
    (moss::string+ 
     "<TR>" "<TD>" header "</TD>" "<TD><INPUT NAME=" name  " TYPE=text"
     " SIZE=" size
     (if value (format nil " VALUE=~S" value) "")
     (if read-only " readonly>" ">") ; does not seem to work with Microsoft browser
     "</TD>" "</TR>")))

#|
(row :header "Nom" :name "name" :read-only t)
"<TR><TD>Nom</TD><TD><INPUT NAME=name TYPE=text SIZE=100 readonly></TD></TR>"
(row :header "Nom" :name "name")
"<TR><TD>Nom</TD><TD><INPUT NAME=name TYPE=text SIZE=100></TD></TR>"
|#  
;;;===============================================================================
;;;              Fonctions to handle the OMAS users list in *omas*
;;;===============================================================================

;;; The omas users entry is an a-list, each entry is a pair, the first element is
;;; an IP number, the cdr is an a-list containing various fields
;;; Example:
;;;   (1234567 ("username" . "barthès") ("pa" . "stevens") ("language" . ":fr"
;;;            ("class-ref" . "news item") ("ref" . "$e-news-item.5")  ...)
;;; the functions are intended to be used in the context of Allegroserve entity
;;; functions

;;;------------------------------------------------------------------------ WU-ADD
;;; this function adds to the current entry more properties, replacing the value
;;; of the ones already existing. If the entry did  not exist, it creates it.

(defun wu-add (ip parms)
  "add more properties to the existing user entry, creating it if necessary."
    (if (wu-get-user-entry ip)
        ;; entry already exits, we add parms
        (dolist (item parms)
          (wu-set ip (car item) (cdr item)))
      ;; otherwise does not exit, create it
      (apply #'wu-set-user-entry ip parms))
  ;; return our entry
  (wu-get-user-entry ip))
  
;;;------------------------------------------------------------------------ WU-GET

(defun wu-get (ip field)
  "get the value of the field."
  (get-field field (cdr (assoc ip (omas::users *omas*)))))

;;;------------------------------------------------------------- WU-GET-USER-ENTRY
;;; if no-page-desc removes the page description

(defun wu-get-user-entry (ip &optional no-page-desc)
  "get the whole description of the user entry"
  (let ((desc (assoc ip (omas::users *omas*))))
    (if no-page-desc
	  (cons (car desc)
	        (remove "page-description" (cdr desc) :key #'car :test #'equal+))
      desc)))

;;;----------------------------------------------------------------------- WU-KEEP

(defun wu-keep (ip field-list)
  "remove all properties from the user entry except those in the field list."
  (let* ((user-list (omas::users *omas*))
         (parms (cdr (assoc ip user-list)))
         val)
    ;; first remove entry from the user list
    (setf (omas::users *omas*)
      (remove ip (omas::users *omas*) :key #'car))
    ;; put a new entry containing only the IP
    (push `(,ip) (omas::users *omas*))
    ;; add all the properties we want to keep if they are present in user entry
    (dolist (field field-list)
      (when (setq val (get-field field parms))
        (wu-set ip field val)))
    ;; return the result
    (omas::users *omas*)
    ))
  
;;;------------------------------------------------------------------------ WU-REM

(defun wu-rem (ip field)
  "remove field from the list of properties associated with a given ip."
  (let* ((user-list (omas::users *omas*))
         (parms (cdr (assoc ip user-list))))
    ;; remove old pair from parms
    (setq parms (remove field parms :key #'car :test #'string-equal))
    ;; remove entry from the user list
    (setq user-list (remove ip user-list :key #'car))
    ;; add updated entry 
    (setq user-list (append user-list (list (cons ip parms))))
    (setf (omas::users *omas*) user-list)
    user-list))

#|
(omas::users *omas*)
((123456 ("username" . "barthès") ("caller" . "edit"))
 (2130706433 ("caller" . "edit") ("ip" . "127.0.0.1")))
(wu-set 123456 "ip-new" "1.2.3.4")
((123456 ("username" . "barthès") ("caller" . "edit") ("ip" . "1.2.3.4"))
 (2130706433 ("caller" . "edit") ("ip" . "127.0.0.1")))
(wu-rem 123456 "ip-new") 
|#
;;;------------------------------------------------------------- WU-REM-USER-ENTRY

(defun wu-rem-user-entry (ip)
  (setf (omas::users *omas*)(remove ip (omas::users *omas*) :key #'car)))

#|
(omas::users *omas*)
((NIL ("ip-new" . "1.2.3.4"))
 (2130706433 ("username" . "barthès") ("language" . ":EN") ("agent-ref" . "stevens-news")
  ("class-ref" . "news-item"))
 (123456))
(wu-rem-user-entry 123456)
((NIL ("ip-new" . "1.2.3.4"))
 (2130706433 ("username" . "barthès") ("language" . ":EN") ("agent-ref" . "stevens-news")
  ("class-ref" . "news-item")))
(wu-rem-user-entry NIL)
((2130706433 ("username" . "barthès") ("language" . ":EN") ("agent-ref" . "stevens-news")
  ("class-ref" . "news-item")))
|#
;;;------------------------------------------------------------------------ WU-SET

(defun wu-set (ip field val)
  "set the value in a specific field, clobbering the previous one"
  (let* ((user-list (omas::users *omas*))
         (parms (cdr (assoc ip user-list))))
    ;; remove old pair from parms
    (setq parms (remove field parms :key #'car :test #'string-equal))
    ;; add the new one
    (setq parms (append parms `((,field . ,val))))
    ;; remove entry from user list
    (setq user-list (remove ip user-list :key #'car))
    ;; add new one
    (push (cons ip parms) user-list)
    ;; update *omas* list
    (setf (omas::users *omas*) user-list)
    ;; return parms
    user-list))

#|
(omas::users *omas*)
((123456 ("username" . "barthès") ("caller" . "edit"))
 (2130706433 ("caller" . "edit") ("ip" . "127.0.0.1")))
(wu-set nil "ip" "1.2.3.4")
((123456 ("username" . "barthès") ("caller" . "edit") ("ip" . "1.2.3.4"))
 (2130706433 ("caller" . "edit") ("ip" . "127.0.0.1")))
(wu-set 123456 "message-list" '(1 2 3 4))
((123456 ("message-list" 1 2 3 4))...)
|#
;;;------------------------------------------------------------- WU-SET-USER-ENTRY

(defun wu-set-user-entry (ip &rest parms)
  "creates a user entry in the OMAS users list.
Arguments:
   ip: the ip of the browser
   parms: a list of pairs properties and values to be added to the IP
Return:
   the new entry."
  (let* ((entry (assoc IP (omas::users *omas*))))
    ;; if already an entry remove it
    (if entry
        (setf (omas::users *omas*)
          (remove ip (omas::users *omas*) :key #'car)))
    ;(print (omas::users *omas*))
    ;; add new entry
    (push `(,ip ,@parms) (omas::users *omas*))
    ;; add the legible version of the IP
    (wu-set ip "ip" (socket:ipaddr-to-dotted ip))
    ;; return list of entries for control
    (omas::users *omas*)))

#|
(wu-set-user-entry nil '("username" . "barthès") '("caller" . "edit"))
((123456 ("username" . "barthès") ("caller" . "edit") ("ip" . "1.2.3.4"))
 (2130706433 ("caller" . "edit") ("ip" . "127.0.0.1")))
|#


;;;===============================================================================
;;;===============================================================================
;;;                            WEB EDIT FORMS
;;;===============================================================================
;;;===============================================================================

;;;===============================================================================
;;;                            ACCESS FORM
;;;===============================================================================
;;; This page is the external input page of the OMAS editor. It is thus protected
;;; by a keyword access, which provides the name of the editing person. When 
;;; accessing directly, then one does not provide any additional information.
;;; We thus record the sender's IP, the sender's name, set the caller to this page,
;;; and post an empty form. Next time around the password form is not required.
;;; However, we transfer control to the locate form.

;;;------------------------------------------------------- PUBLISH-WEB-ACCESS-FORM

(defun omas::publish-web-access-form (agent)
  "post a form to collect info to locate an object to edit."
  (publish 
   :path "/edit"
   :content-type "text/html"
   :function
   #'(lambda (req ent)
       (publish-web-access-form-action agent ent req))))

(defun publish-web-access-form-action (agent ent req)
  (declare (ignore language))
  (let ((ip (socket:remote-host (request-socket req)))
        (parms (remove-empty-parameters (request-query req)))
        language answer result)
    ;; if we are entering this function coming from the selection page, we must
    ;; save agent-ref, class-ref and language that will be cloberred when doing
    ;; a wu-set-user-entry
;;;    (setq class-ref (wu-get ip "class-ref")
;;;        agent-ref (wu-get ip "agent-ref")
;;;        language (wu-get ip "language"))
;;;    (when (get-field "select" parms)
;;;      (push `("class-ref" . ,class-ref) parms)
;;;      (push `("agent-ref" . ,agent-ref) parms)
;;;      (push `("language" . ,language) parms))
        
    (dformat :web 0
             "~2%;========== ENTERING publish-web-access-form /parms:~% ~S" parms)
    (dformat :web 1 "~%;... our user entry:~% ~S~%" (wu-get-user-entry ip))
    ;; password checking
    (multiple-value-bind (name password) (get-basic-authorization req)
      (if* (and name password)
         then  
              ;; ask MEMBERS to check password
              (setq answer
                    (omas::get-answer agent `(("username" . ,name)
                                              ("password" . ,password))
                                      :to-agent :MEMBERS
                                      :action :authorized?
                                      :timeout 5
                                      :allow-nil-answer t))
              (dformat :web 1 "~%; ... in publish-web-access-form-action")
              ;; on error (:ERROR NIL) or timeout NIL, we must return NIL
              (setq result
                    (if (and (listp answer)(eql (car answer) :error))
                        nil
                      answer))
              ;; when password is OK, publish the page
              (if result
                  (with-http-response (req ent)
                    (dformat :web 1 "~%; publish-web-access-form /username: ~S"
                             (get-basic-authorization req))
                    (with-http-body (req ent) 
                      ;; creates a user entry with the current parameters
                      ;; if we are entering for the first time
                      ;; do not do a nything if we are entering the function
                      ;; when coming back e.g. from the selection page
                      (apply #'wu-set-user-entry ip `("username" . ,name) 
                             ;("caller" . "edit")
                             parms)
                      (dformat :web 1 "~%; publish-web-access-form/ users:~% ~S"
                        (omas::users *omas*))
                      (dformat :web 1 
                               "~%; publish-web-access-form /parms:~% ~S~%" parms)
                      (web-accessor agent ip parms)))
                
                ;; else oblige the server to put up a name/password dialog
                (with-http-response (req ent :response *response-unauthorized*)
                  (set-basic-authorization req "OMAS Editor")
                  (with-http-body (req ent))))
         else
              ;; else oblige the server to put up a name/password dialog
              (with-http-response (req ent :response *response-unauthorized*)
                (set-basic-authorization req "OMAS Editor")
                (with-http-body (req ent)))))))

;;;===============================================================================
;;;                            ACCESS SELECT FORM
;;;===============================================================================
;;; we use this page to decouple the edit form from the login/password process
;;; meaning that the web-accessor function is called from the /edit entity but
;;; when the form is filled it calls the /accessedit entity rather than the /edit
;;; one, which bypasses the password checking process
;;; This form is never posted directly but is used to process the content of the
;;; Edit Access Selection window

;;;--------------------------------------------------------- PUBLISH-ACCESS-SELECT

(defun omas::publish-web-access-select (agent)
  "post an introductory selection form and processes its content"
  (publish 
   :path "/accessselect"
   :content-type "text/html"
   :function
   #'(lambda (req ent)
       (dformat :web 0 
                "~2%;========== ~A Entering web-access-select" (omas::name agent))
       (with-http-response (req ent)
         (with-http-body (req ent)
           ;; when called with the result of the access selection form contains
           ;; agent-ref, class-ref, data, language
           (let ((parms (remove-empty-parameters (request-query req)))
                 (ip (socket:remote-host (request-socket req))))
             ;; should merge parms into user entry
             (wu-add ip parms)
             (web-accessor agent ip parms)))))))

;;;------------------------------------------------------------------ WEB-ACCESSOR
;;; web-accessor is a multiple entry function called by the entity function 
;;; controlling the web page.
;;; 1. the first time it is entered with no data from the page, except for that
;;;    which was part of the HTTP calling message
;;; 2. then, it can be entered with messages for incorrect data, in which case
;;;    parms (req) contains the data from the form
;;; 3. it can also be entered from returning from the select page
;;; 4. the last time it is entered with data from the page as part of the req parms

(defun web-accessor (agent ip parms)
  "body of the publish-web-access-form-form.
Arguments:
   agent: the web agent
   ip: ip of the machine hosting the browser
   parms: parameters of the HTTP message
Return:
  transfer to a nother page."
  (declare (special *language*))
  
  (dformat :web 0 "~2%;----------- ~A: Entering web-accessor" (omas::name agent))
  (dformat :web 0 "~%;--- parms:~%  ~S" parms)
  (dformat :web 0 "~%;--- user entry:~% ~S" (wu-get-user-entry ip :no-page))
  
  (let* ((language (wu-get ip "language"))
         ;; here either language exists and is valid or we leave it as it is
         ;; set English in fine, if everything else fails
         (*language* (or (set-language language :only-return t) 
                         (and (boundp '*language*) *language*)
                         :EN))
        class-ref to-agent-ref to-agent-key val answer select caller)
    
      ;; get some of the parameters we'll need
      (setq select (wu-get ip "select"))
      (setq to-agent-ref (wu-get ip "agent-ref"))
      (setq class-ref (wu-get ip "class-ref"))
      (setq val (wu-get ip "val"))
      (setq caller (wu-get ip "caller"))
      
      ;;=== if select has a value, we come back from a selection page and
      ;; must call the edit page
      (when select
        ;; must initialize ref parameter, otherwise post will not find the object
        (wu-set ip "ref" (wu-get ip "select"))
        ;; clean user entry
        (wu-rem ip "select")
        (return-from web-accessor (web-edit-post agent ip parms)))
      
      ;;=== if no parameters are set, then first time around
      (unless (or to-agent-ref class-ref)
        ;; publish empty form (no message, maybe caller)
        (return-from web-accessor
          (web-access-build-page
           (select-name omas::*web-need-info*) caller ip))) ; jpb 1406
      
      ;;=== if owner agent is not mentioned, error
      (unless to-agent-ref
        (return-from web-accessor
          (web-access-build-page 
           (select-name omas::*web-missing-agent*) caller ip))) ; jpb 1406
      
      ;; make agent key
      (setq to-agent-key (intern (string-upcase to-agent-ref) :keyword))
      
      ;;=== if no class-ref complain
      (unless class-ref
        (cg:beep)
        (return-from web-accessor
          (web-access-build-page
           (select-name omas::*web-missing-class*) ; jpb 1406
           caller
           ip)))
      
      ;;=== should check the validity of the class
      
      ;;=== if no value is specified, we assume that we want to create a new
      ;; instance of the class and post an empty form
      (unless val
        (return-from web-accessor (web-edit-post agent ip parms)))
      
      ;;=== otherwise, ask agent to locate object
      (setq answer 
            (omas::get-answer agent `(,class-ref ,val)
                              :to-agent to-agent-key
                              :action :locate-object
                              :language *language*
                              :timeout 7))
      (dformat :web 1 "~%; web-accessor /answer to locate-object: ~S" answer)
      ;;===
      ;; if error  post error inside edit page
      (when (and (listp answer)(eql (car answer) :error))
        (cg:beep)
        ;(print `(++++++++++ answer ,answer))
        (return-from web-accessor
          (web-access-build-page (cadr answer) caller ip)))
      
      ;; if several values, call selection page
      (when (cdr answer)
        (return-from web-accessor (web-locate-select answer ip)))
      
      ;; otherwise add object-id to user entry and call editor
      (wu-set ip "ref" (caar answer))
      ;; should call the edit page with parms to post object directly
      
      (web-edit-post agent ip parms)
      ))

;;;---------------------------------------------------------- WEB-ACCESS-ADD-BODY

(defun web-access-add-body (&optional ip)
  "builds to body of the access form. When parms is given, it is used to initialize ~
   some fields."
  `(((:table :border "0" :cols "2" :width "100%")
     (:tbody
      ,(row :header (select-name omas::*web-owner-agent*) ; jpb 1406
            :name "agent-ref"
            :value (or (wu-get ip "agent-ref") ""))
      ,(row :header (select-name omas::*web-object-class*) ; jpb 1406 
            :name "class-ref"
            :value (or (wu-get ip "class-ref") ""))
      ,(row :header (select-name omas::*web-locate-data*) ; jpb 1406
            :name "val"
            :value (or (wu-get ip "val") ""))
      ,(row :header "Set language" :name "language" 
            :value (or (wu-get ip "language") ":EN"))
      ;:br 
      ))))

;;;--------------------------------------------------------- WEB-ACCESS-BUILD-PAGE

(defun web-access-build-page (message caller &optional ip)
  "builds the HTML string to post the access form, filling it in part if parms ~
   are passed to the page
Arguments:
   message: a message to post into the page
   caller: the name of the page that called this one
   ip (opt): IP of the machine hosting the browser."
  (eval
   `(html 
     (:html
      (:head 
       (:title ,(select-name omas::*web-locate-title*)) ; jpb 1406
       ((:meta :charset "UTF-8")))
      (:body 
       "<font face=\"arial\">"
       ((:form :action "accessselect")
        "<fieldset style=\"background-color:yellow;\">"
        :br
        ,@(if message `(,message :br))
        (:center (:h2 ,(select-name omas::*web-locate-title*))) ; jpb 1406
        ;,@(add-edit-body)
        ,@(web-access-add-body ip)
        :br
        (:center ((:input :type "submit" 
                          :value ,(select-name omas::*web-send*)))) ; jpb 1406
        :br
        ;; save return value to pass it to the next page
        ,@(if caller
              `((:princ ,(format nil "<input name=\"caller\" type=\"hidden\" value=~A>"
                           caller))))
        ))))))


;;;===============================================================================
;;;                            EDIT FORM
;;;===============================================================================

;;;--------------------------------------------------------- PUBLISH-WEB-EDIT-FORM

(defun omas::publish-web-edit-form (agent)
  "post a form to edit contact data"
  (publish 
   :path "/webeditform"
   :content-type "text/html"
   :function
   #'(lambda (req ent)
       (dformat :web 0 "~%;========== ENTERING publish-web-edit-form")
       (with-http-response (req ent)
         (with-http-body (req ent)
           ;(dformat :web 1 "~%;publish-web-edit-form /(request-socket req):~%; ~S"
           ;  (request-socket req))
           (dformat :web 0 "~%;publish-web-edit-form /ip: ~S"
             (socket:remote-host (request-socket req)))
           (let ((ip (socket:remote-host (request-socket req)))
                 (parms (request-query req)))
             ;; we keep all parameters appearing in the posted form, even if their
             ;; value is " ". The rationale is that the old value could have been
             ;; erased. Also we need the list of the properties that were posted
             ;; effectively in the form when deciding to update an object.
             (dformat :web 0 "~%;--- parms:~% ~S" parms)
             (wu-add ip parms)
             (dformat :web 0 "~%;--- user entry:~% ~S" (wu-get-user-entry ip :no-page))
             ;; call web editor. Will return a page (html form)
             (web-editor agent ip parms)))))))

;;;-------------------------------------------------------------------- WEB-EDITOR
;;; The generic behavior of the editor is limitied to the standard actions:
;;; create, delete, update, edit-new-object with again, and abort
;;; any other command must be user-defined and appear in a second row of buttons
;;; The first row has priority over the second row

;;; An example can be found in the NEWS application, where we want to edit
;;; information items, then eventually publish them, i.e. transmit them to the 
;;; publisher. To do so we add a new row with a single button "publish"
;;; when selected and when the execute button is clicked, then control is passed
;;; to the omas::web-edit-user-action function, which must be provided by the user

;;;-------------------------------------------------------------------- WEB-EDITOR

(defun web-editor (agent ip parms)
  "controls the flow of actions of the editor.
Argument:
   agent: the WEB agent
   ip: ip of the machine hosting the browser
   parms: parameters of the HTTP request to this page
Return:
   the result of an html macro producing a web page to send to the client."
  (declare (special *language*))
  (dformat :web 0 "2~%;---------- ~A: Entering web-editor" (omas::key agent))
  (dformat :web 0 "~%;--- parms:~% ~S" parms)
  (dformat :web 0 "~%;--- user entry:~% ~S" (wu-get-user-entry ip :no-page))
  
  (let* ((action (wu-get ip "action"))
         (user-action (wu-get ip "user-action"))
         (language (wu-get ip "language"))
         )
    (dformat :web 2 "~%;--- language: ~S" language)
    (dformat :web 2 "~%;--- *package* ~S" *package*)
    (setq *language* (omas::set-language language :only-return t))
    (dformat :web 0 "~%;--- *language*: ~S" *language*)
    (dformat :web 2 "~%;--- (package *language*): ~S" (symbol-package '*language*))
    ;; reset message-list
    (wu-rem ip "message-list")
    ;; when both action and user-action are present, remove user-action
    (when (and action user-action)
      (wu-rem ip "user-action")
      (setq parms (remove "user-action" parms :test #'equal+ :key #'car))
      )
        
    ;; check various states indicated by the action parameter (or lack or it)
    (cond
     
     ;;=== when action is "refresh page" we simply call the page again
     ;; with the fields that are modified
     ((and action (equal+ action "refresh page"))
      (wu-set ip "refresh" t)
      (web-edit-build-page ip))
     
     ;;=== when action is "new" we'd like to edit a new object,
     ;; since we have been posting the content of an object user entry contains
     ;; information that we do not want to keep. We need to keep only session
     ;; information (caller, IP, user information, language, entry-language, 
	 ;; owner, and class) and discard turn information
     ((equal+ action "edit another object")
      (wu-keep ip '("ip" "caller" "username" "language" "agent-ref" "class-ref"
                    "pa-key" "user-name" "user-first-name" "user-login"
                    "user-email" "entry-language"))
      (wu-set ip "init" "t")
      (web-locator agent ip parms)
      )
     
     ;;=== if the action is "abort", then we must reload the data from the
     ;; owner agent (same as entering for the first time)
     ((and action (string-equal action "abort editing"))
      (wu-set ip "message-list" (list (select-name omas::*web-abort*))) ; jpb 1406
      (web-edit-post agent ip parms)
      )
     
     ;;=== if the action is "commit", then we compare the new values (parms) with 
     ;; the old values and build an update program to send to the owner agent
     ;((and action (string-equal action "commit"))
     ((and action (string-equal action "commit changes"))
      (wu-rem ip "action")
      (wu-rem ip "user-action")
      (web-edit-commit agent ip parms)
      )
     
     ;;=== if the action is "delete", then send the order to the owner and post an
     ;; empty page or error on return
     ;((and action (string-equal action "delete"))
     ((and action (string-equal action "delete object"))
      (wu-rem ip "user-action")
      (wu-rem ip "action")
      (web-edit-delete agent ip parms))
     
     ;; if the action is again, then will create a new instance of the same class
     ((and action (string-equal action "create sibling"))
      (wu-keep ip `("ip" "caller" "username" "language" "agent-ref" "class-ref"
                    "page-description" "pa-key" "user-name" "user-first-name"
                    "user-login" "user-mail" "entry-language"))
      (web-edit-post agent ip parms)
      )
     
     ;; action is user-defined, leave the processing to the user
     (user-action
      (web-edit-user-action agent (wu-get ip "class-ref") ip parms))
     
     ;;=== if action is not specified, which is the case when entering the entity
     ;; for the first time, we call the edit-post function
     ;; one comes here if object has been already saved and the user clicks the
     ;; send button without having selected any action button
     ((wu-get ip "initial-object")
      (web-edit-build-page ip))
     (t
      ;; to play safe
      (wu-rem ip "user-action")
      (web-edit-post agent ip parms))
     )))

;;;------------------------------------------------------------- WEB-EDIT-ADD-BODY

(defun web-edit-add-body (to-agent pattern object-to-edit class-ref caller ip)
  "produces the body of the web page to display.
Arguments:
   to-agent: PA
   pattern: pattern of the web page body
   object-to-edit: a string value of the object to edit
   class-ref: class of the object to edit
   caller: caller of the page to display
   ip: ip of the sender acting as a key in the agent user list
Return:
   an HTML string of the body of the page to display."
  (declare (ignore caller)(special *language*))
  (let ((language (wu-get ip "language"))
        user-pattern)
    (omas::set-language language)
    (dformat :web 0 "~%; web-edit-add-body /*langauge*: ~S" *language*)
    ;; retrieve special entries corresponding to user actions
    (setq user-pattern
          (remove nil 
                  (mapcar #'(lambda (xx) (if (eql (car xx) :user-action) xx))
                    pattern)))
    (dformat :web 0 "~%; web-edit-add-body /object-to-edit:~% ~S~%" object-to-edit)
    `(((:table :border "0" :cols "3" :width "100%")
       ;"<TR><TD/><TD width=\"5%\"/><TD/></TR>"
       ;; compute the HTML string for each field of the page description
       ,(cons :tbody
              (mapcar #'(lambda(xx) 
                          (make-field to-agent xx object-to-edit class-ref ip))
                pattern))
       ;(:princ "<input name=\"old\" type=\"hidden\"  value=\"test 2\">")
       (:tr (:td "")) ; insert an empty line for legibility
       )
      :br
      ;;=== execute buttons
      (:center
       ;((:input :type :radio :name "action" :value "abort")
       ; ,(select-name omas::*web-quit*)) ; jpb 1406
       "<input type=\"submit\" name=\"action\" value=\"abort editing\">..."
       "<input type=\"submit\" name=\"action\" value=\"commit changes\">..."
       "<input type=\"submit\" name=\"action\" value=\"edit another object\">..."
       "<input type=\"submit\" name=\"action\" value=\"create sibling\">..."
       "<input type=\"submit\" name=\"action\" value=\"delete object\">..."
       "<input type=\"submit\" name=\"action\" id=\"refresh\" value=\"refresh page\">"
       )
      :br
      ,@(if user-pattern
            `("User defined actions: <BR>"
              (:center 
               ,@(mapcar #'(lambda(xx) 
                             (make-user-button to-agent xx object-to-edit 
                                               class-ref ip))
                   user-pattern)
               )
              :br))
      )))

;;;----------------------------------------------------------- WEB-EDIT-BUILD-PAGE

(defun web-edit-build-page (ip)
  "builds the html string for posting the page to edit.
Arguments:
   ip: ip of the machine hosting the browser and index for user entry
Return:
   actually not much, prints a string into a :class-ref special stream."
  (declare (special *language*))
  (dformat :web 0 "~%; web-edit-build-page /users:~% ~S" (omas::users *omas*))
  (let ((to-agent-key (intern (string-upcase (wu-get ip "agent-ref")) :keyword))
        (class-ref (wu-get ip "class-ref"))
        (object-to-display (wu-get ip "initial-object"))
        (pattern (wu-get ip "page-description"))
        (caller (wu-get ip "caller"))
        (message-list (wu-get ip "message-list"))
        result)
    (dformat :web 0 "~%:--- caller: ~S" caller)
    
    ;; set the default language to be the one of the user entry
    (set-language (wu-get ip "language"))
    
    (setq result
    (eval
     `(html 
       (:html
        (:head 
         (:title ,(select-name omas::*web-editor-title*))
         ((:meta :charset "UTF-8"))
         ;; this script allows including clickable headers to present list of data
         ;; from which to choose in pop up windows
         "<SCRIPT TYPE=\"text/javascript\">
<!--
function provide(arg, field) {
  if(document.getElementById(field).value == '')
    document.getElementById(field).value = arg;
  else
    document.getElementById(field).value += ', ' + arg;
}

function popup(mylink, windowname)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
   href=mylink;
else
   href=mylink.href;
window.open(href, windowname, 'width=400,height=200,scrollbars=yes');
return false;
}
//-->
</SCRIPT>"
         ;; additional javascripts as defined in the page description (pattern)
         ,@(include-javascripts pattern)
         )
        (:body 
         "<font face=\"arial\">"
         (:princ ,(format nil "~{~A<BR>~%~}" message-list))
         :br
         ((:form :action "webeditform")
          "<fieldset style=\"background-color:yellow;\">"
          (:center 
           (:h2 ,(format nil "~A - ~A" 
                   (select-name omas::*web-editor-title*)
                   class-ref))) ; jpb 1406
          
          ,@(web-edit-add-body to-agent-key pattern object-to-display class-ref 
                               caller ip)
          ,@(if caller 
                `((:princ ,(format nil "<a href=~S>~A</a>"
                             caller (select-name omas::*web-return*))))) ; jpb 1406
          :br
          ))))))
    ;; before leaving remove flag
    (wu-rem ip "refresh")
    result))

;;;--------------------------------------------------------------- WEB-EDIT-COMMIT
;;; we call this function either to create a new object or to update an existing
;;; one. The two cases are distinguished by the presence of ref.
;;; the new object is decribed in parms as a list of dotted pairs.
;;; when the commit function is called, then the "ref" property contains either the
;;; local id of the object to be modified or NIL.

;;; if the object must be created ("ref" is nil) then the :start-creating skill is
;;; called. It returns a list starting with nil and containing initial values. The
;;; description is saved into the "initial object" of the user entry

;;; When calling the :update-object skill, one must pass the class-ref of the object
;;; for creating the corresponding individual

(defun web-edit-commit (agent ip parms)
  "sends a message to the owner agent. The message contains a program for updating ~
   or creating the object.
Arguments:
   agent: current agent, e.g. :WEB
   ip: ip of the machine hosting the browser and index on the user entry
   parms: the HTTP parms, contains the values of the fields of the edit page, i.e.
          the content of the edited object
Return: 
   nothing special."
  (dformat :web 0 "~2%;---------- Entering web-edit-commit")
  (dformat :web 0 "~%:--- user entry:~%  ~S" (wu-get-user-entry ip :no-page))
  (dformat :web 1 "~%;--- new object desc: parms = (request-query req):~%  ~S~%" parms)
  ;; copy parameters
  (let* ((to-agent (wu-get ip "agent-ref"))
         (to-agent-key (intern (string-upcase to-agent) :keyword))
         (class-ref (wu-get ip "class-ref")) ; jpb1401
         (ref (wu-get ip "ref")) ; nil for object to create
         (language (wu-get ip "language"))
         ;; we must remove the "action" field
         (parms (rem-field "action"  parms))
         ;; and user-action in case we checked both radio buttons
         (parms (rem-field "user-action" parms))
         initial-object answer)
    ;; retrieve the old value, i.e. the value in the last page before clicking
    ;; "Envoyer" if there was one.
    ;(setq language (if language (read-from-string language) *language*))
    
    ;; set *language* but do not modify language
    (set-language language)
    
    (cond
     ;;=== when ref is nil, we create a new object
     ((null ref)
      (setq answer 
            (web-edit-commit-create agent to-agent-key parms class-ref 
                                    *language* ip))
      )
     ;;=== when ref is non nil, we update the referenced object
     (t
      ;; retrieve the previous object description from the user entry
      (setq initial-object (wu-get ip "initial-object"))
      (dformat :web 0 "~%;--- initial-object there?: ~S" (not (null initial-object)))
      (setq answer
            (web-edit-commit-update agent to-agent-key parms *language*
                                    ref initial-object ip))
      ;; check if there was any change
      (when (eql answer :no-change)
        ;; no change, post the page again with a message
        (cg:beep)
        ;; record message for printing
        (wu-set ip "message-list" 
                (list (select-name omas::*web-no-change*))) ; jpb 1406
        (return-from web-edit-commit (web-edit-post agent ip parms))) 
      ))
    
    ;; the answer should be a list (<id-ref>|:error <message-list>)
    (dformat :web 0 "~%; web-edit-commit /answer: ~S~%" answer)
    ;; save messages to post them on top of page
    (if (cdr answer) (wu-set ip "message-list" (cadr answer)))
    ;;===
    ;; if error  post error on top of edit page
    (when (and (listp answer)(eql (car answer) :error))
      (cg:beep)
      (return-from web-edit-commit (web-edit-post agent ip parms)))
    
    ;;=== post updated object
    ;; first clean up the user entry
    (wu-keep ip '("ip" "username" "caller" "class-ref" "agent-ref" "message-list"
                  "language" "page-description" "pa-key" "user-name" "user-login"
                  "user-first-name" "user-email"))
    
    ;; save object ref in case we just created it
    (wu-set ip "ref" (car answer))
    
    ;;=== we reload the object description from the owner, in case it was modified,
    ;; e.g. by adding more info when it was created or modified
    (setq answer
          (omas::get-answer agent (car answer)
                            :to-agent to-agent-key
                            :action :get-object-from-id
                            :language *language*
                            :timeout (omas::web-update-timeout *omas*)))
    (dformat :web 1 "~%; web-edit-commit /answer from get-object-from-id:~% ~S" answer)
    ;; in case of error, display the error page
    (when (and (listp answer)(eql (car answer) :error))
      (return-from web-edit-commit (web-error-form (cadr answer) ip)))
    
    ;; save the object description into the user entry initial-object field as a
    ;; reference to further modifications
    (wu-set ip "initial-object" (cdr answer))
    
    ;;********** remove the publish entry? No, this is done by web-edit when
    ;; processing the execute buttons
    
    ;; web-edit-build-page displays the initial object from the user entry (wu)
    (web-edit-build-page ip)
    ))

;;;-------------------------------------------------------- WEB-EDIT-COMMIT-CREATE
;;; This function is called whenever we must create a new object. It sends a
;;; message to the to-agent asking it to create the object (:CREATE-OBJECT skill)
    
(defun web-edit-commit-create (agent to-agent-key parms class-ref language ip)
  "called to create a new object.
Arguments:
   agent: current agent
   to-agent: owner of the objects to create
   parms: alist of fields/values
   class-ref: ref of the class of the object to create
   language: current language
   ip: ip of the machine of the web browser, used as an index
Return:
   the id of the newly created object or a transfer to the error page"
  (declare (special *omas* *language*)(ignore language))
  (let ((private (wu-get ip "private"))
        (owner (wu-get ip "owner"))
        answer)
    ;; if private is non nil, then add it to parms ??? only for NEWS...
    (if private (setq parms (cons `("private" . ,private) parms)))
    
    ;; if owner is present it specifies the object to which we want to link the
    ;; created object (e.g. an annotation is linked to its "owner")
    (if owner (setq parms (cons `("owner" . ,owner) parms)))
    
    ;;=== CREATION of a new object
    ;; when the object is a new object to be created, then the different fields
    ;; can be obtained from the parms. However, the fields are not actual object
    ;; properties but must be reinterpreted with respect to the page description
    ;; Some fields correspond to ancillary objects that exist or must be created.
    (dformat :web 0 "~2%;---------- ~S: Entering web-edit-commit-create" (omas::name agent))
    
    ;; return answer to web-edit-commit that will process the error if there is one
    ;; the answer should be a list (<id-ref>|:error <message-list>)
    (setq answer
          (omas::get-answer agent (cons class-ref parms)
                            :to-agent to-agent-key
                            :action :create-object
                            :language *language*
                            :timeout (omas::web-update-timeout *omas*)))
    (dformat :web 1 
             "~%; web-edit-commit-create/ answer from create-object:~% ~S" answer)
    answer))

;;;------------------------------------------------------ WEB-EDIT-COMMIT-UPDATE  
     
(defun web-edit-commit-update 
    (agent to-agent-key parms language ref initial-object ip)
  "called from web-edit-commit to update an existing object
Arguments:
   agent: current agent
   to-agent-key: owner of the objects
   parms: a-list of edit page fields
   language: language of the object: unused, use *language*
   ref: id ref of the object
   initial-object: previous state of the object
   ip: IP of the machine hosting the browser, used as an index for the user entry
Return:
   a list (<id-ref> <messages>) or (:error <messages>)"
  (declare (special *language*)(ignore language))
  (dformat :web 0
           "~2%;---------- ~S: Entering web-edit-commit-update" (omas::name agent))
  
  (let ((class-ref (wu-get ip "class-ref"))
        old-values changes answer)
    ;; modify object-to-edit so that it appears as a list of dotted pairs 
    ;; (parm . value)* remove pairs in which value is ""
    ;; removes all the path junk from the initial object format
    (dformat :web 0 "~%;--- initial-object:~% ~S" initial-object)
    
    ;; call the modify skill; the default skill returns nil; a user-defined skill 
    ;; must return a non nil answer
	;; adding IP to arguments, thus transferring the editing context JPB1505
    (setq answer
          (omas::get-answer agent (list class-ref initial-object parms ip)
                            :to-agent to-agent-key
                            :action :modify-object
                            :language *language*
                            :timeout (omas::web-update-timeout *omas*)
                            :allow-nil-answer t)) ; JPB0228
    (dformat :web 0 "~%;--- answer: ~S" answer) :JPB0228
    ;; if answer is non nil return it
    (when answer
      (return-from web-edit-commit-update answer))
    
    (when initial-object
      (setq old-values (omas::initial-object2parms initial-object)))
    (dformat :web 0 "~%;--- initial-object (parm form):~% ~S" old-values)
    (dformat :web 0 "~%;--- new-object (parm form):~% ~S" parms)
    
    ;;=== check if there was any change to the object
    ;; we could click the "commit" button without having modified the object or 
    ;; filled any field of an empty form
    
    ;; check changes between new and old version
    (setq changes (data-diff old-values parms))
    (dformat :web 0 "~%; web-edit-commit-update /changes:~% ~S" changes)
    
    (unless changes
      (return-from web-edit-commit-update :no-change))
    
    (dformat :web 0 "~%;--- web-edit-commit-update /Updating the object")
    ;; if the object is private, then add a pair to parms before sending to update
    ;; object
    (if (wu-get ip "private")(push '("private" . T) parms))
    
    ;; return the answer to web-edit-commit that will process errors if any
    ;; it is a list (<id-ref or :error> <message-list>)
    (setq answer
          (omas::get-answer agent (list ref changes parms)
                            :to-agent to-agent-key
                            :action :update-object
                            :language *language*
                            :timeout (omas::web-update-timeout *omas*)))
    (dformat :web 1 
             "~%; web-edit-commit-update /answer from update-object:~% ~S" answer)
    answer))

;;;--------------------------------------------------------------- WEB-EDIT-DELETE

(defun web-edit-delete (agent ip parms)
  "we send an order to delete an object to the owner of the object"
  (let (answer agent-ref ref to-agent-key)
    (dformat :web 0 "~%;========== ENTERING web-edit-delete/user entry:~% ~S~%"
      (wu-get-user-entry ip))
    ;; get the key of the owner agent (we should have one, otherwise error)
    (setq agent-ref (wu-get ip "agent-ref"))
    ;; (unless agent-ref ...
    (setq to-agent-key (intern (string-upcase agent-ref) :keyword))
    
    ;; do we have an object?
    (setq ref (wu-get ip "ref"))
    
    (setq answer
          (omas::get-answer agent ref
                            :to-agent to-agent-key
                            :action :delete-object
                            :timeout (omas::web-delete-timeout *omas*)))
    
    ;; in case of error display the error page
    (when (and (listp answer)(eql (car answer) :error))
      (return-from web-edit-delete 
        (web-error-form (cadr answer) ip)))
    
    ;; post empty page of the same class, ready to create a new object ?
    ;;***** maybe should return to the selection form
    (wu-keep ip '("username" "caller" "class-ref" "agent-ref" "language" "pa-key"
                  "user-name" "user-first-name" "user-login" "user-email"
				  "entry-language"))
    (web-edit-post agent ip parms)
    ))

;;;----------------------------------------------------------------- WEB-EDIT-POST
;;; send a web page to the user showing an empty edit page for creation or a 
;;; filled page for modification
;;; the function is called from access (when the editor is called directly from a
;;; browser, or from locate, or from the webeditform (called from the application.
;;; If the user's entry does not exist, it must set it up.

(defun web-edit-post (agent ip parms)
  "sends a page to the user's browser either empty for creation of filled
Arguments:
   agent: the current web agent
   req: ip of the machine hosting the browser, index on the user entry
   parms: whatever parameters the HTTP message had
Return:
   an HTML string to send ttoo the user's browser."
  (declare (special *language*))
  ;; add parms to user entry
  (dformat :web 0 "~2%;---------- ~A: Entering web-edit-post" (omas::name agent))
  (dformat :web 0 "~%;--- user entry:~%  ~S~%" (wu-get-user-entry ip :no-page))
  
  (let (agent-ref to-agent-key class-ref ref answer pattern)
    
	;;=== check if user entry is initialized
    (unless (wu-get-user-entry ip)
      ;; if not create the entry with whatever parameters we have here
      (apply #'wu-set-user-entry ip parms))
	  
    ;; set global variable to the right keyword tag (not really necessary)
	;; question: should parms take precedence over user entry for language?
	;; wrong question because calling the form has replaced the user entry 
	;; with the value in parms
    (set-language (wu-get ip "language"))
    
    ;;=== get the key of the owner agent (we should have one, otherwise error)
    (setq agent-ref (wu-get ip "agent-ref"))
    (unless agent-ref 
      (return-from web-edit-post 
        (web-error-form "No reference to an owner agent."  ip)))
    ;; build agent key
    (setq to-agent-key (intern (string-upcase agent-ref) :keyword))
    
    ;; get class reference
    (setq class-ref (wu-get ip "class-ref"))
    
    ;; do we have an object?
    (setq ref (wu-get ip "ref"))
    ;; if "" or nil, return nil
    (setq ref (if (equal+ ref "") nil ref))
    
    ;;== if so, ask for the object desription
    (cond
     (ref
      (setq answer
            (omas::get-answer agent ref
                              :to-agent to-agent-key
                              :action :get-object-from-id
                              :language *language*
                              :timeout (omas::web-post-timeout *omas*)))
      (dformat :web 0
               "~%; web-edit-post /answer from get-object-from-id:~% ~S~%" answer)
      
      ;; in case of error display the error page
      (when (and (listp answer)(eql (car answer) :error))
        (return-from web-edit-post
          (web-error-form (cadr answer) ip))))
      
     (t
      ;; if not, tell the owner that we are starting creation and get a temporary 
      ;; stub with possibly values initiated by the system (:initfcn option)
      (setq answer
            (omas::get-answer agent `(:creating ,class-ref)
                              :to-agent to-agent-key
                              :action :start-editing
                              :language *language*
                              :timeout (omas::web-post-timeout *omas*)))
      (dformat :web 0 "~%; web-edit-post /answer from start-editing:~% ~S~%" answer)
      ;;in case of error (non existing class) display the error page
      (when (and (listp answer)(eql (car answer) :error))
        (return-from web-edit-post
          (web-error-form (cadr answer) ip)))
      ;; if no error push temporary id into the user entry
      (wu-set ip "ref" (car answer)) ; jpb 1401
      ))
    
    ;;=== if no error, cache the value of the object description for later comparisons
    (wu-set ip "initial-object" (cdr answer))
    (dformat :web 1 
             "~%; web-edit-post /saved object-descr:~% (~S ...)~%" (cadr answer))
    
    ;;=== ask page layout pattern from the owner agent
    ;; second time around, it is cached on the p-list of to-agent-key
    (setq pattern
          (get-layout-pattern agent class-ref to-agent-key *language*
                              (omas::web-post-timeout *omas*)))
    
    (dformat :web 1 
             "~%; web-edit-post /answer from get-edit-page-layout:~% ~S~%" pattern)
    (dformat :web 1 "~%; web-edit-post /pattern:~% (~S ...)~%" (car pattern))
    
    ;; in case of error display the error page
    (when (and (listp pattern)(eql (car pattern) :error))
      (return-from web-edit-post
        (web-error-form (cadr pattern) ip)))

    ;; save the page layout
    (wu-set ip "page-description" pattern)
        
    (dformat :web 1 "~%; web-edit-post /user-entries:~% ~S~%" (omas::users *omas*))
    ;; so here all parms are recorded in the user's entry of the *omas* table
    ;; when creating a new object the :ref field is nil. The entry is used as
    ;; a temporary storage for all the data concerning the editing session and
    ;; the editing turn
    ;; once a turn is finished (after update or abort) the entry must be cleaned
    
    (dformat :web 1 "~2%; web-edit-post /+++ calling web-edit-build-page~2%")
    ;; make html page
    (web-edit-build-page ip)
    ))

;;;--------------------------------------------------------- WEB-EDIT-REFRESH-PAGE
;;; does not seem to be used

(defun web-edit-refresh-page (agent to-agent-key ref language ip)
  "refreshes the edit form by getting a new copy of the object from the owner. 
The reloaded object is saved into the initial-object field of the user entry.
Arguments:
   agent: current agent
   to-agent-key: owner of the object
   ref: object reference
   language: language in which one wants the object to be interpreted
   ip: index on the user entry
Return:
   nothing: exits with posting the edit form or the error page."
  (declare (special *language*))
  (dformat :web 0
           "~2%;---------- ~S: Entering web-edit-refresh-page" (omas::name agent))
  (let (answer)
    
    (set-language language) ; JPB140927
    
    ;; we  reload the object from the owner agent in case it was modified
    ;; this should not be necessary since when the object is modified the initial-
    ;; object entry is updated
    (setq answer 
          (omas::get-answer agent ref
                            :to-agent to-agent-key
                            :action :get-object-from-id
                            :language *language*
                            :timeout (omas::web-update-timeout *omas*)))
    
    (dformat :web 1
             "~%; web-edit-refresh-page /answer from get-object-from-id:~% ~S" 
             answer)
    ;; in case of error, display the error page
    (when (and (listp answer)(eql (car answer) :error))
      (return-from web-edit-refresh-page (web-error-form (cadr answer) ip)))
    
    ;; save the object description into the user entry initial-object field as a
    ;; reference to further modifications
    (wu-set ip "initial-object" (cdr answer))
    ;; indicate that we want to refresh the page
    (wu-set ip "refresh" t)
    
    ;; web-edit-build-page displays the initial object from the user entry (wu)
    (web-edit-build-page ip)))

;;;---------------------------------------------------------- WEB-EDIT-USER-ACTION

(defun web-edit-user-action (agent class-ref ip parms)
  "function that sends a :WEB-EDIT-USER-ACTION message to the current agent ~
   then posts edit page again.
Arguments:
   agent: current agent
   class-ref: class of object being edited
   parms: list of pairs (fields of the edit form)
Return:
   posts the edit form again with eventual messages."
  (declare (special *language*))
  
  (dformat :web 0 "~2%;---------- ~A: Entering web-edit-user-action" (omas::key agent))
  
  (let* ((ref (wu-get ip "ref"))
         (to-ref (wu-get ip "agent-ref"))
         (initial-object (wu-get ip "initial-object"))
         (to-agent-key (intern (moss::%string-norm to-ref) :keyword))
         (language (wu-get ip "language"))
         answer)

    ;; prepare language (stored as a string in user entry)
;;;    (setq language
;;;          (if language (read-from-string language nil nil)
;;;            *language*))
    
    (set-language language) ; not really necessary
    ;; not sure that it changes the global value of *language* fron there on
    
    ;; we send a message to the agent owning the object (PA?)
    ;; because the function may be executed inside a server thread, we need to
    ;; specify language in the message to agents executing in different threads
    (setq answer
          (omas::get-answer agent (list class-ref parms ref initial-object)
                            :to-agent to-agent-key
                            :action :web-edit-user-action
                            :language *language*
                            :timeout (omas::web-update-timeout *omas*)))
    (dformat :web 1 "~%; web-edit-user-actiont /answer from web-edit-user-action:~% ~S"
             answer)
    
    ;; remove action mark to avoid replay
    (wu-rem ip "user-action")
    
    ;; in case of error, display the error page
    (when (and (listp answer)(eql (car answer) :error))
      (return-from web-edit-user-action (web-error-form (cadr answer) ip)))
    
    ;; answer is (ref (<message-list>)) eventually transferred from another machine
    (wu-set ip :message-list (cadr answer))
    
    ;; we must reload the object from the owner agent in case it was modified
    ;; indeed we cannot guess what the user was doing with the object...
    (web-edit-refresh-page agent to-agent-key ref *language* ip)))

  
;;;==============================================================================
;;;                               ERROR PAGE
;;;-=============================================================================

(defun web-error-form (text ip)
  (let ((caller (wu-get ip "caller")))
    ;; reset all command tags in user entry to avoid future problems
    
    (eval
     `(html
       (:html
        (:head 
         (:title ,(select-name omas::*web-error-title*)) ; jpb 1406
         ((:meta :charset "UTF-8")))
        (:body
         "<font face=\"arial\">"
         "<fieldset style=\"background-color:yellow;\">"
         ((:form :action "weberror")
          (:center (:h2 ,(select-name omas::*web-error-title*)))  ; jpb 1406
          (:princ ,text)
          :br :br
          (:b  ,(select-name omas::*web-error-back*)) ; jpb 1406
          :br :br
          ,@(if caller
                `(((:A :HREF ,caller) 
                   ,(select-name omas::*web-error-zap*)))) ; jpb 1406
          :br
          )))))))

(defun omas::publish-web-error-page (agent)
  "post an error form"
  (declare (ignore agent))
  (publish 
   :path "/weberror"
   :content-type "text/html"
   ;; we should call the function attached to this page here somehow ??
   ;; used when we call the page directly...
   :function
   #'(lambda (req ent)
       (with-http-response (req ent)
         (with-http-body (req ent)
           (web-error-form "Test d'affichage" req))))
   )
  ;; seems to work without that!!!
  )

#|
(publish-web-error-page sa_web)
(web-error-page "Erreur nom d'une pipe!" "edit")
|#
;;;==============================================================================
;;;                                EXTERNAL ENTRY
;;;==============================================================================
;;; this page is called when accessing the editor from another web page, it is 
;;; needed to clean up the user entry before doing the editing
;;; possible parms:
;;; - language
;;; - agent-ref
;;; - class-ref
;;; - caller (required)
;;; - ref 
;;; - create

(defun omas::publish-web-external-entry-page (agent)
  "post an introductory index form"
  (publish 
   :path "/editentry"
   :content-type "text/html"
   :function
   #'(lambda (req ent)
       (dformat :web 0
                "~2%;========== ~A Entering edit external entry form"
                (omas::name agent))
       (with-http-response (req ent)
         (with-http-body (req ent)
           (let ((parms (remove-empty-parameters (request-query req)))
                 (ip (socket:remote-host (request-socket req))))
             (web-edit-external-entry agent ip parms)))))))

;;;------------------------------------------------------ WEB-EDIT-EXTERNAl-ENTRY

(defun web-edit-external-entry (agent ip parms)
  "examines the URL parameters. If it contains \"create\" post the edit page, ~
   otherwise calls the locator. Creates or updates the user entry, keeping some ~
   of the old values and adding the values of the URL parameters.
Arguments:
   agent: the owner of the edited object
   ip: the IP of the user's machine
   parms: the URL list of parameters
Return:
   an HTML string obtained from the post or locate functions."
  (declare (special *language*))
  (let* ()
    ;; set language as specified. Its scope will encompass all functions called
    ;; by this one. If language is not one of the parameters, then the value is
    ;; set to *language*, whatever it is.
    (set-language (get-field "language" parms))
    ;; remove then previous entry and create new one
    ;; creates a user entry in the OMAS user list
    ;; careful here: must not clobber whatever is already in the user entry
    ;; wu-keep recreates an entry and copy the old values
    (wu-keep ip '("ip" "caller" "username" "language" "agent-ref" "class-ref"
                  "pa-key" "user-name" "user-first-name" "user-email" "user-F"))
    ;; replace some of the info with the one from the URL parameters
    (wu-add ip parms)
    (dformat :web 0
             "~2%;~S: ---------- Entering web-edit-external-entry"
             (omas::name agent))
    (dformat :web 0 "~%;--- *package*:  ~S" *package*)
    (dformat :web 0 "~%;--- parms:  ~S" parms)
    (dformat :web 0 "~%;--- user-entry:~%  ~S" (wu-get-user-entry ip :no-page))
    
    (if* (wu-get ip "create")
       then
            ;; if we have a create flag, call an empty post page
            (wu-rem ip "create")
            ;; call edit page
            (web-edit-post agent ip parms)
       else   
            ;; call the locator page
            (web-locator agent ip parms))))

; (net.aserve.client:do-http-request 
;     (format nil "http://editform?IP=~S&~{~{~A=~A~}~^&~}" ip 
;       (mapcar #'(lambda(xx)(list (car xx)(cdr xx))) parms)))
; should also use URIencode on (cdr xx)

;;;===============================================================================
;;;                           WEB LOCATE FORM
;;;===============================================================================

;;;------------------------------------------------------- PUBLISH-WEB-LOCATE-FORM
;;; publish a page to edit an object, specifying the agent, its class and some
;;; property.
;;; Several things may happen:
;;;   - no object is found, an error message is posted
;;;   - a single object is found, the editor is called
;;;   - several objects are found, a choice list is posted

(defun omas::publish-web-locate-form (agent)
  "post a form to collect info to locate an object to edit."
  (publish 
   :path "/locate"
   :content-type "text/html"
   :function
   #'(lambda (req ent)
       (with-http-response (req ent)
         (with-http-body (req ent)
           (let* ((ip (socket:remote-host (request-socket req)))
                  (parms (remove-empty-parameters (request-query req)))
                  (id-ref (get-field "id-ref" parms)))
             (dformat :web 0 "~2%;========== ~S: ENTERING web-locate-form"
               (omas::name agent))
             ;; add parameters to the user entry
             (wu-add ip parms)
             ;; if we have the id-ref in the parameters, then call post directly
             (cond
              (id-ref
               (wu-add ip `(("ref" . ,id-ref)))
               (wu-rem ip "id-ref")
               (web-edit-post agent ip parms)
               )
              (t
               (web-locator agent ip parms)))))))))
             ;(net.aserve.client:do-http-request "locate")))))))
            
;;;------------------------------------------------------------------- WEB-LOCATOR
;;; the function is also called from the external entry page

(defun web-locator (agent ip parms)
  "body of the publish-locate-object-form. Sets the global *language* variable, ~
   retrieves the necessary data from the parameters to ask owner agent to locate ~
   the requested object. If several objects are retrieved, then calls a selction ~
   window.
Arguments:
   agent: the owner agent
   ip: the IP of the user's machine
   parms: the URL list of parameters
Return:
   either the same page with an error message
   or a selection page, or an edit page."
  (declare (special *language*))
  (let* ((language (get-field "language" parms))
         (*language* (or (set-language language :only-return t) *language*)) 
         (entry-language (wu-get ip "entry-language"))
         class-ref to-agent-ref to-agent-key val answer select caller)
    (dformat :web 0 "~2%;---------- ~A: Entering web-locator" (omas::name agent))
    (dformat :web 0 "~%;--- parms:~%  ~S" parms)
    (dformat :web 0 "~%;--- user entry:~%  ~S" (wu-get-user-entry ip :no-page))
    (dformat :web 0 "~%;--- *language*: ~S" *language*) 
    ;; norm entry language if there
    (if entry-language
        (setq entry-language 
              (set-language entry-language :only-return t :allow-all t)))
    (dformat :web 0 "~%;--- entry-language: ~S" entry-language)
    
    ;; get first parameters from the user entry
    (setq to-agent-ref (wu-get ip "agent-ref"))
    (setq class-ref (wu-get ip "class-ref"))
    ;; set caller to this page if available
    (setq caller (wu-get ip "caller"))
    (setq val (wu-get ip "val"))
    (setq select (wu-get ip "select"))
    
    ;; if caller is back to this page, then remove it
    (if (and caller (string-equal caller "locate")) (setq caller nil))
    
    ;; if the page is called from index and partially filled with agent and class
    ;; we need to add info to locate object to edit. However, if we leave the
    ;; field blank, then the function will call the edit page to create the object
    ;; the special parameter :init tells us that we want to stay on the locate
	;; form, and ask the user to provide additional info to locate object
    (when (wu-get ip "init")
      ;; we must remove it to avoid looping
      (wu-rem ip "init")
      ;; post form now, posting web-need-info message
      (return-from web-locator
        (web-locate-build-page
         (select-name omas::*web-need-info*) caller ip))) ; jpb 1406
    
    ;;=== if select has a value, we come back from a selection page and
    ;; must call the edit page
    (when select
      (wu-add ip `(("ref" . ,select)))
      (wu-rem ip "select")
      (return-from web-locator
        (web-edit-post agent ip parms)))
    
    ;;=== if no parameters are set, then first time around
    (unless (or to-agent-ref class-ref)
      ;; publish empty form (no message, maybe caller)
      (return-from web-locator (web-locate-build-page nil caller ip)))
    
    ;;=== if owner agent is not mentioned, complain
    (unless to-agent-ref
      (return-from web-locator
        (web-locate-build-page 
         (select-name omas::*web-missing-agent*) caller ip))) ; jpb 1406

    ;; make agent key
    (setq to-agent-key (intern (string-upcase to-agent-ref) :keyword))
    
    ;;=== check agent existence, if not available we'll get a timout
    (setq answer
          (omas::get-answer agent nil
                            :to-agent to-agent-key
                            :action :hello
                            :timeout 5
                            :allow-nil-answer t
                            :ack t))
    (dformat :web 0 "~%; web-locator /answer to hello: ~S" answer)
     ;; in case of error display the error page
    (unless answer
      (cg:beep)
      (return-from web-locator 
        (web-locate-build-page 
         (select-name omas::*web-unreachable-agent*) ; jpb 1406
         caller ip)))
    
    ;;=== if no class-ref complain
    (unless class-ref
      (return-from web-locator
        (web-locate-build-page
         (select-name omas::*web-missing-class*) ; jpb 1406
         caller ip)))
    
    ;;=== should check the validity of the class (done by skill)
    
    ;;=== if no value is specified, we assume that we want to create a new
    ;; instance of the class and post an empty form
    (when (or (null val)(equal+ val ""))
      (wu-rem ip "val")
      (return-from web-locator (web-edit-post agent ip parms)))
    
    ;;=== otherwise, ask agent to locate object
    (setq answer 
          (omas::get-answer agent `(,class-ref ,val)
                            :to-agent to-agent-key
                            :action :locate-object
                            :language (or entry-language language)
                            :timeout 10))
    (dformat :web 0 "~%; web-locator /answer to locate-object:~%  ~S" answer)
    ;;===
    ;; if error  post error inside edit page
    (when (and (listp answer)(eql (car answer) :error))
      (cg:beep)
      ;; remove val from user entry (rationale: object was not found)
      (wu-rem ip "val")
      (return-from web-locator
        (web-locate-build-page 
         (select-name omas::*web-cannot-find*) ; jpb 1406
         caller ip)))
    
    ;; if several values, call selection page
    (when (cdr answer)
      (return-from web-locator (web-locate-select answer ip)))
    
    ;; otherwise call editor, adding the reference to the user entry
    (wu-set ip "ref" (caar answer))
    (web-edit-post agent ip parms)
    ))

;;;---------------------------------------------------------- WEB-LOCATE-ADD-BODY

(defun web-locate-add-body (ip)
  "builds to body of the locate form. When parms is given, it is used to initialize ~
   some fields."
 ; (su)
  ;(print `(agent-ref ,(wu-get ip "agent-ref")))
  ;(print `(class-ref ,(wu-get ip "class-ref")))
  `(((:table :border "0" :cols "2" :width "100%")
     (:tbody
      ,(row :header (select-name omas::*web-owner-agent*) ; jpb 1406
            :name "agent-ref"
            :value (or (wu-get ip "agent-ref") ""))
      ,(row :header (select-name omas::*web-object-class*)  ; jpb 1406
            :name "class-ref"
            :value (or (wu-get ip "class-ref") ""))
      ,(row :header (select-name omas::*web-locate-data*) ; jpb 1406
            :name "val"
            :value (or (wu-get ip "val") ""))
      ;:br 
      ))))

#|
(web-locate-add-body)
(((:TABLE :BORDER "0" :COLS "2" :WIDTH "100%")
  (:TBODY 
   (:TR 
    (:TD "Agent concerné :") 
    (:TD ((:INPUT :NAME "agent-ref" :MAXLENGTH 100))))
   (:TR 
    (:TD "Classe de l'objet :") 
    (:TD ((:INPUT :NAME "class-ref" :MAXLENGTH 100))))
   (:TR 
    (:TD "Élements de localisation, ex: japon, Barthès :") 
    (:TD ((:INPUT :NAME "val" :MAXLENGTH 200)))))))

(web-locate-add-body '(("agent" . "STEVENS-NEWS")("class-ref" . "News item")))
(((:TABLE :BORDER "0" :COLS "2" :WIDTH "100%")
  (:TBODY
   "<TR><TD>Agent concerné (gérant l'objet) :</TD>
        <TD><INPUT NAME=agent-ref TYPE=text SIZE=100>STEVENS-NEWS</TD>
    </TR>"
   "<TR><TD>Classe de l'objet :</TD>
        <TD><INPUT NAME=class-ref TYPE=text SIZE=100>News item</TD>
    </TR>"
   "<TR><TD>Élements de localisation, ex: japon, Barthès :</TD>
        <TD><INPUT NAME=val TYPE=text SIZE=100></TD>
    </TR>")))
|#
;;;--------------------------------------------------------- WEB-LOCATE-BUILD-PAGE

(defun web-locate-build-page (message caller ip)
  "builds the HTML string to post the locator form, filling it in part if parms ~
   are passed to the page
Arguments:
   message: a message to post into the page
   caller: the name of the page that called this one
   ip: ip of the machine hosting the browser, index on the user entry"
  (eval
   `(html 
     (:html
      (:head 
       (:title ,(select-name omas::*web-editor-title*)) ; jpb 1406
       ((:meta :charset "UTF-8")))
      (:body 
       "<font face=\"arial\">"
       ((:form :action "locate")
        "<fieldset style=\"background-color:yellow;\">"
        :br
        ,@(if message `(,message :br))
        (:center (:h2 ,(select-name omas::*web-locate-title*))) ; jpb 1406
        ;,@(add-edit-body)
        ,@(web-locate-add-body ip)
        :br
        (:center ((:input :type "submit" 
                          :value ,(select-name omas::*web-send*)))) ; jpb 1406
        :br
        ,@(if caller 
              `((:princ ,(format nil "<a href=~S>~A</a>"
                           caller
                           (select-name omas::*web-return*))))) ; jpb 1406
        ))))))

;;;------------------------------------------------------------- WEB-LOCATE-SELECT
;;; compose a dynamic page controlled by the same entity as the locate page

(defun web-locate-select (answer ip)
  "prepares a page to choose one object to be edited. 
Arguments:Arguments:
   answer: a list (<id-string> <summary-string>)*
   req: request body of the HTTP message
Return
   nothing in particular."
  (dformat :web 0 "~2%;---------- Entering web-locate-select")
  (dformat :web 0 "~%;-- answer:~%  ~S" answer)
  
  (let* ((class-ref (wu-get ip "class-ref"))
         (caller (wu-get ip "caller")))    
    (eval
     `(html 
       (:html
        (:head 
         (:title ,(select-name omas::*web-locate-title*)) ; jpb 1406
         ((:meta :charset "UTF-8"))
         )
        (:body 
         "<font face=\"arial\">"
         ((:form :action "locate")
          "<fieldset style=\"background-color:yellow;\">"
          ;,@(if msg `((:princ ,msg) :br))
          (:center (:h2 ,(select-name omas::*web-locate-title*))) ; jpb 1406
          (:center
           ,@(web-locate-select-add-body answer class-ref)
           )
          :br
          :br
          (:center ((:input :type "submit" 
                            :value (select-name omas::*web-send*)))) ; jpb 1406
          ,@(if caller 
                `((:princ ,(format nil "<a href=~S>~A</a>" 
                             caller
                             (select-name omas::*web-return*))))) ; jpb 1406
          :br
          )))))
    )
  )

;;;---------------------------------------------------- WEB-LOCATE-SELECT-ADD-BODY
;;; produces an HTML SELECT form to choose an object

(defun web-locate-select-add-body (answer class-ref)
  "produces an HTTP SELECT clause for calling the edit form with the selection.
Arguments:
   answer a list  (<id-string> <summary-string>)*
   class-ref: e.g. \"contact\"
Return:
   an HTML SELECT clause"
  (declare (ignore class-ref))
  `(,(format nil "<select name=select size=10> ~{~A ~} </select>"
       (mapcar #'(lambda(xx) (format nil "<option value=~A>~A</option>"
                              (car xx) (cadr xx)))
        answer)
    ))
  )

;;;===============================================================================
;;;
;;;                      Functions for creating an object
;;;                       when using the Web Editor
;;;
;;;===============================================================================

;;; It should be possible to write generic functions for creating objects when 
;;; no special side effects are necessary

;;; TO DO

;;;===============================================================================
;;;
;;;                      Functions for updating an object
;;;                       when using the Web Editor
;;;
;;;===============================================================================

;;;----------------------------------------------------------- OMAS::UPDATE-OBJECT
;;; This function executes in the environment of the agent owning the objects

(defun omas::update-object (agent class-ref object-description page-description 
                                  program)
  "takes an update program and executes it in the owner agent environment to ~
   modify an object taking data from an object description and additional info ~
   from the edit page description. Catches errors with :update-error.
Arguments:
   agent: agent to which the object belongs
   class-ref: class of the object to be edited
   object-description: a description of the object using only strings
   page-description: a description of the fields of the edit page
   program: a set of triple containing update instructions
Return:
   the object description of the updated object."
  (dformat :web 0
           "~2%;---------- ~A: update-object (default function)" 
           (omas::name agent))
  (dformat :web 0 "~%;--- *language*: ~S" *language*)
  (let (id fn result)
    (catch 
     :update-error
     ;; preprocessing consists of fusing ADD and DEL instructions for a given 
     ;; property with multiple values
     (setq program
           ;; then process :DEL and :ADD instructions, separate multiple values
           ;; and cancel :ADD and :DEL with identical values
           (omas::update-preprocess-add-del program class-ref))
     
     ;; split instructions to obtain only one value per instruction
     (setq program (omas::update-split-instructions program))
     (dformat :web 0 "~2%; update-object /final program ~%~S" program)
     
     ;; now execute the program distinguishing :MOD from :ADD/:DEL
     (dolist (item program)
       (omas::update-add-del agent item page-description object-description))
     
     ;;== finally execute post functions We assume that we are replacing values
     ;; into an attribute field
     (dolist (field page-description)
       (setq fn (getf (cdr field) :postfcn))
       (when fn
         (push `(,(getf (cdr field) :name) :SET ,(funcall fn)) result)))
     
     (dformat :web 1 "~%; update-object /result: ~%~S" result)
     ;; if we have something execute it
     (dolist (item (reverse result))
         (omas::update-add-del agent item page-description object-description))
         
     ;; return the object-description of the modified object
     (setq id (intern (string-upcase  
                       (car (get-field "id" (cdr object-description))))))
     (send id '=make-object-description)
     )))

;;;--------------------------------------------------------- OMAS::UPDATE-ADD-DEL
;;; an :ADD or :DEL command may apply to an attribute or to a relation
;;; when applying to an attribute, we simply execute the corresponding method
;;; when it applies to a relation, we must first locate the neigboring object
;;; the first element in the instruction is either the name of the property
;;; (attribute or relation) like "ville" or "correspondant UTC", or a string 
;;; from the object description like "sigle partenaire". In the last case we must
;;; retrieve the name of the relation from the page description:
;;;("CONTACT" 
;;;  ...
;;;     (:row :header "Sigle de l'organisation partenaire" :name "sigle partenaire"
;;;           :edit ("partenaire" "organisation" "sigle"))
;;; ...)
;;;
;;; when deleting a neighbor (:DEL instruction), we retrieve the neighbor's id from
;;; the object description:
;;;   (... ("correspondant" ("Sugawara: Kenji" "personne" "$E-PERSON.39")
;;;                     ("Fujita: Shigeru" "personne" "$E-PERSON.18")) ...)
;;; here "correspondant" is the prop-ref and the triple is 
;;;          (<value> <neighbor class><neighbor id>)
;;;
;;; when adding a neighbor we must first locate the neighbor and add a link to it
;;; we must first "de-summarize" the value to recover something that can be used to
;;; locate the neighbor, e.g.
;;;    "Barthès: J.-P.A." -> (("nom" "Barthès")("initiales" "J.-P.A."))


(defun omas::update-add-del (agent instruction page-description object-description)
  "takes an instruction and modifies the value of the attribute or the link to ~
   the specific neighbor.
Arguments:
   agent: agent owning the object
   instruction: a triple (<field-ref> <op> <value>)
   page-description: the description of the edit page associated with the class-ref
   object-description: the description of the object w.r.t. this page"
  (declare (special *language*))
  (let (prop-ref id suc-id val-type val new-val)
    ;;=== first we must make sure to have the right property name since for some
    ;; fields the associated tag (field-ref) is not the name of the relation, e.g. 
    ;; "sigle partenaire" instead of "partenaire"
    ;; property is the first element of the path or the car of instruction when
    ;; there is no path (attributes)
    (setq prop-ref (omas::update-extract-prop-ref (car instruction) page-description))
    (dformat :web 1 "~%; omas::update-add-del /prop-ref: ~S" prop-ref)
    
    ;; get the id of the current object, e.g. $E-CONTACT.21
    (setq id (intern (string-upcase  
                      (car (get-field "id" (cdr object-description))))))
    ;; if not there should throw with an string describing the error
    (dformat :web 1 "~%; omas::update-add-del /id: ~S" id)
    
    ;;;**********
    ;; careful here we need to process the case when values are MLNs, in which
    ;; case we do not add or delete the whole value but modify the value
    ;; corresponding to the current language
    ;; first check if the current value is of type MLN
    (setq val-type (omas::update-extract-value-type
                    (car instruction) page-description))
    (dformat :web 1 "~%; omas::update-add-del /val-type: ~S" val-type)
    
    ;;=== check attribute or relation
    (cond
     ;; if attribute execute instruction
     ;; if value is MLN, must be careful
     ((and (moss::%is-attribute? prop-ref)(eql val-type :mln))
      (setq val (car (send id '=get prop-ref)))
      (setq new-val
            (case (cadr instruction)
              (:ADD
               (mln::add-value val (caddr instruction) *language*))
              (:DEL
               (mln::remove-value val (caddr instruction) *language*))
              ;; set is a brute-force replacement of whatever was there with the
              ;; value from the instruction (used with :postfcn option)
              (:SET
               (caddr instruction))
              ))
      (dformat :web 1 "~%; omas::update-add-del /new-val: ~S" new-val)
      
      (with-transaction agent
          (send id '=replace prop-ref (list new-val)))
      (return-from omas::update-add-del :done))
      
      ;; attribute but value not of type MLN
      ((moss::%is-attribute? prop-ref)
       (case (cadr instruction)
         (:ADD
          ;(send id '=replace prop-ref (cddr instruction)))
          (with-transaction agent
            (send id '=add-attribute-values prop-ref (cddr instruction))))
         
         (:DEL
          (with-transaction agent
            (send id '=delete-attribute-values prop-ref (cddr instruction))))
         (:SET
          (with-transaction agent
            (send id '=replace prop-ref (caddr instruction))))
         )
       (return-from omas::update-add-del :done))
      
      ;; if relation, more difficult
      ((moss::%is-relation? prop-ref)
       ;; must first locate the neihgbor from the value
       (case (cadr instruction)
         (:ADD
          ;; we must first locate the object to add from the given value
          ;; if it does not exist we do not create it
          (setq suc-id 
                (omas::update-add-del-locate-successor 
                 instruction page-description object-description agent id))
          ;;********** warn if several successors
          (dformat :web 0
                   "~%; omas::update-add-del /:ADD, suc-id-list: ~S" suc-id)
          (when suc-id
            (with-transaction agent
              (send  id '=add-related-objects prop-ref suc-id)))
          )
         (:DEL
          ;; locate the successor to remove
          (setq suc-id 
                (omas::update-add-del-locate-successor 
                 instruction page-description object-description agent id))
          (dformat :web 1
                   "~%; omas::update-add-del /:DEL, suc-id-list: ~S" suc-id)
          (when suc-id
            (with-transaction agent
              (send id '=delete-related-objects prop-ref suc-id)))
          )
         )
       (return-from omas::update-add-del :done))
      (t
       (error "bad property: ~S" prop-ref)))  
     ))

;;;---------------------------------------- OMAS::UPDATE-ADD-DEL-LOCATE-SUCCESSOR
;;;
;;; ("Contact" ("type de contact" "suivi") ("date de début" "2000") ("id" "$E-CONTACT.21")
;;;  ("ville" ("Tsudanuma" "ville" "$E-CITY.15")) ("pays" ("Japon" "pays" "$E-COUNTRY.89"))
;;;  ("partenaire" ("CIT" "organisation" "$E-ORGANIZATION.2"))
;;;  ("correspondant" ("Sugawara: Kenji" "personne" "$E-PERSON.39")
;;;   ("Fujita: Shigeru" "personne" "$E-PERSON.18"))
;;;  ("correspondant UTC" ("Barthès: Jean-Paul" "personne" "$E-PERSON.3")
;;;   ("Moulin: Claude" "personne" "$E-PERSON.27")))
;;;
;;;  ...
;;;     (:row :header "Sigle de l'organisation partenaire" :name "sigle partenaire"
;;;           :edit ("partenaire" "organisation" "sigle"))
;;; ...)
  
(defun omas::update-add-del-locate-successor 
    (instruction page-description object-description agent id)
  "tries to locate an object in the environment. Will create ancillary objects ~
   provided the :create option is mentioned in the page description entry and ~
   the property has a =parse-summary method.  
Arguments:
   instruction: a triple like (\"partenaire UTC\" :ADD \"Barthès: Jean-Paul\")
     or (\"partenaire UTC\" :DEL \"Moulin: C.\")
   field-description: the description of the edit page field from which the value
     was retrieved
   object-description: description of the object
   agent: owner of the object; used in transaction
   id: id of the main object (used to index messages)
Return:
   a list of id(s) of found object(s) or NIL if it cannot be found."
  (declare (ignore object-description option))
  (let (class-ref class-id parms path suc-id-list field-description option value-list)
    ;; get successor class from page-description, e.g.
    ;;   ("partenaire" "organisation" "sigle")
    (setq path (omas::update-extract-edit-path (car instruction) page-description))
    
    ;; get the class name
    (setq class-ref (cadr path))
    ;; get the corresponding class-id
    (setq class-id (moss::%%get-id-for-class class-ref))
    (dformat :web 0
             "~%; omas::update-add-del-locate-successor /class-id: ~S" class-id)
    
    ;; de-summarize value, should return a list of pairs: prop value
    ;; e.g. "Barthès: Jean-Paul" => (("nom" "Barthès")("prénom" "Jean-Paul"))
    ;; but "Conferences") ->
    (setq parms (send class-id '=parse-summary (caddr instruction)))
    (dformat :web 0 "~%; omas::update-add-del-locate-successor /parms: ~S" parms)
    ;; when =parse-summary is not defined the default method returns the input value
    ;; when =parse-summary returns the input string, then one cannot build a MOSS
    ;; query but we can try to use locate-objects
    
    ;; =parse-summary can return (("libellé" "Lille")("label" "Rijssel"))
    ;; therefore the access should be an OR query, i.e.
    ;; ("mot clé" (OR ("libellé" :is "lille")("label" :is "rijssel")))
    ;; however, this could be strange for some instance, e.g. persons where
    ;; =parse-summary returns ("nom" "Barthès")("prénom" "Jean-Paul") for which
    ;; clearly we should have an AND in the query.
    ;; to cover both cases it is better to use locate-objects that does a graph
    ;; search with
    ;;  (locate-objects '("lille" "Rijssel") "mot clé")
    ;;  (locate-objects '("Barthès" "Jean-Paul") "personne")
    
    ;; another problem encountered in the NEWS application is related to MLN values
    ;; e.g. (locate-objects '(((:en "Conferences"))) "Category")
    ;; if the value is an MLN, then locate-objects does not work, thus we 
    ;; preprocess parms to make a list of strings
    
    (setq value-list 
          (if (listp parms)
              ;; get all values
              (mapcar #'cadr parms)
            ;; otherwise use string
            (cddr instruction)))
    ;; if some of the values are MLN, then retrieves the corresponding strings and
    ;; merge with the other values
    (setq value-list
          (reduce
           #'append
           (mapcar #'(lambda (xx) (if (mln::mln? xx)
                                      (mln::extract xx :language *language*)
                                    (list xx)))
             value-list)))
    (dformat :web 0
             "~%; omas::update-add-del-locate-successor /value-list: ~S" value-list)
    
    (setq suc-id-list (locate-objects value-list class-ref))
    
    ;; now the question is: do we modify the successor if it does not possess the
    ;; values that we specified, e.g. if the Lille keyword has no Rijssel label?
    ;; we can do that by making a query, and if the answer is nil, then the object
    ;; is missing some value, e.g. Rijssel, of the Jean-Paul. We run the risk to
    ;; add a value to the wrong object, e.g. Jean-Paul to a person named Barthès
    ;; that has no first name but different from Jean-Paul Barthès...
    ;; I decide to run the risk.
    (omas::update-update-successors-if-needed agent suc-id-list parms)
    (dformat :web 0
             "~%; omas::update-add-del-locate-successor /suc-id-list: ~S" 
             suc-id-list)
    
    ;; if we cannot find any object, then we check the :create option
    ;; unless the instruction is to delete the object
    (when (and (null suc-id-list) (listp parms) (not (eql (cadr instruction) :DEL)))
      ;; if we cannot find objects we may be able to create one, provided that the
      ;; option :if-does-not-exist is set to :create, AND WE HAVE A =PARSE-SUMMARY
      ;; METHOD TO PREPARE THE LIST OF PROPERTIES
      (setq field-description 
            (omas::update-extract-field-description (car instruction) page-description))
      (setq option (getf (cdr field-description) :if-does-not-exist))
      
      (dformat :web 0
               "~%; omas::update-add-del-locate-successor /option: ~S" option)
      (when (eql option :create)
        (with-transaction agent
          (setq suc-id-list (list (apply #'make-individual (cadr path) parms))))
        (dformat :web 0
                 "~%; omas::update-add-del-locate-successor /suc-id-list: ~S" 
          suc-id-list)
        ;; record the fact that we created an ancillary object
        (omas::wrnmsg-add 
         agent id (format nil "I created a new ~A individual: ~S" 
                    (cadr path) (car (send (car suc-id-list) '=summary))))))
    
    ;; return what we could find
    suc-id-list
    ))

#|
(setq page-description (cdr (get-field "contact" contact::*edit-layouts*)))
(setq object-description (send 'contact::$E-contact.21 '=make-print-string-with-refs))
(setq instruction '("correspondant UTC" :add "Barthès: Jean-Paul"))
(omas::update-add-del-locate-successor instruction page-description object-description)
|#
;;;------------------------------------------------ OMAS::UPDATE-EXTRACT-EDIT-PATH

(defun omas::update-extract-edit-path (field-ref page-description)
  "takes a field descriptor and returns the list associated with the :edit parm ~
   if there, NIL otherwise.
Arguments;
   field-ref: e.g. \"ville\" or \"sigle partenaire\" that are not necessarily prop ~
      refs but correspon to the :name parameter of the page description
   page-description: description of an edit page
Return:
   the list corresponding to the :edit parm or nil"
  (let ((line (omas::update-extract-field-description field-ref page-description)))
    ;; if nil returns nil
    (when line
      (getf (cdr line) :path))))

#|
(omas::update-extract-edit-path "sigle partenaire" page-description)
("partenaire" "organisation" "sigle")
|#
;;;---------------------------------------- OMAS::UPDATE-EXTRACT-FIELD-DESCRIPTION

(defun omas::update-extract-field-description (field-ref page-description)
  "takes a field ref and a page description and returns the field description.
Arguments;
   field-ref: e.g. \"ville\" or \"sigle partenaire\" that are not necessarily prop ~
      refs but correspond to the :name parameter of the page description
   page-description: description of an edit page
Return:
   the list corresponding to the description."
  (car (member field-ref page-description :key #'(lambda(xx)(car (cddddr xx)))
               :test #'string-equal)))

#|
(omas::update-extract-field-description "sigle partenaire" page-description)
(:ROW :HEADER "Sigle de l'organisation partenaire" :NAME "sigle partenaire" :EDIT
 ("partenaire" "organisation" "sigle"))
|#
;;;------------------------------------------------- OMAS::UPDATE-EXTRACT-PROP-REF

(defun omas::update-extract-prop-ref (field-ref page-description)
  "takes a field description and a page description and returns the relation ref ~
   i.e. the first element of the value-list attached to :edit or the field-ref ~
   that is the property ref when there is no :edit parm.
Arguments;
   field-ref: e.g. \"ville\" or \"sigle partenaire\" that are not necessarily prop ~
      refs but correspond to the :name parameter of the page description
   page-description: description of an edit page
Return:
   a string being the prop ref."
  (or (car (omas::update-extract-edit-path field-ref page-description))
      field-ref))

#|
(omas::update-extract-prop-ref 
 "ville" (setq page-description (get-field "contact" contact::*edit-layouts*)))
"ville"
(omas::update-extract-prop-ref "sigle partenaire" page-description)
"partenaire"
(omas::update-extract-prop-ref "type de contact" page-description)
"type de contact"
|#
;;;----------------------------------------------- OMAS::UPDATE-EXTRACT-VALUE-TYPE

(defun omas::update-extract-value-type (field-ref page-description)
  "extracts the type of the value associated with the property. Used to check if ~
   the type is MLN, which requires special handling.
Arguments;
   field-ref: e.g. \"ville\" or \"sigle partenaire\" that are not necessarily prop ~
      refs but correspond to the :name parameter of the page description
   page-description: description of an edit page
Return:
   :mln or :unspecified (aka :string)"
  (let ((field (car (member field-ref page-description 
                            :key #'(lambda(xx)(car (cddddr xx)))
                            :test #'string-equal))))
    (or (getf (cdr field) :type) :unspecified)))

#|
(omas::update-extract-value-type "pays" (get-field "contact" contact::*edit-layouts*)))
:MLN
(omas::update-extract-value-type "commentaire" (get-field "contact" contact::*edit-layouts*)))
:UNSPECIFIED
|#
;;;------------------------------------------------ OMAS::UPDATE-PREPROCESS-ADD-DEL
;;;********** BUG: when several values are the same and we have something like
;;;(omas::update-preprocess-add-del 
;;; '(("libellé" :DEL "manteau, veste, veste,  veste")
;;;   ("libellé" :ADD "manteau, veste")) "mot clé")
;;; then the answer is nil instead of (("libellé" :DEL "veste" "veste"))

(defun omas::update-preprocess-add-del (program class-ref)
  "consists of separating multiple values, and resolving DEL and ADD commands~
   on the same property, keeping only one, to obtain a non ambiguous update ~
   program."
  (let (val result prop-id max item)
    
    ;; unless property is defined with a maximal cardinality of one, split
    ;; multiple values (default separator is comma)
    (dolist (item program)
      ;; check single command e.g. 
      ;;    (\"correspondant\" :DEL \"_,Sugawara: Kenji, Fujita: S.\")
      ;; splitting the multivalued data to obtain
      ;;    (\"correspondant\" :DEL \"Sugawara: Kenji\" \"Fujita: S.\")
      (setq val (caddr item))
      ;; get property
      (setq prop-id (moss::%%get-id (car item) :property :class-ref class-ref))
      (dformat :web 1
               "~%;++ preprocess-add-del /val: ~S, prop-id: ~S" val prop-id)
      ;; get max number of values
      (setq max (car (send prop-id `=get "MOSS-MAXIMAL-CARDINALITY")))
      ;; unless for single-valued properties (:max 1), we check for #\, in the
      ;; string and split the string into substrings
      (if (and (numberp max)(= max 1))
          ;; don't do anything
          (push item result)
        ;; split multiple values
        (push `(,(car item) ,(cadr item) ,@(omas::update-split-val val)) result)
        )
      )
  
    ;; reassemble
    (setq program (reverse result)
        result nil)
    (dformat :web 0 "~%;++ preprocess-add-del /program:~% ~S" program)
    ;; now merge DEL and ADD
    (loop
      (setq item (car program))
      (unless item (return))
      (unless (cdr program)
        (push item result)
        (return))
      ;; check if next entry is same property
      (if (string-equal (caar program)(caadr program))
          ;; if so go merge both commands
          (setq result 
                (append (reverse (omas::update-trim-instructions
                                  (pop program)(pop program)))
                        result))
        ;; otherwise go to next item
        (push (pop program) result))
      )
    (dformat :web 0 "~%;++ preprocess-add-del /final program:~% ~S" (reverse result))
    ;; return result
    (reverse result)
    )
  )

#|
(omas::update-preprocess 
 '(("correspondant UTC" :DEL "Barthès: Jean-Paul, Moulin: Claude")
   ("correspondant UTC" :ADD "Barthès: Jean-Paul, Abel: M.-H.")
   )
 "contact")
(("correspondant UTC" :DEL "Moulin: Claude")
 ("correspondant UTC" :ADD "Abel: M.-H."))

(omas::update-preprocess-add-del 
 '(("keywords" :ADD "handball, fencing, football"))
 "keyword")
(("keywords" :ADD "handball" "fencing" "football"))

(omas::update-preprocess-add-del '(("libellé" :DEL "manteau, veste, veste,  veste")
                                   ("libellé" :ADD "manteau, veste")) "mot clé")
NIL
|#
;;;--------------------------------------------------- OMAS::UPDATE-PREPROCESS-MOD
;;;
;;;(defun omas::update-preprocess-mod (program class-ref)
;;;  "checks and processes modifications with multiple values. Currently not implemented"
;;;  (declare (ignore class-ref))
;;;  program)

;;;----------------------------------------------- OMAS::UPDATE-SPLIT-INSTRUCTIONS

(defun omas::update-split-instructions (program)
  "takes a program and split instructions that have several values"
  (let (prop-action result)
    (dolist (item program)
      (cond
       ((>= (length item) 3)
        (setq prop-action (subseq item 0 2))
        (dolist (val (cddr item))
          (push (append prop-action (list val)) result)))
       (t 
        (push item result))))
    (reverse result)))

#|
(omas::update-split-instructions 
 '(("category" :add "sparts")
   ("keywords" :add "handball" "fencing" "football")
   ("text" :del "text 1")
   ("text" :add "text 2")))
|#
;;;-------------------------------------------------------- OMAS::UPDATE-SPLIT-VAL

(defUn omas::update-split-val (text &optional (separator #\,))
  "Takes a string, consider it as a synonym string and extract items separated ~
   by a semi-column. Returns the list of string items.
Arguments:
   text: a string
Return
   a list of strings."
  (let (pos result word)
    (unless (and text (stringp text)) (return-from omas::update-split-val text))
    (loop
      ;; remove trailing blanks
      (setq text (string-trim '(#\space) text))
      ;; is there any space left?
      (setq pos (position separator text))
      (unless pos
        (push text result)
        (return-from omas::update-split-val (reverse result)))
      ;; extract first word
      (setq word (string-trim '(#\space) (subseq text 0 pos))
          text (subseq text (1+ pos)))
      (push word result)
      )))

#|
(omas::update-split-val "Barthès: J.-P., Sugawara: Kenji, Fujita: S.")
("Barthès: J.-P." "Sugawara: Kenji" "Fujita: S.")
(omas::update-split-val "Harare")
("Harare")
|#
;;;------------------------------------------------ OMAS::UPDATE-TRIM-INSTRUCTIONS

(defun omas::update-trim-instructions (la lb)
  "takes two instructions and remove common items to avoid deleting them and ~
   adding them again."
  (let ((vala (cddr la))
        (valb (cddr lb))
        ll)
    (setq ll vala)
    (dolist (item ll)
      (when (member item valb :test #'string-equal)
        ;; remove item from both lists, but only once!
        (setq vala (remove item vala :test #'string-equal :count 1)
            valb (remove item valb :test #'string-equal :count 1))))
    
    ;; return updated list
    (cond
     ((and (null vala)(null valb)) nil)
     ;; if one is empty return the other
     ((null vala) `((,(car lb) ,(cadr lb) ,@valb)))
     ((null valb) `((,(car la) ,(cadr la) ,@vala)))
     ;; if none is empty return both
     (t `((,(car la) ,(cadr la) ,@vala)(,(car lb) ,(cadr lb) ,@valb))))))

#|
(omas::update-trim-instructions '("pays" :del "france") '("pays" :add "italie"))
(("pays" :DEL "france") ("pays" :ADD "italie"))

(omas::update-trim-instructions '("pays" :del "france") 
                                '("pays" :add "france" "italie"))
(("pays" :ADD "italie"))

(omas::update-trim-instructions '("pays" :del "france" "suisse") 
                                '("pays" :add "france" "italie"))
(("pays" :DEL "suisse") ("pays" :ADD "italie"))

(omas::update-trim-instructions '("pays" :del "france" "suisse") 
                                '("pays" :add "france"))
(("pays" :DEL "suisse"))
(omas::update-trim-instructions '("libellé" :DEL "manteau" "veste" "veste" "veste") 
                                '("libellé" :ADD "manteau" "veste"))
(("libellé" :DEL "veste" "veste"))
|#
;;;-------------------------------------- omas::update-update-successors-if-needed
;;; Should be careful: will not work when values are MLNs

(defun omas::update-update-successors-if-needed (agent suc-id-list parms)
  "we check each successor in turn and add missing attributes.
Arguments:
   suc-id-list: list of objects
   parms: list of attribute/value pairs
Return
   :done"
  (let (val-list)
    ;; for each successor object
    (dolist (suc-id suc-id-list)
      ;; for each property-value pair
      (dolist (item parms)
        ;; list of values attached to property in existing object
        (setq val-list (send suc-id '=get (car item)))
        ;; if specified value not in the existing object
        (unless (member (cadr item) val-list :test #'equal+)
          ;;...add it to the existing object
          (with-transaction agent
            (send suc-id '=add-attribute-values (car item) (cdr item))))
        ))
    :done))

#|
(omas::update-update-successors-if-needed an::sa_albert-news
  '(an::$e-mot-clé.5) '(("label" "Chiba Institute")))
|#
;;;===============================================================================
;;; publish makes a page and sets up a function called when someone visits the
;;; web page. Example:
#|
(publish 
 :path "/queryform2"
 :content-type "text/html"
 :function
 #'(lambda (req ent)
     (let ((name (cdr (assoc "name" (request-query req) 
                             :test #'equal))))
       (with-http-response (req ent)
         (with-http-body (req ent)
           (if* name
              then ; form was filled out, just say name
                   (html (:html
                          (:head (:title "Hi to " (:princ-safe name)))
                          (:body "Your name is "
                                 (:b (:princ-safe name)))))
              else ; put up the form
                   (html (:html
                          (:head (:title "Tell me your name"))
                          (:body
                           ((:form :action "queryform")
                            "Your name is "
                            ((:input :type "text"
                                     :name "name"
                                     :maxlength "20"))))))))))))
|#

(format t "~&;*** OMAS v~A - Web server loaded ***" omas::*omas-version-number*)

;;; :EOF