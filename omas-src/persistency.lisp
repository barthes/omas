﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/01/01
;;;           O M A S - P E R S I S T E N C Y (file persistency.lisp)
;;;
;;;===============================================================================
;;; This file contains functions to ensure persistency, using Allegrocache from ACL.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| 
For a given Lisp environment containing several agents (application), a single
database is defined in which partitions are installed one per agent. The name
of the partition is the keyword representing the agent ontology. The name of
the database is built from the application name (e.g. NEWS -> ACL-NEWS-ODB or
MCL-NEWS.odb). The application name can be obtained from (coterie-name *omas*),
the ontology name from (ontology-package *omas*).

Persistency is associated with a particular agent. The corresponding database 
handle is recorded in the (omas::base agent) and in (omas-database *omas*)
Functions:
  CLEAR-BASE agent: clears the content of the database
  CLOSE-BASE agent: does nothing, indeed other agents could be connected to the 
        base
  DUMP-BASE agent &key file-patname area: dumps the content of the data base
  EXPORT-BASE agent file-pathname area: exports database content into an ontology 
        file
  LOAD-OBJECT agent key: load the abject setting key (eventually to nil)
  OPEN-DATABASE agent file-name: opens the database and stores the handle into the
        base slot of the agent, creates base if it does not exist
  PRINT-BASE agent &optional no-values: prints the content of the database
  STORE-OBJECT agent key &optional value: store the object default value is value 
        of key
  RESTORE-BASE agent: reloads a database partition from a text file dump

Transactions are handled by the following agent methods
  AGENT-START-CHANGES
  AGENT-ABORT-CHANGES
  AGENT-COMMIT-CHANGES
or the macro
  WITH-TRANSACTION
  
  
2020
 0202 restructuring OMAS as an adf system
|#

(in-package :omas)

;;;===============================================================================
;;;                               AGENT METHODS
;;;===============================================================================

;;;---------------------------------------------------------- AGENT-ABORT-CHANGES

(defMethod agent-abort-changes ((agent agent))
  "reset objects to previous state.
Argument:
   agent: specific agent
Return:
   :done"
  (when (and (persistency agent)(editing agent))
    (format t "~%; agent-abort-changes /agent: ~S" (name agent))
    (let ((editing-box-var (intern "*EDITING-BOX*"))
          editing-box)
      
      (when (or (not (boundp editing-box-var))
                ;; set editing-box var to current editing-box
                (null (setq editing-box (symbol-value editing-box-var))))
        (error "agent ~S wants to abort changes and no *EDITING-BOX*" (name agent)))
      
      ;; make all new object ids unbound (the values will be gc'ed)
      ;; remember editing-box was set to right value
      (mapc #'(lambda (xx) (makunbound xx))
        (moss::new-object-ids editing-box))
      ;; restore all old values to previous state
      (mapc  #'(lambda (xx) (set (car xx)(cdr xx)))
        (moss::old-object-values editing-box))
      ;; must do a database commit now. NO since we changed nothing
      ;(moss::commit)
      
      ;; just to make sure
      (setf (moss::active editing-box) nil)
      ;; release editing box (this is more radical) setting *editing-box* to nil
      (set editing-box-var nil)
      ;;reset editing flag
      (setf (editing agent) nil)
      :done)))

;;;--------------------------------------------------------- AGENT-COMMIT-CHANGES

(defMethod agent-commit-changes ((agent agent))
  "commits all changes. Should save the objects onto disk if persistency.
Argument:
   agent: agent object
Return:
   :done"
  (when (and (persistency agent)(editing agent))
    (format t "~%; agent-commit-changes /agent: ~S" (name agent))
    (let ((editing-box-var (intern "*EDITING-BOX*"))
          (area-key (key agent))
          editing-box)
      
      (when (or (not (boundp editing-box-var))
                (null (setq editing-box (symbol-value editing-box-var))))
        (error "agent ~S wants to commit and no *EDITING-BOX*" (name agent)))
      ;(break "agent-commit-changes 1")      
      ;;********** we should do the following within a database transaction to
      ;; make sure that other agents don't get in the way
      ;; this is necessary if we want to keep the object store in a consistent
      ;; state in case we have a crash while saving objects
      (moss::db-with-transaction 
       
       (format t "~%;=== agent-commit-changes / new-objects:~%  ~S~%; ~
                  old-objects:~%  ~S" 
         ;; don't print values...
         (moss::new-object-ids editing-box) ; JPB1601
         (mapcar #'car
           (remove (intern "$SYS.1") ; JPB1601
                   (moss::old-object-values editing-box) :key #'car)))
       
       ;; for persistent agent database is always opened or should be
       ;; save new objects
       (dolist (item  (moss::new-object-ids editing-box))
         (moss::db-store item (symbol-value item) area-key :no-commit t))
       ;;
       (dolist (item  (moss::old-object-values editing-box))
         (moss::db-store (car item) (symbol-value (car item)) area-key
                         :no-commit t))       
       ;; database commit is done now
       )
      ;(break "agent-commit-changes 2")   
      ;; signal end of OMAS transaction
      (setf (moss::active editing-box) nil)
      ;; release editing box
      (set editing-box-var nil)
      ;;reset editing flag
      (setf (editing agent) nil)
      :done)))

;;;------------------------------------------------------------ AGENT-START-CHANGES

(defMethod agent-start-changes ((agent agent))
  "sets the editing flag for an agent only if persistent.
Argument:
   agent: agent object
Return:
   t"
  (when (persistency agent)
    ;; we create a global variable in the agent package to record all additions and
    ;; changes
    (let ((editing-box-var (intern "*EDITING-BOX*")))
      (set editing-box-var (make-instance 'moss::EDITING-BOX))
      (format t "~%; agent-start-changes /*package*: ~S" *package*)
      ;; mark editing box as active
      (setf (moss::active (symbol-value editing-box-var)) t)
      ;; mark agent as editing
      (setf (editing agent) t))))

;;;=============================== Creating functions ============================

;;;-------------------------------------------------------------------- CLEAR-BASE

(defMethod clear-base ((agent agent))
  "clears the content of the database (careful there). Does not close the base.
Arguments:
   none
Return:
   :done"
  (moss::db-clear-all (key agent)))

;;;-------------------------------------------------------------------- CLOSE-BASE

(defMethod close-base ((agent agent))
  "closes the hash store, e.g. when exiting the application.
Arguments:
   none
Return:
   :done"
  (moss::db-close))

;;;--------------------------------------------------------------------- DUMP-BASE
;;; 

(defmethod dump-base ((agent agent))
  "prints the content of the database as pointed pairs <key . value> into a text ~
   file named like FINAINCING-DUMP-121126.lisp
Argument:
   agent: current agent for which we dump ontology and knowledge base
Return:
   :done or nil in case of failure."
  (let (file-pathname)
    ;; execute only if agent is persistent
    (when (persistency agent)
      ;; make file pathname
      (setq file-pathname 
            (make-pathname 
             :name (format nil "~A-DUMP-~A" (key agent)
                     (moss::get-current-date :compact t))
             :type "lisp"
             :defaults (omas-application-directory *omas*)))
      ;; if file exists, complain
      (if (probe-file file-pathname)
          (progn
            (%beep)
            (warn "dump file ~A already exists" file-pathname))
        ;; otherwise, call the make function in the right package
        (with-package (find-package (key agent))
          (moss::db-export (omas-database *omas*) (key agent) 
                           :file-path file-pathname)))
      ;; return file pathname
      file-pathname)))

;;;------------------------------------------------------------------- EXPORT-BASE
;;; should be modified to use other formats that the DUMP pair format, e.g. XML?

(defmethod export-base ((agent agent))
  "exports the content of the agent partition into a text file located in the ~
    application folder, e.g. FINANCING-ONTOLOGY-120616.lisp.
	Warning if file already exists (not superseded).
Arguments:
   none
Return:
   pathname of the file if success, nil if failure"
  (let (file-pathname) 
    ;; execute only if agent is persistent
    (when (persistency agent)
      ;; if so make file pathname
      (setq file-pathname 
            (make-pathname 
             :name (format nil "~A-ONTOLOGY-~A" (key agent)
                     (moss::get-current-date :compact t))
             :type "lisp"
             :defaults (omas-application-directory *omas*)))
      ;; if file exists, complain
      (if (probe-file file-pathname)
          (progn
            (%beep)
            (warn "export file ~A already exists" file-pathname))
        ;; otherwise, call the make function in the right package
        (with-package (find-package (key agent))
          (moss::db-export
           (omas-database *omas*) ; database pathname (one per OMAS environment)
           (key agent) ; area key
           :file-path file-pathname)))
      ;; return file pathname
      file-pathname)))
		  
;;;------------------------------------------------------------------- LOAD-OBJECT

(defMethod load-object ((agent agent) key)
  "reads a key from the hash store. Set it to value returning its key or value.
Arguments:
   key: key of data to get
Return:
   value"
  (moss::db-load key (key agent)))

;;;----------------------------------------------------------------- OPEN-DATABASE
;;; the name of the persistent database is obtained from the name of the application
;;; saved in (coterie-name *omas*). 
;;; When an agent calls open-database, 3 cases:
;;;   - the database exists as well as the agent partition
;;;   - the database exists but not the agent parttion
;;;   - the database does not exist
;;; In the first case we open the database and do nothing else, this initializes 
;;; the *database-pathname* variable in MOSS.
;;; In the second case we create the partition and send a warning.
;;; In the third case, we create the database, the partition and send a warning.

;;;********** whe an agent is created directly in cg-user no application exists
;;; thus, it will be an error to declare it persistent...
;;;********** should be fixed
;;; create an OMAS-AGENTS base in the omas directory one level up from the fasl
;;; with partition corresponding to agent key.

(defMethod open-database ((agent agent))
  "opens the persistent file.
Arguments:
   filename (key): name of object store (default Omas.wood)
   if-does-not-exist (key): what to do if file does not exist (default :create)
Return:
   nil if error, :done if exist and partition exists, :empty if partition had
   to be created."
  (unless (persistency agent)
    (warn "Can't open a database because agent ~S is not persistent." (name agent))
    (return-from open-database))
  
  ;; set application name to the name of the application or t OMAS-AGENTS if
  ;; the agent is created directly in the user debugging/listener window
  ;; In that case the directory will be the OMAS directory (birrkkh!)
  (let ((application-name  (or (coterie-name *omas*) "OMAS-AGENTS"))
        (application-directory (or (omas-application-directory *omas*)
                                   (omas-directory *omas*)))
        db-pathname)
    ;; OK, cook up database name
    ;; get application name (when compiled we are in the omas package)
    (unless (stringp application-name)
      (warn "~S is not a string naming the application." application-name)
      (return-from open-database))
    
    ;; ask MOSS to produce a name
    (setq db-pathname 
          (moss::db-compute-pathname :name application-name 
                                     :directory application-directory))
    
    ;; check for existing database
    (unless (moss::db-exists? db-pathname)
      ;; create it
      (warn "Creating a persistent object-store for agent ~S in application ~S" 
            (name agent) application-name)
      (moss::db-create :db-pathname db-pathname :area (key agent))
      ;; should check success
      ;; save pathname
      (setf (omas-database *omas*) db-pathname)
      (return-from open-database :empty))
    
    ;; if it exists open it (should never fails)
    (unless (moss::db-open :db-pathname db-pathname)
      (warn "Can't open persistent store for application: ~S" application-name)
      (return-from open-database))
    
    ;; check for existing partition
    (unless (moss::db-area-exists? (key agent))
      ;; create it
      (warn "creating a new partition for agent ~S in application ~S"
            (name agent) application-name)
      (moss::db-extend (key agent))
      (setf (omas-database *omas*) db-pathname)
      (return-from open-database :empty))
    
    ;; here eveything is OK, record db pathname
    (setf (omas-database *omas*) db-pathname)
    :done))


;;;------------------------------------------------------------------- PEEK-OBJECT
;;; useless, use load-object

;;;-------------------------------------------------------------------- PRINT-BASE

(defMethod print-base ((agent agent) &key print-values)
  "prints the content of the database.
Arguments:
   print-values (key): default nil
Return:
   :done"
  (moss::db-vomit (key agent) :print-values print-values))


;;;------------------------------------------------------------------ RESTORE-BASE

(defmethod restore-base ((agent agent) &optional file-name)
  "restore a database partition from an exportedtext file:
   e.g. FINANCING-DUMP-120616.lisp.
 Clears the partition if non empty.
Arguments:
   none
Return:
   pathname of the file if success, nil if failure"
  ;; execute only if agent is persistent
  (when (persistency agent)
    (let (file-pathname) 
      ;; if so select pathname of the file to upload
      (setq file-pathname 
            (if file-name
                (merge-pathnames (omas-application-directory *omas*) file-name)
              (cg:ask-user-for-existing-pathname
               "Please select the file to upload into the database."
               :initial-directory (omas-application-directory *omas*))))
      
      (format t "~%; restore-base /getting data from:~%;  ~S" file-pathname)
      
      (if file-pathname
          (moss::db-restore file-pathname (omas-database *omas*) (key agent))
        (return-from restore-base nil))
      )
    :done))

;;;------------------------------------------------------------------ STORE-OBJECT

(defMethod store-object ((agent agent) key &optional (value nil value?) 
                         &key no-commit)
  "inserts a key (symbol) and value into the hash table, unless p-list ~
   contains :no-save tag.
Arguments:
   key: key of data to save
   value (opt): value of data to save (default is key value)
   no-commit (key): if t does not commit
Return:
   value"
  (unless (get key :no-save)
    (moss::db-store key (if value? value (symbol-value key)) (key agent)
                    :no-commit no-commit)))

#|
? (omas::print-database address-proxy::SA_address-proxy)
|#
;;;------------------------------------------------------------------ SHOW-CHANGES

(defmethod show-changes ((agent agent))
  "show the objects that have been modified, e.g. prior to a commit"
  (let ((editing-box-var (intern "*EDITING-BOX*"))
        editing-box)
    (when (or (not (boundp editing-box-var))
              (null (setq editing-box (symbol-value editing-box-var))))
      (error "agent ~S no *EDITING-BOX*" (name agent)))
    (format t "~%Modified objects:~%  new-objects:~%;  ~S~%; ~
                  old-objects:~%;  ~S" (moss::new-object-ids editing-box)
      (moss::old-object-values editing-box))
    t))
    
;;;============================== tests ==========================================

#|
;; default filename is "Omas.base" if does not exist create it

(defagent :test :persistency t)

(defparameter *p* (omas::open-database sa_test))

(setq k001 '("test K001"))

(omas::store-object sa_test :k001 k001)

(omas::close-base sa_test)

(setq *p* (open::omas-database sa_test))

(omas::load-object sa_test :k001)

(omas::close-base sa_test)

|#

(format t "~&;*** OMAS v~A - persistency loaded ***" *omas-version-number*)

;;; :EOF
