﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;             O M A S - D I A L O G - T R A C E  (file W-dialog-trace.lisp)
;;;
;;;===============================================================================
;;; This file contains all the functions related to the text window for tracing
;;; the dialog states during a conversation. When activated the output is no longer 
;;; sent to the Lisp Console but printed into this window.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| 
We squat the text-window slot of *omas*, although the text window is related to 
a specific PA (we assume that we trace one dialog at a time.

2020
 0202 restructuring OMAS as an asdf system
|#

(in-package :omas)

;;;================================ Globals ======================================
;;; position the trace window rigth under the control panel

(defparameter *td-window* nil "text window for tracing dialogs")

(defparameter *td-left* *panel-left-position*)
(defparameter *td-top* (+ *panel-height-offset* *panel-height*))
(defparameter *td-height* (- (cg:height (cg:screen cg:*system*)) 
                             *td-top* 50))
(defparameter *td-width* (floor (* 1.4 *panel-width*)))

;;; list to repaint graphics window
(defParameter *td-display-list* ())

(defParameter *td-v-offset* 0)
(defparameter *td-last-v-position* 0 "current drawing position")

;;;========================= External Functions ==================================
;;; functions called from processes other than the display process

;;;------------------------------------------------------------------ DIALOG-TRACE

(defun dialog-trace (text &optional (color black))
  (declare (ignore color))
  (if (text-window *omas*) (format *text-pane* text)))

;;;============================= Text window =====================================

(defclass TRACE-DIALOG-WINDOW (cg:text-edit-window)
    ()
    (:documentation "window to print detailed trace of dialogs"))
  
(defun make-trace-dialog-window (&aux win)
  (declare (special *trace-dialog-pane*))
  ;; if window is already recorded don't do a thing
  (unless (omas::text-window omas::*omas*)
    (setq win
           (cg:MAKE-WINDOW 
               :trace-dialog-window 
			:TITLE "Dialog Trace"
             :OWNER (cg:SCREEN cg:*SYSTEM*)
             :CLASS 'trace-dialog-window
             :left *td-left*
			:top *td-top*
			:width *td-width*
			:height *td-height*
             :CLOSE-BUTTON T
             :CURSOR-NAME :ARROW-CURSOR
             :FONT (cg:MAKE-FONT-EX :SWISS "MS Sans Serif / ANSI" 11 NIL)
             :FORM-STATE :NORMAL
             :MAXIMIZE-BUTTON T
             :MINIMIZE-BUTTON T
             :POP-UP NIL
             :RESIZABLE T
             :SCROLLBARS :vertical
             :STATE :NORMAL
             :STATUS-BAR NIL
             :SYSTEM-MENU T
             :TITLE-BAR T
             :TOOLBAR NIL
             ))
    (setq *trace-dialog-pane* (cg:frame-child win))
    (setf (text-window *omas*) win)))

#|
(progn (setf (text-window *omas*) nil) (make-trace-dialog-window))
(format *trace-dialog-pane* "~& albert")
(dotimes (ii 100)
  (format *trace-dialog-pane* "~% ligne n°: ~S" ii))
|#

;;; must be defined in the cg: package
(defmethod cg:user-close ((ww trace-dialog-window))
  (declare (special moss::*verbose* *omas* *trace-dialog-pane*))
  (call-next-method)
  (setf (text-window *omas*) nil
    *trace-dialog-pane* nil)
  ;; reset moss debug flag
  (setq moss::*verbose* nil)
  )

;;;-------------------------------------------------------------- TD-ADJUST-SCROLL

(defun TD-adjust-scroll ()
  "checks if we are printing near the bottom of the text window, or outside ~
      the visible part of the screen. If so, then scrolls the window up some.
Arguments: none."
  (let ((pane (td-pane))
        inside-drawing-position)
    (when pane
      (setq inside-drawing-position (- *TD-last-v-position* *TD-v-offset*))
      
      ;; when position is outside visible part of the window, move the text up
      (when (> inside-drawing-position (height pane))
        ;; set offset so that we have some space
        (setq *TD-v-offset* (- *TD-LAST-V-POSITION*
                               (floor (* 3/4 (height pane)))))
        ;; redraw window
        (invalidate pane)
        ;(update-window pane)
        (return-from TD-adjust-scroll *TD-V-OFFSET*))
      
      ;; otherwise, we are visible, but when within last 10% of window, scroll up
      (when (>= inside-drawing-position (floor (* 9/10 (height pane))))
        ;; move up 1/4 pane
        (incf *TD-v-offset* (floor (* 1/4 (height pane))))
        ;; redraw window
        (invalidate pane)
        ;(update-window pane)
        ))
    :done))

;;;---------------------------------------------------------- TD-DRAW-ON-REDISPLAY

(defun TD-draw-on-redisplay (widget stream)
  "called to update display"
  (declare (ignore widget))
  (when *td-display-list*
    (let ((display-list (reverse *td-display-list*)))
      (dolist (item display-list)
        (when (>= (car item) *Td-V-offset*)
          (case (cadr item)
            (TEXT
             (with-foreground-color (stream (cadddr item))
               (format stream (caddr item))
               )
             )))))))

;;;--------------------------------------------------------- TD-DRAW-STRING-IN-BOX
;;; not really useful, except maybe to wrap text when printing on several lines...

(defun TD-draw-string-in-box (stream string start end text-box horizontal-justification 
                                     vertical-justification )
  "adapter to take into account scroll offset"
  (let ((left (box-left text-box))
        (top (box-top text-box))
        (right (box-right text-box))
        (bottom (box-bottom text-box))
        box)
    ;; make a new box at offset
    (setq box (make-box left (- top *TD-v-offset*) right (- bottom *TD-v-offset*)))
    ;; draw it
    (draw-string-in-box stream string start end box horizontal-justification 
                        vertical-justification)))

;;;------------------------------------------------------------------ TD-DRAW-TEXT

(defun TD-draw-text (text &optional (color black))
  "display text in the graphics box and record it into the display list and note ~
   the scroll position.
Arguments:
   text: text to display
   color (opt): color of text to print.
Return:
   current vertical position."
  (when (omas::text-window omas::*omas*)
    (let ((stream (td-stream)))
      ;; check whether enough room
      (TD-adjust-scroll)
      ;; first print text
      (with-foreground-color (stream color)
        (format stream text))
      ;; record text
      (push (list *td-last-v-position* 'TEXT text color) *TD-display-list*)
      ;; update vertical position
      (setq *td-last-v-position* (+ *td-v-offset* (current-position-y stream)))
      )))

;;;----------------------------------------------------------------------- TD-PANE

(defun TD-pane ()
  "get the drawing pane for the text window, when present."
  (find-component :draw (omas::text-window omas::*omas*)))

;;;--------------------------------------------------------------------- TD-SCROLL

(defun TD-scroll (widget new-value old-value)
  "scroll text window up 1/4 page when reaching the bottom of the page."
  (declare (ignore old-value))
  (let ((pane (find-sibling :draw widget))
        (max-position *TD-last-v-position*))
    ;; add some room after max-position
    (incf max-position (floor (* 1/10 (height pane))))
    (when pane
      (setq *td-v-offset* (floor (/ (* new-value max-position) 100)))
      (invalidate pane)
      ;(update-window pane)
      ))
  t)

;;;--------------------------------------------------------------------- TD-STREAM

(defun TD-stream ()
  "get the drawing stream for the text window.
Arguments:
   none
Return:
   stream object to draw or print, nil when window does not exist."
  (let ((pane  (find-component :draw (text-window *omas*))))
    (when pane (drawable-stream pane))))


(format t "~&;*** OMAS v~A - dialog trace window loaded ***" omas::*omas-version-number*)

;;; :EOF
