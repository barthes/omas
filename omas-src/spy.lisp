﻿;;;-*- Mode: Lisp; Package: "SPY" -*-
;;;===============================================================================
;;;20/02/02
;;;                      O M A S - S P Y (file spy.lisp)
;;;
;;;===============================================================================

;;; This file defines a postman agent that prints all the messages into its graphics
;;; window. Since it can see all the platform messages, it prints all messages 
;;; for a specific platform. 

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;; We must have a spy on each machine since it uses the machine screen.
;;; The window has life lines for all known agents. SPY starts with local agents
;;; (from the local Lisp environment), adding agents as found in new messages, either
;;; in the from or the to properties.
;;; Once the window is open, the agent keeps it open unless a specific message 
;;; is sent to make the agent inactive.
;;; thus the skills are 
;;;    :enable 
;;;    :disable
;;;    :send
;;; However, messages do not contain timeout or time-limit marks, hence the system 
;;; must send specific messages to the spy agent. They should be redistributed to the
;;; other spy agents on other machines... and other sites (?)
;;;    :mark-timeout
;;;    :mark-time-limit
;;;    :mark-abort
;;; The SPY window is created by the scan process  of the spy agent and the send 
;;; skill is called from the same scan process. Thus there is no problem for drawing
;;; into the window. All additional drawing messages should be inform messages so 
;;; that they are processes by the scan process of the SPY agent.

#|
2020
 0202 restructuring OMAS as an asdf system
|#

;;; define name in the loading environment, so that it can be used without prefix,
;;; export it to the agent package

(in-package :SPY)

;;; make a presumably unique SPY name by adding internal real time
(defParameter *spy-key* 
  (intern (format nil "SPY-~A" (get-internal-real-time)) :keyword))

(defParameter *spy-name* (omas::create-postman-name *spy-key*))
;;; save name in the site info
(setf (omas::spy-name omas::*omas*) *spy-name*)

(omas::make-postman *spy-key* :site :local :server nil
                      :raw t :hide t :package (find-package :spy))

;;;===============================================================================
;;;                         service macros and functions 
;;;===============================================================================


;;; ==================================== globals =================================
;;; Globals  can be used within the agent space to simplify programming, although
;;; it is better to use the agent memory.
;;; Those do not need to be unique across the platform since they are limited to
;;; the local environment.

(defParameter *spy-window* nil)

;;;=============================================================================== 
;;;                                    skills 
;;;===============================================================================

;;;===============================================================================
;;; DISABLE
;;;========================================================================= skill
;;; Stop transfer activities 

(make-skill :DISABLE *spy-key*
            :static-fcn 'static-disable
            )

(defUn static-disable (agent environment)
  "documentation"
  (declare (ignore environment))
  (setf (omas::status agent) :ready)
  (when (omas::%window? *spy-window*) (omas::%window-close *spy-window*))
  (setq *spy-window* nil)
  (static-exit agent :done))

;;;===============================================================================
;;; ENABLE
;;;========================================================================= skill

;;;(defParameter *spy-enable-functions*
;;;  '((:functions static-enable 
;;;                )))

(make-skill :ENABLE *spy-key*
            :static-fcn 'static-ENABLE
            )

;;; close graphics window and borrow its environment (quick and dirty solution)

(defUn static-enable (agent environment)
  "the enable function makes the agent active.
Arguments:
   nine
Return:
   the :done"
  (declare (ignore environment protocol)(special *spy-window*))
  ;#+ALLEGRO-V8.2
  ;(ide::format-debug "~&~A:STATIC-ENABLE/ spy-window: ~S" *spy-key* *spy-window*)
  
  ;; unless *spy-window* exists create it
  (unless *spy-window* 
    ;; initialize with local agents, not including spy
    (setf (omas::names-of-agents-to-display omas::*omas*) 
      (remove *spy-key* (mapcar #'car (omas::local-agents omas::*omas*))))
    (make-spy-window)
    (cg:set-foreground-window *spy-window*)
    )
  (static-exit agent :done))

;;;===============================================================================
;;; MARK-ABORT
;;;========================================================================= skill

;;;(defParameter *spy-mark-abort-functions*
;;;  '((:functions static-STOP)))

(make-skill :MARK-ABORT *spy-key*
            :static-fcn 'static-MARK-ABORT
            )

(defUn static-mark-abort (agent environment sender-key)
  "draw a mark for abort"
  (declare (ignore environment))
  (draw-square sender-key omas::*black-color*)
  (static-exit agent :done))

;;;===============================================================================
;;; MARK-ERROR
;;;========================================================================= skill

;;;(defParameter *spy-mark-error-functions*
;;;  '((:functions static-mark-error)))

(make-skill :MARK-ERROR *spy-key*
            :static-fcn 'static-mark-error
            )

(defUn static-mark-error (agent environment sender-key)
  "puts a mark onto the lifeline of the agent"
  (declare (ignore environment))
  (draw-square sender-key omas::*black-color*)
  (static-exit agent :done))

;;;===============================================================================
;;; MARK-TIMEOUT
;;;========================================================================= skill

;;;(defParameter *spy-mark-timeout-functions*
;;;  '((:functions static-mark-timeout)))

(make-skill :MARK-TIMEOUT *spy-key*
            :static-fcn 'static-mark-timeout
            )

(defUn static-mark-timeout (agent environment sender-key)
  "puts a mark onto the lifeline of the agent"
  (declare (ignore environment))
  (draw-timeout-mark sender-key)
  (static-exit agent :done))

;;;===============================================================================
;;; MARK-TIME-LIMIT
;;;========================================================================= skill

;;;(defParameter *spy-mark-time-limit-functions*
;;;  '((:functions static-mark-time-limit)))

(make-skill :MARK-TIME-LIMIT *spy-key*
            :static-fcn 'static-mark-time-limit
            )

(defUn static-mark-time-limit (agent environment sender-key)
  "puts a mark onto the lifeline of the agent"
  (declare (ignore environment))
  (draw-time-limit-mark sender-key)
  (static-exit agent :done))

;;;===============================================================================
;;; MESSAGE-DRAW
;;;========================================================================= skill

;;;(defParameter *spy-message-draw-functions*
;;;  '((:functions static-MESSAGE-DRAW)))

(make-skill :MESSAGE-DRAW *spy-key*
            :static-fcn 'static-MESSAGE-DRAW
            )

(defUn static-message-draw (agent environment message)
  "draws a message, done normally by send..."
  (declare (ignore environment))
  (draw-message message)
  (static-exit agent :done))

;;;===============================================================================
;;; MESSAGE-TRACE
;;;========================================================================= skill

;;;(defParameter *spy-message-trace-functions*
;;;  '((:functions static-MESSAGE-TRACE)))

(make-skill :MESSAGE-TRACE *spy-key*
            :static-fcn 'static-MESSAGE-TRACE
            )

(defUn static-message-trace (agent environment message)
  "draws a message text, normally done by send..."
  (declare (ignore environment))
  (draw-message message)
  (static-exit agent :done))

;;;===============================================================================
;;; SEND
;;;========================================================================= skill
;;; SPY is a postman, i.e. it can see every message that comes by. Thus, it is not 
;;; necessary to send a specific message to SPY to draw a particular message.
;;; Messages are only needed to draw marks

;;;(defParameter *spy-send-functions*
;;;  '((:functions static-SEND)))

(make-skill :SEND *spy-key*
            :static-fcn 'static-SEND
            )

(defUn static-send (agent environment message-string message)
  "documentation"
  (declare (ignore environment message-string))
  ;(dformat :skill-header 0 "~%;--- message: ~S" environment)
  ;(print `("SPY/send/ message-string:" ,message-string))
  ;; display current message
  (draw-message message)  
  (static-exit agent :done))

;;;===============================================================================
;;; STATE-DRAW
;;;========================================================================= skill

;;;(defParameter *spy-state-draw-functions*
;;;  '((:functions static-STATE-DRAW)))

(make-skill :STATE-DRAW *spy-key*
            :static-fcn 'static-STATE-DRAW
            )

(defUn static-state-draw (agent environment)
  "puts a mark onto the lifeline of the agent"
  (declare (ignore environment))
  (draw-all-states)
  (static-exit agent :done))

;;;==============================================================================
;;;                                    goals
;;;==============================================================================

;;; no goals


;;;===============================================================================

;;; by default we assume that the application will execute in the common-lisp-user
;;; package

(format t "~&;*** OMAS v~A - Agent ~A loaded ***" omas::*omas-version-number* *spy-key*)

;;; :EOF
