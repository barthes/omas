;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;24/10/18
;;;                U T I L S  (file utils.lisp)
;;;
;;;===============================================================================
;;; This file contains service functions that are not part of the public API

;;; (c) Jean-Paul Barthès@HEUDIASYC, CNRS, UTC. See LICENSE file for permissions.

(in-package :omas)

;;;===============================================================================
;;;                          SERVICE FUNCTIONS
;;;===============================================================================
;;; The following service functions are not meant to be part of the public API
;;; However, trace functions that will be part of the API are defined here since
;;; this is the first file being loaded and since they are used in the following
;;; files

;;;------------------------------------------------------------------ ALT-LIST-SET
;;; should be replaced by (setf (getf ...

(defUn alt-list-set (list value prop)
  "replacing whatever was associated with prop by value in alternated list. ~
   Add it if prop not there."
  (cond ((null list) (list prop value))
        ((equal prop (car list)) (cons prop (cons value (cddr list))))
        (t (cons (car list)
                 (cons (cadr list) (alt-list-set (cddr list) value prop))))))

#|
(defparameter ll '(:a 1 :b 2 :c 3))
;; non destructive
(alt-list-set ll 222 :b) 
;; destructive
(setf (getf ll :b) 222)

(getf ll :a)
1
|#
;;;------------------------------------------------------------------ ALT-LIST-GET
;;; should be replaced by getf

(defUn alt-list-get (ll prop)
  "same as the getf primitive"
  (cadr (member prop ll)))

;;;same stuff with alists
;;;-------------------------------------------------------------- AL-ADD-PV-IF-NEW

(defUn al-add-pv-if-new (item sequence)
  "add an item at end of list if new (test is equal)"
  (cond ((null sequence) (list item))
        ((equal item (car sequence)) sequence)
        (t (cons (car sequence) (al-add-pv-if-new item (cdr sequence))))))

;;;---------------------------------------------------------------- AL-REMOVE-PROP

(defUn al-remove-prop (prop sequence)
  "removing all occurences of pairs whose car is prop. Uses equal test.
Arguments:
   prop: property
   sequence: alist to modify
Return:
   a new list from which pairs starting with prop have been removed."
  (remove prop sequence :key #'car :test #'equal))
;(remove-if #'(lambda (xx) (equal (car xx) prop)) sequence)) 

#|
(defparameter la '((:a 1)(:b 2)(:c 3) (:b 22)))
;; non destructive
(al-remove-prop :b la)
((:A 1) (:C 3))
(remove-if #'(lambda(xx)(equal xx :b)) la :key #'car)
((:A 1) (:C 3))
(remove :b la :key #'car :test #'equal)
((:A 1) (:C 3))
|#
;;;---------------------------------------------------------- AL-REPLACE-OR-ADD-PV

(defUn al-replace-or-add-pv (prop values sequence)
  "replacing whatever was associated with prop by values in alist ~
   Add it if prop not there."
  (cond ((null sequence) (list (cons prop values)))
        ((equal prop (caar sequence)) (cons (cons prop values) (cdr sequence)))
        (t (cons (car sequence) (al-replace-or-add-pv prop values (cdr sequence))))))
		
;;;------------------------------------------------------------- REPLACE-OR-ADD-PV

(defUn replace-or-add-pv (prop values sequence)
  "replacing whatever was associated with prop by values in alist ~
   Add it if prop not there."
  (cond ((null sequence) (list (cons prop values)))
        ((equal prop (caar sequence)) (cons (cons prop values) (cdr sequence)))
        (t (cons (car sequence) (replace-or-add-pv prop values (cdr sequence))))))
		
;;;--------------------------------------------------------------- ADD-ITEM-IF-NEW

(defUn add-item-if-new (item sequence)
  "add an item at end of list if new (test is eql)"
  (cond ((null sequence) (list item))
        ((equal item (car sequence)) sequence)
        (t (cons (car sequence) (add-item-if-new item (cdr sequence))))))
		
;;;-------------------------------------------------------------- %ALIST-ADD-VALUE

(defUn %alist-add-value (sequence prop value)
  "adding value if a new one (test is eql) to old ones in the alist"
  (declare (inline add-item-if-new))
  (cond ((null sequence) (list (list prop value)))
        ((equal prop (caar sequence)) 
         (cons (cons (caar sequence)
                     (add-item-if-new value (cdar sequence)))
               (cdr sequence)))
        (t (cons (car sequence) (%alist-add-value (cdr sequence) prop value)))))

;;;==============================================================================
;;;                         TRACING FUNCTIONS
;;;==============================================================================
;;; Those functions are intended for developers

;;;----------------------------------------------------------------- DFORMAT-RESET
;;; rremoves tracing functions with the specified tag

(defun dformat-reset (tag)
  (setf (get tag :trace) nil)
  )

;;;------------------------------------------------------------------- DFORMAT-SET
;;; controls the triggering of (dformat <tag> <level> "..." ...)

(defun dformat-set (tag level)
  (setf (get tag :trace) t)
  (setf (get tag :trace-level) level)
  )

;;;------------------------------------------------------------------- OMAS-TRACE

(defun omas-trace (&optional tag level)
  "adds a trace tag to the set of trace tags or gives the list of trace tags.
Argument:
   tag (opt): if there adds the key to the set of trace keys
   level (opt): level of trace (default is 0)
Return:
   list of tags"
  (cond
   (tag 
    (pushnew tag (debugging *omas*))
    (setf (get tag :trace) t)
    (setf (get tag :trace-level) (or level 0))
    )
   (t
    (debugging-tags *omas*))))

;;;------------------------------------------------------------------ OMAS-TRACE?

(defun omas-trace? ()
  "shows the current active trace tags"
  (let ((tag-list (debugging-tags *omas*)))
  (mapcar #'(lambda (xx) (cons xx (or (cdr (assoc xx tag-list)) "?")))
    (debugging *omas*)))
  )

#|
(omas-trace?)
(:WEB :LOAD)
|#
;;;--------------------------------------------------------------- OMAS-TRACE-ADD

(defun omas-trace-add (tag reason)
  "creates a new trace tag and add it to the debugging-tags list.
Argument:
   tag: new tag
   reason: a string explaining what will be traced
Return:
   updated list"
  (push (cons tag reason) (debugging-tags *omas*)))

;;;----------------------------------------------------------------- OMAS-UNTRACE

(defun omas-untrace (tag)
  "removes a tag from the set of trace tags. If the arg is :all, removes everything."
  (if (eql tag :all)
      (setf (debugging *omas*) nil)
    (let ((tags (remove tag (debugging *omas*))))
      (setf (debugging *omas*) tags)
      (setf (get tag :trace) nil)
      tags)))


;;;=============== adding here some general service functions =====================
;;;----------------------------------------------------------------- REMOVE-AT-POS
;;; function that removes an item in the nth position of a list (non destructive)

(defUn remove-at-pos (nn ll)
  (cond ((< nn 0) ll) 
        ((null ll) nil)
        ((zerop nn) (cdr ll))
        (t (cons (car ll)(remove-at-pos (1- nn) (cdr ll))))))

;;;--------------------------------------------------------------------- SET-EQUAL

(defun set-equal (set1 set2 &key (test #'equal+))
  "check if two sets are equal"
  (and (null (set-difference set1 set2 :test test))
       (null (set-difference set2 set1 :test test))))

#|
(setq s1 '("a" "b" "c") s2 '("b" "A" "c"))
(set-equal s1 s2)
T
(setq s1 '("a" "b" "c") s2 '("b" "A" "c" "a"))
(set-equal s1 s2)
T
(setq s1 '("a" "b" "d") s2 '("b" "A" "c" "a"))
(set-equal s1 s2)
NIL
(setq s1 nil s2 nil)
(set-equal s1 s2)
T
(setq s1 nil s2 '("b" "A" "c" "a"))
(set-equal s1 s2)
NIL
|#
;;;---------------------------------------------------------------- STRING-AT-MOST

(defUn string-at-most (string count)
  (if (<= (length string) count)
    string
    (concatenate 'string (subseq string 0 (- count 3)) "...")))

;;;============================ Function to get references =======================
;;;--------------------------------------------------------------------------- REF
;;; the following function traces in what function a specific symbol appears
;;; by examining the code of the function in the corresponding file.

;;; this function cannot be used when loadin OMAS with asdf, because *omas-file-names*
;;; is not initialized

(defUn ref (symbol &optional show-file)
  "traces in what function a specific symbol appears by examining the code of the ~
   function in the corresponding file."
  (let ((omas-prefix "omas-")
        expr)
    (dolist (file-name *omas-file-names*)
      (let ((file-path
             (make-pathname
              :device (pathname-device *omas-directory-string*)
              :host (pathname-host *omas-directory-string*)
              :directory (append (pathname-directory *omas-directory-string*)
                                 (list "omas source"))
              :name (concatenate 'string omas-prefix file-name)
              :type "lisp")))
        (with-open-file 
          (ss file-path)
          ;; if wanted, we print the path of the file we are checking
          (when show-file
            (print file-path))
          (loop
            (setq expr (read ss  nil :end))
            (when (eql :end expr) (return :done))
            (when (and (listp expr)
                       (member (car expr)
                               '(defUn macro defmethod definstmethod defownmethod
                                 defuniversalmethod))
                       (appears-in? symbol expr))
              (print (cadr expr)))))))
    :done))
	
;;;------------------------------------------------------------------- APPEARS-IN?

(defUn appears-in? (symbol expr)
  (cond ((null expr) nil)
        ((equal symbol expr) t)
        ((listp expr)
         (or (appears-in? symbol (car expr))
             (appears-in? symbol (cdr expr))))))


;;;===============================================================================
;;;                              SAV Functions
;;;===============================================================================
;;; Editing (creating or updating) an object requires storing it somewhere while it
;;; is modified. We use the edited-object slot of the agent to do that.
;;; Because several objects may be edited in parallel, the slot is organized as a 
;;; list of objects indexed by their id (real or temporary). The set of SAV) 
;;; functions has been written to simplify interactions with such a list

;;; Nice functions but currently unused, because we do not save object to be edited
;;; into the edit-object slot of the agent

;;;----------------------------------------------------------------------- SAV-ADD

(defun sav-add (agent id prop-ref value-list)
  "adds the value list to the property prop in the object description indexed by id.
 Updates the edited-object agent slot.
Arguments:
   agent: the active agent
   id: the index of the object, real or temporary id
   prop-ref: a string specifying the taget property
   value-list: a list of values
Return
   :done"
  (let (obj-l val)
    ;; get the object description (alist)
    (setq obj-l (cdr (assoc id (omas::edited-object agent) :test #'equal+)))
    ;; add the values to the object description alist
    (setq val (moss::alist-add-values obj-l prop-ref value-list))
    ;; replace the object description in the list of object descriptions
    (setf (omas::edited-object agent) 
      (alist-set-values (omas::edited-object agent) id val))
    ))
	
#|
(setq agent sn::sa_stevens-news)
(setf (omas::edited-object agent)
      '((A ("aa" 1)("bb" 2)("cc" 3))
        (B ("cc" 3)("dd" 4)("ee" 5))
        ("C" ("dd" 6 7)("ff" 99)))
  )
(sav-add agent 'A "bb" '(22 222))
(edited-object agent)
((A ("aa" 1) ("bb" 2 22 222) ("cc" 3)) (B ("cc" 3) ("dd" 4) ("ee" 5)))
(sav-add agent 'A "dd" '("d1" "d2"))
((A ("aa" 1) ("bb" 2 22 222) ("cc" 3) ("dd" "d1" "d2")) (B ("cc" 3) ("dd" 4) ("ee" 5)))
(sav-add agent "c" "ff" '(100 101))
((A ("aa" 1) ("bb" 2 22 222) ("cc" 3)) (B ("cc" 3) ("dd" 4) ("ee" 5)) 
 ("c" ("dd" 6 7) ("ff" 99 100 101)))
|#
;;;----------------------------------------------------------------------- SAV-DEL

(defun sav-del (agent id prop-ref val)
  "removes a specific value from the set of values associated with prop-ref"
  (let (obj-l)
    ;; get the object description (alist)
    (setq obj-l (cdr (assoc id (omas::edited-object agent) :test #'equal+)))
    ;; remove the value from the list
    (setq obj-l (alist-rem-val obj-l prop-ref val))
    ;; replace the object description in the list of object descriptions
    (setf (omas::edited-object agent)
      (alist-set-values (omas::edited-object agent) id obj-l))
    ))

#|
(setf (edited-object agent)
      '((A ("aa" 1)("bb" 2)("cc" 3))
        (B ("cc" 3)("dd" 4)("ee" 5))))
(sav-del agent 'B "dd" '4)
((A ("aa" 1) ("bb" 2) ("cc" 3)) (B ("cc" 3) ("ee" 5)))
|#
;;;---------------------------------------------------------------------- SAV-GET

(defun sav-get (agent id prop-ref)
  "returns the value associated with prop in the objects with ID id."
  (let (obj-l)
    ;; get the object description (alist)
    (setq obj-l (cdr (assoc id (omas::edited-object agent) :test #'equal+)))
    ;; get values
    (get-field prop-ref obj-l)))

#|
(setf (edited-object agent)
      '((A ("aa" 1)("bb" 2)("cc" 3))
        (B ("cc" 3)("dd" 4)("ee" 5))))
(sav-get agent 'b "dd")
(4)
(sav-get agent 'c "dd")
NIL
|#
;;;----------------------------------------------------------------------- SAV-REM

(defun sav-rem (agent id)
  "removes the entry index by id from the list (edited-object agent)"
  (setf (omas::edited-object agent) 
    (alist-rem (omas::edited-object agent) id)))

#|
(setf (omas::edited-object agent)
      '((A ("aa" 1)("bb" 2)("cc" 3))
        (B ("cc" 3)("dd" 4)("ee" 5))))
(sav-rem agent 'A)
((B ("cc" 3) ("dd" 4) ("ee" 5)))
|#
;;;---------------------------------------------------------------------- SAV-REPL

(defun sav-repl (agent id obj-l)
  "replaces the sublist prop-val index by id in (edited-object agent)"
  (setf (omas::edited-object agent) 
    (alist-set-values (omas::edited-object agent) id obj-l)))

#|
(sav-repl agent 'B '(("ff" 88)))
((A ("aa" 1) ("bb" 2) ("cc" 3)) (B ("ff" 88)))
(sav-repl agent 'C '(("gg" 99)))
((A ("aa" 1) ("bb" 2) ("cc" 3)) (B ("ff" 88)) (C ("gg" 99)))
|#
;;;----------------------------------------------------------------------- SAV-SET

(defun sav-set (agent id prop-ref values)
  "replaces the values associated to prop-ref in the specified object"
  (let (obj-l)
    (setq obj-l (cdr (assoc id (omas::edited-object agent) :test #'equal+)))
    ;; replace the values
    (setq obj-l (alist-set-values obj-l prop-ref values))
    ;; replace the object description in the list of object descriptions
    (setf (omas::edited-object agent) 
      (alist-set-values (omas::edited-object agent) id obj-l))
    ))

#|
(setf (edited-object agent)
      '((A ("aa" 1)("bb" 2)("cc" 3))
        (B ("cc" 3)("dd" 4)("ee" 5))))
(sav-set agent 'b "dd" '(44 444))
((A ("aa" 1) ("bb" 2) ("cc" 3)) (B ("cc" 3) ("dd" 44 444) ("ee" 5)))
|#
;;;===============================================================================
;;;                            WRNMSG Functions
;;;===============================================================================
;;; Those functions are useful when editing (creating or updating) an object to
;;; store warning messages into the message-list slot of the agent ownning the
;;; object.
;;; Because several objects may be edited in parallel, the slot is organized as a 
;;; list of objects indexed by their id. 
;;; The functions are similar to the sav-xxx functions

;;;-------------------------------------------------------------------- WRNMSG-ADD

(defun wrnmsg-add (agent id msg)
  "adds the message (string) to the list of messages indexed by id.
 Updates the edit-message-list agent slot.
Arguments:
   agent: the active agent
   id: the id of the object
   msg: a message (string)
Return
   :done"
    ;; replace the object description in the list of object descriptions
    (setf (omas::edit-message-list agent) 
      (alist-add (omas::edit-message-list agent) id msg))
    )
#|
(setf (edit-message-list agent)
      '((A "aa" "bb" "cc")
        (B "cc" "dd" "ee")))
(wrnmsg-add agent 'b "ff")
((A "aa" "bb" "cc") (B "cc" "dd" "ee" "ff"))
(wrnmsg-add agent 'C "ff")
((A "aa" "bb" "cc") (B "cc" "dd" "ee") (C "ff"))
|#
;;;------------------------------------------------------------------ WRNMSG-CLEAR

(defun wrnmsg-clear (agent id)
  "removes the entry index by id from the list (edit-object agent)"
  (setf (omas::edit-message-list agent) 
    (alist-rem (omas::edit-message-list agent) id)))

#|
(setf (edit-message-list agent)
      '((A "aa" "bb" "cc")
        (B "cc" "dd" "ee")))
(wrnmsg-clear agent 'A)
((B "cc" "dd" "ee"))
|#
;;;-------------------------------------------------------------------- WRNMSG-GET


(defun wrnmsg-get (agent id)
  "returns the value associated with id."
    (cdr (assoc id (omas::edit-message-list agent) :test #'equal+)))

#|
(setf (edit-message-list agent)
      '((A "aa" "bb" "cc")
        (B "cc" "dd" "ee")))
(wrnmsg-get agent 'b)
("cc" "dd" "ee")
|#

;;;-------------------------------------------------------------------- WRNMSG-SET

(defun wrnmsg-set (agent id msg-list)
  "replaces the values associated with id"
    ;; replace the object description in the list of object descriptions
    (setf (omas::edit-message-list agent) 
      (alist-set-values (omas::edit-message-list agent) id msg-list)))

#|
(setf (edit-message-list agent)
      '((A "aa" "bb" "cc")
        (B "cc" "dd" "ee")))
(wrnmsg-set agent 'b '(44 444))
((A "aa" "bb" "cc") (B 44 444))
(wrnmsg-set agent 'C '(44 444))
((A "aa" "bb" "cc") (B 44 444) (C 44 444))
|#


(format t "~&;*** OMAS v~A - utils loaded ***" *omas-version-number*)

;;; :EOF
