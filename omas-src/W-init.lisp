﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;                O M A S - W - I N I T  (file W-init.lisp)
;;;
;;;===============================================================================
;;; This file contains all the functions related to the init window for loading and
;;; launching the application in the MCL environment.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| 
History
-------
2020
 0202 restructuring OMAS as an asdf system
|#  

(in-package :omas)

;;;================================ Globals ======================================

(defVar omas::*init-window*)   ; defined in globals 

;;; we need this global variable, because when the button is clicked on the init    
;;; window the file has been loaded and *load-pathname* is nil    
(defVar *omas-load-pathname* *load-pathname*)

(defParameter *iw-slot-height* 20)


;;;================================= Window ======================================
;;; INIT-WINDOW is a subclass of dialog    

(defClass omas-init-window (cg:dialog)
  ()    
  (:documentation "window to choose and launch the application"))

;;;--------------------------------------------------------- MAKE-OMAS-INIT-WINDOW   

(defUn make-omas-init-window ()
  (declare (special *init-window* *omas-version-number*))
  (setq *init-window*
        (cr-window  
         :class 'OMAS-INIT-WINDOW
         ;:type #+carbon-compat :DOCUMENT-WITH-ZOOM-NO-GROW
         :title (format  nil (get-text *WINI-title*) *omas-version-number*)
         :top 300
         :left 550
         :right (+ 550 350)
         :bottom (+ 300 220)
         :name :omas-init-window
         :background-color (%rgb (* 256 208) (* 256 242) (* 256 214))
         :font (cg:MAKE-FONT-EX :SWISS "MS Sans Serif / ANSI" 11 NIL)
         :subviews (make-omas-init-window-widgets))
        ))

#|
(Make-Omas-Init-Window)
|#   
;;;------------------------------------------------- MAKE-OMAS-INIT-WINDOW-WIDGETS

(defUn make-omas-init-window-widgets () 
  (let ((label-font (cg:MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL))
        (pos -10) (delta 30)
        (pos-slot -15))
    (list
     ;;--- LOCAL REFERENCE
     (cr-static-text 
      :left 25 :top (incf pos delta) 
      :width 125 :height 16
      :name :local-reference
      :text (get-text *WINI-local-reference*)
      :font label-font
      )
     (cr-text-item
      :left 160 :top (incf pos-slot delta)
      :width 150 :height *iw-slot-height*
      :name :site-name
      :text (caddr (assoc "site" omas::*omas-parameters* 
                          :test #'string-equal))
      :font label-font
      )
     ;;--- APPLICATION
     (cr-static-text 
      :left 25 :top (incf pos delta) 
      :width 125 :height 20
      :text (get-text *WINI-appli*)
      :font label-font
      )
     (cr-text-item
      :left 160 :top (incf pos-slot delta)
      :width 150 :height *iw-slot-height*
      :name :appli/coterie
      :text (caddr (assoc "coterie" omas::*omas-parameters* 
                          :test #'string-equal))
      :font label-font
      )
     ;;--- FOLDER
     (cr-static-text 
      :left 25 :top (incf pos delta) 
      :width 125 :height 16
      :text (get-text *WINI-folder*)
      :font label-font
      )
     (cr-text-item
      :left 160 :top (incf pos-slot delta)
      :width 150 :height *iw-slot-height*
      :name :folder
      :text (get-text *WINI-not-available*)
      :font label-font
      )
     ;;--- IP
     (cr-static-text 
      :left 25 :top (incf pos delta) 
      :width 125 :height 16
      :text (get-text *WINI-IP*)
      :font label-font
      )
     (cr-text-item
      :left 160 :top (incf pos-slot delta)
      :width 150 :height *iw-slot-height*
      :name :ip-address
      :text (caddr (assoc "broadcast" omas::*omas-parameters* 
                          :test #'string-equal))
      :font label-font
      )
     ;;--- PORT
     (cr-static-text 
      :left 25 :top (incf pos delta) 
      :width 125 :height 16
      :text (get-text *WINI-port*)
      :font label-font
      )
     (cr-text-item
      :left 160 :top (incf pos-slot delta)
      :width 150 :height *iw-slot-height*
      :name :port
      :text (caddr (assoc "port" omas::*omas-parameters* 
                          :test #'string-equal))
      :font label-font
      )
     ;;--- HIDE button
     (cr-button
      :left 25 :top (incf pos-slot delta) :height 20
      :title (get-text *WINI-HIDE*)
      :on-click OI-HIDE-ON-CLICK)
     
     ;;--- LOAD button
     (cr-button
      :left 230 :top pos-slot :height 20
      :title (get-text *WINI-LOAD*)
      :on-click OI-LOAD-ON-CLICK)
     )))

#|
(make-omas-init-window)
|#  
;;;===============================================================================
;;;
;;;                          FUNCTIONS AND CALLBACKS
;;;
;;;===============================================================================

;;;--------------------------------------------------------- CLONE-AND-ADJUST-FILE
;;; The function allows for replacing some strings in the in-file by other values
;;; found in the substitutions a-list, e.g.
;;;  (clone-and-adjust-file "Ze" "XXXXX" :substitutions '(("MMMMM" . "Ramos")))
;;; This can replace the master name in the Ontology file for example

(defun clone-and-adjust-file (agent-ref in-filename &key out-filename substitutions)
  "clone a file and replaces the XXXXX marks by agent-name-string.
Arguments:
   agent-ref: name of the agent to create, e.g. \"Moulin\"
   in-filename: name of file to be cloned: e.g. \XXXXX\"
   out-filename (key): name of the file to produce, if not there, same as agent-ref
   substitutions (key): an a-list containing pairs of strings to be substituted
Return:
   name of the produced file."
  ;(print `(substitutions ,substitutions))
  (let* ((in-file (merge-pathnames 
                   (omas-application-directory *omas*) 
                   (string+ in-filename ".lisp")))
         (out-file (merge-pathnames  
                     (omas-application-directory *omas*)
                     (string+ (or out-filename agent-ref) ".lisp")))
         line)
    (with-open-file (out out-file :direction :output :if-exists :supersede
                         :if-does-not-exist :create :external-format :UTF-8)
      (with-open-file (in in-file :direction :input :external-format :utf-8)
        (loop
          (unless (setq line (read-line in nil nil)) (return))
          ;; replace XXXXX by agent name
          (setq line (substitute-substring agent-ref "XXXXX" line))
          ;; do now other substitutions
          (dolist (item substitutions)
            ;(print `(item ,item))
            (setq line (substitute-substring (cdr item)(car item) line))
            ;(break "item: ~S~%line: ~S" item line)
            )
          ;; write the modified line to the output file
          (format out "~A~%" line))))
    (format t "~%; temporary file: ~S" out-file)
    out-file))

#|
(clone-and-adjust-file "BARTHES" "HDSRI-FR-ONTOLOGY" :out-filename "BARTHES-ONTOLOGY"
                       :substitutions '(("LLLLL" . "FR")))
"\\Program Files\\Allegro CL 9.0\\OMAS-MOSS 12.1.0\\OMAS\\applications\\UTC-SERVER-V4\\BARTHES-ONTOLOGY.lisp"
|#
;;;----------------------------------------------------------- COMPILE-AND/OR-LOAD

(defUn compile-and/or-load (file-pathname &key output-file external-format)    
  "takes a file path, cook up a file-pathname, look for text file. If more ~
   recent than compiled one, recompiles it and load compiled version. If ~
   untouched or absent, load compiled version. If neither text file nor compiled ~
   file are there, then complain. Result of compilation is in a separate fasl ~
   folder.    
Arguments:    
   file-pathname: pathname containing directory and file-name 
   output-file (key): if present must be a file pathname to the output file   
   external-format (key): refers to the file encoding if nil uses default external
       format, otherwise can be set as :UTF-8 for example
Return:    
   t if file was found, nil otherwise."   
  (let ((file-type "fasl")
        (file-encoding (excl::find-external-format (or external-format :default)))  
        source compiled-file)    
    ;;; cook up the proper file name pathnames (lisp and fasl)    
    (setq source    
          (omas::make-new-pathname      
           file-pathname     
           :type "lisp"))    
    ;; when no output file, then cook up one to store into a fasl folder at the     
    ;; same level as the source files    
    (setq compiled-file     
          (or output-file    
              (omas::make-new-pathname     
               file-pathname     
               :directory (append (pathname-directory file-pathname) 
                                  (list file-type))    
               :type file-type)))    
    ;; whenever the file or compile file are absent, give up    
    (unless (or (probe-file source)    
                (probe-file compiled-file))    
      (return-from COMPILE-AND/OR-LOAD nil))    
    ;; compile file if source was modified    
    (when (and source     
               (or (null (probe-file compiled-file))    
                   (< (file-write-date compiled-file)    
                      (file-write-date source))))    
      (compile-file source :output-file compiled-file :verbose nil 
                    :external-format file-encoding))    
    ;;  load compiled version    
    (load compiled-file :external-format file-encoding)    
    t))

;;;---------------------------------------------------------- COMPILE-FILE-NO-LOAD
;;; This is used by agents to compile additional files

(defUn compile-file-no-load (file-pathname 
                                 &key output-file external-format)    
  "takes a file path, cook up a file-pathname, look for text file. If more ~
   recent than compiled one, recompiles it.    
Arguments:    
   file-pathname: pathname containing directory and file-name 
   output-file (key): if present must be a file pathname to the output file   
   external-format (key): refers to the file encoding if nil uses default external
       format, otherwise can be set as :UTF-8 for example
Return:    
   t if file was found, nil otherwise."   
  (let ((file-type "fasl")
        (file-encoding (excl::find-external-format (or external-format :default)))  
        source compiled-file)    
    ;;; cook up the proper file name pathnames (lisp and fasl)    
    (setq source    
          (omas::make-new-pathname      
           file-pathname     
           :type "lisp"))    
    ;; when no output file, then cook up one to store into a fasl folder at the     
    ;; same level as the source files    
    (setq compiled-file     
          (or output-file    
              (omas::make-new-pathname     
               file-pathname     
               :directory (append (pathname-directory file-pathname) 
                                  (list file-type))    
               :type file-type)))    
    ;; whenever the file or compile file are absent, give up    
    (unless (probe-file source)    
      (error "can't find the file to compile ~S" source)) 
    
    ;; compile file if source was modified    
    (compile-file source :output-file compiled-file :verbose nil 
                  :external-format file-encoding)   
    t))

;;;-------------------------------------------------------------------- LOAD-AGENT
;;; compiled in the OMAS package. Called from init Window in the OMAS package ?

(defUn load-agent (&key agent-file-pathname)
  "load a specific agent including associated files like ontology, task, etc.
   Add the agent to the *local-agents* list from the applications folder.
   If :browse-include-files is a PA, launches the associated process.
Arguments:t
   agent-file-pathname (key): pathname to agent file (ask user if nil)
Return:
   :done"
  ;(declare (special *moss-system*))
  
  (let (agent file-pathname agent-name-string agent-package)
    (with-compilation-unit ()
      ;; if agent is not there get file name by asking user
      (unless agent-file-pathname 
        (setq agent-file-pathname
              (cg:ask-user-for-directory 
               :browse-include-files t
               :root (make-pathname
                      :device (pathname-device *omas-directory*)
                      :directory
                      (append (pathname-directory *omas-directory*)
                              (list "OMAS" "applications"))  
                      ))))
      ;; if the the selection was aborted the file pathname is nil
      (unless agent-file-pathname
        (return-from load-agent nil))
      ;; check if the user has selected a file an not a directory
      (unless (pathname-name agent-file-pathname)
        (warn "load-file /selection is not a file: ~S." agent-file-pathname)
        (return-from load-agent nil))
      
      ;; get agent name string, i.e. the file name (necessary to repeat that when
      ;; agent-file-pathname is passed as the argument)
      (setq agent-name-string (pathname-name agent-file-pathname))
      ;(format t "~%;--- agent-name-string: ~S" agent-name-string)
      
      ;;==============================================================================
      ;;                          INFERER AGENT 
      ;;==============================================================================
      
      ;;; when agent is an inferer (file starting with "IA-") special processing 
      ;; we create agent here, set package and load the file
      
      (when (and (>= (length agent-name-string) 3) ; JPB1005
                 (string-equal "IA-" (subseq agent-name-string 0 3)))
        (let ((package-string (string-trim '(#\space) (subseq agent-name-string 3)))
              agent-key)
          (if (equal "" package-string)
              (error "bad inferer agent name: ~S" agent-name-string))
          ;; create package
          (setq agent-key (intern package-string :keyword))
          (setq agent-package (make-package agent-key :use '(:cl :moss :omas)))
          ;; load file in the right package
          (with-package agent-package
            (unless (probe-file agent-file-pathname)
              (warn "Missing agent file: ~S" agent-file-pathname)
              )
            ;; create agent
            (make-inferer agent-key)
            ;; now load the stuff    
            ;(format t "~&;load-agent; agent file: ~S" agent-file-pathname)    
            (unless (compile-and/or-load agent-file-pathname 
                                         :external-format :utf-8)    
              (warn "Missing agent file ~S, can't load it." agent-file-pathname))
            )
          ;; before leaving reset global entry
          (setf (omas::agent-being-loaded *omas*) nil)
          (return-from load-agent :done)))
      
      ;;==============================================================================
      ;;                 load AGENT file, creating agent
      ;;==============================================================================
      (unless (probe-file agent-file-pathname)
        (warn "Missing agent file: ~S" agent-file-pathname))
      ;; now load the stuff    
      ;(format t "~&;load-agent; agent file: ~S" agent-file-pathname)
      
      (unless (compile-and/or-load agent-file-pathname 
                                   :external-format :utf-8)    
        (warn "Missing agent file ~S, can't load it." agent-file-pathname)
        (return-from load-agent :failure))
      
      ;; here the agent structure has been created
      ;(format t "~&;load-agent; agent file loaded")
      
      ;; now that the agent file has been loaded, recover the package
      (setq agent-package (find-package agent-name-string))
      ;(format t "~&;load-agent; agent package: ~S" agent-package)
      
      ;;==============================================================================
      ;;                             load ONTOLOGY
      ;;==============================================================================
      ;;=== load ONTOLOGY depending on the persistency state of the agent
      ;; if the agent is not persistent, we load the ontology file
      ;; if the agent is persistent but is created for the first time, we load the
      ;; ontology file, then save the world into the database
      ;; if the agent is persistent and the database exists, we do not load the
      ;; ontology file, since concepts have been loaded from it 
      ;; get agent structure now it has been loaded
      (setq agent (omas::agent-being-loaded *omas*))
      (unless agent
        (error "can't recover agent structure"))
      
      (with-package agent-package
        ;(format t "~%; load-agent/ontology *package*: ~S" *package*)
        (load-agent-ontology agent agent-name-string agent-file-pathname)
        ) ; jpb 150303
      
      ;; TASK and DIALOGS are used by PAs and PAs have no persistent database
      
      ;;==============================================================================
      ;;                            load TASKS file
      ;;==============================================================================
      ;; only for personal assistant
      
      (when (typep agent 'assistant) 
        (with-package agent-package ; JPB150304
          ;(format t "~%; load-agent/tasks *package*: ~S" *package*)
          (load-agent-tasks agent agent-name-string agent-file-pathname)))
      
      ;;==============================================================================
      ;;                             load DIALOG
      ;;==============================================================================
      ;; only for personal assistant
      ;; add agent to the local agent list ?
      
      ;(format t "~%; load-agent/dialog *package*: ~S" *package*)
      (when (typep agent 'assistant)
        (with-package agent-package  ; jpb150304
          (load-agent-dialog agent agent-name-string agent-file-pathname)))
      
      ;;==============================================================================
      ;;                          load special WINDOW
      ;;==============================================================================
      ;;=== if the agent has a special interface window, we load the code here
      (when (private-interface agent)
        (setq file-pathname
              (make-pathname 
               :device (pathname-device agent-file-pathname)
               :directory (pathname-directory agent-file-pathname)
               :name (format nil "~A-window" agent-name-string)
               :type "lisp"))
        ;(format *debug-io*  "~&;load-agent /window: ~S" file-pathname)    
        (moss::catch-error
         (format nil "while loading ~A window code" agent-name-string)
         (and (probe-file file-pathname) 
              (compile-and/or-load file-pathname :external-format :utf-8)))     
        )
      
      ;;==============================================================================
      ;;                        Create assistant interface
      ;;==============================================================================
      
      ;; if the agent is an assistant agent, then start its conversation process
      ;; we first recover the agent structure from the list of local agents
      ;; we cannot launch the conversation before having loaded the ontology and
      ;; dialog files, which is why we do not start the conversation in the 
      ;; make-assistant function
      ;; also, since the defagent is called from the agent file, we cannot create a
      ;; special interface window, since when executing defagent, the rest of the 
      ;; file including the functions defining the specific window are not yet loaded  
      
      ;; if agent is an assistant and has a window interface, it launches a
      ;; conversation by using the converse skill
      ;(moss::trformat "load-agent ~S, :converse-v2 ~S" (key agent)
      ;                (and (member :converse-v2 *features*) T))
      ;(moss::trformat "type of agent assistant?: ~S" (typep agent 'assistant))
      ;(moss::trformat "NPA?: ~S" (eql :npa (assistant agent)))
      (cond
       ((and (typep agent 'assistant)(member :converse-v2 *features*))
        ;; if we have a PA for a human (not NPA), then create the PA conversation
        (unless (eql :npa (assistant agent))
          (send-message 
           (make-instance 'message :from (key agent) :to (key agent) :type :inform
             :action :converse :args `(:contact ,(key agent)))))
        )
       ;; versions prior to v13.1 use this function (when :converse-v2 is not in
       ;; *features*)
       ((typep agent 'assistant)
        (start-converse-process agent)))
      
      ;;==============================================================================
      ;;                  Initialize assistant voice interface
      ;;==============================================================================
      ;; this is done in the make-assistant functIon, Thus Voice Is Already Initialized
      ;(When (and (Typep Agent 'Assistant)(Voice-Io Agent))
      ;  (Net-Initialize-Voice Agent))
      
      ;; before leaving reset global entry
      (setf (omas::agent-being-loaded *omas*) nil)
      :done))
  )

#|
CG-USER(19): (omas::load-agent)
|#
;;;---------------------------------------------------- LOAD-AGENT-CREATE-DATABASE
;;; we don't allow separate databases for agents.

(defUn load-agent-create-database (agent agent-file-pathname agent-name-string)
  "creates a database for a newly created persistent agent.
Arguments:
   agent: agent being loaded
   agent-file-pathname: pathname to locate files
Return:
   T if the database has been created error otherwise."  
  ;; otherwise, we have a new database. Open it, load ontology file and fill
  ;; the database
  (let ((file-pathname
         (make-pathname 
          :device (pathname-device agent-file-pathname)
          :directory (pathname-directory agent-file-pathname)
          :name (format nil "~A-DATABASE" agent-name-string)
          :type "lisp"))
        base-handle)
    (format t "~&;load-agent-process-persistency / opening base: ~S" 
            file-pathname)
    (setq base-handle
          ;(open-database agent :filename file-pathname))
          (open-database agent)) ; JPB1011
    (unless base-handle
      (error "can't open object database. base-handle: ~S" base-handle)) 
    ;; save handle, leave the database opened
    (setf (base agent) base-handle)
    ;; tell caller that we successfully created the database
    t))

;;;------------------------------------------------------------- LOAD-AGENT-DIALOG

(defun load-agent-dialog (agent agent-ref agent-file-pathname)
  "load the dialog file far an assistant agent. If the file is not the standard one ~
   then makes a temporary clone, replacing all XXXXX marks by the agent name and ~
   loading it, then deleting the file."
  (let (filename temp-pathname file-pathname)
    ;(format t "~&;load-agent-dialog /(dialog-file agent): ~S" (dialog-file agent))
    
    (cond
     
     ;;=== if the file is not the standard file, assume it is shared
     ((setq filename (dialog-file agent))
      ;(format t "~&;load-agent-dialog /cloning: ~S" filename)
      ;; clone the file
      (setq temp-pathname 
            (clone-and-adjust-file agent-ref filename
                                   :out-filename (string+ agent-ref "-DIALOG")))
      ;; load it
      (with-package (ontology-package agent)
        (moss::catch-error
         (format nil "while loading ~A dialog" agent-ref)
         (when (probe-file temp-pathname) 
              (format t "~%; load-agent-dialog /*package*: ~S" *package*)
              ;; m-load does not work, since it is intended for ontologies...
              ;(moss::m-load temp-pathname :external-format :utf-8)
              (compile-and/or-load temp-pathname :external-format :utf-8))))
      ;; once loaded delete the clone
      (delete-file temp-pathname)
      )
     
     ;;=== otherwise do load standard TASKS file
     (t
      ;; load the agent TASKS file, only if it exists (only for a PA)	
      (setq filename (format nil "~A-dialog" agent-ref))
      (setq file-pathname
            (make-pathname 
             :device (pathname-device agent-file-pathname)
             :directory (pathname-directory agent-file-pathname)
             :name filename
             :type "lisp"))
      ;(format t "~&;load-agent-dialog /file-pathname: ~S" file-pathname)
      ;; load file
      (with-package (ontology-package agent)
        (moss::catch-error
         (format nil "while loading ~A dialogs" agent-ref)
         (and (probe-file file-pathname) 
              (compile-and/or-load file-pathname :external-format :utf-8)))
        )))))

;;;----------------------------------------------------------- LOAD-AGENT-ONTOLOGY

(defun load-agent-ontology (agent agent-ref agent-file-pathname)
  "load ONTOLOGY depending on the persistency state of the agent:
   - if the agent is not persistent, we load the ontology file
   - if the agent is persistent but is created for the first time, we load the ~
   ontology file, then save the world into the database
   - if the agent is persistent and the database exists, we do not load the ~
   ontology file, since concepts have been loaded from it get agent structure ~
   now it has been loaded."
  (let (filename file-pathname agent-package temp-pathname)
    ;;=== if agent is persistent and database exists do not load ontology
    (unless (eql (persistency agent) :installed)
      ;; here agent is not persistent or ontology is not yet installed
      ;; get the agent ontology package (normally same as agent keyword)
      (setq agent-package (ontology-package agent))
      
      ;;=== we first load the ontology file standard or shared before looking at
      ;; the persistency status
      
      (cond
       ;;== external non standard file
       ((setq filename (ontology-file agent))
        ;; if file is a non standard file, then clone the external file replacing
        ;; the XXXXX markers by the agent name
        (setq temp-pathname
              (clone-and-adjust-file agent-ref filename
                                     :out-filename (string+ agent-ref "-ONTOLOGY")))
        
        (with-package (ontology-package agent)
          (moss::catch-error
           (format nil "while loading ~A dialog" agent-ref)
           (when (probe-file temp-pathname) 
             (format t "~%; load-agent-ontology /*package*: ~S" *package*)
             (moss::m-load temp-pathname :external-format :utf-8)
             ;; then create MOSS person (user name) and master link
             (if (typep agent 'assistant)
                 (make-master-reference agent))
             )
           )
          )
        ;; once loaded delete the clone
        (delete-file temp-pathname)
        )
       
       ;;== here there is no external file (an ontology file is not compulsory)
       (t
        ;; compose the name of an ontology file
        (setq filename (format nil "~A-ontology" agent-ref))
        ;; set ontology pathname
        (setq file-pathname
              (make-pathname 
               :device (pathname-device agent-file-pathname)
               :directory (pathname-directory agent-file-pathname)
               :name filename
               ;:name (format nil "~A-ontology" agent-name-string)
               :type "lisp"))
        
        ;; load the agent ONTOLOGY file only if it exists (it might not)
        ;(format *debug-io*  "~&;load-agent /ontology: ~S" file-pathname)
        ;; catch errors from bad MOSS syntax (this avoids error breaks while
        ;; trying to reload the file...)
        (moss::catch-error
         (format nil "while loading ~A ontology" agent-ref)
         (with-package (ontology-package agent)
           ;(format t "~%; load-agent-ontology /*package*: ~S" *package*)
           (and (probe-file file-pathname)
                (moss::m-load file-pathname :external-format :utf-8))
           ;; then create MOSS person (user name) and master link JPB 1406
           (if (typep agent 'assistant)
               (make-master-reference agent))
           ))
        ))
      
      ;;== check now persistency
      (when (omas::persistency agent)
        ;; now save world (system entry points are not yet merged into *moss-system*)

        ;(break "persistency situation: *moss-entry* not merged yet ready to save world.")
        
        ;; save everything, making sure we are in the agent package
        (moss::with-package agent-package
          ;; should set moss::*database-pathname* to agent database
          ;; and moss::*application-package* to agent-package
          (let ((moss::*database-pathname* (omas-database *omas*))
                (moss::*application-package* agent-package))
            (send (symbol-value (intern "*MOSS-SYSTEM*" agent-package)) '=save))
          ))
      )))

;;;===============================================================================
;;;                       FUSE ENTRY POINTS functions
;;;===============================================================================
;;; the following functions determine the list of entry points shared by the agent
;;; and by the MOSS system and merge the common ones for all contexts

;;;------------------------------------------------------- %%LOAD-AGENT-FUSE-ENTRY

(defUn %%load-agent-fuse-entry (agent entry)
  "fuses system entry with local entry.
Argument:
   agent: agent
   entry: entry name (value is system entry)
   base-handle: object base handle
Return:
   new entry value"
  (when (moss::%pdm? entry)
    (let ((system-entry-l (symbol-value entry))
          value-list prop-id values local-entry-l)
      ;; loading object replaces the value of entry with local value
      (load-object agent entry)
      (setq local-entry-l (symbol-value entry))
      ;; merge each property of system entry with local entry
      (dolist (item system-entry-l)
        ;; value may be ((0 a b c)(2 e d f)...) we must fuse all versions
        (setq prop-id (car item)
              value-list (cdr item))
        (setq values 
              (%%load-agent-fuse-values value-list 
                                        (cdr (assoc prop-id local-entry-l))))
        ;; replace property values in local entry
        (moss::nalist-replace-pv entry prop-id values))
      ;; OK done
      (symbol-value entry))))

#|
? (setq PERSON  ; in the MOSS package
 '((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 PERSON)) (MOSS::$ENAM.OF (0 moss::$PERSON))
 (MOSS::$EPLS.OF (0 $SYS.1))))
? (setq PERSON  ; in the address package
 '((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 PERSON)) (MOSS::$ENAM.OF (0 $E-PERSON))
 (MOSS::$EPLS.OF (0 $E-MOSS-SYSTEM.1))))

? (%%load-agent-fuse-entry address::SA_ADDRESS 'PERSON))
((moss::$type (0 moss::$ep)) (moss::$id (0 person))
 (moss::$enam.of (0 moss::$person $e-person))
 (moss::$epls.of (0 $sys.1 $e-moss-system.1)))
|#
;;;------------------------------------------- %%LOAD-AGENT-FUSE-MOSS-ENTRY-POINTS

(defUn %%load-agent-fuse-moss-entry-points (agent entry-point-list)
  "merge system and local entry points, fusing the shared ones. We fuse all versions.
Arguments:
   agent: agent
   entry-point-list: list of local unbound entry points.
Return:
   list of merged entry points."
  ;;********** we should do that for all versions
  (declare (special moss::*moss-system*))
  (let ((moss-ep-list (moss::%%has-value moss::*moss-system* 'moss::$EPLS 
                                         (moss-context agent)))
        shared-ep-list)
    ;; determine shared entries
    (setq shared-ep-list (intersection moss-ep-list entry-point-list))
    (format t "~&; %%load-agent-fuse-moss-entry-points / shared entry points:~& ~S"
            shared-ep-list)
    ;; fuse content
    (when shared-ep-list
      ;; fuse the shared entries
      (mapc #'(lambda (xx) (%%load-agent-fuse-entry agent xx)) shared-ep-list))
    ;; update local *moss-system* (some of the entries are still not loaded...)
    ;(moss::nalist-replace-pv *moss-system* 'moss::$EPLS result)
    ;; return list of fused ep
    shared-ep-list))
   
;;;------------------------------------------------------ %%LOAD-AGENT-FUSE-VALUES

(defUn %%load-agent-fuse-values (values1 values2 &optional result)
  "takes two lists of context tagged values and fuses them.
Arguments:
   values1: a list like ((0 a b)(2 a b c)...)
   values2: same kind of list
Return:
   the fused list."
  (cond ((null values1) (append (reverse result) values2))
        ((assoc (caar values1) values2)
         (%%load-agent-fuse-values 
          (cdr values1)
          (remove (caar values1) values2 :key #'car)
          (cons (%%load-agent-fuse-values-merge 
                 (car values1) (assoc (caar values1) values2))
                result)))
        (t (%%load-agent-fuse-values (cdr values1)
                                     values2 
                                     (cons (car values1) result)))))
         
#|
(%%load-agent-fuse-values '((0 a b c) (2 c d) (5 e f)) '((0 a b r) (3 s y) (5 z)))
((0 C A B R) (2 C D) (5 E F Z) (3 S Y))
|#
;;;----------------------------------------------- %%LOAD-AGENT-FUSE-VALUES-MERGE

(defUn %%load-agent-fuse-values-merge (values1 values2)
  "takes two lists of context tagged values and fuses them.
Arguments:
   values1: a list like (0 a b)
   values2: same kind of list
Return:
   the fused list."
  (if values1
    (cons (car values1) 
          (delete-duplicates 
           (append (cdr values1)(cdr values2))
           :test #'(lambda(xx yy)
                     (if (and (stringp xx) (stringp yy))
                       (string-equal xx yy)
                       (equal xx yy)))))
    values2))

#|
(%%load-agent-fuse-values-merge '(0 "a" "b" :c :d) '(0 :c "B" :s :v :u))
(0 "a" :D :C "B" :S :V :U)

(%%load-agent-fuse-values-merge '(0 "a" "b" "C" "d") '(0 "c" "B" "s" "v" "u"))
(0 "a" "d" "c" "B" "s" "v" "u")
|#
;;;============================ end fusing entry points ==========================

;;;--------------------------------------------- LOAD-AGENT-INITIALIZE-ENVIRONMENT
;;; unused when using asdf

(defUn load-agent-initialize-environment (agent base-handle)
  "downloads from the object database all the items necessary to inialize the agent ~
   environment. *moss-system* is supposed to exist. Description of the agent ~
   object and skills have been recreatedand should be merged (?)
Arguments:
   agent: current agent
   file-handle: handle to the database
Return:
   :done"

  ;; when entering this function several MOSS objects have been created when 
  ;; creating the agent, namely:
  ;; - concepts (MOSS-SYSTEM, OMAS-AGENT, OMAS-SKILL)
  ;; - counters (orphan-counter linked to moss-system, system, agent and skill 
  ;;   counters)
  ;; - instances (*moss-system*, agent ($E-OMAS-AGENT.1), skills)
  ;; - entry points (agent-name, skill names, system name)
  ;; the %initialize-moss-classes-from-database function has already been called
  ;; while creating the MOSS object representing the agent. Consequently:
  ;; - the MOSS agent object is initialized
  ;; - the name of the agent e.g. ADDRESS-AGENT points to $E-MOSS-AGENT.1 (usually)
  ;; - the local *moss-system* environment has been loaded from disk
  ;; - entry points have been reconciled
  ;; - variables, macros, service functions have been loaded
  ;; Thus, the function is useless


  (declare (ignore agent base-handle))
  #|  
  ;; the *moss-system* variable is supposed to be the one defined in the agent
  ;; package. However, this function executes in the OMAS package. Thus, we must be
  ;; careful
  ;; (intern "*MOSS-SYSTEM*" (ontology-package agent)) -> ADDRESS::*moss-system*
  ;; (symbol-value ADDRESS::*moss-system*) -> ADDRESS::$E-SYSTEM.1
  ;; *moss-system* is created in %create-moss-classes-and-proxies called by
  ;; make-moss-agent and its value saved into the moss-system slot of the agent
  
  (let ((agent-moss-system (moss-system agent))
        object-list agent-id skill-list)
    
    ;;=== variables
    (setq object-list (moss::%get-value agent-moss-system 'moss::$SVL))
    (mapc #'(lambda (xx) (load-object agent xx) (compile xx)) object-list)
    
    ;;=== macros
    (setq object-list (moss::%get-value agent-moss-system 'moss::$SML))
    (mapc #'(lambda (xx) (load-object agent xx) (compile xx)) object-list)
    
    ;;=== methods
    (setq object-list (moss::%get-value agent-moss-system 'moss::$FNLS))
    (mapc #'(lambda (xx) (load-object agent xx) (compile xx)) object-list)
    
    ;;=== service functions
    (setq object-list (moss::%get-value agent-moss-system 'moss::$SFL))
    (mapc #'(lambda (xx) (load-object agent xx) (compile xx)) object-list)
    
    ;;=== agent object
    ;; we have a special case for the agent object since it has been redefined in
    ;; %create-moss-classes-and-proxies, the current skills have been attached to
    ;; the agent, but other types of info could have been attached to the object 
    ;; during previous sessions. Thus, we must reconcile the two versions of the
    ;; agent object
    ;; get the agent moss id (stored on the p-list of the agent keyword
    (setq agent-id (get (agent-key agent) :ID))
    ;; first save the list of skills
    (setq skill-list (send agent-id '=get "skill"))
    ;; load the object from disk, overwriting the object in core
    (load-object agent agent-id)
    ;; replace with current skill values
    (send agent-id '=set-list "skill" skill-list)
    
    ;;=== skills
    ;; skills (concept, counter and instances) have been redefined when executing
    ;; make-moss-agent, and are not normally saved into the database. Id. for
    ;; associated =summary methods
|#
    
  :done)

;;;-------------------------------------------------------------- LOAD-AGENT-TASKS

(defun load-agent-tasks (agent agent-ref agent-file-pathname)
  "load the tasks file for an assistant agent. If the file is not the standard one ~
   then makes a temporary clone, replacing all XXXXX marks by the agent name and ~
   loading it, then deleting the file."
  (let (filename file-pathname temp-pathname)
    ;(format t "~%; load-agent-tasks /agent: ~S package: ~S" agent *package*)

    (cond
     
     ;;=== if the file is not the standard file, assume it is shared
     ((setq filename (tasks-file agent))
      ;; clone the file
      (setq temp-pathname 
            (clone-and-adjust-file agent-ref filename
                                   :out-filename (string+ agent-ref "-TASKS")))
      (with-package (ontology-package agent)
        (moss::catch-error
         (format nil "while loading ~A tasks" agent-ref)
         (and (probe-file temp-pathname) 
              (moss::m-load temp-pathname :external-format :utf-8))))
      ;; once loaded delete the clone
      (delete-file temp-pathname)
      )
     
     ;;=== otherwise do load standard TASKS file
     (t
      ;; load the agent TASKS file, only if it exists (only for a PA)	
      (setq filename (format nil "~A-tasks" agent-ref))
      (setq file-pathname
            (make-pathname 
             :device (pathname-device agent-file-pathname)
             :directory (pathname-directory agent-file-pathname)
             :name filename
             :type "lisp"))
      ;(format *debug-io*  "~&;load-agent /tasks: ~S" file-pathname)
      ;; one problem is that a shared file has no specific package
      (with-package (ontology-package agent)
        (moss::catch-error
         (format nil "while loading ~A tasks" agent-ref)
         (and (probe-file file-pathname) 
              (moss::m-load file-pathname :external-format :utf-8)))
        )))))

;;;-------------------------------------------------------------- LOAD-APPLICATION
;;; moved to omas-load.lisp file (used to load omas)

;;;---------------------------------------------------------------  MAKE-FILE-NAME

(defUn make-file-name (reference-pathname coterie-name application-name file-name)    
  "creates a pathname applying to an application file, contained in the ~
   application subdirectory.    
Arguments:    
   reference-pathname: absolute pathname to the main directory,     
                       e.g. *omas-load-pathname*    
   coterie-name: name of the local coterie, e.g. THOR    
   application-name: name of the application, e.g. TEST    
Return:    
   a composed pathname,     
   e.g. #p\"C:\\Program Files\\ACL60\\OMAS-v5.0.15a-PC25\\Z-THOR-TEST\\agents.lisp\"     
   "    
  (omas::make-new-pathname    
   reference-pathname    
   :directory (append 
               (pathname-directory reference-pathname)    
               (list (concatenate 'string coterie-name "-" application-name)))    
   :name file-name    
   :type "lisp"))    

;;;------------------------------------------------ MAKE-APPLICATION-FILE-PATHNAME

(defUn make-application-file-pathname     
       (reference-pathname folder coterie-name application-name file-name
                           &optional (file-type "lisp"))    
  "creates a pathname applying to an application file, contained in the ~
   application subdirectory.    
Arguments:    
   reference-pathname: absolute pathname to the main directory,     
                       e.g. *omas-load-pathname*    
   folder: folder in which are the applications (normally: \"applications\")    
   coterie-name: name of the local coterie, e.g. THOR    
   application-name: name of the application, e.g. TEST    
   file-name: name of the file, e.g. FAC, agents, Z-MESSAGES    
Return:    
   a composed pathname,     
   e.g. #p\"C:\\Users\\barthes\\OMAS-projects\\agents.lisp\"     
   "    
  (make-pathname    
   :device (pathname-device reference-pathname)    
   :directory (append 
               (pathname-directory reference-pathname) 
               (if folder (list folder))    
               (list (concatenate 'string coterie-name "-" application-name)))
   :name file-name    
   :type file-type))

#|
(omas::make-application-file-pathname 
  omas::*omas-directory-string* "applications" "UTC" "SERVER" "HDSRI-WEB")
#P"C:\\Program Files\\Allegro CL 8.2\\OMAS-MOSS 8.1.3\\OMAS\\applications\\UTC-SERVER\\HDSRI-WEB.lisp"
|#
;;;--------------------------------------------------------- MAKE-MASTER-REFERENCE
;;; (master-identity agent) must contain something like
;;; ("person" ("name" "barthès")("first-name" "Jean-Paul") ...
;;;           ("personal assistant" "albert")) if we are creating albert
;;; the personal assistant clause is compulsory and found here because of the 
;;; language that applies to the property names

(defun make-master-reference (agent)
  "when agent is an assistant and the slot master-identity is filled, then create ~
   a representation of the master and of the assistant."
  (let ((master-description (master-identity agent)))
    ;(format t "~%; make-master-reference /*package*: ~S" *package*)
    (with-package (find-package (key agent))
      (when master-description
        (with-transaction agent
          ;; already created by agent-make-moss-environment
;;;          (make-individual "omas agent" `("ag name" ,(symbol-name (key agent)))
;;;                           `("ag key" ,(key agent))
;;;                           `("ag language" ,(language agent)))
          (apply #'make-individual master-description))))))                   
                                     
;;;-------------------------------------------------------------- OI-HIDE-ON-CLICK

(defun oi-hide-on-click (dialog widget)
  "set the AUTOMATIC flag in the omas parameters, which will cause not to show the ~
   init window and load the application automatically."
  (declare (ignore widget)(special *omas-parameters*))
  ;; must update the automatic entry in the omas-parameters list
  ;; send a warning to the user
  (when (y-or-n-p 
         "If you hide this window, then the application will start automatically.
To get the window again, hold the SHIFT button while reloading the application.
Do you really want to hide the window?")
    ;; read the omas parameters from the window and save them into *omas*
    (set-parameters-from-init-window dialog)
    
    ;; set AUTOMATIC to T
    (setf (omas-automatic *omas*) T)
    ;; save them into parameters file
    (record-omas-parameters)
    
    ;; rebuilds the *omas-parameters* list
    (setq *omas-parameters* (initialize-omas-parameters))
    
    ;; now load application, trying first applications folder
    (if (or (load-application) 
            (load-application "sample-applications"))     
        ;; and close init window
        (%window-close dialog))
    )
  ;; otherwise do nothing
  t)
  
;;;-------------------------------------------------------------  OI-LOAD-ON-CLICK    

(defUn oi-load-on-click (dialog widget)    
  "launches the application by loading the corresponding files and by creating the     
control panel in a specific DISPLAY thread.    
The initializing menu is closed but the application does not quit."    
  (declare (ignore widget)) 
  ;; read values from the window and save them into *omas*
  (set-parameters-from-init-window dialog)
  ;; and update parameters file
  (record-omas-parameters)
  
  ;; now load application, trying first applications folder
  (if (or (load-application) 
          (load-application "sample-applications"))     
      ;; and close init window
      (%window-close dialog))
  
  ;; otherwise init window stays opened
  :done)

;;;----------------------------------------------- SET-PARAMETERS-FROM-INIT-WINDOW
;;; unused, when using asdf file

(defun set-parameters-from-init-window (dialog)
  "read the parameters from the init window and records them into *omas*.
Argument:
   dialog: the init window
Return:
   :done (the function is called for its side effect)"
    (declare (special *omas*))      
  (let (site-name appli/coterie-name omas-port local-broadcast-address)
    
    ;; Get parameters from init window
    ;; we do not consider AUTOMATIC since we can't get it from the window
    (setq site-name
          (string-trim 
           '(#\space)(string-upcase 
                      (%get-value (%find-named-item :site-name dialog))))
          appli/coterie-name
          (string-trim 
           '(#\space)(string-upcase     
                      (%get-value (%find-named-item :appli/coterie dialog))))
          omas-port
          (read-from-string
           (string-trim '(#\space)    
                        (string-upcase 
                         (%get-value (%find-named-item :port dialog)))))
          local-broadcast-address
          (string-trim '(#\space)    
                       (string-upcase     
                        (%get-value (%find-named-item :ip-address dialog)))) 
          )
    
    ;; ... and save values into site object
    (setf (site-name *omas*) site-name)
    (setf (coterie-name *omas*) appli/coterie-name)
    (setf (omas-port *omas*) omas-port)
    (setf (local-broadcast-address *omas*) local-broadcast-address)
  :done))


;;;-------------------------------------------------------------------  USER-CLOSE    

;;; the next method is assumed to be a special method called when closing the init    
;;; window, e.g. to prevent application from exiting.    

(defMethod user-close ((self omas-init-window))    
  "This is called when the user tries to close the window interactively.    
   The window will actually be closed only if we call call-next-method ~    
      or we call CLOSE directly."    
  ;(declare (ignore self))    
  ;; room for clean up here (e.g. sockets...)    
  ;(print "+++++test+++++")    
  (call-next-method)    
  )


#|
;;; test for 
(with-open-file (ss (cg:ask-user-for-directory 
                     :browse-include-files t
                     :root (make-pathname
                            :device (pathname-device *omas-directory*)
                            :directory
                            (append (pathname-directory 
                                     *omas-directory*)
                                    (list "OMAS" "applications"))  
                            )) 
                    :external-format (excl::find-external-format :unicode))
  (loop 
    (print (read ss))))
|#
(format t "~&;*** OMAS v~A - OMAS init window loaded ***" *omas-version-number*)

;;; :EOF