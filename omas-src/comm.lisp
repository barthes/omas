﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;                O M A S - C O M M  (file omas-comm.lisp)
;;;
;;;===============================================================================
;;; This file contains functions dealing with the communication process.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|  In practice
the functions are mainly used to analyse input messages, since all messages are
directly sent by the send-message function.
 
In the OMAS version an agent when created (after a defagent) has two processes:
   - one for running the skills that is started and put into a wait state
   - another one, the scanner, for processing the input-messages
During its life more processes can be created
   - for processing call-for-bids
   - for processing inform messages
   - for handling its master in case of an assistant agent
When a message is received, the scanner processes it. I.e., it decides 
   - whether it should be ignored (depending on the state of the agent),
   - whether it should interrupt what the agent is doing,
   - whether it should be inserted into the list of tasks to do (agenda),
   - whether it should create a special process to process it.
The agent process processes task it finds in the agenda until completion, unless
it is interrupted by the scanner (:abort, :cancel, :kill).

History
2020
 0202 restructuring OMAS as an asdf system
|#

(in-package :omas)

;;; ============================= main driver ===================================

;;;---------------------------------------------------------- agent-scan-messages

(defun agent-scan-messages (agent)
  "the function is associated with the input mailbox of the agent and waits for ~
      incoming messages. This takes place in the SCAN process.
When a message is received the scanner processes it. I.e., it decides 
   - whether it should be ignored (depending on the state of the agent),
   - whether it should interrupt what the agent is doing,
   - whether it should be inserted into the list of tasks to do (agenda),
   - whether it should create a special process to process it.
Arguments:
   agent: agent.
   package (key): global package in which process executes
   language (key): language of the agent
   moss::*context* (key): version of the ontology
   moss::*version-graph* (key): configuration of the ontology
Return:
   never returns unless killed."  
  ;; first establish process global variables
  (let ((*package* (ontology-package agent))
        (moss::*language* (language agent))
        (moss::*context* (moss-context agent))
        (moss::*version-graph* (moss-version-graph agent))
        message)
    (declare (special *package* moss::*language* moss::*context* 
                      moss::*version-graph*))    
    (loop
      ;; wait if no message in the mailbox
      (process-wait "idle: waiting for incoming message" #'input-messages agent)
      ;; remove one message from the mailbox
      (setq message (agent-scan-select-message agent))
      
      ;; update agent window
      (agent-display agent)
      (agent-trace agent "just received: ~s" (message-format message))
      ;; log message
      (push message (input-log agent))
      ;; record agent and skills if not already done
      (agent-parse-message-for-acquaintances agent message)
      
      (cond
       ;; if agent is a postman, then we have a special processing
       ;; ... unless message is specifically for us (but not in broadcast mode)
       ;; if receiver is a list, then it is not for us
       ((and (typep agent 'postman) 
             (or (listp (to! message)) ; list, therefore not only for postman...
                 (and (symbolp (to! message))  ; required for keywordize
                      (not (eql (key agent)(keywordize (to! message)))))))
        (agent-scan-process-postman-message agent message))
       
       ;; for non postmen, check if the message was for us
       ((setq message (agent-scan-message-for-us? agent message))
        ;; if so, check for acknowlegment
        (if (ack message)
            (send-message (message-make-acknowledgement agent message)))
        ;; and process message
        (agent-scan-process-message agent message))
       ))))

;;;-------------------------------------------------- agent-scan-messages-for-us?

(defun agent-scan-message-for-us? (agent message)
  "checks whether the message in the box was sent to the agent, either
   - point-to-point
   - broadcast
   - multicast (agent name in the list)
   - conditionally (list starting by :_cond)
   If so, accepts the message after checking eventual condition. 
   Otherwise discard it.
Arguments:
   agent: agent
   message: received message to check
Value:
   message where the to field has been changed to agent if the message was for us, ~
      nil otherwise."
  (let* ((to (to! message))
         (from (from! message))
         master-list staff-agent)
    
    ;; if agent is a staff agent, then it only accepts :request messages from its
    ;; master
    (when
      (and
       ;; message is a request
       (eql (type! message) :request)
       ;; agent is a staff agent
       (setq master-list (master agent))
       ;; sender is not local user or master
       (not (or (null from)
                (eql from (local-user *omas*))
                (member from (master agent))
                ;; or a staff agent of same master (they share a master
                (and (setq staff-agent (local-agent? from))
                     (intersection  master-list (master staff-agent)))
                )))
      (return-from agent-scan-message-for-us? nil))
    
    ;; otherwise, check addressing modes
    (cond
     ;; point-to-point ?
     ((eql (local-agent? to) agent) message)
     ;; multicast ?
     ;((and (listp to) (not (eql (car to) :_cond)) (member (name agent) to)) 
     ((and (listp to) (not (eql (car to) :_cond)) (member (key agent) to)) ; JPB0906
      message)
     ;; conditional distribution
     ((and (listp to) (eql (car to) :_cond))
      ;; check condition
      (dformat :cond 0 "agent-scan-message-for-us? / query:~%  ~S" (cadr to))
      (let* ((query (cadr to))
             (result (access query)))
        (cond
         ;; if query concerns agents, the receiving agent must be in the result
         ((and (string-equal "agent" (car query))
               (member (get (omas::key agent) :id) result))
          message)
         ;; otherwise the result must be non nil
         (result message))))
     ;; broadcast ?
     ((member to '(:all :all-and-me)) message)
     ;; not for us, so far discard it
     (t 
      ;(agent-trace agent "...not for us.") ; too much trace
      nil))))

#|
? (let ((agent biblio::biblio)
        (message (make-instance 'message 
                    :to '(:_cond ("omas-agent" ("skill" ("omas-skill" 
                                     ("name" :is ":get-publications")))))
                    :action :hello))
        (*package* (find-package :BIBLIO)))
   (agent-scan-message-for-us? agent message))
#<MESSAGE 16:16:25 NIL (:_COND
                        ("omas-agent"
                         ("skill"
                          ("omas-skill"
                           ("name" :IS
                            ":get-publications"))))) NIL :HELLO NIL NIL Tid:NIL 
                                 :BASIC-PROTOCOL no-TO TL:3600>

? (let ((agent biblio::biblio)
        (message (make-instance 'message 
                    :to '(:_cond ("omas-agent" ("skill" ("omas-skill" 
                                     ("name" :is ":hello")))))
                    :action :hello))
        (*package* (find-package :BIBLIO)))
   (agent-scan-message-for-us? agent message))
NIL

? (let ((agent biblio::biblio)
      (message (make-instance 'message :type :request :from :sa-address
                              :to :biblio
                              :action :hello))
      (*package* (find-package :BIBLIO)))
  (agent-scan-message-for-us? agent message))
#<MESSAGE 17:14:16 :SA-ADDRESS :BIBLIO :REQUEST :HELLO NIL NIL Tid:NIL :BASIC-PROTOCOL
     no-TO TL:3600>

? (let ((agent biblio::biblio)
      (message (make-instance 'message :type :request :from :albert
                              :to :biblio
                              :action :hello))
      (*package* (find-package :BIBLIO)))
  (agent-scan-message-for-us? agent message))
#<MESSAGE 17:14:16 :SA-ADDRESS :BIBLIO :REQUEST :HELLO NIL NIL Tid:NIL :BASIC-PROTOCOL
     no-TO TL:3600>

? (let ((agent biblio::biblio)
      (message (make-instance 'message :type :request :from (spy-name *omas*)
                              :to :biblio
                              :action :hello))
      (*package* (find-package :BIBLIO)))
  (agent-scan-message-for-us? agent message))
NIL

? (let ((agent biblio::biblio)
      (message (make-instance 'message :type :request 
                              :to :biblio
                              :action :hello))
      (*package* (find-package :BIBLIO)))
  (agent-scan-message-for-us? agent message))
#<MESSAGE 17:14:16 :SA-ADDRESS :BIBLIO :REQUEST :HELLO NIL NIL Tid:NIL :BASIC-PROTOCOL
     no-TO TL:3600>

? (let ((agent biblio::biblio)
      (message (make-instance 'message :type :request :from :<odin-user>
                              :to :biblio
                              :action :hello))
      (*package* (find-package :BIBLIO)))
  (agent-scan-message-for-us? agent message))
#<MESSAGE 17:16:45 :<ODIN-USER> :BIBLIO :REQUEST :HELLO NIL NIL Tid:NIL :BASIC-PROTOCOL no-TO TL:3600>

? (let ((agent biblio::biblio)
      (message (make-instance 'message :type :request :from :fac
                              :to :biblio
                              :action :hello))
      (*package* (find-package :BIBLIO)))
  (agent-scan-message-for-us? agent message))
NIL
|#
;;;--------------------------------------------------- agent-scan-process-message

(defun agent-scan-process-message (agent message)
  "takes an input message and decides what to do with it. if the agent is an ~
      assistant agent, then special actions can be taken, like transferring ~
      the message to the user unprocessed...
Arguments:
   agent: agent
   message: input to be dispatched."
  
  ;;******* debug
  ;(if (and (eql (from! message) :VICTIM-1)
  ;         (eql (to! message) :WORLD)
  ;         (eql (key agent) :WORLD)
  ;         )
  ;    (format t "~2%;++++++++ agent-scan-process-message / input-message(~S): ~%  ~S"
  ;      (key agent) message))
  ;;*******
  
  (case (type! message)
    (:abort           (agent-scan-abort agent message))
    (:alert           (agent-scan-alert agent message))
    (:answer          (agent-scan-answer agent message))
    (:acknowledge     (agent-scan-acknowledge agent message))
    (:bid             (agent-scan-bid agent message))
    (:bid-with-answer (agent-scan-bid-with-answer agent message))
    (:call-for-bids   (agent-scan-call-for-bids agent message))
    (:cancel          (agent-scan-cancel agent message))
    (:cancel-grant    (agent-scan-cancel-grant agent message)) 
    (:display         (agent-scan-display agent message))
    (:grant           (agent-scan-grant agent message))
    (:inform          (agent-scan-inform agent message))
    (:internal
     ;; an internal message is either executed or is equivalent to a request
     ;; and thus put into the agenda (we don't update agent window...)
     (agent-scan-internal agent message))
    (:request
     (agent-insert-into-agenda agent message)
     (agent-display agent))
    (:sys-inform
     ;; sys-inform is used by the system to send inform messages i.e. to spy
     ;; such messages are transparent to be processed only by postmen
     (agent-scan-sys-inform agent message))
    (otherwise
     (warn "agent ~A received a message of unknown type ~S~&Message: ~S"
           (name agent) (type! message) message))
    )
  :exit-agent-scan-process-message)

;;;------------------------------------------- AGENT-SCAN-PROCESS-POSTMAN-MESSAGE

(defun agent-scan-process-postman-message (agent message)
  "when agent is a postman, then the messages must be sent to the destination. ~
   The way to do it is to send an internal message of type :inform with skill :send ~
   and content the message we just received in a linear format. The :send skill ~
   will further process the message before sending it.
Arguments:
   agent: agent
   message: the message object
Return:
   :done."
  (let ((message-string (net-message-to-string message :no-packing t))
        new-message)
    ;; print the message
    ;(format t "~% agent-scan-process... Message to be sent by the ~A postman:~&   ~S" 
    ;        (name agent) message-string)
    
    ;; KLUDGE: if the postman is the SPY agent for tracing the messages, then we
    ;; send an ALERT message rather than an INFORM message
    ;; otherwise, build an INFORM internal message. This will pass the message
    ;; content to the postman :send skill (pass the marshalled and original message)
    ;; We can do that since we do not leave the Lisp environment JPB0807
    (cond
     ((eql *package* (find-package :spy))
      (setq new-message 
            (message-make-alert agent agent :send (list message-string message)))
      ;(format *debug-io* "alert-message: ~S" new-message)
      (agent-scan-alert agent new-message)
      )
     (t
      (setq new-message 
            (message-make-inform agent agent :send (list message-string message)))
      ;; process it directly, which will create a new thread
      (agent-scan-inform agent new-message))
     )

    ;(setf (input-messages agent) (append (input-messages agent) (list new-message)))
    :done))
  
;;;---------------------------------------------------- AGENT-SCAN-SELECT-MESSAGE
  
(defun agent-scan-select-message (agent)
  "looks for :abort, then :cancel, then :cancel-grant messages, then picks a ~
   random one. Removes the selected message from the mail-box.
Arguments:
  agent: agent
Return
  a message to process."
  (excl::without-interrupts
    (let ((message-list (input-messages agent)))
      ;; if agent is spy, must pick messages in the order they come in
      (when (eql (key agent) (spy-name *omas*))
        (let ((message (pop message-list)))
          (%agent-remove-value agent message :input-messages)
          (return-from agent-scan-select-message message)))
      ;; do some work only when more than one message in the queue
      (when (cdr message-list)
        ;; go first for abort messages
        (dolist (message message-list)
          (when (eql (type! message) :abort)
            (%agent-remove-value agent message :input-messages)
            (return-from agent-scan-select-message message)))
        ;; then for cancels
        (dolist (message message-list)
          (when (eql (type! message) :cancel)
            (%agent-remove-value agent message :input-messages)
            (return-from agent-scan-select-message message)))
        ;; then for cancel-grants
        (dolist (message message-list)
          (when (eql (type! message) :cancel-grant)
            (%agent-remove-value agent message :input-messages)
            (return-from agent-scan-select-message message))))
      ;; otherwise pick the first one
      (pop (input-messages agent)))))

#|
(defun agent-scan-assistant-will-process (agent message)
  "check if we got a message for our master or if it is for us to process.
Arguments:
   agent: agent
   message: received message"
  (declare (ignore agent))
  ;; check if message will be processed specially
  (and
   (member (action message) *master-actions*)
   (member (type! message) '(:request :abort :cancel :answer :inform)))
  ;; acknowledge, grant cancel-grant, bid, bid-with-answer, call-for-bids
  ;; are processed as usual
  )
|#
;;;=============================================================================
;;;
;;;                             SCAN FUNCTIONS
;;;
;;;=============================================================================

;;;---------------------------------------------------- AGENT-ACCEPTABLE-ANSWER?

(defun agent-acceptable-answer? (agent message)
  "right now everything is acceptable"
  (declare (ignore agent message))
  t)

;;;---------------------------------------------------- AGENT-INSERT-INTO-AGENDA

(defun agent-insert-into-agenda (agent message &aux task)
  "function that inserts message into the agenda to be processed by main agent process.
Arguments:
   agent: agent
   message: message to be processed."
  ;; first check if we received a message in :free-style, in which case we replace 
  ;; action with PROCESS
  (if (eql (protocol message) :free-style) (setf (action message) :PROCESS))
  ;; then process normally
  (if
    ;; if the answer is a read out from memory, process it right away
    ;; wait a minute, all messages are not requests... 
    (and (member :learning (features agent))
         (setq task (agent-check-memory-for-same-task agent message)))
    ;; then send back the answer readily
    (send-message
     (message-make-answer agent (from! message) (task-id message)
                          (skill task) (arg-list task) (answer task)))
    ;; otherwise give the message to agent to process
    (setf (agenda agent) (append (agenda agent) (list message)))
    ))

;;;------------------------------------------------------------ AGENT-SCAN-ABORT

(defun agent-scan-abort (agent message)
  "function that processes a message that will abort a task ~
      (cancel a task and send and error message to manager).
   In order to locate the right task-frame we need the task-number ~
      and the identity of the original sender, since two tasks ~
      from different coteries could have the same task-number.
Arguments:
   agent: agent
   message: message to process (sender id in the args list)
Return:
   :done"
  ;; when an agent is receiving an abort message, then three cases
  ;; can occur:
  ;;   - some process (thread) is executing the task to be aborted
  ;;   - the task is waiting to be executed in the input-messages bin or in the
  ;;     agenda bin
  ;;   - the task is no longer present
  (let ((task-id (task-id message))
        (sender (car (args message)))
        target-task-frame)
    (agent-trace agent "~&... aborting task ~A and its subtasks" (task-id message))
    ;; we have a potential racing condition from the agenda, so get rid of the
    ;; messages that are still in the agenda and are unprocessed (could be repeat
    ;; messages)
    (#+MCL without-interrupts #+MICROSOFT-32 excl::without-interrupts
           (%agent-remove-if agent #'(lambda (xx) (and (eql (task-id xx) task-id)
                                                       (eql (from! xx) sender)))
                             :agenda)
           ;; ... and from tasks-to-do master bin (if none does not matter)
           (%agent-remove-if agent #'(lambda (xx) (and (eql (task-id xx) task-id)
                                                       (eql (from! xx) sender)))
                             :waiting-messages))
    ;; update master screen if applicable, just in case
    (if (com-window agent)
        (assistant-display-tasks-to-do agent))
    
    ;; clear sent pending-bids related to the task, i.e., if the request was
    ;; a broadcast using the CN protocol, and we made a bid
    ;;***** the sender corresponds to the :to slot of the pending bid message?
    (setf (pending-bids agent)
      (delete-if #'(lambda (xx) (and (eql (task-id xx) task-id)
                                     (eql (to! xx) sender))) 
                 (pending-bids agent)))
    ;; now, let's see if the task was actually started
    ;; look for active task-frame
    (setq target-task-frame 
          (car (member-if #'(lambda (xx) (and (eql (id xx) task-id)
                                              (eql (from! xx) sender))) 
                          (active-task-list agent))))
    ;; if task is being active, then kill it and associated subtasks
    (when target-task-frame
      (abort-task agent target-task-frame :ignore-queues t))
    
    ;; remove all messages from the input message-box (here we have time since
    ;; we are executing the scan process. Do that last however since new messages
    ;; could arrive asynchronously (e.g. a cancel-grant)
    (%agent-remove-if agent #'(lambda (xx) (and (eql (task-id xx) task-id)
                                                (eql (from! xx) sender)))
                      :input-messages)
    ;; do an agent display in case something changed
    (agent-display agent)
    ;; otherwise, when the task is no longer present we do nothing.
    :done))

;;;------------------------------------------------------ AGENT-SCAN-ACKNOWLEDGE

(defun agent-scan-acknowledge (agent message)
  "Receives an acknowledge from the sent message. Should disable the repeat ~
   mechanism. Calls the optional acknowledge function of the skill.
Arguments:
   agent: agent
   message: acknowledge message"
  (let ((subtask-id (task-id message))
        task-message fn)
    ;; locate subtask frame to indicate acknowledgement 
    (multiple-value-bind (task-frame subtask-frame)
        (agent-get-task-frame-from-subtask-id agent subtask-id)
      ;; if there, record acknowledgement, otherwise forget it (task probably canceled)
      (when subtask-frame
        (setf (acknowledge subtask-frame)
          (cons (from! message) (acknowledge subtask-frame))))
      ;; look for an acknowledge option in the skill
      (setq task-message (message task-frame))
      (when task-message
        (setq fn (agent-get-skill-acknowledge agent (action task-message)))
        (when fn
          ;; apply, then give up
          ;(apply fn agent (environment agent) (args message))
          (apply fn agent message (args message)) ; JPB0906
          )))
    ;; otherwise do nothing (here should disable the repeat mechanism)
    ;; upgrade representation and quit
    (agent-display agent)
    :done))

;;;------------------------------------------------------------ AGENT-SCAN-ALERT

(defun agent-scan-alert (agent message &aux fn)
  "processes an incoming message in the scan thread, protected with a no-error ~
   condition. Done for messages that require immediate attention."
  (declare (special *current-message*))
  ;; first establish process global variables
  (let ()  
    ;; save message so that we can use it in the skill code
    (setq *current-message* message)
    ;; first check if we have the skill
    (unless (agent-has-skill? agent (action message))
      (return-from agent-scan-alert nil))
    
    ;; if we do, process the message taking care of possible errors (we do not
    ;; want to crash the scan process)
    ;(ignore-errors
     ;; look for any preconditions
     (setq fn (agent-get-skill-preconditions agent (action message)))
     (when fn
       ;; if preconditions don't apply, then give up
       (unless (apply fn agent message (args message)) ; JPB0906
         (return-from agent-scan-alert nil)))
     
     ;; otherwise get skill function (only static)
     (setq fn (agent-get-static-skill agent (action message)))
     (when fn
       ;; signal that we are entering the skill, unless it's for graphics window
       ;; i.e. we do not want to signal messages going to the graphics window
       (unless (eql (to! message)(key (symbol-value (spy-name *omas*)))) ; JB1506
         (dformat :skill-header 0
                  "~2%;========== ~A: Entering ~A (alert) ==========" 
                  (key agent) (action message)))
       ;; apply skills immediately
       (apply fn agent message (args message)) ; JPB0906
       )
     ;; reset current message
     (setq *current-message* nil)
     ;)
    
    ;; if *current-message is non mil then we had an error in executing one of the
    ;; expressions
    (when *current-message*
      (warn "*** ~S: The alert message could not be executed normally: ~S~
Package: ~S ***"
        (name agent) *current-message* *package*)
      
      (setq *current-message* nil)
      (return-from agent-scan-alert :error))
    
    ;; upgrade representation and quit
    (agent-display agent)
    :done))

;;;----------------------------------------------------------- AGENT-SCAN-ANSWER


(defun agent-scan-answer (agent message)
  "Received answer is put into the saved-answer-list of the proper task, ~
      which will trigger the process waiting on that slot (agent-process-answers).
Arguments:
   agent: agent
   message: answer message."
  (let ((task-frame (agent-get-task-frame-from-subtask-id agent (task-id message))))
    (dformat :send 1 "agent-scan-answer / task-frame: ~S" task-frame)
    ;; the task or subtask-id of the message is the one that we issued for the 
    ;; request. Thus we can use it confidently, since it is unique within a local
    ;; coterie
    ;; The answering message is inserted into the task-frame slot and not into a
    ;; a subtask-frame because the task process is the same for all subtasks and is 
    ;; waiting on the saved-answer-list task slot
    ;; When the task is no longer active, we simply ignore the answer
    
    (when task-frame 
      (setf (saved-answer-list task-frame)
        (append (saved-answer-list task-frame)(list message))))
    ;; return content for debugging purposes (when tracing function)
    (if task-frame (saved-answer-list task-frame) :no-task-frame)))

;;;-------------------------------------------------------------- AGENT-SCAN-BID

(defun agent-scan-bid (agent message)
  "agent received a bid. We first check if the subtask was not granted to some ~
      agents.
   If so, we ignore bid. Otherwise, if the contract-net strategy is ~
      :take-first-answer then we grant the task immediately canceling further ~
      offers. Otherwise, we simply add the bid to the list of saved-bids.
Arguments:
   agent: agent
   message: bid just received."
  (let ((subtask-id (task-id message)) 
        best-bid-list)
    ;; === first check if the task is part of our active task list
    ;; bids are addressed to the sender of the broadcast or multicast, thus,
    ;; there is no ambiguity on who issued the message and the subtask number
    ;; can be used to recover the subtask-frame 
    (multiple-value-bind (task-frame subtask-frame)
        (agent-get-task-frame-from-subtask-id agent subtask-id)
      ;(format t "~%; agent-scan-bid /task-frame: ~S~%; subtask-frame: ~S"
      ;  task-frame subtask-frame)
      ;; if not or if subtask gone, ignore bid
      (unless (or task-frame subtask-frame) (return-from agent-scan-bid nil))
      
      ;(format t "~%; agent-scan-bid /contractors: ~S"
      ;  (contractors subtask-frame))
      ;; check if the task was already granted. If so, ignore bid
      (when (contractors subtask-frame) (return-from agent-scan-bid nil))
      
      
      ;(format t "~%; agent-scan-bid /message: ~S" message)
      ;; === here we have a good bid to be processed
      (agent-trace agent "processing bid ~A" (message-format message))
      ;(format t "~%; agent-scan-bid /broadcast-max-answers: ~S" 
      ;  (broadcast-max-answers subtask-frame))
      (cond 
       ;; if we have a take-first-n-answers strategy and we got all the bids
       ((and (broadcast-max-answers subtask-frame)
             (>= (1+ (length (saved-bids subtask-frame)))
                 (broadcast-max-answers subtask-frame)))
        ;; save bid
        ;(format t "~%; agent-scan-bid /saving bid: ~S" message)
        (setf (saved-bids subtask-frame) 
          (cons message (saved-bids subtask-frame)))
        ;; kill cfb-timeout-timer (no longer usefull)
        (agent-trace agent "killing cfb-timeout-timer ~A" 
                     (cfb-timeout-process subtask-frame))
        (agent-process-kill agent (cfb-timeout-process subtask-frame))
        ;; select best bids, returns the list of all best (equal) bids
        (setq best-bid-list (select-best-bids agent (saved-bids subtask-frame)))
        
        ;; reset saved bids slot
        (setf (saved-bids subtask-frame) nil)
        ;; if none is available or none is acceptable, return nil
        (unless best-bid-list 
          (return-from agent-scan-bid nil))
        
        ;; grant task to agents that have the best offer
        ;; why set task-id to the last bid ??
        (send-message 
              (message-make-cancel-grant agent :all (task-id message) 
                                         (mapcar #'from! best-bid-list)))
        ;; cancel-grant is equivalent to a request message to the agent(s) 
        
        ;; record the change in the subtask-frame, updating the stored message
        (setf (type! (message subtask-frame)) :request
          (to! (message subtask-frame)) (mapcar #'from! best-bid-list)
          (protocol (message subtask-frame)) :simple-protocol
          ;; strategy should be take the first answer since all agents are "good"
          (strategy (message subtask-frame)) :take-first-answer)
        
        ;; record that we contracted the task
        (setf (contractors subtask-frame)(mapcar #'from! best-bid-list))
        )
       
       ;; otherwise simply record bid, waiting for timeout
       (t
        ;(format t "~%; agent-scan-bid /saving bids for timeout: ~S" message)
        (setf (saved-bids subtask-frame) 
          (cons message (saved-bids subtask-frame)))))
      )
    :done))

;;;-------------------------------------------------- AGENT-SCAN-BID-WITH-ANSWER

(defun agent-scan-bid-with-answer (agent message)
  "we receive a bid containing an answer, presumably from a recorded result. We ~
      simply transform the message into an answer message and put it into the agenda ~
      for the main process to examine it.
Arguments:
   agent: agent
   message: bid message containing an aswer."
  (let ((task-id (task-id message))
        new-message)
    ;; first check if we have some task being executed
    (multiple-value-bind (task-frame subtask-frame)
                         (agent-get-task-frame-from-subtask-id agent task-id)
      ;; if no task or subtask, ignore bid
      (unless (and task-frame subtask-frame)
        (return-from agent-scan-bid-with-answer nil))
      
      ;; otherwise process message
      (cond 
       ((agent-acceptable-answer? agent message)
        ;; record agent that sent the answer
        (setf (contractors subtask-frame) (list (from! message)))
        ;; copy message
        (setq new-message (message-clone message))
        ;; change it to :answer
        (setf (type! new-message) :answer)
        ;; do as if we had received an answer
        (excl::with-delayed-interrupts
         (setf (saved-answer-list task-frame)
               (append (saved-answer-list task-frame)(list message))))
        
        ;; cancel corresponding subtask, unless the CN strategy is :collect-answers ?
        ;; cancel seems to be taken into account elsewhere, we don't need to do it here
        ;(send-message (message-make-cancel agent 'ALL (task-id message)))
        )
       ;; if message is not acceptable, then issue a warning
       (t 
        (agent-trace 
         agent "~S received a bid-with-answer ~S which is not acceptable" message))))
    ))

;;;---------------------------------------------------- AGENT-SCAN-CALL-FOR-BIDS

(defun agent-scan-call-for-bids (agent message &aux task bid job-parameters fn)
  "processes a call-for-bids in parallel with whatever the agent is doing. Checks ~
      whether the agent can and wants to answer, and if so does the following:
   - check if the agent uses its memory, if so if its has already encountered ~
      the same job previously. If so, we do a table look up and send a special bid ~
      containing the answer
or else
   - estimates job parameters
   - returns a bid to the emitter
Arguments:
   agent: agent
   message: call-for-bids message."
  ;; first check if we have the skill for making a bid. If not, give up
  (unless (agent-has-skill? agent (action message))
    (return-from agent-scan-call-for-bids nil))
  
  ;; then check if we already have sent a bid for this job
  ;; does not apply since messages are sent only once
;;;  (when (and (repeat-count message) 
;;;             (> (repeat-count message) 0)
;;;             (member-if #'(lambda(xx) (eql (task-id xx) (task-id message)))
;;;                        (pending-bids agent)))
;;;    (return-from agent-scan-call-for-bids nil))
  
  ;; then check if we are a learning agent and if we already did this job
  (when (and (member :learning (features agent))
             (setq task (agent-check-memory-for-same-task agent message)))
    ;; if so we prepare a special bid message containing the answer
    (setq bid (message-make-bid agent (answer task) message))
    (setf (type! bid) :bid-with-answer)
    ;; mail it
    (send-message bid)
    (agent-trace agent "returning ~A from memory" (format nil "~S" (answer task)))
    (return-from agent-scan-call-for-bids nil))
  
  ;; otherwise process the message as usual
  ;; look for any preconditions
  (setq fn (agent-get-skill-preconditions agent (action message)))
  (when (and fn 
             ;(not (apply fn (name agent) (environment agent) (args message))))
             (not (apply fn (name agent) message (args message)))) ; JPB0906
    ;; if preconditions don't apply, then give up
    (return-from agent-scan-call-for-bids nil))
  
  ;; estimate job parameters (earliest-start-time, duration, quality, cost, rigidity)
  (setq job-parameters (agent-estimate-task-parameters agent message))
  
  ;; there should be a time limit on bids in case the emitter (contractor) dies
  ;; to prevent the bids to clog the :pending-bids slot of the agent
  ;; the time limit should be something like
  ;;   (*default-cfb-repeat-count* + 1) x *default-cfb-timeout*
  ;; another solution would be to run periodically a cleaning function that
  ;; would remove all pending-bids that are too old to be still active 
  
  ;; make a bid
  (setq bid (message-make-bid agent job-parameters message))
  ;(format t "~%; agent-scan-call-for-bids /bid:~%;  ~S" bid)
  (send-message bid)
  ;; and save it for further reference
  (setf (pending-bids agent) 
        (append (pending-bids agent) (list (message-clone bid))))
  ;; return something even though it is not very useful
  job-parameters 
  )

;(trace agent-scan-call-for-bids)
;(untrace agent-scan-call-for-bids)
;;;----------------------------------------------------------- AGENT-SCAN-CANCEL

(defun agent-scan-cancel (agent message)
  "function that processes a message canceling a task process. No ~
      error message is sent back to the agent that requested the job.
   The name of the agent that started the job to cancel should be the ~
      first element of the contents field of the message. If not, then ~
      we assume that it is the sender
Arguments:
   agent: agent
   message: cancel message to process."
  (let ((emitter (car (contents message))))
    ;; if the key of the agent that initiated the task to cancel is not in the 
    ;; contents slot, then we assume that it is the sender of the cancel message
    (unless emitter
      (setq emitter (from! message)))
    ;; when the task is being executed, then we kill the main process 
    (agent-trace agent "canceling main task ~A and subtasks if any." 
                 (task-id message))
    ;; can't do that: call functions try to display stuff in windows
    ;(#+MCL without-interrupts #+MICROSOFT-32 excl::without-interrupts
    ;       (cancel-task agent (task-id message) emitter))
    (cancel-task agent (task-id message) emitter)
    ;; do an agent display in case something changed
    ;; however, cancel-task may not return
    (agent-display agent)
    :done))

;;;----------------------------------------------------- AGENT-SCAN-CANCEL-GRANT

(defun agent-scan-cancel-grant (agent message)
  "a cancel grant message has been received. If the message is a cancel, then ~
      cancel the specified task. If the agent is in the :but-for list, then the ~
      task is granted.
Arguments:
   agent: agent
   message: cancel-grant-message."
  (let* ((task-id (task-id message))
         (bid (car (member-if #'(lambda (xx) (eql task-id (task-id xx))) 
                              (pending-bids agent))))
         new-message)
    ;(format t "~%; agent-scan-cancel-grant /agent: ~S but-for list:~%; ~S"
    ;  agent (but-for message))
    (cond
     ;; === here the message is a grant message
     ((member (key agent) (but-for message))
      (agent-trace agent "is granted the task: ~A" (task-id message))
      ;; if no pending bid send warning and cancel subtask since the pending bid
      ;; message contained the detail of the task to execute (that are now lost)
      (unless bid
        (warn "~S received a grant message but lost the corresponding pending bid."
              (name agent))
        (return-from agent-scan-cancel-grant nil))
      ;; === OK, we got a bid containing the desctiption of the task to perform
      ;; we remove the bid from the list of pending bids
      (delete bid (pending-bids agent))
      ;; clone message
      (setq new-message (message-clone bid))
      ;(format t "~%; agent-scan-cancel-grant /considering bid:~%;  ~S" new-message)
      ;; switch to and from
      (setf (from! new-message) (to! bid))
      (setf (to! new-message) (from! bid))
      ;; replace :type with request 
      (setf (type! new-message) :request)
      ;; update time
      (setf (date new-message) (get-universal-time))
      ;; and put it into the agenda
      ; MCL: (%agent-add agent new-message :agenda)
      ;(format t "~%; agent-scan-cancel-grant /processing bid:~%;  ~S" new-message)
      (setf (agenda agent) (append (agenda agent) (list new-message)))
      )
     ;; === here the message is a cancel message
     (bid
      ;; we remove the bid from the list of pending bids when it is there
      (delete bid (pending-bids agent))
      (agent-scan-cancel agent message)))
    :done))

;(trace (agent-scan-grant :step t))
;(untrace agent-scan-grant)
;;;---------------------------------------------------------- AGENT-SCAN-DISPLAY

(defun agent-scan-display (agent message)
  "a display message has been received, call the corresponding set of functions."
  ;; try to catch possible errors from user functions
  (let (test errno)
    (multiple-value-setq (test errno)
      (ignore-errors 
       (agent-process-display-message agent message)))
    (format t "~&~S/ agent-scan-display error check. test: ~S errno: ~S."
      (name agent) test errno)
    
    (when (and (null test) errno)
      ;; there was some error
      (cg:beep)
      (warn "~&~S encountered an error while in a display action: ~&   ~S 
                   ~&Please, first debug expr before sending internal message. 
errno: ~S, test: ~S" 
        (name agent) (args message) errno test))
    :done))

;;;------------------------------------------------------------ AGENT-SCAN-GRANT

(defun agent-scan-grant (agent message)
  "a cancel grant message has been received.
Arguments:
   agent: agent
   message: grant-message."
  (let* ((task-id (task-id message))
         (bid (car (member-if #'(lambda (xx) (eql task-id (task-id xx))) 
                              (pending-bids agent))))
         new-message)
    (agent-trace agent "is granted the task: ~A (status: ~S)" 
                 (task-id message) (status agent))
    ;; if no pending bid send warning, we can't process the task because we cannot
    ;; recover the task description contained in the bid message.
    (unless bid
      (warn "~S received a grant message but lost the corresponding pending bid."
            (name agent))
      (return-from agent-scan-grant nil))
    
    ;; === OK, we got bid describing the job to do
    ;; We remove the bid from the list of pending bids
    (setf (pending-bids agent) (delete bid (pending-bids agent)))
    ;; clone bid message
    (setq new-message (message-clone bid))
    ;; switch to and from in the cloned message
    (setf (from! new-message) (to! bid))
    (setf (to! new-message) (from! bid))
    ;; replace :bid type with :request 
    (setf (type! new-message) :request)
    ;; update time
    (setf (date new-message) (get-universal-time))
    (agent-trace agent "inserting message into agenda: ~S" new-message)
    ;; and put it into the agenda
    (setf (agenda agent) (append (agenda agent) (list new-message)))
    :done))

;;;----------------------------------------------------------- AGENT-SCAN-INFORM

(defun agent-scan-inform (agent message &aux fn)
  "we check if we have the skill and if preconditions are met. If so, we add ~
   message to agenda."
  ;; first check if we have the skill
  (unless (agent-has-skill? agent (action message))
    (return-from agent-scan-inform nil))
  ;; if we do, process the message as usual
  ;; look for any preconditions
  ;; ***** maybe preconditions should be checked by the main process (agenda)
  (setq fn (agent-get-skill-preconditions agent (action message)))
  (when fn
    ;; if preconditions don't apply, then give up
    (unless (apply fn agent message (args message)) ; JPB0906
      (return-from agent-scan-inform nil)))
  ;; add message to agenda and quit
  (setf (agenda agent) (append (agenda agent) (list message)))
  )

;;;------------------------------------------------------- AGENT-SCAN-SYS-INFORM

(defun agent-scan-sys-inform (agent message &aux fn)
  "system messages that are not part of the application (e.g. used for displaying)
arguments:
   agent: agent
   message: received message
return:
   unimportant."
  (declare (special *current-message*))
  ;; save message so that we can use it in the skill code
  (setq *current-message* message)
  ;; first check if we have the skill
  (unless (agent-has-skill? agent (action message))
    (return-from agent-scan-sys-inform nil))
  ;; if we do, process the message as usual
  ;; look for any preconditions
  (setq fn (agent-get-skill-preconditions agent (action message)))
  (when fn
    ;; if preconditions don't apply, then give up
    ;(unless (apply fn agent (environment agent) (args message))
    (unless (apply fn agent message (args message)) ; jpb0906
      (return-from agent-scan-sys-inform nil)))
  ;; otherwise get skill function (only static)
  (setq fn (agent-get-static-skill agent (action message)))
  (when fn
    ;; apply skills immediately
    ;(apply fn agent (environment agent) (args message))
    (apply fn agent message (args message)) ; jpb0906
    )
  ;; reset current message
  (setq *current-message* nil)
  ;; upgrade representation and quit
  (agent-display agent)
  :done)

;;;--------------------------------------------------------- AGENT-SCAN-INtERNAL

(defun agent-scan-internal (agent message)
  "function that analyzes internal message. If :action is :execute ~
   we execute the content of the argument part of the message. Otherwise, we ~
   call the agent-insert-into-agenda function.
Arguments:
   agent: agent
   message: message to be processed.
Return:
   :done"
  (let (test errno)
    ;; first check if the action part of the message asks for execution
    ;; in particular, this allows to create display windows within the scan process
    ;; of the agent, with the result that they do not disappear right away
    ;(skill-trace agent message)
    (case (action message) 
      (:execute
       ;; execute function (for side effect since nothing is returned) catching
       ;; possible errors
       (multiple-value-setq (test errno)
         (ignore-errors 
          (eval (args message))))
       (format t "~&~S/ agent-scan-internal/ error check. test: ~S errno: ~S."
         (name agent) test errno)
       
       (when (and (null test) errno)
         ;; there was some error
         (cg:beep)
         (warn "~&~S *** we encountered an error while executing: ~&   ~S 
                   ~&Please, first debug expr before sending internal message. 
errno: ~S, test: ~S" 
           (name agent) (args message) errno test))
       )
      ;; otherwise go process the message as usual
      (otherwise
       (agent-insert-into-agenda agent message)))
    :done))

;;;----------------------------------------------------- AGENT-MAKE-PROCESS-NAME

;;; should be kept in this file only if specific function, otherwise should be 
;;; transferred to the agents file.

(defun agent-make-process-name (agent mode-string &key fixed-name)
  "produces a string for naming processes. E.g., FACTORIAL-call-for-bids-22
Arguments:
   agent: agent
   mode-string: string identifying type of job (e.g., \"inform\")
   fixed-name: (key) if t, there is no version to the name."
  (if fixed-name
    ;; don't put version on the name (for main processes)
    (concatenate 'string (symbol-name (name agent)) "-" mode-string)
    ;; put one for dispensable processes, e.g., for processing call-for-bids
    (symbol-name
     (gentemp 
      (concatenate 'string (symbol-name (name agent)) "-" mode-string "-")))))

(format t "~&;*** OMAS v~A - comm loaded ***" *omas-version-number*)

;;; :EOF