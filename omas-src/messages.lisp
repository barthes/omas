﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;                O M A S - M E S S A G E S  (file omas-messages.lisp)
;;; created 2000
;;;===============================================================================
;;; This file contains the definitions and methods associated with the class 
;;; MESSAGE.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|  
History
-------
2020
 0202 restructuring OMAS as an asdf system
|# 

(in-package :omas)

;;;=================================== Macros ====================================

;;;-------------------------------------------------------------------- DEFMESSAGE

(defMacro defmessage (name &rest arg-list)
  "can be used to define messages statically e.g., in test files"
  `(progn 
     (defParameter ,(if (keywordp name)(intern (symbol-name name)) name) 
       (let ((msg (apply #'make-instance 'MESSAGE :name ',name ',arg-list)))
         ;(print (list '=======> *user-messages*))
         (setf (user-messages *omas*) (reverse (user-messages *omas*)))
         (remove ',name (user-messages *omas*) :key #'car)
         (push (cons ',name msg) (user-messages *omas*))
         (setf (user-messages *omas*) (reverse (user-messages *omas*)))
         msg))
     ',name))

#|
? (defmessage :MSG01 :type :request :to :test :action :hello :args ("Hello!"))
MSG01
? omas::*user-messages*
(setq *user-messages* nil)
? (send-message MSG01)
:MESSAGE-SENT

(defmessage :test-cond-A :type :request :action :hello
  :to (:_cond ("recommendation" ("ID" :is "VG-BASIC"))))
:TEST-COND-A

(omas::user-messages *omas*)
((:TEST-COND-A
  . #<MESSAGE 18:37:19 NIL (:_COND
                            ("recommendation"
                             ("ID" :IS
                              "VG-BASIC"))) :REQUEST :HELLO NIL NIL Tid:NIL 
  :BASIC-PROTOCOL no-TO TL:3600 id:NIL>))
|#
;;;------------------------------------------------------------------ MAKE-MESSAGE

(defun make-message (&rest args)
  "calls make-instance to create a new message. The message is not added to the~
   list of user messages to be posted in the control panel."
  (apply #'make-instance 'message args))

;;;------------------------------------------------------------------ MESSAGE-MAKE

(defMacro message-make (&rest args)
  `(make-instance 'message ,@args)
  )

;;;============================== classes and methods ============================
;;; Message structure

#| a message has the following structure:
   :name message-name      ; name identifying the message, e.g."MM-2"
   :key                    ; the message name should be a key
   :type type              ; type de message (:request, :answer, :abort, :error)
   :from agent-id          ; if none assumed to be the user
   :to agent-id            ; if none assumed to be the user
   :date clock-time        ; time at which the message is produced
   :action function        ; wame of agent function to axecute
   :task-id task-id        ; task name e.g., T-2
   :args list-of-arguments ; arguments for executing the task
   :contents contents      ; contains an answer (e.g.)
   :error-contents contents ; some explanation for error
   :reply-to agent-id      ; to whom the result is sent (default the sender)
   :content-language       ; language used in the args area
   :contents               ; some contents
   :timeout delay           ; timout delay
   :time-limit             ; time-limit for the task specified by the message
   :repeat-number number   ; 0 first message, otherwise repeated message
   :protocol               ; :simple-protocol, :contract-net
   :strategy               ; for broadcast, :take-first-answer, :collect-answers
   :task-timeout number    ; timeout used by the CNet call-for-bids
   :but-for agent-name     ; used to grant contracts via :cancel messages
   :previous-status        ; used by agent-idle-process-call-for-bids (?)
   :ack                    ; requires an acknowledgement from the receiver
   :thru                   ; contains the list of sites the message was sent to 
   :id                     ; message identifier <site name>-<sequence number>
                                               

;;; deprecated
(defClass BASIC-MESSAGE ()
  ((name :accessor name :initform nil :initarg :name)
   (type :accessor type! :initform nil :initarg :type)
   (date :accessor date :initform (get-universal-time) :initarg :date)
   (from :initarg :from :accessor from! :initform nil)
   (to :accessor to! :initarg :to :initform nil)
   (action :accessor action :initarg :action :initform nil)
   (args :accessor args :initarg :args :initform nil)
   (contents :accessor contents :initarg :contents :initform nil)
   (content-language :accessor content-language :initform nil)
   (error-contents :accessor error-contents :initarg :error-contents :initform nil)
   (timeout :accessor timeout :initarg :timeout :initform nil)
   (time-limit :accessor time-limit :initarg :time-limit 
               :initform (default-time-limit *omas*))
   (ack :accessor ack :initform nil :initarg :ack)
   (protocol :accessor protocol :initarg :protocol :initform :basic-protocol)
   (strategy :accessor strategy :initform :take-first-answer :initarg :strategy)
   )
  (:documentation "BASIC-MESSAGE is a message from which all other messages in ~
                               the system are constructed. It contains only the minimally ~
                               required slots."))
|#

;;; the order of the slot is the order in which we want the slots to appear in
;;; the message window
(defClass message ()
  (
   ;; 1 message name, e.g. MM-3, but can be anything
   (name :accessor name :initform nil :initarg :name)
   ;; 2 performative
   (type :accessor type! :initform nil :initarg :type)
   ;; 3 date is set by the system when a message is received. It uses the receiver
   ;; clock
   (date :accessor date :initform (get-universal-time) :initarg :date)
   ;; 4 sender
   (from :initarg :from :accessor from! :initform nil)
   ;; 5 receiver
   (to :accessor to! :initarg :to :initform nil)
   ;; 6 skill
   (action :accessor action :initarg :action :initform nil)
   ;; 7 skill arguments
   (args :accessor args :initarg :args :initform nil)
   ;; 8 used when returning an answer
   (contents :accessor contents :initarg :contents :initform nil)
   ;; 9 normally the formalism used by the exchange protocol
   (content-language :accessor content-language :initarg :content-language 
                     :initform nil)
   ;; 10 a string to explain the error
   (error-contents :accessor error-contents :initarg :error-contents :initform nil)
   ;; 11 timeout for getting an answer
   (timeout :accessor timeout :initarg :timeout :initform nil)
   ;; 12 time allowed to the receiver to do the job
   (time-limit :accessor time-limit :initarg :time-limit 
               :initform (default-time-limit *omas*))
   ;; 13 request for an acknowledgment of the message
   (ack :accessor ack :initform nil :initarg :ack)
   ;; 14 type of protocol (:basic-protocol, :contract-net)
   (protocol :accessor protocol :initarg :protocol :initform :basic-protocol)
   ;; 15 used by broadcasts and contract net: :take-first-answer (default),
   ;; take-first-nnanswers, or collect-answers
   (strategy :accessor strategy :initform :take-first-answer :initarg :strategy)
   ;; 16 used in cancel-grant messages to specify the agents that won the contract
   (but-for :accessor but-for :initarg :but-for :initform nil)
   ;; 17 id of the task  that sent the message (needed to route the answer)
   (task-id :accessor task-id :initarg :task-id :initform nil)
   ;; 18 continuation (unused)
   (reply-to :accessor reply-to :initarg :reply-to :initform nil)
   ;; 19 number of times the message is repeated (set by OMAS)
   (repeat-count :accessor repeat-count :initarg :repeat-count :initform nil)
   ;; 20 task-timeout is used by contract-net call-for bids 
   (task-timeout :accessor task-timeout :initarg :task-timeout :initform nil)
   ;; 21 tagged to a message for undeclared sites so that postman can send messages 
   ;; back (however sender-ip can be retrieved from the receiving stream/ to check)
   (sender-ip :accessor sender-ip :initform nil)
   ;; 22 id of the site that sent the message, e.g. :UTC
   ;; if two postmen are on the same site, HTTP won't use proxies
   (sender-site :accessor sender-site :initform nil)
   ;; 23 list of sites to which the message has been sent
   (thru :accessor thru :initarg :thru :initform nil)
   ;; 24 message identifier trying to uniquely define message, e.g. :UTC-377
   ;; this assumes that site references are unique across a distributed coterie
   (id :accessor id :initform nil :initarg :id)
   ))

;;;===============================================================================
;;;
;;;     dictionary for coding message properties when sending on the web
;;;
;;;===============================================================================

#+MCL
(setf (message-property-dictionary *omas*) 
      (let ((*cc* 0))
        (mapcar #'(lambda (xx) (cons xx (incf *cc*))) 
                ;(mapcar #'car (class-instance-slots (find-class 'MESSAGE))))))
                (mapcar #'(lambda(xx) (slot-value xx 'ccl::name))  ; JPB0805
                        (class-instance-slots (find-class 'MESSAGE))))))

;;; ACL uses the mop:compute-slots to recover the class slots
;;; watch it name function for slot definitions is used in cg-user...
#+MICROSOFT-32
(setf (message-property-dictionary *omas*) 
      (let ((*cc* 0))
        (mapcar #'(lambda (xx) (cons xx (incf *cc*))) 
                (mapcar #'cg-user::name (mop:compute-slots (find-class 'message))))))

;; print result
;;;(format t "~%; omas-messages / message-property-dictionary: ~%  ~S"
;;;        (message-property-dictionary *omas*))


#|
((NAME . 1) (TYPE . 2) (DATE . 3) (FROM . 4) (TO . 5) (ACTION . 6) (ARGS . 7)
 (CONTENTS . 8) (CONTENT-LANGUAGE . 9) (ERROR-CONTENTS . 10) (TIMEOUT . 11)
 (TIME-LIMIT . 12) (ACK . 13) (PROTOCOL . 14) (STRATEGY . 15) (BUT-FOR . 16)
 (TASK-ID . 17) (REPLY-TO . 18) (REPEAT-COUNT . 19) (TASK-TIMEOUT . 20)
 (SENDER-IP . 21) (SENDER-SITE . 22) (THRU . 23) (ID . 24))
|#
;;;----------------------------------------------------------------- COPY-INSTANCE
;;;--- copy-instance does not seem to exist in ACL

(defMacro setprop (prop obj1 obj2)
  `(let ((prop-accessor (eval ,prop)))
     (setf (prop-accessor ,obj1)(prop-accessor ,obj2))))

; (setprop 'name aa bb)

#+MICROSOFT-32
(defUn copy-instance (mm)
  (let((ma (make-instance 'message))
       (prop-list (mapcar #'cg-user::name 
                          (mop:compute-slots (find-class 'message)))))
    (dolist (prop prop-list)
      ;; three names are not allowed by ACL (birrrkk)
      (case prop
        (type (setf (type! ma) (type! mm)))
        (from (setf (from! ma) (from! mm)))
        (to (setf (to! ma) (to! mm)))
        (otherwise (setf (slot-value ma prop)(slot-value mm prop)))))
    ma))

;;;----------------------------------------------------------------- MESSAGE-CLONE

(defMethod message-clone ((mm message))
  "clone a given instance of message, using the copy-instance primitive. Must not ~
   forget to initialize the name in the copied message."
  (copy-instance mm))

#|
;; there should be a better way to do that. E.g., by recovering the list of instance
;; slots and doing a loop comparing the corresponding values.
(defmethod message-equal ((ma MESSAGE)(mm MESSAGE))
  (let ((slot-list (mapcar #'car (class-instance-slots (find-class 'message)))))
    (eval (cons 'and (mapcar #'(lambda (xx) (equal (funcall xx ma) (funcall xx mm)))
                       ;; must remove name since messages won't have the same
                       (remove 'name slot-list))))))
|#
;;;------------------------------------------------------------------ PRINT-OBJECT

(defMethod print-object ((mm message) stream)
  ;(format stream "#<MESSAGE ~A ~S ~S ~S ~S ~A ~A ~A ~S ~A ~A~A~A~A~A id:>"
  (format stream "#<MESSAGE ~A ~S ~S ~S ~S ~A ~A ~A ~S ~A ~A~A~A~A~A id:~S>" ; jpb0907
          (time-string (date mm)) 
          (from! mm)
          (to! mm)
          (type! mm)
          (action mm)
          (format nil "~S" (args mm)) ; to avoid broken printouts
          (format nil "~S" (contents mm))
          (cond
           ((not (numberp (task-id mm))) (format nil "Tid:~S" (task-id mm)))
           ((minusp (task-id mm)) (format nil "T:~S" (task-id mm)))
           (t(format nil "ST:~S" (task-id mm))))
          (protocol mm)
          (cond ((timeout mm) (format nil "TO:~S" (timeout mm)))("no-TO"))
          (cond ((time-limit mm)(format nil "TL:~S" (time-limit mm)))("no-TL"))
          (if (repeat-count mm)
            (format nil " RC:~S" (repeat-count mm))
            "")
          (if (eql :error (contents mm))
            (error-contents mm)
            "")
          (if (but-for mm) (format nil " BF: ~S" (but-for mm)) "")
          (if (ack mm) " ACK" "")
          (id mm)
          ))

;;;======================= Functions to format messages ==========================

;;;---------------------------------------------------------------- MESSAGE-FORMAT

(defUn message-format (message &optional (length #+MCL 120 #-MCL 1000))
  "builds a short string to display messages into agent windows
   <date type from/to task-id action args contents"
  (when message
    (let ((date (time-string (date message)))
          (type (type! message))
          (from (from! message))
          (to (to! message))
          (task-id (task-id message))
          (action (action message))
          (args (args message))
          (contents (contents message))
          (but-for (but-for message))
          ;(protocol (protocol message))
          (time-limit (time-limit message))
          (timeout (timeout message))
          (repeat-count (repeat-count message))
          (protocol (protocol message))
          (strategy (strategy message))
          (id (id message))
          )
      (setq protocol (case protocol (:basic-protocol :BP)(:contract-net :CN)))
      (setq strategy (case strategy (:take-first-answer :FA) (:collect-answers :CA)))
      (message-limit-text-length 
       (case type
         (:alert
          (format nil "~3D AL ~A R: ~S ~S->~S ~S:~A id:~S"
                  date task-id contents from to action (format nil "~S" args) id))
         (:request
          (format nil "~3D RQ ~A ~S->~S ~S:~{~S~^,~}~A~A~A ~S/~S id:~S"
                  date task-id (cond (from) ('user)) to action args
                  (if (eql time-limit (default-time-limit *omas*))
                    ""
                    (format nil " TL:~S" time-limit))
                  (if timeout (format nil " TO:~S" timeout) "")
                  (if repeat-count (format nil " RPT:~S" repeat-count) "")
                  protocol
                  strategy
                  id))
         (:acknowledge
          (format nil "~3D ACK ~A ~S->~S ~S:~{~S~^,~} TL:~S id:~S"
                  date task-id (or from 'user) to action args
                  (if time-limit time-limit)
                  id))
         (:grant
          (format nil "~3D GR ~A ~S->~S ~S:~{~S~^,~} id:~S"
                  date task-id from to action args id))
         (:answer
          (format nil "~3D ANS ~A R:~A ~S->~S ~S:~{~S~^,~} ~S/~S id:~S"
                  date task-id (format nil "~S" contents) from to action args
                  protocol strategy id))
         (:cancel
          (format nil "~3D CANCEL ~A ~S->~S id:~S"
                  date task-id from to id))
         (:cancel-grant
          (format nil "~3D CCLGRT ~A ~S->~S but-for:~{~S~^,~} ~S/~S id:~S"
                  date task-id from to but-for protocol strategy id))
         (:call-for-bids
          (format nil "~3D CFB ~A ~S->~S ~S:~{~S~^,~} TO:~S ~S/~S id:~S"
                  date task-id from to action args timeout protocol strategy id))
         (:bid
          (format nil "~3D BID ~A R: ~S ~S-> ~S:~{~S~^,~} id:~S"
                  date task-id contents from action args id))
         (:bid-with-answer
          (format nil "~3D BWA ~A R: ~S ~S-> ~S:~{~S~^,~} id:~S"
                  date task-id contents from action args id))
         (:inform
          (format nil "~3D INF ~A R: ~S ~S->~S ~S:~A id:~S"
                  date task-id contents from to action (format nil "~S" args) id))
         (:sys-inform
          (format nil "~3D SYSINF ~A R: ~S ~S->~S ~S:~A id:~S"
                  date task-id contents from to action (format nil "~S" args)id))
         (:abort
          (format nil "~3D ABRT ~A R: ~S ~S->~S ~S:~{~S~^,~} id:~S"
                  date task-id contents from to action args id))
         (otherwise
          (format nil "~3D ? ~A R: ~S ~S->~S ~S:~{~S~^,~} ~S/~S id:~S"
                  date task-id contents from to action args protocol strategy id)))
       length))))

#|
(defun message-get (message accessor-keyword)
  "get the value through the proper accessor for a message. For the sake of ~
      compatibility with version 2, uses slot-value bypassing accessors
      E.g. (message-get mm-0 :task-id)"
  (slot-value message (intern (symbol-name accessor-keyword) :omas)))
|#
;;;----------------------------------------------------- MESSAGE-LIMIT-TEXT-LENGTH

(defUn message-limit-text-length (text limit)
  "takes a text and limits its length, replacing CR, LF and NL by spaces. Remove multiple spaces.
Arguments:
  text: string to be checked
  limit: max length."
  (unless (stringp text)
    (warn "text argument : ~S should be a string" text)
    (return-from message-limit-text-length "<unknown text>"))
  (let ((result
         (string-trim '(#\Space #\Return #\Newline #\Linefeed)
                      (if (> (length text) limit)
                        (concatenate 'string (subseq text 0 (- limit 3)) "...")
                        text))))
    ;; transform result into a list of chars
    (setq result (map 'list #'(lambda(x) x) result))
    ;; remove multiple spaces
    (setq result
          (mapcon 
           #'(lambda(x) 
               (cond 
                ;; if we have CR LF NL we replace by a space
                ((member (car x) '(#\return #\Newline #\Linefeed) :test #'char-equal)
                 (list #\space))
                ;; if we have 2 successive blanks we remove one
                ((and (cdr x)(eq (car x) '#\space)
                      (eq (cadr x) '#\space)) 
                 nil)
                (t (list(car x)))))
           result))
    (format nil "~{~A~}" result)
    ))

#|
(message-limit-text-length "albert
est
parti  très     rapidement
au
pays.   " 
60)
"albert est parti très rapidement au pays."
(message-limit-text-length "albert
est
parti
au
pays." 
15)
"albert est p..."
|#
;;;----------------------------------------------------------------- MESSAGE-PRINT

(defun message-print (message &key (header "") (stream t))
  " prints a message in a detailed way"
  (unless (typep message 'message) (error "~S should be a message" message))
  (format stream "~%=====  ~A~%~{  ~A~%~}====="
    header
    (remove nil
            (mapcar #'(lambda (xx) 
                        ;; get value associated with property
                        (let ((value (slot-value message (cg:name xx))))
                          (if value (format nil "~A: ~S" (cg:name xx) value))))
              (mop::class-direct-slots (find-class 'message))))))
#|
mm
#<MESSAGE 10:55:20 :UTC-TATIN :JEAN-PAUL-WORKBENCH :REQUEST :CREATE (((:DATA ("category" "postit")
   ("content" "Nouveau cahier des charges")))) NIL Tid:NIL :BASIC-PROTOCOL no-TO TL:3600 id:NIL>
(message-print mm :header "message de création")
=====  message de création
"NAME: :CR-P"
"TYPE: :REQUEST"
"DATE: 3535437320"
"FROM: :UTC-TATIN"
"TO: :JEAN-PAUL-WORKBENCH"
"ACTION: :CREATE"
"ARGS: (((:DATA (\"category\" \"postit\")
         (\"content\" \"Nouveau cahier des charges\"))))"
"TIME-LIMIT: 3600"
"PROTOCOL: :BASIC-PROTOCOL"
"STRATEGY: :TAKE-FIRST-ANSWER"
=====
|#
;;;===============================================================================
;;;                    Functions to build specific messages
;;;===============================================================================
;;; the following functions are not very useful. They require that one knows their
;;; arguments in the proper order. It is easier to use make-message

;;;------------------------------------------------------------ MESSAGE-MAKE-ABORT

(defUn message-make-abort (agent to-agent sender subtask-id)
  "building an abort message sent to an agent. The message must ~
   contain the task-number to be aborted and the original sender ~
   of the task that will be passed in the arg-list of the message.
Arguments:
   agent: agent structure or agent-id (keyword)
   to-agent: agent structure or agent-id (keyword)
   sender: original sender of the task to be aborted
   subtask-id: number identifying the subtask
Return:
  a new message"
  (make-instance 'message
    :type :abort
    :from (keywordize agent)
    :to (keywordize to-agent)
    :args (keywordize (list sender))
    :date (get-universal-time)
    :task-id subtask-id
    ))

;;;-------------------------------------------------- MESSAGE-MAKE-ACKNOWLEDGEMENT

(defUn message-make-acknowledgement (agent message)
  "building an acknowlegement message to send back to the caller."
  (make-instance 'message
    :type :acknowledge
    :from (keywordize agent) 
    :to (from! message)
    :date (get-universal-time)
    :task-id (task-id message)
    :action (action message)
    :args (args message)
    ))

;;;------------------------------------------------------------ MESSAGE-MAKE-ALERT
;;; an ALERT message does not create a specific thread for processing the message
;;; which is the difference with an INFORM message
;;; This means that if the skill function processing the data crashes, then it 
;;; crashes the scan process of the agent

(defUn message-make-alert (agent to-agent action args)
  "building an inform message to send to some agent(s).
Arguments:
   agent: agent
   to-agent: list of agents to which the job will be granted or ALL
   action: skill of the receiving agent
   args: arguments for the specified action."
  (make-instance 'message
    :type :alert
    :from (keywordize agent) 
    :to (keywordize to-agent)
    :date (get-universal-time)
    :action action
    :args args
    ))

;;;----------------------------------------------------------- MESSAGE-MAKE-ANSWER

(defUn message-make-answer (agent to-agent task-id skill args result 
                                      &key duration quality)
  "building an answer message for a specific task.
Arguments:
   agent: agent
   to-agent: agent that will get the result
   task-id: task for which answer is provided
   skill: skill that was used
   args: args that were used
   result: result provided
   duration: (&key) time the task took to process
   quality: (&key) quality of the result."
  (declare (ignore duration quality))
  (make-instance 'message
    :type :answer
    :from (keywordize agent) 
    :to (keywordize to-agent)
    :date (get-universal-time)
    :action skill
    :args args
    :contents result
    :task-id task-id
    ))

;;;-------------------------------------------------------------- MESSAGE-MAKE-BID

(defUn message-make-bid (agent job-parameters cfb-message)
  "building a bid message from the corresponding call-for-bids and the parameters.
Arguments:
   agent: agent
   job-parameters: time quality accuracy
   CFB-message: message from which info is taken 
     to <- from slot
     action, task-id, args from corresponding slots."
  (make-instance 'message
    :type :bid
    :contents job-parameters
    :from (keywordize agent) 
    :to (from! CFB-message)
    :date (get-universal-time)
    :action (action CFB-message)
    :task-id (task-id CFB-message)
    :args (args CFB-message)
    ))

;;;----------------------------------------------------------- MESSAGE-MAKE-CANCEL

(defUn message-make-cancel (agent to-agent subtask)
  "building a cancel message to send to a subcontractor. We received an answer ~
   and cancel additional requests.
Arguments:
   agent: agent
   to-agent: to-agent
   subtask: subtask-id
   but-for: (opt) list of names of agents  to which subtask will be granted."
  (make-instance 'message
    :type :cancel
    :from (keywordize agent) 
    :to (keywordize to-agent)
    :date (get-universal-time)
    :task-id subtask
    ))

;;;----------------------------------------------------- MESSAGE-MAKE-CANCEL-GRANT

(defUn message-make-cancel-grant (agent to-agent subtask &optional but-for)
  "building a cancel-grant message to send to a subcontractor. We received an answer ~
   and cancel additional requests.
Arguments:
   agent: agent
   to-agent: to-agent
   subtask: subtask-id
   but-for: (opt) list of names of agents  to which subtask will be granted."
  (make-instance 'message
    :type :cancel-grant
    :from (keywordize agent) 
    :to (keywordize to-agent)
    :but-for (keywordize but-for)
    :date (get-universal-time)
    :task-id subtask
    ))

;;;------------------------------------------------------------ MESSAGE-MAKE-ERROR

(defUn message-make-error (agent received-message &optional (contents :error))
  "building an error message to send back to the caller. We could not process ~
   the job. The message will be passed to the user with an :error content.
Arguments:
   agent: agent
   received-message: message for which error is issued."
  ;; we should also give a reason for the error
  (make-instance 'message
    :type :answer
    :contents contents
    :from (keywordize agent) 
    :to (from! received-message)
    :date (get-universal-time)
    :action (action received-message)
    :task-id (task-id received-message)
    :args (args received-message)
    ))

;;;------------------------------------------------------------ MESSAGE-MAKE-GRANT

(defUn message-make-grant (agent to-agent subtask)
  "building a grant message to send to a list of subcontractors.
Arguments:
   agent: agent
   to-agent: list of agent to which the job will be granted
   subtask: subtask-id."
  (make-instance 'message
    :type :grant
    :from (keywordize agent) 
    :to (keywordize to-agent)
    :date (get-universal-time)
    :task-id subtask
    ))

;;;----------------------------------------------------------- MESSAGE-MAKE-INFORM

(defUn message-make-inform (agent to-agent action args)
  "building an inform message to send to some agent(s).
Arguments:
   agent: agent
   to-agent: list of agent to which the job will be granted or ALL
   action: skill of the receiving agent
   args: arguments for the specified action."
  (make-instance 'message
    :type :inform
    :from (keywordize agent) 
    :to (keywordize to-agent)
    :date (get-universal-time)
    :action action
    :args args
    ))
;(message-make-inform :albert :albert :send "(<message>)")

;;;------------------------------------------------------------ %MESSAGE-MAKE-NAME

(defUn %message-make-name ()
  "builds a new keyword to qualify a  message.
Arguments:
   none
Return:
    message key, e.g. :MM-23"
  (intern (symbol-name (gentemp "MM-")) :keyword))

;;;------------------------------------------------------------ MESSAGE-MAKE-REPLY

(defUn message-make-reply (agent received-message result &optional reason)
  "building an answer message after the task has been completed.
Arguments:
   agent: agent
   received-message: message granting the task
   result: result of applying the skill (put into the contents slot)
   reason (opt): in case of failure gives the reason for it (a string)."
  (let (mm)
    (setq mm (make-instance 'message
               :type :answer
               :from (keywordize agent) 
               :to (or (reply-to received-message) (from! received-message))
               :date (get-universal-time)
               :action (action received-message)
               :task-id (task-id received-message)
               :contents result
               :error-contents reason
               :args (args received-message)))
    ;;kludge for version 10.0.0 (content-language not an initarg) JPB1303
    (setf (content-language mm)(content-language received-message))
    ;; return message
    mm))

#|
(defun message-make-request)
|#
;;;===============================================================================
;;;
;;;            Functions to send and receive messages from the net 
;;;
;;;===============================================================================

;;;-------------------------------------------------- MESSAGE-NET-STRING-TO-OBJECT

(defUn message-net-string-to-object (message-string)
  "transform an alternate list to a structured message object.
Arguments:
   string: string representing the message
Value:
   nil if message cannot be read
   message-object otherwise."
  (let* ((message (make-instance 'message))
         (message-alist (read-from-string message-string nil nil)))
    ;; ignore all messages that can't be parsed (return nil)
    (unless message-alist (return-from message-net-string-to-object nil))
    ;; rebuild message faithfully using slot-value
    (loop 
      (unless message-alist (return message))
      (setf (slot-value  message
                         (car (rassoc (pop message-alist) 
                                      (message-property-dictionary *omas*))))
            (pop message-alist)))))

#|
(message-net-string-to-object 
   "(2 :REQUEST 3 3522862819 5 :ALL 6 :HELLO 11 2 12 3600 14 :BASIC-PROTOCOL 15 :TAKE-FIRST-ANSWER)")
#<MESSAGE 23:0:19 NIL :ALL :REQUEST :HELLO NIL NIL Tid:NIL :BASIC-PROTOCOL TO:2 TL:3600 id:NIL>
|#
;;;-------------------------------------------------- MESSAGE-OBJECT-TO-NET-STRING

(defUn message-object-to-net-string (message)
  "takes an object and builds a compact string resulting from an alternate list ~
   where property values are replaced by numbers and null properties are omitted."
  (let* (result sv)
    ;; now we translate the message into a list
    (dolist (pair (message-property-dictionary *omas*))
      (setq sv (slot-value message (car pair)))
      (when sv 
        (push (cdr pair) result)
        (push sv result)))
    ;; ...then into a string
    (format nil "~S" (reverse result))))

;;;---------------------------------------------------------- USER-GET-LAST-ANSWER

(defUn user-get-last-answer ()
  "get the last answer that was posted into the control panel.
Arguments:
   none
Return:
   :no-message or the contents of the last message received."
  (let ((win (omas-window *omas*)))
    (if (slot-boundp win 'input-message)
      (let ((answer-message (input-message win)))
        (if (typep answer-message 'message)
          (contents answer-message)
          (input-message win)))
      :no-message)))

#|
? (user-get-last-answer)
"Greetings..."
|#
;;;-------------------------------------------------------------- USER-SEND-INFORM

(defUn user-send-inform (&key to action args)
  "the function sends a, inform to an agent on behalf of the user. No answer ~
   expected.
Arguments:
   to (key): destination
   action (key): requested skill
   args (key): arguments to be processed by the skill
Return:
   :done"
  (send-message
   (make-instance 'message :type :inform :to to :action action :args args))
  :done)
#|
? (user-send-inform :to :test :action :hello :args '("Hello!"))
|#
;;;------------------------------------------------------------- USER-SEND-REQUEST

(defUn user-send-request (&key to action args)
  "the function sends a request to an agent on behalf of the user, then waits for ~
   one second, and tries to recover the answer from the control panel to post it.
Arguments:
   to (key): destination
   action (key): requested skill
   args (key): arguments to be processed by the skill
Return:
   content of the answer message or nil if nothing came back."
  (let (answer-message)
    (send-message
     (make-instance 'message :type :request :to to :action action :args args))
    ;; wait one second
    (sleep 1)
    ;; get the answer back
    (setq answer-message (input-message (omas-window *omas*)))
    (if (typep answer-message 'message)
      (contents answer-message)
      (input-message (omas-window *omas*)))
    ))

#|
? (user-send-request :to :test :action :hello :args '("Hello!"))
"Hello from :PUBLISHER..."
|#
;;;---

(format t "~&;*** OMAS v~A - messages loaded ***" *omas-version-number*) 

;;; :EOF