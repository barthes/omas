﻿;;;-*- mode: lisp; package: "web2pa" -*-
;;;===============================================================================
;;;14/12/02
;;;                           O M A S - AGENT WEB2PA
;;; 
;;;===============================================================================
#| 
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
History
-------
2020
 0202 restructuring OMAS as an asdf system
|#

;;; this agent is a go between between the web server and various PAs. it receives 
;;; messages, select the right PA and forwards the message. It then transfers the
;;; answer to the server thread.
;;;
;;; WEB2PA has the following skills:
;;;   - GET-LANGUAGE to get the PA language
;;;   - LOCATE-PA to locate the PA and obtain its IP
;;;   - LOG-ERROR logs error
;;;   - PROCESS-WEB-REQUEST more general than the following
;;;   - PROCESS-WEB-TEXT send text to the PA to be processed
;;;   - SHUT-DOWN-SERVER unused
;;;   - START-SERVER starts Allegroserve and post pages
;;;
;;; WEB2PA handles the following pages (Allegroserve entities):
;;;   - LOGIN the application login page
;;;   - PASSWORD FORM to change password
;;;   - WEB DIALOG FORM for hosting dialogs
;;;
;;; WEB2PA indirectly requires the following skills:
;;;   - :authorized? (M)
;;;   - :check-password (M)
;;;   - :get-language to get the language of a target agent
;;;   - :get-member-description (M)
;;;   - :locate-pa (W)
;;;   - :process-web-text (P)
;;;   - :set-password (M)
;;;   - :start-server (W)


;;; WEB2PA is a service agent. Thus, before sending messages, it should create a
;;; task, which sets up the picture for receiving the answer.

#|
2014
 1202 creation
|#

(defpackage :web2PA (:use :cl :moss :omas :excl :net.aserve :net.html.generator)
         (:nicknames :wp))
(in-package :web2PA)


;;;===============================================================================
;;; 
;;;                           creating web agent
;;;
;;;===============================================================================

(omas::defagent :web2PA)

;;;===============================================================================
;;; 
;;;                     globals, service macros and functions 
;;;
;;;===============================================================================

;;;==============================================================================
;;;                                GLOBALS
;;;==============================================================================
  
(defparameter *web-server-port* 80 "server port for serving hdsri forms.")

;;; the following parameters are various timeouts for the editor

(defparameter *web-hello-timeout* 10 "10 seconds to see if agent exists")
(defparameter *web-post-timeout* 10 "10 seconds to post an object")

;;; mulitlingual tags
;;;   (mln::extract omas::*web-no-pa*)
(defmacro defmln (name expr)
  "transforms the old string format into the new list format"
  `(defparameter ,name (mln::make-mln ,expr)))


(defmln *ir-add-news*
    '(:fr "Ajouter/éditer une information" :en "Add/edit a news item"))
(defmln *ir-add-news-go*
    '(:fr "Ajouter une information" :en "Add a news item"))
(defmln *ir-assistant*
    '(:fr "Nom de votre assistant si différent du vôtre :" 
          :en "Your personal assistant name if different from yours:"))
(defmln *ir-assistant-create*
    '(:fr "Créer un nouvel assistant ? (OK ou rien)" 
          :en "Create a new assistant? (OK or nothing)"))
(defmln *ir-beta-test*
    '(:fr "Pendant la phase de bêta test :" :en "During bêta tests:"))
(defmln *ir-category* 
    '(:fr "Catégorie" :en "Category"))
(defmln *ir-comments*
    '(:fr "Commentaires" :en "Comments"))
(defmln *ir-comments-go*
    '(:fr "Commentaires ou rapports d'incidents" :en "Comments or Bugs"))
(defmln *ir-correct-error*
    '(:fr "Pour corriger, revenez à la page précédente."
          :en "To correct the error go back to previous page"))
(defmln *ir-dialog*
    '(:fr "Consultation des informations"
          :en "Consulting the database"))
(defmln *ir-dialog-answer*
    '(:fr "Réponse à : " :en "Answering: "))
(defmln *ir-dialog-explain*
    `(:fr ,(format nil "Le dialogue est flexible mais n'est pas ouvert. votre ~
            assistant se concentre sur les éléments d'information (création, ~
            présentation), ou sur les abonnements.")
          :en ,(format nil "Dialog is flexible but not open. the pa is concentrating ~
            on handling news items (creation, presentation), or subscriptions.")))
(defmln *ir-dialog-question*
    '(:fr "Question ?" :en "Question?"))
(defmln *ir-edit-category*
    '(:fr "Éditer une catégorie (corriger l'orthographe)"
          :en "Edit a category (to correct spelling)"))
(defmln *ir-edit-category-go*
    '(:fr "Catégorie" :en "Category" ))
(defmln *ir-edit-index*
    '(:fr "Créer/éditer un mof clé" :en "Add/Edit Index"))
(defmln *ir-edit-index-go*
    '(:fr "Mot-clé" :en "Index"))
(defmln *ir-edit-news*
    '(:fr "Éditer une information" :en "Edit a news item"))
(defmln *ir-edit-news-go*
    '(:fr "Localiser l'information à éditer" :en "Locate news item"))
(defmln *ir-edit-person*
    '(:fr "Ajouter une personne" :en "Add a person"))
(defmln *ir-edit-person-go*
    '(:fr "Personne" :en "Person"))
(defmln *ir-email*
    '(:fr "Email" :en "Email"))
(defmln *ir-error-text* 
    '(:fr "Merci d'écrire vos remarques ou de signaler les dysfonctionnements ci-contre."
          :en "Please signal and/or describe the problem."))
(defmln *ir-error-title*
    '(:fr "Nouvelles internationales - Erreur"
          :en "International News - Error"))
(defmln *ir-features*
    '(:fr "Options :" :en "Features:"))
(defmln *ir-index*
    '(:fr "Mot clé" :en "Index"))
(defmln *ir-goto-dialog*
    '(:fr "Activer le dialogue" :en "Goto dialog"))
(defmln *ir-journal*
    '(:fr "Journal (OK/<rien>)" :en "Journal (OK/<nothing>)"))
(defmln *ir-login-error*
    `(:fr ,(format nil "Si vous ne pouvez vous connecter, il est possible que vous ~
             n'ayez pas d'assistant personnel. merci de contacter JP. Barthès.")
             :en ,(format nil "If this does not work, you may  not have a personal ~
                 assistant, please contact JP. Barthès.")))
(defmln *ir-login-explain*
    `(:fr ,(format nil "Le login est utilisé pour activer votre assistant personnel ~
           ou vous en créer un nouveau, ce qui peut prendre quelques secondes.")
          :en ,(format nil "Login is used to locate your personal assistant or to ~
            create a new one, which can take a few seconds.")))
(defmln *ir-login-assistant*
    `(:fr "Voulez-vous que je crée un assistant (OK/rien) ?"
          :en "Do you want me to create an assistant (OK/nothing)?"))
(defmln *ir-login-language*
    '(:fr "Quelle langue ? ex. FR, EN,... (défaut EN)"
          :en "What language? e.g. EN, FR,...(default EN)"))
(defmln *ir-misc*
    '(:fr "Divers :" :en "Miscellaneous:"))
(defmln *ir-new-pass*
    '(:fr "Nouveau mot de passe :" :en "New password:"))
(defmln *ir-new-pass-check*
    '(:fr "Nouveau mot de passe (vérification) :" :en "New password (check):"))
(defmln *ir-news-item* '(:fr "Info" :en "News Item"))
(defmln *ir-no-assistant*
    '(:fr "Je ne trouve pas votre assistant. merci de vérifier l'orthographe."
          :en "I cannot find your personal assistant, please check spelling."))
(defmln *ir-old-pass*
    '(:fr "Ancien mot de passe :" :en "Old password:"))
(defmln *ir-or-return* 
    '(:fr "ou ... retour vers la page d'accueil"
          :en "or ... return to index page"))
(defmln *ir-password*
    '(:fr "Modifier le mot de passe" :en "Changing the password"))
(defmln *ir-password-go*
    '(:fr "Mot de passe" :en "Password"))
(defmln *ir-password-no-check*
    '(:fr "Le nouveau mot de passe n'est pas celui retapé."
          :en "New password and verification do not check."))
(defmln *ir-password-no-user*
    '(:fr "Je n'arrive pas à récupérer le nom de l'utilisteur."
          :en "I can't recover the username."))
(defmln *ir-password-not-ok*
    '(:fr "Échec de la mise à jour du mot de passe."
          :en "Password could not be changed."))
(defmln *ir-password-ok*
    '(:fr "Mot de passe modifié." :en "Password has been changed."))
(defmln *ir-password-wrong*
    '(:fr "Ancien mot de passe erronné." :en "Wrong password."))
(defmln *ir-person*
    '(:fr "Personne(s) à suivre" :en "Person(s) to follow"))
(defmln *ir-person-class*
    '(:fr "Personne" :en "Person"))
(defmln *ir-report-title*
    '(:fr "Nouvelles Internationales - Remarques ou erreurs"
          :en "International News - Remarks or Bugs"))
(defmln *ir-return* 
    '(:fr "Retour vers la page d'accueil" :en "Home"))
(defmln *ir-see-journal*
    '(:fr "Dernier journal" :en "Latest journal"))
(defmln *ir-see-journal-go*
    '(:fr "afficher le dernier journal" :en "Display the latest journal"))
(defmln *ir-send* 
    '(:fr "Envoyer" :en "Send"))
(defmln *ir-subscriber*
    '(:fr "Abonné (read only)" :en "Subscriber (read only)"))
(defmln *ir-subscription*
    '(:fr "Abonnement" :en "Subscription"))
(defmln *ir-subscriptions*
    '(:fr "Créer/éditer abonnements" :en "Create/Edit Subscriptions"))
(defmln *ir-subscriptions-go*
    '(:fr "Abonnements" :en "Subscriptions"))
(defmln *ir-title* 
    '(:fr "NEWS: Nouvelles internationales" :en "International News"))
(defmln *ir-title-dialog*
    '(:fr "NEWS: Nouvelles internationales - Dialogue"
          :en "International News - Dialog"))
(defmln *ir-title-pass*
    '(:fr "NEWS: Nouvelles internationales - Changement du mot de passe"
          :en "International News - Password Reset"))
(defmln *ir-title-subscription*
    '(:fr "NEWS: Nouvelles internationales - Abonnements"
          :en "International News - Subscriptions"))
(defmln *ir-username*
    '(:fr "Votre login :" :en "User login name:"))

;;;==============================================================================
;;;                             SERVICE MACROS
;;;==============================================================================

;;; none

;;;==============================================================================
;;;                            SERVICE FUNCTIONS
;;;==============================================================================

(defun area  (&key header name rows cols value)
  "produces a row in an html table (part of an html macro).
arguments:
   header: name of the field
   name: name of the html parameter
   rows: number of rows to the area
   cols: somehow the width of the area
return:
   a string to be inserted into an html macro."
    `(:tr
       (:td ,header)
      (:td ((:textarea :name ,name :rows ,rows :cols ,cols) ,value))))

;;;============================ service functions ================================

;;;----------------------------------------------------- CREATE-ASSISTANT-FOR-OMAS
;;; This function creates and installs a temporary PA agent and staff, cloning the
;;; necessary files, creating a single  new file in the application folder. I.e.,
;;; once the PA and staff are created the corresponding temporary files are deleted
;;; the assistant stub should be called, e.g., XXXXX-FR if the language is French
;;; user-description is something like:
;;;  ("personne" ("nom" "Charara") ("prénom" "Ali")
;;;   ("assistant personnel" "Nestor"))

(defun create-assistant-for-omas (agent-key &key (language :EN) user-description)
  "creates a new assistant by cloning ASSISTANT-STUB and other files according to ~
   language, e.g. XXXXX-FR and XXXXX-ONTOLOGY-FR for French.
Arguments:
   key: key of the future PA
   language (key): language of the PA (default is English)
   user-description (key): description of the user, master of the PA
Return:
   the key of the new assistant or nil in case of problem."
  (let* ((assistant-ref (symbol-name agent-key))
         (in-filename (format nil "XXXXX-~A" language))
         assistant-pathname)
    (format t "~2%;---------- Entering create-assistant-for-omas") 
    
    ;; first clone the PA stub, e.g. XXXXX-FR.lisp, substituting master-name to MMMMM
    (setq assistant-pathname
          (omas::clone-and-adjust-file 
           assistant-ref in-filename 
           :substitutions `(("YYYYY" . ,(format nil "~S" user-description)))))
    (unless assistant-pathname
      (return-from create-assistant-for-omas nil))
        
    ;;== OK we can load everything
    (omas::load-agent :agent-file-pathname assistant-pathname)
    
    ;;== delete temporary files
    (delete-file assistant-pathname)
    
    ;;== return success
    agent-key))

;;;--------------------------------------------------------------------- get-pa-ip
  
(defun get-pa-ip (req)
  "from a web request determines who is the sender, recording the ip of the sending
   machine. we assume different machines for different senders. another solution ~
   would be to use cookies.
argument:
   req: the web request from the webserver thread
return:
   the ip of the pa."
  (let ((socket (request-socket req))
        ip)
    ;(format t "~%; get-pa-ip /socket:~%;  ~s" socket)
    ;; extracts the ip of the calling machine
    (setq ip (socket:remote-host socket))
    ;(format t "~%; get-pa-ip /remote-host: ~s: ~s~%" ip (socket:ipaddr-to-dotted ip))
    ;; check for an entry in the users slot to get the pa key
    ip))

;;;-------------------------------------------------------------------- get-pa-key

(defun get-pa-key (req)
  "from a web request determines who is the sender, recording the ip of the sending ~
   machine. we assume different machines for different senders. another solution ~
   would be to use cookies.
argument:
   req: the web request from the webserver thread
return:
   the ip of the pa."
  ;;;  (format t "~%; get-pa-keomas::omas-application-directoryy /calling get-pa-ip")
  ;;;  (setq ip (get-pa-ip req))
  ;; check for an entry in the users slot to get the pa key
  (let ((ip (socket:remote-host (request-socket req))))
    (format t "~%; get-pa-key /users: ~s" (omas::users *omas*))
    (intern (omas.web::wu-get ip "pa-key") :keyword)
    ))

;;;------------------------------------------------------- load-assistant-for-news

;;;(defun load-assistant-for-news (pa-name)
;;;  "checks in the application folder if there is a folder named <pa-name>.lisp and ~
;;;   one named <pa-name>-NEWS.lisp. If there, loads them. If not, returns nil."
;;;  (let (pa staff file-pathname staff-file-pathname)
;;;    ;; make a tentative filename
;;;    (setq file-pathname (merge-pathnames (omas::omas-application-directory *omas*)
;;;                                         (format nil "~A.lisp" pa-name)))
;;;    ;; if file there try assistant's
;;;    (unless file-pathname (return-from load-assistant-for-news nil))
;;;    ;; check assistant's
;;;    (setq staff-file-pathname
;;;          (merge-pathnames (omas::omas-application-directory *omas*)
;;;                           (format nil "~A-NEWS.lisp" pa-name)))
;;;    ;; if not there quit
;;;    (unless staff-file-pathname (return-from load-assistant-for-news nil))
;;;    
;;;    ;;== OK we are in business
;;;    ;; load PA file
;;;    (setq pa (omas::load-agent :agent-file-pathname file-pathname))
;;;    ;; load staff agent
;;;    (setq staff (omas::load-agent :agent-file-pathname staff-file-pathname))
;;;    
;;;    ;; add agents to local agents
;;;    (push `(,(omas::key pa) ,pa) (omas::local-agents *omas*))
;;;    (push `(,(omas::key staff) ,staff) (omas::local-agents *omas*))
;;;    
;;;    ;; return success
;;;    t))
                      
;;;------------------------------------------------------- remove-empty-parameters

(defun remove-empty-parameters (alist)
  "takes an a-list and removes pairs whose cdr is an empty string"
  (remove nil
          (mapcar #'(lambda(xx) (unless (equal "" (cdr xx)) xx)) alist)))
                                 
#|
(remove-empty-parameters '(("pays" . "japon") ("correspondant" . "inoue")
            ("prénom correspondant" . "")
            ("initiale correspondant" . "t") ("url" . "")
            ("partenaire" . "u of tsukuba") ("sigle partenaire" . "")
            ("correspondant utc" . "barthès") ("type de contact" . "r")
                           ("date de début" . "2012")))
(("pays" . "japon") ("correspondant" . "inoue") ("initiale correspondant" . "t") 
 ("partenaire" . "u of tsukuba")("correspondant utc" . "barthès")
 ("type de contact" . "r") ("date de début" . "2012"))
|#
;;;-------------------------------------------------------------------------- row

;;;(defun row (header name &optional (size 100))
;;;  "produces a single row in an html table.
;;;arguments:
;;;   header: name of the field
;;;   name: name of the html parameter
;;;   size (opt): width of the value field (default 100)
;;;return:
;;;   a string to be inserted into an html macro"
;;;    `(:tr
;;;       (:td ,header)
;;;      (:td ((:input :name ,name :size ,size)))))

(defun row (&key header name (size 100) read-only value (type "text") 
                 &allow-other-keys)
  "produces a single row in an html table.
arguments:
   header (key): name of the field
   name (key): name of the html parameter
   size (key): width of the value field (default 100)
   value (key): initial value
   read-only (key): if true declares the field to be read-only (does not work)
return:
   a string to be inserted into an html macro"
  (moss::string+ 
   "<tr><td>" header "</td>" "<td><input name=" name  " type=" type
   " size=" size
   (if value (format nil " value=~s" value) "")
   (if read-only " readonly>" ">")
   "</td></tr>"))

#|
(row :header "nom" :name "name" :read-only t)
"<tr><td>nom</td><td><input name=name type=text size=100 readonly></td></tr>"
(row :header "nom" :name "name")
"<tr><td>nom</td><td><input name=name type=text size=100></td></tr>"
|#

;;;-------------------------------------------------------------------------- trim

(defun trim (text &key (char-list '(#\space)))
   (string-trim char-list (format nil "~a" text)))
   
#|
(trim "    le meux  ")
"le meux"
(trim " c'est lui...  " :char-list '(#\space #\.))
"c'est lui"
|# 

;;;===============================================================================
;;;===============================================================================
;;;                                 SKILLS 
;;;===============================================================================
;;;===============================================================================


;;;========================================================================= skill
;;;                               :GET-LANGUAGE
;;;===============================================================================
;;; we need this skill here to ask pa what language it understand. we callont send
;;; a message to pa directly since the server worker is not an agent

(defskill :get-language :web2pa
  :static-fcn static-get-language
  :dynamic-fcn dynamic-get-language)

(defun static-get-language (agent message args)
  "tries to get the pa language."
  (declare (ignore message))
  (let* ((to-agent (cdr (assoc :to-agent args)))
         (gate (cdr (assoc :gate args)))
         )
    ;; sage index gate
    (env-set agent gate :gate)
    ;; send message to to-agent
    (send-subtask agent :to to-agent :action :get-language :ack t :timeout 3)
    (static-exit agent :done)))

(defun dynamic-get-language (agent message answer)
  "get the answer from pa return it"
  (declare (ignore message))
  (let ((index (env-get agent :gate)))
    ;; if we come here, then we have an answer
    (omas::web-push-answer index answer)
    (format t "~%; dynamic-get-language /answer-list: ~s" 
      omas.web::*web-answer-list*)
    (omas::web-open-gate index)
    (dynamic-exit agent t)))

;;;========================================================================= skill
;;;                               :LOCATE-PA
;;;===============================================================================

(defskill :locate-pa :web2pa
  :static-fcn static-locate-pa
  :dynamic-fcn dynamic-locate-pa
  :timeout-handler timeout-locate-pa)

(defun static-locate-pa (agent message args)
  "tries to locate a pa by sending a hello message with a timeout. if there is ~
   no answer will return failure to the caller.
Arguments:
   agent: ..
   message: ignored
   args: (((:gate . ,gate-index)
           (:data . ,text)
           (:to-agent . ,to-agent)
           (:pattern . ,pattern))
           (:timeout . ,timeout)))
Return:
   :OK, :loaded, :done"
  (declare (ignore message))
  (let ((key (cdr (assoc :data args)))
        (index (cdr (assoc :gate args)))

        file-pathname
        )
    ;; check first if agent is one of the local agents
    (when (assoc key (omas::local-agents *omas*))
      (omas::web-push-answer index t)
      (omas::web-open-gate index)
      (return-from static-locate-pa (static-exit agent :ok)))
    
    ;; is assistant in our application folder, if so load it and quit
    (when
        (probe-file 
         (setq file-pathname
               (merge-pathnames (omas::omas-application-directory *omas*)
                                (format nil "~a.lisp" key))))
      (omas::load-agent :agent-file-pathname file-pathname)
      (omas::web-push-answer index t)
      (omas::web-open-gate index)
      (return-from static-locate-pa (static-exit agent :loaded)))
        
    ;; otherwise send message to try to find it elsewhere, timeout is 5 seconds
    (env-set agent index :gate)
    (send-subtask agent :to key :action :hello :timeout 5)
    (static-exit agent :done)))

(defun dynamic-locate-pa (agent message answer)
  (declare (ignore message))
  (let ((index (env-get agent :gate)))
    ;; if we come here, then we have an answer
    (omas::web-push-answer index answer)
    (format t "~%; dynamic-locate-pa /answer-list: ~s" 
      omas.web::*web-answer-list*)
    (omas::web-open-gate index)
    (dynamic-exit agent t)))

(defun timeout-locate-pa (agent message)
  (declare (ignore agent message))
  ;; asks OMAS to kill the task, avoiding to resend messages
  :kill-task)

;;;========================================================================= skill
;;;                            :LOG-ERROR
;;;===============================================================================
;;; log errors into the hdsri-error-log.text file, creating it if necessary

;;;(defskill :log-error :web2pa
;;;   :static-fcn static-log-error)
;;;   
;;;(defun static-log-error (agent message args)
;;;  "log errors into the hdsri-error-log.txt file."
;;;  (declare (ignore message))
;;;  (format t "~2%;========== WEBNEWS: LOG-ERROR")
;;;  (let ((text (cdr (assoc :data args)))
;;;        (file-pathname            
;;;         (make-pathname 
;;;             :name "NEWS-ERROR-LOG"
;;;             :type "txt"
;;;          :defaults (omas::omas-application-directory *omas*))))
;;;    (format t "~%;--- file-pathname: ~S" file-pathname)
;;;    (with-open-file (out file-pathname 
;;;                         :direction :output 
;;;                         :if-exists :append
;;;                         :if-does-not-exist :create
;;;                         :external-format :utf-8)
;;;      (format out "~%~s~%~a~%" 
;;;        (omas::date-string (get-universal-time)) text)))
;;;  (static-exit agent :done))
  
;;;========================================================================= skill
;;;                            :PROCESS-WEB-REQUEST
;;;===============================================================================
;;; essentially the same as process-web-text

(defskill :process-web-request :web2pa
   :static-fcn static-process-web-request
   :dynamic-fcn dynamic-process-web-request)
   
(defun static-process-web-request (agent message args)
  "function processes the data received by the web server to send the request to
   the target service agent.
arguments:
   agent: this agent
   message: incoming message
   args: ((:data ...)(:gate ...)(:to-agent ...)
     - data contains the text to be processed
     - gate contains the index of a synchronization variable. the function waiting
       for the answer is waiting on the gate associated with this index.
     - to-agent is the agent key of the destination pa
return:
   :done"
  (declare (ignore message))
  (let ((to-agent (cdr (assoc :to-agent args)))
        (action (cdr (assoc :action args)))
        (data (cdr (assoc :data args)))
        (language (cdr (assoc :language args)))
        (pattern (cdr (assoc :pattern args)))
        (gate (cdr (assoc :gate args)))
        (timeout (cdr (assoc :timeout args))))
    (env-set agent gate :gate)
    (send-subtask agent :to to-agent :action action 
                  :args `(((:data . ,data)
                           ,@(if language `((:language . ,language)))
                           ,@(if pattern `((:pattern . ,pattern)))
                           (:timeout . ,timeout)))
                  :timeout timeout  ; to kill process if it waits too long
                  )
    (static-exit agent :done)))

(defun dynamic-process-web-request (agent message answer)
  ;(format t "~%;web/ dynamic-process-web-request /message: ~s" message)
  ;; recover gate index
  (let ((index (env-get agent :gate))
        reason)
    (format t "~2%;<<< WEB2PA: dynamic-process-web-request" )
    (format t "~%;---answer:~%  ~S" answer)
    
    ;(omas::message-print message)
    ;; if answer is a failure, then extract reason if any
    ;; a NIL answer is not considered a failure...
    (when (and answer (moss::is-answer-failure? answer))
      (setq reason (omas::error-contents message))
      (format t "~%;--- reason: ~S" reason)
      ;; build a list starting with :error followd by reason (a string)
      (setq answer (list :error reason))
      )
    ;; save answer into list of answers indexed by gate number
    (omas::web-push-answer index answer)
    ;(format t "~%;web/ dynamic-process-web-request /answer-list: ~s" 
    ;  omas.web::*web-answer-list*)
    ;; open gate
    (omas::web-open-gate index)
    (dynamic-exit agent :done)))
	
;;;========================================================================= skill
;;;                           :PROCESS-WEB-TEXT
;;;===============================================================================

(defskill :process-web-text :web2pa
   :static-fcn static-process-web-text
   :dynamic-fcn dynamic-process-web-text)
   
(defun static-process-web-text (agent message args)
  "function processes the data received by the web server to send the text to the
   target pa.
arguments:
   agent: this agent
   message: incoming message
   args: ((:data ...)(:gate ...)(:to-agent ...)
     - data contains the text to be processed
     - gate contains the index of a synchronization variable. the function waiting
       for the answer is waiting on the gate associated with this index.
     - to-agent is the agent key of the destination pa
return:
   :done"
  (declare (ignore message))
  (let ((pa (cdr (assoc :to-agent args)))
        (text (cdr (assoc :data args)))
        (gate (cdr (assoc :gate args)))
        (language (cdr (assoc :language args)))
        (timeout (cdr (assoc :timeout args))))
    (env-set agent gate :gate)
    (send-subtask agent :to pa :action :process-web-text 
                  :args `(((:data . ,text)
                           (:language . ,language)
                           (:timeout . ,timeout)))
                  :timeout timeout  ; to kill process if it waits too long
                  )
    (static-exit agent :done)))

(defun dynamic-process-web-text (agent message answer)
  (declare (ignore message))
  ;; the answer her could be a failure...
  ;; recover gate index
  (let ((index (env-get agent :gate)))
    ;; save answer
    (omas::web-push-answer index answer)
    ;(format t "~%;web/ dynamic-process-web-text /answer-list:~%   ~s" 
    ;  omas.web::*web-answer-list*)
    ;; open gate
    (omas::web-open-gate index)
    (dynamic-exit agent :done)))
  
	
;;;======================================================================== skill
;;;                            :SHUT-DOWN-SERVER
;;;==============================================================================
;;; this should enable to shut down server before exiting omas
;;; disabled

(defskill :shut-down-server :web2pa
   :static-fcn static-shut-down-server)

(defun static-shut-down-server (agent message)
   "shut down the server if no other agent uses it"
;;;   (decf (omas::http-server-reference-count omas::*omas*))
;;;   ;; noop here for the time being
;;;   ;(omas::stop-web-server)
   ;; actually not very usefull when http is being used
   (declare (ignore message))
   (static-exit agent :noop))
  
;;;======================================================================== skill
;;;                              :START-SERVER
;;;==============================================================================
;;; this enables the server and loads the entities for answering questions and
;;; filling the forms through web pages
;;; use connect instead, since connect published the needed pages

(defskill :start-server :web2pa
   :static-fcn static-start-server)
   
(defun static-start-server (agent message)
  "start the http server, and set up receiving pages for handling the requests"
  (declare (ignore message)
           (special *web-server-port*))
  (let ()
    ;; first start server
    (omas::start-web-server agent *web-server-port*)
    ;; publish a few pages
    
    (publish-error-page agent)
    (publish-login-form agent)
    (publish-omas-web-edit-forms agent) ; web editor
    ;(publish-password-form agent)
    (publish-web-dialog agent)
    
    ;; publishing the login form requires a list of authorizations given by the
    ;; members agent. thus, we must ask for it.
    ;; note that the list is static and is built at start up
    
    (static-exit agent :done)))


;;;==============================================================================
;;;==============================================================================
;;;                                WEB PAGES
;;;==============================================================================
;;;==============================================================================


;;; checks can be done using the html parser
;;;   (require :phtml)
;;;   (use-package :net.html.parser)
;;;   (pprint (parse-html "..."))
   
;;;==============================================================================
;;;                           ERROR PAGE
;;;==============================================================================
;;; this does not use the published error page but the page from which the error
;;; -page function is called!

(defun error-page (text)
  (eval
   `(html
     (:html
      (:head 
       (:title ,(mln::extract-to-string *ir-error-title*))
       ((:meta :charset "utf-8")))
      (:body
       "<font face=\"arial\">"
       "<fieldset style=\"background-color:yellow;\">"
       ((:form :action "error")
        (:center (:h2 ,(mln::extract-to-string *ir-error-title*)))  
        (:princ ,text)
        :br
        :br
        (:b ,(mln::extract-to-string *ir-correct-error*))
        :br :br
        ((:a :href "index") ,(mln::extract-to-string *ir-or-return*))
        :br
        ))))))

(defun publish-error-page (agent)
  "post an introductory index form"
  (declare (ignore agent))
  (publish 
   :path "/error"
   :content-type "text/html"))

;;;==============================================================================
;;;                             LOGIN
;;;==============================================================================
;;; form for login into the system, the initial password is the login name
;;; Each member of the NEWS application has a login name, a name, a first name,
;;; and an email address. All the information is handled by the MEMBERS agent.
;;; When a member logs in, MEMBERS returns the name, first name and email that
;;; are copied into the user entry.

(defun add-login-body (user-login-name)
  `(((:table :border "0" :cols "2" :width "100%")
     (:tbody
      ,(row :header (mln::extract-to-string *ir-username*)
            :name "login" :value user-login-name)
      :br 
      ,(row :header (mln::extract-to-string *ir-assistant*) :name "pa-name")
      :br
      ,(row :header (mln::extract-to-string *ir-login-language*) :name "language"
            :value "EN")
      ))))
	
(defun add-login-form (&key user-login-name error message)
  (format t "~%;--- posting login form...")
  (eval
   `(html 
     (:html
      (:head 
       (:title ,(mln::extract-to-string *ir-title*))
       ((:meta :charset "utf-8")))
      (:body 
       "<font face=\"arial\">"
       ((:form :action "NEWS")
        "<fieldset style=\"background-color:yellow;\">"
        :br
        (:center (:h2 ,(mln::extract-to-string *ir-title*)))
        ,@(when error (list (mln::extract-to-string *ir-no-assistant*)))
        ,@(if message `(,(format nil message))) ; removes ~
        :br
        ,@(add-login-body user-login-name)
        :br
        ,@(unless error (list (mln::extract-to-string *ir-login-explain*)))
        :br :br
        (:center ((:input :type "submit" :value ,(mln::extract-to-string *ir-send*))))
        :br
        ,@(when error (list (mln::extract-to-string *ir-login-error*)))
        ))))))

;;;----------------------------------------------------------- publish-login-form
;;; the function is called from within the start-server skill, one then can request
;;; a list of names and indexes from the members agent.

(defun publish-login-form (agent)
  "post a form to connect to a personal assistant. will only connect to known pas.
arguments:
   agent: web2pa
return:
   an html string corresponding to the page to publish"
  (publish 
   :path "/OMAS"
   :content-type "text/html"
   :function
   #'(lambda (req ent)
       (format t "~2%;========== ENTERING login form")
       (format t "~%;--- req: ~S " req)
       (let (answer result)
         (multiple-value-bind (login password) (get-basic-authorization req)
           (format t "~%;--- (request-query req): ~S " (request-query req))
           (format t "~%;--- login: ~S, password: ~S"  login password)
           (if* (and login password)
              then  
                   ;; ask MEMBERS to check login and password
                   (setq answer
                         (omas::get-answer
                          agent 
                          `(("login" . ,login) ("password" . ,password))
                          :to-agent :MEMBERS
                          :action :authorized?
                          :pattern '("person" ("name")("first name")("email"))
                          :timeout 5
                          :allow-nil-answer t))
                   ;; in case of error or timeout we must return nil
                   (setq result
                         (if (and (listp answer)(eql (car answer) :error))
                             nil
                           answer))
                   ;; if OK, answer should be something like
                   ;; (("name" "Barthès") ("first-name" "Jean-Paul") ("email" "barthes@utc.fr"))
                   ;; publish the page
                   (if result
                       (with-http-response (req ent)
                         ;(format t "~%; publish-login-form /username: ~s"
                         ;  (get-basic-authorization req))
                         (with-http-body (req ent)
                           ;; call the NEWS login page to specify the pa
                           (publish-login-form-action agent req ent login answer)))
                     ;; else oblige the server to put up a name/password dialog
                     (with-http-response (req ent :response *response-unauthorized*)
                       (set-basic-authorization req "OMAS server")
                       (with-http-body (req ent))))
              else
                   ;; else oblige the server to put up a name/password dialog
                   (with-http-response (req ent :response *response-unauthorized*)
                     (set-basic-authorization req "OMAS server")
                     (with-http-body (req ent)))))))))
                                    
;;;---------------------------------------------------- publish-login-form-action

(defun publish-login-form-action (agent req ent user-login-name user-info)
  "controls the login page. We get here once login and password have been checked.
Arguments:
   agent: WEB2PA
   req: request
   ent: entity handling the page
   user-login-name: login, e.g. \"barthes\" 
   user-info: ((\"name\" <name>)(\"first name\" <first name>)(\"email\" <email>))"
  (declare (ignore ent))
  (format t "~2%;---------- WEB2PA: entering publish-login-form-action")
  (format t "~%;--- parms:~%  ~s" (request-query req))
  ;;=== the first time around req is nil (no parameters transmitted by the login/
  ;; password form) - display login form
  (unless (request-query req) 
    (return-from publish-login-form-action
      (add-login-form :user-login-name user-login-name)))
  
  ;;=== otherwise we get the parms of the input form
  (let* ((query-req (remove-empty-parameters (request-query req)))
         (login (get-field "login" query-req))
         (pa-name (get-field "pa-name" query-req))
         (user-name (car (get-field "name" user-info)))
         (user-first-name (car (get-field "first name" user-info)))
         (user-email (car (get-field "email" user-info)))
         ip pa-key answer language user-description)
    
    ;; get ip of the sender of the web page
    (setq ip (get-pa-ip req))               
    (format t "~%; publish-login-form /sender ip: ~s,  ~s" 
      ip (socket:ipaddr-to-dotted ip))
    
    ;; if not specified, assistant has the same name as the user login (better 
    ;; than the user name because the login is unique)
    (setq pa-name (or pa-name user-login-name))
    (format t "~%; publish-login-form-action /pa-name:  ~s" pa-name)
    
    (setq pa-key (intern (moss::%string-norm pa-name) :keyword))
    (format t "~%; publish-login-form-action /pa key:  ~s" pa-key)
    
    ;;=== try to locate corresponding pa (2 seconds should be enough to get an
    ;;  answer)
    (setq answer 
          (omas::get-web-answer :web2pa pa-key :action :locate-pa
                                :timeout 5 :allow-nil-answer t))
    (format t "~%; publish-login-form-action/ locate-pa answer: ~s" answer)
    
    (cond
     ;;=== here we could locate the PA, thus it exists as an agent
     (answer
      ;; create an entry in the *omas* table with name and ip
      (omas.web::wu-set-user-entry ip `("user-login" . ,login)
                                   `("user-name" . ,user-name)
                                   `("user-first-name" . ,user-first-name)
                                   `("user-email" . ,user-email)
                                   `("pa-key" . ,pa-key))
      ;; get pa language
      (setq answer 
            (omas::get-web-answer :web2pa nil :to-agent pa-key
                                  :action :get-language
                                  :timeout 3 :allow-nil-answer t))
      (format t "~%; publish-login-form-action/ get-language answer: ~s" answer)
      ;; if nil default to english
      (setq language (or answer *language*))
      (omas.web::wu-add ip `(("language" . ,(format nil "~s" language))
                             ("agent-ref" . ,pa-name)))
      
      ;; kludge: if ip is local ip, also put internal ip
      ;; thus, if pa is on the current machine, it will have 2 addr
      ;; "127.0.0.1" and internal ip (e.g. dhcp: 192.168.1.19)
      ;;;              (if (eql ip 2130706433) ; localhost
      ;;;                  (pushnew `(,(socket:dotted-to-ipaddr (omas::internal-ip agent))
      ;;;                               ,pa-key
      ;;;                               ,username)
      ;;;                           (omas::users *omas*) :test #'equal))
      ;; return the index page
      (let ((*language* language))
        (add-dialog-form ip))
      )
     
     ;;=== here the answer is nil PA cannot be located; we create a new PA
     ((let ((language (get-field "language" query-req)))
        (setq language (intern (moss::%string-norm language) :keyword))
        
        ;; get member description
        (setq answer
              (omas::get-answer agent `(:master ,login :assistant ,pa-name)
                                :to-agent :MEMBERS
                                :action :get-member-description
                                :language language
                                :timeout 3 :allow-nil-answer t))
        ;; if answer is nil then problem
        (unless answer 
          (error "publish-login-form-action: can't get user description from ~
                     MEMBERS, hence can't create assistant."))
        
        (format t "~%; publish-login-form-action / answer: ~%  ~S" answer)
        
        ;; save user-description
        (setq user-description answer)
        ;; now go create assistant with the right language 
        (format t "~%; publish-login-form-action / *current-process*: ~S" mp:*current-process*)
        (create-assistant-for-OMAS
         pa-key :language language :user-description answer))
      
      ;; create an entry in the *omas* table with name and ip
      (omas.web::wu-set-user-entry ip `("user-login" . ,login)
                                   `("user-name" . ,(getv "name" user-description))
                                   `("user-first-name" . ,(getv "first name" user-description))
                                   `("user-email" . ,(getv "email" user-description))
                                   `("pa-key" . ,pa-key)
                                   `("language" . ,(format nil "~s" language))
                                   `("agent-ref" . ,(symbol-name pa-key)))
      
      ;; here we could create a person for the master in the staff agent
      (omas::get-answer agent user-description 
                        :to-agent (intern (string+ pa-key "-NEWS") :keyword)
                        :type :inform
                        :action :record-person
                        :language language)
      
      ;; do that for publisher too, (careful person may already be known from PUBLISHER)
      (omas::get-answer agent user-description 
                        :to-agent :PUBLISHER
                        :type :inform
                        :action :record-person
                        :language language)
      
      ;; return the index page
      (let ((*language* language))
        (add-dialog-form ip))
      )
     
     ;; Otherwise, post an error message
     (t
      (error-page "I can't find your PA, nor can I create a new one...")
      ))
    ))

;;;=============================================================================
;;;                            PASSWORD FORM
;;;=============================================================================

;;;----------------------------------------------------------- add-password-body

(defun add-password-body (&key (old-password "")(new-password "")(echo ""))
  `(((:table :border "0" :cols "2" :width "100%")
     (:tbody
      ,(row :header (mln::extract-to-string *ir-old-pass*) :name "old-password"
            :type "password" :value old-password)
      :br 
      ,(row :header (mln::extract-to-string *ir-new-pass*) :name "new-password" 
            :type "password" :value new-password)
      :br
      ,(row :header (mln::extract-to-string *ir-new-pass-check*) :name "echo" 
            :type "password" :value echo)
      :br
      ))))

;;;----------------------------------------------------------- add-password-form

(defun add-password-form (&key messages old-password new-password echo)
  (eval
   `(html 
     (:html
      (:head 
       (:title ,(mln::extract-to-string *ir-title-pass*))
       ((:meta :charset "utf-8")))
      (:body 
       "<font face=\"arial\">"
       ((:form :action "passwordform")
        "<fieldset style=\"background-color:yellow;\">"
        :br
        (:center (:h2 ,(mln::extract-to-string *ir-title-pass*)))
        ,@(if messages (list (format nil "~{~a~^~%~}" messages)))
        :br
        ,@(add-password-body :old-password old-password :new-password new-password
                             :echo echo)
        :br
        :br
        (:center ((:input :type "submit" :value ,(mln::extract-to-string *ir-send*))))
        :br 
        :br
        ((:a :href "index") ,(mln::extract-to-string *ir-return*))
        ))))))

;;;------------------------------------------------------- publish-password-form

(defun publish-password-form (agent)
  (publish 
   :path "/passwordform"
   :content-type "text/html"
   :function
   #'(lambda (req ent)
       ;(format t "~%;++++ publish-password-form /req: ~s~%~%" req)
       ;(format t "~%;++++ publish-password-form /(request-query req): ~s~%~%" 
       (with-http-response (req ent)
         (with-http-body (req ent)
           (publish-password-form-action agent req ent))))))

;;;------------------------------------------------ publish-password-form-action

(defun publish-password-form-action (agent req ent)
  "controls the password form"
  (declare (ignore ent))
  (let* ((query-req (request-query req))
         (old-password (get-field "old-password" query-req))
         (new-password (get-field "new-password" query-req))
         (echo (get-field "echo" query-req))
         ip answer result username)
    
    ;; first time around, post page
    (unless old-password 
      (return-from publish-password-form-action (add-password-form)))
    
    ;; get ip of the sender of the web page
    (setq ip (get-pa-ip req))               
    (format t "~%; publish-password-form-action /sender ip: ~s,  ~s" 
      ip (socket:ipaddr-to-dotted ip))
    
    ;; get user name
    (format t "~%; publish-password-form-action /users:~% ~s"
      (omas::users *omas*))
    (setq username (get-field "username" (cdr (assoc ip (omas::users *omas*)))))
    (format t "~%; publish-password-form-action /username: ~s" username)
    
    (unless username
      (return-from publish-password-form-action 
        (add-password-form 
         :messages `(,(mln::extract-to-string *ir-password-no-user*)))))
    
    ;; if fields contain values, check old index
    (setq answer
          (omas::get-answer agent `(("username" . ,username)
                                    ("password" . ,old-password))
                            :to-agent :members
                            :action :check-password
                            :timeout 5
                            :allow-nil-answer t))
    ;; answer is either t, or (:error nil) or nil on timeout
    (setq result (if (and (listp answer)(eql (car answer) :error)) nil answer))
    (format t "~%; publish-password-form-action /checked old-password ~s" result)
    
    ;; if nil call page again
    (unless result
      (return-from publish-password-form-action 
        (add-password-form :messages `(,(mln::extract-to-string *ir-password-wrong*))
                           :new-password new-password :echo echo)))
    
    ;; check new index and echo
    (unless (moss::equal-gen new-password echo)
      (return-from publish-password-form-action 
        (add-password-form 
         :messages `(,(mln::extract-to-string *ir-password-no-check*))
         :old-password old-password :new-password new-password)))
    
    ;; ok update index
    (setq answer
          (omas::get-answer agent `(("username" . ,username)
                                    ("password" . ,new-password))
                            :to-agent :members
                            :action :set-password
                            :timeout 5
                            :allow-nil-answer t))
    (setq result (if (and (listp answer)(eql (car answer) :error)) nil answer))
    (format t "~%; publish-password-form-action /set new-password ~s" result)
    
    (unless result
      (return-from publish-password-form-action 
        (add-password-form :messages 
                           `(,(mln::extract-to-string *ir-password-not-ok*)))))
    
    ;; ok, redisplay page
    (add-password-form :messages `(,(mln::extract-to-string *ir-password-ok*)))
    ))

;;;=============================================================================
;;;                            WEB DIALOG FORM
;;;=============================================================================
;;; when the server uses this entity, it must decide who is calling. to do so
;;; it uses the ip of the sending machine, retrieving the corresponding pa key
;;; from the a-list stored in the users slot of the *omas* globals. if there is
;;; no entry, then transfers message to hdsri. we assume that each different
;;; caller is on a different machine. if not (e.g. for tests on the server) the
;;; new caller, e.g. untel, has priority over hdsri (the ip will be "127.0.0.1")
;;; when verbatim in non nil, then pass all chars

(defun dialog-form (&optional user-input answer verbatim)
  (if verbatim
      ;; we want to pass every character (no end of sentence on . or ? or !
      (eval
       `(html 
         (:html
          (:head (:title ,(mln::extract-to-string *ir-title-dialog*))
                 ((:meta :charset "utf-8")))
          (:body
           "<fieldset style=\"background-color:yellow;\">"
           "<font face=\"arial\">"
           ((:form :action "webdialog")
            (:center (:h2 ,(mln::extract-to-string *ir-title-dialog*)))
            ,@(if answer
                  (progn
                    `((:b ,(mln::extract-to-string *ir-dialog-answer*))
                      (:princ " ")
                      (:princ ,user-input)
                      :br
                      :br
                      (:princ ,answer)))
                `((:b ,(mln::extract-to-string *ir-dialog-question*))))
            :br
            :br
            (:center
             "<textarea rows=\"3\" cols=\"100%\" name=\"user-input\" autofocus>
          </textarea>")
            :br
            "<center>"
            ((:input :type "submit" :value ,(mln::extract-to-string *ir-send*)))
            "</center>"
            :br :br
            (:small ,(mln::extract-to-string *ir-dialog-explain*))
            :br
            :br
            ((:a :href "index") ,(mln::extract-to-string *ir-return*))
            )))))
    
    ;; otherwise end input automatically on . or ? or !
    (eval
     `(html 
       (:html
        (:head (:title ,(mln::extract-to-string *ir-title-dialog*))
               ((:meta :charset "utf-8")))
        (:body
         "<script language=\"javascript\">
	   function checkKey(e) {
              var character = String.fromCharCode(e.keyCode);
              if( character == '.' || character == '!' || character == '?') {
                 document.forms[0].submit();
                 }
              }
        </script>"
         "<fieldset style=\"background-color:yellow;\">"
         "<font face=\"arial\">"
         ((:form :action "webdialog")
          (:center (:h2 ,(mln::extract-to-string *ir-title-dialog*)))
          ,@(if answer
                (progn
                  `((:b ,(mln::extract-to-string *ir-dialog-answer*))
                    (:princ " ")
                    (:princ ,user-input)
                    :br
                    :br
                    (:princ ,answer)))
              `((:b ,(mln::extract-to-string *ir-dialog-question*))))
          :br
          :br
          (:center
           "<textarea rows=\"3\" cols=\"100%\" name=\"user-input\" 
              onkeypress= \"return checkKey(event)\" autofocus></textarea>")
          :br
          "<center>"
          ((:input :type "submit" :value ,(mln::extract-to-string *ir-send*)))
          "</center>"
          :br :br
          (:small ,(mln::extract-to-string *ir-dialog-explain*))
          :br
          :br
          ((:a :href "index") ,(mln::extract-to-string *ir-return*))
          )))))
    ))

;;;---------------------------------------------------------- publish-web-dialog
;;; publish web page

(defun publish-web-dialog (agent)
  (publish 
   :path "/webdialog"
   :content-type "text/html"
   :function
   #'(lambda (req ent)
       ;(format t "~%;++++ publish-web-page /req: ~s~%~%" req)
       ;(format t "~%;++++ publish-web-page /(request-query req): ~s~%~%" 
       (with-http-response (req ent)
         (with-http-body (req ent)
           (publish-web-dialog-action agent req ent))))))

;;;--------------------------------------------------- publish-web-dialog-action

(defun publish-web-dialog-action (agent req ent)
  "functions associated to the entity handling the webdialog page"
  (declare (ignore ent))
  (let* ((parms (request-query req))
         ;; parms contains whatever was typed by the user in user-input
         (user-input (get-field "user-input" parms))
         ;; there is no "language" parameter in parms!
         ;(language (get-field "language" parms))
         (ip (socket:remote-host (request-socket req)))
         ;; ... but there might be one in the user-input
         (language (omas.web::wu-get ip "language"))
         data text pa-key edit-data verbatim)
    ;; set language
    (set-language language)
    (cond
     ((and user-input (setq pa-key (get-pa-key req)))
      ;; here we got some question from the user and its ip
      (format t "~%;++++ publish-web-dialog-action /user-input: ~s~%~%" user-input)
      
      ;; send the request as an internal message to the web agent
      ;; with the text and the identity of the pa
      ;; get-answer will create a gate to wait for any processing 
      ;; that must be done to compute the answer
      ;; default action is :process-web-text
      (setq data 
            (omas::get-web-answer :web2pa user-input :to-agent pa-key))
      ;; answer returns a list of pairs 
      ;;  ((:text "")(:question "")(:answer ""))
      ;; that may be inserted into the answer page==
      (format  t "~%; publish-web-dialog-action /answer: ~s" data)
      
      ;;== when the answer is a list containing :data, then we launch an editor
      (cond
       ((setq edit-data (cdr (assoc :data data)))
        ;; call editor, return data is like (:create "stevens-web" "news item")
        ;;*****
        ;; calling the edit post will post a page, however, it is not clear as to
        ;; what entity will handle the form when clicking the "send" button
        ;;*****
        ;; we skip te first returnd value...
;;;        (net.aserve.client:do-http-request "http://localhost/locate"
;;;          :query `(("agent" . ,(cadr edit-data))
        ;;;                   ("class-ref" . ,(caddr edit-data))))
        ;;;********* not sure the agent is the right one
        (omas.web::web-edit-external-entry agent ip
                       `(("agent-ref" . ,(cadr edit-data))
                         ("class-ref" . ,(caddr edit-data))))
        )
       ;;== otherwise, recover text containing answer and question
       (t
        (setq text (cdr (assoc :text data)))
        ;; check if we want to pass all chars
        (setq verbatim (getvl data :verbatim))
        ;; print the page with the result underneath
        (dialog-form user-input text verbatim))
       )
      )
     
     ;;=== here we have a user input but could not locate pa
     (user-input
      (dialog-form user-input (mln::extract-to-string omas::*web-no-pa*))
      )
     
     ;;=== we come here the first time around since 
     (t
      ;; first time access, put up the form
      (dialog-form)))))



;;;===============================================================================
;;; 
;;;                               INITIAL CONDITIONS 
;;;
;;;===============================================================================

;;; none

;;;==============================================================================
;;; 
;;;                                    GOALS
;;;
;;;==============================================================================

;;; goal to start the server a few seconds after the agent has been created

(defgoal :start-server :web2pa
  :activation-delay 3 
  :script start-server-script)

(defun start-server-script (agent)
  "send a request message to start the web server"
  (declare (ignore agent))
  (list (make-instance 'omas::message :type :request :to :web2pa 
                       :action :start-server)))

;;; ================================== setup messages ===========================

;;; none

(format t "~&;*** OMAS v~A - agent WEB2PA loaded ***" omas::*omas-version-number*)

;;; :eof
