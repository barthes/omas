﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;            O M A S - W - S M A L L - W I N D O W (file W-small-window.lisp)
;;;
;;;===============================================================================

;;; This is a very simple PA window derived from the TATIN application.
;;; Only works for ACL
;;; for some of the OMAS functions to work the name of the output upper pane must be
;;; :assistant and that of the input pane underneath :master

#| 
2020 
 0202 restructuring OMAS as anasdf system
|#

(in-package :omas)

;;;================================================================================
;;;
;;;                                GLOBALS
;;;
;;;================================================================================

;(setf (com-window albert::albert) (make-master-window albert::albert))

(defParameter *PA-window* nil)


;;;=============== dimensions for the assistant window ============================
;;;
;;;
;;;      ____________________________________________________________
;;;     |____________________________________________________________|
;;;     |                                      _______     _______   |
;;;     |   OMAS logo ?                       |_______|   |_______|  |
;;;     |   ______________________________________________________   |
;;;     |  | <dialog assistant>                                   |  |
;;;     |  |                                                      |  |
;;;     |  |______________________________________________________|  |
;;;     |                                      _______     _______   |
;;;     |                                     |_______|   |_______|  |
;;;     |   ______________________________________________________   |
;;;     |  | <input from master>                                  |  |
;;;     |  |______________________________________________________|  |
;;;     |____________________________________________________________|

;;;(defParameter *mw-default-font* 
;;;  (cg:make-font-ex :SWISS "MS Sans Serif / ANSI" 11 NIL)
;;;  "seems to be unused"
;;;  )
;;;
;;;(defParameter *master-window-color*
;;;  (cg:make-rgb :RED 202 :GREEN 228 :BLUE 255)
;;;  )
;;;
(defParameter *assistant-SW-label-font*
  (cg:make-font-ex NIL "Tahoma / ANSI" 11 NIL)
  )

;;;================================================================================
;;;
;;;                       WINDOW and METHODS
;;;
;;;================================================================================
;;; must be removed and the regular one updated to take small windows unto account

(defMethod display-assistant-small-window ((agent agent) &aux win) ; JPB 1411
  "we build and display a window for the dialog with the master.
Arguments:
   agent: agent."
  (setq win (make-PA-small-window agent))
  ;; record window name in agent
  (setf (com-window agent) win)
  :done)

;;;===============================================================================
;;;
;;;                     MASTER/ASSISTANT WINDOW INTERFACE
;;;
;;;===============================================================================

;;; We define a panel for OMAS assistants

(defClass assistant-small-window (cg:dialog)
  ((agent :accessor agent)
   (answer-to-task-to-do :accessor answer-to-task-to-do :initform nil)
   (pass-every-char :accessor pass-every-char :initform nil)
   )
  (:documentation
   "Defines the format and behavior of the ASSISTANT WINDOW"))

;;; adding MOSS methods for handling dialogs

;;;---------------------------------------- (ASSISTANT-WINDOW) MOSS::ACTIVATE-INPUT

(defMethod moss::activate-input ((win assistant-small-window) &key erase)
  "activates and clears the input pane of the window.
Arguments:
   erase (key): if true erase the content, otherwise select it
Return: unimportant."
  (declare (ignore erase))
  (let ((input-pane (cg:find-component :master win)))
    ;; activate the user panel
    ;; the user will enter its data that will be transferred into the to-do
    ;; slot, waking up the resume process
    (cg:select-window win)
    ;; activate the master pane
    (cg:set-focus-component input-pane)
    ;; and clear the input pane JPB1110 (since we post the history anyway)
    (setf (cg:value  input-pane) "")
    ))

;;;------------------------------------------ (ASSISTANT-WINDOW) MOSS::DISPLAY-TEXT
;;; we should add to the window a property registering how many lines there are in
;;; the assistant pane (depending on the height of the font) so that we can scroll
;;; to keep newly printed stuff in view.

;;; here we assume that the assistant pane is a rich-edit pane, which leads to
;;; some problems concerning size of the font. The size is not the same thing
;;; as the face of the font. So when we use the size for printing and give the
;;; same value to the plain-to-rich-text function, we have some problems

(defMethod moss::display-text ((win assistant-small-window) text &key erase newline
                               final-new-line (header "") clean-pane 
                               (color moss::*black-color*)
                               &allow-other-keys)
  "prints a text into the output pane of the assistant panel.
Arguments:
   text: text to display (must be a simple string)
   clean-pane (key): erase the assistant pane in all cases
   erase (key): if t, erase previous text, otherwise append to it
   final-new-line (key): if t, add newline command at the end of text
   header (key): a string that will be printed in front of the text 
                 (default \"\")
   newline (key): it t, add a new line in front of the header
Return: 
   nil."
  (declare (ignore erase))
  (let* ((output-pane (cg:find-component :assistant win))
         (output-window (cg:window output-pane))
         (agent (agent win))
         nb-of-lines)
    (unless (and (stringp text)(stringp header))
      (error "text ~S and header ~S should be strings." text header))
    
    (if final-new-line (setq text (concatenate 'string text "~%")))
    (if newline (setq header (concatenate 'string "~%" header)))
    
    ;; because text or header could contain format directives, we process it
    (setq text (format nil (concatenate 'string header text)))
    
    ;(moss::vformat "OMAS-ASSISTANT-WINDOW display-text /erase: ~S" erase)
    ;; record  the number of lines
    (setq nb-of-lines (cg:number-of-text-lines output-pane))
    ;(print nb-of-lines)
    
    (when clean-pane
      (setf (cg:value output-pane) "")
      (return-from moss::display-text))
    
    ;(format t "~% assistant moss::display-text /text: ~S" text)
    ;(format t "~% assistant moss::display-text /size: ~S" (output-font-size agent))
    
    ;; add more text
    (setf (cg:rich-edit-range output-window)
      (cg:concatenate-rich-text 
       (cg:rich-edit-range output-window)
       (cg:plain-to-rich-text (format nil text) 
                              (or (omas::output-font-string agent)
                                  (cg:font-face (cg:font (cg:screen cg:*system*))))
                              (omas::output-font-size agent) :color color)))
        
    ;(print (cg:rich-edit-range (cg:window output-pane)))
    ;; should scroll the pane so that we see the new values, i.e. so that the first
    ;; added line is now the top line
    (setq nb-of-lines (max 0 (- nb-of-lines 15)))
    ;(print nb-of-lines)
    (cg:set-first-visible-line output-pane nb-of-lines)
    nil))

#|
(moss::display-text (com-window albert::PA_albert) "... encore du texte")
(moss::display-text (com-window albert::PA_albert) "... encore du texte"
                    :header "~%Master> ")
|#
;;;----------------------------------------------------------------- MAKE-PA-WINDOW
;(setf (com-window jp) (omas::make-master-window stevens::stevens))
; (setf (com-window albert::albert)(omas::make-master-window albert::albert))
#|
(make-PA-small-window ALBERT::PA_ALBERT)
|#

(defUn make-PA-small-window (agent)
  "define the master's interface window, capable of handling dialogs and viewing ~
            various types of info like the contents of the master's bins."
  (let ((font (output-font-string agent))
        win master-pane)
    ;; font is non nil if defassistant contained a font option
    (setq win
          (cr-window 
           :class 'assistant-small-window
           :type :document
           :font (if font
                     (cg:make-font-ex :swiss font (output-font-size agent))
                   ;; otherwise we take the system font
                   (cg:font (cg:screen cg:*system*)))
           :title (symbol-name (name agent))
           :left 1
           :top 200  ; 300
           :width (- 1346 684) ; 550
           :height (- 750 168) ; 550
           :background-color (cg:make-rgb :red 239 :green 235 :blue 143)
           :maximize-button t
           :resizable t
           :subviews (make-PA-SW-widgets)
           ))
    #| other parameters from ACL
           :resizable t
           :state :normal
           :system-menu t
           :title title
           :title-bar t
           :dialog-items (make-form5-widgets)
|#
    ;; record agent
    (setf (agent win) agent)
    ;; select text from the master pane
    (setq master-pane (cg:find-component :master win))
    ;; set delayed value to nil to wake up on-change function after each char
    (setf (cg:delayed master-pane) nil)
    ;; select text
    (cg:set-selection master-pane 0 (1+ (length (cg:value master-pane))))
    ;; select pane
    (cg:set-focus-component master-pane)
    ;; record windo into agent
    (setf (omas::com-window agent) win)
    ;; return window object, for debugging purposes
    (setq *PA-small-window* win)
    ))

;;;---------------------------------------------------------------- MAKE-PA-WIDGETS
; (make-PA-SW-widgets)
; (setq agent albert::albert)
; (omas::make-PA-small-window albert::albert)
(defUn make-PA-SW-widgets ()
  
  (list 
   ;;===== OMAS LOGO / TITLE
;;;   (make-instance 'cg:static-picture 
;;;     :bottom-attachment :scale 
;;;     :height 80 
;;;     :left 180
;;;     :left-attachment :scale 
;;;     :name :logo 
;;;     ;:pixmap-name 'logo-tatin-pic ; if cached with cache-pixmap
;;;     ;:pixmap-use-handle t 
;;;     ;:pixmap-source (merge-pathnames "logo-Tatin-PicS.bmp" (omas-directory *omas*))
;;;     :pixmap-source (merge-pathnames "OMAS.bmp" (omas-directory *omas*))
;;;     :right-attachment :scale 
;;;     :top 6 
;;;     :top-attachment :scale 
;;;     :width 130)
   (make-instance 'cg:static-text 
     :bottom-attachment :scale 
     :font (cg:make-font-ex nil "Segoe UI / Default" 40)
     :height 40
     :left 10
     :left-attachment :scale 
     :name :mi-title 
     :right-attachment :scale
     :top 10
     :top-attachment :scale 
     :width 130
     :value "OMAS")
   
   ;;===== assistant pane
   ;; title of the assistant pane
   (make-instance 'cg:static-text 
     :bottom-attachment :scale 
     :font *assistant-label-font*
     ;(cg:make-font-ex nil "Segoe UI / Default" 12) 
     :left 49
     :left-attachment :scale 
     :name :assistant-title 
     :right-attachment :scale
     :top 60 
     :top-attachment :scale 
     :value (mln::extract-to-string *WASS-a*) ; jpb 1406
     :width 130)
   
   ;; switch button to big interface
   (make-instance 'cg:button 
     :bottom-attachment :scale 
     :font *assistant-label-font* 
     :left 375
     :left-attachment :scale 
     :name :mi-switch-button
     :right-attachment :scale 
     :title (mln::extract-to-string *WASS-a-big-interface*)  ; jpb 1406
     :top 45
     :top-attachment :scale 
     :width 123
     :on-click 'MSW-A-switch-on-click)
   
   ;; clear button of assistant pane
   (make-instance 'cg:button 
     :bottom-attachment :scale 
     :font *assistant-label-font*
     :left 516
     :left-attachment :scale 
     :name :dh-clear-button 
     :right-attachment :scale 
     :title (mln::extract-to-string *WASS-clear*)  ; jpb 1406
     :top 45 
     :top-attachment :scale
     :width 123
     :on-click 'MSW-A-clear-on-click)
   
   ;; assistant pane (only for printing)
   (make-instance 'cg:rich-edit 
     :bottom-attachment :scale 
     :font *assistant-label-font* 
     :height 288 
     :left 10
     :left-attachment :scale 
     :name :assistant 
     :right-attachment :scale 
     :top 82 
     :top-attachment :scale 
     :value "" 
     :width 625)
   
   ;;===== Master input
   ;; master input pane title
   (make-instance 'cg:static-text 
     :bottom-attachment :scale 
     :font *assistant-label-font* 
     :left 49
     :left-attachment :scale 
     :name :mi-title 
     :right-attachment :scale
     :top 396 
     :top-attachment :scale 
     :value (mln::extract-to-string *WASS-m*)) ; jpb 1406
   
   ;; master input terminate button
   (make-instance 'cg:button 
     :bottom-attachment :scale 
     :font *assistant-label-font* 
     :left 375
     :left-attachment :scale 
     :name :mi-terminate-button
     :right-attachment :scale 
     :title (mln::extract-to-string *WASS-done*)  ; jpb 1406
     :top 382
     :top-attachment :scale 
     :width 123
     :on-click 'MSW-M-done-on-click)
   
   ;; master input clear button
   (make-instance 'cg:button 
     :bottom-attachment :scale 
     :font *assistant-label-font* 
     :left 516
     :left-attachment :scale 
     :name :mi-clear-button 
     :right-attachment :scale 
     :title (mln::extract-to-string *WASS-clear*)  ; jpb 1406
     :top 382 
     :top-attachment :scale
     :width 123
     :on-click 'MSW-M-clear-on-click)
   
   ;; master input pane
   (make-instance 'cg:multi-line-editable-text 
     :bottom-attachment :scale 
     :font *assistant-label-font* 
     :height 109 
     :left 10
     :left-attachment :scale 
     :name :master
     :right-attachment :scale 
     :top 418 
     :top-attachment :scale 
     :value  (mln::extract-to-string *WASS-m-msg*) ; jpb 1406
     :width 625
     :on-change 'msw-input-on-change)
   ))

;;;-------------------------------------------------------------------- USER-CLOSE

(defMethod user-close ((win assistant-small-window))
  "when closing the window we should remove agent pointer"
  (setf (com-window (agent win)) nil)
  (call-next-method))


;;;===============================================================================
;;;
;;;                            EVENT FUNCTIONS
;;;
;;;===============================================================================

;;;===============================================================================
;;;                             ASSISTANT PANE
;;;===============================================================================

;;;---------------------------------------------------------- MSW-A-CLEAR-ON-CLICK

(defUn msw-a-clear-on-click (dialog widget)
  "clears the assistant answer pane"
  (declare (ignore widget))
  (moss::display-text dialog "" :clean-pane t)
  ;(%set-value (%find-named-item :assistant dialog) "")
  t)

;;;--------------------------------------------------------- MSW-A-SWITCH-ON-CLICK
;;; only works for ACL

(defun msw-a-switch-on-click (dialog widget)
  "switches to a full interaction window"
  (declare (ignore widget))
  (let ((agent (agent dialog))
        (pane (%find-named-item :assistant dialog))
        text win output-window conversation new-pane)
    ;; get the rich-edit pane
    (setq output-window (cg:window pane))
    ;; first copy assistant history
    (setq text (cg:rich-edit-range output-window))
    ;; then close assistant current window
    (%window-close dialog)
    ;; create standard window
    (setq win (make-master-window agent))
    ;; make it the com-window
    (setf (com-window agent) win)
    ;; get the assistant pane of the new window
    (setq new-pane (%find-named-item :assistant win))
    ;; copy history text
    (setf (cg:rich-edit-range (cg:window new-pane)) text)
    ;; since agent is a PA, update the I/O dialog channels
    ;; get the conversation object
    (if (member :converse-v2 *features*)
        (setq conversation (cadr (assoc (key agent) (conversation agent))))
      (setq conversation (conversation agent)))
    ;; update I/O channels
    (send conversation '=replace "moss-input-window" win)
    (send conversation '=replace "moss-output-window" win)
    ;; get out
    t
    ))

;;;===============================================================================
;;;                                MASTER PANE
;;;===============================================================================

;;;----------------------------------------------------------- MSW-INPUT-ON-CHANGE
;;; does not seem to work with multiple-line-editable-text
;;; this function is done by the key handler associated with the pane in MCL


(defUn msw-input-on-change (item new-value old-value)
  "Called whenever the content of the input pane (a string) changes.
   We check whether the new char is a period or a question mark 
   if so we terminate input and process it."
  (declare (ignore old-value))
  ;(format t "~&mw-input-on-change /text: ~S old-value: ~S" new-value old-value)
  ;; extract last char and text before the last char, when text is not empty
  (when (> (length new-value) 0) ; just in case (should always be true)
    (let* ((size (length new-value))
           (char (char new-value (1- size)))
           (text (subseq new-value 0 (1- size)))
           (prev (if (> size 1) (char new-value (- size 2)) '#\A))
           (dialog (cg:parent item))
           (pass-all (pass-every-char dialog))
           (agent (agent dialog))
           (language (language agent))
           )
      ;(format t "~&mw-input-on-change /new char: ~S" char)
      ;; analyse situation:
      ;; - if char is nil do nothing
      ;; - if char is question mark selet the text and call process-master-text
      ;;   adding question mark to the text
      ;; - if char is a period, then select the text and call process-master-text
      ;; - if text is a linefeed, beep ??
      ;; - otherwise do nothing.
      (cond 
       ;; when we pass all chars we nevertheless check for "?." ending the text JPB1108
       ((and pass-all
             (char-equal char '#\.)
             (char-equal prev '#\?)
             )
        ;(format t "~&mw-input-on-change /end char: ~S text ~S" char text)
        ;; select text
        (cg:set-selection item 0 (1+ (length text)))
        (assistant-process-master-text (agent dialog) (subseq text 0 (- size 2)))
        )
       ;; otherwise we quit JPB1108
       (pass-all)
       ;; a null char should not appear unless we erase area
       ((null char)) ; do nothing
       ;; if the char is a question mark, insert a space
       ;((char-equal '#\? char)
       ((member char (getf omas::*question-markers* language))
        ;; select text
        (cg:set-selection item 0 (1+ (length text)))
        ;; add a " ?" at the end of the text
        (assistant-process-master-text 
         (agent (cg:parent item)) 
         (format nil "~A ~A" text char))
        ;; reset input area
        (setf (cg:value item) ""))
       ;(concatenate 'string text " ?")))
       ;; when a period, end of the sentence (ACL does not know #\Enter!)
       ;((char-equal char '#\.)
       ((member char (getf omas::*full-stop-markers* language))
        (format t "~%; mw-input-on-change /char: ~S item: ~S" char item)
        ;; select text
        (cg:set-selection item 0 (1+ (length (cg:value item))))
        (assistant-process-master-text (agent (cg:parent item)) text)
        ;; reset input area
        (setf (cg:value item) "")
        )
       ;; why should we cg:beep on linefeed (because it is not an enter command?)
       ((char-equal '#\Linefeed char)
        (cg:beep))
       )))
  ;; return t to accept changes ?
  t)

;;;---------------------------------------------------------- MSW-M-CLEAR-ON-CLICK

(defUn msw-m-clear-on-click (dialog widget)
  "clears the master input pane"
  (declare (ignore widget))
  (let ((pane (cg:find-component :master dialog)))
    (setf (cg:value pane) "")
    (cg:set-focus-component pane)
    )
  t)

;;;----------------------------------------------------------- MSW-M-DONE-ON-CLICK
;;;********** should be reviewed seriously...

(defUn msw-m-done-on-click (dialog widget)
  "ends the input into master pane, calling the process function"
  (declare (ignore widget))
  (let* ((item (cg:find-component :master dialog))
         (text (cg:value item))
         ;; get last char
         (last-char (if (> (length text) 0) (char text (1- (length text)))))
         (short-text (if (>= (1- (length text)) 0) 
                         (subseq text 0 (1- (length text))) ""))
         (agent (agent dialog))
         (language (language agent))
         )
    ;; in case we were inputting a long text from master passing all chars,
    ;; reset flag
    (setf (pass-every-char dialog) nil)
    (cond 
     ;; a null char should not occur unless we erase area
     ((null last-char)) ; do nothing
     ;; if the char is a question mark, insert a space
     ;((char-equal '#\? last-char)
     ((member last-char (getf omas::*question-markers* language))
      ;; select text
      (cg:set-selection item 0 (1+ (length (cg:value item))))
      ;; add ? " ?" at the end of the text
      (assistant-process-master-text 
       (agent dialog) 
       (concatenate 'string short-text " ?")))
     ;; when a period end of the sentence, pass the text without it
     ;((char-equal last-char '#\.)
     ((member last-char (getf *full-stop-markers* language))
      ;; select text so the user can overwrite it
      (cg:set-selection item 0 (1+ (length (cg:value item))))
      (assistant-process-master-text (agent dialog) short-text))
     ;; otherwise pass the whole text
     (t (assistant-process-master-text (agent dialog) text))
     )
     )
  t)

(format t "~&;*** OMAS v~A - small window loaded ***" *omas-version-number*)

;;; :EOF
