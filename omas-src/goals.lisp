﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;                O M A S - G O A L S  (file goals.lisp)
;;;
;;;===============================================================================
;;; This file contains all the functions and macros needed to build goals

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| 
2001
 1003 version 1.0
      Goals are defined so that an agent can display a particular behavior.
      They are defined by using the defgoal macro, which has a number of arguments.
      Goals use scripts, inspired by the scenario mechanism of the SMAS simulator.
      Scripts represent a plan to achieve the goal. They are transformed into
      internal messages each calling a certain skill. The result of achieving
      a script step can be tested or transferred to the next step (skill).
2003
 0315 What is a goal?
      A goal in OMAS is something corresponding to a specific skill that is called
      or executed at a given time. The action is triggered only when the
      prerequisite function returns a non NIL value. I.e., the prerequisite function
      tests if the goal has been achieved (in a sense).
      A goal may be one-shot or recurrent. When recurrent it may be cyclic or 
      permanent. A permanent goal fires when the agent becomes idle.
      When a goal is set up it may have a life-time (time-limit), a status
      (waiting, active, dead), an importance and a degree of urgency. The two
      last features are not implemented.
      An example of a cyclic goal is that of a function coupled with a timer.
      When the timer fires, the function is executed. If the goal is active,
      or dead, then nothing happens, if the goal is waiting, then the
      prerequisite function is run and if it returns a non NIL value, the
      script of the goal is executed and the status of the goal is set to active.
      To be compatible with SMAS the defgoal macro must create a thread with the 
      timer associated with a goal and the corresponding handler.
 1107 version 5
 1118 Can we make a goal to send a message until it is acknowledged?
2004
 0403 ACL: adding eval-when
 0802 adding expiration-delay and activation-delay
 1104 changing execute-goal to replace static message by a function that is evaluated
      when the goal fires
 1204 adding tracing steps (vformat)
 1207 fixing make-goal
2005
 1001 adding a print-self method for instances
2007
 0105 adding a default enabling function agent-goal-always-true
2008
 0720 removing activation-change-function
 1228 declaring activation-change-function in defgoal...
2009
 0128 removing the :activation-change-fcn parameter from def-goal
2010
 0618 noop -> omas-noop
2011
 0817 correcting bug in agent-run-goal, kill goal when 1-shot and not enabled
 0824 adding possibility of arguments to the script function
2014
 0307 -> utf8
2018
 1116 modified the agent-run-goal function to introduce permanent goals
|#


;;;=================================== GOALS =====================================
#|
A goal is an object having a number of properties :
GOAL
   description           (name)
   type                  (permanent, cyclic, one-shot,...)
   mode                  (rigid, flexible/allows activation level mechanism)
   period                (period for a cyclic goal)
   expiration-date       (date at which it dies)
   expiration-process    (process that will kill the goal on expiration date)
   importance            (high, medium, low)
   urgency               (high, medium, low)
   status                (waiting, active, dead,...) may not be useful with activation
   activation-date       (date at which the goal should fire)
   activation-level      (on a 0-100 scale)
   activation-threshold  (value above which the goal is activate)
   activation-change-fcn (fcn that change the activation level at each cycle)
   goal-enable-fcn       (function that checks for complex enabling conditions)
   scripts               (list of scripts, currently a request message)
   
|#

(in-package :omas)


(defClass goal ()
  ((name :accessor name :initform nil :initarg :name)
   (mode :accessor mode :initform :flexible :initarg :mode)
   (type :accessor type! :initform nil :initarg :type)
   (period :accessor period :initform 20 :initarg :period)
   (expiration-date :accessor expiration-date :initform nil :initarg :expiration-date)
   (expiration-delay :accessor expiration-delay :initform nil
                     :initarg :expiration-delay)
   (expiration-process :accessor expiration-process :initform nil 
                       :initarg :expiration-process)
   (importance :accessor importance :initform nil :initarg :importance)
   (urgency :accessor urgency :initform nil :initarg :urgency)
   (activation-date :accessor activation-date :initform nil
                    :initarg :activation-date)
   (activation-delay :accessor activation-delay :initform 0
                     :initarg :activation-delay)
   (activation-level :accessor activation-level :initform 0 
                     :initarg :activation-level)
   (activation-threshold :accessor activation-threshold :initform 101
                         :initarg :activation-threshold)
   (activation-change-fcn :accessor activation-change-fcn :initform nil
                          :initarg :activation-change-fcn)
   (status :accessor status :initform nil :initarg :status)
   (goal-enable-fcn :accessor goal-enable-fcn :initform nil 
                    :initarg :goal-enable-fcn)
   (script :accessor script :initform nil :initarg :script)
   (script-args :accessor script-args :initform nil :initarg :script-args)
   )
  (:documentation "a goal is something that is installed in the self part of the ~
                   agent, and that is used to control its behavior."))

;;; define a default method for printing a goal
(defMethod print-object ((ss goal) stream)
  (format stream "#<GOAL ~A: ~A AD:~S ED:~S AL:~A AT:~A S:~A>"
          (name ss) (type! ss)(activation-date ss)(expiration-date ss)
          (activation-level ss)(activation-threshold ss)(status ss)
          ))

(defMethod print-self ((self omas::goal) &optional (stream t))
  (let ((slot-list (class-instance-slots (find-class 'omas::goal))))
    (dolist (slot slot-list)
      ;; print content of slots
      (format stream "~%~S: ~S" (car slot) (slot-value self (car slot))))
    :done))

;;;================================== SCRIPTS ====================================
#|
defgoal is a macro that builds a goal structure.
When the goal status is :dead does not do anything else. Otherwise sets up a
new thread (process) for executing the goal.
If an activation date is given, then it considers it as a delay expressed in 
seconds before the goal is activated.
If an expiration date is given, then it considers it as a delay in seconds. It will
set a timer to kill the goal after the corresponding delay.
If the goal is periodic then the period is assumed to be in seconds (default is 10
seconds).
|#

;;;----------------------------------------------------------------------- DEFGOAL

(defMacro defgoal (goal-name agent-name 
                             &key (mode :rigid) 
                             (type :1-shot) 
                             period 
                             expiration-date 
                             expiration-delay
                             importance 
                             urgency 
                             activation-date
                             (activation-delay 0)
                             (activation-level 50)
                             (activation-threshold 50)
                             (status :waiting)
                             (goal-enable-fcn 'agent-goal-always-true)
                             script
                             script-args)
  "macro that builds a goal structure for an agent.
Arguments:
   goal-name:                 name of the goal that will be used to create task-ids
   agent-name:                name of the agent to receive the goal
keys arguments:
   mode:                      activation mode (:rigid or :flexible)
   type:                      type of goal (:permanent :cyclic :1-shot)
   period:                    period for cyclic goals (default is 20)
   expiration-date:           date at which the goal dies
   expiration-delay:          time to wait until we kill the goal
   importance:                on a 1-100 scale
   urgency:                   on a 1-100 scale
   activation-date:           date at which the goal should fire (default is now)
   activation-delay:          time to wait before activating the goal
   activation-level:          on a 1-100 scale (default 50)
   activation-threshold:      on a 1-100 scale (default 50)
   status:                    waiting, active, dead,... may not be useful with activation
   goal-enable-fcn:           function checking the goal enabling conditions
   script:                    goal function, must return a list of messages
   script-args                eventual list of arguments to the script function
Return:
   goal-name."
  (declare (special activation-change-fcn))
  `(make-goal ',goal-name ',agent-name :mode ',mode :type ',type :period ',period
              :expiration-date ',expiration-date :expiration-delay ',expiration-delay
              :importance ',importance :activation-delay ',activation-delay
              :urgency ',urgency :activation-date ',activation-date 
              :activation-level ',activation-level
              :activation-threshold ',activation-threshold 
              :status ',status
              :goal-enable-fcn ',goal-enable-fcn 
              :script ',script :script-args ',script-args
              ))

#|
(defgoal :send-action :boss :script send-action)
(MAKE-GOAL ':SEND-ACTION
           ':BOSS
           :MODE
           ':RIGID
           :TYPE
           ':1-SHOT
           :PERIOD
           'NIL
           :EXPIRATION-DATE
           'NIL
           :EXPIRATION-DELAY
           'NIL
           :IMPORTANCE
           'NIL
           :ACTIVATION-DELAY
           '0
           :URGENCY
           'NIL
           :ACTIVATION-DATE
           'NIL
           :ACTIVATION-LEVEL
           '50
           :ACTIVATION-THRESHOLD
           '50
           :ACTIVATION-CHANGE-FCN
           'NIL
           :STATUS
           ':WAITING
           :GOAL-ENABLE-FCN
           'AGENT-GOAL-ALWAYS-TRUE
           :SCRIPT
           'SEND-ACTION)
|#
;;;--------------------------------------------------------------------- MAKE-GOAL

(defUn make-goal (goal-name agent-name &key (mode :rigid) 
                            (type :1-shot)
                            (period 10)
                            expiration-date 
                            expiration-delay
                            importance 
                            urgency
                            activation-date
                            (activation-delay 0)
                            (activation-level 50) 
                            (activation-threshold 50)
                            (status :waiting)
                            (goal-enable-fcn 'agent-goal-always-true)
                            script
                            script-args)
  "macro that builds a goal structure for an agent.
Arguments:
   goal-name:                 name of the goal that will be used to create task-ids
   agent-name:                name of the agent to receive the goal
keys arguments:
   mode:                      activation mode (:rigid or :flexible)
   type:                      type of goal (:permanent :cyclic :1-shot)
   period:                    period for cyclic goals (default is 20)
   expiration-date:           date at which the goal dies
   expiration-delay:          time to wait until we kill the goal
   importance:                on a 1-100 scale
   urgency:                   on a 1-100 scale
   activation-date:           date at which the goal should fire (default is now)
   activation-delay:          time to wait before activating the goal
   activation-level:          on a 1-100 scale (default 50)
   activation-threshold:      on a 1-100 scale (default 50)
   status:                    waiting, active, dead,... may not be useful with activation
   goal-enable-fcn:           function checking the goal enabling conditions (default: function returning t)
   script:                    goal function, must return a list of messages
   script-args                eventual list of arguments to the script function
Return:
   goal-name."  
  (when (not (boundp agent-name))
    (warn "~&*** Warning: unbound agent name: ~S, when attempting to define ~
           the ~S goal." agent-name goal-name)
    (return-from make-goal nil))
  ;; executing defgoal automatically redefines the goal. Thus, we first clean it
  ;; just in case we had no dynamic part and we decided to add one
  (let* ((agent (%agent-from-key (keywordize agent-name)))
         goal process exp-delay)
    ;; compute expiration-delay 
    (setq exp-delay
          (cond
           ;; if specified and delay is specified take the max
           ((and expiration-date expiration-delay) 
            (max expiration-delay
                 (- expiration-date (get-universal-time))))
           ;; if delay is not specified keep date
           (expiration-date
            (max 0 (- expiration-date (get-universal-time))))
           ;; if delay is specified, then use it
           (expiration-delay)
           ;; otherwise, leave it to nil
           ))
    (vformat "~&;=== make-goal; exp-delay: goal expires in ~S seconds" exp-delay)
    ;; OK. Now we create a new goal object
    (setq goal 
          (make-instance 'goal
            :name goal-name
            :mode mode
            :type type
            :period period
            :expiration-delay exp-delay
            :importance importance
            :urgency urgency
            :activation-date activation-date
            :activation-delay activation-delay
            :activation-level activation-level
            :activation-threshold activation-threshold
            :status status
            :goal-enable-fcn goal-enable-fcn
            :script script
            ))
    
    (vformat "~&;= make-goal; time now: ~S; activation-date: ~S; activation-delay: ~S; ~
              activation-time: ~S; setting goal process."
             (time-string (get-universal-time))
             (time-string (or activation-date (get-universal-time)))  ;JPB0412
             activation-delay
             (time-string (+ (or activation-date (get-universal-time)) ;JPB0412
                             activation-delay)))
    
    (%agent-add agent goal :goals)
    ;; launch the process for running the goal
    (setq process 
          (acl/mcl-process-run-function
           (concatenate 'string (symbol-name agent-name)
             "-" (symbol-name goal-name))
           #'agent-run-goal agent goal))
    ;; if there is an expiration date (delay) then set-up a timer to kill the
    ;; goal process
    (when (and (numberp exp-delay) (> exp-delay 0))
      (vformat "~&;= make-goal; time now: ~S; setting up expiration process, delay: ~S" 
               (time-string (get-universal-time)) exp-delay)
      (setf (expiration-process goal)
        (acl/mcl-process-run-function 
         (concatenate 'string (symbol-name agent-name)
           "-" (symbol-name goal-name) "-time-limit")
         #'agent-kill-goal-on-time-limit 
         agent goal process 
         exp-delay)))
    (vformat "~&;= make-goal; time now: ~S; expiration process set." 
             (time-string (get-universal-time)))
    
    ;; create the MOSS representation of goals
    (make-moss-goal agent goal-name)
    
    ;; return the name of the goal
    goal))

;;;---------------------------------------------------------------- MAKE-MOSS-GOAL

(defMethod make-moss-goal ((agent agent) goal &aux goal-id agent-id)
  "creates a MOSS skill concept if needed. Create an instance of a skill concept 
   in the knowledge base of the agent. Adds the skill to the MOSS agent object 
   representing the agent.
Arguments:
   agent: agent
   goal: symbol or keyword"
  (let ((goal-name (symbol-name goal)))
    ;; check first if there is already an agent MOSS object
    (setq agent-id (moss-ref agent))
    ;; if null problem
    (unless agent-id
      (warn "no agent MOSS representation for agent ~S." agent)
      (return-from make-moss-goal nil))
    
    ;; otherwise we have agent and skill models
    ;; if already recorded don't do anything
    (when (access `("omas-agent" ("ag-name" :is ,(symbol-name (key agent)))
                    ("ag-goal" ("omas-goal" ("gl-name" :is ,goal-name)))))
      (return-from make-moss-goal :already-there))
    
    ;; create then the instance of skill
    ;; this can create it twice when redefining an agent
    ;; however the database could not be created yet
    
    ;;************ We must record it in case of a persistent agent
    ;; ? agents are reloaded at each session
    (setq goal-id (moss::%make-instance "omas-goal" (list "gl-name" goal-name)))
    ;; add the skill to the agent object  
    (moss::send agent-id 'moss::=add-related-objects "ag-goal" (list goal-id))
    ;; return id
    (list agent-id goal-id)))

;;;------------------------------------------------------------------ EXECUTE-GOAL

(defUn agent-execute-goal (agent goal)
  "execute the content of a goal, i.e. takes the script and puts it into the input ~
   box. 
   Evaluation of the script should return a list of object messages.
Arguments:
   agent: agent
   goal: the goal structure"
  (let ((script (script goal))
        (args (script-args goal))
        message-list)
    (setq message-list (apply script agent args))
    (vformat "~&;=== agent-execute-goal; time now: ~S; ~&;= message-list: ~S" 
             (time-string (get-universal-time)) message-list)
    (unless (every #'(lambda (xx) (typep xx 'message)) message-list)
      (error "evaluating the script of goal ~S for agent ~S should yield a message ~
              list rather than ~&  ~S"
             (name goal) (name agent) message-list))
    ;; first set the goal status
    (setf (status goal) :active)
    ;; add messages to the input-message-box, setting the proper date
    (dolist (message message-list)
      ;; then move the message into the proper box
      ;; we set the date to current time
      (setf (date message) (get-universal-time))
      ;; task-id is necessary. If none, create one
      (unless (task-id message)
        (setf (task-id message)(create-task-id)))
      ;; this is not a public message and does not go onto the net
      (%agent-add agent message :input-messages))
    ;;update agent's display to update input-message box
    (agent-display agent)
    :done))

;;;-------------------------------------------------------- AGENT-GOAL-ALWAYS-TRUE

(defUn agent-goal-always-true (&rest dummy)
  "default function that allows a goal to be run by always returning true.
Arguments:
   dummy (rest): arguments are unimportant
Return:
   t"
  (declare (ignore dummy))
  t)

;;;--------------------------------------------------------------- AGENT-KILL-GOAL

(defUn agent-kill-goal (agent goal)
  "killing a goal, i.e., we set its status to :dead and kill the corresponding process.
   The function is usually called from a task process.
Arguments:
   agent: agent
   goal: goal id
Return:
   :goal-killed"
  (vformat "~&;=== agent-kill-goal; time now: ~S; agent: ~S; goal: ~S"
           (time-string (get-universal-time)) (omas::name agent) (omas::name goal))
  ;; set goal to :dead
  (setf (status goal) :dead)
  ;; remove the goal from the goal list
  (setf (goals agent) (delete goal (goals agent)))
  ;; kill timer process if it exists
  (when (expiration-process goal)
    (agent-process-kill agent (expiration-process goal)))
  :goal-killed)

;;;------------------------------------------------- AGENT-KILL-GOAL-ON-TIME-LIMIT

(defUn agent-kill-goal-on-time-limit (agent goal process delay)
  "waiting until the time-limit is reached, we then set the goal status to :dead and kill ~
   the corresponding process (our process).
Arguments:
   agent: agent
   goal: goal id
   process: process id
   delay: delay to wait before killing the process (seconds)
Return:
   The timer kills itself when it fires. The function never returns."
  (vformat "~&;=== agent-kill-goal-on-time-limit; time now: ~S; agent: ~S; goal: ~S; ~
            delay: ~S; setting wait loop"
           (time-string (get-universal-time))(omas::name agent) (omas::name goal) delay)
  (acl/mcl-process-wait-with-timeout 
   (concatenate 'string (symbol-name (name agent)) "-" (symbol-name (name goal)) 
                "-time-limit-wait")
   delay #'omas-noop)
  (vformat "~&;= agent-kill-goal-on-time-limit; time nos: ~S; agent: ~S; goal: ~S"
           (time-string (get-universal-time)) (omas::name agent) (omas::name goal))
  ;; set goal to :dead
  (setf (status goal) :dead)
  ;; kill goal process
  (agent-process-kill agent process)
  ;; remove the goal from the goal list
  (setf (goals agent) (delete goal (goals agent)))
  ;; kill timer process (our own process)
  (agent-process-kill-current-process agent)
  )

;;;---------------------------------------------------------------- AGENT-RUN-GOAL
;;; the function is in a separate process
;;; we loop until some condition make us quit

;;; algorithm
;;; Loop
;;; 1. if the goal is dead, return
;;; 2. if there is an enabling function and the goal is suspended, wait using the
;;;    enabling function
;;; 3. if there is a delay, wait
;;; 4. if goal is 1-shot, execute it and return
;;; 5. if the goal is periodic (cyclic)
;;;    - execute the function
;;;    - reset the delay to 0
;;;    - wait on the periodic time
;;; 6. if the goal is permanent, then wait on the value of delay that can be 
;;;    changed by the enabling function

(defUn agent-run-goal (agent goal)
  "function that tries to run a goal"
  (let (delay fn ok-flag)
    ;; compute delay from activation date
    ;; activation-date is absolute
    ;; activation-delay runs from the time the goal is installed
    (setq delay 
          (max 0 (- (or (activation-date goal) 0) (get-universal-time))
               (activation-delay goal)))
    (vformat "~&;=== agent-run-goal; time now: ~S; delay: ~S seconds" 
             (time-string (get-universal-time)) delay)
    ;; then wait - maybe could use (sleep delay)
    (when (> delay 0) (sleep delay))
    
    ;; first check the enabling function
    (setq fn (goal-enable-fcn goal))
    (vformat "~&;= agent-run-goal: time now: ~S; goal-enable-fcn: ~S" 
             (time-string (get-universal-time)) fn)
    
    ;; prepare a loop in case the goal is periodic
    (loop
      ;; if the goal is dead, then give up
      (when (eql :dead (status goal))
        (return-from agent-run-goal nil))
      (setq ok-flag
            (if fn 
                (apply fn agent goal nil)
              ;; when no function we assume that we can execute the goal
              t))
      ;; if the result is nil (goal is suspended) wait until it becomes active 
      (unless ok-flag
        (acl/mcl-process-wait  
         (concatenate 'string (symbol-name (name agent)) "-" 
           (symbol-name (name goal)) "-period")
         fn agent goal)  ; given as seconds
        )
      
      ;; when result is not nil then activate the goal 
      ;; should be activated if not already running: i.e., if there is not a
      ;; task executing the goal
      (agent-execute-goal agent goal)
      
      ;; if one-shot goal then give up (pb with type?)
      (case (type! goal) 
        (:1-shot
         ;; mark goal as dead (maybe we should remove it) 
         (setf (status goal) :dead)
         ;; return, which kills the current process
         (return-from agent-run-goal nil)
         )
        
        ;; when goal is cyclic wait for next event, using period
        ((:cyclic :periodical)
         (vformat "~&;= agent-run-goal: time now: ~S; waiting: ~S seconds" 
                  (time-string (get-universal-time)) 
                  (period goal))
         (sleep (period goal))
         )
        
        ;; when goal is permanent, we wait on the delay presumably set up by the
        ;; enabling function
        (:permanent
         (vformat "~&;= agent-run-goal: time now: ~S; waiting: ~S seconds" 
                  (time-string (get-universal-time)) 
                  (activation-delay goal))
         (sleep (or (activation-delay goal) 0.01)) ; safety is 1/100s
         )
        )
      ) ; end of cyclic loop
    ))


#|
How does this one works
1. Assume we have a 1-shot goal, with delay (not suspended, not dead)
 - the function waits for <delay> time
 - then enters the loop
   - gets the enabling function
   - executes the goal
   - sets the goal status to :dead and kills the process
2. Assume we have a cyclic goal (not suspended, not dead)
 - the first time around
   - the function waits for <delay> time
   - then enters the loop
   - executes the goal
   - waits for <period> time
 - go back to start of the loop
   - check suspension function
   - ...
3. Assume we have a :permanent goal (not suspended, not dead)
 - wait for <delay> time
 - enter the loop
 - get enabling function
 - execute goal 
 - kill the process (thus equivalent to 1-shot goal)

(defUn agent-run-goal (agent goal)
  "function that tries to run a goal"
  (let (delay fn ok-flag)
    ;; compute delay from activation date
    ;; activation-date is absolute
    ;; activation-delay runs from the time the goal is installed
    (setq delay 
          (max 0 (- (or (activation-date goal) 0) (get-universal-time))
               (activation-delay goal)))
    (vformat "~&;=== agent-run-goal; time now: ~S; delay: ~S seconds" 
             (time-string (get-universal-time)) delay)
    ;; then wait - maybe could use (sleep delay)
    (when (> delay 0)
      (acl/mcl-process-wait-with-timeout 
       (concatenate 'string (symbol-name (name agent)) "-" 
                    (symbol-name (name goal)) "-wait")
       delay
       #'omas-noop)
      )
    
    ;; prepare a loop in case the goal is periodic
    (loop
      ;; if the goal is dead, then give up
      (when (eql :dead (status goal))
        (return-from agent-run-goal nil))
      ;; first check the enabling function
      (setq fn (goal-enable-fcn goal))
      (vformat "~&;= agent-run-goal: time now: ~S; goal-enable-fcn: ~S" 
               (time-string (get-universal-time)) fn)
      (setq ok-flag
            (if fn 
              (apply fn agent goal nil)
              ;; when no function we assume that we can execute the goal
              t))
      ;; when result is not nil then activate the goal 
      (when ok-flag
        ;; should be activated if not already running: i.e., if there is not a
        ;; task executing the goal
        (agent-execute-goal agent goal)
        ;; if one-shot goal then give up (pb with type?)
        (unless (member (type! goal)'(:cyclic :periodical))
          ;; mark goal as dead (maybe we should remove it) 
          (setf (status goal) :dead)
          ;; kill the current process
          (agent-process-kill-current-process agent)
          ;; does not return since killed...
          ))
      ;; if goal is 1-shot but enabling function is nil, then too bad, we kill it
      (unless (member (type! goal)'(:cyclic :periodical))
        (setf (status goal) :dead)
        (agent-process-kill-current-process agent))
      
      ;; otherwise reset timer
      ;; here we should change the name of the task in the goal message
      ;; to avoid confusions 
      (vformat "~&;= agent-run-goal: time now: ~S; waiting: ~S seconds" 
               (time-string (get-universal-time)) 
               (period goal))
      (acl/mcl-process-wait-with-timeout 
       (concatenate 'string (symbol-name (name agent)) "-" 
                    (symbol-name (name goal)) "-period")
       (period goal)  ; given as seconds
       #'omas-noop)
      ) ; end of cyclic loop
    ))
|#


(format t "~&;*** OMAS v~A - goals loaded ***" *omas-version-number*)

;;; :EOF