﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;            O M A S - I N F E R E R (inferer.lisp)
;;;
;;;===============================================================================
;;; This file contains all functions related to making an inferer agent (IA), i.e. 
;;; an agent controlled by production rules rather than by skill Lisp functions

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| 
The inferer has predefined skills:
   :ask for answering a question
   :tell for acuiring data
   :start for starting the inference process

Inference is done by using unification (prolog style) and returning answers messages.

Format of input messages
   ((:data . <set of clauses>){(:language . <tag>}{(:pattern . <pattern>})

2020
 0202 restructuring OMAS as an asdf system
|# 

(in-package :omas)

;;;================================== Globals ====================================

(defparameter *rules* nil)

;;;============================= macros ==========================================
;;; macro for defining set methods

(defMacro def-ia-set (prop sub-object-name)
  `(defMethod (setf ,prop) (val (self inferer) )
     (let ((item (,sub-object-name self)))
       (if item
         (setf (,prop item) val)
         (error ,(concatenate 'string "non existing "
                              (symbol-name sub-object-name)
                              " part in inferer ~S") (name self)))
       val)))

;;; same with no check

(defMacro def-ia-set-no-check (prop sub-object-name)
  `(defMethod (setf ,prop) (val (self inferer) )
     (setf (,prop (,sub-object-name self)) val)
     ))

#|
;;; Example of use: (def-xa-set input-messages comm)
;;; should produce

(defmethod (setf output-messages) (val (self postman) )
  (let ((item (comm self)))
    (if item
        (setf (output-messages item) val)
      (error "non existing COMM part in postman ~S" (name self))))
  val)
|#

;;; macro to extend the method applying to a subpart of an agent, so that it can
;;; be applied to the agent as a whole. Includes the definition for defsetf

(defMacro extend-method-for-inferer (method subobject)
  `(progn
     (defMethod ,method ((self inferer))
       (,method (,subobject self)))
     (def-ia-set-no-check ,method ,subobject)))

;;;=================================== Class =====================================

(defClass inferer (agent)
  ((inference :accessor inference :type inference :initarg :inference))
  (:documentation "agent for making inferences using rules"))

(defClass inference ()
  ((rule-set :accessor rule-set :initarg :rule-set :initform nil)
   (facts :accessor facts :initform nil)
   (reasoning :accessor reasoning :initform nil)
   )
  (:documentation "object containing the inference parameters"))

;;; Extending methods from parts of an agent to apply to the agent as a whole.

;--- INFERENCE
(extend-method-for-inferer rule-set inference)
(extend-method-for-inferer facts inference)
(extend-method-for-inferer reasoning inference)

;;;=============================== Creating functions ============================

;;;------------------------------------------------------------------ MAKE-POSTMAN

(defUn make-inferer (name &key redefine hide learning
                          (package *package*) 
                          (language (default-language *omas*)) 
                          (context (or moss::*context* 0)) 
                          (version-graph (or moss::*version-graph* '((0)))))
  "creates an agent, puts it onto the *agents* list, name must be an unused symbol ~
   unless the agent is to be redefined with the option (:redefine t).
Arguments:
   name: name given to the agent (e.g. JEAN or :JEAN)
   redefine: (key) if t allows the agent to be redefined
   protocol (key): protocol to use for sending the message
   destination (key): IP address of the gateway or the target computer
   port (key): integer specifying the port for sending and receiving the messages
   context (key): context (version) of the ontology
   version-graph (key): version (configuration) graph of the ontology
   package (key): default current (package object) or keyword
Return:
   the agent internal CLOS object reference."
  (let (key)
    ;; chek if name is a symbol or a keyword, then set name to be a symbol
    ;; and key the corresponding keyword
    (cond
     ((keywordp name)
      (setq key name
            name (create-inferer-name name package)))
     ((symbolp name)
      (setq key (intern (symbol-name name) :keyword)
            name (create-inferer-name name package)))
     (t (error "the name ~A for the agent must be a symbol or a keyword." name)))
    (cond
     ;; here name is a symbol
     ((and (boundp name) (null redefine))
      (error "name ~A for the agent is bound to ~A and the :redefine option is ~
              not set in the defagent macro." name (symbol-value name)))
     ((not (boundp name))
      ;; here name is not bound we must declare it as a global variable
      (eval `(defVar ,name)))
     (t 
      ;; here agent was defined (name is bound) and is being redefined. First
      ;; we must kill the old agent
      (agent-destroy (symbol-value name)))
     )
    
    (export (list name))
    
    ;; then we create a whole new agent
    ;; ***** we should add ontologies
    (let ((comm (make-instance 'comm))
          (control (make-instance 'control))  ; default status is :idle
          (self (make-instance 'self))
          (world (make-instance 'world))
          (ontologies (make-instance 'ontologies))
          (appearance (make-instance 'appearance))
          (tasks (make-instance 'tasks))
          (inference (make-instance 'inference))
          agent process)
      ;; create agent structure
      (setq agent (make-instance 'INFERER 
                    :name name
                    :key key
                    :comm comm
                    :control control
                    :self self
                    :ontologies ontologies
                    :world world
                    :tasks tasks
                    :appearance appearance
                    :inference inference
                    ))

      ;; store a handle for load-agent (will stay there if agent loaded manually)
      (setf (agent-being-loaded omas::*omas*) agent) ; JPB 0912

      ;; create MOSS classes and method proxy if not already there JPB 0912
      ;; create a MOSS structure representing the agent
      ;; second argument is nil since postmen are not persistent
      (agent-set-moss-environment agent nil)
      
      ;; save global info into agent structure         
      (setf (ontology-package agent) package)
      (setf (language agent) language)
      (setf (moss-context agent) context)
      (setf (moss-version-graph agent) version-graph)
      (set name agent)
      ;; add default skills (used essentially by PAs (JPB 051001)
      (agent-add-default-skills agent)
      
      ;; when learning is present we add it to the agent features
      (when learning (%agent-add agent :learning :features))
      
      ;; and add the name to the *local-agents* list if not there
      (unless (assoc key (local-agents *omas*))
        (setf (local-agents *omas*) 
              (append (local-agents *omas*) (list (cons key agent)))))
      (if hide (setf (get key :hide) t))
      ;; add name to names of known agents
      (pushnew name (names-of-known-agents *omas*))
      
      ;; launch agent process working on the agenda
      (setq process (agent-process-create-restartable
                     agent
                     '(:type :main) ; no task being executed, process is main 
                     (agent-make-process-name agent "main" :fixed-name t)
                     #'agent-process-task 
                     agent))
      (setf (process agent) process)
      ;; launch then scanner process on input-messages queue
      (setq process (agent-process-create-restartable
                     agent
                     '(:type :scan) ; no task being executed, process is scan
                     (agent-make-process-name agent "scan" :fixed-name t)
                     #'agent-scan-messages 
                     agent))
      (setf (mbox-process agent) process)

      ;; add specific skills
      (make-skill :infer-ask key :static-fcn 'infer-ask-static)
      (make-skill :infer key :static-fcn 'infer-start-static)
      (make-skill :infer-reset key :static-fcn 'infer-reset-static)
      (make-skill :infer-tell key :static-fcn 'infer-tell-static)
      (make-skill :hello key :static-fcn 'hello-static)

      ;; we update the control-panel
      #+MCL
      (omas-window-update-agent-list)
      
      ;; should initialize the database by exploding the MOSS upper ontology and
      ;; knowledge base, to allow meta reasoning
      ;; then the ontology must be exploded too
      agent)))

;;;-------------------------------------------------------------------- DEFINFERER

(defMacro definferer (name &rest ll)
  "creates an agent, puts it onto the *agents* list, name must be an unused symbol.
Arguments:
   name: name given to the agent (e.g. MUL-3)
   ll (rest): options of make-inferer.
Return:
   the agent identifier"
  `(apply #'make-inferer ',name ',ll))

;;; ==================================== macros ==================================

(defMacro definfrule (name &key if then)
  `(let ((agent (agent-being-loaded omas::*omas*)))
     (pushnew '(,name ,@if -> ,then) (rule-set agent) :test #'equal)))

;;; =================================== functions ================================

;;;----------------------------------------------------------------------- ACTIONS

(defUn actions (rule)
  "Find the action part of a rule. It follows the symbol ->"
  (cdr (member '-> rule)))

;;;------------------------------------------------------------------- APPLICABLE?

(defUn applicable? (rule-name premisses facts old-rules)
  "Tests if rule was not already applied, and, if not, checks if premisses
   are contained in the facts"
  (unless (member rule-name old-rules)
    (dolist (xx premisses)
      (unless (member xx facts :test #'equal)(return-from applicable? nil)))
    t))

;;;-------------------------------------------------------------------- CONCLUSION

(defUn conclusion (facts)
  "Conclusion if any is associated with the label action"
  (cdr (assoc :action facts)))

;;;--------------------------------------------------------------------- PREMISSES

(defUn premisses (rule)
  "They are recorded between the name of the rule and the -> symbol"
  (reverse (cdr (member '-> (reverse (cdr rule))))))

;;;------------------------------------------------------------------------ REASON

(defUn reason (rules facts)
  "Inference engine that takes two arguments:
     rules: a list of rules
     facts: a list of dotted pairs representing data"
  (let (flag 
        applied-rules) ; will hold applied rules to prevent infinite loops
    (loop ; main loop - loop until no more rule applies
      ;; for each rule in the list
      (dolist (rule rules)
        ;(print (list 'Trying (car rule) 'applied applied-rules))
        (when (applicable? (car rule) (premisses rule) facts applied-rules)
          (setq flag t) ; note that one rule at least applied
          (push (car rule) applied-rules) ; record applied rule
          ;(print (list 'applying (car rule)))
          ;; merge the action part of the rule into facts
          (setq facts (append (actions rule) facts))
          ;(print (list 'facts facts))
          ))
      ;; outside loop, test if at least one rule was applied
      (if flag 
        (setq flag nil) ; reset flag before going around the loop once again
        (return-from reason (conclusion facts)) ; return the conclusion
        ))))

;;;==============================================================================
;;;                        Default Generic Skills
;;;==============================================================================

;;; Methods are added by agent-add-default-skills
;;; The agent skills are:
;;;   HELLO
;;;   INFER-ASK
;;;   INFER-START
;;;   INFER-RESET
;;;   INFER-TELL

;;;======================================================================== HELLO

;;;----------------------------------------------------------------- hello-static

;;;(defUn static-hello (self message &rest ll)
;;;  "greeting function"
;;;  (declare (ignore message ll))
;;;  (static-exit self "Hello there..."))

;;;======================================================================== INFER

;;;----------------------------------------------------------- infer-start-static

(defUn infer-start-static (agent message)
  "asks the agent to start an inference.
Arguments:
   agent: agent
   message: incoming message
   args: an a-list, e.g. ((:data <clause>))
Return
   result or :failure"
  (declare (ignore message))
  (let (result)
    (setq result (reason (rule-set agent) (facts agent)))    
    (static-exit agent (or result :failure))))

;;;==================================================================== INFER-ASK

;;;------------------------------------------------------------- infer-ask-static

(defUn infer-ask-static (agent message &rest args)
  "provides data and ask for inference.
Arguments:
   agent: agent
   message: incoming message
   args: an a-list, e.g. ((:data <clause>))
Return
   result or :failure"
  (declare (ignore message))
  (let (result)
    ;; add to fact base
    (dolist (clause (cdr (assoc :data args)))
      (pushnew clause (facts agent) :test #'equal))
    (setq result (reason (rule-set agent) (facts agent)))    
    (static-exit agent (or result :failure))))

;;;================================================================== INFER-RESET

;;;----------------------------------------------------------- infer-reset-static

(defUn infer-reset-static (agent message)
  "asks the agent to clean facts area.
Arguments:
   agent: agent
   message: incoming message
   args: an a-list, e.g. ((:data <clause>))
Return
   result or :failure"
  (declare (ignore message))
  (setf (facts agent) nil)
  (static-exit agent :done))

;;;=================================================================== INFER-TELL

;;;------------------------------------------------------------ infer-tell-static

(defUn infer-tell-static (agent message &rest args)
  "asks the agent to add clauses to facts area.
Arguments:
   agent: agent
   message: incoming message
   args: an a-list, e.g. ((:data <clause>))
Return
   result or :failure"
  (declare (ignore message))
  (dolist (clause (cdr (assoc :data args)))
    (pushnew clause (facts agent) :test #'equal))
  (static-exit agent :done))

(format t "~&;*** OMAS v~A - inferer loaded ***" *omas-version-number*)

;;; :EOF