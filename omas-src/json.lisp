﻿;;;-*- Mode: Lisp; Package: "YASON" -*-
;;;===============================================================================
;;;20/02/02
;;;                O M A S - J A S O N  (file json.lisp)
;;;
;;;===============================================================================
;; This file is includes part of yason, a Common Lisp JSON parser/encoder

#|
Copyright (c) 2008-2019 Hans Huebner and contributors
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

  - Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

  - Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.

  - Neither the name BKNR nor the names of its contributors may be
    used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#

;;; this file is not loaded automatically with OMAS and must be loaded by the 
;;; agent program if needed

#|
2013
 1003 Adding Marcio's functions
2014
 0201 Changing some encode functions to get strings in lower case and numbers as
      strings
 0307 -> utf8
2020
 0202 restructuring OMAS as an asdf system
|#

(push :json *features*)

(defpackage :yason

  (:use :cl)

  (:nicknames :json)

  (:export
   ;; Parser
   #:parse
   #:*parse-object-key-fn*
   #:*parse-object-as-alist*
   #:*parse-json-arrays-as-vectors*
   #:*parse-json-booleans-as-symbols*

   #:true
   #:false

   ;; Basic encoder interface
   #:encode
   #:encode-slots
   #:encode-object
   #:encode-plist
   #:encode-alist

   ;; Streaming encoder interface
   #:with-output
   #:with-output-to-string*
   #:no-json-output-context
   #:with-array
   #:encode-array-element
   #:encode-array-elements
   #:with-object
   #:encode-object-element
   #:encode-object-elements
   #:with-object-element
   #:with-response

   ))


(in-package :yason)

(defvar *json-output*)

(defgeneric encode (object &optional stream)

  (:documentation "Encode OBJECT to STREAM in JSON format.  May be
  specialized by applications to perform specific rendering.  STREAM
  defaults to *STANDARD-OUTPUT*."))

;; from alexandria
(defun plist-hash-table (plist &rest hash-table-initargs)
  "Returns a hash table containing the keys and values of the property list
PLIST. Hash table is initialized using the HASH-TABLE-INITARGS."
  (let ((table (apply #'make-hash-table hash-table-initargs)))
    (do ((tail plist (cddr tail)))
        ((not tail))
      (setf (gethash (car tail) table) (cadr tail)))
    table))

(defparameter *char-replacements*
  (plist-hash-table
   '(#\\ "\\\\"
     #\" "\\\""
     #\/ "\\/"
     #\Backspace "\\b"
     #\Page "\\f"
     #\Newline "\\n"
     #\Return "\\r"
     #\Tab "\\t")))

(defmethod encode ((string string) &optional (stream *standard-output*))
  (write-char #\" stream)
  (dotimes (i (length string))
    (let* ((char (aref string i))
           (replacement (gethash char *char-replacements*)))
      (if replacement
          (write-string replacement stream)
          (write-char char stream))))
  (write-char #\" stream)
  string)


(defmethod encode ((object rational) &optional (stream *standard-output*))
  (encode (float object) stream)
  object)

(defmethod encode ((object double-float) &optional (stream *standard-output*))
  (encode (coerce  object 'single-float) stream)
  object)

;;;(defmethod encode ((object float) &optional (stream *standard-output*))
;;;  (princ object stream)
;;;  object)

;;; changed by jpb 140201
(defmethod encode ((object float) &optional (stream *standard-output*))
  (princ (format nil "~A" object) stream)
  object)

;;;(defmethod encode ((object integer) &optional (stream *standard-output*))
;;;  (princ object stream))

;;; changed by jpb 140201
(defmethod encode ((object integer) &optional (stream *standard-output*))
  (prin1 (format nil "~A" object) stream))

;;; added by jpb 130318
;;; make if produce lowercase strings JPB 140201
(defmethod encode ((object symbol) &optional (stream *standard-output*))
  (prin1 (format nil "~(~A~)" object) stream))

(defun encode-key/value (key value stream)
  (encode key stream)
  (write-char #\: stream)
  (encode value stream))

(defmethod encode ((object hash-table) &optional (stream *standard-output*))
  (write-char #\{ stream)
  (let (printed)
    (maphash (lambda (key value)
               (if printed
                   (write-char #\, stream)
                   (setf printed t))
               (encode-key/value key value stream))
             object))
  (write-char #\} stream)
  object)

(defmethod encode ((object vector) &optional (stream *standard-output*))
  (write-char #\[ stream)
  (let (printed)
    (loop
       for value across object
       do
       (when printed
         (write-char #\, stream))
       (setf printed t)
       (encode value stream)))
  (write-char #\] stream)
  object)

(defmethod encode ((object list) &optional (stream *standard-output*))
  (write-char #\[ stream)
  (let (printed)
    (dolist (value object)
      (if printed
          (write-char #\, stream)
          (setf printed t))
      (encode value stream)))
  (write-char #\] stream)
  object)

;;;(defun encode-symbol/value (symbol value stream)
;;;  (let ((string (symbol-name symbol)))
;;;    (encode-key/value string value stream)))

(defun encode-symbol/value (symbol value stream)
  (let ((string (string-downcase (symbol-name symbol))))
    (encode-key/value string value stream)))

(defun encode-alist (object &optional (stream *standard-output*))
  (loop initially (write-char #\{ stream)
     with printed = nil
     for (key . value) in object
     do (if printed
            (write-char #\, stream)
            (setf printed t))
       (encode-symbol/value key value stream)
     finally (write-char #\} stream)
       (return object)))

(defun encode-plist (object &optional (stream *standard-output*))
  (loop initially (write-char #\{ stream)
     with printed = nil
     for (key value . rest) on object by #'cddr
     do (if printed
            (write-char #\, stream)
            (setf printed t))
       (encode-symbol/value key value stream)
     finally (write-char #\} stream)
       (return object)))

(defmethod encode ((object (eql 'true)) &optional (stream *standard-output*))
  (write-string "true" stream)
  object)

(defmethod encode ((object (eql 'false)) &optional (stream *standard-output*))
  (write-string "false" stream)
  object)

(defmethod encode ((object (eql 'null)) &optional (stream *standard-output*))
  (write-string "null" stream)
  object)

(defmethod encode ((object (eql t)) &optional (stream *standard-output*))
  (write-string "true" stream)
  object)

(defmethod encode ((object (eql nil)) &optional (stream *standard-output*))
  (write-string "null" stream)
  object)

(defclass json-output-stream ()
  ((output-stream :reader output-stream
                  :initarg :output-stream)
   (stack :accessor stack
          :initform nil))
  (:documentation "Objects of this class capture the state of a JSON stream encoder."))

(defun next-aggregate-element ()
  (if (car (stack *json-output*))
      (write-char (car (stack *json-output*)) (output-stream *json-output*))
      (setf (car (stack *json-output*)) #\,)))

(defmacro with-output ((stream) &body body)
  "Set up a JSON streaming encoder context on STREAM, then evaluate BODY."
  `(let ((*json-output* (make-instance 'json-output-stream :output-stream ,stream)))
     ,@body))

(defmacro with-output-to-string* (() &body body)
  "Set up a JSON streaming encoder context, then evaluate BODY.
Return a string with the generated JSON output."
  `(with-output-to-string (s)
     (with-output (s)
       ,@body)))

(define-condition no-json-output-context (error)
  ()
  (:report "No JSON output context is active")
  (:documentation "This condition is signalled when one of the stream
  encoding function is used outside the dynamic context of a
  WITH-OUTPUT or WITH-OUTPUT-TO-STRING* body."))

(defmacro with-aggregate ((begin-char end-char) &body body)
  `(progn
     (unless (boundp '*json-output*)
       (error 'no-json-output-context))
     (when (stack *json-output*)
       (next-aggregate-element))
     (write-char ,begin-char (output-stream *json-output*))
     (push nil (stack *json-output*))
     (prog1
         (progn ,@body)
       (pop (stack *json-output*))
       (write-char ,end-char (output-stream *json-output*)))))

(defmacro with-array (() &body body)
  "Open a JSON array, then run BODY.  Inside the body,
ENCODE-ARRAY-ELEMENT must be called to encode elements to the opened
array.  Must be called within an existing JSON encoder context, see
WITH-OUTPUT and WITH-OUTPUT-TO-STRING*."
  `(with-aggregate (#\[ #\]) ,@body))

(defmacro with-object (() &body body)
  "Open a JSON object, then run BODY.  Inside the body,
ENCODE-OBJECT-ELEMENT or WITH-OBJECT-ELEMENT must be called to encode
elements to the object.  Must be called within an existing JSON
encoder context, see WITH-OUTPUT and WITH-OUTPUT-TO-STRING*."
  `(with-aggregate (#\{ #\}) ,@body))

(defun encode-array-element (object)
  "Encode OBJECT as next array element to the last JSON array opened
with WITH-ARRAY in the dynamic context.  OBJECT is encoded using the
ENCODE generic function, so it must be of a type for which an ENCODE
method is defined."
  (next-aggregate-element)
  (encode object (output-stream *json-output*)))

(defun encode-array-elements (&rest objects)
  "Encode OBJECTS, a list of JSON encodeable object, as array elements."
  (dolist (object objects)
    (encode-array-element object)))

(defun encode-object-element (key value)
  "Encode KEY and VALUE as object element to the last JSON object
opened with WITH-OBJECT in the dynamic context.  KEY and VALUE are
encoded using the ENCODE generic function, so they both must be of a
type for which an ENCODE method is defined."
  (next-aggregate-element)
  (encode-key/value key value (output-stream *json-output*))
  value)

(defun encode-object-elements (&rest elements)
  "Encode plist ELEMENTS as object elements."
  (loop for (key value) on elements by #'cddr
     do (encode-object-element key value)))

(defmacro with-object-element ((key) &body body)
  "Open a new encoding context to encode a JSON object element.  KEY
  is the key of the element.  The value will be whatever BODY
  serializes to the current JSON output context using one of the
  stream encoding functions.  This can be used to stream out nested
  object structures."
  `(progn
     (next-aggregate-element)
     (encode ,key (output-stream *json-output*))
     (setf (car (stack *json-output*)) #\:)
     (unwind-protect
          (progn ,@body)
       (setf (car (stack *json-output*)) #\,))))

(defgeneric encode-slots (object)
  (:method-combination progn)
  (:documentation
   "Generic function to encode objects.  Every class in a hierarchy
   implements a method for ENCODE-OBJECT that serializes its slots.
   It is a PROGN generic function so that for a given instance, all
   slots are serialized by invoking the ENCODE-OBJECT method for all
   classes that it inherits from."))

(defgeneric encode-object (object)
  (:documentation
   "Encode OBJECT, presumably a CLOS object as a JSON object, invoking
   the ENCODE-SLOTS method as appropriate.")
  (:method (object)
    (with-object ()
      (json:encode-slots object))))


;; This file is part of yason, a Common Lisp JSON parser/encoder
;;
;; Copyright (c) 2008 Hans Hübner
;; All rights reserved.
;;
;; Please see the file LICENSE in the distribution.

(in-package :yason)

(defconstant +default-string-length+ 20
  "Default length of strings that are created while reading json input.")

(defvar *parse-object-key-fn* #'identity
  "Function to call to convert a key string in a JSON array to a key
  in the CL hash produced.")

(defvar *parse-json-arrays-as-vectors* nil
  "If set to a true value, JSON arrays will be parsed as vectors, not
  as lists.")

(defvar *parse-json-booleans-as-symbols* nil
  "If set to a true value, JSON booleans will be read as the symbols
  TRUE and FALSE, not as T and NIL, respectively.")

(defvar *parse-object-as-alist* nil
  "If set to a true value, JSON objects will be parsed as association lists and not hash tables.")

(defun make-adjustable-string ()
  "Return an adjustable empty string, usable as a buffer for parsing strings and numbers."
  (make-array +default-string-length+
              :adjustable t :fill-pointer 0 :element-type 'character))

(defun parse-number (input)
  ;; would be
  ;; (cl-ppcre:scan-to-strings "^-?(?:0|[1-9][0-9]*)(?:\\.[0-9]+|)(?:[eE][-+]?[0-9]+|)" buffer)
  ;; but we want to operate on streams
  (let ((buffer (make-adjustable-string)))
    (loop
       while (position (peek-char nil input nil) ".0123456789+-Ee")
       do (vector-push-extend (read-char input) buffer))
    (values (read-from-string buffer))))

(defun parse-string (input)
  (let ((output (make-adjustable-string)))
    (labels ((outc (c)
               (vector-push-extend c output))
             (next ()
               (read-char input))
             (peek ()
               (peek-char nil input)))
      (let*
       (
        (starting-symbol (next))
        (string-quoted (equal starting-symbol #\"))
        )
       (unless string-quoted (outc starting-symbol))
       (loop
        (cond
         ((eql (peek) #\")
          (next)
          (return-from parse-string output))
         ((eql (peek) #\\)
          (next)
          (ecase (next)
             (#\" (outc #\"))
             (#\\ (outc #\\))
             (#\/ (outc #\/))
             (#\b (outc #\Backspace))
             (#\f (outc #\Page))
             (#\n (outc #\Newline))
             (#\r (outc #\Return))
             (#\t (outc #\Tab))
             (#\u (outc (code-char (let ((buffer (make-string 4)))
                                     (read-sequence buffer input)
                                     (parse-integer buffer :radix 16)))))))
         ((and (or (whitespace-p (peek)) 
                   (eql (peek) #\:)) 
               (not string-quoted)) 
          (return-from parse-string output))
         (t
          (outc (next)))))))))

(defun whitespace-p (char)
  (member char '(#\Space #\Newline #\Tab #\Linefeed #\Return)))

(defun skip-whitespace (input)
  (loop
     while (and (listen input)
                (whitespace-p (peek-char nil input)))
     do (read-char input)))

(defun peek-char-skipping-whitespace (input &optional (eof-error-p t))
  (skip-whitespace input)
  (peek-char nil input eof-error-p))

(defun parse-constant (input)
  (destructuring-bind (expected-string return-value)
      (find (peek-char nil input nil)
            `(("true" ,(if *parse-json-booleans-as-symbols* 'true t))
              ("false" ,(if *parse-json-booleans-as-symbols* 'false nil))
              ("null" nil))
            :key (lambda (entry) (aref (car entry) 0))
            :test #'eql)
    (loop
       for char across expected-string
       unless (eql (read-char input nil) char)
       do (error "invalid constant"))
    return-value))

(define-condition cannot-convert-key (error)
  ((key-string :initarg :key-string
               :reader key-string))
  (:report (lambda (c stream)
             (format stream "cannot convert key ~S used in JSON object to hash table key"
                     (key-string c)))))

(defun create-container ()
  (if *parse-object-as-alist*
      '()
      (make-hash-table :test #'equal)))

(defgeneric add-attribute (to key value)
  (:method ((to hash-table) key value)
   (setf (gethash key to) value)
   to)
  (:method ((to list) key value)
   (acons key value to)))

(defun parse-object (input)
  (let ((return-value (create-container)))
    (read-char input)
    (loop
      (when (eql (peek-char-skipping-whitespace input)
                 #\})
        (return))
      (skip-whitespace input)
      (setf return-value
        (add-attribute return-value
                       (prog1
                           (let ((key-string (parse-string input)))
                             (or (funcall *parse-object-key-fn* key-string)
                                 (error 'cannot-convert-key :key-string key-string)))
                         (skip-whitespace input)
                         (unless (eql #\: (read-char input))
                           (error 'expected-colon))
                         (skip-whitespace input))
                       (parse input)))
      (ecase (peek-char-skipping-whitespace input)
        (#\, (read-char input))
        (#\} nil)))
    (read-char input)
    return-value))

(defconstant +initial-array-size+ 20
  "Initial size of JSON arrays read, they will grow as needed.")

(defun %parse-array (input add-element-function)
  "Parse JSON array from input, calling ADD-ELEMENT-FUNCTION for each array element parsed."
  (read-char input)
  (loop
     (when (eql (peek-char-skipping-whitespace input)
                #\])
       (return))
     (funcall add-element-function (parse input))
     (ecase (peek-char-skipping-whitespace input)
       (#\, (read-char input))
       (#\] nil)))
  (read-char input))

(defun parse-array (input)
  (if *parse-json-arrays-as-vectors*
      (let ((return-value (make-array +initial-array-size+ :adjustable t :fill-pointer 0)))
        (%parse-array input
                      (lambda (element)
                        (vector-push-extend element return-value)))
        return-value)
      (let (return-value)
        (%parse-array input
                      (lambda (element)
                        (push element return-value)))
        (nreverse return-value))))

(defgeneric parse (input)
  (:documentation "Parse INPUT, which needs to be a string or a
  stream, as JSON.  Returns the lisp representation of the JSON
  structure parsed.")
  (:method ((input stream))
    (ecase (peek-char-skipping-whitespace input)
      (#\"
       (parse-string input))
      ((#\- #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9)
       (parse-number input))
      (#\{
       (parse-object input))
      (#\[
       (parse-array input))
      ((#\t #\f #\n)
       (parse-constant input))))
  (:method ((input pathname))
   (with-open-file (stream input)
     (parse stream)))
  (:method ((input string))
    (parse (make-string-input-stream input))))

#|
Exemple of a translation of a JSON string into Lisp structure
(setq json-string 
      "{
      \"id\":\"id1\",
      \"port\":40001,
      \"sender\":\"ExternalAgent\",
      \"task-id\":\"1:121\",
      \"receiver\":[\"claude\"],
      \"date\":1320488736396,
      \"ip\":\"192.168.100.11\",
      \"performative\":16,
      \"content\":
        {
          \"action\":\"create\",
          \"args\":
            {
              \"category\":\"postit\",
              \"content\":\"Cahier des charges\"
            }
        }
    }")

;; ask the parser to output alists
(setq *parse-object-as-alist* t)

;; parse the json string (it can be reversed to keep the string order")
(parse json-string)
(("content" ("args" ("content" . "Cahier des charges") ("category" . "postit")) ("action" . "create"))
 ("performative" . 16) ("ip" . "192.168.100.11") ("date" . 1320488736396) ("receiver" "claude")
 ("task-id" . "1:121") ("sender" . "ExternalAgent") ("port" . 40001) ("id" . "id1"))
|#

;;;=================================================================================
;;;                         Marcio's functions
;;;=================================================================================
;;; the following function was added by Marcio Fuckner

(defun moss-list-to-json (result)
  "we create a JSON representation of a MOSS list of individuals
Arguments:
   result: the resulting moss list
Return:
   the list in a json format"
  (let (json)
    (cond
     ;; is an attribute representation
     ((is-attr-rep result)
      (setq json (concatenate 'string "\"" (car result) "\"" " : "))
      (setq json (concatenate 'string json " [ "))
      (let ((index 0))
        (mapc #'(lambda(x) 
                  (setq json (concatenate 'string json (eval-attr-value x) ))
                  (incf index)
                  (if (< index (list-length (cdr result)))
                      (setq json (concatenate 'string json " , "))))
          (cdr result)))
      (setq json (concatenate 'string json " ] ")))
     ;; is an object representation
     ((is-obj-rep result)
      (setq json (concatenate 'string json " { "))
      (setq json (concatenate 'string json "\"type\" : \"" 
                   (car result) "\","))
      (let ((index 0))
        (mapc #'(lambda(x)
                  (setq json (concatenate 'string 
                               json (moss-list-to-json x))) 
                  (incf index)
                  (if (< index (list-length (cdr result)))
                      (setq json (concatenate 'string json " , ")))) 
          (cdr result)))
      (setq json (concatenate 'string json " } ")))
     ;; is a list of objects
     ((is-list-obj-rep result)
      (setq json (concatenate 'string json " [ "))
      (let ((index 0))
        (mapc #'(lambda(x)
                  (setq json (concatenate 'string 
                               json (moss-list-to-json x))) 
                  (incf index)
                  (if (< index (list-length result))
                      (setq json (concatenate 'string json " , ")))) result))
      (setq json (concatenate 'string json " ] "))))	
    json))
	  
(defun is-list-obj-rep (ref-symbol)
"check if the symbol is a list of moss objects
Arguments:
   ref-symbol: the moss list
Return:
   a boolean result"
   ;; must be a list
  (if (not (listp ref-symbol))
    (return-from is-list-obj-rep nil))
  ;; evaluate each list member to check its contents	
  (mapc #'(lambda(x) 
            (if (not (is-obj-rep x))
              (return-from is-list-obj-rep nil))) 
    (cdr ref-symbol))
  t)		 
		 
(defun is-obj-rep (ref-symbol)
"check if the symbol is according to the moss object format
Arguments:
   ref-symbol: the moss potential object
Return:
   a boolean result"
   ;; must be a list
  (if (not (listp ref-symbol))
    (return-from is-obj-rep nil))
  ;; to be an object the first element must be a string
  (if (not (stringp (car ref-symbol)))
    (return-from is-obj-rep nil))
  ;; to be an object the remainder must be attributes
  (mapc #'(lambda(x) 
            (if (not (is-attr-rep x))
              (return-from is-obj-rep nil))) 
    (cdr ref-symbol))
  t)


(defun is-attr-rep (ref-symbol)
"check if the symbol is according to the moss object format
Arguments:
   ref-symbol: the moss potential object
Return:
   a boolean result"
  (if (not (listp ref-symbol))
    (return-from is-attr-rep nil))
  ;; to be an attribute the first element must be a string
  (if (not (stringp (car ref-symbol)))
    (return-from is-attr-rep nil))
  ;; to be an attribute the remainder must be a:
  ;; string, mln string or an object
  (mapc #'(lambda(x) 
            (if (and (not (stringp x) ) 
                     (not (mln::mln? x)) ; jpb 1406
                     (not (is-obj-rep x)))
              (return-from is-attr-rep nil))) 
    (cdr ref-symbol))
  t)	  

(defun eval-attr-value (attr-value)
"this function checks if the attribute is multilingual. 
if so, it extracts the corresponding value according to 
the default language
Arguments:
   attr-value: the attribute
Return:
   changed attribute"
  (if (mln::mln? attr-value) ; jpb 1406
    (return-from eval-attr-value (concatenate 'string "\"" 
      (mln::-print-string attr-value *language*) "\""))) ; jpb 1406
  (if (stringp attr-value)
    (return-from eval-attr-value (concatenate 'string "\"" attr-value "\"")))
  (if (listp attr-value)
    (return-from eval-attr-value (moss-list-to-json attr-value))))
	
:EOF