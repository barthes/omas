﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/03/01
;;;                O M A S - C O N T R O L  (file control.lisp)
;;;
;;;===============================================================================
;;; This file contains the agent library for processing tasks.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;; AGENT-PROCESS-TASK (agent &aux message)                          [FUNCTION]
;;; AGENT-PROCESS-AGENDA-TASK (agent message)                        [FUNCTION]
;;; TASK-FRAME ""                                                       [CLASS]
;;; PRINT-OBJECT ((mm task-frame) stream)                              [METHOD]
;;; SUBTASK-FRAME ""                                                    [CLASS]
;;; PRINT-OBJECT ((mm subtask-frame) stream)                           [METHOD]
;;; AGENT-DELETE-PENDING-BID (agent task-id)                         [FUNCTION]
;;; AGENT-ESTIMATE-TASK-COST (agent skill args)                      [FUNCTION]
;;; AGENT-ESTIMATE-TASK-DURATION (agent skill args)                  [FUNCTION]
;;; AGENT-ESTIMATE-TASK-PARAMETERS (agent message)                   [FUNCTION]
;;; AGENT-ESTIMATE-TASK-QUALITY (agent message)                      [FUNCTION]
;;; AGENT-ESTIMATE-TASK-STARTING-TIME (agent message)                [FUNCTION]
;;; AGENT-GET-DYNAMIC-SKILL (agent action)                           [FUNCTION]
;;; AGENT-GET-HOW-LONG-FCN (agent action)                            [FUNCTION]
;;; AGENT-GET-HOW-LONG-LEFT-FCN (agent action)                       [FUNCTION]
;;; AGENT-GET-PATTERN-SKILL (agent action)                           [FUNCTION]
;;; AGENT-GET-SELECT-BEST-ANSWER-FCN (agent skill)                   [FUNCTION]
;;; AGENT-GET-SELECT-BEST-BIDS-FCN (agent skill)                     [FUNCTION]
;;; AGENT-GET-STATIC-SKILL (agent action)                            [FUNCTION]
;;; AGENT-GET-SKILL (agent action)                                   [FUNCTION]
;;; AGENT-GET-SKILL-ACKNOWLEDGE (agent skill)                        [FUNCTION]
;;; AGENT-GET-SKILL-PRECONDITIONS (agent skill)                      [FUNCTION]
;;; AGENT-GET-SKILL-TIME-LIMIT (agent action)                        [FUNCTION]
;;; AGENT-GET-SKILL-TIMEOUT (agent skill)                            [FUNCTION]
;;; AGENT-HAS-SKILL? (agent skill)                                   [FUNCTION]
;;; AGENT-OK-TO-PROCESS-MESSAGE (agent skill-name)                   [FUNCTION]
;;; AGENT-PROCESS-ANSWER (agent current-task subtask-frame message)  [FUNCTION]
;;; AGENT-PROCESS-ANSWERS (agent current-task-frame fn)              [FUNCTION]
;;; AGENT-PROCESS-BIDS-ON-TIMEOUT (agent subtask-id)                 [FUNCTION]
;;; AGENT-PROCESS-BROADCAST-ANSWER (agent current-task fn)           [FUNCTION]
;;; AGENT-PROCESS-FIRST-ANSWER (agent message current-task           [FUNCTION]
;;;                             processed-subtask fn)
;;; AGENT-PROCESS-INFORM (agent message)                             [FUNCTION]
;;; AGENT-PROCESS-INTERNAL (agent message skill)                     [FUNCTION]
;;; AGENT-PROCESS-REQUEST (agent message)                            [FUNCTION]
;;; AGENT-PROCESS-SAVED-ANSWERS (agent subtask-id)                   [FUNCTION]
;;; AGENT-PROCESS-SIMPLE-REQUEST-ANSWER (agent message               [FUNCTION]
;;;                                      current-task-frame
;;;                                      processed-subtask fn
;;;                                      &aux result)
;;; AGENT-PROCESS-SUBTASK-ANSWER (agent current-task-frame fn)       [FUNCTION]
;;; AGENT-MAKE-TASK-FRAME (agent message)                            [FUNCTION]
;;; AGENT-REMOVE-SKILL (agent skill-name)                            [FUNCTION]
;;; AGENT-REPLACE-TASK-OBJECT-IN-LIST (task-info task-list)          [FUNCTION]
;;; AGENT-SELECT-AGENDA-MESSAGE (agent)                              [FUNCTION]
;;; AGENT-SELECT-ANSWER-MESSAGE (agent &optional task-frame)         [FUNCTION]
;;; AGENT-SELECT-BEST-ANSWER (agent answer-list)                     [FUNCTION]

#|
2020
 0202 restructuring OMAS as an asdf system
|# 

(in-package :omas)

;;;========================== macros =============================================


;;; =============== functions associated with agent processes ====================

(defun agent-process-task (agent)
  "the function is associated with the agenda of the agent. As long as ~
   there are tasks on the agenda, will pick one and execute it.
      It runs in the :cg-user package.
   *package* is the agent package in which the function executes
   *language* is the language of the agent
   *context* is the version of the ontology
   *version-graph* is the configuration of the ontology
Arguments:
   agent: agent
Return:
   never returns unles killed."
  ;; first establish process global variables  
  (let ((*package* (ontology-package agent))
        (*language* (language agent))
        (*context* (moss-context agent))
        (*version-graph* (moss-version-graph agent))
        message)
    (declare (special *package* *language* *version-graph* *context*))
    ;; the variables are now defined in the agent package, except for *language*
    ;; inherited from MOSS
    (loop
      ;; wait if no message in the agenda
      (process-wait "idle: waiting for some task to do" #'agenda agent)
      (setq message (agent-select-agenda-message agent))
      ;; if no message is selected then there is something wrong, probably with the
      ;; selection process
      (unless message (error "no message can be selected in agenda of ~A" agent))
      ;; process task
      (agent-process-agenda-task agent message)
      ;; should not reset agent here, since it can be waiting for answer
      )))

;;;========================== main driver ========================================

(defun agent-process-agenda-task (agent message)
  "agent found something to do (:request, :internal, :answer). ~
   It calls the proper skill, and applies it.
   The function is executed by the main process.
Arguments:
   agent: agent
   message: message to process."
  (declare (special *omas*))
  (agent-trace agent "processing: ~S" (message-format message))
  (let ((task-id (task-id message))
        (sender (from! message))
        (time-limit (or (time-limit message)(default-time-limit *omas*))))
    ;; branch according to type of message to process
    ;; time-limit is the maximal time allowed to the process to execute. If it 
    ;; takes too long, then it is aborted
    (case (type! message)
      
      (:inform
       ;; when message has no task-id slot, we declare an error
       ;; if the message has no task-id, then we create one and insert it into the 
       ;; message
       (unless task-id
         (setq task-id (create-task-id))
         (setf (task-id message) task-id))
       ;; indeed, send-message should have created a default task-id
       
       ;; we set up a new process to execute the skill (:action part of the message)
       ;; the thread will run at most time-limit seconds (can be float)
       (agent-process-create-with-timeout
        agent
        (list :sender sender :task-id task-id :type :inform)
        (string+ (name agent) "/" task-id)
        time-limit
        'agent-process-inform  ; function to run
        agent message) 
       )
      
      (:request
       ;; when message has no task-id slot, we declare an error
       ;; indeed, send-message should have created a default task-id
       (unless task-id
         (error "message ~s does not have a task-id" (message-format message)))
       ;; we set up a new process to execute the skill (:action part of the message)
       ;; we record the sender just in case
         (agent-process-create-with-timeout
          agent
          (list :sender (from! message) :task-id task-id :type :request)
          (string+ (name agent) "/" task-id)
          time-limit
          'agent-process-request
          agent message)
         )
      
      (:internal
       ;; internal messages are used in particular for assistants
       ;; if the message has no task-id, then we create one and insert it into the 
       ;; message
       (unless task-id
         (setq task-id (create-task-id))
         (setf (task-id message) task-id))
       (dformat :intern 1
                "~&...~S: Creating new process for handling internal message. Id: ~S"
                (name agent) task-id)
       ;; create the process to take care of internal request
       (agent-process-create-with-timeout
        agent
        (list :task-id task-id :type :internal)
        ;(concatenate 'string (symbol-name (name agent)) "/" (format nil "~S" task-id))
        (string+ (name agent) "/" task-id)
        time-limit
        'agent-process-internal 
        agent message (action message))
       )
      
      ;; answers are directly put into the saved-answer-list slot of the task-frame
      ;; by the SCAN process
      
      ;; All other messages are ignored, i.e., poped out
      (otherwise
       (warn "~A received a message of unknown type: ~A, ~A" 
         agent (type! message) (message-format message)))
      )
    (agent-display agent)
    :done))

;;;========================== structures ===========================================
;;; a task-frame records the task being currently executed as the result of processing
;;; a message found in the agenda. Once a result has been obtained for the task, then
;;; either the result is sent to the agent waiting for it, or if the agent is an
;;; assistant agent and the task was initiated by the master, the result is shown
;;; to the master. The result can also be saved (logged or restructured to be saved
;;; in memory).

#|
TASK-FRAME
  answer-being-processed  contains answer message being processed. With multi-threads
               does not mean much since several answers may be processed at the
               same time
  environment  area left to the user to structure intermediate data 
  estimated-length    estimated length of the task (unused)
  estimated-end-of-task  estimated end-time for the task (universal time)
  from         agent that sent the message
  id           id of task
  master-answer master puts the answer to an :ask request. Task process is waiting
                on that slot (should be careful when the task is aborted)
  master-decision ?
  master-task  ?
  message      processed message by the task (message that triggered the task)
  process      thread that runs the task
  processed-broadcast-info  a list of answers, task and subtask frames resulting
               from broadcast answers processed by a timeout process
               (((<message>*) <task-frame> <subtask-frame>)*)
  quality      expected quality for the result
  reply-to    agents that should receive the results
  result       result of the task (initially nil)
  saved-answer-list  slot to put returning answers (unless broadcast or multicast)
               when answers are stored into the corresponding subtask-frame
  status       unused ?
  start-time   start time of the task
  subtask-list list of task-frames corresponding to spawned subtasks
  time-limit   time-limit set by the timer (unused ?)
  timer-process  timer process attached to the task (default 1 hr)
|#

(defClass TASK-FRAME ()
  ((answer-being-processed :accessor answer-being-processed :initform nil)
   (environment :accessor environment :initform nil)
   (estimated-length :accessor estimated-length :initarg :estimated-length
                     :initform 100000000)
   (estimated-end-of-task :accessor estimated-end-of-task
                          :initarg :estimated-end-of-task)
   (from :accessor from! :initarg :from :initform nil)
   (id :accessor id :initarg :id :initform nil)
   (master-task :accessor master-task :initform nil)
   (master-decision :accessor master-decision :initform nil)
   (master-answer :accessor master-answer :initform nil) ; put answer to :ask requests
   (message :accessor message :initarg :message :initform nil :type message)
   (process :accessor process :initarg :process :initform nil)
   (processed-broadcast-info :accessor processed-broadcast-info
                             :initform nil)
   (quality :accessor quality :initform nil)
   (reply-to :accessor reply-to :initarg :reply-to :initform nil)
   (result :accessor result! :initform nil)
   (saved-answer-list :accessor saved-answer-list :initform nil)
   (start-time :accessor start-time :initarg :start-time :initform nil)
   (status :accessor status :initarg :status :initform :idle)
   (subtask-list :accessor subtask-list :initform nil)
   (time-limit :accessor time-limit :initform nil)
   (timer-process :accessor timer-process :initform nil)
   )
  (:documentation "a task-frame describes a task being executed"))

(defMethod print-object ((mm task-frame) stream)
  (format stream "#<TASK ~A ~A ~A ~A ~A>"
          (id mm)
          (message mm)
          (subtask-list mm)
          (result! mm)
          (process mm)
          ))
#|
SUBTASK-FRAME
  acknowledge      set with sender key when an acknowledge answer has been received
  broadcast-max-answers  counts the number of broadcast answers
  cfb-timeout-process    set by send-subtask
  contractors      agent(s) that were chosen to execute the task
  estimated-end-time  estimated end-time for the task (universal time)
  id               id of task
  message          message sent by the task to the possible contractors
  parent-task      task-frame of the parent task
  quality          expected quality for the result
  repeat-count     repeat count for the timeout process (usually 3)
  result           result of the task (initially nil)
  saved-answers    answers returned during broadcast or multicast
  saved-bids       bids that were received on this subtask (contract net)
  start-time       start time of the task
  timeout          timeout set on the subtask
  timeout-process  process that runs the timeout
  to               targeted contractors
|#

(defClass SUBTASK-FRAME ()
  ((acknowledge :accessor acknowledge :initform nil)
   (broadcast-max-answers :accessor broadcast-max-answers :initform nil)
   (cfb-timeout-process :accessor cfb-timeout-process :initform nil)
   (contractors :accessor contractors :initform nil)
   (estimated-end-time :accessor estimated-end-time :initform nil)
   (id :accessor id :initarg :id :initform nil)
   (message :accessor message :initarg :message :initform nil :type message)
   (parent-task :accessor parent-task :initarg :parent-task :initform nil)
   (quality :accessor quality :initform nil)
   ;; initialize count to 2 to simulate having received message already twice
   (repeat-count :accessor repeat-count :initarg :repeat-count :initform 2)
   (result :accessor result! :initform nil)
   (saved-bids :accessor saved-bids :initform nil)
   (saved-answers :accessor saved-answers :initform nil)
   (start-time :accessor start-time :initarg :start-time :initform nil)
   (timeout :accessor timeout :initform nil)
   (timeout-process :accessor timeout-process :initform nil)
   (to :accessor to! :initarg :to :initform nil)
   )
  (:documentation "a subtask-frame describes a pending subtask"))

(defMethod print-object ((mm subtask-frame) stream)
  (format stream "#<TASK ~A ~A B:~A ~A>"
          (id mm)
          (message mm)
          (length (saved-bids mm))
          (result! mm)
          ))

;;;======================== specific agent functions =============================

;;; in the following functions, it is expensive to remove messages from the queue 
;;; and then to put them back. They can be modified without being moved.

;;;------------------------------------------------------ AGENT-DELETE-PENDING-BID

(defun agent-delete-pending-bid (agent task-id)
  "remove bid from the list of pending bids using the task-id.
Arguments:
   agent: agent
   task-id: task-id of the bid to be removed."
  (delete-if #'(lambda(xx) (eql task-id (task-id xx))) (pending-bids agent)))

;;;-------------------------------------------------- AGENT-ESTIMATE-STARTING-TIME

(defun agent-estimate-starting-time (agent skill args)
  "for a given skill with particular args estimates the delay before starting time.
Arguments:
   agent: agent
   skill: skill being checked
   args: whatever args are being necessary for the how-long-fcn.
Return:
   an integer, currently 0 since our agents are multithreaded."
  0)

;;;------------------------------------------------------ AGENT-ESTIMATE-TASK-COST

(defun agent-estimate-task-cost (agent skill args)
  "for a given skill with particular args estimates the cost of the task.
Arguments:
   agent: agent
   skill: skill being checked
   args: whatever args are being necessary for the how-long-fcn.
Return:
   2 values: cost and rigidity (100 means rigid, 0 flexible)"
  (let ((fcn (agent-get-bid-cost-fcn agent skill))
        (cost-expr '(0 100)))
    (if fcn
        (setq cost-expr (funcall fcn agent args)))
    ;; if expr not a list, should be a number
    (cond
     ((and (not (listp cost-expr)) (not (numberp cost-expr)))
      (error "bad value: ~S returned by estimating cost function: ~S" cost-expr fcn))
     ;; if simple number add rigidity  of 100
     ((not (listp cost-expr))
      (setq cost-expr (list cost-expr 100))))
      cost-expr))

;;;-------------------------------------------------- AGENT-ESTIMATE-TASK-DURATION

(defun agent-estimate-task-duration (agent skill args) 
  "for a given skill with particular args estimates how long the task will take.
Arguments:
   agent: agent
   skill: skill being checked
   args: whatever args are being necessary for the how-long-fcn."
  (let ((fcn (agent-get-how-long-fcn agent skill)))
    (if fcn
      (funcall fcn agent args)
      (default-execution-time *omas*))))

;(trace agent-estimate-task-parameters)
;(untrace agent-estimate-task-parameters)
;;;------------------------------------------------ AGENT-ESTIMATE-TASK-PARAMETERS

(defun agent-estimate-task-parameters (agent message)
  "estimates earliest execution time, delay, quality, cost and rigidity of result. ~
   Takes into account currently executing task, granted subtasks through :grant or ~
   :cancel-grant messages.
WHO CALLS THAT?
   Called by the scan process when answering a call-for-bids.
Arguments:
   agent: agent
   message: message containing the task to execute.
Returned value:
   list: (earliest-execution-time delay quality cost rigidity)"
  ;; this function was useful when the agent was executing one task at a time.
  ;; Since the agents are now multi threaded, the delay is more difficult to compute
  ;; and depends on the load of the agent, i.e., the number of tasks it is 
  ;; currently executing, and the load of the machine.
  ;; In addition, when the number of parallel tasks is limited
  ;; the agent may have to wait until one of the tasks completes or is aborted.
  ;; This time should be available from the task-frame information.
  (let ((skill (action message))
        (args (args message))
        duration quality start-time
        (slow-factor 1)
        cost-expr)
    ;; here we should compute some sort of coefficient depending on the load of the
    ;; agent and of its machine (e.g., if agent is already executing several tasks
    ;; then the duration time could be expected to be slower than if no task is
    ;; active
    ;(setq slow-factor (1+ (* 0.3 (length (active-task-list agent)))))
    ;; then estimate job duration for the task in the call-for-bids
    (setq duration (* slow-factor
                      (agent-estimate-task-duration agent skill args)))
    (setq cost-expr (agent-estimate-task-cost agent skill args))
    (setq quality (agent-estimate-task-quality agent skill args))
    (setq start-time (agent-estimate-starting-time agent skill args))
               
    ;(print (list 3 (name agent)(get-universal-time) early-start duration quality))
    ;; return set of parameters
    (list start-time        ; availability: 0 can start immediately
          duration          ; processing-time
          quality           ; expected quality of result (percent)
          (car cost-expr)   ; expected cost
          (cadr cost-expr)  ; expected cost rigidity (100 cannot change)
          )))

;(trace (agent-estimate-task-parameters :step t))
;(untrace agent-estimate-task-parameters)
;;;--------------------------------------------------- AGENT-ESTIMATE-TASK-QUALITY

(defun agent-estimate-task-quality (agent skill args) 
  "for a given skill with particular args estimates the quality of the result.
Arguments:
   agent: agent
   skill: skill being checked
   args: whatever args are being necessary for the how-long-fcn."
  (let ((fcn (agent-get-bid-quality-fcn agent skill)))
    (if fcn
      (funcall fcn agent args)
      100  ; default is "perfect"
      )))

;;;--------------------------------------------- AGENT-ESTIMATE-TASK-STARTING-TIME
;;; could be used by PA to estimate when master will answer...

(defun agent-estimate-task-starting-time (agent skill args) 
  "for a given skill with particular args estimates the quality of the result.
Arguments:
   agent: agent
   skill: skill being checked
   args: whatever args are being necessary for the how-long-fcn."
  (let ((fcn (agent-get-bid-starting-time-fcn agent skill)))
    (if fcn
      (funcall fcn agent args)
      0  ; default is "right away"
      )))

;;;---
;;; the following functions should be removed since it is easy to use the accessors
;;; directly

;;;------------------------------------------------------- AGENT-GET-DYNAMIC-SKILL

(defun agent-get-bid-cost-fcn (agent action)
  "get the function estimating cost for a proposed task in a call-for-bids"
  (if (agent-has-skill? agent action) 
      (bid-cost-fcn (agent-get-skill agent action))))

(defun agent-get-bid-quality-fcn (agent action)
  "get the function estimating quality for a proposed task in a call-for-bids"
  (if (agent-has-skill? agent action) 
      (bid-quality-fcn (agent-get-skill agent action))))

(defun agent-get-start-time-quality-fcn (agent action)
  "get the function estimating starting time for a proposed task in a call-for-bids"
  (if (agent-has-skill? agent action) 
      (get-bid-start-time-fcn (agent-get-skill agent action))))
  
(defun agent-get-dynamic-skill (agent action)
  "get the name of the function corresponding to the requested skill"
  (if (agent-has-skill? agent action) 
    (dynamic-fcn (agent-get-skill agent action))))

(defun agent-get-how-long-fcn (agent action)
  "get the name of the function estimating processing time for an action"
  (if (agent-has-skill? agent action) 
    (how-long-fcn (agent-get-skill agent action))))

(defun agent-get-how-long-left-fcn (agent action)
  "get the name of the function estimating how much time to end of task"
  (if (agent-has-skill? agent action) 
    (how-long-left-fcn (agent-get-skill agent action))))

(defun agent-get-pattern-skill (agent action)
  "get the type pattern for the function corresponding to the requested skill. ~
   If nil then no check on arguments. Ex: ((x int)(y string))"
  (if (agent-has-skill? agent action) 
      (static-pattern (agent-get-skill agent action))))

(defun agent-get-select-best-answer-fcn (agent skill)
  "get the name of the function for selecting the best answers from a list of answer~
   messages"
  (select-best-answer-fcn (agent-get-skill agent skill)))

(defun agent-get-select-best-bids-fcn (agent skill)
  "get the name of the function for selecting the best bids from a list of bid messages"
  (select-bids-fcn (agent-get-skill agent skill)))

(defun agent-get-static-skill (agent action)
  "get the name of the function corresponding to the requested skill"
  (if (agent-has-skill? agent action) 
    (static-fcn (agent-get-skill agent action))))

(defun agent-get-skill (agent action)
  "get the skill object from its name"
  (dolist (skill (skills agent))
    (if (eql action (name skill))
      (return-from agent-get-skill skill))))

(defun agent-get-skill-acknowledge (agent skill)
  "get the name of a function for processing a returned acknowledge message.
Arguments
   agent: current agent
   action: skill name (current task)."
  (if (agent-has-skill? agent skill) 
    (acknowledge-fcn (agent-get-skill agent skill))))

(defun agent-get-skill-preconditions (agent skill)
  "get the name of a function for processing timeout conditions on the skill.
Arguments
   agent: current agent
   action: skill name (current task) for which we want to process the timeout."
  (if (agent-has-skill? agent skill) 
    (preconditions (agent-get-skill agent skill))))

(defun agent-get-skill-time-limit (agent action)
  "get the name of the function to process timeout messages"
  (if (agent-has-skill? agent action) 
    (time-limit-fcn (agent-get-skill agent action))))

(defun agent-get-skill-timeout (agent skill)
  "get the name of a function for processing timeout conditions on the skill.
Arguments
   agent: current agent
   action: skill name (current task) for which we want to process the timeout."
  (if (agent-has-skill? agent skill) 
    (timeout-handler (agent-get-skill agent skill))))

#|
(defun agent-get-subtask-info (agent subtask-id)
  "get subtask info from the name of the subtask in the list of pending tasks."
  (car (member-if #'(lambda(xx)(eql subtask-id (id xx))) 
                  (pending-tasks agent))))
|#

(defun agent-has-skill? (agent skill)
  "check if agent has the required skill"
  (member-if #'(lambda(xx)(eql skill (name xx))) (skills agent)))

;;;--------------------------------------------------------- AGENT-MAKE-TASK-FRAME

(defun agent-make-task-frame (agent message)
  "build a task-frame description for the task to be executed, from the information ~
   found in the incoming message.
Arguments:
   agent: agent
   message: message that creates the task."
  (let ((fn (agent-get-how-long-fcn agent (action message)))
        (duration most-positive-fixnum))
    ;; when we have a function for estimating duration, then we use it
    (when fn 
      ;; duration should be in seconds
      (setq duration (apply fn agent message nil)))
    ;(print `(current-process ,*current-process*))
    ;; then set up task-frame
    (make-instance 'TASK-FRAME
      :message message
      :start-time (get-universal-time)
      :id (cond ((task-id message)) ((create-task-id)))
      :from (from! message)
      :reply-to (cond ((reply-to message))((from! message)))
      :estimated-length duration
      :estimated-end-of-task (+ (get-universal-time) duration)
      :process *current-process*
      )))

;;;--------------------------------------------------- AGENT-OK-TO-PROCESS-MESSAGE

(defun agent-ok-to-process-message (agent skill-name arg-list message)
  "function that decides whether the agent wants to process the incoming message.
   Checks if the skill is there and if the preconditions are met.
Arguments:
   agent: agent
   skill-name: content of the action area of the incoming message
   arg-list: list of arguments of the message
   message: original message
Return:
   nil : does not want to process
   otherwise, a function corresponding to the skill that was called."
  (let ((skill (agent-get-static-skill agent skill-name))
        (fn (agent-get-skill-preconditions agent skill-name)))
    (unless skill
      (vformat "~&...Agent ~S has not the ~S skill" (name agent) skill-name))
    (when (and fn
               ;(not (apply fn agent (environment agent) arg-list)))
               (not (apply fn agent message arg-list))) ; JPB0906
      (vformat "~&...Agent ~S: preconditions for the ~S skill not met."
               (name agent) skill-name)
      (return-from agent-ok-to-process-message nil))
    skill))

;;;---------------------------------------------------------- AGENT-PROCESS-ANSWER

(defun agent-process-answer (agent current-task subtask-frame message)
  "processes a simple answer to a subtask by calling the corresponding dynamic ~
   part of the skill.
   Called from an undefined process.
Arguments:
   agent: agent
   current-task: task that spawned the subtask
   subtask-frame: subtask corresponding to the answer to be processed
   message: answer message.
Return:
   ?"
  (let (skill fn)
    ;; get the skill from the action part of the message
    (setq skill (action (message current-task)))
    ;; get the dynamic part of the skill
    (setq fn (agent-get-dynamic-skill agent skill))
    (unless fn
      ;; we can't process the request
      ;; for the time being we declare an error. We should cancel the task and
      ;; return an error message to the sender
      (error "~A has no dynamic skill ~S, can't process the job" agent skill))
    (dformat :skill-header 0 "~2%;========== ~A: ~S =========="
             (key agent) (action message))
    ;; call as if it were a first answer process 
    (agent-process-first-answer agent message current-task subtask-frame fn)
    ))

;;;--------------------------------------------------------- AGENT-PROCESS-ANSWERS

(defun agent-process-answers (agent current-task-frame fn)
  "the agent has spawned subtasks and we must process answers when they come. ~
   A waiting loop is set on the saved-answer-list or the process-broadcast-info ~
   of the task-frame. An answer ~
   must be associated with the right subtask (found in the subtask-list). 
   If the subtask was a simple task or if the multi-cast/broadcast strategy ~
   is :take-first-answer, or if it is :collect-first-n-answers and the number ~
   of answers is reached, then we process the answer directly. Otherwise, we ~
   insert the answer into a special slot until the timeout timer fires or a ~
   special condition is met (e.g., number of answers, quality of the answer,...)
Arguments:
   agent: agent
   current-task-frame: task-frame for current-task
   fn: function name of dynamic skill to be applied to each answer.
Return:
   we never return from this function.
   Either the process has been killed or we get an error."
  ;; we set up a loop for waiting for answers
  (dformat :ans 1 "~S: agent-process-answers /current-task-frame: ~S" (name agent) current-task-frame)
  (dformat :ans 1 "~S: agent-process-answers /sub-task-list: ~S" (name agent)(subtask-list current-task-frame))
  (loop
    ;; we get out of the loop when all pending subtasks have found an answer
    ;; i.e., have been removed from the subtask-list
    ;; in fact, when all subtasks have been processed, the dynamic skill
    ;; should have called dynamic-exit when detecting that there are no more
    ;; pending tasks. dynamic-exit kills the process
    ;; hence, there should not be any problem getting out of this loop
    (unless (subtask-list current-task-frame)
      (agent-trace agent 
                   "warning: Subtasks have been exhausted, which should have been ~
                    detected by the dynamic-exit function. This function might ~
                    miss from the dynamic skill code")
      (return nil))
    ;; reset the answer-being-processed slot
    (setf (answer-being-processed current-task-frame) nil)
    ;; wait until somebody sends an answer back
    (agent-process-wait agent "Waiting for subtasks answers..."
                        #'(lambda (xx)
                            (or (saved-answer-list xx)
                                (processed-broadcast-info xx))) 
                        current-task-frame)
    ;; here we are woken up by an answer that may come from a subtask or
    ;; from a broadcast timeout process (selected answer list after a
    ;; broadcast/multicast timeout)
    (vformat "~A aget-process-answers: saved-answer-list: ~S" (name agent)
             (saved-answer-list current-task-frame))
    
    (cond 
     ;; broadcasts have priority
     ((processed-broadcast-info current-task-frame)
      (agent-process-broadcast-answer agent current-task-frame fn))
     ;; answer from a subtask
     ((saved-answer-list current-task-frame)
      ;(format t "~2%;===== agent-process-answers/ subtask-list:~%;  ~S"
      ;  (subtask-list current-task-frame))
      (agent-process-subtask-answer agent current-task-frame fn))
     (t
      (error "agent process answers woke up but we can't find the answer")))
    ) ; end of the loop
  ;; here the subtask list is empty meaning that the task is finished.
  ;; The dynamic-exit function should have been called to do the 
  ;; final cleaning job. If this was not done, then we are in trouble, since
  ;; no answer has been sent back to the requesting agent. The system will
  ;; either hang up or send a new request and give up after a while.
  ;; dynamic-exit kills the task process, thus we should never come here 
  (error "~A dynamic skill ~A is faulty, no dynamic exit was done" 
         agent (action (message current-task-frame)))
  )

;;;------------------------------------------------- AGENT-PROCESS-BIDS-ON-TIMEOUT

(defun agent-process-bids-on-timeout (agent subtask-frame)
  "function called by a call-for-bids-timeout process to process eventual bids. ~
   If no bids are found acceptable, it returns nil. Otherwise, it sends a grant ~
   message to the sender(s) of a winning bid.
Arguments:
   agent: agent
   subtask-id: subtask attached to this process."
  (let (best-bid-list message grantees)
    ;; === here we found the right subtask
    (setq best-bid-list (select-best-bids agent (saved-bids subtask-frame)))
    ;; if none is available or none is acceptable, return nil
    ;; reset saved bids slot
    (setf (saved-bids subtask-frame) nil)
    (unless best-bid-list 
      (return-from agent-process-bids-on-timeout nil))
    
    ;; get the agent refs from the best-bid messages
    (setq grantees (mapcar #'from! best-bid-list))
    ;(format t "~%; agent-process-bids-on-timeout /grantees: ~S" grantees)
    
    (excl::without-interrupts
     ;; otherwise, send a cancel-grant message, granting the
     ;; job to a particular agent. Other agents will be able to remove the task
     ;; from the pending bids and resubmit old bids with better time delays
     (setq message 
           (message-make-cancel-grant agent :all (id subtask-frame) grantees))
     (send-message message)
     
     ;; record the change in the subtask-frame, updating the stored message JPB1011
     (setf (type! (message subtask-frame)) :request
       (to! (message subtask-frame)) grantees
       ;; should be a multicast, however we take the first answer
       (protocol (message subtask-frame)) :broadcast
       ;; strategy should be take the first answer since all agents are "good"
       (strategy (message subtask-frame)) :take-first-answer)
     
     ;; record that we contracted the task
     (setf (contractors subtask-frame) grantees)

     ;; we do not remove bids from subtask-frame. This could be useful.
     ;; kill timer
     (agent-process-kill agent *current-process*)
     )))

;;;------------------------------------------------ AGENT-PROCESS-BROADCAST-ANSWER

(defUn agent-process-broadcast-answer (agent current-task fn)
  "called by agent-process-answers or agent-process-subtask-answer to process an ~
   answer that has been selected by a broadcast timeout process (already dead), or ~
   because we have received enough answers.
   Called from the process attached to the task.
Arguments:
   agent: agent
   current-task: task-frame of the task that sent the broadcast/multicast
   fn: dynamic part of the skills
Return:
   ?"
  ;;===== gotcha!
  (let (info result best-answer message ff)
    ;; select first entry of the info list (use pop?)
    (setq info (car (processed-broadcast-info current-task)))
    (vformat "~&;=== agent-process-broadcast-answer /info ~&~S" info)
    ;; update list
    (setf (processed-broadcast-info current-task)
          (cdr (processed-broadcast-info current-task)))
    ;; when a select-best-answer function is provided by the user, then we
    ;; activate it and pass the resulting structure to the dynamic part of
    ;; the skill as if it were the content of an answer message
    ;; first get the function
    ;(format t "~&;+++ agent-process-broadcast-answer: current-task: ~S~
    ;           (message current-task):~& ~S" current-task (message current-task))
    (setq ff (agent-get-select-best-answer-fcn 
              agent (action (message current-task))))
    ;; compute best answer(s) using the user-provided function if available
    ;; otherwise pick the first available message
    ;; (car info) contains the list of valid answer messages
    ;(format t "~&;+++ agent-process-broadcast-answer: select-answer-fcn ~S" ff)
    (cond
     (ff
      ;(setq best-answer (funcall ff agent (environment agent) (car info)))
      ;; we remove the environment arg
      (setq best-answer (funcall ff agent (car info))) ; jpb0906
      ;; the answer from the user function must be a list of: the message and the
      ;; computed value to be passed to the skill
      
      ;; if the answer is nil (no interesting answer) then give up
      (unless best-answer
        ;; current process is killed
        (abort-current-task agent)
        ;; safety feature
        (error "the current process ~S should have been killed" *current-process*))
      
      (unless (and (listp best-answer) (typep (car best-answer) 'message))
        (error "the best answer user function should return a list with the ~
                message and the value to be passed to the dynamic skill, instead ~
                of: ~%  ~S" best-answer))
      (setq message (car best-answer)
          best-answer (cadr best-answer)))
     
     ;; if no selection function is provided, then keep the list of unprocessed
     ;; messages (including failures), and mark the result as :unprocessed
     (t
      (setq message :unprocessed)
      (setq best-answer (car info))))
    ;(format t "~&;+++ agent-process-broadcast-answer: best answer~& ~S
    ;                        ~&Applying dynamic skill to it..." best-answer)
    ;; apply skill to the previous result, passing the selected best answer message
    (setq result (funcall fn agent 
                          ;(environment current-task)
                          message ; JPB0906
                          best-answer))
    ;(format t "~&;+++ agent-process-broadcast-answer: result! ~S" result)
    ;; the keyword :abort indicates that the subtask is ignored, but the task 
    ;; cannot be completed as such and must be aborted
    (when (eql :abort result)
      ;; current process is killed
      (abort-current-task agent)
      ;; safety feature
      (error "the current process ~S should have been killed" *current-process*))
    
    ;; standard case: we update subtask-list, removing the processed subtask
    (setf (subtask-list current-task)
          (remove-if #'(lambda (xx)(eql (id xx)(caddr info)))
                     (subtask-list current-task)))
    ;; if we had the last answer, then the skill will have called the
    ;; dynamic-exit function killing the current process. In this case we do not
    ;; return
    :done))

;;;---------------------------------------------------- AGENT-PROCESS-FIRST-ANSWER

(defun agent-process-first-answer 
    (agent message current-task processed-subtask fn)
  "process an answer message for a subtask, corresponding to point-to-point, or ~
   a broadcast or multicast with a :take-first-answer strategy.
Arguments:
   agent: agent
   message: answer message
   current-task: task being executed by the agent
   fn: dynamic skill function for the message."
  (let (ff best-answer result)
    ;; first kill timeout timer if any
    (when (timeout-process processed-subtask)
      (agent-trace agent "killing ~S timeout timer"
                   (agent-process-name agent 
                                       (timeout-process processed-subtask)))
      (excl::without-interrupts
        (agent-process-kill agent (timeout-process processed-subtask))
        ;; reset timeout process and timeout value
        (setf (timeout-process processed-subtask) nil
          (timeout processed-subtask) nil)))
    
    ;; send cancel message to the rest of the world
    (send-message (message-make-cancel agent :all (task-id message)))
    ;; apply skill. However, when there is a select-best-answer user function
    ;; we must apply it because it may construct special data structures to be
    ;; passed to the dynamic function
    ;; note that specifying a :take-first-answer strategy and providing a 
    ;; select-best-answer function is somewhat inconsistent. However, strategy
    ;; and options can be set independently
    (setq ff (agent-get-select-best-answer-fcn 
              agent (action (message current-task))))
    ;; compute best answer(s) using user-provided function if available
    (setq best-answer
          (if ff
              (funcall ff agent nil (list message))
            (contents message)))
    ;; then call the dynamic part of the skill, passing the best answer message
    (setq result (funcall fn agent 
                          ;(environment current-task)
                          message ; JPB0906
                          best-answer))
    ;; a return of :abort indicates that the subtask is ignored, but the task 
    ;; cannot be completed as such and must be aborted
    (when (eql :abort result)
      ;; current process is killed
      (abort-current-task agent)
      (error "the current process ~S should have been killed" *current-process*))
    ;; normaly we never come here except when dynamic-exit is missing from the
    ;; dynamic skill (programming error) or if the computation is not finished
    ;; when we have sent several subtasks.
    ;; we update subtask-list, removing the processed subtask
    (setf (subtask-list current-task)
      (remove-if #'(lambda (xx)(eql (id xx)(task-id message)))
                 (subtask-list current-task)))
    ;; if we had the last answer, then the skill will have called the
    ;; dynamic exit function killing the current process. In this case we do not
    ;; return
    :done))

;;;---------------------------------------- AGENT-PROCESS-BROADCAST-FIRST-N-ANSWER

(defUn agent-process-broadcast-first-n-answers (agent task-frame subtask-frame fn)
  "called by agent-process-subtask-answer to process the list of answers that have ~
   been collected during the broascast. Must do some cleaning. 
   We are in the task process (as if resuming from a timeout).
Arguments:
   agent: agent
   task-frame: current task-frame that sent the broadcast/multicast
   subtask-frame: frame of the subtask that answers the broadcast/multicast
   fn: dynamic part of the skills
Return:
   ?"
  ;; first kill timer, cleaning subtask-frame
  (agent-process-kill agent (timeout-process subtask-frame))
  ;; this is useless since qe are going to kill the subtask
  (setf (timeout-process subtask-frame) nil
        (timeout subtask-frame) nil)
  ;; we cancel subtask
  (send-message (message-make-cancel agent :all (id subtask-frame)))
  ;; anyway the subtask-frame will be destroyed
  
  ;; then process the list of messages saved into the saved-answers slot of the
  ;; subtask-frame
  (let (result best-answer message ff)
    ;; when a select-best-answer function is provided by the user, then we
    ;; activate it and pass the resulting structure to the dynamic part of
    ;; the skill as if it were the content of an answer message
    ;; first get the function
    ;(format t "~&;+++ agent-process-broadcast-first-n-answer: current-task: ~S~
    ;           (message current-task):~& ~S" current-task (message current-task))
    (setq ff (agent-get-select-best-answer-fcn 
              agent (action (message task-frame))))
    ;; compute best answer(s) using the user-provided function if available (it 
    ;; must apply to a list of messages, and return the best message)
    ;; otherwise pick a message randomly
    ;(format t "~&;+++ agent-process-broadcast-first-n-answer select-answer-fcn ~S" ff)
    (cond
     (ff
      ;(setq best-answer (funcall ff agent (environment agent) (car info)))
      ;; we remove the environment arg
      (setq best-answer (funcall ff agent (saved-answers subtask-frame)))) ; jpb0906
     (t
      (setq message (agent-select-best-answer agent (saved-answers subtask-frame))
            best-answer (contents message))
      ))
    ;(format t "~&;+++ agent-process-broadcast-answer: best answer~& ~S
    ;                        ~&Applying dynamic skill to it..." best-answer)
    ;; apply skill to the previous result, passing the selected best answer message
    ;; message could be NIL
    (setq result (funcall fn agent 
                          ;(environment current-task)
                          message ; JPB0906
                          best-answer))
    ;(format t "~&;+++ agent-process-broadcast-answer: result! ~S" result)
    ;; the keyword :abort indicates that the subtask is ignored, but the task 
    ;; cannot be completed as such and must be aborted
    (when (eql :abort result)
      ;; current process is killed
      (abort-current-task agent)
      ;; safety feature
      (error "the current process ~S should have been killed" *current-process*))
    ;; standard case: we update subtask-list, removing the processed subtask
    (setf (subtask-list task-frame) (remove subtask-frame (subtask-list task-frame)))
    ;; if we had the last answer, then the skill will have called the
    ;; dynamic-exit function killing the current process. In this case we do not
    ;; return
    :done))

;;;---------------------------------------------------------- AGENT-PROCESS-INFORM

(defun agent-process-inform (agent message)
  "a new process has been created. We must build the control structures ~
   (task-frame) call the skill, and, if present, execute it. We are executing in ~
   the process created to execute the inform message.
   No return message is produced.
Arguments:
   agent: agent
   message: input message (task requires answer)."
  ;; create a global variable that will keep the value of the message just received
  ;; OK, since we execute in the process of the current task
  (declare (special *current-message*))
  (setq *current-message* message)
  ;; first establich process global variables
  (let ((*package* (ontology-package agent))
        (moss::*language* (language agent))
        (moss::*context* (moss-context agent))
        (moss::*version-graph* (moss-version-graph agent)))
    (let* ((skill (action message))
           ;; get static part of the skill (ckecked in omas-comm)
           (fn (agent-get-static-skill agent skill))
           current-task result delay time-limit reason)
      (agent-trace agent "task created to process: ~A" (message-format message))
      (dformat :skill-header 0 "~2%;========== ~A: ~S =========="
               (key agent) (action message))
      ;(dformat :skill-header 0 "~%;--- message: ~S" message)
      ;; we create then a new task-frame describing the task being processed
      (setq current-task (agent-make-task-frame agent message))
      ;; add task-frame to the active task list
      (agent-add agent current-task 'active-task-list)
      ;; update agent window to show message?
      (agent-display agent)
      
      ;; no need to set up a timer, the thread is executed within a with-timeout      
;;;      ;; ... we set up a timer process, eventually using the default time.
;;;      ;; The default time-limit prevents dead task to clutter
;;;      ;; the agent active-task-list slot of agent. 
;;;      ;; I.e., all tasks are removed after 1 hr.
;;;      (setq time-limit (or (time-limit message) (default-time-limit *omas*)))
;;;      ;; It will be triggered at the specified date: (date + time-limit - current)
;;;      ;; time-limit may be a real number (fraction of a second (hence: ceiling)
;;;      (setq delay (+ (date message) (- (get-universal-time)) time-limit))
;;;      ;; record time-limit into current-task (in seconds)
;;;      (setf (time-limit current-task) 
;;;            (+ (date message) (ceiling time-limit)))
;;;      ;; next function also sets timer-process slot of current task frame
;;;      (set-time-limit-timer agent current-task delay)
      
      ;; then apply static skill (static exit returns two values)
      (multiple-value-setq (result reason)
        (apply fn agent 
               ;(environment current-task)
               message ; JPB0906
               (args message)))
      (dformat :o-control 0 "~%;+++ agent-process-request /result: ~S, reason: ~S"
               result reason)
      ;; Once the skill has been executed, reset the message variable
      (setq *current-message* nil)
      
      ;; if the result is the keyword :abort, which is the case when an agent has
      ;; the required skill but does not want to do the task, then we simply quit
      (when (eql :abort result)
        ;; we signal here in the graphics trace that the job was abandoned 
        (agent-display-abort agent)
        ;; check what we exactly did while executing the skill and undo it
        (cancel-current-task agent :task-frame current-task)
        ;; cancel never returns
        ;; no error message is returned to the sender, since this is not an error
        ;; condition but the decision not to do the job. However, the skill code
        ;; could send back an error message
        )
      
      ;; Here the skill has been executed and two cases may happen
      ;;   - the skill was atomic and we have a result
      ;;   - the skill has spawned subtasks and we must wait for their result
      (unless (subtask-list current-task)
        (cancel-current-task agent :no-subtasks t :no-mark t)
        ;; cancel-current-task does not return (process is killed)
        )
      ;; here we spawned subtasks and must process answers as they come.
      ;; first check if we have a dynamic skill needed to process answers
      (setq fn (agent-get-dynamic-skill agent skill))
      (unless fn
        ;; we can't process the request
        ;; for the time being we declare an error. We should cancel the task and
        ;; return an error message to the sender
        (error "~A has no dynamic skill ~S, can't process the job" agent skill))
      ;; if we have a dynamic skill, then go process answers (same as for requests)
      (agent-process-answers agent current-task fn)
      :done)))

#| Test
we give an agent a :test skill calling another agent with an hello request
we send an inform message to the agent and see what happens
|#
;;;-------------------------------------------------------- AGENT-PROCESS-INTERNAL

(defUn agent-process-internal (agent message skill)
  "a new process is created to deal with an internal message.
   The function processes an internal message using adequate skills.
   No return message is produced.
   Called within the internal task process.
Arguments:
   agent: agent
   message: input message (task requires answer)
   skill: requested static skill."
  ;; first establish process global variables
  ;; an internal message is sent by the agent itself, it is thus not necessary to
  ;; reinstall a context. Furthermore, setting up the MOSS global variables is not
  ;; a good idea since they are shared by all agents calling MOSS, unless we 
  ;; assume that a single application is running in the Lisp environment and all
  ;; agents share the same language (versioning is an application business)
  (let ((*package* (ontology-package agent))
        (moss::*language* (language agent))
        (moss::*context* (moss-context agent))
        (moss::*version-graph* (moss-version-graph agent)))
    (let ((fn (agent-get-static-skill agent skill))
          current-task-frame result)
      (dformat :intern 1 "~A: processing-internal/ ~S" (name agent) message)
      (dformat :intern 1 "~A: processing-internal/ fn: ~S" (name agent) fn)
      ;; if skill is not there ignore message and quit
      (unless fn 
        (warn "~S: unknown skill on internal message ~S" agent message)
        (agent-process-kill-current-process agent))
      
      ;; we create then a task-frame describing the task being processed
      (setq current-task-frame (agent-make-task-frame agent message))
      (dformat :intern 1 "~A: processing-internal/ current-task-frame: ~S" (name agent) current-task-frame)	  
      ;; add task-frame to the active task list
      (agent-add agent current-task-frame 'active-task-list)
      ;; update agent window (is this necessary?)
      (dformat :intern 1 "~A: processing-internal/ active-task-list" (key agent)(active-task-list agent))
      (agent-display agent)
      
      ;; we do not need to set a timer since we created the task with a timeout
      
      ;; apply skill
      (dformat :intern 1 "~A: processing-internal/executing skill ~A" (key agent) fn)
      (setq result (apply fn agent 
                          ;(environment current-task-frame)
                          message ; JPB0906
                          (args message)))
      ;; subtask-list should contain a subtask frame						  
      (dformat :intern 1 "~A: processing-internal/ subtask-list ~A" 
               (key agent)(subtask-list current-task-frame))
      
      ;; if the result is the keyword :abort, which is the case when an agent has
      ;; the required skill but does not want to do the task, then we simply quit
      (when (eql :abort result)
        ;; we signal here in the graphics trace that the job was abandoned 
        (agent-display-abort agent)
        (dformat :intern 1 "task to cancel ~A" current-task-frame)
        ;; check what we exactly did while executing the skill and undo it
        (cancel-current-task agent :no-subtasks t)
        ;; cancel never returns
        )
      
      ;; **** we should remove the task frame when the task is finished....
      ;; Here the internal skill has been executed and two cases may happen
      ;;   - the skill was atomic and we have a result
      ;;   - the skill has spawned subtasks and we must wait for their result
      
      (unless (subtask-list current-task-frame)
        ;; OK, here no subtask, we got the result. We do nothing special with it
        ;; presumably it was executed for side effect
        (cancel-current-task agent :no-subtasks t :task-frame current-task-frame)
        ;; cancel-current-task does not return (process is killed)
        )
		
      ;;=== here we spawned subtasks and must process answers as they come
      ;; first, check if we have a dynamic skill needed to process answers
      (setq fn (agent-get-dynamic-skill agent skill))
	 (dformat :intern 1 "~A: processing-internal/ dynamic skill ~A" (name agent) fn)

      (unless fn
        ;; we can't process the task
        ;; for the time being we declare an error. We should cancel the task and
        ;; return an error message to the sender (what if it is an internal message?)
        (error "~A has no dynamic skill ~S, can't finish the job" agent skill))
      ;; and go process answers
      (agent-process-answers agent current-task-frame fn)
      :done)))

;;;--------------------------------------------------------- AGENT-PROCESS-REQUEST

(defun agent-process-request (agent message)
  "a new process has been created. We must build the control structures ~
   (task-frame) call the skill, and, if present, execute it. We are executing in ~
   the process created to execute the request.
   A return message is produced.
Arguments:
   agent: agent
   message: input message (task requires answer)."
  ;; create a global variable that will keep the value of the message just received
  ;; OK, since we execute in the process of the current task
  (declare (special *current-message*))
  (setq *current-message* message)
  ;; first establich process global variables
  (let ((*package* (ontology-package agent))
        (moss::*language* (language agent))
        (moss::*context* (moss-context agent))
        (moss::*version-graph* (moss-version-graph agent)))
    (let* ((skill (action message))
           ;(fn (agent-ok-to-process-message agent skill (args message)))
           ;; checks if preconditions are verified for the required skill
           (fn (agent-ok-to-process-message agent skill (args message)
                                            message)) ; JPB0906
           current-task result delay answer-message time-limit reason)
      (agent-trace agent "task created to process: ~A" (message-format message))
      ;; if fn is nil, then agent does not want or cannot process request
      ;; quit, killing the newly created process
      (unless fn 
        (vformat "~&...~S: agent-ok-to-process-message function prevents from ~
                  processing message:~& ~S" (name agent) message)
        (agent-process-kill-current-process agent))
      (dformat :skill-header 0 "~2%;========== ~A: ~S =========="
               (key agent) (action message))
      ;; we create then a new task-frame describing the task being processed
      (setq current-task (agent-make-task-frame agent message))
      ;;=== when the agent is learning, we create a task-frame for recording
      ;; the result when the task will be finished
      (when (member :learning (features agent))
        ;; record the task being started (function in the task.lisp file)
        (agent-record-starting-task agent message))
      ;; add task-frame to the active task list
      (agent-add agent current-task 'active-task-list)
      ;; update agent window
      (agent-display agent)
      
;;;      ;; ... we set up a timer process, eventually using the default time.
;;;      ;; The default time-limit prevents dead task to clutter
;;;      ;; the agent active-task-list slot of agent. 
;;;      ;; I.e., all tasks are removed after 1 hr.
;;;      (setq time-limit (or (time-limit message) (default-time-limit *omas*)))
;;;      ;; It will be triggered at the specified date: (date + time-limit - current)
;;;      ;; time-limit may be a real number (fraction of a second (hence: ceiling)
;;;      (setq delay (+ (date message) (- (get-universal-time)) time-limit))
;;;      ;; record time-limit into current-task (in seconds)
;;;      (setf (time-limit current-task) 
;;;            (+ (date message) (ceiling time-limit)))
;;;      ;; next function also sets timer-process slot of current task frame
;;;      (set-time-limit-timer agent current-task delay)
      
      ;; then apply static skill (static exit returns two values)
      (multiple-value-setq (result reason)
        (apply fn agent 
               ;(environment current-task)
               message ; JPB0906
               (args message)))
      ;(format t "~%;+++ agent-process-request /result: ~S, reason: ~S" result reason)
      
      ;; Once the skill has been executed, reset the message variable
      (setq *current-message* nil)
      
      ;; if the result is the keyword :abort, which is the case when an agent has
      ;; the required skill but does not want to do the task, then we simply quit
      (when (eql :abort result)
        ;; we signal here in the graphics trace that the job was abandoned 
        (agent-display-abort agent)
        ;; check what we exactly did while executing the skill and undo it
        (cancel-current-task agent :task-frame current-task)
        ;; cancel never returns
        ;; no error message is returned to the sender, since this is not an error
        ;; condition but the decision not to do the job. However, the skill code
        ;; could send back an error message
        )
      
      ;; Here the skill has been executed and two cases may happen
      ;;   - the skill was atomic and we have a result
      ;;   - the skill has spawned subtasks and we must wait for their result
      (unless (subtask-list current-task)
        ;; OK no subtask we got the result
        ;; we could do better in case the agent that must receive the result is the same
        ;; prepare the answer message
        (setq answer-message (message-make-reply agent message result reason))
        ;; if the agent is learning, then we record the task data
        (when (member :learning (features agent))
          ;; record the task being started (function in the task.lisp file)
          (agent-remember-task agent answer-message))
        ;; send the result
        ;(format t "~%;+++ agent-process-request /sending answer message, process: ~S"
        ;  *current-process*)
        (send-message answer-message)
        ;; update display
        (agent-display agent)        
        ;; delete current task, indicating that there are no subtasks
        ;; however let the graphics trace display the timers before killing them
        (sleep 0.01) ; JPB0807
        (cancel-current-task agent :no-subtasks t :no-mark t)
        ;; cancel-current-task does not return (process is killed)
        )
      
      ;; here we spawned subtasks and must process answers as they come.
      ;; first check if we have a dynamic skill needed to process answers
      (setq fn (agent-get-dynamic-skill agent skill))
      (unless fn
        ;; we can't process the request
        ;; for the time being we declare an error. We should cancel the task and
        ;; return an error message to the sender
        (error "~A has no dynamic skill ~S, can't process the job" agent skill))
      ;; if we have a dynamic skill, then go process answers
      (agent-process-answers agent current-task fn)
      :done)))

;(trace (agent-process-request :step t))
;(untrace agent-process-request)

;;;--------------------------------------------------- AGENT-PROCESS-SAVED-ANSWERS

(defun agent-process-saved-answers (agent subtask-frame)
  "function called by a broadcast-timeout process to process eventual answers. ~
   It simply moves the non error answers to a list that will be processed by ~
   the task process. 
   When the  list is empty, then we are in a timeout condition.
Arguments:
   agent: agent
   subtask-frame: subtask attached to this process.
Return:
   NIL if no answer in the saved-answers slot
   does not return otherwise."
  (let (message-list current-task)
    ;; === first check whether it is too late or not
    ;; get main task frame 
    (setq current-task (parent-task subtask-frame))
    ;; if no more, then kill timer (task was either completed or aborted)
    (unless (and current-task subtask-frame) 
      (agent-process-kill-current-process agent))
    ;; filter out the answers that are errors
    (dolist (message (saved-answers subtask-frame))
      (unless (eql :error (contents message))
        (push message message-list)))
    
    ;; anything left?
    (when message-list
      ;; we put some info into the task-frame info slot, which triggers the
      ;; agent-process-answer function that was waiting on this slot
      (setf (processed-broadcast-info current-task)
            (append (processed-broadcast-info current-task)
                    (list (list message-list current-task subtask-frame))))
      ;; we cancel subtasks
      (send-message (message-make-cancel agent :all (id subtask-frame)))
      ;; we do not remove answers from subtask-frame. This could be useful
      ;; anyway the subtask-frame will be destroyed
      ;; kill timer, cleaning subtask-frame
      (setf (timeout-process subtask-frame) nil
            (timeout subtask-frame) nil)
      (agent-process-kill-current-process agent))
    
    ;; otherwise the function returns nil, that will trigger some more wait or
    ;; an error message
    (vformat "~&;~S;agent-process-saved-answers; no valid answer found before ~
              timeout for subtask ~S; we go try to extend timeout."
             (name agent) (id subtask-frame))
    nil))

;;;------------------------------------------- AGENT-PROCESS-SIMPLE-REQUEST-ANSWER

(defun agent-process-simple-request-answer 
       (agent message current-task-frame processed-subtask fn &aux result)
  "process an answer message for a subtask, corresponding to a simple request, ~
   i.e., a subtask addressed to a single agent with a simple protocol.
Arguments:
   agent: agent
   message: answer message
   current-task-frame: task being executed by the agent
   processed-subtask: subtask-frame of the current subtask
   fn: dynamic skill function for the message.
Return:
   :done or does not return (on abort from skill or on last answer)"
  ;; first kill timeout timer if any
  ;(describe current-task-frame)
  (when (timeout-process processed-subtask)
    (agent-trace agent "killing ~S timeout timer"
                 (agent-process-name agent 
                                     (timeout-process processed-subtask)))
    (agent-process-kill agent (timeout-process processed-subtask))
    ;; reset timeout process and timeout value
    (setf (timeout-process processed-subtask) nil
          (timeout processed-subtask) nil))
  
  ;; apply skill to the answer of message
  (setq result (funcall fn agent 
                        ;(environment current-task-frame)
                        message ; JPB0906
                        (contents message)))
  ;; if the result is the keyword :abort, which is the case when an agent has
  ;; the required skill but does not want to do the task, then we simply quit
  ;; In fact we should have 2 cases:
  ;;    :ignore to specify that the subtask is ignored, but does not affect
  ;;            the task itself (unnecessary anything but :abort is ignored)
  ;;    :abort  to indicate that the subtask is ignored, but the task cannot be
  ;;            completed as such and must be aborted
  (when (eql :abort result)
    ;; current process is killed
    (abort-current-task agent)
    ;; safety feature
    (error "the current process ~S should have been killed" *current-process*))
  
  ;; standard case: we update subtask-list, removing the processed subtask
  (setf (subtask-list current-task-frame)
        (remove-if #'(lambda (xx)(eql (id xx)(task-id message)))
                   (subtask-list current-task-frame)))
  ;; also cancel other jobs that could be processing the same subtask "in case"
  ;; no, if this is a simple request, then only one agent did the job
  ;(send-message (message-make-cancel agent 'ALL (task-id message)))
  ;; if we had the last answer, then the skill will have called the
  ;; dynamic exit function killing the current process. In this case we do not
  ;; return from the fn call
  :done)

;;;-------------------------------------------------- AGENT-PROCESS-SUBTASK-ANSWER

(defun agent-process-subtask-answer (agent current-task-frame fn)
  "called by agent-process-answers to process an answer from the saved-answer-list ~
   of the subtask frame.
   If it is a 
   - PTP answer, then calls agent-process-simple-request-answer
   - broadcast with strategy :take-first-answer, then call agent-process-first-answer
   - broadcast with strategy :collect-answers, then put the message ~
   into the saved-answers list of the subtask-frame.
   - broadcast with strategy (:collect-first-n-answers nn), decreases the number of ~
   answers to receive and if zero processes the list of answers
Arguments:
   agent: agent
   current-task-frame: task-frame of task currently run by the agent
   fn: dynamic part of the skill
Return:
   ?"
  (let (subcontractors processed-subtask message max strategy)
    ;;===== gotcha!
    ;; select some message (currently oldest time stamp) from saved-answer-list
    ;; normally there should be only one message in the box
    (setq message (agent-select-answer-message agent current-task-frame))
    (agent-trace agent "processing the answer: ~S" (message-format message))
    ;; indicate that we are processing an answer (slot eventually used by skill)
    (setf (answer-being-processed current-task-frame) message)
    ;(format t "~%;===== agent-process-subtask-answer /subtask-id: ~S"
    ;  (task-id message))
    ;; recover the subtask frame from the subtask-list
    (setq processed-subtask
          (agent-get-subtask-frame-from-subtask-id agent (task-id message)
                                                   current-task-frame))
    ;(format t "~%; agent-process-subtask-answer /current-task-frame:~%;  ~S~
    ;           ~%; ...processed-subtask:~%;  ~S"
    ;  current-task-frame processed-subtask)
    ;; if subtask is gone, then somebody has already answered it and the cancel
    ;; message maybe came in late... In that case we quit, doing nothing
    (unless processed-subtask
      (return-from agent-process-subtask-answer))
    
    ;; note the strategy if any
    (setq strategy (strategy (message processed-subtask)))
    ;(print `(++++ ,(to! processed-subtask) ,strategy ))

    (cond
     ((null processed-subtask)
      ;; here the answer task-id does not correspond to a pending subtask
      ;; it can only mean that it is an answer to an old subtask that was already
      ;;  processed. Thus, we can safely ignore it.
      (agent-trace agent 
                   "warning: task-id ~S of answer message is not in the pending ~
                    subtask list:~& ~S~&; answer is ignored..." 
                   (task-id message) (mapcar #'id (subtask-list current-task-frame)))
      )

     ;; here we got an answer for one of the pending subtasks, what we do
     ;; depends on how the subtask was called (simple request, broadcast or 
     ;; multicast, take-first answer or collect-answers strategy,...)
     ;; simple request? Receiver was a single agent?
     ;((typep (setq subcontractors (to! processed-subtask)) 'AGENT)
     ;; since agent can be a non local agent, we can but test for a name
     ;; remember keywords are also symbols (interned into the keyword package)
     ((and (symbolp (setq subcontractors (to! processed-subtask)))
           (not (member subcontractors '(:all :all-and-me))))
      ;; yes, go process simple PTP request (don't forget to kill timeout)
      (agent-process-simple-request-answer agent message current-task-frame 
                                           processed-subtask fn))

     ;; broadcast or multicast and strategy is "take-first-answer" 
     ;; broadcast strategy is now part of the subtask message 
     ((and (or (listp subcontractors)
               (member subcontractors '(:all :all-and-me)))
           (eql strategy :take-first-answer))
      ;     (eql *broadcast-strategy* :take-first-answer))
      ;; same as simple request go process it (don't forget to kill timeout)
      (agent-process-first-answer agent message current-task-frame 
                                  processed-subtask fn))

     ;; broadcast or multicast with a "collect-answers" strategy
     ((and (or (listp subcontractors)
               (member subcontractors '(:all :all-and-me)))
           (eql strategy :collect-answers))
      ;     (eql *broadcast-strategy* :collect-answers))
      ;; collect answers and stay in the loop
      ;; answers will be processed by the timeout process when it fires
      (setf (saved-answers processed-subtask)
            (append (saved-answers processed-subtask) (list message))))

     ;; broadcast or multicast with a (:collect-first-n-answers nn) strategy
     ;; if number of messages to receive is le 0, then go process answer
     ;; otherwise do as in the previous case and decrease count
     ((and (or (listp subcontractors)
               (member subcontractors '(:all :all-and-me)))
           (listp strategy)
           (eql (car strategy) :collect-first-n-answers))
      
      ;; if message is an error message we decrease max count and check if it was
      ;; the last one JPB1011
      (when (eql :error (contents message))
        ;; if we have a number in broadcast-max-answers, decrease it
        (when (numberp (broadcast-max-answers processed-subtask))
          (decf (broadcast-max-answers processed-subtask))
          ;; check if it was the last message
          (if (<= (broadcast-max-answers processed-subtask) 0)
              ;; if so go process saved answers
              (agent-process-broadcast-first-n-answers agent current-task-frame 
                                                       processed-subtask fn))))
      
      (unless (eql :error (contents message))
        ;; set number of answers still to receive (1 if not initialized)
        ;; here we could have put a large number, letting the process wait for a
        ;; timeout
        ;(format t "~&;agent-process-subtask-answer/ broadcast-max-answers: ~S" 
        ;        (broadcast-max-answers processed-subtask))
        (setq max (or (broadcast-max-answers processed-subtask) 1))
        ;(format t "~&;agent-process-subtask-answer/ max: ~S" max)
        ;; save answer
        (setf (saved-answers processed-subtask)
                (append (saved-answers processed-subtask) (list message)))
        ;; decrease number of answers still to receive
        (decf max)
        ;; if number of answers becomes zero (or less), process answers
        (if (<= max 0)
          ;; task process is waiting on the processed-broadcast-info slot of the ~
          ;; current task-frame. Thus the following function must emulate the ~
          ;; agent-process-broadcast timeout behavior.
          (agent-process-broadcast-first-n-answers agent current-task-frame 
                                                   processed-subtask fn)
          )
        ;; save expected number of answers
        (setf (broadcast-max-answers processed-subtask) max)
        ))

     ;; otherwise unknown case
     (t
      (error "agent ~S got an answer to a specific subtask ~S and we ~
              cannnot determine how the message was sent: ~&~S"
             (name agent) (id processed-subtask) message)))
    ;(print `(++++ ,(saved-answers processed-subtask)))
    :done))

;(trace agent-process-subtask-answer)
;(untrace agent-process-subtask-answer)

;;;------------------------------------------------------------ AGENT-REMOVE-SKILL

(defun agent-remove-skill (agent skill-name)
  "removes the entry corresponding to the skill from the list of skills.
Arguments:
   agent: agent
   skill-name: name of the skill that should be removed from the list of skills."
  (setf (skills agent)
        (remove-if #'(lambda (xx) (eql (name xx) skill-name)) 
                   (skills agent))))

;;;--------------------------------------------- AGENT-REPLACE-TASK-OBJECT-IN-LIST
;(trace agent-replace-task-object-in-list)
;(untrace agent-replace-task-object-in-list)

(defun agent-replace-task-object-in-list (task-info task-list)
  "used by agent-replace-task-info to replace an entry on the task-info list.
Arguments:
   task-info: new entry
   task-list: list of task-info entries."
  (cond
   ((null task-list)(list task-info))
   ((eql (id (car task-list)) (id task-info)) (cons task-info (cdr task-list)))
   (t (cons (car task-list) 
            (agent-replace-task-object-in-list task-info (cdr task-list))))))

;;;--------------------------------------------------- AGENT-SELECT-AGENDA-MESSAGE

(defun agent-select-agenda-message (agent)
  "should allow to implement fancy algorithms to select messages in the agenda.
   Currently selects the oldest from the list."
  (let ((time (get-universal-time))
        selected-message)
    (dolist (message (agenda agent))
      (when (<= (date message) time)
        (setq time (date message)
              selected-message message)))
    (%agent-remove-value agent selected-message :agenda)
    selected-message))

;;;--------------------------------------------------- AGENT-SELECT-ANSWER-MESSAGE
;;; maybe we should select :inform messages first, but it is not really justified

(defun agent-select-answer-message (agent &optional task-frame)
  "currently selects the oldest message in the bin, regardless of its type.
Arguments:
   agent: agent
   task-frame (opt): task-frame of the agent if known.
Return:
   a message"
  (let ((time (get-universal-time))
        (current-task-frame 
         (or task-frame
             (agent-process-get-task-frame agent *current-process*)))
        selected-message)
    (unless current-task-frame 
      (error "something wrong in ~S no task-frame for current process" 
             *current-process*))
    ;; loop to select oldest message
    (dolist (message (saved-answer-list current-task-frame))
      ;(print (list "message date" (date message) "time" time))
      (when (<= (date message) time)
        (setq time (date message)
              selected-message message)))
    (setf (saved-answer-list current-task-frame) 
          (remove selected-message (saved-answer-list current-task-frame) 
                  :test #'equal))
    selected-message))

;;;------------------------------------------------------ AGENT-SELECT-BEST-ANSWER

(defun agent-select-best-answer (agent answer-list)
  "currently selects a random message in the list.
Arguments:
   agent: agent
   answer-list: list to choose from."
  (declare (ignore agent))
  (nth (random (length answer-list)) answer-list))


(format t "~&;*** OMAS v~A - agent function library loaded (control) ***" 
        *omas-version-number*)
		
;;; :EOF