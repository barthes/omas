;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;24/10/18
;;;                G L O B A L S  (file omas-globals.lisp)
;;;
;;;===============================================================================
;;; (c) Jean-Paul Barthès@HEUDIASYC, CNRS, UTC. See LICENSE file for permissions.

#| This file contains all the global variables needed for a OMAS platform that are common to several files and the global functions that are not directly linked to the functioning of an agent and that are not part of the API (e.g., the net interface mechanism).
Globals only used in a single file should be defined in that file unless used by
several agents at the same time.

It also contains macros and service functions that are not part of the public API

History

2024
 1016 adapting to Windows 10
2023
 0715 we added suppress-list to avoid drawing unwanted messages
|#

(in-package :omas)

;;;================================= SITE CLASS ==================================
;;; the values included in the description of the site should be values that are 
;;; not better represented by global variables. E.g. constants or variables that
;;; grow or shrink but do not change radically when passing from a process to 
;;; another one

(defClass site ()
  (;; used W-init-P
   ;(application-name :accessor application-name :initform nil) ; e.g. :FAC
   ;; agent being loaded filled by make-agent and used by load-agent
   (agent-being-loaded :accessor agent-being-loaded :initform nil)
   ;; record the initial active processes (used when resetting omas)
   (basic-processes :accessor basic-processes :initform mp:*all-processes*)
   ;; 3 seconds default timeout for broadcast or multicast
   (broadcast-default-timeout :accessor broadcast-default-timeout :initform 1)
   ;; specify contract-net strategy. Can be :take-first-bid or :collect-bids
   (contract-net-strategy :accessor contract-net-strategy :initform :take-first-bid)
   ;; control-panel. If true opens control panel after application is loaded
   (control-panel :accessor control-panel :initform T)
   ;; used W-init-P. Coterie-name is the same as application-name
   (coterie-name :accessor coterie-name :initform nil) ; e.g. "NEWS" from ODIN-NEWS
   ;; used by dformat, default is to trace loading files
   (debugging :accessor debugging :initform '(:load)) ; JPB 1504
   ;; used to record info about the trace flags
   (debugging-tags :accessor debugging-tags :initform nil)
   ;; used by dformat the higher the more detailed the trace
   (debugging-trace-level :accessor debugging-trace-level :initform 0)
   ;; unused
   (default-broadcast-repeat-count :accessor default-broadcast-repeat-count 
     :initform 3) ; should be constant across an application
   ;; unused   
   (default-broadcast-strategy :accessor default-broadcast-strategy 
     :initform :take-first-answer)
   ;; time to wait for an answer to a call-for-bids: 1 s
   (default-call-for-bids-timeout-delay 
     :accessor default-call-for-bids-timeout-delay :initform 1)
   ;; number of times we renew our wait when no bids present.
   (default-call-for-bids-repeat-count :accessor default-call-for-bids-repeat-count 
     :initform 0)
   ;; unused
   (default-contract-net-strategy :accessor default-contract-net-strategy 
     :initform :take-first-bid)
   ;; high number: 10 hr for a default execution time
   (default-execution-time :accessor default-execution-time :initform 36000)
   ;; default-language is English
   (default-language :accessor default-language :initform :EN)
   ;; messages, control: default time-limit on all tasks to avoid dead tasks
   (default-time-limit :accessor default-time-limit :initform 3600)
   ;; display timeout processes on graphics window
   (drawing-timeouts :accessor drawing-timeouts :initform t)
   ;; unused
   (display-command-queue :accessor display-command-queue :initform nil) ; obsolete
   ;; list of temporary windows created to edit messages
   (edit-message-window-list :accessor edit-message-window-list :initform nil)
   ;; list of answers associated with gates
   (gate-answers :accessor gate-answers :initform nil)
   ;; counter for creating new gates across the omas environment
   (gate-counter :accessor gate-counter :initform 0)
   ;; list of pairs (<gate> . <answer>) implementing local synchronization
   (gate-list :accessor gate-list :initform nil)
   ;; control-panel-P, spy-P: if t, bids are not displayed on the graphics trace.
   (graphics-no-bids :accessor graphics-no-bids :initform nil) ; fairly local
   ;; if true opens the graphics window after loading application
   (graphics-window :accessor graphics-window :initform nil)
   ;; "1000000 seconds a little less than 2 yr"
   (highest-time-limit :accessor highest-time-limit :initform 60000000)
   ;; http server (only one server by Lisp session
   (http-server :accessor http-server :initform nil)
   ;; server port (default 80)
   (http-server-port :accessor http-server-port :initform 80)
   ;; number of agents using the server (reference count)
   (http-server-reference-count :accessor http-server-reference-count :initform 0)
   ;; size of buffer for receiving messages
   (ktransferbuffersize :accessor ktransferbuffersize :initform 4096)
   ;; authorized languages
   (languages :accessor languages :initform '(":EN"))
   (last-drawn-message :accessor last-drawn-message :initform nil) ; ?
   (last-printed-message :accessor last-printed-message :initform nil) ; ?
   ;; agents, asistants, postman, API, control-panel, W-assistant-P, W-init-P, W-spy-P
   ;; a-list identifying local agents objects on given machine (:FAC . #<AGENT FAC>)
   (local-agents :accessor local-agents :initform nil) 
   ;; net-UDP-P, W-init-P
   (local-broadcast-address :accessor local-broadcast-address :initform nil) ; constant
   ;; set when loading and used by all agents
   (local-ip-address :accessor local-ip-address :initform nil) ; constant
   ;; unused
   (local-reference :accessor local-reference :initform nil) ; e.g. "ODIN"
   ;; local user associated with the panel, e.g. :<ODIN-USER>
   (local-user :accessor local-user :initform :<user>)
   ;; counter to produce message ids
   (message-id-counter :accessor message-id-counter :initform (random 999999))
   ;; dictionary is built automatically from the message properties and remains constant
   ;; netcomm, messages
   (message-property-dictionary :accessor message-property-dictionary :initform nil)
   ;; number of times a message will be repeated after a timeout
   (max-repeat-count :accessor max-repeat-count :initform 0)
   ;; agents, assistants, postman: list of known agent-names, e.g. (:FAC :MUL-1)
   (names-of-known-agents :accessor names-of-known-agents :initform nil) ; for SPY
   ;; agents, control-panel-P spy-P
   ;; list of agents to be displayed in the graphics window, a list of keys
   (names-of-agents-to-display :accessor names-of-agents-to-display :initform nil) ; id
   ;; next if nil, no messages are broadcast on the net loop
   ;; API, init-P
   (net-broadcast :accessor net-broadcast :initform nil)
   ;; process that handles reassembling pieces of messages and dispatching
   ;; Launched by net-initialize-broadcast
   (net-dispatch-process :accessor net-dispatch-process :initform nil)
   ;; used by the demarshalling process
   ;; net-UDP-P
   (net-incoming-message-stack :accessor net-incoming-message-stack :initform nil)
   ;; list of task-frames corresponding to processes trying to reassemble messages
   (net-input-task-list :accessor net-input-task-list :initform nil)
   ;; for receiving local coterie net messages
   ;; net-UDP-P
   (net-receive-process :accessor net-receive-process :initform nil)
   ;; for sending messages to local loop
   ;; unused
   (net-send-process :accessor net-send-process :initform nil)
   ;; application directory (set in init-window) used by make-moss-agent
   (omas-application-directory :accessor omas-application-directory :initform nil)
   ;; automatic. If true, Application is loaded automatically (no init window)
   (omas-automatic :accessor omas-automatic :initform nil)
   ;; application database containing ontologies and KB of persistent agents
   (omas-database :accessor omas-database :initform nil)
   ;; omas-directory
   ;; W-init-P
   (omas-directory :accessor omas-directory :initform nil :initarg :omas-directory)
   ;; unused
   (omas-listener :accessor omas-listener :initform *standard-output*) ; ?
   ;; toggles the debugging trace
   (omas-verbose :accessor omas-verbose :initform nil) ; should be T for SPY
   ;; net-UDP-P W-init-P
   (omas-port :accessor omas-port :initform nil) ; for local broadcast
   ;; messages W-control-panel-P: OMAS local control panel
   (omas-window :accessor omas-window :initform nil)
   ;; type of omas-window :small :large :user-defined
   (omas-window-type :accessor omas-window-type :initform :small)
   ;; ontology for normalizing concepts: entries are a concept associated with 
   ;; generic concepts. Useful for normalizing skills
   (ontology :accessor ontology :initform nil)
   ;; for saving database pheap when opened (persistency using Wood store mechanism)
   (pheap :accessor pheap :initform nil)
   ;; used in W-agent-P
   (screen :accessor screen :initform (cg:screen cg:*system*)) ; JPB0806
   ;; unused
   (security-timeout :accessor security-timeout :initform 3600) ; 1hr
   ;; used by init to record application parameters
   (site-name :accessor site-name :initform "*unknown*")
   ;; net-UDP-P
   (socket :accessor socket :initform nil) ; for ACL (net socket)
   ;; SPY name, e.g. :SPY-123456 where 123456 is a random number
   (spy-name :accessor spy-name :initform :undefined)
   ;; API:number generator for subtask names e.g. FAC-ST-0, FAC-ST-1,...
   (subtask-number :accessor subtask-number :initform 0)
   ;; note here info to avoid drawing unnecessary messages
   (suppress-list :accessor suppress-list :initform nil)
   ;; API: number generator for task names e.g. T-0, T-1,...
   (task-number :accessor task-number :initform 0)
   ;; threshold to cut tasks selection in the process task dialog
   (task-threshold :accessor task-threshold :initform 0.4)
   ;; a global couter to produce temporary ids when editing objects
   ;; such objects last only for a session, hence the initform may be set to 0
   (temp-id-counter :accessor temp-id-counter :initform 0)
   ;; string for testing net connections (at very low level)
   (test-string :accessor test-string :initform "Ceci est un test...")
   (text-window :accessor text-window :initform nil) ; window to print info
   ;; for printing conversations 
   ;; unused by ACL
   (traced-agents :accessor traced-agents :initform nil) ; agents that are traced
   ;; W-control-panel-P, spy-P: flag: if true messages are traced
   (trace-messages :accessor trace-messages :initform t) ; for SPY
   ;; if true traces the execution of skills
   (trace-skills :accessor trace-skills :initform t)
   ;; maximal length of UDP messages on the net (used to do tests)
   (udp-max-message-length :accessor udp-max-message-length :initform 512)
   ;; W-control-panel-P W-message-P: alist of test messages produced by the user
   (user-messages :accessor user-messages :initform nil) ; application messages
   ;; a list of user-IP pairs for web access
   (users :accessor users :initform nil)
   ;; W-init-P, load-omas-moss
   (voice-interaction :accessor voice-interaction :initform nil) ; voice flag
   ;; web editor: 10 seconds to see if agent exists
   (web-hello-timeout :accessor web-hello-timeout :initform 10)
   ;; web editor: 10 seconds to create object
   (web-create-timeout :accessor web-create-timeout :initform 20)
   ;; web editor: defparameter10 seconds to delete object
   (web-delete-timeout :accessor web-delete-timeout :initform 10)
   ;; web editor: 10 seconds to post an object
   (web-post-timeout :accessor web-post-timeout :initform 10)
   ;; web editor: 10 seconds to update object
   (web-update-timeout :accessor web-update-timeout :initform 20)
   ;; web editor: 10 seconds to get a page description
   (web-get-page-timeout :accessor web-get-page-timeout :initform 10)
   )
  (:documentation "site object containing global OMAS parameters. A site is an
                   environment located on a particular machine. As such it is 
                   part of a coterie."))

(defParameter *omas* 
  (make-instance 'site 
    :omas-directory *omas-directory-string*) 
  "contains main globals")

;;; it should be enough to pass this object to the various processes for getting
;;; to the shared global values

;;; get the local host address to be used later by agents
(eval-when (:load-toplevel)
  (setf (local-ip-address *omas*)
    (socket:ipaddr-to-dotted
         (socket:lookup-hostname (excl.osi:gethostname)))))

;;; add omas-exit to the list of functions that must be executed before exiting
;(pushnew 'omas-exit *lisp-cleanup-functions*)

;;; this variable is imported from the agents.lisp file defining the application
;;; when the application is loaded it contains the list of the application

;;; local agents and is used to load them in the load-application function

(defVar *local-coterie-agents* ())

;;;===============================================================================
;;; Other Variables
;;;===============================================================================

(eval-when (compile load eval)
  (import '(mp:*current-process*
            mp:process-kill
            mp:process-wait
            mp:process-wait-with-timeout
            )))

;;;============================= Display Windows =================================

(defParameter *color-list*
  ;;; When in need use (ask-user-for-color)
  '((:black . #.cg:black) 
    (:blue . #.cg:blue) 
    (:green . #.cg:green) 
    (:red . #.cg:red) 
    (:yellow . #.cg:yellow)
    (:grey . #.(cg:make-rgb :RED 128 :GREEN 128 :BLUE 128))
    (:light-blue . #.(cg:make-rgb :RED 66 :GREEN 160 :BLUE 255))
    (:light-green . #.(cg:make-rgb :RED 0 :GREEN 255 :BLUE 64))
    (:orange . #.(cg:make-rgb :RED 255 :GREEN 128 :BLUE 0))
    (:tan . #.(cg:make-rgb :RED 128 :GREEN 64 :BLUE 0)))
  )

(defUn %color-from-key (key)
  (cdr (assoc key *color-list*)))


(format t "~&;*** OMAS v~A - globals loaded ***" *omas-version-number*)

;;; :EOF
