;;;-*- Mode: Lisp; Package: "WEB" -*-
;;;==============================================================================
;;;19/04/12
;;;                          O M A S - AGENT WEB
;;;
;;;==============================================================================
;;; Copied from ACE4SD-WEB developed by Wanderley, to connect ML to the Google 
;;; vocal interface. Needs the webres folder for defining the web pages.

#|
Copyright: Wanderley@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC 
(July 2018)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
2019
 0412 receiving message from browser and forwarding it with an :inform message
      other than a request message
|#

(eval-when (compile load eval)
  ;; uses hunchensocket because Allegro does not handle https (?)
  (ql:quickload 'hunchensocket)
  )

  
(defpackage :WEB
  (:use :cl :moss :omas :excl)
  (:nicknames :aw))
  
(in-package :WEB)

(omas::defagent :WEB)


(defparameter *web-server-port* 12345 "server port for serving the WEB forms.")

;;;===============================================================================
;;; 
;;;                              WebSockets - classes
;;;
;;;===============================================================================
;; Classes symbolize "channels", i.e. sockets that users and the server use to 
;; communicate

;;;--------------------------------------------------------------- CLASS CHAT-ROOM
(defclass chat-room (hunchensocket:websocket-resource)
  ((name :initarg :name :initform (error "Name this room!") :reader name))
  (:default-initargs :client-class 'user))


;;;-------------------------------------------------------------------- CLASS USER
(defclass user (hunchensocket:websocket-client)
  ((name :initarg :Sec-WebSocket-Protocol :reader name 
         :initform (error "Name this user!"))))


;;;===============================================================================
;;; 
;;;                Websockets - parameters, methods and functions
;;;
;;;===============================================================================

;; Instantiate the "channels" (sockets) for each resource
(defparameter *chat-rooms* (list (make-instance 'chat-room :name "/manager")))

;hunchensocket:websocket-acceptor

;; The server needs to be SSL for handling the connections
(defparameter *server* 
  (make-instance 'hunchensocket:websocket-acceptor :port *web-server-port*
         ;:SSL-CERTIFICATE-FILE (string+ "C:" (namestring (omas::omas-application-directory *omas*)) "webres/server.pem")
         ;:SSL-PRIVATEKEY-FILE (string+ "C:" (namestring (omas::omas-application-directory *omas*)) "webres/server.pem")
    :document-root 
    (string+ "C:" (namestring (omas::omas-application-directory *omas*)) "webres/")))

(defun find-room (request)
  (find (hunchentoot:script-name request) *chat-rooms* :test #'string= :key #'name))
	
(defun broadcast-chat (room message &rest args)
  (loop for peer in (hunchensocket:clients room)
        do (hunchensocket:send-text-message peer (apply #'format nil message args))))


;;;-------------------------------------------------------------- CLIENT-CONNECTED
(defmethod hunchensocket:client-connected ((room chat-room) user)
  "prints the name of the user connected to this chat-room."  
  (format t "~%[WEB]: ~S is connected!~%" (name user)) 
  
  )

;;;------------------------------------------------------- RECEIVE-CLIENT-MESSAGE
;; Receive messages from clients

(defmethod hunchensocket:text-message-received ((room chat-room) user message)
  "recover here a message from the browser and transfer it to ML"
  (let (msg)
    ;; Message received from:
    (format t "~%[WEB] received the message ~S from ~S~%" message (name user)) 
    
    ;; transfers the received message to some agent, for instance, a given PA
    (setq msg (make-instance 'omas::message
                :name :browser-message
                :type :inform ; rather than :request JPB 190412 
                :from :WEB
                :to :ML
                :action :RECEIVE-BROWSER
                :args `(:data ,message)))
    (send-message msg)
    ))

;;;===============================================================================
;;; 
;;;                                    skills 
;;;
;;;===============================================================================

;;;========================================================================= skill
;;;                               :SEND-TO-BROWSER
;;;===============================================================================
;;; This skills is used to send messages to the Browser

(defskill :SEND-TO-BROWSER :WEB
:static-fcn static-send-to-browser)

(defun static-send-to-browser (agent message &key data)
  (declare (ignore message)(special *chat-rooms*))
  ;; Search for the client 
  ;; for each "channel/page" in the server
  (dolist (xx *chat-rooms*) 
    ;; for each client connected to the "channel/page"
    (dolist (yy (hunchensocket:clients xx))
      ;; if it is the desired client        
      (if (equal+ "VocalBrowser"  (name yy))
          ;; if it has found the Browser object, then send the message
          (hunchensocket:send-text-message yy data)
        )
      )
    )
  (static-exit agent :done))

;;;========================================================================= skill
;;;                               :START-SERVER
;;;===============================================================================
;; This skill starts the web server
(defskill :START-SERVER :WEB
:static-fcn static-start-server)

(defun static-start-server (agent message)
  (declare (ignore message)
           (special *server* hunchensocket:*websocket-dispatch-table*))
  (pushnew 'find-room hunchensocket:*websocket-dispatch-table*)
  (hunchentoot:start *server*)
  (format t "~%[WEB]:START-SERVER - \"Websockets server has been started.\"~%")
  (static-exit agent :done))



;;;========================================================================= skill
;;;                               :STOP-SERVER
;;;===============================================================================
;;; This skills stops the web server

(defskill :STOP-SERVER :WEB
:static-fcn static-stop-server)

(defun static-stop-server (agent message)
  (declare (ignore message)(pecial *server*))
  (hunchentoot:stop *server*)
  (static-exit agent :done))

			
;;;==============================================================================
;;; 
;;;                                    goals
;;;
;;;==============================================================================
;;; Goal to start the server a few seconds after the agent has been created

(defgoal :start-web-server :WEB
  :activation-delay 3 
  :script start-web-server-script)

(defUn start-web-server-script (agent)
  "send a request message to start the web server"
  (declare (ignore agent))
  (list (make-instance 'omas::message :type :request :to :WEB
          :action :start-server)))

;;; ================================== Setup messages ===========================

;;; none

(format t "~&;*** OMAS v~A - agent WEB loaded ***" omas::*omas-version-number*)

;;; :EOF