﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;                O M A S - W - T E X T  (file W-text.lisp)
;;;
;;;===============================================================================
;;; This file contains all the functions related to the text window for tracing
;;; the messages in an OMAS execution.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
Builds a text window under the graphics window and displays the text trace. Uses
the same principle as the graphics window for the display-list and the scrolling.
Arrows are color-coded, Text can be printed underneath.

2020
 0202 restructuring OMAS as an asdf system
|#

(in-package :omas)

;;;================================ Globals ======================================
;;; screen size
;;;(defParameter *screen-width* (width *screen*))
;;;(defParameter *screen-height* (height *screen*))

;;; kludge for new multilisp windows
(defparameter *kludge-width* (if (boundp '*OP-width*)
                           *OP-width*
                         omas::*panel-width*
                         ))

(defparameter *kludge-height-offset* (if (boundp '*height-offset*)
                           *height-offset*
                         omas::*panel-height-offset*))
    
;;; kludge to compensate the removal of graphics file
(defParameter cg-user::*gw-h-position* (+ *kludge-width* 2))
(defParameter cg-user::*gw-width* (- (cg:width (cg:screen cg:*system*))
                                     *kludge-width* 10))
(defParameter cg-user::*gw-height* 500)

(defparameter *text-window* nil)
(defparameter *text-pane* nil)
(defparameter *screen* (cg:screen cg:*system*))
(defParameter *tw-h-position* cg-user::*gw-h-position*)
(defParameter *tw-width* cg-user::*gw-width*)
(defParameter *tw-height* (- (cg:height *screen*) cg-user::*gw-height* 
                             *kludge-height-offset* 26))
(defparameter *tw-exterior* 
  (cg:make-box *TW-H-POSITION* 
               (+ *kludge-height-offset* cg-user::*gw-height*)
               (+ *TW-H-POSITION* *tw-width*) 
               (- (cg:height *screen*) 26)))


;;; list to repaint graphics window
(defParameter *tw-display-list* ())

(defParameter *tw-v-offset* 0)
(defparameter *tw-last-v-position* 0 "current drawing position")

;;;========================= External Functions ==================================
;;; functions called from processes other than the display process

;;;-------------------------------------------------------------- OMAS::TEXT-TRACE

(defun omas::text-trace (text &optional (color black))
  (declare (ignore color))
  (if (omas::text-window omas::*omas*) (format *text-pane* text)))

;;;============================= Text window =====================================

(defclass TEXT-WINDOW (cg:text-edit-window)
    ()
    (:documentation "window to print detailed trace"))
  
(defun make-text-window
    (&KEY PARENT (OWNER (OR PARENT (cg:SCREEN cg:*SYSTEM*)))
          (NAME :text-window)
          (TITLE "Text Trace")
          (BORDER :FRAME) (CHILD-P NIL) FORM-P
          &aux win)
  (declare (special *text-pane*))
  (unless (omas::text-window omas::*omas*)
    (setf win
           (cg:MAKE-WINDOW 
               NAME 
             :OWNER OWNER
             :CLASS 'text-window
             :EXTERIOR *tw-exterior*
             :BORDER BORDER
             :CHILD-P CHILD-P
             :CLOSE-BUTTON T
             :CURSOR-NAME :ARROW-CURSOR
             :FONT (cg:MAKE-FONT-EX :SWISS "MS Sans Serif / ANSI" 11 NIL)
             :FORM-STATE :NORMAL
             :MAXIMIZE-BUTTON T
             :MINIMIZE-BUTTON T
             :NAME :text-window
             :POP-UP NIL
             :RESIZABLE T
             :SCROLLBARS :vertical
             :STATE :NORMAL
             :STATUS-BAR NIL
             :SYSTEM-MENU T
             :TITLE TITLE
             :TITLE-BAR T
             :TOOLBAR NIL
             ))
    (setq *text-pane* (cg:frame-child win))
    (setf (omas::text-window omas::*omas*) win)))

;(progn (setq omas::*text-window* nil) (make-text-window))
;(format omas::*text-pane* "~& albert")

;;; must be defined in the cg: package
(defmethod cg:user-close ((ww text-window))
  (call-next-method)
  (setf (omas::text-window omas::*omas*) nil
    *text-pane* nil)
  ;; remove check in the omas panel
  (setf (cg:value (cg:find-component :verbose (omas-window *omas*))) nil)
  )

;;;-------------------------------------------------------------- TW-ADJUST-SCROLL

(defun TW-adjust-scroll ()
  "checks if we are printing near the bottom of the text window, or outside ~
      the visible part of the screen. If so, then scrolls the window up some.
Arguments: none."
  (let ((pane (tw-pane))
        inside-drawing-position)
    (when pane
      (setq inside-drawing-position (- *TW-last-v-position* *TW-v-offset*))
      
      ;; when position is outside visible part of the window, move the text up
      (when (> inside-drawing-position (height pane))
        ;; set offset so that we have some space
        (setq *TW-v-offset* (- *TW-LAST-V-POSITION*
                               (floor (* 3/4 (height pane)))))
        ;; redraw window
        (invalidate pane)
        ;(update-window pane)
        (return-from TW-adjust-scroll *TW-V-OFFSET*))
      
      ;; otherwise, we are visible, but when within last 10% of window, scroll up
      (when (>= inside-drawing-position (floor (* 9/10 (height pane))))
        ;; move up 1/4 pane
        (incf *TW-v-offset* (floor (* 1/4 (height pane))))
        ;; redraw window
        (invalidate pane)
        ;(update-window pane)
        ))
    :done))

;;;---------------------------------------------------------- TW-DRAW-ON-REDISPLAY

(defun TW-draw-on-redisplay (widget stream)
  "called to update display"
  (declare (ignore widget))
  (when *tw-display-list*
    (let ((display-list (reverse *tw-display-list*)))
      (dolist (item display-list)
        (when (>= (car item) *TW-V-offset*)
          (case (cadr item)
            (TEXT
             (with-foreground-color (stream (cadddr item))
               (format stream (caddr item))
               )
             )))))))

;;;--------------------------------------------------------- TW-DRAW-STRING-IN-BOX
;;; not really useful, except maybe to wrap text when printing on several lines...

(defun TW-draw-string-in-box (stream string start end text-box horizontal-justification 
                                     vertical-justification )
  "adapter to take into account scroll offset"
  (let ((left (box-left text-box))
        (top (box-top text-box))
        (right (box-right text-box))
        (bottom (box-bottom text-box))
        box)
    ;; make a new box at offset
    (setq box (make-box left (- top *TW-v-offset*) right (- bottom *TW-v-offset*)))
    ;; draw it
    (draw-string-in-box stream string start end box horizontal-justification 
                        vertical-justification)))

;;;------------------------------------------------------------------ TW-DRAW-TEXT

(defun TW-draw-text (text &optional (color black))
  "display text in the graphics box and record it into the display list and note ~
   the scroll position.
Arguments:
   text: text to display
   color (opt): color of text to print.
Return:
   current vertical position."
  (when (omas::text-window omas::*omas*)
    (let ((stream (tw-stream)))
      ;; check whether enough room
      (TW-adjust-scroll)
      ;; first print text
      (with-foreground-color (stream color)
        (format stream text))
      ;; record text
      (push (list *tw-last-v-position* 'TEXT text color) *TW-display-list*)
      ;; update vertical position
      (setq *tw-last-v-position* (+ *tw-v-offset* (current-position-y stream)))
      )))

;;;----------------------------------------------------------------------- TW-PANE

(defun TW-pane ()
  "get the drawing pane for the text window, when present."
  (find-component :draw (omas::text-window omas::*omas*)))

;;;--------------------------------------------------------------------- TW-SCROLL

(defun TW-scroll (widget new-value old-value)
  "scroll text window up 1/4 page when reaching the bottom of the page."
  (declare (ignore old-value))
  (let ((pane (find-sibling :draw widget))
        (max-position *TW-last-v-position*))
    ;; add some room after max-position
    (incf max-position (floor (* 1/10 (height pane))))
    (when pane
      (setq *tw-v-offset* (floor (/ (* new-value max-position) 100)))
      (invalidate pane)
      ;(update-window pane)
      ))
  t)

;;;--------------------------------------------------------------------- TW-STREAM

(defun TW-stream ()
  "get the drawing stream for the text window.
Arguments:
   none
Return:
   stream object to draw or print, nil when window does not exist."
  (let ((pane  (find-component :draw (omas::text-window omas::*omas*))))
    (when pane (drawable-stream pane))))


(format t "~&;*** OMAS v~A - text trace window loaded ***" omas::*omas-version-number*)

;;; :EOF