;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;=================================================================================
;;;24/10/15		
;;;		M A I N - (File main.LiSP)
;;;	
;;;=================================================================================
;;; File that launches OMAS

;;; (c) Jean-Paul Barth�s@HEUDIASYC, CNRS, UTC. See LICENSE file for permissions.

#|
2020
 0205 creation from pieces of the load-omas-moss.lisp file
|#

(in-package :omas)

;;; should be somewhere else (in the asd file)
#+ALLEGRO-v9.0
(require :acache "acache-2.1.21.fasl")

;;;=================================================================================
;;;                               DEBUGGING
;;;=================================================================================

;;; tra�age des dialogues
(dformat-set :dia 0)

;;; d�tails de la trace par MOSS
(setq moss::*verbose* t)

;;;=================================================================================
;;;                               PARAMETERS
;;;=================================================================================
;;; We use global variables since globals has not been loaded yet and we cannot use
;;; the *omas* site structure...

;;; record OMAS load directory

(defParameter *omas-directory* "~/Allegro CL 9.0/quicklisp/local-projects/omas/")

;;; just in case the omas.properties file has been erased we keep some default
;;; parameters (the third value of a triple is the default)
;;; otherwise the omas.properties file provides the defaults to be posted into the
;;; init window or used directly when we bypass the window

;;; the second value of the tuples is used by record-omas-parameters
;;; it is the accessor to the *omas* site structure and also the name of the slot
;;: the last argument if there (:eval) means that a read-from-string must be done
;;; when reading from the file or from the init-window

(defparameter *omas-parameters*
  '(("SITE" omas::site-name "THOR"); reference to the local machine
    ("COTERIE" omas::coterie-name "BOOK") ; APPLI/COTERIE name
    ("BROADCAST" omas::local-broadcast-address "172.8.255.255")
    ("PORT" omas::omas-port "50000" :eval)
    ("AUTOMATIC" omas::omas-automatic "" :eval)
    ("INTERACTION" omas::voice-interaction "NOVOICE")
    ("CONTROL-PANEL" omas::control-panel "T" :eval)
    ("GRAPHICS-WINDOW" omas::graphics-window "T" :eval)
    ))

;;; application parameters are appended to the OMAS parameters in the configuration 
;;; file omas.properties, basically they are any non OMAS parameter

(defparameter *application-parameters* NIL
  "parameters defined at application level")

;;;==============================  service functions =============================

;;;---------------------------------------------------- INITIALIZE-OMAS-PARAMETERS
;;; cannot save them into omas::*omas* because it is not yet created. Thus, uses
;;; *omas-parameters* special variable instead.

(defUn initialize-omas-parameters ()
  "read the omas.property file and add default values if needed"
  (declare (special omas::*omas-parameters*))
  ;; input contains a list of pairs (<parameter> . <file value>)
  (let ((input (read-omas-parameters))
        pair result)
    ;; if file not there use default parameter list
    (unless input (return-from initialize-omas-parameters *omas-parameters*))
    
    ;; otherwise, use the default list for checking parameters
    ;; updating the *omas-parameters* list with values from input
    (dolist (parameter omas::*omas-parameters*)
      (setq pair (assoc (car parameter) input :test #'string-equal))
      (if pair
          (push `(,(car pair) ,(second parameter) ,(cdr pair) 
                    ,@(if (fourth parameter)(list (fourth parameter))))
                result)
        ;; otherwise use default value
        (push parameter result)))
    
    ;; update parameter-list and return list of parameters
    (setq omas::*omas-parameters* (reverse result))))

#|
(initialize-omas-parameters)
(("SITE" OMAS::SITE-NAME "THOR")
 ("COTERIE" OMAS::COTERIE-NAME "NULL")
 ("BROADCAST" OMAS::LOCAL-BROADCAST-ADDRESS "") 
 ("PORT" OMAS::OMAS-PORT "50000" :EVAL)
 ("AUTOMATIC" OMAS::OMAS-AUTOMATIC "T" :EVAL) 
 ("INTERACTION" OMAS::VOICE-INTERACTION "NIL")
 ("CONTROL-PANEL" OMAS::CONTROL-PANEL "T" :EVAL) 
 ("GRAPHICS-WINDOW" OMAS::GRAPHICS-WINDOW "T" :EVAL))
|#
;;;------------------------------------------------------------- PROCESS-PARAMETER

(defUn process-parameter (text-line)
  "processes a single line corresponding to a SOL compiler parameter. Returns ~
   any triple that appears in the file with an = sign in the middle."
  (let ((text (string-trim '(#\space #\linefeed) text-line))
	parm pos data)
    ;; whenever the line is empty, return
    (if (equal text "")(return-from process-parameter nil))
    ;; when the line starts with a semi column return
    (if (equal #\; (char text 0)) (return-from process-parameter nil))
    ;; otherwise get first word
    (setq pos (position '#\space text))
    (unless pos (return-from process-parameter nil))
    ;; otherwise process each parameter
    ;; get equal sign
    (setq pos (position '#\= text)
	  parm (string-trim '(#\space)(subseq text 0 pos))
	  data (string-trim '(#\space)(subseq text (1+ pos)))
          ;var (cadr (assoc parm omas::*omas-parameters* :test #'equal))
          )
    ;(format t "~&=====> Parameter: ~S" text)
    (cons parm data)
    ))

#|
(process-parameter "PORT = 52008")
("PORT" . "52008")
|#
;;;---------------------------------------------------------- READ-OMAS-PARAMETERS

(defUn read-omas-parameters ()
  "reads the omas.properties file transforming it into an a-list"
  (let (text-line result parameter)
    (with-open-file 
      (ss (make-pathname 
           :directory (pathname-directory  omas::*omas-directory*)
           :name "omas"
           :type "properties")
          :direction :input)
      (loop
        (setq text-line (read-line ss nil nil))
        (unless text-line (return))
        (unless (string-equal text-line "") (print text-line))
        (setq parameter (process-parameter text-line))
        (if parameter (push parameter result))))
    (reverse result)))

#|
? (read-omas-parameters)
(("SITE" . "TATIN") ("COTERIE" . "JEAN-PAUL") ("BROADCAST" . "") ("PORT" . "50000")
 ("AUTOMATIC" . "T"))
|#
;;;-------------------------------------------------------- RECORD-OMAS-PROPERTIES

(defUn record-omas-parameters ()
  "rewrites the omas.properties file to record the omas properties, using format ~
   from *omas-parameters* and values from *omas* table."
  (declare (special omas::*omas* omas::*omas-parameters*))
  (with-open-file 
    (ss (make-pathname 
         :directory (pathname-directory  omas::*omas-directory*)
         :name "omas"
         :type "properties")
        :direction :output
        :if-exists :supersede)
    ;(format *debug-io* "~%; record-omas-parameters /*package*: ~S" *package*)
    ;(format *debug-io* "~%; record-omas-parameters /file:~%; ~S" ss)
    ;; print first comment line
    (format ss 
            ";;; Parameters for the OMAS multi-agent platform~%")
    (dolist (parameter omas::*omas-parameters*)
      (format *debug-io* "~%; record-omas-parameters /saving: ~A = ~A"
              (car parameter)(funcall (cadr parameter) omas::*omas*))
      ;; save into file
      (format ss "~%~A = ~A" (car parameter)(funcall (cadr parameter) omas::*omas*))
      )
    ;; end of file
    (format ss "~2%;;; EOF"))
  :done)

; (omas::record-omas-parameters)

;;;---------------------------------------------------------- SAVE-OMAS-PROPERTIES
;;; can be used only after the site structure is created and assigned to *omas*

(defun save-omas-properties-to-globals (omas-list)
  "from the list of properties stores the information into the omas::*omas* ~
   global structure, using the accessors from the list.
Arguments:
   omas-list: a list of properties, each entry a tuple:
              (<property name string> <*omas* accessor> <value> {:eval})
Return:
   t"
  (declare (special omas::*omas*))
  (dolist (parameter omas-list)
    (setf (slot-value omas::*omas* (second parameter)) 
      ;; if :eval tag, read-from-string before saving
      (if (eql :eval (fourth parameter))
          (read-from-string (third parameter) nil nil)
        (third parameter))))
  t)

#|
(omas::save-omas-properties-to-globals 
 '(("GRAPHICS-WINDOW" omas::graphics-window "T" :eval)))
T
(OMAS::GRAPHICS-WINDOW OMAS::*OMAS*)
T
(omas::save-omas-properties-to-globals 
 '(("GRAPHICS-WINDOW" omas::graphics-window "" :eval)))
T
(OMAS::GRAPHICS-WINDOW OMAS::*OMAS*)
NIL
|#
    
;;;===============================================================================
;;;                              LOAD OMAS
;;;===============================================================================

;;; here we launch OMAS
;;; if AUTOMATIC is true and SHIFT key is not pressed, then we load the application
;;; without showing the init window
;;; if AUTOMATIC is false or if the SHIFT key is pressed, then we open the init
;;; window

(eval-when (:load-toplevel :execute)
  ;; get the parameters of the last session from the omas.properties file
  (initialize-omas-parameters)
  ;(print `("main/ *omas-parameters*" ,*omas-parameters*))
  ;; save parameters into the *omas* glabals
  (omas::save-omas-properties-to-globals omas::*omas-parameters*)
  ;; the function is in the load-app file
  (load-application-or-open-init-window)
  )

;;; :EOF
