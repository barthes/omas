﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/04
;;;                O M A S - D I S P L A Y  (file omas-display.lisp)
;;;
;;;===============================================================================
;;; This file contains functions to create displays associated with a specific agent.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
Actions can be the following:
   - :create <name> <window parameters>
   - :close <name>
   - :erase <name>
   - :show <name>
   - :execute <name> <expr>

When a window is created, its name and id are put onto the list attached to the
AGENT/DISPLAYS slot of the agent structure. A special user-close function is 
attached to the instance of window, so that when the window is closed, it removes
the entry in the AGENT/DISPLAYS list.
<name> should preferably be a keyword used to retrieve the id of the window.

2020
 0204 restructuring OMAS as an asdf system
|#

;;;===============================================================================

(in-package :omas)

(defclass omas-window (cg:frame-window)
  ((agent :accessor agent))
  (:documentation "very general window used for creating specific agent windows")
  )

;;;------------------------------------------------- AGENT-PROCESS-DISPLAY-MESSAGE

(defun agent-process-display-message (agent message)
  "dispatches the work according to the various actions
Arguments:
   agent: current agent
   message: the display message
Return:
   :done"
  (case (action message)
    (:create (agent-display-create agent message))
    (:close (agent-display-close agent message))
    (:erase (agent-display-erase agent message))
    (:show (agent-display-show agent message))
    (:execute (agent-display-execute agent message))
    (otherwise
     (error "unknown display action: ~S" (action message))))
  :done)

;;;---------------------------------------------------------- AGENT-DISPLAY-CREATE

(defun agent-display-create (agent message)
  "creates a window to display information attached to the agent, by applying ~
   the function contained in the message arguments to the first two values of ~
   the message argument."
  (let* ((args (args message))
         (name (car args))              ; usually a keyword naming the window
         (window-class (cadr args))     ; a window class
         (create-function (caddr args)) ; the create function
         win)
    (setq win (funcall create-function name window-class))
    ;(setq *win* win) ; debugging handle
    ;; create a special user-close method
    (eval `(defmethod cg:user-close 
                ((win ,window-class))
             (close win)
             ;; remove window reference frome the list
             (setf (displays ,agent)
               (remove-if #'(lambda(xx)(equal win (cadr xx))) 
                          (displays ,agent)))))
    (setf (agent win) agent)
    ;; record  window
    (push (list name win) (displays agent)) 
    win))

;;;----------------------------------------------------------- AGENT-DISPLAY-CLOSE

(defun agent-display-close (agent message)
  (let ((win (get-win-id agent message)))
    (unless win (error "can't retrieve win ID"))
    (user-close win)
  :done))

;;;----------------------------------------------------------- AGENT-DISPLAY-ERASE
;;; we can't do that because the display list is attached to the created window
;;; that we do not know here. Erasing must be done from the application

(defun agent-display-erase (agent message)
  (let ((win (get-win-id agent message)))
    (unless win (error "can't retrieve win ID"))
  nil))

;;;------------------------------------------------------------ AGENT-DISPLAY-SHOW

(defun agent-display-show (agent message)
  "expose the window..."
  (let ((win (get-win-id agent message)))
    (unless win (error "can't retrieve win ID"))
    (cg:select-window win)
  :done))

;;;--------------------------------------------------------- AGENT-DISPLAY-EXECUTE

(defun agent-display-execute (agent message)
  "executes a function presumably to write or to draw onto the window. :action is ~
   :execute, first item of args is window name, the rest is the function to ~
   process and coresponding args.
Arguments:
   agent: owner of the window
   message: internal display message
Return:
   nothing interesting."
  (let ((win (get-win-id agent message)))
    (format t "~&win: ~S" win)
    ;; must get t
    (unless win (error "can't retrieve win ID"))
    ;(print (args message))
    (eval (cdr (args message)))
    :done))

#|
(omas::agent-display-execute 
 contact::SA_contact
 (omas::message-make
  :type :display 
  :to :CONTACT 
  :action :execute
  :args '(:UML let ((stream (omas::get-drawable-stream contact::sa_contact 
                                                       :uml :uml-draw)))
               (cg:with-background-color (stream cg:blue)
                 (cg:draw-box stream (cg:make-box 20 20 50 50))))
  ))
|#
;;;----------------------------------------------------------- GET-DRAWABLE-STREAM

(defun get-drawable-stream (agent window pane)
  "get the pane to draw onto"
  (cg:drawable-stream 
    (cg:find-component pane (cadr (assoc window (omas::displays agent))))))

#|
(get-drawable-stream contact::sa_contact :uml :uml-draw)
#<DRAWABLE-PANE :UML-DRAW in Listener 1 @ #x21d1725a>
|#
;;;-------------------------------------------------------------------- GET-WIN-ID

(defun get-win-id (agent message)
  ;; AGENT/DISPLAYS is a list of opened displays
  (cadr (assoc (car (args message))(displays agent))))

;;;(defun repaint (widget stream)
;;;  "redraws the window"
;;;  (cg:with-foreground-color (stream cg:red)
;;;    (cg:draw-box stream (cg:make-box 20 20 50 50))))

;;;-------------------------------------------------------------------------- GETW

(defun getw (agent name)
  "returns the display window with name name."
  (cadr (assoc name (displays agent))))

;;;===============================================================================
  
(format t "~&;*** OMAS v~A - display loaded ***" *omas-version-number*)

;;; :EOF