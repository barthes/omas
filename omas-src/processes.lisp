﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;          O M A S - P R O C E S S E S  (file processes.lisp)
;;;
;;;===============================================================================
;;; This file contains the functions dealing with processes and timers

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| 
2020
 0202 restructuring OMAS as an asdf system
|# 

(in-package :omas)

;;; ========================= Agent processes ====================================
;;;--- some functions and macros related to processes

;;;------------------------------------------------- AGENT-PROCESS-GET-INDEX-VALUE

(defun agent-process-get-index-value (agent process)
  "uses the active-task-list to obtain the corresponding description of the process.
Arguments:
   agent: agent id
   process: process structure
Return:
   an alternated list like (:type :timeout :task-id T-23 :task-frame <...>)."
  (cdr (assoc process (process-list agent))))

;;;-------------------------------------------------- AGENT-PROCESS-GET-TASK-FRAME

(defun agent-process-get-task-frame (agent process)
  "calls agent-process-get-task-id to obtain a task-id. Then retrieves the ~
      task frame from the active-task-list slot. 
   For a standard process the task-id is that associated with the process, ~
      for a time-limit timer the task-id is the one executing the skill, for a ~
      timeout timer the one that spawned the subtask for which the timer is ~
      active.
Arguments:
   agent: agent id
   process: process structure"
  (let ((task-id (agent-process-get-task-id agent process)))
    (if task-id
      (car (member-if #'(lambda (xx) (eql task-id (id xx)))
                      (active-task-list agent)))
      (error "can't retrieve task-id for process ~S" process))))

;;;----------------------------------------------------- AGENT-PROCESS-GET-TASK-ID

(defun agent-process-get-task-id (agent process)
  "get info from the process index structure (currently an a-list). Returns ~
      two values, the first one is the task-id associated with the process ~
      the second one is the infos structure e.g., (:type :timeout :subtask-id T-34),...
   For a standard process the task-id is that associated with the process, ~
      for a time-limit timer the task-id is the one executing the skill, for a ~
      timeout timer the one that spawned the subtask for which the timer is ~
      active.
Arguments
   agent: agent-id
   process: process structure, e.g. *current-process*
Return:
   task-id of the process executing the skill
   list including type and other parameters."
  (let ((table (process-list agent))
        info)
    (unless table
      (warn "trying to obtain info for process ~S from process list and ~
             corresponding index structure does not exist" process))
    (setq info (cdr (assoc process table)))
    ;; return nultiple values
    (values 
     (cadr (member :task-id info))
     info)))

;;;------------------------------------------------------------ AGENT-PROCESS-KILL

(defun agent-process-kill (agent process)
  "kills an agent process removing its enty from the agent process table.
Arguments:
 agent: agent
 ll: rest of the arguments that must obey the syntax of process-kill for MCL
     or mp:process-kill for AL.
Return
 process-id of killed process when process is not current process
 does not return otherwise"
  (when process
    ;; remove process from process table
    (agent-process-remove agent process)
    ;; kill process
    (acl/mcl-process-kill process)
    ;; if process is not current process return process id
    process))

;;;-------------------------------------------- AGENT-PROCESS-KILL-CURRENT-PROCESS

(defun agent-process-kill-current-process (agent)
  "kills an agent current process removing its enty from the agent process table.
Arguments:
 agent: agent
Return
 does not return "
  ;; remove process
  (agent-process-remove agent *current-process*)
  ;; kill it
  (acl/mcl-process-kill *current-process*))

;;;------------------------------------------------------------ AGENT-PROCESS-NAME

(defun agent-process-name (agent process)
  "get the name of the process given the process-id
Arguments:
   agent: ignored
   process: process-id
Return
   process name (a string)"
  (declare (ignore agent))
  (acl/mcl-process.name process))

;;;---------------------------------------------------------- AGENT-PROCESS-RECORD

(defun agent-process-record (agent process info)
  "record info pertaining to a process to be used later as an index. Info is a list ~
   that includes the type of process and possibly additional info, e.g.:
   (:time-limit T-23), (:timeout ST-8), (:cfb-timeout T-3), (:task T-4).
   Process info is recorded onto an a-list.
Arguments:
   agent: agent-id
   process: process object (e.g. *current-process*)
   info: list including type and parameters."
  (let ((process-list (process-list agent)))
    (setf (process-list agent)
          (push (cons process info) process-list))
    ;; return
    (cons process info)))

#|
(trace (agent-process-record 
        :before #'(lambda (func agent process info)
                    (if (eql (key agent) :rc)
                      (format t "~&Calling ~S~&Process list: ~S "
                              (list func agent process info)
                              (process-list agent))))
        ))
(untrace omas::agent-process-record)
|#
;;;---------------------------------------------------------- AGENT-PROCESS-REMOVE

(defun agent-process-remove (agent process)
  "removes process entry from the process index.
Arguments:
   agent: agent-id
   process: process structure (e.g. *current-process*)
Return:
  T if process was recorded, NIL otherwise"
  (let ((process-list (process-list agent)))
    (setf (process-list agent)
          (remove (assoc process process-list) process-list :test #'equal))
    ))

;;;---------------------------------------------------- AGENT-PROCESS-RUN-FUNCTION

(defun agent-process-run-function (agent name function &rest args)
  "launches (function . args) into a new process
Arguments:
 agent: agent
 name: whostate of the process
 function: function to execute
 args: args to function
Return
 process id."
  (declare (ignore agent))
  `(acl/mcl-process-run-function ,name ,function ,@args))

;;;------------------------------------------------------------ AGENT-PROCESS-WAIT

(defMacro agent-process-wait (agent &rest ll)
  "waits inside a given process
Arguments:
 agent: agent
 ll: list of arguments must obey the syntax of process-wait for MCL
     or mp:process-wait for ACL.
Return
 whatever process-wait returns."
  (declare (ignore agent))
  `(acl/mcl-process-wait ,@ll))

;;;----------------------------------------------- AGENT-PROCESS-WAIT-WITH-TIMEOUT

(defMacro agent-process-wait-with-timeout (agent &rest ll)
  "waits inside a given process
Arguments:
 agent: agent
 ll: list of arguments must obey the syntax of process-wait-with-timeout for MCL
     or mp:process-wait-with-timeout for ACL.
Return
 whatever process-wait returns."
  (declare (ignore agent))
  `(acl/mcl-process-wait-with-timeout ,@ll))

;;;--- end of functions and macros related to agent processes

;;; ============================== Timers ========================================
#|
Timers are set up to implement timeouts or time-limits that have the following 
semantics:

Timeout
-------
A timeout is set when an agent sends a subtask to be executed by another agent and
waits for an answer. 
   - If the answer comes back before the timeout timer fires, then
     the timer is killed. 
   - If the timer fires before the answer comes back, then either the task is aborted
     through a broadcast message, or the message is repeated until a repeat count
     is reached and the sender gives up.
A timeout timer can only be set while executing a task. I.e., when the user sends
a message to execute a task, there is no way to set up a timer.
When a goal fires (internal message), then a main task should be created. Indeed,
the corresponding skill can spawn subtasks with timeouts.

Time-limit
----------
A time-limit timer is set up when receiving a message and is associated to a particular
task to do. Thus, a task-frame necessarily exists, and we can pass the task-frame to
the setting routine.

|#

;;;-------------------------------------------------------------------- OMAS-NOOP

(defun omas-noop () nil)

;;;------------------------------------------------- AGENT-PROCESS-TASK-TIME-LIMIT

(defun agent-process-task-time-limit (agent task-id retry)
  "see whether a time-limit function is provided with the skills. If not, does ~
      nothing, if the retry count is 0 kills the timer. When the task being watched is ~
      no longer in the active-task-list, then we assume that it has completed and kill ~
      the timer. 
   This function is called in the timer process.
Arguments:
   agent: agent
   task-id: id of task to watch (might have been killed)
   retry: number of retries left."
  (let* ((active-task-list (active-task-list agent))
        (task-frame (car (member-if #'(lambda(xx)(eql task-id (id xx)))
                                    active-task-list)))
        fn)
    ;; if we cannot find the task in the list of active task, then it has finished
    ;; or it has already be aborted. We cancel the timer process (current process)
    (unless task-frame
      ;; send a warning
      (agent-trace agent 
                   "~&task ~S no longer exists when time-limit fires" task-id)
      ;; first draw state
      (state-draw)
      (agent-process-kill-current-process agent))
    ;; get user specified function if any
    (setq fn (agent-get-skill-time-limit agent (action (message task-frame))))
    ;(print (list "======== user-time-limit function:" fn))
    ;; apply function if any
    (if fn
      ;; jpb0407 adding (message task-frame) in the args
      (apply fn agent (environment task-frame) (message task-frame) nil)
      ;; if no function was provided, we do nothing until we hit the hard
      ;; timeout (retry = 0)
      (if (> retry 0)
        (warn "~S encountered a time-limit for task ~S. No function was provided ~
               thus we do nothing." (name agent) task-id)))
    (when (<= retry 0) 
      ;; draw state before killing process, otherwise busy state won't show
      (state-draw)
      ;; give time to draw timers
      (sleep 0.1)
      ;; the abort function tests if the task is still active
      (abort-task agent (id task-frame) (from! task-frame))
      ;; we killed task, now we must kill the time-limit timer, i.e. commit suicide
      (agent-process-kill-current-process agent))
    :done))

;;;--------------------------------------------------------- AGENT-PROCESS-TIMEOUT
;;; the function is called by the timeout timer that executes in a thread different
;;; from the one executing the skill. Thus, the package may be different from that
;;; of the skill functions when calling the user-defined timeout handler. In the 
;;; same fashion the timeout handler may not do an env-get since env-get uses the
;;; process in which it executes to retrieve the task-frame structure containing
;;; the data. However it can do a recall (shared by all threads).

;;; At v14, we discard the possibility of repeating the message on a timeout

(defun agent-process-timeout (agent subtask-frame)
  "process a timeout from the timeout process.
   For point-to-point mode, kills the task, canceling all subtasks and sending 
   an error message to the emitter.
   Called by the timeout process: any timeout handler executes in the timer 
   process thread.
Arguments:
   agent: agent
   subtask-frame: frame of the subtask to be canceled."
  ;; a timeout process is associated with a subtask and as such is recorded in
  ;; a subtask-frame 
  (let (current-task skill fn message subtask-id info)
    ;; get info associated with process in index table
    (setq info (agent-process-get-index-value agent *current-process*))
    
    (print `("agent-process-timeout/ info: ~S" ,info))
    
    ;; if not available, then timeout process was canceled, we abort process
    (unless info (agent-process-kill-current-process agent))
    
    ;; here we want to kill the subtask
    ;; check first that subtask-frame is still in the list of tasks of the agent
    (setq current-task (parent-task subtask-frame))
    ;; if the parent task is not there, either it is finished or has been canceled
    (unless (and current-task
                 (member subtask-frame (subtask-list current-task) :test #'equal))
      (agent-trace agent
                   "killing timeout timer for subtask ~S that no longer exists"
                   subtask-id)
      ;(print (active-task-list agent))
      ;; commit suicide
      (agent-process-kill-current-process agent))
    
    ;; indicate that a timeout occurred by drawing an orange notch on the agent lifeline
    (agent-display-timeout agent)
    ;; now we are sure that current-task exists, get some data
    ;; the timeout handler should not be attached to the skill since it is then
    ;; common to all subtasks that were sent by a given task using this skill.
    ;; it should be defined separately and attached to the subtask when the
    ;; subtask is created
    (setq skill (action (message current-task))
        fn (agent-get-skill-timeout agent skill)
        ;; get message that spawned the subtask
        message (message subtask-frame))
    
    (agent-trace agent "processing timeout condition for ~S, fn: ~S" 
                 subtask-id fn)
    
    ;; check if the user wants to process timeouts by himself by looking if a
    ;; timeout handler (fn) has been defined for the current skill
    ;; when the function exists apply it
    (when fn (funcall fn agent (message subtask-frame)))
    
    ;; since we are timing out a subtask, we must remove it from the list of
    ;; subtasks of the parent (current) task
    (setf (subtask-list current-task)
      (remove subtask-frame (subtask-list current-task)))
    
    ;; === we then abort the corresponding task, sending an error message to the 
    ;; requester, and killing the timeout process
    
;;; we do not want to do that. This should be done by the requester that receives
;;; the error message
;;;    ;; if the agent is a PA and the message had no answer, we must post a failure
;;;    ;; message, which will restart the conversation JPB1104
;;;    (when (typep agent 'assistant)  ; JPB1201
;;;      ;; old conversation format
;;;      #-:converse-2
;;;      (progn
;;;        (setf (answer agent) :failure)
;;;        (setf (error-contents agent) "No answer.")
;;;        (setf (to-do agent) :answer-there))
;;;      #+:converse-2
;;;      ;; this will wake up the converse threads failing all conversations!!
;;;      (setf (to-do agent) `(:to ,(keyword agent) :failure t :reason "No answer"))
;;;      )
       
    ;; abort cancels all subtasks, sets up error message, and kill task process
    (abort-task agent (id current-task) (from! current-task))
    ;; kill timer process (this process!)
    (agent-process-kill-current-process agent)
    
    :end))

;(trace agent-process-timeout-message)
;(untrace agent-process-timeout-message)

;;;----------------------------------------------- AGENT-PROCESS-BROADCAST-TIMEOUT
;;; the function executes in a timer thread

;;; when a broadcast timeout happens with a PA, it coud be that
;;;  - the PA was doing some research by itself
;;;  - the PA was involved in a conversation and trying to get some informationto
;;;    return, but nobody answered the broadcast. How can we return the failure
;;;    to the right conversation?

;;; during a conversation, research for information is done by the :send-message
;;; option of the defstate macro. This puts up an internal message into the
;;; agent agenda, which creates a new thread to serve the message. When the
;;; send-message thread broadcasts some request and does receive any answer, then
;;; the timeout should be captured by a send-request timeout handler that will
;;; eventually wake up the conversation

(defun agent-process-broadcast-timeout (agent subtask-frame delay &optional retry)
  "function that processes a broadcast timeout. Its action depends on the type of ~
   broadcast strategy that we are executing:
   - :take-first-answer do as in the standard timeout case
   - :collect-answers results in processing the collected answers. If none, too ~
     bad we don't wait any longer.
   - (:collect-first-n-answers nn) results in processing the collected answers ~
     like in the :collect-answers case
Arguments:
   agent: agent
   subtask-id: id of task to be watched
   delay: time to wait (in seconds)
   retry: number of retries to do (default none)."
  (declare (ignore retry))
  (let ((subtask-id (id subtask-frame)) strategy)
    
    ;;=== WAIT
    (sleep delay)
    (agent-trace agent "... broadcast timeout on subtask ~S" subtask-id)
    
    ;; we must recover strategy from the message that triggered the task
    ;(format t "~&;~S;agent-process-broadcast-timeout; strategy: ~S"
    ;        (name agent) (strategy (message subtask-frame)))
    (setq strategy (strategy (message subtask-frame)))
    ;; stategy can be :take-first-answer or :collect-answers or 
    ;; (:collect-first-n-answers nn)
    (if (listp strategy) (setq strategy (car strategy)))
    (case strategy
      (:take-first-answer
       ;; nothing to do, got no answer, do as in the standard case
       (agent-process-timeout agent subtask-frame))
      
      ((:collect-answers :collect-first-n-answers)
       ;; indicate the timeout interrupt by displaying a green square
       (agent-display-timeout agent (%color-from-key  :green))
       ;; go process answers; if there are answers, this function does not
       ;; return, when no answer, it returns nil
       (agent-process-saved-answers agent subtask-frame)
       
       ;; if no answer, do as in the standard case after the broadcast timeout
       (agent-process-timeout agent subtask-frame))
      
;;;       ;; on the conservative side, we cancel current task and subtasks
;;;       ;;***** this is not advisable when we are in the middle of a conversation
;;;       ;; the conversation will wait for the answer indefinitely
;;;       (abort-task agent (id (parent-task subtask-frame))
;;;                   (from! (parent-task subtask-frame)))
;;;       ;; and kill timer process already done, but just in case...
;;;       (agent-process-kill-current-process agent))
      
      (otherwise
       (error "broadcast timer firing for agent ~S, unknown broadcast mode ~S"
         (name agent) (strategy (message subtask-frame)))
       (agent-process-kill-current-process agent)
       ))))

;;;------------------------------------------- AGENT-PROCESS-CALL-FOR-BIDS-TIMEOUT

(defun agent-process-call-for-bids-timeout (agent task-frame subtask-frame delay 
                                                        &optional (retry 0))
  "function that processes a call-for-bids timeout. The idea is to wait some time ~
      to allow bids to come in. 
   - If we have some bids, then we process them to find the best one. If we have ~
      a satisfactory answer we send a cancel-grant message.
   - If none is suitable, then we try to wait some more time (until retry gets to 0).
   - If no bid is suitable and retry is 0, we have a hard timeout, and we must cancel ~
      the task and subtasks, and send back an error message to the caller.
   Should be called by a task process (when sending the CFB message)
Arguments:
   agent: agent
   task-frame: frame of the task that started CN
   subtask-frame: frame of the subtask to be watched
   delay: time to wait (in seconds)
   retry: number of retries to do (default none)."
  (let ((subtask-id (id subtask-frame))
        current-task)
    (loop
      (agent-process-wait-with-timeout
       agent
       (concatenate 'string (symbol-name (name agent)) "/"
                    (format nil "~s" subtask-id) "-cfb-timeout")
       delay
       #'omas-noop)
      (agent-trace agent "... cfb timeout on subtask ~S retry: ~S" subtask-id retry)
      ;; indicate the timeout interrupt by displaying a light-blue square
      (agent-display-timeout agent (%color-from-key :light-blue))
      
      ;(format t "~%; agent-process-call-for-bids-timeout /task-frame: ~S~
      ;           ~%; active-task-list: ~%;  ~S" task-frame (active-task-list agent))
      ;; check that task and subtask still exist. They could have disappeared
      ;; if canceled or aborted at the same time as the timer fires before being
      ;; canceled
      (unless (member task-frame (active-task-list agent))
        (warn "task associated with cfb-timer ~S can't be found" 
              *current-process*)
        ;; kill current timer
        (agent-process-kill-current-process agent))
      (unless (member subtask-frame (subtask-list task-frame))
        (warn "subtask associated with cfb-timer ~S can't be found" 
              *current-process*)
        ;; kill current timer
        (agent-process-kill-current-process agent))
      
      ;; if we found a good bid then we process it
      (when (agent-process-bids-on-timeout agent subtask-frame)
        ;; ... and kill timer process (in fact already done by agent-process-bids)
        ;; so we should never get here
        (agent-process-kill-current-process agent))
      
      ;; otherwise no bids or insufficient bids
      ;; check if we have exhausted retry possibilities
      (when (<= retry 0)
        ;; Yes, we kill everything. get task-frame associated with timer
        (setq current-task (parent-task subtask-frame))
        (unless (member current-task (active-task-list agent))
          ;; if it can't be found, then probably task was cancelled in the mean time
          ;; e.g. by the emitter
          (warn "task associated with cfb-timer ~S can't be found" 
                *current-process*))
        ;; cancel current task and subtasks (saved-bids are part of the task
        ;; frame, thus will be cleaned automatically)
        (abort-task agent (id current-task) (from! current-task))
        ;; and kill timer process
        (agent-process-kill-current-process agent))
      
      ;; otherwise wait some more, repeating the call-for-bids message
      (excl:without-interrupts ; protect against racing conditions       
       ;; Send message again, increasing repeat-count
       ;; update message in subtask-frame
       (incf (repeat-count (message subtask-frame)))  
       ;; turn it into a call for bids
       (setf (type! (message subtask-frame)) :call-for-bids)
       ;; as well as duplicated repeat-count within subtask-frame
       (incf (repeat-count subtask-frame))
       ;; maybe we should clone the message before sending it?
       (send-message (message subtask-frame)))
      
      (agent-trace agent "increasing repeat count on subtask ~S in process ~S" 
                   subtask-id (acl/mcl-process.name *current-process*))
      (decf retry))))

;;;===============================================================================
;;;                         Functions setting timers
;;;===============================================================================

;;;--------------------------------------------------------- SET-BROADCAST-TIMEOUT
;;; we set a broadcast normally from a send-subtask, meaning that the task exists
;;; and is running.

(defun set-broadcast-timeout (agent subtask-frame delay &optional (retry 0))
  "set up a timer to process collected answers.
Arguments:
   agent: agent
   subtask-id: id of subtask to be watched
   delay: time to wait (in seconds)
   retry: number of retries to do (default none)."
  (let* ((subtask-id (id subtask-frame))
         (timer-name (string+ (key agent) "/" subtask-id "-broadcast-timeout"))
         process task-frame)
    (agent-trace agent "setting up a broadcast-timeout ~S on subtask ~S of ~,2Fs" 
                 timer-name subtask-id delay)
    
    (setq task-frame (agent-get-task-frame-from-subtask-id agent subtask-id))
    ;; if task not in active task-list, then somethig wrong
    (unless task-frame
      (error "no task corresponding to subtask ~S not in the active task list of ~
              agent ~S while setting a broadcast timeout." 
        subtask-id agent))
    
    ;; start the timer process. The task info is used to recover task parameters
    ;; when calling a dynamic-exit from the timeout process
    (setq process
          (agent-process-create
           agent
           (list :subtask-id subtask-id :type :broadcast-timer 
                 :task-id (id task-frame) :task-frame task-frame)
           timer-name 
           #'agent-process-broadcast-timeout
           agent subtask-frame delay retry))
    ;; quit
    process))

;;;----------------------------------------------------- SET-CALL-FOR-BIDS-TIMEOUT
;;; adding task-frame to args JPB1011

(defun set-call-for-bids-timeout (agent task-frame subtask-frame delay 
                                        &optional (retry 0))
  "set up a timer to process returned bids.
Arguments:
   agent: agent
   subtask-id: id of subtask to be watched
   delay: time to wait in seconds
   retry: number of retries to do (default none)."
  (let* ((subtask-id (id subtask-frame))
         (timer-name (concatenate 'string (symbol-name (key agent)) "/"
                                  (format nil "~S" subtask-id) "-cfb-timeout"))
         process)
    (agent-trace agent "setting up a call-for-bids-timeout ~S on subtask ~S of ~,2Fs" 
                 timer-name subtask-id delay)
    (setq process
          (agent-process-create
           agent
           (list :subtask-id subtask-id :type :call-for-bids-timer)
           timer-name 
           ;; adding task-frame to args JPB1011
           #'agent-process-call-for-bids-timeout agent task-frame subtask-frame 
           delay
           retry))
    ;; quit
    process))

;;;---------------------------------------------------------- SET-TIME-LIMIT-TIMER

(defun set-time-limit-timer (agent task-frame delay &optional (retry 0))
  "set up a timer for imposing a time-limit on task.
   Called by the main process while setting up the task?
Arguments:
   agent: agent
   task-frame: frame of task to time out
   delay: delay (in seconds)
   retry: (opt) number of retries (default is 0)."
  (let* ((task-id (id task-frame))
         (timer-name (concatenate 'string (symbol-name (key agent)) "/" 
                                  (format nil "~S" task-id) "-time-limit"))
         process)
    (agent-trace agent "setting up a time-limit timer on task ~S, ~S, of ~,2Fs" 
                 task-id timer-name delay)
    (vformat "~&...~A: setting a time-limit timer, process: ~A, delay: ~S" 
             (name agent) timer-name delay)
    ;; set up timer
    (setq process
          (agent-process-create
           agent
           (list :task-id task-id :type :time-limit)
           timer-name 
           #'time-limit-timer agent task-frame 
           delay
           retry))
    (setf (timer-process task-frame) process)
    ;; return process
    process))

;(trace set-time-limit-timer)
;(untrace set-time-limit-timer)

;;;------------------------------------------------------------- SET-TIMEOUT-TIMER

(defun set-timeout-timer (agent subtask-frame delay)
  "set up a timer to process a subtask timeout.
Arguments:
   agent: agent
   subtask-id: id of task to be watched
   delay: time to wait (in  seconds)
Returned value:
   process object."
  (let* ((subtask-id (id subtask-frame))
         (timer-name (concatenate 'string (symbol-name (key agent)) "/"
                                  (format nil "~S" subtask-id) "-timeout"))
         task-frame)
    (agent-trace agent "setting up a timeout on subtask ~S, ~S, of ~,2Fs" 
                 subtask-id timer-name delay)
    (setq task-frame (agent-get-task-frame-from-subtask-id agent subtask-id))
    ;; if task not in active task-list, then somethig wrong
    (unless task-frame
      (error "no task corresponding to subtask ~S not in the active task list of ~
              agent ~S" 
             subtask-id agent))
    ;; create and return process value
    (agent-process-create
     agent
     (list :subtask-id subtask-id :type :timeout :task-id (id task-frame)
           :task-frame task-frame)
     timer-name #'timeout-timer agent subtask-frame 
     delay
     )))

#|
(defun set-timer (agent delay &optional (retry 0))
  (process-run-function (symbol-name (gentemp "timer-")) #'timer agent delay retry))
|#

;;;-------------------------------------------------------------- TIME-LIMIT-TIMER

(defun time-limit-timer (agent task-frame delay &optional (retry 0))
  "waiting function used by the time-limit process.
Arguments:
   agent: agent
   task-id: id of task to be watched
   delay: time to wait (in  seconds)
   retry: number of retries to do (default none)."
  (loop
    (agent-process-wait-with-timeout 
     agent
     (concatenate 'string (symbol-name (key agent)) "/"
                  (format nil "~S" (id task-frame) "-time-limit"))
     delay
     #'omas-noop)
    (agent-trace agent "~&timeout retry: ~S" retry)
    ;; check for associated task, abort in case retry <= 1
    (agent-process-task-time-limit agent task-frame retry)
    ;; otherwise wait some more
    (decf retry)))

;;;----------------------------------------------------------------- TIMEOUT-TIMER

(defun timeout-timer (agent subtask-frame delay)
  "set up a timer to process subtask timeout. Runs in a separate thread. ~
      Calls agent-process-timeout-message.
Arguments:
   agent: agent
   task-id: id of task to be watched
   delay: time to wait (in seconds)."
  (loop
    (agent-process-wait-with-timeout  
     agent
     (concatenate 'string (symbol-name (key agent)) "/"
                  (format nil "~S" (id subtask-frame)) "-timeout")
     delay 
     #'omas-noop)
    (agent-process-timeout agent subtask-frame)
    ))

(format t "~&;*** OMAS v~A - processes loaded ***" *omas-version-number*)

;;; :EOF