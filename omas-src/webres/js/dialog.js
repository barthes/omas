/**
 * Set of functions for dialog management
 * Author: Marcio Fuckner / Gregory Wanderley
 *
 * Revision history:
 * 02/11/2013 - MFK - Interface Redesign
 * 25/06/2014 - MFK - removal of ! shortcut for auto-submission
 * 01/02/2017 - GW - Added Text-to-speech and Speech-to-text
 * 19/05/2017 - GW - Added an Avatar (BotLibre) synchronized with the 
 *                   TTS (made natively by the avatar, in order to allow face movements). 
 *                   The Speech-to-text is made by Google API.
 * 19/10/2017 - GW - The communication with the server is made now by Websockets
 */

// ======== global variables ========

// == Server Parameters ====

// Put the server IP:
var server_IP = "localhost";

// Put the server port:
var server_port = "12345";

// ==================================


var count = 0; // to count the divs ("dialog panels")

var synth = window.speechSynthesis; // handler for text-to-speech

var utterThis = new SpeechSynthesisUtterance(); // variable for the utterances when using the text-to-speech 

var recognition =  new webkitSpeechRecognition(); // variable for speech-to-text

var recognizing = false; // flag for the speech recognition

var speaking = false; // flag for the speech synthesis

// Avatar variables
SDK.applicationId = "554699923894232259";

var sdk = new SDKConnection();
var web = new WebAvatar(); // "system voice" (for some technical speech) in English

var sdk1 = new SDKConnection();
var web1 = new WebAvatar(); // French male voice - Pierre

// WebSockets
var ws = new ws({ // used to handle the WebSocket
    server: httpsServer
});


/**
* Initialize the avatar(s).
*/
function avatarInitialize() {

// For the system voice
web.connection = sdk;
web.avatar = "12717875";
web.voice = "cmu-slt";
web.voiceMod = "default";

// For the French male voice
web1.connection = sdk1;
web1.avatar = "12717875";
//web1.voice = "pierre-voice-hsmm";
web1.voice = "enst-dennys-hsmm";
web1.voiceMod = "default";


/**Avatar voices
Test:(http://fr.botlibre.com/forum-post?id=566608)

(http://fr.botlibre.com/forum-post?id=566608)

*cmu-slt - English : US : Female
*cmu-slt-hsmm - English : US : Female
*cmu-bdl-hsmm - English : US : Male
*cmu-rms-hsmm - English : US : Male
*dfki-prudence-hsmm - English : GB : Female
*dfki-spike-hsmm - English : GB : Male
*dfki-obadiah-hsmm - English : GB : Male
*dfki-poppy-hsmm - English : GB : Female
*bits1-hsmm - German : DE : Female
*bits3-hsmm - German : DE : Male
*dfki-pavoque-neutral-hsmm - German : DE : Male
*camille - French : FR : Female
*camille-hsmm-hsmm - French : FR : Female
*jessica_voice-hsmm - French : FR : Female
*pierre-voice-hsmm - French : FR : Male
*enst-dennys-hsmm - French : FR : Male
*istc-lucia-hsmm - Italian : IT : Male
*voxforge-ru-nsh - Russian : RU : Male
*dfki-ot-hsmm - Turkish : TR : Male
*cmu-nk-hsmm - Telugu : TE : Female

=> SDK.tts(text, voice)
*/
	
}


/**
* Start the websocket connection.
*/
function websocketsConnection(){
	
//alert("ws://" + server_IP + ":" + server_port + "/manager");
var urlws = "ws://" + server_IP + ":" + server_port + "/manager";
var userLogin = "VocalBrowser"; // This is the "user login" that will appears in the server when connected through the WebSockets

// Connect!
ws = new WebSocket(urlws, userLogin);

// Called when the connection to the server is opened.
ws.onopen = function () {

};
 
// Called when the connection to the server is closed.
ws.onclose = function (e) {
    alert("Connection with server closed; Maybe the server wasn't found, it shut down or you're behind a firewall/proxy.");
};


/*on récupère les messages provenant du serveur websocket */
ws.onmessage = function(e){
	
	// receive a JSON with the requested query as one of its parameters
	
	// Format of the message coming from OMAS:
	// "{\"label\":\"Amandine\", \"message\":\"Aujourd'hui il fait beau\", \"voice\":\"French female\"}"
	
	var obj = JSON.parse(e.data);
	
	var label = obj.label; // take the label
	var message = obj.message; // take the message
	var voice = obj.voice; // take the voice
		
    processResponse(message, label, voice);	
}


ws.onerror = function(e){} /*on traite les cas d'erreur*/	

window.addEventListener("beforeunload", function (e) {
  ws.close();
});

}

/**
*Controls the speech recognition in the web page
*/ 
function speechRecognition() {
	
  recognition.lang = 'fr-FR';
  recognition.continuous = false;
  recognition.interimResults = true;
  recognition.maxAlternatives = 5;	
  recognizing = false;

  var final_transcript = '';

   recognition.onresult = function(event) {
    var interim_transcript = '';
    for (var i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {
        final_transcript += event.results[i][0].transcript;
      } else {
        interim_transcript += event.results[i][0].transcript;
      }
    }
	
	speaking = true;
	
	if(final_transcript.trim() != ""){
	 document.getElementById("user-input").value = final_transcript;
     document.getElementById("btn").click();
	}
	
	final_transcript = '';
    
  };

   // Here we protect the recognition phase, avoiding that it recognizes the PA's own voice (when synthesing)
   recognition.onend = function() {
	
	}	  		
}


function recognitionStart() {
 recognition.start();
 recognizing = true;
}

function recognitionStop() {
 recognition.stop();
 recognizing = false;
}


/**
*Initialize the text to speech - WebAPI (not from avatar)
*/   
function texttoSpeech() {
//speakPA.lang = "en-US";
utterThis.lang = "fr-FR";
//utterThis.text = "Aujourd'hui il fait beau!";
//synth.speak(utterThis);

// enforce the voice speaks in French
utterThis.voice = speechSynthesis.getVoices().filter(function(voice) {
    return voice.name == "Google français"

  })[0];

/** 
* The volume property allows you to adjust the volume of the speech.
* A float value between 0 and 1 should be specified here. The default is 1.
*/
//utterance.volume = 1;

/**
* The rate attribute defines the speed at which the text should be spoken. 
* This should be a float value between 0 and 10, the default being 1.
*/
//utterance.rate = 1;

/**
* The pitch attribute controls how high or low the text is spoken. 
* This should be a float value between 0 and 2, with a value of 1 being the default.
*/
// utterance.pitch = 1;

}


/**
 * Checks if the response is in JSON format
 * Parameters:
 *   response: the string containing the response
 * Returns:
 *   a boolean value.
 */
function isJSON(response) {
   return response.trim().substring(0,1) == '[' || response.trim().substring(0,1) == '{';
}

/**
 * Adds a new empty div for the history composition
 * Returns:
 *   the new div
 */
function addHistoryDiv() {
  var historyDiv = document.getElementById('panel-history');
  var innerDiv = document.createElement('div');
  innerDiv.id = "message" + (++ count); 
  innerDiv.className = "message";
  historyDiv.appendChild(innerDiv);
  return innerDiv;
}

/**
 * Return the current time
 * Returns:
 *   a string containing the current time in the format hh:mm
 */
function currentTime() {
  var date = new Date(); 
  return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
}

/**
 * Creates a div with the user message
 * Parameters:
 *   query: the string containing the user message
 * Returns:
 *   none
 */
function addUserMessage(query) {
   var div = addHistoryDiv();
   div.innerHTML = processText(query,"Speaker");
}

/**
 * Process the PA response
 * Parameters:
 *   response: The string containing the response
 *   query: The string containing the user utterance
 * Returns:
 *   none
 */
function processResponse(response, response_label, voice_type) {

   // JSON coming from the agent server: "{\"label\":\"Amandine\", \"message\":\"Aujourd'hui il fait beau\", \"voice\":\"French female\"}"
     var div = addHistoryDiv();
     div.innerHTML = processText(response, response_label); 

	 if(response.trim().substring(0,5) == '<span'){
		 
         speaking = true;
		 recognition.stop();

		 web.voice = "cmu-slt";
		 web.addMessage("Vocal Natural language interface. Version 1.0, developed by Gregory Moro Puppi Wanderley, a P.h.D. student at U.T.C., under the supervision of Prof. Jean-Paul Barthès.", "", "", "");
		 web.processMessages();
		
	 }else{
		speaking = true;
		recognition.stop();

      if(voice_type != null){
		if(voice_type.toLowerCase() == "french male"){ // speak with the male voice
		  web1.addMessage(response, "", "", "");
	      web1.processMessages();
		}else{
          if(voice_type.toLowerCase() == "french female"){	// speak with the female voice	 
		    utterThis.text = response;
			synth.speak(utterThis);
		  }
		}
     }
  } 
 
  document.getElementById("user-input").value = "";
  document.getElementById("user-input").focus();
  div.scrollIntoView(false);
  document.body.style.cursor = "default";
}

function processText(msg,participant) {
  var img;
  
  if(participant == "Speaker")
	   msg = msg + "&nbsp;<a href='javascript:repeatSentence(\"" + msg + "\")'><span class='glyphicon glyphicon-repeat'></span></a>";
   

  var content = "<li class='left clearfix'>"; 
                
  content += "<div class='chat-body clearfix'>"+
             "<div class='header'>" +
             "<strong class='primary-font'>" + participant + "</strong><small class='pull-right text-muted'>" +
             "<span class='glyphicon glyphicon-time'></span>" + currentTime() + "&nbsp;</small>" + 
             "</div>" +
             "<p>" + msg + "</p></div>";
  content += "</li>"
  
  return content;
}

/**
 * Process the user request
 * Parameters:
 *   speaker_message: The user utterance
 * Returns:
 *   none
 */
function processRequest(speaker_message) {
  if(speaker_message.trim() == "" || speaker_message == "[#")
    return;

    var myJSON;
    processResponse(speaker_message, "Speaker", null);
	
	// sends the speaker message to the server
    // first creates a json
	myJSON = { "browser-message": speaker_message }

    var myString = JSON.stringify(myJSON);
   
	ws.send(myString);
}

function repeatSentence(sentence) {
  document.getElementById("user-input").value = sentence;
  document.getElementById("btn").click();
}

function startProcess(processName) {
  processRequest(processName,true)
}


/**
 * Checks if the phrase could be submitted
 * Parameters:
 *   the onKeyPress event
 * returns:
 *   none
 */
function checkKey(e) {
  var character = String.fromCharCode(e.keyCode);
  if(e.keyCode == 13 || character == '.' || character == '?') {
    document.getElementById("btn").click();
  }
}

/* EOF */