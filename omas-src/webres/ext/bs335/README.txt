﻿Bootstrap is a free and open-source front-end framework for developing websites and web applications. It contains HTML- and CSS-based design templates for typography, forms, buttons, navigation and other interface components, as well as optional JavaScript extensions. Unlike many earlier web frameworks, it concerns itself with front-end development only.

All information is available at
   http://getbootstrap.com
   
How does it work
----------------
When the dialog.html page is created, then
  - in the meta part of the page a number of library are loaded:
    - Bootstrap CSS : library
    <link href="ext/bs335/css/bootstrap.min.css" rel="stylesheet">
	
	- Bootstrap-toogle CSS : library
	<link href="ext/bootstrap-toggle-master/css/bootstrap-toggle.min.css" rel="stylesheet">
	
    - dialog stylesheet
    <link rel="stylesheet" href="css/dialog.css">
    
    - dialog functions
    <script language="javascript" src="js/dialog.js" type="text/javascript"></script>    

    - jquery API : library
    <script src="ext/jquery/jquery-1.11.3.js"></script>
	
    - Bootstrap API : library
    <script src="ext/bs335/js/bootstrap.min.js"></script>
	
	- Botlibre! API : library
	<script type='text/javascript' src="http://www.botlibre.com/scripts/sdk.js"></script>
	
	- Bootstrap-toogle API :library
    <script src="ext/bootstrap-toggle-master/js/bootstrap-toggle.min.js"></script>
