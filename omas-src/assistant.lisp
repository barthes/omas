;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;24/10/18
;;;               A S S I S T A N T (file assistant.lisp)
;;;
;;;===============================================================================
;;; (c) Jean-Paul Barthès@HEUDIASYC, CNRS, UTC. See LICENSE file for permissions.

#| 
 This file contains all functions related to the functioning of an agent as an
 assistant

 (start-converse-process albert::albert)

History
-------
2024
 1018 adapting to Windows 10
2020
 0202 restructuring OMAS as an asdf system
|#

(in-package :omas)

;;;================================== Globals ====================================
; done in globals
;(defParameter *initial-conversation-entry* () "initialized by dialog engine")

;(push :converse-v2 *features*) ; done in load-omas

;;;============================= macros ==========================================
;;; macro for defining set methods

(defMacro def-pa-set (prop sub-object-name)
  `(defMethod (setf ,prop) (val (self assistant) )
     (let ((item (,sub-object-name self)))
       (if item
           (setf (,prop item) val)
         (error ,(concatenate 'string "non existing "
                   (symbol-name sub-object-name)
                   " part in assistant ~S") (name self)))
       val)))

;;; same with no check

(defMacro def-pa-set-no-check (prop sub-object-name)
  `(defMethod (setf ,prop) (val (self assistant) )
     (setf (,prop (,sub-object-name self)) val)
     ))

#|
;;; Example of use: (def-pa-set input-messages comm)
;;; should produce

(defmethod (setf output-messages) (val (self assistant) )
  (let ((item (comm self)))
    (if item
        (setf (output-messages item) val)
      (error "non existing COMM part in assistant ~S" (name self))))
  val)
|#

;;; macro to extend the method applying to a subpart of an agent, so that it can
;;; be applied to the agent as a whole. Includes the definition for defsetf

(defMacro extend-method-for-assistant (method subobject)
  `(progn
     (defMethod ,method ((self assistant))
       (,method (,subobject self)))
     (def-pa-set-no-check ,method ,subobject)))

;;;=================================== Class =====================================

(defClass assistant (agent)
  ((assistant-process :accessor assistant-process :initarg :assistant-process 
                      :initform nil)
   (master-bins :accessor master-bins :initarg :master-bins :initform nil)
   (master-window :accessor master-window :initform nil)
   )
  (:documentation "personal assistant agent"))

(defClass master-bins ()
  (;; answer
   (answer :accessor answer :initform nil)
   ;; slot containing an answer to return to sender. Also used to synchronize
   ;; a skill processing the input message
   (answer-to-return :accessor answer-to-return :initform nil)
   ;; slot containing an a-list on which the tasks running an :ask skill will wait
   (answer-to-task-to-do :accessor answer-to-task-to-do :initform nil)
   ;; timeout for letting the master answer a question from assistant
   (answer-timeout :accessor answer-timeout :initform most-positive-fixnum)
   ;; a stack containing the keys of the senders of a request (set by :ASK skill) 
   (ask-sender :accessor ask-sender :initform nil)
   ;; slot containing details to pass to the CONVERSE process so that it can return
   ;; the result of the dialog to the proper process
   ;; list of sublists e.g. ((:ML moss::$CVSE.1)(:NURSE-1 moss::$CVSE.2)...)
   (conversation :accessor conversation :initform nil)
   (conversation-state :accessor conversation-state :initform nil)
   (conversation-context :accessor conversation-context :initform nil)
   (conversation-entry :accessor conversation-entry :initform nil)
   (details :accessor details :initform nil)
   ;; file to be loaded as a dialog file
   (dialog-file :accessor dialog-file :initform nil)
   ;; entry point of the toplevel dialog
   (dialog-header :accessor dialog-header :initform nil)
   ;; keeps the dialog history as a set of string lists
   (dialog-history :accessor dialog-history :initform nil)
   (discarded-messages :accessor discarded-messages :initform nil)
   ;; saves here the value associated with error-contents 
   (error-contents :accessor error-contents :initform nil)
   ;; contains something like ("person" ("name" "barthès")("first-name" "Jean-Paul")))
   (master-identity :accessor master-identity :initform nil)
   ;; flag to prevent the creation of an interface window (e.g. when using voice)
   ;; default we open an interface window
   (no-window :accessor no-window :initform nil)
   ;; size of the font in the output window
   (output-font-size :accessor output-font-size :initform 9)
   ;; e.g. "@Arial Unicode MS / ShiftJIS" or "Tahoma / ANSI" 
   ;; when NIL we'll use (cg:font (cg:screen cg:*system*))
   (output-font-string :accessor output-font-string :initform nil)
   ;; request to the user posted by the assistant
   (pending-requests :accessor pending-requests :initform nil)
   ;; tasks that are not yet done (no answer was delivered)
   (pending-task-id :accessor pending-task-id :initform nil)
   ;; transféré dans appearance au niveau d'un SA
   ;(private-interface :accessor private-interface :initform nil)
   ;; stack of salient features for resolving pronaouns ?
   (salient-features :accessor salient-features :initform nil)
   ;; max length recommended for salient-features (default 100)
   (salient-features-max-length :accessor salient-features-max-length :initform 100)
   ;; max time in seconds for keeping entries (default 5mn)
   (salient-features-max-time :accessor salient-features-max-time :initform 300)
   ;; if non nil prints the whole dialog into the assistant's pane
   (show-dialog :accessor show-dialog :initform nil)
   ;; name of a file to be loaded as a task file
   (tasks-file :accessor tasks-file :initform nil)
   ;; contains input from the master, CONVERSE waits on this slot
   (to-do :accessor to-do :initform nil)
   ;; slot to indicate a direct connection to a voice I/O system
   (voice-io :accessor voice-io :initform nil)
   ;; port for receiving input strings
   (voice-input-port :accessor voice-input-port :initform 52010)
   ;; voice input socket
   (voice-input-socket :accessor voice-input-socket :initform nil)
   ;; IP of the machine containing the voice componenet (external to OMAS)
   (voice-ip :accessor voice-ip :initform "127.1") ; default current machine
   ;; maximal length of output string
   (voice-max-message-length :accessor voice-max-message-length :initform 4096)
   ;; port for sending string to be spoken
   (voice-output-port :accessor voice-output-port :initform 52011)
   ;; voice output socket
   (voice-output-socket :accessor voice-output-socket :initform nil)
   ;; process for receiving loop
   (voice-receiving-process :accessor voice-receiving-process :initform nil)
   ;; slot containing answers to master's requests
   (waiting-answers :accessor waiting-answers :initform nil)
   ;; slot containing messages to be processed
   (waiting-messages :accessor waiting-messages :initform nil)
   )
  (:documentation "object containing information passed to and from the master"))


;;; Extending methods from parts of an agent to apply to the agent as a whole.

;--- MASTER-BINS
(extend-method-for-assistant answer master-bins)
(extend-method-for-assistant answer-timeout master-bins)
(extend-method-for-assistant answer-to-return master-bins)
(extend-method-for-assistant answer-to-task-to-do master-bins)
(extend-method-for-assistant ask-sender master-bins)
(extend-method-for-assistant conversation master-bins)
(extend-method-for-assistant conversation-state master-bins)
(extend-method-for-assistant conversation-context master-bins)
(extend-method-for-assistant conversation-entry master-bins)
(extend-method-for-assistant output-font-size master-bins)
(extend-method-for-assistant details master-bins)
(extend-method-for-assistant dialog-file master-bins)
(extend-method-for-assistant dialog-header master-bins) 
(extend-method-for-assistant dialog-history master-bins)
(extend-method-for-assistant discarded-messages master-bins)
(extend-method-for-assistant error-contents master-bins)
(extend-method-for-assistant master-identity master-bins)
(extend-method-for-assistant output-font-string master-bins)
(extend-method-for-assistant no-window master-bins)
(extend-method-for-assistant pending-requests master-bins)
(extend-method-for-assistant pending-task-id master-bins) 
;(extend-method-for-assistant private-interface master-bins)
(extend-method-for-assistant show-dialog master-bins)
(extend-method-for-assistant to-do master-bins)
(extend-method-for-assistant salient-features master-bins)
(extend-method-for-assistant salient-features-max-length master-bins)
(extend-method-for-assistant salient-features-max-time master-bins)
(extend-method-for-assistant tasks-file master-bins)
(extend-method-for-assistant voice-io master-bins)
(extend-method-for-assistant voice-input-port master-bins)
(extend-method-for-assistant voice-input-socket master-bins)
(extend-method-for-assistant voice-ip master-bins)
(extend-method-for-assistant voice-max-message-length master-bins)
(extend-method-for-assistant voice-output-port master-bins)
(extend-method-for-assistant voice-output-socket master-bins)
(extend-method-for-assistant voice-receiving-process master-bins)
(extend-method-for-assistant waiting-messages master-bins)
(extend-method-for-assistant waiting-answers master-bins)
;;; special function to allow ACL to wait on to-slot
#+microsoft
(defUn wait-on-to-do (agent)
  (to-do (master-bins agent)))

;;;=============================== Creating functions ============================
;;; the :redefine option is deprecated, was used to reset and reload agent
;;; agents should be created in their own package

(defUn make-assistant (name &key interface hide (show-dialog t) master
                            package-nickname tasks-file dialog-file ontology-file
                            (language (or moss::*language* :en)) 
                            (context (or moss::*context* 0))
                            (version-graph (or moss::*version-graph* '((0))))
                            font size no-window npa
                            redefine learning ; kept for backward compatibility
                            voice voice-input-port voice-output-port voice-ip
                            )
  "creates an agent, puts it onto the *agents* list, name must be an unused symbol. ~
   On exit package is set to specified package or to agent package.
Arguments:
   name: name given to the agent (e.g. JEAN or :JEAN)
   context (key): context (version) of the ontology
   dialog-file (key): name of a specific dialog file (e.g. shared)
   interface (key): gives the name of a function in the agent package for building
                    a specific interface window different from the default one
                    e.g. if true calls MAKE-JEAN-WINDOW.
   language (key): language for the ontology (may be :all)
   master (key): used to set master identity in the language of the PA
      e.g. (\"person\" (\"name\" \"Barthès\")...)
   no-window (key): we do not want to open a PA window
   npa (key): Non Player Agent: represents some avatar and has no human master
   ontology-file (key): name of a specific ontology file (e.g. shared)
   package-nickname (key): nickname for the agent package
   show-dialog (key): if t prints the dialog history into the assistant pane
   task-file (key): name of a specific task file (e.g. shared)
   version-graph (key): version (configuration) graph of the ontology
   voice (key): if t specifies that the assistant can use a vocal interface
   voice-input-port (key): PA port of the receive UDP socket (default 52010)
   voice-ip (key): IP of the machine containing voice component
   voice-output-port (key): remote port on the voice component (default 52011)
Return:
   the agent internal CLOS object reference."
  (declare (ignore redefine learning)
           (special *package* *omas*))
  ;(format t "~%; make-assistant ~S /language: ~S" name language)
  (let (key package)
    ;;=== check if agent package exists
    (unless  (setq package (find-package name))
      ;; if not, create it
      (setq package 
            (if package-nickname
                (make-package name :use '(:cl :moss :omas) 
                              :nicknames (list package-nickname))
              (make-package name :use '(:cl :moss :omas)))))   
    ;; set current package to specified or agent package
    (setq *package* package)
    
    ;;=== chek if name is a symbol or a keyword, then set name to be a symbol
    ;; and key the corresponding keyword. PA name is PA_<assistant name>
    (cond
     ((keywordp name)
      (setq key name
          name (create-assistant-name key package)))
     ((symbolp name)
      (setq key (intern (symbol-name name) :keyword)
          name (create-assistant-name name package)))
     (t (error "the name ~A for the assistant must be a symbol or a keyword." name)))
    
    
    ;; if agent name is a symbol already bound to something, error
    (if (boundp name)
        (error "agent name, ~A, is already bound to ~A." 
          name (symbol-value name)))
    
    ;; if no error, declare agent name as a special variable
    (proclaim `(special ,name))
    
    ;;=== then create a whole new agent, piece by piece
    (let ((comm (make-instance 'comm))
          (control (make-instance 'control))  ; default status is :idle
          (self (make-instance 'self))
          (world (make-instance 'world))
          (ontologies (make-instance 'ontologies))
          (appearance (make-instance 'appearance))
          (tasks (make-instance 'tasks))
          (master-bins (make-instance 'master-bins))
          agent process)
      
      ;;=== create agent structure
      (setq agent (make-instance 'ASSISTANT 
                    :name name
                    :key key
                    :comm comm
                    :control control
                    :self self
                    :ontologies ontologies
                    :world world
                    :tasks tasks
                    :appearance appearance
                    :master-bins master-bins
                    ))
      ;; save a handle to the agent structure
      (set name agent)
      
      ;;=== store a handle for load-agent (will stay there if agent loaded manually)
      (setf (agent-being-loaded omas::*omas*) agent) ; JPB 0912
      
      ;;=== save global info into agent structure
      (setf (ontology-package agent) package)
      (setf (language agent) language)
      (setq moss::*language* language) ; JPB1104
      (setf (moss-context agent) context)
      (setf (moss-version-graph agent) version-graph)
      (setf (no-window agent) no-window)
      (setf (show-dialog agent) show-dialog)
      ;; if assistant is NPA, record it
      (setf (assistant agent) (if npa :npa t))  ; used when loading the agent
      (setf (master-identity agent) master)
      ;; if a font is specified, we'll use it for output
      (if font (setf (output-font-string agent) font))
      (if size (setf (output-font-size agent) size))
      ;; take care of case when we have a specific task or dialog file
      (setf (dialog-file agent) dialog-file)
      (setf (tasks-file agent) tasks-file)
      (setf (ontology-file agent) ontology-file)
      ;; if voice parameters are specified record them
      (if voice (setf (voice-io agent) voice))
      (if voice-input-port (setf (voice-input-port agent) voice-input-port))
      (if voice-output-port (setf (voice-output-port agent) voice-output-port))
      (if voice-ip (setf (voice-ip agent) voice-ip))
      
      ;; create MOSS classes and method proxy if not already there JPB 0912
      ;; create a MOSS instance representing the agent
      ;; second argument is nil since assistants are not persistent
      (agent-set-moss-environment agent nil)
      
      ;; add default skills (used essentially by PAs (JPB 051001)
      (agent-add-default-skills agent)
      ;; add PA default skills (ask, tell)
      (assistant-add-default-skills agent)
      ;; when learning is present we add it to the agent features
      ;;********** this should be removed
      ;(when learning (%agent-add agent :learning :features))
      
      ;; and add the name to the list of local agents if not already there
      (unless (or hide (assoc key (local-agents *omas*)))
        (setf (local-agents *omas*) 
          (append (local-agents *omas*) (list (cons key agent)))))
      ;; add name to names of known agents
      (pushnew name (names-of-known-agents *omas*))
      
      ;;=== if we have a Japanese option, we load the segmentation library
      (when (eql language :jp)
        (load (make-pathname 
               :device (pathname-device omas::*omas-directory*)
               :directory (append 
                           (butlast (pathname-directory omas::*omas-directory*)) 
                           '("aclmecab"))
               :name "install"
               :type "fasl")))
      
      ;;=== launch agent process working on the agenda
      (setq process (agent-process-create-restartable
                     agent
                     '(:type :main) ; no task being executed, process is main 
                     (agent-make-process-name agent "main" :fixed-name t)
                     #'agent-process-task 
                     agent))
      (setf (process agent) process)
      ;;=== launch then scanner process on input-messages queue
      (setq process (agent-process-create-restartable
                     agent
                     '(:type :scan) ; no task being executed, process is scan
                     (agent-make-process-name agent "scan" :fixed-name t)
                     #'agent-scan-messages 
                     agent))
      (setf (mbox-process agent) process)
      
      ;; the PA window is created by the "converse" process, otherwise ACL
      ;; could not write into it...
      
      ;;=== if the voice interface is specified, we must create the sockets and 
      ;; process
      (unless npa ; no voice interface for Non Player Agents
        (when (and voice (not (eql voice :delayed)))
          (let (test errno remote-ip)
            ;; if voice-ip is mentioned, transform it into an IP if it is a name
            (when (and voice-ip (not (socket:dotted-address-p voice-ip)))
              (multiple-value-setq (test errno)
                (ignore-errors
                 (setq remote-ip
                       (socket:ipaddr-to-dotted
                        (socket:lookup-hostname ; works on names and IPs
                         (voice-ip agent))))))
              ;; check if error
              (when (and (null test) errno)
                (cg:beep)
                (error "~&~S/ connect-static/ can't get IP for remote site: ~S" 
                  (key agent) (voice-ip agent))
                )
              (setf (voice-ip agent) remote-ip))
            
            (net-initialize-voice agent))))
      
      ;;=== check if we want a special interface window
      (when (and interface (null no-window))
        ;; name of a specific application window, see :CAAP application example
        ;; and we allow opening windows
        (setf (private-interface agent) interface)
        ;; cannot create the window now, because the necessary functions are not
        ;; yet loaded (they are in the agent file)
        )
      
      ;; return the internal name of the agent
      ;; ... with current package set to package option or agent package
      agent)))

;;;------------------------------------------------------------------ DEFASSISTANT

(defMacro defassistant (name &rest ll)
  "creates an agent, puts it onto the *agents* list, name must be an unused symbol ~
   unless the agent is to be redefined with the option (:redefine t).
Arguments:
   see the make-assistant function for options
Return:
   the agent identifier"
  `(apply #'make-assistant ',name ',ll))

;;; might have a problem for :interface arg (should be given with #')

;;;-------------------------------------------------------------- CREATE-ASSISTANT
;;; this function is meant to create assistants using different languages. It 
;;; requires that files exist corresponding either to the file names specified
;;; in the defassistant macros, or if no names are given, to standard names like
;;; HDSRI-FR-TASKS, HDSRI-FR-DIALOG, HDSRI-FR-ONTOLOGY where the suffix
;;; depends on the target language "FR", and the prefix to the application "HDSRI"
;;; Unless indicated otherwise, the cloned files are cloned, loaded, then deleted.
;;; Since the agent keyword is not part of the original agent list, one must load
;;; the different files manually.
;;; Example of call: (create-assistant :BARTHES :language :FR :stub "HDSRI")
;;; The stub argument is used to locate the assistant stub file, i.e. the file to
;;; clone. When specified, and the tasks, ontology and dialog files are not 
;;; specified in the defassistant macro, then it is used to prefix the standard
;;; names of such files, e.g. BARTHES-FR-ONTOLOGY, or TASKS-FR-ONTOLOGY.

(defun create-assistant (agent-key &key language keep stub)
  "creates a new assistant and loads it into the environment. The application ~
   folder should contain either the file corresponding to the stub argument or ~
   a file named ASSISTANT-STUB, and the files corresponding to the key arguments ~
   of the defassistant macro or standard files like APP-LL-ONTOLOGY, ~
   APP-LL-TASKS, and APP-LL-DIALOG file, where APP is the name of the application ~
   e.g. HDSRI and LL stands for the  2-letter-language code, e.g. EN or FR or JP, ~
   etc.
   Note that the new agent becomes part of the local-coterie, but the agents.lisp ~
   file is not updated. However, if the keep argument is t, then the assistant ~
   file is kept in the application folder.
Arguments:
   agent-key: key of the future assistant
   language (key): language of the future assistant (can be omitted if unambiguous)
   keep (key): if t, the assistant file is kept in the application folder
   stub (key): name of the assistant stub file, e.g. \"HDSRI\"." 
  (format t "~2%;---------- Entering create-assistant")
  
  (let ((prefix (or stub (format nil "~A" agent-key)))
        file-pathname)
    ;; first make an assistant file by cloning <PREFIX>-FR.lisp or <PREFIX>.lisp
    ;; if language is NIL 
    (setq file-pathname 
          (make-assistant-file agent-key :language language :prefix prefix))
    
    ;;============================================================================
    ;;                 load AGENT file, creating agent
    ;;============================================================================
    (unless (probe-file file-pathname)
      (error "Missing agent file: ~S" file-pathname))
    ;; now load the stuff    
    (compile-and/or-load file-pathname :external-format :utf-8) 
    (format t "~%;--- keep: ~S" keep)
    (unless keep (delete-file file-pathname))
    
    ;;============================================================================
    ;;                             load ONTOLOGY
    ;;============================================================================
    ;; create an ontology file by cloning SHARED-ONTOLOGY
    (setq file-pathname 
          (make-assistant-ontology-file agent-key language prefix))
    
    (unless (probe-file file-pathname)
      (warn "Missing agent ontology file: ~S" file-pathname))
    
    (moss::catch-error
     (format nil "while loading ~A ontology" agent-key)
     (and (probe-file file-pathname) 
          (moss::m-load file-pathname :external-format :utf-8)))
    (delete-file file-pathname)
    
    ;;============================================================================
    ;;                            load TASK file
    ;;============================================================================
    ;;=== load the agent SHARED TASKS file (only for a PA)
    (setq file-pathname (make-assistant-tasks-file agent-key language prefix))
    
    (unless (probe-file file-pathname)
      (warn "Missing agent task file: ~S" file-pathname))
    
    ;(format *debug-io*  "~&;load-agent /tasks: ~S" file-pathname)    
    (moss::catch-error
     (format nil "while loading ~A tasks" agent-key)
     (with-package (ontology-package (agent-being-loaded *omas*))
       (moss::m-load file-pathname :external-format :utf-8)))
    (delete-file file-pathname)
    
    ;;============================================================================
    ;;                             load DIALOGS
    ;;============================================================================
    ;;=== load the agent DIALOG file
    (setq file-pathname (make-assistant-dialog-file agent-key language prefix))
    
    (unless (probe-file file-pathname)
      (warn "Missing agent dialog file: ~S" file-pathname))
    
    ;(format *debug-io*  "~&;load-agent /dialog: ~S" file-pathname)    
    (moss::catch-error
     (format nil "while loading ~A dialogs" agent-key)
     (with-package (ontology-package (agent-being-loaded *omas*))
       (and (probe-file file-pathname) 
            (compile-and/or-load file-pathname :external-format :utf-8))))
    (delete-file file-pathname)
    
    ;;=== start conversation, which creates a new thread
    (start-converse-process (agent-being-loaded *omas*))
    ;; reset temporary variable
    (setf (agent-being-loaded *omas*) nil)
    ;; add agent to list of local agents
    
    :done))

;;;-------------------------------------------- CREATE-ASSISTANT-FROM-SHARED-FILES
;;; This function should be now useless. Use create-assistant.

(defun create-assistant-from-shared-files (agent-key &key (language :EN) keep stub)
  "creates a new assistant and loads it into the environment. The application ~
   folder should contain an ASSISTANT-STUB file, a SHARED-ONTOLOGY file, a ~
   SHARED-TASKS file, and a SHARED-DIALOG file. Such files are cloned and ~
   adjusted to the new agent before being loaded, then loaded, then discarded.
Arguments:
   agent-key: key of the future assistant
   language (key): language of the future assistant
   keep (key): if t, wil keep the agent file in the application folder, otherwise
               will delete the temporary file.
   stub (key): name of the STUB file from which the assistant is created."
  (format t "~2%;---------- Entering create-assistant-from-shared-files")
  
  (let (file-pathname)
    ;; first make an assistant file by cloning ASSISTANT-STUB
    (setq file-pathname 
          (make-assistant-file agent-key :language language :prefix stub))
    
    (unless (probe-file file-pathname)
      (error "Missing agent file: ~S" file-pathname))
    ;; now load the stuff    
    (load-agent :agent-file-pathname file-pathname)
    
    (unless keep
      ;; the get rid of the file to avoid cluttering the application folder
      (format t "~%;--- Deleting ~S: " file-pathname) ; JPB1412
      (delete-file file-pathname)
      )
    ))

;;;----------------------------------------------------------- MAKE-ASSISTANT-FILE
;;; We can use
;;; (clone-and-adjust-file "BARTHES" "HDSRI-FR" :substitutions '(("LLLLL" . "FR")))

(defun make-assistant-file (key &key language (prefix "ASSISTANT-STUB"))
  "creates an assistant file by cloning ASSISTANT-STUB.lisp replacing XXXXX by the ~
   print-name of the key argument, e.g. BARTHES.lisp.
Arguments:
   key: key of the future assistant
   language (key): language of the future assistant
   stub (key): a string, name of the stub file
Return:
   pathname of the output file named <PREFIX>{-LL>}-TMP"
  (clone-and-adjust-file
   ;; agent key
   (string+ key)
   ;; file to clone
   (if language (string+ prefix "-" language) ; e.g. "HDSRI-FR"
     prefix) ; e.g. "HDSRI"
   ;; substitutions
   :substitutions (if language `(("LLLLL" . ,(symbol-name language))))))

;;;---------------------------------------------------- MAKE-ASSISTANT-DIALOG-FILE

(defun make-assistant-dialog-file (key language prefix)
  "creates an assistant file by cloning <PREFIX>-DIALOG-<LL>.lisp replacing XXXXX ~
  by the print-name of the key argument."
  (let ((tag (if language (string+ "-" language) "")))
    (clone-and-adjust-file
     ;; agent key
     (string+ key)
     ;; file to clone, e.g. "HDSRI-FR-DIALOG" or "HDSRI-DIALOG"
     (string+ prefix tag "-DIALOG")
     ;; output file, e.g. BARTHES-FR-DIALOG
     :out-filename (string+ key tag "-DIALOG")
     ;; substitutions
     :substitutions (if language `(("LLLLL" . ,(symbol-name language))))))
  )

;;;-------------------------------------------------- MAKE-ASSISTANT-ONTOLOGY-FILE

(defun make-assistant-ontology-file (key language prefix)
  "creates an assistant file by cloning <PREFIX>-{LL-}ONTOLOGU.lisp replacing ~
   XXXXX by the print-name of the key argument, e.g. BARTHES-FR-ONTOLOGY.lisp."
  (let ((tag (if language (string+ "-" language) "")))
    (clone-and-adjust-file
     ;; agent key
     (string+ key)
     ;; file to clone, e.g. "HDSRI-FR-ONTOLOGY"
     (string+ prefix tag "-ONTOLOGY") ; e.g. "HDSRI-ONTOLOGY"
     ;; output file, e.g. "BARTHES-FR-ONTOLOGY
     :out-filename (string+ key tag "-ONTOLOGY")
     ;; substitutions
     :substitutions (if language `(("LLLLL" . ,(symbol-name language)))))
    ))

;;;----------------------------------------------------- MAKE-ASSISTANT-TASKS-FILE

(defun make-assistant-tasks-file (key language prefix)
  "creates an assistant file by cloning <PREFIX>-TASKS{-<LL>}.lisp replacing XXXXX ~
  by the print-name of the key argument, e.g. BARTHES-TASKS-FR.lisp."
  (let ((tag (if language (string+ "-" language) "")))   
    (clone-and-adjust-file
     ;; agent key
     (string+ key)
     ;; file to clone, e.g. "HDSRI-FR-TASKS"
     (string+ prefix tag "-TASKS") ; e.g. "HDSRI-TASKS"
     ;; output, e.g. BARTHES-FR-TASKS
     :out-filename (string+ key tag "-TASKS")
     ;; substitutions
     :substitutions (if language `(("LLLLL" . ,(symbol-name language)))))
    ))

;;;---------------------------------------------------------------- SPECIFIC-TO-DO
;;; this function is used for wakening the process when the :from value of the 
;;; input equals the agent-key, meaning that the input is from the agent
;;; possible inputs are:
;;;   (:from <agent-key> {:mode :web-client} :data <string>)
;;;   (:from <agent-key> {:mode :external} :data <data>)

(defun specific-to-do (agent)
  ;; when running with the legacy format (mono conversations) anything non nil
  ;; will wake up the process
  #-:converse-v2
  (to-do agent)
    
  ;; in the new approach
  #+:converse-v2
  (let ((input (to-do agent)))
    ;; we should have a list (if we ignore the input, then the agent might wait
    ;; indefinitely)
    (unless (listp input)
      (error "the content of (to-do ~S) should be a list rather then ~S"
        agent input))
    
    ;; if the following condition is filled we can wake up the agent
    (or 
     ;; the input comes from an agent in a recorded conversation
     (assoc (getf (to-do agent) :from) (conversation agent))
     ;; for answers returned by external agents, the message is addressed to 
     ;; this PA
     (eql (omas::key agent) (getf (to-do agent) :to)))))

;;;-------------------------------------------- (ASSISTANT) START-CONVERSE-PROCESS

(defMethod start-converse-process ((agent assistant))
  "create the converse process and the interaction window."
  (setf (assistant-process agent) 
    (agent-process-create-restartable
     agent
     '(:type :master) ; process is master (is it necessary?) 
     (agent-make-process-name agent "converse" :fixed-name t)
     #'assistant-start-action 
     agent))
  )

; (start-converse-process albert::albert)
;;;==============================================================================
;;;
;;;                        Default Generic Skills
;;;
;;;==============================================================================

;;; clearly assistant agents should have generic skills
;;; we give an agent the capacity to send messages to other agents (?)
;;; i.e. we can ask an agent to send a message

;;; Methods are added by assistant-add-default-skills

;;;======================================================================== skill
;;; ASK
;;;==============================================================================

;;; The following function takes an input message, looks at its content and
;;; decides either to post it to the master or to discard it, posting it into 
;;; the garbage bin.
;;; Defined as:
;;;  (make-skill :ask assistant
;;;              :static-fcn 'ask-static
;;;              )

;;;------------------------------------------------------------------- ASK-STATIC
;;; this function is executed in a specific thread due to the :request
;;; performative
;;; The message is posted into the "tasks to do" pane it will be processed in a
;;; dialog triggered by the :process-ask tag
;;; In order to synchronize the answer, a gate is created and closed and the
;;; incoming message and gate number are saved in the waiting-tasks slot of the PA
;;; the mechanism uses the web gate functions
;;; The previous mechanism was to install the answer into a slot of the assistant,
;;; which does not take into account several tasks being done simultaneously
;;; processing a task-to-do requires using the dialog. There could be some problem
;;; here if a complex dialog is interrupted. A possible solution is to use a standard
;;; :process-task-to-do subdialog and insert a test into the crawler to implement
;;; the subdialog as an interrupting subdialog (change of context)

(defUn ask-static (agent message &rest args)
  "takes an input message and post it
Arguments:
   agent: PA
   args (rest): content of an :ask request
Return
   returns through static-exit."
  (declare (special *omas*)(ignore args))
  ;; create a gate to synchronize answer
  (let ((gate-index (gate-create)))
    ;; add and display result, but pass gate index needed to send back answer
    (assistant-add-task-to-do agent (cons message gate-index))
    
    ;; wait until the gate opens or the process is killed (default 1 hour)
    (unwind-protect
        (mp:process-wait "waiting :ASK processing" 
                         #'mp:gate-open-p 
                         (cadr (assoc gate-index (gate-list *omas*))))
      
      ;; if the process is killed by the timeout, then the clean up will be done
      
      ;; remove enry from the window pane (td-list is ignored)
      (mw-td-discard-on-click (com-window agent) :td-list)
      ;; close gate, removing it from the gate list
      (gate-close gate-index)    
      )
    (static-exit agent (or (gate-pop-data gate-index) :failure))))

;;;=================================================================== TELL SKILL
;;; The following function takes an input message, looks at its content and
;;; decides either to post it to the master or to discard it and post it into 
;;; the garbage bin.
;;; Defined as:
;;;  (make-skill :tell assistant
;;;              :static-fcn 'tell-static
;;;              )
;;; It is similar to an :ask skill but the sender does not expect an answer.

;;;======================================================================== skill
;;; CONVERSE (requires MOSS)
;;;==============================================================================
;;; the following skill is an attempt to run a conversation process as a skill
;;; rather than having a dedicated thread for doing so.
;;; This is a conversation between the PA and its master or a neutral conversation
;;; Input comes into the assistant TO-DO slot, output is done by the =display-text
;;; from within the MOSS dialog methods.
;;; the to-do slot contains an alternated list (:from <partner> :text "...")
;;; e.g.  (:from :ML :text "Hello, Olga!") so that the data trigger only the
;;; appropriate waiting thread

(defun converse-static (agent message &key contact)
  "sets up a single conversation process that will run in this thread.
Arguments:
   agent:..
   message: ignored
   contact (key): either assistant key for dialog with the master, or another PA
Return:
   never returns, but will timeout after 1 hour, or eventually crash."
  (declare (ignore message))
  ;; when there is no specified contact, do nothing
  (unless contact 
    (return-from converse-static (static-exit agent :done)))
  
  ;; first establish process global variables
  (let (win conversation-id dialog-header initial-state)
    
    ;;=== check dialog header
    ;; all conversations use the main header from the dialog file
    (setq dialog-header (dialog-header agent))
    (unless dialog-header
      (error "CONVERSE skill: cannot run conversation without dialog header."))
    (unless (moss::%type? dialog-header 'moss::$QHDE)
      (error "CONVERSE skill: unknown conversation reference ~S" dialog-header))
    
    (setq initial-state (car (has-moss-entry-state dialog-header)))

    ;;=== create a MOSS conversation object (in the MOSS package)
    (setq conversation-id (make-individual "moss-conversation"))
    ;; link conversation to conversation state slot (what for?)
    (setf (conversation-state agent) conversation-id)
    ;; record the interlocutor
    (setf (moss::has-moss-contact conversation-id) contact)
    ;; record initial state
    (setf (has-moss-initial-state conversation-id) (list initial-state))
    ;; record it as current state
    (setf (has-moss-state conversation-id) (list initial-state))
    (setf (has-moss-agent conversation-id) (list agent))
    (setf (has-moss-dialog-header conversation-id) (list dialog-header))
    
    (cond
     ;;== if contact is the PA itself, we interact with the Master
     ((eql (key agent) contact)
      (dformat :dia 0 "contact is the PA itself: ~S" contact)
      ;; add this conversation channel to the other ones
      (push (list contact conversation-id) (conversation agent))
      
      ;; create interface window, special call if private
      (unless (no-window agent)
        (dformat :dia 0 "PA ~S has a window interface" (key agent))
        (setq win 
              (cond
               ;; if private, call private function
               ((private-interface agent)
                (funcall (private-interface agent) agent))
               ;; if default check for small window
               ((eql (omas-window-type *omas*) :small)
                (dformat :dia 0 "we build a small window")
                ;; *PA-small-window*
                (make-pa-small-window agent))
               ;; otherwise make regular windoe-w
               (t
                ;; *master-window*
                (make-master-window agent))))
        ;; record window name in agent
        (setf (com-window agent) win)
        (dformat :dia 0 "we record the window reference in (com-window agent)~%~S"
                 win)
        ;; set channels when we have a window interface (not a Web client)
        (setf (moss::has-moss-input-window conversation-id) win)
        (setf (moss::has-moss-output-window conversation-id) win)
        )
      )
     ;;== otherwise we create a neutral conversation to wait for random inputs
     (contact
      ;; set the channels to contain the contact key
      (setf (moss::has-moss-input-window conversation-id) contact)
      (setf (moss::has-moss-output-window conversation-id) contact)
      
      ;; add this conversation channel to the other ones
      (push (list contact conversation-id) (conversation agent))
      ))
    
    ;; here we install a dialog crawler for this particular dialog
    (drformat :dia 0 "~2%;===== ~S Starting ~S conversation with state: ~S" 
              (key agent) contact (car (has-moss-label initial-state)))
    (dformat :dia 0 "... and call the function starting the conversation")
    ;; we enter the dialog loop and never return...
    ;(assistant-start-conversation-v2 agent dialog-header contact)
    (assistant-run-dialog-v2 agent conversation-id initial-state)
    
    (static-exit agent :done)))

;;;======================================================================== skill
;;; TELL
;;;==============================================================================

(defUn tell-static (agent environment &rest args)
  "takes an input message and post it. Clicking the EXAMINE displays details.
Arguments:
   agent: PA
   args (rest): content of a :tell messsage
Return
   returns through static-exit."
  (declare (ignore environment args)(special *current-message*))
  ;; add item (message . process-id) to the agent waiting-messages list
  (assistant-add-answer-info agent *current-message*)
  ;; update the AP window (?)
  (assistant-display-answers-to-examine agent)
  ;; quit (nothing returned to sender), normally should be an inform message
  (static-exit agent :abort))

;;;=============================== Proxy's com window ============================

;;;------------------------------------------------------------------- AGENT-RESET

(defMethod agent-reset 
    ((agent assistant) &key reset-self (reset-world t) reset-tasks 
     reset-skills no-position no-log &aux process)
  "reset an agent input/output trays (comm part), control part and appearance part.
Arguments:
   agent agent to be reset
   reset-self: (key) if t reset data, goals, memory, intentions
   reset-world: (key) if t reset acquaintances, environment, external-services
   reset-tasks: (key) if t reset projects, waiting-tasks
   reset-skills: (key) if t reset skills
   no-position: (key) if t does not reset agent position in graphics trace
   no-log: (key) if t does not reset logs
Return:
   :done"
  (declare (ignore no-log no-position reset-skills reset-tasks reset-world 
                   reset-self))
  ;; first reset the standard SA
  (call-next-method)
  ;; then take care of special assistant stuff
  ;; only assistant agents have a converse process
  (setq process (assistant-process agent))
  ;; kill it
  (agent-process-kill agent process)
  ;; empty to-do buffer
  (setf (to-do agent) nil) ; clear
  ;; relaunch then converse process
  (setq process (agent-process-create-restartable
                 agent
                 '(:type :converse)
                 (agent-make-process-name agent "converse" :fixed-name t)
                 #'assistant-start-conversation 
                 agent))
  ;; save process id
  (setf (assistant-process agent) process)
  :done)

;;;-------------------------------------------- ASSISTANT-ACTIVATE-ANSWER-PANEL-V2
;;; This function should be called for activating the input channel and waiting for
;;; input; hence the semantic of this function is that we prepare to receive 
;;; something in the TO-DO slot of the agent.
;;; - If the input is a window, then calls the moss::activate-input method attached
;;;   to the window (MOSS-INPUT-CHANNEL contains the reference to the window and 
;;;   not to the input pane of the window)
;;; - Otherwise, then input may come from a Web client or from an external agent
;;;   in that case there is nothing to do but wait (when the conversation is with 
;;;   another agent, MOSS-INPUT-CHANNEL contains the agent key; when it is a web
;;;   client it is the PA key)

;; when we have an input from an asssistant agent, we wait on the TO-DO slot
;; of the agent structure
;; If the conversation is with the master, then input can come
;;  - from the interface window (default or specific) (:from :ML :text "...")
;;  - from the Web client (:from :WEB :text "...")
;;  - from an external source (other agent) answering a request 
;;      (:from :victim1 :answer :failure/92)
;;  - NOT from another PA (:tell/:ask) since there are specific conversations
;;    for that case  (:from :NURSE-1 :text "...")
;; If the conversation is with another PA, then the answer comes from that PA

;; In case of timeout we have a failure message. The default
;; timeout is for all practical purposes infinite (most-positive-fixnum)
;; and is contained in the assistant ANSWER-TIMEOUT slot.
;; when the input comes from another agent, then it is inserted into the 


(defmethod assistant-activate-answer-panel-v2
    ((agent agent) conversation)
  "activates the answer panel specified in the conversation object ~
   (when the panel is t, listener, reads the input as a list). Reads in the ~
    data and puts the result into the FACT/RAW-INPUT slot of the conversation ~
    object.
Argument:
   conversation: conversation object
Return:
   text"
  (unless conversation
    (warn "Can't activate answer panel if no conversation is active.")
    (return-from assistant-activate-answer-panel-v2 nil))
  
  (drformat :dia 0
            "~2%;----------------------------------------------------------------")
  (drformat :dia 0
            "~%; ~A                    waiting for input (v2)"
            (key agent))
  (drformat :dia 0
            "~%;----------------------------------------------------------------")
  
  (let ((win (car (send conversation '=get "MOSS-INPUT-WINDOW")))
        answer)
    
    ;;=== Now the case where we should have a window
    (when (typep win 'stream)
      ;; win should have an activate-input method in the MOSS package!
      (moss::activate-input win))
    
    ;;=== now set up the waiting loop    
    ;; wait for a new input into the TO-DO agent slot (default is indefinitely,
    ;; i.e. most-positive-fixnum)
    ;; for the current conversation (either with the master or with another PA)
    ;; a to-do entry is an alternated list, e.g. (:from :ml :text "...")
    (if (mp:process-wait-with-timeout 
         (format nil "waiting for ~S input" (key agent))
         (answer-timeout agent)
         #'specific-to-do agent)
        ;; here we got an answer
        (setq answer (to-do agent))
      ;; here nil means that we had a timeout, initialize to-do slot
      (setq answer 
            `(:from ,(key agent) :failure t :reason "Timeout: No answer"))
      )
    (dformat :dia 0 "(to-do agent): ~S" answer)
    
    ;;=== answer should be a list otherwise we'll get an error, which kills the 
    ;; thread
    (unless (listp answer)
      (error "assistant-activate-answer-panel-v2/ input: ~S from ~S is not a list." 
        answer (key agent)))
    
    ;; reset the waiting trigger
    (setf (omas::to-do agent) nil)
    
    ;;=== reorganize FACTS according to type of input
    (cond
     ;; failure may occur in an answer from a PA request to an external source
     ;; or from a timeout (no answer)
     ((or (getf answer :failure)
          (eql :failure (getf answer :answer)))
      (moss::replace-fact conversation :raw-input :failure)
      ;; maybe the reason for failure is provided...if so, record it
      (moss::replace-fact conversation :reason (getf answer :reason)))
     
     ;; answer from another agent, but not a failure
     ((getf answer :answer)
      (moss::replace-fact conversation :raw-input (getf answer :answer))
      )
     
     ;; anything else (input from master, from external agent) is sent to the 
     ;; raw-input slot; we may have filled patterns (?)
     (t
      (moss::replace-fact conversation :raw-input (or (getf answer :text)
                                                      (getf answer :data)))
      )
     )

    ))

#|
;; activate new input style
(pushnew :converse-v2 *features*)
;; trace conversation
(dformat-set :dia 0)

;; select a conversation object
(with-package :ml
  (omas::assistant-activate-answer-panel-v2 ml::PA_ml 'moss::$CVSE.1))

;; use some agent window input something

;; have the agent wait from a request answer

|#
;;;-------------------------------------------------- ASSISTANT-ADD-DEFAULT-SKILLS

(defMethod assistant-add-default-skills ((assistant assistant))
  "adds skills particular to an assistant, e.g. :tell and :ask that will filter ~
   messages before either disarding them or posting them for the master"
  ;; tell is an atomic skill: somebody wants to tell something to master
  (make-skill :tell assistant
              :static-fcn 'tell-static
              )
  ;; ask is also an atomic skill: somebody is asking something to master
  (make-skill :ask assistant
              :static-fcn 'ask-static
              )
  ;; test for adding a skill handling the conversation process
  (make-skill :converse assistant
              :static-fcn 'converse-static
              )
  )

;;;------------------------------------------------ ASSISTANT-CLEAN-ASSISTANT-AREA

(defMethod assistant-clear-assistant-area ((agent agent))
  "erase the assistant posting area when active.
Arguments:
   agent: self."
  (declare (ignore message))
  ;; currently we do not discard any message
  (let ((master-area (com-window agent)))
    ;; if active clean the master area
    (if (and master-area (wptr master-area))
        (set-dialog-item-text (view-named 'assistant-output master-area) ""))))

;;;--------------------------------------------------- ASSISTANT-CLEAN-MASTER-AREA

(defMethod assistant-clear-master-area ((agent agent))
  "erase the master typing area when active.
Arguments:
   agent: self."
  (declare (ignore message))
  ;; currently we do not discard any message
  (let ((master-area (com-window agent)))
    ;; if active clean the master area
    (if (and master-area (wptr master-area))
        (set-dialog-item-text (view-named 'master-input master-area) ""))))

;;;--------------------------------------------------- ASSISTANT-DIALOG-CRAWLER-V2

(defmethod assistant-dialog-crawler-v2 ((agent agent) conversation &key state)
  "works as follows:
   - start from the initial state of the conversation header or a specific state
   - send an =execute method to the state
   - makes a transition according to what is returned by the =execute method
      - if :resume send a =resume method to the current state
      - if :success or :failure (subdialogs) make the correponding transition
   - after a =resume method
      - if :success or :failure (subdialogs) return the corresponding state
      - if a state object makes the transition
Arguments:
   conversation: a conversation object
   state (key): a specified state of the conversation
Return:
   when exiting from a sub-dialog, returns a state, e.g. :failure or _xx-state."
  ;; check type of first argument
  (unless (moss::%type? conversation 'moss::$CVSE)
    (error "unknown conversation object ~S" conversation))
  
  ;; fetch dialog header from the conversation object and initial state from the
  ;; dialog header
  (let* ((dialog-header (car (moss::%get-value conversation 'moss::$DHDRS)))
         (initial-state (car (moss::%get-value dialog-header 'moss::$ESTS)))
         (count 0)  ; will count the number of transitions
         old-state result action arg-list raw-input old-header
         saved-state saved-conversation)
    ;; check state if not given, start with initial state
    (unless state
      (setq state initial-state))
    ;; trace what we are doing
    (drformat :dia 0 "~2%;========== ~S Entering dialog-state-crawler ~%; 
         ~S/state: ~S; package: ~S" (key agent) count state *package*)
    
    (loop
      ;; at the beginning of the loop the state variable may have several distinct
      ;; values: 
      ;;  - a state id (normal case)
      ;;  - :failure or :success when returning from a sub-dialog
      ;;  - :return to return from an interrupting sub-conversation
      
      ;; first increase value of turn/step counter (for debugging purposes)
      (incf count)
      
      ;; the catch directive is used to reset the control at the beginning of the
      ;; loop when an interrupting dialog terminates
      (catch 
       :start
       
       ;; save state and conversation for resetting them after an interrupting
       ;; subdialog
       (setq saved-state state 
           saved-conversation (copy-list (symbol-value conversation)))
       
       ;; then process special cases
       (case state
         
         ((:failure :success)
          ;; pop task-frame-list to return to calling (super-)dialog
          (setq old-state state
              old-header (car (has-MOSS-sub-dialog-header conversation))
              ;; get transition state from the task-frame-list
              state (assistant-return-from-sub-dialog-v2 agent conversation state))
          (dformat :dia 0 "~S/returning from ~S sub-dialog.~%state: ~S"
                   count (if old-header (car (has-MOSS-label old-header))) state)
          ;; if OMAS is active put a stackframe mark onto the salient features queue
          ;(dialog-state-crawler-put-sf-end conversation `(quote ,old-header))
          )
         
         ;; here we return from an incident dialog to the main one
         (:return
          ;;********** should pop frame-list and set context???
          ;; restore sub-dialog header and state-context. state stays the same
          (dformat :dia 0 "~S/...returning from side-conversation." count)
          (return-from assistant-dialog-crawler-v2))
         
         (otherwise
          ;; here, we have a transition (state or header)
          ;; when transition is sub-conversation header, we make a transition
          ;; onto the input state of the sub-conversation (in make-transition)
          (setq state (assistant-make-transition-v2
                       agent conversation old-state state))
          ;; check for errors or illegal states
          (unless (moss::%type? state 'moss::$QSTE)
            (error "illegal state object ~S" state))
          
          ;; here we have a bona fide case and launch the =execute method
          (drformat :dia 0 "~2%================== STATE: ~S" 
                    (car (HAS-MOSS-LABEL state)))
          
          (setq result (send state "=execute" conversation))
          ;; "=execute" rather than '=execute so that name is interned into current
          ;; execution package
          ;; possible values for result are:
          ;;   - (:wait) to wait for the user's input
          ;;   - (:resume) we do not wait (no question asked)
          ;; if there is a :text or :question option and :web is t, then they are
          ;; stored onto the :text-to-print and :question-to-print FACTS fields
          ;(vformat "~%; dialog-state-crawler /*language*: ~S" *language*)
          
          (setq action (car result)
              old-state state)
          ;(vformat "dsc ~S/=execute returned:~%   ~S" count result)
          
          ;; do something according to the value of result
          (case action
            
            ;; if we asked something to the user, then we must wait for the answer, 
            ;; i.e.
            ;; until something appears in the TO-DO slot of the conversation object
            (:wait
             
             ;; activate the interface and wait for an input. The input may come: 
             ;;   - from the master (keyboard or vocal)
             ;;   - from a message: an answer to a PA request (may be :failure)
             ;;   - from the click of the PROCESS button associated with the tasks 
             ;;     to do
             ;; when :web is true, prints the saved texts
             
             (assistant-activate-answer-panel-v2 agent conversation)
             ;; now, we got some answer in FACTS/RAW-INPUT, whether from the master 
             ;; or from other agents. Several cases:
             ;;   - :failure (e.g. from an agent's answer)
             ;;   - verbatim input (for long texts)
             ;;   - string short text
             ;;   - structured list from another agent's answer (pattern)
             (dformat :dia 0 "~S/answer received - FACTS:~%~{~&   ~S~}" 
                      count (has-MOSS-facts conversation))
             
             ;; get input data
             (setq raw-input (moss::read-fact conversation :raw-input))
             
             (cond
              ;; if the flag :verbatim is set, it means that we want the text to be
              ;; passed without analysis
              ((moss::read-fact conversation :verbatim)
               (moss::replace-fact conversation :input (list raw-input)))
              ;; the :verbatim flag should be reset in the calling dialog
              
              ;; if a failure transfer it to :input
              ((eql raw-input :failure)
               (moss::replace-fact conversation :input (list :failure)))
              
              ;; if we have a string, we go process it (segmenting it) and putting
              ;; the result into FACTS/INPUT
              ((stringp raw-input)
               (moss::replace-fact conversation :input
                                   (moss::segment-master-input 
                                    (car (has-moss-agent conversation))
                                    raw-input)))
              
              ;; if raw-input is a list starting with :interrupt, we call the 
              ;; dialog-state-crawler-interrupt function that will prepare a recursive
              ;; call to dialog-state-crawler
              ((and (listp raw-input)(eql (car raw-input) :interrupt))
               (dformat :dia 0 "~S/saved conversation:~%   ~S" 
                        count saved-conversation)
               (dformat :dia 0 "~S/saved-state: ~S~%" count saved-state)
               ;; when returning from this function (actually from a recursive call
               ;; to dialog-state-crawler, the state variable will be the current
               ;; one, but the FACTS area should be reinstalled and the FRAME-LIST
               ;; poped (done by dialog-state-crawler-interrupt)
               (assistant-dialog-state-crawler-interrupt-v2
                agent conversation state count)
               ;; the state variable is that of the state prior to the interruption
               (setq state saved-state)
               ;; the conversation is reset to the one prior to the interruption
               (set conversation saved-conversation)
               ;; we should tell master that we go back to the interrupted conversation
               ;; here we restart the turn at the =execute level
               
               (dformat :dia 0 "~S/input processed - FACTS:~%~{~&   ~S~}" 
                        count (has-MOSS-facts conversation))
               (dformat :dia 0 "~S/state: ~S~%" count state)
               (throw :start nil)
               )
              
              ;; otherwise, put the result into FACTS/ANSWER
              (t
               (moss::replace-fact conversation :answer raw-input)
               ;; put a mark into FACTS/INPUT to avoid positive failure test
               ;; it seems that nobody uses that mark. Indeed, when an answer is
               ;; expected, the concerned functions fetch the data from FACTS/ANSWER
               ;; directly
               (replace-fact conversation :input (list "*see-answer*"))))
             
             (dformat :dia 0 "~S/input processed - FACTS:~%~{~&   ~S~}" 
                      count (has-MOSS-facts conversation))
             
             ) ; end of first part or :wait case, go execute =resume method
            
            (:resume
             ;; case when no question was asked to master. exit from cond, will call 
             ;; =resume method
             (dformat :dia 0 "~S/FACTS:~%~{~&   ~S~}" 
                      count (has-MOSS-facts conversation))
             )
            
            (otherwise
             ;; anything else than :wait or :resume is an error
             ;(trformat "dialog-state-crawler ~S/bad =execute return: ~S" count  action)
             (throw :dialog-error nil))
            )
          
          ;;============================================================================
          ;; call now the =resume method possible return values are:
          ;;     (:failure) (:success) (:transition <state>) (:sub-dialog ...)
          ;;============================================================================
          (dformat :dia 0 "~S/calling =resume" count)
          (dformat :dia 0 "~S/FACTS:~%~{~&   ~S~}" 
                   count (has-MOSS-facts conversation))
          
          (setq result (send state "=resume" conversation)
              action (car result)
              arg-list (cdr result))
          ;(trformat "dialog-state-crawler ~S/=resume returned: ~S" count result)
          (dformat :dia 0 "~S return-from-resume/FACTS:~%~{~&   ~S~}" 
                   count (has-MOSS-facts conversation))
          ;; analyze results from :resume
          (case action
            
            (:transition
             ;; normal transition
             (setq state (car arg-list))
             )
            
            ((:failure :success) 
             (setq state action)
             ;; do nothing, return to the start of the loop
             )
            
            (:sub-dialog
             ;; we step into the sub-dialog, getting the entry state
             (dformat :dia 0 "calling subdialog: ~S" (car arg-list))
             (setq state 
                   (apply #'assistant-make-transition-to-sub-dialog-v2
                          agent
                          conversation
                          state
                          (eval (car arg-list))
                          (cdr arg-list)))
             ;; the subdialog returns a transition state
             ;(trformat "dialog-state-crawler ~S/subdialog returned: ~S" 
             ;          count state)
             ;; if OMAS is active put a stackframe mark on salient features queue
             ;; record sub-dialog-header onto the saleint list in case of a PA
             ;(dialog-state-crawler-put-sf conversation (car arg-list))
             )
            
            (otherwise
             ;; anything else is illegal and aborts the dialog turn
             (throw 
                 :dialog-error
               (format nil 
                   "illegal value returned by the =resume method of state ~S"
                 state)))
            )
          ;; end processing =resume returns
          ) ; end of the otherwise case
         ) ; end of the top level case
       
       ;;=== debug trace
       ;; prints leading stars (;*****), then skips a line and prints the rest
       ;(trformat "~2%;===== dialog-state-crawler ~S/transition to: ~S~%;   ~S" 
       ;          count state (if (keywordp state) state (HAS-MOSS-LABEL state)))
       ;(print-q-state  conversation)
       ) ; end catch
      ) ; end loop
    ))

;;;----------------------------------------- ASSISTANT-DIALOG-CRAWLER-INTERRUPT-V2

(defmethod assistant-dialog-crawler-interrupt-v2
    ((agent agent) conversation state count)
  "handles dialog interruptions by creating a new sub-dialog to process the ~
   interruption. facts:raw-input contains a list (:interrupt <info>)
Arguments:
   conversation: the current conversation object
   state: the current state
   count: turn number (used for debugging)
Return:
   nothing interesting since we use frame-list stack"
  (declare (ignore state))
  (let (next-state raw-input todo-message frame)
    (drformat :dia 0 "~2%;========== ~S ~S/interrupting conversation...~%" 
              (key agent) count )
    (dformat :dia 0 "~S/input processed - FACTS:~%~{~&   ~S~}" 
             count (has-MOSS-facts conversation))
    ;(break "...frame-list:~%  ~S" (has-frame-list conversation))
    ;; Here we start a new conversation segment
    ;; In that case, we call the crawler recursively and set
    ;; failure and success return values to be :return
    ;; we must save the context (FACTS area), process the new segment and
    ;; return to the state we are currently in, and reexecute the =execute
    ;; method
    ;; fake a subdialog entry with a :return tag
    ;; <success> <failure> <cur-state-context> <goal> <sub-dialog-header>
    (push (list :return :return 
                (symbol-value conversation) ; save the whole conversation object
                (HAS-MOSS-GOAL conversation)
                (HAS-MOSS-SUB-DIALOG-HEADER conversation))
          (HAS-MOSS-FRAME-LIST conversation))
    ;; reset sub-dialog header
    (setf (HAS-MOSS-SUB-DIALOG-HEADER conversation) nil)
    ;; call the crawler recursively
    ;; however, we are going to enter the process state of the main current
    ;; conversation after the initial message, directly at the processing
    ;; state. We assume that the top-level is valid for the interrupting
    ;; conversation since it is a feature of the application.
    (setq next-state (car (HAS-MOSS-PROCESS-STATE
                           (car (HAS-MOSS-DIALOG-HEADER conversation)))))
    ;(dformat :dia 0 "~S/next state: ~S~%" count next-state)
    
    ;;=== here we should set FACTS properly to allow the dialog to proceed
    ;; get raw-input from the current FACTS
    (setq raw-input (moss::read-fact conversation :raw-input))
    ;; we must also save the reference to the message to be processed
    (setq todo-message (moss::read-fact conversation :todo-message))
    ;; clean current FACTS
    (moss::clear-all-facts conversation)
    ;; initialize :input with second part of raw-input
    (moss::replace-fact conversation :input (cdr raw-input))
    ;; add message to process
    (moss::replace-fact conversation :todo-message todo-message)
    
    ;; the only thing to keep is the salient feature stack!
    ;(break "...recursive call; state: ~S" next-state)
    ;(trformat "dialog-state-crawler ~S/calling function recursively ~
    ;           frame-list: ~% ~S~2%" count (HAS-FRAME-LIST conversation)) 
    
    (assistant-dialog-crawler-v2 agent conversation :state next-state)
    ;; on return, the conversation is restarted where it was left
    ;; when returning from a conversation we return to top-level 
    ;(trformat "dialog-state-crawler ~S/returning from incident ~
    ;           conversation; frame-list:~% ~S~2%" count 
    ;          (HAS-MOSS-FRAME-LIST conversation))
    
    ;; pop the frame-list stack
    (setq frame (pop (HAS-MOSS-FRAME-LIST conversation)))
    ;; reset conversation object as it was before interruption
    (set conversation (nth 2 frame))
    
    (drformat "~%;========== ~S ~S/returning from interrupting conversation...~2%"
              (key agent) count)
    
    ;(break "dialog-state-crawler-interrupt")
    
    ;; FRAME-LIST will be poped when the answer of a =resume method will be :success
    ;; or :failure(defUn mw-ae-save-on-click (dialog widget)
    
    t)
  )

;;;--------------------------------------------- ASSISTANT-DISCARD-REQUEST-MESSAGE

(defMethod assistant-discard-request-message? ((agent agent) message)
  "check if there are some good reasons for discarding an incoming request.
Arguments:
   agent: self
   message: incoming request."
  (declare (ignore message))
  ;; currently we do not discard any message
  nil)

#|
#+(or ALLEGRO-V6.1 MICROSOFT-32)#-MCL
(defun to-do-agent (agent)
  (not (eql (input-messages agent) nil))
  )
|#
;;;--------------------------------------------------- ASSISTANT-INITIALIZE-DIALOG

(defMethod assistant-initialize-dialog ((agent agent))
  "sets up the conversation channel.
   - locate a MOSS conversation object
   - initialize input and output pane
   - insert the conversation start state
   - link to agent"
  (let ((conversation (moss::m-definstance moss::q-conversation)))
    ;; link conversation to conversation state slot
    (setf (conversation-state agent) conversation)
    ;; get channel
    (setf (moss::has-input-window conversation)
      (view-named :input (com-window agent)))
    (setf (moss::has-output-window agent)
      (view-named :output (com-window agent)))
    (setf (agent conversation) agent)
    ;(setf (moss::has-initial-state conversation) *pa-dialog-header*)
    ;(setf (moss::has-state conversation) *pa-dialog-header*)
    :done))

; (assistant-initialize-dialog albert::albert)
;;;-------------------------------------------------- ASSISTANT-MAKE-TRANSITION-V2

(defmethod assistant-make-transition-v2 
    ((agent agent) conversation old-state state)
  "makes a transition to the new state. When the new state is a sub-conversation ~
   header, then sets up the new conversation and return the input state of the ~
   sub-conversation.
Arguments:
   conversation: current conversation
   old-state: current-state
   state: new state to which me make the transition
Return:
   the id of the new state"
  ;; check state for the transition
  (unless (moss::%type? state 'moss::$QSTE)
    (error "bad state parameter; ~S in =set-transition from ~S" state old-state))
  (send conversation '=replace 'HAS-MOSS-STATE (list state))
  ;;return the id of the state
  state)

;;;------------------------------------ ASSISTANT-MAKE-TRANSITION-TO-SUB-DIALOG-V2

(defmethod assistant-make-transition-to-sub-dialog-v2
    ((agent agent) conversation current-state dialog-header
     &key (success :success) (failure :failure))
  "makes a transition to the initial input state of the sub-conversation. ~
   Calls make-transition to do the transition.
Arguments:
   conversation: current conversation
   current-state: current-state
   dialog-header: header of the sub-conversation
   success: a state to go to if return is successful (or :success)
   failure: a state to go to if sub-dialog was a failure (or :failure)
Return:
   the id of the new state"
  (let (state)
    ;; check the validity of the dialog header
    (unless (moss::%type? dialog-header 'moss::$QHDE)
      (error "bad sub-conversation dialog header: ~S in state: ~S" 
        dialog-header current-state))
    ;; OK. push an entry frame onto the frame-list
    ;;   (<suc> <fail> <q-context> <goals> <sub-dialog-header>)
    ;; we assume that the success and failure state arguments are there
    (push (list success failure 
                :no-context ; used to record context object
                (HAS-MOSS-GOAL conversation)
                (HAS-MOSS-SUB-DIALOG-HEADER conversation))
          (HAS-MOSS-FRAME-LIST conversation))
    ;; record current sub-conversation
    (send conversation '=replace 'HAS-MOSS-SUB-DIALOG-HEADER (list dialog-header))
    ;; if we have an action, then set up the corresponding goal
    (if (HAS-MOSS-ACTION dialog-header)
        (send conversation '=replace 'HAS-MOSS-GOAL 
              (list (send (car (HAS-MOSS-ACTION dialog-header)) '=create-pattern))))
    ;; replace conversation header by entry state
    (setq state (car (HAS-MOSS-ENTRY-STATE dialog-header)))
    ;(break)
    ;; do the actual transition
    (assistant-make-transition-v2 agent conversation current-state state)
    ))

;;;--------------------------------------------------------- ASSISTANT-PICK-ACTION
;;; called by assistant-start-action
;;; in fact it is called only once when starting the conversation process
;;; during the conversation the master's input is handled directly by the crawler

(defMethod assistant-pick-action ((agent agent))
  "the function is associated with the to-do bin of the agent and waits for ~
   data from the master.
When data arrive in the to-do bin, then the assistant processes them, activating ~
   the current conversation state.
Arguments:
   agent: self"
  (loop
    ;; wait if no message in the to-do bin
    (process-wait "idle: waiting for master's data" #'to-do agent)
    (agent-trace agent "Picking up what master said: ~S" (to-do agent))
    ;; removes the content of the to-do bin to transfer to conversation bin
    (replace-fact (conversation agent) :raw-input (to-do agent))
    (setf (to-do agent) nil)
    (moss::trformat "assistant-pick-action; Q-cv to-do: ~S" 
                    (read-fact (conversation agent) :raw-input))
    ;; clean assistant output window
    ;(moss::clear-assistant-output (conversation agent))
    (assistant-clear-assistant-area agent)
    ;; process data, activating current conversation state
    (moss::resume-conversation (conversation agent))
    ))

;;;---------------------------------------------- ASSISTANT-POST-MESSAGE-TO-MASTER

(defMethod assistant-post-message-to-master ((agent agent) process)
  "we received a message and want to post it to the master into its to-do bin.
Arguments:
   agent: PA
   process: task process"
  (let ((task-frame (agent-get-task-frame-from-process agent process)))
    (unless task-frame
      (error "can't recover task-frame from process ~S for agent ~S"
        process agent))
    ;; add request to list of waiting messages
    (setf (waiting-messages agent) (append (waiting-messages agent)
                                           (list (message task-frame))))
    ;; show it
    (assistant-display-tasks-to-do agent)
    ;; return
    :done))

;;;--------------------------------------------- ASSISTANT-PROCESS-WAITING-MESSAGE
;;; defined in omas-W-assistant.lisp

;;;--------------------------------------------- ASSISTANT-RESTART-CONVERSATION-V2

(defMethod assistant-restart-conversation-v2 
    ((agent agent) conversation-id &optional initial-state)
  "restarts a conversation. Uses the conversation object to restart at the ~
   beginning. Reinitializes with initial state. 
Arguments:
   conversation-id: conversation to be restarted
   initial-state (opt): state where to start conversation
Return:
   the id of the conversation object."
  (unless initial-state
    (setq initial-state (car (has-moss-entry-state 
                              (car (has-moss-dialog-header conversation-id))))))
  ;; keep input and output channels
  ;; keep DIALOG-HEADER 
  ;; reset frame-list, goal, facts
  (send 'moss::$FLT '=delete-all conversation-id)
  (send 'moss::$GLS '=delete-all conversation-id)
  (send 'moss::$FCT '=delete-all conversation-id)
  ;; copy initial state into STATE (current state)
  (send conversation-id '=replace 'HAS-MOSS-STATE (list initial-state))
  
  (drformat :dia 0 "~2%;===== ~S Restarting conversation with state: ~S" 
            (key agent) (car (has-MOSS-label initial-state)))
  ;;*** debug
  ;(moss::print-conversation conversation-id)
  ;;*** end debug
  
  ;; clean the input slot to avoid infinit looping
  (setf (to-do agent) nil
    (answer agent) nil)
  
  ;; go execute the new state with the associated context
  (assistant-run-dialog-v2 agent conversation-id initial-state)
  ;; return the id of the conversation object
  conversation-id)

;;;------------------------------------------------- ASSISTANT-RESUME-CONVERSATION
;;; only used once at the beginning of the conversation

(defMethod assistant-resume-conversation ((agent agent))
  "the function is called when some message appears in the to-do bin of the agent ~
   to activate the current conversation state.
Arguments:
   agent: agent."
  ;; process data, activating current conversation state
  ;; first move data to the data slot of the conversation
  (replace-fact (conversation agent) :raw-input (to-do agent))
  ;(setf (moss::has-data (conversation agent)) (list (to-do agent)))
  ;; should remove whatever is in the to-do bin to avoid infinite loops
  (setf (to-do agent) nil)
  #+(or ALLEGRO-V6.1 MICROSOFT-32)
  (warn "~&not implemented yet. Agent: ~A" agent)
  )

;;;------------------------------------------- ASSISTANT-RETURN-FROM-SUB-DIALOG-V2

(defmethod assistant-return-from-sub-dialog-v2 
    ((agent agent) conversation fail/success)
  "returns from a sub-dialog. Get the frame list from the conversation object ~
   pops it, reinstall the old goal if any, the corresponding state context, ~
   and return the fail/success state that can be keywords.
   When the frame list is empty resets the conversation by throwing to dialog ~
   error.
Arguments:
   conversation: current conversation
   fail/success: a keyword indicating failure or success
Return:
   a state to go to or :failure or :success"
  (let ((frame-list (HAS-MOSS-FRAME-LIST conversation))
        frame)
    ;(trformat "exec-return-from-sub-dialog /frame-list IN: ~%  ~S" 
    ;          (has-frame-list conversation))
    ;; if frame-list is empty, reset dialog
    (unless frame-list (assistant-restart-conversation-v2 agent conversation))
    ;; otherwise get frame (<fail> <success> <context> <goal> <sub-dial-hdr>)
    (setq frame (pop frame-list))
    ;; update frame-list
    (setf (HAS-MOSS-FRAME-LIST conversation) frame-list)
    ;; frame looks like (success failure state goal sub-dialog-header)
    (send conversation '=replace 'HAS-MOSS-GOAL (nth 3 frame))
    ;; reinstall previous state-context (in case we start an alternative task)
    ;; must not fo that on normal returns (loss of results)
    ;(send conversation '=replace 'HAS-Q-CONTEXT (list (nth 2 frame)))
    ;; reset previous sub-dialog header
    (send conversation '=replace 'HAS-MOSS-SUB-DIALOG-HEADER (nth 4 frame))
    ;(trformat "exec-return-from-sub-dialog /frame-list OUT: ~%  ~S" 
    ;          (has-frame-list conversation))
    ;; return ad hoc state or keyword to set up new transition
    (case fail/success
      (:success (assistant-set-state-for-subdialog-v2 agent (car frame)))
      (:failure (assistant-set-state-for-subdialog-v2 agent (cadr frame)))
      (otherwise 
       (error "bad fail/success argument when returning from sub-dialog")))
    ))


;;;------------------------------------------------------- ASSISTANT-RUN-DIALOG-V2

(defMethod assistant-run-dialog-v2 ((agent agent) conversation initial-state)
  "global control of the conversation loop. Catches the fatal conversation ~
   errors by restarting the process.
Arguments:
   conversation: a MOSS conversation object
   state: a specified initial state
Return:
   does not return."
  ;; launch dialog, restarting completely in case of error 
  ;; (the assistant gets lost)
  
  (catch :dialog-error
         (assistant-dialog-crawler-v2 agent conversation :state initial-state))
  
  ;;********** check display-text
  (send conversation '=display-text
        "***** Dialog Programming error, I restart dialog *****" :color cg:red) 
  ;; reset conversation object, and restart the loop
  (assistant-restart-conversation-v2 agent conversation initial-state)
  )


;;;---------------------------------------- ASSISTANT-SET-STATE-FOR-SUBDIALOG-V2

(defmethod assistant-set-state-for-subdialog-v2 ((agent agent) state-ref)
  "get the state to return to when returning from a subdialog. State can be ~
   a keyword (:success :failure :return) a state objects ($QSTE.nn), or ~
   the name of a state object (_q-more?). In the later case we must get ~
   its value.
Arguments:
   state-ref: reference to a virtual (keyword) or real state object
Return:
   keyword of state object, error if not a valid state ref."
  (cond
   ((member state-ref '(:success :failure :return)) state-ref)
   ((and (moss::%pdm? state-ref)(moss::%type? state-ref 'moss::$QSTE)) state-ref)
   ((and (symbolp state-ref)
         (moss::%pdm? (symbol-value state-ref))
         (moss::%type? (eval state-ref) 'moss::$QSTE))
    (symbol-value state-ref))
   (t (error "bad state reference: ~S" state-ref))))

;;;-------------------------------------------------------- ASSISTANT-START-ACTION
;;; called by start-converse-process
;;; If we have an assistant without a window, e.g. interacting through web pages,
;;; then do not need to create a window, but must start the dialog loop
;;; One problem is that MOSS expects the conversation object to have an I/O
;;; channel
;;; as it is currently OMAS will create a small window for the agent even if there
;;; is a no-window flag.

(defMethod assistant-start-action ((agent agent))
  "the function creates the assistant window and starts the conversation and ~
   then waits for master's input by watching the to-do bin of the agent.
When data arrive into the to-do bin, then the assistant processes them, activating ~
   the current conversation state.
Arguments:
   agent: self"
  ;; first establich process global variables
  (let ((*package* (ontology-package agent))
        (moss::*language* (language agent))
        (moss::*context* (moss-context agent))
        (moss::*version-graph* (moss-version-graph agent))
        moss::*answer*
        win)
    (declare (special *package* moss::*language* moss::*context* 
                      moss::*version-graph* moss::*answer* *omas*))
    ;; create interface window, special call if private
    (unless (no-window agent)
      (setq win 
            (cond
             ;; if private, call private function
             ((private-interface agent)
              (funcall (private-interface agent) agent))
             ;; if default check for small window
             ((eql (omas-window-type *omas*) :small)
              (make-pa-small-window agent))
             ;; otherwise make regular windoe-w
             (t
              (make-master-window agent)))))
    ;; record window name in agent
    (setf (com-window agent) win)
    ;; start conversation, posting welcome message
    (assistant-start-conversation agent)
    ;; wait then for master's input. No way of getting out of the loop...
    ;;******** this function is never called since start-conversation calls the
    ;; crawler loop
    (assistant-pick-action agent))
  )


;;;-------------------------------------------------- ASSISTANT-START-CONVERSATION

(defMethod assistant-start-conversation ((agent agent) &rest ll)
  "the function is called to start a conversation using the input node of the ~
   conversation graph.
Arguments:
   agent: agent."
  (declare (ignore ll)(special *PA-dialog-header*))
  ;; process data, activating current conversation state
  (progn
    ;; wait until window is created...
    (sleep 1)
    (if (member :converse-v2 *features*)
        (assistant-start-conversation-v2 agent (dialog-header agent))
      ;; old stye interaction
      (if (dialog-header agent)
          (moss::start-conversation 
           (dialog-header agent)  :agent agent   ; 
           ;:input (cg:find-component :master (com-window agent))
           :input (com-window agent)
           ;:output (cg:find-component :assistant (com-window agent))))
           :output (com-window agent))))
    (warn "agent  ~S has no dialog header. Can't start conversation" agent))
  ;; creates a conversation object that will allow us to know at which point in
  ;; the conversation graph we are, and allow us to bridge agent and MOSS 
  ;; environments
  ;; fills the conversation object with channel info, who info, initial state
  ;; from the assistant dialog-header
  ;; creates an initial empty state context; link it to conversation
  ;; set up a pointer to the conversation object in the conversation slot of
  ;; the agent.
  ;; starts the dialog by sending the =execute message to the conversation
  
  ;; however, when defassistant is launched the agent files are not yet entirely 
  ;; loaded, thus we allow a delay before executing the function
  
  )

;;;--------------------------------------------- ASSISTANT-SEND-FREE-STYLE-MESSAGE

(defMethod assistant-send-free-style-message ((agent agent) to data)
  "the function creates a new task, sending a free style message, e.g. when in ~
   the middle of a concersation to obtain some result. Saves the message in the ~
   pending-requests bin of the agent and update the com-window.
Arguments:
   agent: agent
   to: another agent or :all
   data: a list of data
Return:
   nothing of interest."
  ;; construct a task-id and send a message free-style with the
  ;; content of HAS-DATA attribute of context
  (let ((task-id (create-task-id))   
        message)
    ;; ********** here we should create a task structure... 
    ;; that will recover answer when it comes back
    (setq message
          (message-make :date (get-universal-time) :type :request 
                        :from agent :to to
                        :action nil :task-id task-id
                        :args  (list data) :protocol :free-style))
    ;; save it into the pending-task-id slot of the master bins
    (%agent-add agent message :pending-requests)
    (display-assistant-window agent)
    ;; *********** this is probably wrong
    ;; save task-id for allowing assistant to check the answer
    (setf (pending-task-id agent) task-id)
    (send-message message))
  )

;;;-------------------------------------------- ASSISTANT-WAIT-FOR-MASTER-DECISION

(defMethod assistant-wait-for-master-decision ((agent agent) task-frame)
  "we posted job to do to the master, and are waiting for his decision. He can ~
   discard the message, or decide to process it. The answer is found in the ~
   master-decision slot of the task-frame (?)
Arguments:
   agent: self
   task-frame: ?"
  ;(declare (ignore agent))
  (unless task-frame
    (error "task-frame argument should not be NIL"))
  ;; wait on master's decision
  (process-wait "Waiting for subtasks answers..."
                #'(lambda (xx) (master-decision xx))
                task-frame)
  ;; return decision to the user
  (prog1 (master-decision task-frame)
    (setf (master-decision task-frame) nil))
  )

;;;-------------------------------------------- ASSISTANT-WAIT-FOR-MASTER-DECISION

;;;===============================================================================
;;;
;;;                         SALIENT FEATURES
;;;
;;;===============================================================================

#|
Salient Features
================
Salient features must be associated with the PA, since they are used for 
disambiguating pronoun references among other things. The salient features queue
is kept in the salient-features slot of a PA.
 
Structure
---------
The SF structure is a queue organized in stack frames. Each frame corresponds to a 
micro context active during a specific action (e.g. create a group of post it). 
Stack frame in French could be translated as a BES (bloc d'éléments significatifs).
Each stack frame records references to salient objects ranging from the last
person that was mentioned to the current action. A frame (corresponding to a
micro context) is "closed" as soon as the task is completed. One must
however realize that asynchronous information can be sent by other agents, like
the fact that some specific object has been selected or deselected on the table.
Thus, the PA must have a special skill to take such events into account.
The simplest possible structure of a stack frame is a set of properties like:
   - (:micro-context <time> <dialog reference>) 
   - ("personne" ("prénom" "Claude")) reference to a specific person
   - ("post it" ("ID" "P-23") :selected) reference to a selected post it
   - more generally:
     (<ontological concept> (<property> <value>))
   - (:end-micro-context <time><success/failure>)
In order to resolve references one probably requires to do a local grammatical
analysis including semantic patterns like who or what can be a subject or an
object.

Usage
-----
We view three types of situations:
   - when entering a sub-dialog, we create a :micro-context entry
   - during the course of the dialog, we add (or remove ?) entries according to
     the exchanged information, and we access the salient feature queue to extract
	 relevant information.
   - when leaving the sub-dialog (top-level sub-dialog, we add an :end-micro-context
     entry
In addition we keep the queue to a reasonable length by removing old stack frames,
based on the length of the queue or on the date. This may not be necessary since 
the queue is erased after each session when the PA is disabled.

Implementation
--------------
The implementation should include basic mechanisms (API-like):
   - SF-checks          ; used to check an entry tag
   - SF-clean           ; removes all entries that satisfy the "checks" test
   - SF-delete          ; removes the last entry (pop) ?
   - SF-get             ; retrieves an entry from the queue
   - SF-put             ; adds an item to the queue 
   - SF-reset           ; set the SF queue to NIL
   - ST-trim            ; remove the last elements of the queue either because the
                        ; queue became too long or the entries are too old
SF: salient features

We need 2 additional entries to the PA structure
   - salient-features-max-length
   - salient-features-max-time

Test
----
Typical dialog
   - créer un nouveau post it
   > post it créé P-23
   - ajouter un texte
   > vous voulez rajouter un texte au post it P-23 ?
   - oui, Cahier des charges.
   > C'est fait
or
   - supprimer un post it
   > sélectionner un post it.
   <a post it is selected graphically and is highlighted>
   > celui-là ?
   - oui.
   > le post it P-23 a été supprimé.
or
   <a post it is selected>
   - supprimer le post it.
   <the selected post it is highlighted>
   > celui-là ?
   - non.
   > Merci de sélectionner un autre post it.
   <after 10 seconds, if nothing is selected>
   > la suppression du post it est abandonnée.
   
NOTE: on interaction we must limit the waiting time of the assistant. This is done
by putting a value into (omas::answer-timeout agent) e.g. 10 seconds
|#

;;;---------------------------------------------------------------------- SF-CHECKS

(defun sf-checks (item sf-tag)
  "if item is a list, returns items that belong to the list"
  (if (listp item)
      (notevery #'null (mapcar #'(lambda (xx) (sf-checks-item xx sf-tag)) item))
    (sf-checks-item item sf-tag)))

;;;----------------------------------------------------------------- SF-CHECKS-ITEM

(defun sf-checks-item (item sf-tag)
  "checks if sf-tag corresponds to what we look for. Currently if is is not a ~
   string then checks for equality, if it is a string and ontology concept, ~
   checks whether sf-tag is a subconcept.
   Could be anything else.
Arguments:
   item: what we are looking for
   sf-tag: tag (first item of an SF entry)
Return:
   t or NIL"
  (let (item-id tag-id)
    (cond
     ;; first deal with non strings
     ((not (stringp item)) (equal item sf-tag))
     ;; if a string and ontology concept, check for subconcepts
     ((and (stringp item)
           (stringp sf-tag)
           (moss::%is-concept? (setq item-id (moss::%%get-id item :concept)))
           (moss::%is-concept? (setq tag-id (moss::%%get-id sf-tag :concept))))
      (moss::%subtype? item-id tag-id))
     ;; if both strings but no concepts, check equality
     ((and (stringp item)(stringp sf-tag))
      (string-equal item sf-tag))
     )))

#|
(setq sf 
      '((:END-MICRO-CONTEXT 3533305223) 
        ("postit" ("id" "P-3"))
        ("texte" "nouveaux tests")
        (:MICRO-CONTEXT 3533305212 "create postit") 
        (:END-MICRO-CONTEXT 3533304655) 
        ("postit" ("id" "P-2"))
        (:MICRO-CONTEXT 3533304655 "create postit") 
        (:END-MICRO-CONTEXT 3533304595) 
        ("postit" ("id" "P-1"))
        (:MICRO-CONTEXT 3533304595 "create postit") 
        (:END-MICRO-CONTEXT 3533304404) 
        ("postit" ("id" "P-0"))
        (:MICRO-CONTEXT 3533304404 "create postit")))
(sf-checks "postit" (caadr sf))
T
(sf-checks "postit" (caar sf))
NIL
(sf-checks :end-micro-context (caar sf))
T
|#
;;;----------------------------------------------------------------------- SF-CLEAN

(defun sf-clean (agent item)
  "clean the salient features list. Not implemented yet"
  (declare (ignore agent item))
  (error "not implemented yet"))

;;;---------------------------------------------------------------------- SF-DELETE

(defun sf-delete (agent item &key depth all)
  "removes the last (?) entry from the salient features list."
  (declare (ignore agent item depth all))
  (error "not implemented yet"))


;;;------------------------------------------------------------------------- SF-GET
;;; should allow searching for previous entries at a given depth

(defun sf-get (agent item &key (depth 2) (rank 1) collect recent)
  "looks for something on the salient features list.
Arguments:
   agent: PA
   item: string naming a concept of the ontology, or list of strings
   depth (key): number of micro-contexts to be searched (default is 2)
   rank (key): order of the elements (1 -default- is last, 2 before last, etc.)
               when collecting, max number of items collected
   collect (key): if t we collect all encountered object that check
   recent (key): if t we want to stop looking before the last task (default: nil)
Return:
   a list of SF entries the first element of which is a subconcept of item, or nil."
  (let ((sf (salient-features agent))
        (depth-count depth)
        (ranking rank)
        collection)
    ;; loop until we reach past the last micro-context or the list becomes empty
    (loop
      (if (null sf)
          ;; we quit, failure
          (return-from sf-get (reverse collection)))
      
      ;; if recent is true then we want to stop before the end of the last task
      (when (and recent (eql (caar sf) :end-micro-context))
        (return-from sf-get (reverse collection)))
      
      ;; if we hit micro-context decrease depth-count
      (when (eql (caar sf) :micro-context)
        ;; remove boundary marker (micro-context entry)
        ;(pop sf)
        ;; decrease count
        (decf depth-count)
        ;; if null get out
        (if (< depth-count 1)
            (return-from sf-get (reverse collection)))
        )
      
      ;; otherwise check entry
      (if (sf-checks item (caar sf))
          ;; if checks, return success if rank is 1
          (if (<= ranking 1)
              (return-from sf-get (reverse (push (car sf) collection)))
            ;; otherwise decrease rank
            (progn
              (decf ranking)
              ;; if we collect save entry
              (if collect
                  (push (car sf) collection)))))
      
      ;; otherwise continue checking
      (pop sf))))
#|
(setq sf 
      '((:END-MICRO-CONTEXT 3533305223) 
        ("postit" ("id" "P-3")) 
        ("texte" "nouveaux tests")
        (:MICRO-CONTEXT 3533305212 "create postit") 
        (:END-MICRO-CONTEXT 3533304655) 
        ("postit" ("id" "P-2"))
        (:MICRO-CONTEXT 3533304655 "create postit")
        (:END-MICRO-CONTEXT 3533304595) 
        ("postit" ("id" "P-1"))
        (:MICRO-CONTEXT 3533304595 "create postit") 
        (:END-MICRO-CONTEXT 3533304404)
        ("postit" ("id" "P-0"))
        (:MICRO-CONTEXT 3533304404 "create postit")))

;; look for the last entry in the scope of the 2 last micro-contexts (default)
(sf-get PA_jean-paul "postit")
(("postit" ("id" "P-3")))

;; last entry research is done over 3 micro-contexts
(sf-get PA_jean-paul "postit" :depth 3)
(("postit" ("id" "P-3")))

;; we get before last entry
(sf-get PA_jean-paul "postit" :rank 2)
(("postit" ("id" "P-2")))

(sf-get PA_jean-paul "postit" :rank 3)
NIL

;; no third entry when limiting to the first micro-context
(sf-get PA_jean-paul "postit" :rank 3 :depth 1)
NIL

;; get the entries from the last 2 micro-context
(sf-get PA_jean-paul-PA "postit" :rank 3 :depth 2 :collect t)
(("postit" ("id" "P-3")) ("postit" ("id" "P-2")))

;; get the entry from the last micro-context
(sf-get PA_jean-paul "postit" :rank 3 :depth 1 :collect t)
(("postit" ("id" "P-3")))
|#
;;;------------------------------------------------------------------------- SF-PUT
				
(defun sf-put (agent item)
  "adds an item to the salient features queue, keeping the queue under the max ~
   length limit."
  (let ((max-length (salient-features-max-length agent))
        (sfq (salient-features agent)))
    ;; add new entry
    (push item sfq)
    ;; update queue trimming it if necessary
    (setf (salient-features agent) 
      (if (<= (length sfq) max-length)
          sfq
        (subseq sfq 0 max-length)))
    ))
#|
(setq sf 
      '(("postit" ("id" "P-3"))
        ("texte" "nouveaux tests")
        (:MICRO-CONTEXT 3533305212 "create postit") 
        (:END-MICRO-CONTEXT 3533304655) 
        ("postit" ("id" "P-2"))
        (:MICRO-CONTEXT 3533304655 "create postit") 
        (:END-MICRO-CONTEXT 3533304595) 
        ("postit" ("id" "P-1"))
        (:MICRO-CONTEXT 3533304595 "create postit") 
        (:END-MICRO-CONTEXT 3533304404) 
        ("postit" ("id" "P-0"))
        (:MICRO-CONTEXT 3533304404 "create postit")))
(setf (salient-features jp::PA_jean-paul) sf)
(sf-put jp::PA-jean-paul '(:END-MICRO-CONTEXT 3533305223))
|#
;;;----------------------------------------------------------------------- SF-RESET
  
(defun sf-reset (agent)
  "sets the salient features queue to nil."
  (setf (salient-features agent) nil))

;;;------------------------------------------------------------------------ SF-TRIM

(defun sf-trim (agent)
  "trims the SF queue if needed (too long or some entries too old)."
  (error "not implemented yet"))


;;;===============================================================================
(format t "~&;*** OMAS v~A - assistant loaded ***" *omas-version-number*)

;;; :EOF
