﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;14/03/07
;;;               O M A S - W I N D O W - M A C R O S  (file W-macros.lisp)
;;;
;;;===============================================================================
#|
This approach is obsolete after the disappearance of MCL.
The idea was to define a single set of functions that could create and handle MCL 
or ACL windows.
|#

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
The following code is used to create ACL or MCL windows

(make-window 'OMAS-agent :owner owner
            :class 'omas-agent
            :exterior (AW-compute-exterior position )
            :border border
            :childp t
            :close-button nil
            :cursor-name :arrow-cursor
            :font (make-font-ex :swiss "MS Sans Serif / ANSI" 11 nil)
            :form-package-name :common-graphics-user
            :BACKGROUND-COLOR (MAKE-RGB :RED 255 :GREEN 128 :BLUE 128)
            :form-state :normal
            :maximize-button nil
            :minimize-button nil
            :name 'form-message
            :pop-up nil
            :resizable nil
            :scrollbars nil
            :state :normal
            :status-bar nil
            :system-menu t
            ;:title (name agent)
            :title-bar nil
            :toolbar nil
            :dialog-items (make-agent-widgets agent)
            :path #p"D:\\OMAS-v5.0.12a-PC\\5-omas-agentwin-P.lisp"
            :help-string nil
            :form-p form-p
            :form-package-name :common-graphics-user)

(make-instance 
          'OMAS-agent
          :WINDOW-TYPE :single-edge-box 
          :WINDOW-TITLE (format nil "~S" (name agent))
          :VIEW-POSITION `,(make-point (- *screen-left-offset* 4)
                                       (+ 158 (* (1- position) 114)))
          :VIEW-SIZE #@(540 110)
          :COLOR-P t
          ;:VIEW-FONT '("Chicago" 12 :SRCOR :PLAIN)
          :VIEW-SUBVIEWS (make-agent-window-widgets agent)
          )

(cr-window 'OMAS-agent
           :type :document

ACL/MCL Window Options are:
---------------------------

window class (OMAS-agent)
:class :window-type
:owner :view-container
:exterior :view-size/:view-position/:view-origin?
:border -
:childp -
:close-button / :close-box-p
:cursor-name :arrow-cursor
:font :view-font
:form-package-name :common-graphics-user
:BACKGROUND-COLOR :back-color
:form-state -
:maximize-button :grow-icon-p
:minimize-button -
:name :view-nick-name
:pop-up -
:resizable -
:scrollbars - :h-scrollp / :v-scrollp (scrolling-fred-window)
:state -
:status-bar -
:system-menu -
:title :window-title
:title-bar -
:toolbar -
:dialog-items :view-subviews
:path -
:help-string -
:form-p -
:form-package-name -
- :color-p
- :color-list
- :wptr
- :window-do-first-click
- :window-active-p
- :view-scroll-position
- :direction
- :window-active-p
- :auto-position


SCROLLING-FRED-VIEW (SFV)
SIMPLE-VIEW (SV)
DIALOG-ITEM (DI

:ALLOW-RETURNS         SFV  
:ALLOW-TABS            SFV
:AUTO-POSITION               W
:BACK-COLOR                  W
:BAR-DRAGGER           SFV
:BUFFER                SFV
:BUFFER-CHUNK-SIZE     SFV
:CLOSE-BOX-P                 W
:COLOR-P                     W
:COMTAB                SFV
:CONTENT-COLOR               W
:COPY-STYLES-P         SFV
:DIALOG-ITEM-ACTION                       DI
:DIALOG-ITEM-ENABLED-P                    DI
:DIALOG-ITEM-HANDLE                       DI
:DIALOG-ITEM-TEXT      SFV                DI
:DIRECTION             SFV   W   V   SV   DI
:ERASE-ANONYMOUS-INVALIDATIONS W
:FILENAME              SFV
:GROW-ICON-P                 W
:H-SCROLLP             SFV
:H-SCROLL-CLASS        SFV
:H-PANE-SPLITTER       SFV       
:HELP-SPEC             SFV   W   V   SV   DI
:HISTORY-LENGTH        SFV
:IGNORE                SFV   W   V   SV   DI
:JUSTIFICATION         SFV
:LINE-RIGHT-P          SFV
:MARGIN                SFV
:PART-COLOR-LIST       SFV                DI
:PROCESS                     W
:PROCID                      W
:SAVE-BUFFER-P         SFV
:TEXT-EDIT-SEL-P       SFV
:THEME-BACKGROUND            W
:TRACK-THUMB-P         SFV
:V-SCROLL-CLASS        SFV
:V-SCROLLP             SFV
:V-PANE-SPLITTER       SFV
:VIEW-FONT             SFV
:VIEW-CONTAINER        SFV
:FRED-ITEM-CLASS       SFV
:DRAW-SCROLLER-OUTLINE SFV
:H-SCROLL-FRACTION     SFV
:GROW-BOX-P            SFV
:VIEW-CONTAINER              W   V   SV   DI
:VIEW-FONT                   W   V   SV   DI
:VIEW-NICK-NAME        SFV   W   V   SV   DI
:VIEW-POSITION         SFV   W   V   SV   DI
:VIEW-SCROLL-POSITION  SFV   W   V
:VIEW-SIZE             SFV   W   V   SV   DI
:VIEW-SUBVIEWS         SFV   W   V
:WINDOW-DO-FIRST-CLICK       W
:WINDOW-LAYER                W
:WINDOW-SHOW                 W
:WINDOW-TITLE                W
:WINDOW-TYPE                 W
:WORD-WRAP-P           SFV
:WPTR                  SFV   W   V   SV   DI
:WRAP-P                SFV

Common Windows Options
----------------------
Type
   :type 
      :standard
      :document
      :menu
      :pop-up
Title
   :title
Position and size
   :top :left :bottom :right (screen coordinates)
Name
   :name
Scrollbbars
   :scrollbars
State
   :state
Background-color
   :background-color
Font
   :font
Owner
   :owner
Package
   :package
Subviews, dialo-items
   :subviews vs :dialog-items

|#

(in-package :omas)
;;;===============================================================================
;;;                               COLORS
;;;===============================================================================

;;; colors. When in doubt use (ask-user-for-color) ACL or (user-pick-color) MCL
#-MCL (defParameter *black-color* cg:black)
#-MCL (defParameter *blue-color* cg:blue)
#-MCL (defParameter *green-color* cg:green)
#-MCL (defParameter *red-color* cg:red)
#-MCL (defParameter *light-blue-color* (cg:make-rgb :RED 66 :GREEN 160 :BLUE 255))
#-MCL (defParameter *orange-color* (cg:make-rgb :RED 255 :GREEN 128 :BLUE 0))
#-MCL (defParameter *tan-color* (cg:make-rgb :RED 128 :GREEN 64 :BLUE 0))
#-MCL (defParameter *white-color* cg:white)
;; not really yellow that one cannot read
(defParameter *yellowish-color*  #+MCL 15781437 #-MCL cg:yellow)
(defParameter *grey-color* #+MCL 9866133 
  #-MCL (cg:make-rgb :RED 128 :GREEN 128 :BLUE 128))
(defParameter *light-green-color* #+MCL 6748510
  #-MCL (cg:make-rgb :RED 0 :GREEN 255 :BLUE 64))
(defParameter *light-grey-color* #+MCL *grey-color*
  #-MCL (cg:make-rgb :RED 228 :GREEN 228 :BLUE 228))

;;;===============================================================================
;;;
;;;                          COMPATIBILITY MACROS
;;;
;;;===============================================================================

;;;--------------------------------------------------------------------- CR-WINDOW

(defMacro cr-window (&key class type title top left bottom right height width
                          (close-button t) maximize-button (minimize-button t)
                          name scrollbars state background-color font
                          scroller-class track-thumb-p resizable
                          (title-bar t)
                          package owner subviews dialog-items)
  "produced MCL or ACL make-window function according to the feature list.
Argument:
   background-color (key): depends on system
   bottom (key): bottom
   class: window-class, a user defined window class
   close-button (key): t, the default close button (default t)
   font (key): depends on system
   left (key): left
   maximize-button (key): t, the button for expanding the window (default nil)
   minimize-button (key): t, the button makes window into an icon (default t)
   name (key): name, ususlly a keyword, e.g. :omas-window
   owner (key): the owner of the window (another window or the screen)
   package (key): package
   right (key): right
   scrollbars (key): :vertical, :horizontal, :both
   scroller-class (key): MCL, class of the scrolling area
   state (key): :normal, shrunk
   subviews (key): list of widgets
   dialog-items (key): same as subviews
   title (key): window title
   title-bar (key): t, specifies a title bar area (default t)
   top (key): top
   track-thumb-p (key): MCL, needed for scrolling
   type (key): :standard, :document, :popup, :menu (default :document, MCL)
Return:
   a window object."
  #+MICROSOFT-32
  (declare (ignore type scroller-class track-thumb-p))
  #+MICROSOFT-32
  `(cg:make-window 
    ,(or name ''cg:frame-window)
    :owner ,(or owner '(cg:screen cg:*system*))
    :class ,class
    :exterior (cg:make-box ,left ,top (or ,right (+ ,left ,width)) 
                           (or ,bottom (+ ,top ,height)))
    :border :frame
    :childp nil
    :close-button ,close-button
    :cursor-name :arrow-cursor
    :font ,(or font (cg:make-font-ex :swiss "MS Sans Serif / ANSI" 11 nil))
    :form-package-name :common-graphics-user
    :background-color ,(or background-color
                           (cg:make-rgb :red 255 :green 128 :blue 128))
    :form-state ,state
     :maximize-button ,maximize-button
    :minimize-button ,minimize-button
    ,@(if name `(:name ,name))
    :pop-up nil
    :resizable ,resizable
    :scrollbars ,scrollbars
    :state ,(or state :normal)
    :status-bar nil
    :system-menu t
    :title ,(or title "")
    :title-bar ,title-bar
    :toolbar nil
    :dialog-items ,(or dialog-items subviews)
    :path nil
    :help-string nil
    :form-p nil
    :form-package-name (or ,package *package*))
  #+MCL
  (declare (ignore owner package scrollbars state close-button
                   maximize-button title-bar))
  #+MCL
  `(let* ((width (or ,width (if (and ,left ,right) (- ,right ,left) 200)))
          (height (or ,height (if (and ,top ,bottom) (- ,bottom ,top) 100)))
          (win
           (make-instance ,(or class ''dialog)
             ,@(if type `(:window-type ,type))
             :window-title ,(or title "")
             :view-position (make-point (or ,left 100) 
                                        (or ,top 100))
             :view-size (make-point width height)
             :color-p t
             ,@(if name `(:view-nick-name ,name))
             ,@(if font `(:view-font ,font))
             :view-subviews ,(or subviews dialog-items)
             ,@(if scroller-class `(:scroller-class ,scroller-class))
             ,@(if track-thumb-p `(:track-thumb-p ,track-thumb-p))
             ,@(if minimize-button `(:grow-icon-p ,minimize-button)
                   `(:grow-icon-p t))
             )))
     ,@(if background-color `((set-back-color win ,background-color)))
     win)
  )

#|
(cr-window :class 'OMAS-agent
           :type :single-edge-box
           :title (format nil "~S" (name agent))
           :left 4
           :top (+ 158 (* (1- position) 114))
           :right (+ 4 540)
           :bottom (+ (+ 158 (* (1- position) 114)) 110)
           :background-color (omas::\%rgb 64764 48059 51143) ; pink
           :dialog-items (make-agent-window-widgets agent))
;;; expands to (MCL)
(LET* ((WIDTH (OR NIL (IF (AND 4 (+ 4 540)) (- (+ 4 540) 4) 200)))
       (HEIGHT
        (OR NIL
            (IF (AND (+ 158 (* (1- POSITION) 114))
                     (+ (+ 158 (* (1- POSITION) 114)) 110))
                (- (+ (+ 158 (* (1- POSITION) 114)) 110)
                   (+ 158 (* (1- POSITION) 114)))
                100)))
       (WIN
        (MAKE-INSTANCE
          'OMAS-AGENT
          :WINDOW-TYPE
          :SINGLE-EDGE-BOX
          :WINDOW-TITLE
          (FORMAT NIL "~S" (NAME AGENT))
          :VIEW-POSITION
          (MAKE-POINT (OR 4 100) (OR (+ 158 (* (1- POSITION) 114)) 100))
          :VIEW-SIZE
          (MAKE-POINT WIDTH HEIGHT)
          :COLOR-P
          T
          :VIEW-SUBVIEWS
          (MAKE-AGENT-WINDOW-WIDGETS AGENT)
          :GROW-ICON-P
          T)))
  (SET-BACK-COLOR WIN (%RGB 64764 48059 51143))
  WIN)
;;; axpands to (ACL)
(CG.BASE:MAKE-WINDOW 'CG.BASE:FRAME-WINDOW :OWNER
                     (CG.BASE:SCREEN CG.BASE:*SYSTEM*)
  :CLASS 'OMAS-AGENT
  :EXTERIOR (CG.BASE:MAKE-BOX 4 (+ 158 (* (1- POSITION) 114))
                              (COND ((+ 4 540)) (T (+ 4 NIL)))
                              (COND
                               ((+
                                 (+ 158 (* (1- POSITION) 114))
                                 110))
                               (T
                                (+
                                 (+ 158 (* (1- POSITION) 114))
                                 NIL))))
  :BORDER :FRAME
  :CHILDP NIL
  :CLOSE-BUTTON T
  :CURSOR-NAME :ARROW-CURSOR
  :FONT #.(CG.BASE:MAKE-FONT-EX :SWISS "MS Sans Serif / ANSI" 11)
  :FORM-PACKAGE-NAME :COMMON-GRAPHICS-USER
  :BACKGROUND-COLOR (%RGB 64764 48059 51143)
  :FORM-STATE NIL
  :MAXIMIZE-BUTTON NIL
  :MINIMIZE-BUTTON T
  :POP-UP NIL
  :RESIZABLE NIL
  :SCROLLBARS NIL
  :STATE :NORMAL
  :STATUS-BAR NIL
  :SYSTEM-MENU T
  :TITLE (FORMAT NIL "~S" (NAME AGENT))
  :TITLE-BAR T
  :TOOLBAR NIL
  :DIALOG-ITEMS (MAKE-AGENT-WINDOW-WIDGETS AGENT)
  :PATH NIL
  :HELP-STRING NIL
  :FORM-P NIL
  :FORM-PACKAGE-NAME (COND (NIL) (T *PACKAGE*)))
|#
;;;===============================================================================
;;;
;;;                                  WIDGETS
;;;
;;;===============================================================================

;;; Supported WIDGETS are:
;;;   - check-box
;;;   - button
;;;   - static-text
;;;   - one-line-editable-text
;;;   - editable-text
;;;   - multi-item-list
;;;
#|
;; close button, agent name is associated with this button
(make-instance 'check-box :font (make-font-ex nil "Lucida Sans Unicode / ANSI" 11 nil)
           :left 5 :height 16 :name 'agent-close :top 3 :title (omas::name agent) 
           :on-click 'AW-close-on-click)

;;;=== CLOSE
(make-instance 'check-box-dialog-item
     :view-position #@(5 6)
     :view-size #@(15 15)
     :dialog-item-action 'aw-close-on-click)  
(make-instance 'static-text-dialog-item
     :view-position #@(25 6)
     :view-size #@(510 15)
     :view-font '("Chicago" 12 :SRCOR :PLAIN)
     :dialog-item-text (format nil "~S" (name agent)))

(cr-check-box :top 6 :left 5 :height 15 :on-click 'aw-close-on-click :title "exit")
|#
;;;------------------------------------------------------------------------ BUTTON
#|
;; inspect button (example)
(make-instance 'button :font (make-font-ex nil "Lucida Sans Unicode / ANSI" 11 nil)
               :height 16 :left 5 :name 'inspect-button :width 50 :on-click
               'AW-inspect-on-click :title "Inspect" :top 20)
;;=== INSPECT
(make-instance 'button-dialog-item
  :view-position #@(465 2)
  :view-size #@(70 15)
  :dialog-item-text "inspect"
  :dialog-item-action 'aw-inspect-on-click
  :view-nick-name :inspect-button)
(cr-check-button 'button-dialog-item :top 20 :left 5 :width 50 :height 16 
                 :name :inspect-button :on-click 'aw-inspect-on-click)

|#

(defMacro cr-button (&key class left top height width name title font on-click)
  #-MCL
  `(make-instance ,(or class ''cg:button)
     ,@(if font `(:font ,font))
     :left ,left
     :top ,top
     :height ,height
     :width ,(or width 70)
     :name ,name 
     :title ,(or title "")
     :on-click ',on-click)
  #+MCL
  `(make-dialog-item 
    ,(or class ''button-dialog-item)
    (make-point (max (- (or ,left 3) 3) 0) (or ,top 0))
    (make-point (or ,width 100) (or ,height 20))
    ,(or title "")
    ,(if on-click `#'(lambda (item)(,on-click (view-container item) item)))
    ,@(if name `(:view-nick-name ,name))
    ,@(if font `(:view-font ,font))
    )
  )

#|
? (omas::cr-button 
    :class 'OMAS-button
    :left 388 :top 2 :width 70 :height 15
    :title "trace"
    :on-click 'aw-trace-on-click
    :name :trace-button)
;; For MCL
(MAKE-DIALOG-ITEM
  'OMAS-BUTTON
  (MAKE-POINT (MAX (- (OR 388 3) 3) 0) (OR 2 0))
  (MAKE-POINT (OR 70 100) (OR 15 20))
  "trace"
  #'(LAMBDA (ITEM) ('AW-TRACE-ON-CLICK (VIEW-CONTAINER ITEM) ITEM))
  :VIEW-NICK-NAME
  :TRACE-BUTTON)
;; for ACL
(MAKE-INSTANCE 'OMAS-BUTTON :LEFT 388 :TOP 2 :HEIGHT 15 :WIDTH 70
               :NAME :TRACE-BUTTON :TITLE "trace" :ON-CLICK
               ''AW-TRACE-ON-CLICK)
|#
;;;--------------------------------------------------------------------- CHECK-BOX

;;; check boxes are created using the default classes
;;; If a colored title must be added, one must use a separate field

(defMacro cr-check-box (&key left height name title title-width font top check 
                             on-change)
  #-MCL
  (declare (ignore width))
  #-MCL
  `(progn
     (make-instance 'cg:check-box
       ,@(if name `(:name ,name))
       :left ,left
       ,@(if title-width `(:width ,title-width))
       ,@(if font `(:font ,font))
       :title ,(or title "")
       :top ,top
       ,@(if height `(:width ,(+ height 5)) `(:width 20))
       :height ,(or height 20)
       :on-change ',on-change
       :value ,check
       )
;;;     (make-instance 'cg:static-text
;;;       :top ,(+ top 2)
;;;       :left ,(+ left 25)
;;;       :height ,(or height 20)
;;;       ,@(if title-width `(:width ,title-width))
;;;       ,@(if font `(:font ,font))
;;;       ,@(if color `(:foreground-color ,color))
;;;       :value ,(or title "")
;;;       )
     )
  #+MCL
  (declare (ignore width))
  #+MCL
  `(make-dialog-item 
    'check-box-dialog-item
    (make-point ,(max (- (or left 3) 3) 0) ,(or top 0))
    (make-point 25 ,(or height 20))
    ',(or title "")
    #'(lambda(item) 
        (,on-change item
                    (check-box-checked-p item) ; new value
                    (not (check-box-checked-p item)) ; old value
                    )) 
    ;; color for MCL is a list e.g. (:text *yellow-color* :frame *green-color*)
    ;,@(if color `(:part-color-list ,color))
    ;,@(if font `(:view-font ,font))
    :check-box-checked-p ,check
    ,@(if name `(:view-nick-name ,name)))
  )

#|
(cr-check-box :title "trace messages"
              :title-width 120
              :name :trace-messages
              :on-change OP-trace-messages-on-change
              :top 30 :left 8 :height 20
              :font *check-label-font*
              :color *check-label-color*
              :check (trace-messages *omas*)
              )

;;; for MCL:
(MAKE-DIALOG-ITEM
  'CHECK-BOX-DIALOG-ITEM
  (MAKE-POINT 5 30)
  (MAKE-POINT 120 20)
  "trace messages"
  #'(LAMBDA (ITEM)
      (OP-TRACE-MESSAGES-ON-CHANGE
        ITEM
        (CHECK-BOX-CHECKED-P ITEM)
        (NOT (CHECK-BOX-CHECKED-P ITEM))))
  :PART-COLOR-LIST
  (LIST :TEXT *CHECK-LABEL-COLOR*)
  :VIEW-FONT
  *CHECK-LABEL-FONT*
  :CHECK-BOX-CHECKED-P
  (TRACE-MESSAGES *OMAS*)
  :VIEW-NICK-NAME
  :TRACE-MESSAGES)
;;; For ACL:
(MAKE-INSTANCE 'CG.CHECK-BOX:CHECK-BOX :NAME :TRACE-MESSAGES :FONT
               *CHECK-LABEL-FONT* :LEFT 8 :TOP 30 :WIDTH 120 :HEIGHT
               20 :FOREGROUND-COLOR *CHECK-LABEL-COLOR* :TITLE
               "trace messages" :ON-CHANGE
               'OP-TRACE-MESSAGES-ON-CHANGE :VALUE
               (TRACE-MESSAGES *OMAS*))
|#
;;;----------------------------------------------------------------- EDITABLE-TEXT
#|
   (make-instance 'editable-text-dialog-item
     :view-position #@(7 27)
     :view-size #@(526 12)
     :view-font '("Chicago" 9 :SRCOR :PLAIN)
     :dialog-item-text (aw-make-task-id-list agent)
     :text-edit-sel-p t
     :view-nick-name 'task-id)

   (make-instance 'MOSS-OUTPUT-PANE
     :view-position #@(10 245)
     :view-size #@(622 519)
     :h-scrollp nil
     :word-wrap-p t
     :view-font '("Tahoma" 11 :SRCOR :bold)
     :dialog-item-text "<answer pane>"
     :view-nick-name :answer-pane)

(make-instance 'cg:EDITABLE-TEXT 
  :FONT (cg:MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL)
  :LEFT 8 :TOP 10
  :HEIGHT 20 :WIDTH 368
  :NAME :display-pane
  :TEMPLATE-STRING NIL 
  :UP-DOWN-CONTROL NIL 
  :VALUE "" 
  ) 

|#
(defMacro cr-text-item (&key class left height width name top text font word-wrap
                             allow-returns
                             (color #xFFFFFF color?)
                             (text-selected nil there?)
                             on-change)
  #-MCL
  (declare (ignore word-wrap allow-returns color color? text-selected there?))
  #-MCL
  `(make-instance 
     ',(or (eval class) 'cg:editable-text)
     :left ,left
     :top ,top
     :height ,height
     :width ,width
     ,@(if name `(:name ,name))
     ,@(if font `(:font ,font))
     ,@(if on-change `(:notify-when-selection-changed t :on-change ',on-change
                                                      :up-down-control nil
                                                      :delayed nil))
     :value ,(if (null text) "" text)
     )
  #+MCL
  (declare (ignore on-change))
  #+MCL
  `(make-instance
     ',(or (eval class) 'editable-text-dialog-item)
     :view-position (make-point (or ,left 0) (or ,top 0))
     :view-size (make-point (or ,width 100) (or ,height 16))
     ,@(if name `(:view-nick-name ,name))
     ,@(if font `(:view-font ,font))
     ,@(if there? `(:text-edit-sel-p ,text-selected))
     ,@(if color? `(:part-color-list 
                    '(:body ,color :frame *master-window-color*)))
     :word-wrap-p ,word-wrap
     :allow-returns ,allow-returns
     :dialog-item-text ,text
     )
  )

#|
(cr-text-item :class  'omas-dialog-item
              :left (car pos)
              :top  (cadr pos)
              :width  (car size)
              :height (cadr size)
              :text "so what?"
              :word-wrap  t
              :allow-returns  t 
              :name :list
              :font *assistant-label-font*)
expands to (MCL)
(MAKE-INSTANCE
  'OMAS-DIALOG-ITEM
  :VIEW-POSITION
  (MAKE-POINT (OR (CAR POS) 0) (OR (CADR POS) 0))
  :VIEW-SIZE
  (MAKE-POINT (OR (CAR SIZE) 100) (OR (CADR SIZE) 16))
  :VIEW-NICK-NAME
  :LIST
  :VIEW-FONT
  *ASSISTANT-LABEL-FONT*
  :WORD-WRAP-P
  T
  :ALLOW-RETURNS
  T
  :DIALOG-ITEM-TEXT
  "so what?")
;; for ACL
(MAKE-INSTANCE 'OMAS-DIALOG-ITEM :LEFT (CAR POS) :TOP (CADR POS)
               :HEIGHT (CADR SIZE) :WIDTH (CAR SIZE) :NAME :LIST
               :FONT *ASSISTANT-LABEL-FONT* :VALUE "so what?")
|#
;;;--------------------------------------------------------------- MULTI ITEM LIST
#|
     ;; input messages
     (make-instance 'multi-item-list 
       :font (make-font-ex nil "Tahoma / ANSI" 11 nil) :name :input-messages
       :background-color (AW-status-color agent)
       :tabs nil :left 5 :top 38 :width 282 :height 70
       :range (mapcar #'omas::message-format (omas::input-messages agent)))
   ;;=== INPUT MESSAGES
   (make-instance 'area-for-values
     :view-position #@(5 45) 
     :view-size #@(260 60) 
     :dialog-item-text "Input messages"
     :cell-size #@(260 15)
     :view-font '("Chicago" 9 :SRCOR :PLAIN)
     :table-hscrollp nil
     :table-vscrollp t
     :view-nick-name 'input-messages
     :table-sequence 
     (mapcar #'message-format (input-messages agent)))

(cr-multi-item-list 'area-for-values  
                    :left 5 :top 45 :height 60 :width 260 :title "Input Messages"
                    :name :input-messages 
                    :sequence (mapcar #'message-format (input-messages agent))
                    :hscrollp nil :vscrollp t :cell-height 15)
|#
(defMacro cr-multi-item-list (&key class left height width name top title font 
                                   sequence cell-height cell-width hscrollp 
                                   vscrollp selection-type tabs cell-color 
                                   on-click on-double-click)
  #-MCL
  (declare (ignore cell-height hscrollp vscrollp selection-type))
  #-MCL
  `(make-instance ,class
     ,@(if font `(:font ,font))
     :left ,left
     :top ,top
     :width (or ,width ,cell-width)
     :height ,height 
     ,@(if name `(:name ,name))
     ,@(if tabs `(:tabs ,tabs))
     ,@(if on-click `(:on-click ',on-click))
     ,@(if on-double-click `(:on-double-click ',on-double-click))
     :range ,sequence
     :title ,(or title "")
     ,@(if cell-color `(:background-color ,cell-color))
     )
  #+MCL
  (declare (ignore background-color tabs))
  #+MCL
  `(make-dialog-item 
    ,(or class 'sequence-dialog-item)
    (make-point (max (- (or ,left 3) 3) 0) (or ,top 0))
    (make-point (or ,cell-width 100) (or ,height 32))
    ,(or title "")
    ,@(cond 
       ((and on-click on-double-click)
        `(#'(lambda(item)
              (if (double-click-p)
                (,on-double-click (view-container item) item)
                (,on-click (view-container item) item)))))
       (on-double-click
        `(#'(lambda(item)
              (if (double-click-p)
                (,on-double-click (view-container item) item)
                ))))
       (on-click
        `(#'(lambda(item)
              (,on-click (view-container item) item))))
       (T (LIST NIL)))
    ,@(if name `(:view-nick-name ,name))
    ,@(if font `(:view-font ,font))
    :cell-size (make-point (or ,cell-width ,width 100) (or ,cell-height 16))
    ,@(if cell-color `(:cell-colors ,cell-color))
    ,@(if selection-type `(:selection-type ,selection-type)) ;:single, :contiguous
    ; :disjoint
    :table-hscrollp ,hscrollp
    :table-vscrollp ,vscrollp
    :table-sequence ,sequence
    )
  ) 
#|
? (cr-multi-item-list :class 'area-for-values
                      :title "List of found objects"
                      :name :agents/messages
                      :on-double-click OP-agents/messages-on-double-click
                      :cell-width 100
                      :cell-height 15
                      :hscrollp nil
                      :vscrollp t
                      :selection-type :disjoint
                      :sequence (mapcar #'car (omas::local-agents omas::*omas*))
                      :top 5 :left 383 :height 95
                      )
;; for MCL
(MAKE-DIALOG-ITEM
  'AREA-FOR-VALUES
  (MAKE-POINT 380 5)
  (MAKE-POINT 100 95)
  "List of found objects"
  #'(LAMBDA (ITEM)
      (IF (DOUBLE-CLICK-P)
          (OP-AGENTS/MESSAGES-ON-DOUBLE-CLICK (VIEW-CONTAINER ITEM) ITEM)))
  :VIEW-NICK-NAME
  :AGENTS/MESSAGES
  :CELL-SIZE
  (MAKE-POINT 100 15)
  :SELECTION-TYPE
  :DISJOINT
  :TABLE-HSCROLLP
  NIL
  :TABLE-VSCROLLP
  T
  :TABLE-SEQUENCE
 (MAPCAR #'CAR (LOCAL-AGENTS *OMAS*)))
;; for ACL
(MAKE-INSTANCE 'AREA-FOR-VALUES :LEFT 383 :TOP 5 :WIDTH
               (COND (NIL) (T 100)) :HEIGHT 95 :NAME
               :AGENTS/MESSAGES :ON-DOUBLE-CLICK
               'OP-AGENTS/MESSAGES-ON-DOUBLE-CLICK :RANGE
               (MAPCAR #'CAR (LOCAL-AGENTS *OMAS*)) :TITLE
               "List of found objects")
|#
;;;------------------------------------------------------------------- STATIC TEXT
#|
   (make-instance 'static-text-dialog-item
     :view-position #@(25 6)
     :view-size #@(510 15)
     :view-font '("Chicago" 12 :SRCOR :PLAIN)
     :dialog-item-text (format nil "~S" (name agent)))


(make-instance 'static-text-dialog-item
     :view-position #@(7 25)
     :view-size #@(526 14)
     :view-font '("Chicago" 9 :SRCOR :PLAIN)
     :dialog-item-text (aw-make-task-id-list agent)
     ;:text-edit-sel-p t
     :part-color-list '(:body #xFFFFFF)
     :view-nick-name 'task-id)

(make-dialog-item 'omas-DIALOG-ITEM
                       #@(5 8) #@(368 13) 
                       ""
                       NIL
                       :VIEW-NICK-NAME 'output
                       :ALLOW-RETURNS t
                       )

|#

(defMacro cr-static-text (&key class top left height width name text title font 
                               text-color color justification)
  #+MCL
  `(make-dialog-item 
    ,(or class ''static-text-dialog-item)
    (make-point (max (- (or ,left 3) 3) 0) (or ,top 0))
    (make-point (or ,width 100) (or ,height 32))
    ,(or title text "")
    nil
    ,@(if name `(:view-nick-name ,name))
    ,@(if font `(:view-font ,font))
    ,@(if justification `(:text-justification ,justification))
    ,@(if color `(:part-color-list (list :body ,color)))
    )
  #-MCL
  (declare (ignore justification))
  #-MCL
  `(make-instance ,(or class ''cg:static-text)
     ,@(if top `(:top ,top))
     ,@(if left `(:left ,left))
     ,@(if height `(:height ,height))
     ,@(if width `(:width ,width))
     ,@(if name `(:name ,name))
     ,@(if font `(:view-font ,font))
     ,@(if text-color `(:foreground-color ,text-color))
     ,@(if color `(:background-color ,color))
     :value ,(or title text "")
  ))

#|
(cr-static-text :class 'omas-DIALOG-ITEM
                :top 8 :left 8 :width 368 :height 13
                :name :display-pane
                )
;; for MCL
(MAKE-DIALOG-ITEM
  'OMAS-DIALOG-ITEM
  (MAKE-POINT (MAX (- (OR 8 3) 3) 0) (OR 8 0))
  (MAKE-POINT (OR 368 100) (OR 13 32))
  ""
  NIL
  :VIEW-NICK-NAME
 :DISPLAY-PANE)
;; for ACL
(MAKE-INSTANCE 'OMAS-DIALOG-ITEM :TOP 8 :LEFT 8 :HEIGHT 13 :WIDTH
  368 :NAME :DISPLAY-PANE :VALUE "")
|#
;;;===============================================================================
;;;
;;;                              Service functions
;;;
;;;===============================================================================

;;;------------------------------------------------------------------------- %BEEP

(defun %beep ()
  #+MCL (beep)
  #-MCL (cg:beep)
  )

;;;-------------------------------------------------------------------- %CONTAINER

(defun %container (item)
  #+MCL (view-container item)
  #-MCL (cg:parent item)
  )

;;;-------------------------------------------------------------- %FIND-NAMED-ITEM

(defun omas::%find-named-item (name win)
  "get the named view inside the window.
Arguments:
   name: a the (nick)name of the component (a view)
   win: a window object
Return:
   the named view."
  #+MCL
  (view-named name win)
  #-MCL
  (cg:find-component name win)
  )


;;;------------------------------------------------------------ %GET-ITEM-SEQUENCE

(defun %get-item-sequence (pane)
  #+MCL (table-sequence pane)
  #-MCL (cg:range pane)
  )

;;;------------------------------------------------------ %GET-SELECTED-CELL-INDEX

(defun %get-selected-cell-index (pane)
  #+MCL (cell-to-index pane (car (selected-cells pane)))
  #-MCL (car (cg:list-widget-get-index pane))
  )

;;;-------------------------------------------------------------------- %GET-VALUE

(defun %get-value (item)
  #+MCL
  (dialog-item-text item)
  #-MCL
  (cg:value item)
  )

;;;----------------------------------------------------------------------- %HEIGHT

(defun %height (item)
  #+MCL
  (point-v (view-size item))
  #-MCL
  (cg:height item)
  )

;;;------------------------------------------------------------------- %MAKE-POINT

(defun %make-point (hh vv)
  #+MCL
  (make-point hh vv)
  #-MCL
  (cg:make-position hh vv)
  )

;;;-------------------------------------------------------------------------- %RGB

(defun omas::%rgb (red green blue)
  "make a color from the components 0-65535."
  #-MCL
  ;; ACL components are limited to 0-255
  (cg:make-rgb :red (floor (/ red 256)) 
               :green (floor (/ green 256)) 
               :blue (floor (/ blue 256)))
  #+MCL
  (make-color red green blue))

; (omas::%rgb 65535 0 0)
;;;------------------------------------------------------------------- %SELECT-ALL

(defun %select-all (pane)
  "select all the text of a text window"
  #+MCL (select-all pane)
  #-MCL (cg:set-selection pane 0 (1+ (length (cg:value pane)))) 
  t)

;;;------------------------------------------------------------------ %SELECT-PANE

(defun %select-pane (dialog pane)
  "select pane setting the mouse into it"
  #+MCL (set-current-key-handler dialog pane)
  #-MCL (declare (ignore dialog))
  #-MCL (cg:set-focus-component pane)
  )

;;;---------------------------------------------------------------- %SELECT-WINDOW

(defun %select-window (dialog)
  "selects a window"
  #+MCL (window-show dialog)
  #-MCL (cg:select-window dialog)
  )

;;;-------------------------------------------------------------------- %SELECTION

(defun %selection (item)
  "returns the selected cells of a multi-item-list.
Arguments:
   item: widget
Return:
  a list."
  #+MCL
  (mapcar #'(lambda (xx) (nth (cell-to-index item xx) (table-sequence item)))
          (selected-cells item))
  #-MCL
  (cg:value item)
  )

;;;---------------------------------------------------------------- %SET-ITEM-TEXT

(defun omas::%set-item-text (item text)
  "set the text of a part.
Arguments:
   item: a the component (a view)
   text: a string
Return:
   the named view."
  #+MCL
  (set-dialog-item-text item text)
  #-MCL
  (setf (cg:title item) text)
  )


;;;--------------------------------------------------------------- %SET-ITEM-COLOR

(defun omas::%set-item-color (item color)
  "set the color (different structure in ACL or MCL) of a part.
Arguments:
   item: a the component (a view)
   color: a color
Return:
   the named view."
  #+MCL
  (set-part-color item :body color)
  #-MCL
  (setf (cg:background-color item) color)
  )

;;;------------------------------------------------------------ %SET-ITEM-SEQUENCE

(defun omas::%set-item-sequence (item sequence)
  "set the values of a sequence.
Arguments:
   item: a the multi item list component
   sequence: a list of strings
Return:
   unimportant."
  #+MCL
  (set-table-sequence item sequence)
  #-MCL
  (setf (cg:range item) sequence)
  )

;;;-------------------------------------------------------------------- %SET-TITLE

(defun %set-title (pane text)
  #+MCL (set-dialog-item-text pane text)
  #-MCL (setf (cg:title pane) text)
  )

;;;-------------------------------------------------------------------- %SET-VALUE

(defun %set-value (item text)
  #+MCL
  (set-dialog-item-text item text)
  #-MCL
  (setf (cg:value item) text)
  )

;;;--------------------------------------------------------------------- %UNSELECT

(defun %unselect (item)
  #+MCL (declare (ignore item))
  #+MCL nil
  #-MCL
  (setf (cg:value item) nil)
  )

;;;------------------------------------------------------------------------ %WIDTH

(defun %width (item)
  #+MCL
  (point-h (view-size item))
  #-MCL
  (cg:width item)
  )

;;;---------------------------------------------------------------------- %WINDOW?

(defun omas::%window? (win)
  "checks if win is an existing window.
Arguments:
   win: a lisp object
Return:
   t if an existing window."
  #+MCL
  (and win (wptr win))
  #-MCL
  (cg:windowp win)
  )

;;;----------------------------------------------------------------- %WINDOW-CLOSE

(defun %window-close (win)
  (when (%window? win)
    #+MCL (window-close win)
    #-MCL (close win)
    ))

(format t "~&;*** OMAS v~A - window macros loaded ***" *omas-version-number*)

;;; :EOF
