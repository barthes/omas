﻿;;;===============================================================================
;;;20/02/02
;;;                N E T U D P  (file netUDP.lisp)
;;;
;;;===============================================================================
;;; This file contains the functions dealing with the net protocol. It uses the 
;;; socket mechanism. 

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| 
A global variable *net-broadcast* controls the behavior of OMAS. When true, then 
all messages are broadcast on the web and a special process listens to the web for
incoming messages. When false, all communications remain local, and the functions 
in this file are not used. Default is broadcasting.

UDP limits the length of a message. Thus, if a message is too long, it must be 
cut into several fragments. The net-send and net-receive functions in this file
only deal with a single fragment that must be shorter than the maximum allowed by
the network (Mac IP: 64K bytes). Marshalling and demarshalling is done by the
agent COMM interface, as well as dealing with rebuilding a full message from its
fragments (that may arrive in any order...).

Three functions allow to use the net services:

INITIALIZE-NET-BROADCAST

   This function opens a socket. 
   Some global variables control the broadcast mechanism:
   - *local-broadcast-address* for the broadcast mask, e.g., "172.17.255.255" 
   - *omas-port* specifies the port 
   - *broadcast-distance* specifies the maximum number of gates that
     a broadcast message can cross before dying (default is 1)
   A receiving process is set up:
   - *net-receive-process* that listens for incoming messages and stacks them onto
     the *net-incoming-message-stack*

NET-SEND message

   Sends a broadcast message on the web using the UDP protocol. The object message
   must be a string shorter than the maximal allowed length for UDP messages.
   Messages are currently limited to 4096 bytes.

TERMINATE-NET-BROADCAST

   Cleans up the various structures and closes OpenTransport.

RECEIVING
Message (string fragments) are stacked onto the *net-incoming-message-stack*

Needed external variables
=========================
*net-receive-process* to host the receiving loop
*net-incoming-message-stack* to store the incoming fragments

2020
 0202 restructuring OMAS as an asdf system
|#


(eval-when (compile load eval)
  (require :sock)
  (unless (find-package :omas)(make-package :omas))) 

(in-package :omas)

;;;=============================== Globals =========================================

;;; global parameters setting the local network characteristics (although the
;;; machine IP should be recoverable from system info, hence the broadcast
;;; address is available internally)...


;;;========================== Service functions ====================================

;;;--------------------------------------------------------- MAKE-STRING-FROM-BUFFER
;;; defined in netcomm.lisp

;;;========================= External functions ====================================

;;;-------------------------------------------------------- NET-INITIALIZE-BROADCAST
; (net-initialize-broadcast)

(defun net-initialize-broadcast ()
  "This function opens a socket if necessary.
Arguments:
   none
Return:
   :done"
  (unless (socket *omas*)
    (setf (socket *omas*) 
      (socket:make-socket :type :datagram 
                          :local-port (omas-port *omas*)
                          :broadcast t)))
  ;; set up receiving process unless it already exists
  (unless (net-receive-process *omas*)
    (setf (net-receive-process *omas*) 
          (mp:process-run-function "Net Receive" #'receiving)))
  :done)

;;;------------------------------------------------------------------------ NET-SEND
; (defParameter *mescount* 0)
; (net-send (format nil "Test A-~S"(incf *mescount*)))
; (net-send (format nil "Test B-~S"(incf *mescount*)))
; (net-send "Test BB")

(defUn net-send (fragment)
  "Sends a broadcast message fragment on the net using the UDP protocol. ~
   fragment lengt is limited.
Arguments:
   fragment: a String less than *kTransferBufferSize* bytes long
Return
   code returned by the send-to fuction."
  (let (buffer test errno)
    ;; check arg
    (unless (stringp fragment)
      (error "fragment should be a string rather then: ~S" fragment))
    ;; convert fragment to a byte array using unicode :fat format (big endian)
    (setq buffer (excl::string-to-octets fragment :external-format :fat))
    ;; try to catch errors
    (multiple-value-setq (test errno)
      (ignore-errors
       (socket:send-to (socket *omas*) buffer (length buffer)
                       :remote-host (local-broadcast-address *omas*)
                       :remote-port (omas-port *omas*)))
      )
    ;(format t "~&~S/ net-send/ ... send-to error check. test: ~S error: ~S."
    ;        "OMAS" test errno)
    (if (and (null test) errno)
        ;; could not send message for some reason
        (progn
          (cg:beep)
          (format t "~&~S/ net-send/ Can't send message fragment on LAN loop ~S. 
   Error: ~S~&Message: ~S" 
            "OMAS" (local-broadcast-address *omas*) errno fragment))
      ;; otherwise OK
      ;(format t "~&~S/ net-send/ message fragment was put on LAN loop:~%  ~S" 
      ;  "OMAS" fragment)
      )
    ))

;;;----------------------------------------------------------------------- RECEIVING

(defun receiving (&aux message)
  "receive a fragment from the network and pushes that into the stack"
  (loop
    (multiple-value-bind 
      (raw-buffer size)
        (socket:receive-from (socket *omas*) (KTRANSFERBUFFERSIZE *omas*) :extract t)
      ;(format t "~&receiving/ raw buffer:~& ~S~&  size: ~S" raw-buffer size) 
 
      ;; transform unicode string into internal string
      ;; truncate is added as a safety measure
      (setq message (excl::octets-to-string raw-buffer :external-format :fat
                                            :truncate t))
      ;(format t "~&receiving/ ...as string:~& ~S" message)

      (push message *net-incoming-message-stack*)
      (push message (net-incoming-message-stack *omas*)))
    ))

;;;---------------------------------------------------------- NET-TERMINATE-BROADCAST
;(net-terminate-broadcast)

(defun net-terminate-broadcast ()
  "empties all lists, reset global-variables, closes socket"
  (when (socket *omas*)
    (close (socket *omas*))
    (setf (socket *omas*) nil))
  ;; also kill receive process
  (when (net-receive-process *omas*)
    (mp:process-kill (net-receive-process *omas*))
    (setf (net-receive-process *omas*) nil))
  :done)

(format t "~&;*** OMAS v~A - UDP net interface loaded ***" *omas-version-number*)

;;; :EOF 