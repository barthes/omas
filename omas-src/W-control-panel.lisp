﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;
;;;     O M A S - C O N T R O L - P A N E L  (file W-control-panel.lisp)
;;;
;;;===============================================================================
;;; This file contains all the functions related to the control panel for running
;;; the OMAS platform.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| 
2020
 0202 restructuring OMAS as an asdf system
|#

(in-package :omas)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (omas::omas-trace-add :control-panel "traces a message received by the control panel")
  )

;;;=========================== panel interface ==============================

;;; ========== Control Panel ==========
;;; the screen set up is organized as foloows
;;;
;;;   +------------------------------+----------------------------+
;;;   |       OMAS    control        |                            |
;;;   |           panel              |     graphics trace         |
;;;   +------------------------------|                            |
;;;   |        Agent 1               |                            |
;;;   +------------------------------|                            |
;;;   |       ..........  ...........+----------------------------+
;;;   +------------------------------|       Listener             |
;;;   |        Agent n-1             |    (message trace)         |
;;;   +------------------------------|                            |
;;;   |        Aggent n              |                            |
;;;   +------------------------------+----------------------------+ 
;;;
;;; Everything is done through the control panel which is launched at the
;;; beginning. The area agent 1 to agent n are slots for displaying the
;;; content of agents specified by double clicking in the list of agents in
;;; the control panel. The system allocates a free slot  in the agent area
;;; The graphics trace displays a diagram of what is going on.
;;; The Listener area can be used as a Lisp listener or is used to display
;;; messages and messages when traced.
;;;
;;; To display a control panel, execute the following:
;;;        (OMAS)
;;; This should display a green control panel with a number of buttons.
;;;
;;; To display an agent window, execute the following:
;;;        (make-agent-window 'proxy 1)
;;;        (make-agent-window 'factorial 2)

;;; geting last answer sent to the control panel
;;;   (user-get-last-answer)

;;;============================== Globals ========================================

(defParameter *panel-left-position* 0)
(defParameter *panel-width* 555)

(defParameter *panel-height* 140)
;; used to take into account height of IDE menus no longer needed in ACL 8.1
(defParameter *panel-height-offset* 2)

(defParameter *panel-exterior*
  (cg:MAKE-BOX *panel-left-position* *panel-height-offset* 
               *panel-width* (+ *panel-height-offset* *panel-height*)))

(defParameter *panel-color* (cg:MAKE-RGB :RED 68 :GREEN 90 :BLUE 22) "dark green")

(defParameter *panel-font* (cg:MAKE-FONT-EX :SWISS "MS Sans Serif / ANSI" 11 NIL))

;;; widget attributes

(defParameter *label-font* (cg:MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL))

(defParameter *label-color* (cg:MAKE-RGB :RED 255 :GREEN 255 :BLUE 0))

(defParameter *left-center-button-position* 205) ;#+MCL 278 #-MCL 296)

(defParameter *selecting-agents* t 
  "if true, displays agents in selection list, otherwise list of messages")

;;;===============================================================================
;;;                         WINDOW and BUTTON CLASSES
;;;===============================================================================

;;;===== first create a special class of buttons

(defClass omas-button (#+mcl button-dialog-item #-mcl cg:button)
  ((dummy)))

;;; also create a class for displaying lists of values
(defClass area-for-values (cg:multi-item-list)
  ((original-list)) ; list to be displayed in the table area
  )

;; We define a control panel for OMAS
(defClass omas-control-panel (cg:dialog)
  ((input-message :accessor input-message :initform nil))
  (:documentation
   "Defines the format and behavior of the OMAS control panel"))

;;; now a subclass for handling the scenario inputs
(defClass omas-dialog-item (cg:multi-line-editable-text) ; JPB00901
  ((dummy)))

;;;===============================================================================
;;;                             CONTROL PANEL
;;; (make-OMAS-control-panel)
;;;===============================================================================

;;;------------------------------------------------------- MAKE-OMAS-CONTROL-PANEL

(defUn make-omas-control-panel
    (&KEY PARENT (OWNER (OR PARENT (cg:SCREEN cg:*SYSTEM*)))
          (EXTERIOR *panel-exterior*) 
          (NAME :OMAS-PANEL)
          (TITLE 
           (format nil (get-text *WCP-title*) 
             (format nil "~A - ~A - ~A" *omas-version-number*
               (site-name *omas*) (coterie-name *omas*))))
          )
  (let ((win
         (cg:MAKE-WINDOW 
             NAME :OWNER OWNER
           :CLASS 'OMAS-CONTROL-PANEL
           :EXTERIOR EXTERIOR
           :BACKGROUND-COLOR *panel-color*
           :BORDER :FRAME
           :CHILD-P NIL
           :CLOSE-BUTTON T
           :CURSOR-NAME :ARROW-CURSOR
           :FONT *panel-font*
           :FORM-STATE :SHRUNK
           :MAXIMIZE-BUTTON NIL
           :MINIMIZE-BUTTON T
           :NAME :OMAS-CONTROL-PANEL
           :POP-UP NIL
           :RESIZABLE NIL
           :SCROLLBARS NIL
           :STATE :NORMAL
           :STATUS-BAR NIL
           :SYSTEM-MENU T
           :TITLE TITLE
           :TITLE-BAR T
           :TOOLBAR NIL
           :DIALOG-ITEMS (MAKE-OMAS-PANEL-WIDGETS)
           :FORM-P NIL
           )))
    (setf (OMAS-window *omas*) win)))

#|
(make-OMAS-control-panel)
(set-part-color (view-named 'clock-item *OMAS-window*) :text *red-color*)
(set-part-color (view-named 'step-button *OMAS-window*) :text *red-color*)
|#
;;;------------------------------------------------------ MAKE-OMAS-PANEL-WIDGETS

(defUn make-omas-panel-widgets ()
  "creates all widgets inside control panel"
  (declare (special *label-font* *label-color*))
  (list 
   ;;----------------------------------------------------------- posting area
   (cr-text-item
    :top 10 :left 8 :width 295 :height 20
    :font *label-font*
    :name :display-pane
    )
   ;;------------------------------------------------------------ check boxes
   ;; check boxes to control the printing of text into graphics window
   (cr-check-box :name :trace-messages
                 :on-change OP-trace-messages-on-change
                 :top 30 :left 8 :height 20
                 :check t     
                 )
   (cr-static-text :top 32 :left 33 :height 20 
                   :text (get-text *WCP-trace-messages*)     
                   :width 100 
                   :font *label-font*
                   :text-color *label-color*)
   ;; radio button to control drawing of bids on the graphics trace
   (cr-check-box :name :draw-bids
                 :on-change OP-draw-bids-on-change
                 :top 50 :left 8 :height 20
                 :check (graphics-no-bids *omas*) ; JPB1005
                 )
   (cr-static-text :top 52 :left 33 :height 20 
                   :text (get-text *WCP-draw-bids*)     
                   :width 100 
                   :font *label-font*
                   :text-color *label-color*)
   ;; radio button to control drawing of bids on the graphics trace
   (cr-check-box :name :draw-timers
                 :on-change OP-draw-timers-on-change
                 :top 70 :left 8 :height 20
                 :check (drawing-timeouts *omas*)
                 )
   (cr-static-text :top 72 :left 33 :height 20 
                   :text (get-text *WCP-draw-timers*)     
                   :width 100 
                   :font *label-font*
                   :text-color *label-color*)
   ;; set/reset explanations (verbose)
   (cr-check-box :name :verbose
                 :on-change OP-verbose-on-change
                 :top 90 :left 8 :height 20
                 :check (OMAS-verbose *omas*)
                 )
   ;; not done as title of check box, ACL does not allow color titles (Windows 7)
   (cr-static-text :top 92 :left 33 :height 20 
                   :text (get-text *WCP-verbose*)     
                   :width 100 
                   :font *label-font*
                   :text-color *label-color*)
   ;;------------------------------------------------------ leftmost buttons 
   
   ;; kill message, removing it from the list of user messages
   (cr-button :class 'OMAS-BUTTON
              :title (get-text *WCP-kill-msg*)
              :name :kill-message
              :on-click OP-kill-message-on-click
              :font *label-font*
              :width 65
              :top 35 :left 135 :height 20
              )
   
   ;; open a window to create a new message
   (cr-button :class 'OMAS-BUTTON
              :title (get-text *WCP-new-msg*)
              :name :new-message
              :on-click OP-new-msg-on-click
              :font *label-font*
              :width 65
              :top 60 :left 135 :height 20
              )
   
   ;; send selected message
   (cr-button :class 'OMAS-BUTTON
              :title (get-text *WCP-send-msg*)
              :name :send-message
              :on-click OP-send-message-on-click
              :font *label-font*
              :width 65
              :top 85 :left 135 :height 20
              )
   
   ;;--------------------------------------------------- left center buttons 
   
   ;; toggles between agent and message lists
   (cr-button :class 'OMAS-BUTTON
              :title (get-text *WCP-agents/msg*)
              :width 100
              :name :agents/msg-button
              :on-click OP-agents/msg-on-click
              :font *label-font*
              :top 35 :left *left-center-button-position* :height 20
              )
   
   ;; load additional agent, selecting folder if needed
   (cr-button :class 'OMAS-BUTTON
              :title (get-text *WCP-load-agent*)
              :width 100
              :name :load-agent
              :on-click OP-load-agent-on-click
              :font *label-font*
              :top 60 :left *left-center-button-position* :height 20
              )
   
   ;; reset graphics window, closing it and creating a new one
   (cr-button :class 'OMAS-BUTTON
              :title (get-text *WCP-reset-graphics*)
              :width 100
              :name :reset-graphics
              :on-click OP-reset-graphics-on-click
              :font *label-font*
              :top 85 :left *left-center-button-position* :height 20
              )
   
   ;;----------------------------------------------------- agent/message area
   ;; area for selecting agents to display
   ;; select list of agents to be displayed in the time graph
   ;; obtain selected cells from agent-list-item
   (cr-multi-item-list 
    :class 'area-for-values
    :title (get-text *WCP-found-objects*)
    :name :agents/messages
    ;; we are using one click to select object in the list...
    :on-double-click OP-agents/messages-on-double-click
    :cell-width 170
    :cell-height 15
    :hscrollp t
    :vscrollp t
    :selection-type :disjoint
    :sequence (if *selecting-agents* 
                (mapcar #'name (get-visible-agent-ids)) 
                (mapcar #'car (user-messages *omas*)))
    :top 10 :left 310 :height 95
    )
   
   ;;--------------------------------------------------------- right buttons 
   ;; trace by marking plist of agent key
   (cr-button :class 'OMAS-BUTTON
              :title (get-text *WCP-trace*)
              :width 55
              :name :trace
              :on-click OP-TRACE-ON-CLICK
              :font *label-font*
              :top 10 :left 488 :height 20
              )
   
   (cr-button :class 'OMAS-BUTTON
              :title (get-text *WCP-untrace*)
              :width 55
              :name :untrace
              :on-click OP-UNTRACE-ON-CLICK
              :font *label-font*
              :top 35 :left 488 :height 20
              )
   
   ;; reset keeps the set-up but resets everything to nil, fresh to start
   ;; a new simulation scenario
   (cr-button :class 'OMAS-BUTTON
              :title (get-text *WCP-ontology*)
              :width 55
              :name :ontology
              :on-click OP-ontology-on-click
              :font *label-font*
              :top 60 :left 488 :height 20
              )
   
   ;; add a quit button. In fact here we should not quit but reset the 
   ;; simulation environment, to allow loading a new simulation from some 
   ;; sort of list built dynamically from the name (or extension of the 
   ;; files in the simulation folder...
   (cr-button :class 'OMAS-BUTTON
              :title (get-text *WCP-quit*)
              :width 55
              :name :quit
              :on-click OP-quit-on-click
              :font *label-font*
              :top 85 :left 488 :height 20
              )
   ))

;;;===============================================================================
;;;                          FUNCTIONS AND CALLBACKS
;;;===============================================================================

;;; ------------------------------------------- OP-AGENTS/MESSAGES-ON-DOUBLE-CLICK

(defUn op-agents/messages-on-double-click (dialog widget)
  "opens an agent window on double click or a message window"
  (declare (ignore dialog))
  (let ((selection (%selection widget)))
    ;; first a single item should be selected
    (when (or (null selection) (cdr selection))
      (%beep)
      (warn "a single item must be selected in the list")
      (return-from  OP-agents/messages-on-double-click nil))
    ;; when agents are showing, open an agent window
    ;; selection is a list of agent names, takes the first one
    (if *selecting-agents*
      ;(format *debug-io* "Showing agent ~s" (car selection))
      (display-agent (%agent-from-key (keywordize (car selection))))
      ;; otherwise open message window
      ;(format *debug-io* "Showing message ~s" (car selection)))
      (message-edit (%message-from-key (car selection))))
    )
  t)

;;; ------------------------------------------------------- OP-AGENTS/MSG-ON-CLICK

(defUn op-agents/msg-on-click (dialog widget)
  "toggles the agent/message list in the multi-list pane"
  (declare (ignore widget))
  (setq *selecting-agents* (not *selecting-agents*))
  (%set-item-sequence (%find-named-item :agents/messages dialog)
                      (if *selecting-agents*
                        (mapcar #'name (get-visible-agent-ids))
                        (mapcar #'car (user-messages *omas*))))
  ;; unselect anything selected
  (%unselect (%find-named-item :agents/messages dialog))
  t)

;;; ------------------------------------------------------- OP-DRAW-BIDS-ON-CHANGE

(defUn op-draw-bids-on-change (dialog new-value old-value)
  "toggles the flag for drawing bids (contract net) onto the graphics window"
  (declare (ignore dialog old-value))
  ;; value returns the value before it is changed
  (setf (graphics-no-bids *omas*) new-value)
  t)

;;; ----------------------------------------------------- OP-DRAW-TIMERS-ON-CHANGE

(defUn op-draw-timers-on-change (dialog new-value old-value)
  "toggles the flag for drawing timers along lifelines onto the graphics window"
  (declare (ignore dialog old-value))
  ;; value returns the value before it is changed
  (setf (drawing-timeouts *omas*) new-value)
  t)

;;; ----------------------------------------------------- OP-KILL-MESSAGE-ON-CLICK

(defUn op-kill-message-on-click (dialog widget)
  "kill the selected message"
  (declare (ignore widget))
  (let ((selection (%selection (%find-named-item :agents/messages dialog))))
    ;; kill all messages in the selection, i.e. removes them from user-messages 
    (when *selecting-agents*
      ;; not the right selection
      (%beep)
      (return-from OP-kill-message-on-click nil))
    
    ;; remove all messages from the user list
    (dolist (msg selection)
      (setf (user-messages *omas*)
            (remove msg (user-messages *omas*) :key #'car)))
    ;; update list
    (%set-item-sequence (%find-named-item :agents/messages dialog)
                        (mapcar #'car (user-messages *omas*)))
    ;; unselect anything selected
    (%unselect (%find-named-item :agents/messages dialog))
    )
  t)

;;;-------------------------------------------------------- OP-LOAD-AGENT-ON-CLICK

(defUn op-load-agent-on-click (dialog widget)
  "let the user load a new agent from one of the user's files."
  (declare (ignore widget))
  (load-agent)
  ;; should update agent list in the control panel
  (%set-item-sequence (%find-named-item :agents/messages dialog)
                      (mapcar #'car (local-agents *omas*)))
  ;; unselect anything selected
  (%unselect (%find-named-item :agents/messages dialog))
  t)

;;; ---------------------------------------------------------- OP-NEW-MSG-ON-CLICK

(defUn op-new-msg-on-click (dialog widget)
  "opens a message edit window to let the user create a  new message"
  (declare (ignore dialog widget))
  ;; use open message window or create a new one
  (make-message-window)
  t)

;;; -------------------------------------------------------- OP-ONTOLOGY-ON-CLICK

(defUn op-ontology-on-click (dialog widget)
  "opens the ontology window on click."
  (declare (ignore widget)(special *selecting-agents*))
  (let* ((agent-pane (%find-named-item :agents/messages dialog))
         (selection (%selection agent-pane)))
    ;; first a single item should be selected
    (when (or (null selection) (cdr selection)(not *selecting-agents*))
      (%beep)
      (warn "a single item must be selected from the list of agents")
      (return-from  OP-ontology-on-click nil))
    ;; when agents are showing, open an agent window
    ;; selection is a list of agent names, takes the first one
    (if *selecting-agents*
      ;(format *debug-io* "Showing agent ~s" (car selection))
      (show-ontology dialog (%agent-from-key (keywordize (car selection))))
      ;; otherwise open message window
      ;(format *debug-io* "Showing message ~s" (car selection)))
      )
    )
  t)

;;; ------------------------------------------------------------- OP-POST-MESSAGE
;;; kludge: kept for compatibility with ACL version

(defUn op-post-message (message)
  "post a message into the upper left window, presumably an answer to the user.
Arguments:
   message: a message object"
  (declare (special *omas*))
  (let ((ww (omas-window *omas*)))
    ;; kludge: for some reason can't get text to print in black with MCL...
    (when ww
      #+MCL (set-part-color (%find-named-item :display-pane ww) :body 6387520)
      
      (%set-value (%find-named-item :display-pane ww)
                  (message-format message))
      ;; record it in the window structure
      (setf (input-message ww) message)
      )
    ;; print also in console, which should not be bothering since answers to 
    ;; control panel are not many
    (user-receive-message message)
    t))

#|
(op-post-message (message-make :to :mul-1 :type :request :action :multiply 
                               :args '(3 4)))
; =================================================================== 
; Control Panel: Message reveived by the user (NIL return address) : 
; Task: NIL, date: "14/12/02 18:13:27", done-by: NIL, args: (3 4), 
; result: NIL 
; ===================================================================
T
|#
;;; ------------------------------------------------------------- OP-QUIT-ON-CLICK

(defUn op-quit-on-click (dialog widget)
  "leaves the application"
  (declare (ignore widget))
  (when (y-or-n-p "Are you sure you want to quit?")
    ;; should close all the opened windows
    (mapc #'close (cg.base:windows (cg:screen cg.base:*system*)))
    (omas-exit) ; closes database and net
    (close dialog)
    (excl:exit))
  t)

;;; ------------------------------------------------------- OP-RESET-CONTROL-PANEL
;;; not too useful ???

(defUn op-reset-control-panel ()
  "resets control panel with what is left as local agents"
  (%window-close (omas-window *omas*))
  ;; reset panel to display agents
  (setq *selecting-agents* t)
  (setf (omas-window *omas*) (make-omas-control-panel))
  :done)

;;; ------------------------------------------------------------ OP-RESET-ON-CLICK

;;; This is not so easy to do since agents may have ontologies, with entries
;;; intermingled with MOSS or OMAS entries. THey also have specific functions
;;; in their own name space.
;;; A possible solution would be to remove every symbol of the agent name spaces
;;; and to relaod each agent again.

(defUn op-reset-on-click (dialog widget)
  "reset the application, cleans the environment"
  (declare (ignore dialog widget))
  (%beep)
  (warn "Can't do that yet...")
  t)

#|
 #'(lambda (item)
     (reset-all-agents)
     ;(setq *clock* 0 *cycle-number* 0)
     (reset-control-panel item)
     ;; update all windows
     (dolist (agent (mapcar #'cdr (local-agents *omas*)))
       (agent-display agent)))
|#
;;; --------------------------------------------------- OP-RESET-GRAPHICS-ON-CLICK

(defUn op-reset-graphics-on-click (dialog widget)
  "creates a new graphics window to trace messages"
  (declare (ignore dialog widget))
  (let ((spy-key (key (symbol-value (spy-name *omas*)))))
    ;; close graphics window if opened
    (%send-message (make-instance 'message :type :sys-inform :to spy-key 
                                  :action :disable)
                   t) ; indicates local message
    ;; if agents were not selected select them all
    (unless (names-of-agents-to-display *omas*)
      (setf (names-of-agents-to-display *omas*) 
            (mapcar #'car (local-agents *omas*))))
    (%send-message (make-instance 'message :type :sys-inform :to spy-key
                                  :action :enable)
                   t) ; indicates local message
    )
  t)

;;; ----------------------------------------------------- OP-SEND-MESSAGE-ON-CLICK

(defUn op-send-message-on-click (dialog widget)
  "send the selected message"
  (declare (ignore widget))
  (if *selecting-agents*
    ;; must be in selecting messages mode
    (progn 
      (%beep)
      (warn (get-text *WCP-select-message*)))
    (progn
      (send-message 
       (%message-from-key 
        (car (%selection (%find-named-item :agents/messages dialog)))))))   
  t)

;;;--------------------------------------------------------------- OP-SHOW-AGENTS

(defun op-show-agents ()
  "function to redisplay agents in current control-panel"
  (let ((op (cg:find-window :omas-panel)) aw)
    (when op
      (setq aw (cg:find-component :agents/messages op))
      (when aw
        (when *selecting-agents* 
            (setf (cg:range aw)(mapcar #'name (get-visible-agent-ids))))))))

;;; -------------------------------------------------- OP-TRACE-MESSAGES-ON-CHANGE

(defUn op-trace-messages-on-change (widget new-value old-value)
  "toggles the flag for printing messages onto the graphics window"
  (declare (ignore widget old-value))
  ;; value returns the value before it is changed
  (setf (trace-messages *omas*) new-value)
  t)

;;; ------------------------------------------------------------ OP-TRACE-ON-CLICK

(defUn op-trace-on-click (dialog widget &aux agent-key)
  "trace messages sent to the selected(s) agent(s) by inserting mark ~
   on plist of agent key."
  (declare (ignore widget))
  (cond
   (*selecting-agents*
    ;; trace all selected agents
    (dolist (agent-name (%selection (%find-named-item :agents/messages dialog)))
      ;; here we get the name of the agent, not the key
      (if (boundp agent-name)
        (setq agent-key (key (symbol-value agent-name)))
        (progn
          (warn "can't get agent key from name: ~S" agent-name)
          (return-from op-trace-on-click nil)))
      ;; mark the agent key (trace property)
      (setf (get agent-key :trace) t)
      ))
   ;; should select agents first
   (t
    (%beep)
    (warn (get-text *WCP-select-agent*))))
  t)

#|
(setq dialog (omas-window *omas*))
|#
;;; ---------------------------------------------------------- OP-UNTRACE-ON-CLICK

(defUn op-untrace-on-click (dialog widget &aux agent-key)
  "untrace messages sent to the selected agent by removing ~
   mark from plist of agent key."
  (declare (ignore widget))
  (cond
   (*selecting-agents*
    ;; trace all selected agents
    (dolist (agent-name (%selection (%find-named-item :agents/messages dialog)))
      ;; here we get the name of the agent, not the key
      (if (boundp agent-name)
        (setq agent-key (key (symbol-value agent-name)))
        (progn
          (warn "can't get agent key from name: ~S" agent-name)
          (return-from op-untrace-on-click nil)))
      ;; mark the agent key (trace property)
      (setf (get agent-key :trace) nil)))
   ;; should select agents first
   (t
    (%beep)
    (warn (get-text *WCP-select-agent*))))
  t)

;;; --------------------------------------------------------- OP-VERBOSE-ON-CHANGE

(defUn op-verbose-on-change (widget new-value old-value)
  (declare (ignore widget old-value)
           #-MCL
           (special *TW-DISPLAY-LIST* *TW-V-OFFSET* *TW-LAST-V-POSITION*))
  ;; record value
  (setf (omas-verbose *omas*) new-value)
  #-MCL
  (cond (new-value 
         (setf (text-window *omas*) (make-text-window))
         ;; reset everything in the text window just to make sure
         (setq *tw-display-list* nil
               *tw-v-offset* 0
               *tw-last-v-position* 0))
        ((TEXT-WINDOW *omas*)
         ;; close the text window and reset values
         (%window-close (text-window *omas*))
         (setq *tw-display-list* nil
               *tw-v-offset* 0
               *tw-last-v-position* 0)
         (setf (text-window *omas*) nil))
        )
  t)

;;;==============================================================================

;;;----------------------------------------------------------------- SHOW-ONTOLOGY
;;; the way we handle the situation is as follows:
;;; - we allow a single ontology window to be opened at a time
;;; - before opening the window we set some global process variables (*package*
;;;   *language* *context* and *version-graph*)
;;; When we open a new window, we check if another one is already active. If so
;;; we kill it before opening the new one.
;;; the global variable cg:*outline-window-info* contains the necessary info
;;; for doing the bookkeeping, formatted as follows:
;;;   (:agent <agent> :ontology-window <window>)
;;;
;;; Warning: the mechanism is not clean in the sense that closing the ontology
;;; window directly does not reset the global variable. However, this does not
;;; impair the functioning, since closing a closed window is accepted.

(defUn show-ontology (dialog agent)
  "creates and display a MOSS overview window in a separate process."

  (let* (win forest)
    ;; if an outline window is active, we assume it displays the ontology of
    ;; another agent; we must first kill it
    (when *outline-window-info*
      (let ((old-agent (getf *outline-window-info* :agent)))
        ;; close outline window
        (close (getf *outline-window-info* :ontology-window))
        ;; remove window reference from the old agent structure
        (setf (ontology-window old-agent) nil)))
    
    ;(print `("W-control-panel/show-ontology/ *package*" ,*package*))
    
    ;; when entering from the Control Panel *package* is that :CG-USER
    ;; We must set current package to that of agent, as well as moss values
    (let ((*package* (ontology-package agent))
          ;; not really usefull since we'll take the values from the agent package
          ;(moss::*language* (language agent))
          ;(moss::*context* (moss-context agent))
          ;(moss::*version-graph* (moss-version-graph agent))
          )
      
      (break "show-ontology")
      
      ;; build the tree in the agent's package
      (setq forest (moss::%make-entity-tree-names 
                    (moss::%make-entity-tree)))
      ;; display with concepts in alphabetic order
      (setq win
            (moss::ow-make-outline-window  
             (moss::mw-gg (moss::onto-sort (moss::mw-ff forest))) 
             dialog
             :owner
             agent
             :title 
             (concatenate 'string (symbol-name (name agent)) " ONTOLOGY")
             :database
             (base agent))
            )
      ;; the problem is that the window operates in the current process in
      ;; whatever package is currently active
      ;; instances should be accessed in the agent's package
      (setf (ontology-window agent) win)
      ;; set global info variable
      (setq *outline-window-info* 
            `(:agent ,agent :ontology-window ,win))
      ))
  t)

;;;====================== Printing a message to the user ========================
;;; useless?

(defUn user-receive-message (message)
  "simply prints the message on the user channel of the control panel"
  (dformat :control-panel 0
          "~&; =================================================================== ~
           ~&; Control Panel: Message reveived by the user (NIL return address) : ~
           ~&; Task: ~S, date: ~S, done-by: ~S, args: ~A, 
; result: ~A ~
           ~&; ===================================================================" 
          (task-id message)
          (date-time-string (date message))
          (from! message)
          (format nil "~S" (args message))
          (format nil "~S" (contents message))
          ))

(format t "~&;*** OMAS v~A - control panel loaded ***" *omas-version-number*)

;;; :EOF
