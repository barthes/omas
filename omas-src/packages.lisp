;;;==========================================================================
;;;24/10/17
;;;		
;;;		     P A C K A G E S - (File packages.LISP)
;;;	 
;;;==========================================================================
;;; This file contains the definitions of packages used by the OMAS application

;;; (c) Jean-Paul Barthès@HEUDIASYC, CNRS, UTC. See LICENSE file for permissions.

#|
History
-------
2024
 1017 adapting to Windows 10
2020
 0201 creation
|#

(defpackage :moss)

(defpackage :omas (:use :cl :moss)(:nicknames :o)
  (:export 
   :getv 
   :getvl 
   :getstr 
   :with-transaction 
   :*omas*
   :publish-omas-web-edit-forms
   ;;== from agents
   :defagent 
   :defskill 
   :defassistant 
   :make-agent 
   :make-skill 
   :make-assistant
   :defeditlayout
   ;;== from display
   :getw
   :omas-window   
   ;;== salient features from assistant
   :SF-checks 
   :SF-clean 
   :SF-delete 
   :SF-get 
   :SF-put 
   :SF-reset 
   :ST-trim
   ;;== from messages
   :defmessage
   :make-message
   :user-send-inform
   :user-send-request
   :user-get-last-answer
   ;;== from self
   :remember 
   :recall 
   :deffact 
   :forget
   ;;== from API
   :abort-current-task 
   :abort-task 
   :assistant?
   ;:add-values
   :cancel-all-subtasks 
   :answering-agent
   :cancel-subtask 
   :dformat
   :drformat
   :dynamic-exit
   :env-add-values
   :env-get
   :env-rem-values
   :env-set
   :forget
   :gate-close
   :gate-create
   :gate-get-data
   :gate-open
   :gate-pop-data
   :gate-push-data
   :get-environment
   :get-field
   :get-field+
   :get-internal-time-string
   :get-str
   :get-time-limit
   :getstr
   :master-task?
   ;:omas
   :omas-exit
   :omas-trace
   :omas-trace?
   :omas-trace-add
   :omas-untrace
   :pending-subtasks?
   :purely-local-skill?
   :receiving-agent
   :rem-field
   :reset-all-agents
   :save-selection
   :send-message 
   :send-subtask 
   :send-inform
   :sending-agent
   :static-exit
   :slow
   :speak
   :time-string
   :update-environment
   ;;== from goals
   :defgoal 
   :make-goal 
   :df 
   :dtf
   ;;== from persistencey
   :agent-start-changes 
   :agent-commit-changes 
   :agent-abort-changes
   :agent-show-changes
   ;; from inferer
   :definfrule
   ;; from load-app
   :load-omas-file 
   :load-application-file
   )
  )

;; package for the message tracing window
(defpackage :spy (:use :cl :moss :omas))

(defpackage :omas.web
  (:use :common-lisp :excl :net.aserve :net.html.generator :omas)
  (:nicknames :owb)
  (:import-from :omas :*omas*))


  

;; :EOF