﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;14/03/07
;;;                O M A S - W - P O S T M A N  (file W-postman.lisp)
;;;
;;;===============================================================================
;;; This file contains all the functions related to postman windows.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| 
2020
 0202 restructuring OMAS as an asdf system
|#

(in-package :omas)

;;;===============================================================================
;;;                               POSTMAN WINDOW
;;;===============================================================================
;;; we use data from the omas-W-agent.lisp file

(defClass omas-postman (omas-agent)
  ((known-postmen :accessor known-postmen :initform nil))
  (:documentation
   "Contains special functions for connecting/deconnecting other postmen."))

;;;---------------------------------------------------------------- %AGENT-DISPLAY

(defMethod %agent-display ((agent postman))
  "update postman window by filling the various fields. This method should be ~
   called by the process that created the window. The window must exist.
Arguments:
   agent: postman to display
Return:
  unimportant."
  (let ((win (omas::window agent))) 
    (when (omas::%window? win)
      (let* ((task-list (omas::active-task-list agent))
             ;(environment (omas::environment agent))
             message-log)
        ;; update status coloring the input-messages area
        (omas::%set-item-color (omas::%find-named-item :input-messages win)
                               (AW-status-color agent)) 
        
        ;; post task list (should limit the # of chars)
        (omas::%set-item-text 
         (omas::%find-named-item :task-list win)
         (if task-list 
           (omas::string-at-most (format nil "~{~S~^,~}" 
                                         (mapcar #'omas::id task-list))
                                 60)
           (get-text *WAG-tasks*)))
        
        ;; spy agents are postmen but do not have connections
        (unless (eql (name agent) (spy-name *omas*))
          ;; display list of connected sites, record list of site keys into 
          ;; (known-sites win)
          (make-connection-info agent win) 
          
          ;; refresh selected sites
          (pw-display-selected-site-on-click 
           win (omas::%find-named-item :connections win))
          )
        
        ;; post last result (not yet)
        ;; post subtasks (not yet)
        ;; post the last 20 received messages
        (setq message-log (input-log agent))
        (if (> (length message-log) 20) 
          (setq message-log (reverse (nthcdr 20 message-log)))
          (setq message-log (reverse message-log)))
        (omas::%set-item-sequence 
         (omas::%find-named-item :input-messages win)
         (mapcar #'omas::message-format message-log))
        ))))

;;;----------------------------------------------------------- MAKE-POSTMAN-WINDOW
; (make-postman-window xa-utc::xa-utc 2)

(defUn make-postman-window (agent position &aux win)
  "Creates a window for displaying an agent"
  (setq win
        (cr-window 
         :class 'OMAS-postman
         :type :single-edge-box
         :title (format nil "~S" (name agent))
         :left *aw-h-position* 
         :top (aw-compute-vpos position)
         :right (+ *aw-h-position* *aw-width*)
         :bottom (+ (aw-compute-vpos position) *aw-height*)
         :close-button nil
         :title-bar nil
         :background-color (omas::%rgb 64764 48059 51143) ; pink
         :dialog-items (make-postman-window-widgets agent)
         )
        )
  ;; record agent into the agent slot of the window
  (setf (agent win) agent)
  ;; record position
  (setf (rank win) position)
  ;; save agent window somewhere
  (setf (com-window agent) win)
  ;(format t "~&+++ make-postman-window /agent: ~S, win: ~S, com-window: ~S"
  ;  agent win (com-window agent))
  ;; add now list of connections to the display
  (make-connection-info agent win)
  ;; return instance id
  win)

#|
(make-postman-window xa-utc::xa-utc 2)
|#
;;;--------------------------------------------------- MAKE-POSTMAN-WINDOW-WIDGETS

(defUn make-postman-window-widgets (agent)
  "defines the different widgets and pane of a postman agent window."
  (let ((label-font(cg:make-font-ex nil "Lucida Sans Unicode / ANSI" 11 nil))
        (connection-font (cg:make-font-ex nil "courrier" 11 nil))
        )
    (list  
     ;; === first row: exit, ontology, trace, inspect
     ;; close box, agent name is associated with this button
     (cr-check-box
      :left 5 :top 6 :height 15
      :title (format nil "~A (~A)" (omas::key agent)(omas::site agent))
      :title-width 210
      :font label-font
      :on-change aw-close-on-click)
     
     ;;=== DETAILS
     (cr-button 
      :class 'OMAS-button
      :left (- *aw-width* (* 4 80) 5) :top 6 :width 75 :height 16
      :title (get-text *WAG-details*)
      :font label-font
      :on-click pw-details-on-click
      :name :details-button)
     
     ;;=== ONTOLOGY
     (cr-button 
      :class 'OMAS-button
      :left (- *aw-width* (* 3 80) 5) :top 6 :width 75 :height 16
      :title (get-text *WAG-ontology*)
      :font label-font
      :on-click aw-ontology-on-click
      :name :ontology-button)
     
     ;;=== TRACE
     (cr-button 
      :class 'OMAS-button
      :left (- *aw-width* (* 2 80) 5) :top 6 :width 75 :height 16
      :title (get-text *WAG-trace*)
      :font label-font
      :on-click aw-trace-on-click
      :name :trace-button)
     
     ;;=== INSPECT
     (cr-button 
      :class 'OMAS-button
      :left (- *aw-width* (* 1 80) 5) :top 6 :width 75 :height 16
      :title (get-text *WAG-inspect*)
      :font label-font
      :on-click aw-inspect-on-click
      :name :inspect-button)
     
     ;;=== second row: print current list of tasks
     (cr-static-text 
      :class 'omas-read-only-text-area
      :left 7 :top 27 :width (- *aw-width* 12) :height 12
      :title (aw-make-task-id-list agent)
      :font *aw-label-font*
      :color *white*
      :name :task-list
      )
     
     ;; === third row: input-messages and agenda           
     ;;=== INPUT MESSAGES
     (cr-multi-item-list 
      :class 'area-for-values
      :left 5 :top 45 :height 60 :cell-width *aw-display-width*
      :title (get-text *WAG-input-messages*)
      :cell-height 15
      ;:cell-color (AW-status-color agent)
      :font *aw-label-font*
      :name :input-messages 
      :sequence (mapcar #'omas::message-format (omas::input-messages agent))
      :hscrollp nil :vscrollp t)
     
     ;; === CONNECTIONS
     ;; posts the list of sites from the *known-sites-info* 
     ;;   <site name> <ON/OFF> <#messages sent> <#messages received>
     ;;   color green if connected (in *connected-sites-info*), red otherwise
     (cr-multi-item-list 
      :class 'area-for-values
      :left (+ *aw-display-width* 12) :top 45 :height 60 
      :cell-width (floor (* *aw-display-width* 2/3))
      :cell-height 15
      ;:cell-color (AW-status-color agent)
      :font connection-font
      :name :connections 
      :on-click pw-display-selected-site-on-click
      ;:sequence (make-connection-info agent)
      :hscrollp nil :vscrollp t)
     
     ;;=== SELECTED SITE
     (cr-static-text 
      :class 'omas-read-only-text-area
      :left (- *aw-width* 80 5) :top 45 :width 75 :height 16
      :title (get-text *WAG-no-selection*)
      :font *aw-label-font*
      :color *white*
      :name :selected-site
      )
     
     ;;=== CONNECT BUTTON
     (cr-button 
      :class 'OMAS-button
      :left (- *aw-width* 80 5) :top 65 :width 75 :height 16
      :title (get-text *WAG-connect*)
      :font label-font
      :on-click pw-connect-on-click
      :name :connect-button)
     
     ;;=== DISCONNECT BUTTON
     (cr-button 
      :class 'OMAS-button
      :left (- *aw-width* 80 5) :top 85 :width 75 :height 16
      :title (get-text *WAG-disconnect*)
      :font label-font
      :on-click pw-disconnect-on-click
      :name :disconnect-button)
     )))
	 
;;;===============================================================================
;;;                     Callback and service functions
;;;===============================================================================

;;;---------------------------------------------------------- MAKE-CONNECTION-INFO

(defUn make-connection-info (agent win)
  "make a list of strings <site> <ON/OFF> /<protocole>"
  (let (results postmen-to-display)
    ;; remove some connected postmen from the list of known postmen
    (setq postmen-to-display
          (remove-duplicates
           (append (CONNECTED-postmen-INFO agent)
                   (KNOWN-postmen-INFO agent))
           :test #'(lambda (xx yy)
                     (and (eql (car xx)(car yy))
                          (eql (postman-protocol xx) (postman-protocol yy))
                          (eql (postman-site xx)(postman-site yy))))
           :from-end t)) ; to keep connected postmen JPB121031
    ;; keep the list to recover entry on selection
    (setf (known-postmen win) postmen-to-display)
    
    ;; make list of info to display
    (dolist (postman-description postmen-to-display)
      (push
       (format nil "~&~A ~A /~A (~A)" 
         (car postman-description)
         (if (member postman-description 
                     (connected-postmen-info agent) :test #'equal) 
             (get-text *WAG-on*) 
           (get-text *WAG-off*))
         (postman-protocol postman-description)
		(postman-site postman-description)
         )
       results))
    ;(format t "~&+++ make-connection-info /results: ~S" results)
    
    ;; set sequence
    (%set-item-sequence (%find-named-item :connections win) (reverse results))
    ;; return list of connections
    :done))

;;;----------------------------------------------------------- PW-CONNECT-ON-CLICK

(defUn pw-connect-on-click (dialog widget)
  "sends a message to the postman to connect the specified site"
  (declare (ignore widget))
  (let (message agent-info rank)
    ;; find the rank of selected agent info
    (setq rank (%get-selected-cell-index (%find-named-item :connections dialog)))
    (unless rank
	   (%beep)
	   (return-from pw-connect-on-click))
	   
    ;;extract info
    (setq agent-info (nth rank (known-postmen dialog)))
    
    (unless agent-info (error "can't retrieve the postman description"))
    
    ;; send message wit a :connect skill
    (setq message (make-instance 'message
                    :type :sys-inform
                    :to (key (agent dialog)) ; postman
                    :action :connect
                    :args (list agent-info)))
    (format t "~&;+++ pw-connect-on-click /sending connect message:~&  ~S" message)
    (%send-message message t) ; t means message is local
    :done))

;;;-------------------------------------------------------- PW-DISCONNECT-ON-CLICK

(defUn pw-disconnect-on-click (dialog widget)
  "sends a message to the postman to connect the specified site"
  (declare (ignore widget))
  (let (message agent-info rank)
    ;; find the rank of selected agent info
    (setq rank (%get-selected-cell-index (%find-named-item :connections dialog)))
	(unless rank
	  (%beep)
	  (return-from pw-disconnect-on-click))
	  
    ;;extract info
    (setq agent-info (nth rank (known-postmen dialog)))
    
    (unless agent-info (error "can't retrieve the postman description"))

    ;; send message
    (setq message (make-instance 'message
                    :type :sys-inform
                    :to (key (agent dialog)) ; postman
                    :action :disconnect
                    :args (list agent-info)))
    (format t "~&;+++ pw-disconnect-on-click /sending connect message:~&  ~S" 
      message)
    (%send-message message t) ; t means message is local
    :done))

;;;--------------------------------------------- PW-DISPLAY-SELECTED-SITE-ON-CLICK

(defUn pw-display-selected-site-on-click (dialog widget)
  "on selection of a cell posts the content into the selection pane"
  ;(format t "~&+++ aw-display-selected.../selection: ~S" (%selection widget))
  ;(format t "~&+++ aw-display-selected.../index: ~S" 
  ;  (%get-selected-cell-index widget))
  ;(format t "~&+++ aw-display-selected.../sequence: ~S" (%get-item-sequence widget))
  (let ((index (%get-selected-cell-index widget))
        (pane (%find-named-item :selected-site dialog)))
    ;; reset color to white
    (%set-item-color (%find-named-item :connections dialog) (%rgb 65535 65535 65535))
    (%set-value pane (if index 
                         (car (nth index (known-postmen dialog)))
                       (get-text *WAG-no-selection*)))
    )
  t)

;;;--------------------------------------------------------------- PW-SHOW-FAILURE

(defUn pw-show-failure (agent)
  "paints the connection pane green briefly to indicate successful connection.
Argument:
   agent: current postman"
  (let (win pane)
    ;; get window
    (setq win (com-window agent))
    ;; quit if not there
    (unless win
      (return-from pw-show-failure))
    ;; get connection pane
    (setq pane (%find-named-item :connections win))
    ;; set color red
    (%set-item-color pane (%rgb 65535 0 0))
    ;; wait
    #+mcl (sleep 0.5) #-mcl (cg::sleep 1)
    ;;;    ;; reset color to white
    ;;;    (%set-item-color pane (%rgb 65535 65535 65535))
    :done))

;;;--------------------------------------------------------------- PW-SHOW-SUCCESS

(defUn pw-show-success (agent)
  "paints the selection pane green to indicate successful connection.
Argument:
   agent: current postman"
  (let (win pane)
    ;; get window
    (setq win (com-window agent))
    ;; change color only if window is defined
    (unless win
      (return-from pw-show-success))
    ;; get connection pane
    (setq pane (%find-named-item :connections win))
    ;; set color green
    (%set-item-color pane (%rgb 0 65535 0))
    ;; wait
    #+mcl (sleep 0.5) #-mcl (cg::sleep 1)
    ;;;    ;; reset color to white
    ;;;    (%set-item-color pane (%rgb 65535 65535 65535))
    :done))

(format t "~&;*** OMAS v~A - postman windows loaded ***" *omas-version-number*)

;;; :EOF