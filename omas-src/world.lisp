﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;                O M A S - W O R L D  (file world.lisp)
;;;
;;;===============================================================================
;;; This file contains the definitions and methods associated with the class WORLD
;;; that models the world for a given agent.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#
#| 
2020
 0202 restructuring OMAS as an asdf system
|#

#| the world part of an agent is structured as:
world
   acquaintances           ; other known agents
   environment             ; (obsolete: replaced by data)
   external-services       ; knowledge of other agents
|# 

(in-package :omas)

;;; format of an entry on the list of acquaintances

(defClass AGENT-INFO ()
  ((agent :accessor agent :initarg :agent) ; the entry is a keyword
   (skills-requested :accessor skills-requested :initarg :skills-requested
                     :initform nil)
   (skills-displayed :accessor skills-displayed :initarg :skills-displayed
                     :initform nil)
   (date-of-first-encounter :accessor date-of-first-encounter
                            :initarg :date-of-first-encounter))
  (:documentation "entry describing another agent. It contains the agent name the ~
                               skills we could get from the messages (either requested or ~
                               displayed, and the time the agent was detected for the first ~
                               time."))

(defMethod print-object ((mm agent-info) stream)
  (format stream "#<AGENT-INFO ~A ~A ~A ~A>"
          (agent mm)
          (skills-requested mm)
          (skills-displayed mm)
          (date-of-first-encounter mm)))

;;;----------------------------------------- AGENT-PARSE-MESSAGE-FOR-ACQUAINTANCES

(defun agent-parse-message-for-acquaintances (agent message)
  "function called when an agent starts processing a message. Checks if the agent ~
      sending the message is known, if not, then records its id and current date, ~
      otherwise, check the skill and record it if not already known. For each agent ~
      we record the skill in one of two lists: skill displayed, or skill requested, ~
      according to the type of message. If the skill is a new information, we record ~
      it in the external services list.
Arguments:
   agent: agent
   message: message to be processed."
  (let ((sender (from! message)))
    ;; if message comes from user or the sender is nil, don't record anything
    (when (or (null sender) 
              (eql sender :<user>) ;  normally replaced by next line
              (eql sender (local-user *omas*)))
      (return-from agent-parse-message-for-acquaintances nil))
    ;; otherwise we check for the agent identity
    (let* ((acquaintances (acquaintances (world agent)))
           (agent-info (car (member-if 
                             #'(lambda(xx) (eql sender (agent xx))) acquaintances))))
      ;; check if the agent was already modeled
      (when agent-info
        ;; if so check if the skill was recorded according to the type of message
        (case (type! message)
          ;; messages asking for skills
          ((:request :call-for-bids :grant :cancel-grant)   
           (unless (member (action message) (skills-requested agent-info))
             (setf (skills-requested agent-info)
                   (append (skills-requested agent-info) (list (action message))))))
          ;; messaged displaying some used or available skill
          ((:answer :bid :bid-with-answer)
           (unless (member (action message) (skills-requested agent-info))
             (setf (skills-displayed agent-info)
                   (add-item-if-new (action message) 
                                    (skills-displayed agent-info)))
             (agent-trace agent "adding info to record: ~S" (from! message))
             ))))
      ;; if there is no entry, then create one
      (unless agent-info
        (case (type! message)
          ((:request :call-for-bids :grant :cancel-grant)           
           (setq agent-info
                 (make-instance 'AGENT-INFO
                   :agent sender
                   :date-of-first-encounter (get-universal-time)
                   :skills-requested (list (action message))))
           )
          ((:answer :bid :bid-with-answer)
           (setq agent-info
                 (make-instance 'AGENT-INFO
                   :agent sender
                   :date-of-first-encounter (get-universal-time)
                   :skills-displayed (list (action message))))
           ;; also note that the from agent is capable of providing the service
           ;; attached to the skill
           (agent-update-external-services agent message)))
        ;; add new info on the agent's list when we do have info
        (when agent-info
          (setf (acquaintances agent)
                (append (acquaintances agent) (list agent-info)))
          (agent-trace agent "recording sender of message: ~S" (from! message)))
        )           
      ;; return the agent-info object
      agent-info)))

;(trace (agent-parse-message-for-acquaintances :step t))
;(untrace agent-parse-message-for-acquaintances)

;;;----------------------------------------------------- AGENT-RESET-ACQUAINTANCES

(defun agent-reset-acquaintances (agent)
  "remove the description of other agents from the acquaintances slot of the world ~
      model of an agent. Also resets external services.
Arguments:
   agent: agent."
  (setf (acquaintances (world agent)) nil)
  (setf (external-services (world agent)) nil))

;(mapc #'agent-reset-acquaintances *agents*)

;;;------------------------------------------------ AGENT-UPDATE-EXTERNAL-SERVICES

(defun agent-update-external-services (agent message)
  "record the skill into the list of external services. An entry on the list of ~
      external services looks like (<skill> :agent1... :agentn).
Arguments:
   agent: agent that received the message
   message: message containing the skill and provider to be recorded."
  (let ((skill-list (external-services (world agent)))
        (skill (action message)))
    (setf (external-services (world agent))
          (%alist-add-value skill-list skill (from! message)))))

;(trace (agent-update-external-services :step t))
;(untrace agent-update-external-services)

(format t "~&;*** OMAS v~A - world loaded ***" *omas-version-number*) 

;;; :EOF