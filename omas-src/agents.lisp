;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;24/10/18
;;;                O M A S - A G E N T S  (file omas-agents.lisp)
;;;
;;; first created 2000
;;;===============================================================================
;;; (c) Jean-Paul Barthès@HEUDIASYC, CNRS, UTC. See LICENSE file for permissions.

#|
 This file contains the structure and service methods for OMAS agents, as well 
 as macros for creating agents, proxies, and skills. 
 OMAS means Open System of Asynchronous Agents. Basically
 OMAS and SMAS use agents with the same structure.  
 In the OMAS version an agent when created (after a defagent) has two processes:
    - one for running the skills that is started and put into a wait state
    - another one, the scanner, for processing the input-messages
 During its life more processes can be created
    - for processing call-for-bids
    - for processing inform messages
    - for handling its master in case of an assistant agent
 When a message is received, the scanner processes it. I.e., it decides 
    - whether it should be ignored (depending on the state of the agent),
    - whether it should interrupt what the agent is doing,
    - whether it should be inserted into the list of tasks to do (agenda),
    - whether it should create a special process to process it.
 The agent process processes task it finds in the agenda until completion, 
 unless it is interrupted by the scanner (:abort, :cancel, :kill).

History
-------
2024
 1018 adapting to Windows 10
2020
 0202 restructuring OMAS as an asdf system
|#

(in-package :omas)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (omas::omas-trace-add :make-agent "traces the function creating an agent")
  (omas::omas-trace-add :skill-header "traces each skill invocation")
  )

;;;============================= macros ==========================================
;;; macro for defining set methods (local to this file)

(defMacro def-ag-set (prop sub-object-name)
  `(defMethod (setf ,prop) (val (self agent) )
     (let ((item (,sub-object-name self)))
       (if item
         (setf (,prop item) val)
         (error ,(concatenate 'string "non existing "
                              (symbol-name sub-object-name)
                              " part in agent ~S") (name self)))
       val)))

;;; same with no check

(defMacro def-ag-set-no-check (prop sub-object-name)
  `(defMethod (setf ,prop) (val (self agent) )
     (setf (,prop (,sub-object-name self)) val)
     ))

#|
;;; Example of use: (def-ag-set output-messages comm)
;;; should produce

(defmethod (setf output-messages) (val (self agent) )
  (let ((item (comm self)))
    (if item
        (setf (output-messages item) val)
      (error "non existing COMM part in agent ~S" (name self))))
  val)
|#

;;; macro to extend the method applying to a subpart of an agent, so that it can
;;; be applied to the agent as a whole. Includes the definition for defsetf

(defMacro extend-method (method subobject)
  `(progn
     (defMethod ,method ((self agent))
       (,method (,subobject self)))
     (def-ag-set-no-check ,method ,subobject)))

;;;========================== agent structure ====================================

#| an agent has the following structure, implementes as sub-objects
=== direct slots
   assistant               ; if T agent is assistant (obsolete, test sub-type)
   gate                    ; sychronizing object
   key                     ; keyword corresponding to its name   
   master-if-staff         ; list of masters (keyword) for a staff service agent
   mbox-process            ; process attached to the input-messages mailbox
   name                    ; qualifies the agent (external name) e.g. SA_ADDRESS
   process                 ; process running the agent
   skills                  ; list of skill objects
   traced                  ; t if agent must be traced, nil otherwise

=== sub-objects
appearance
   window                  ; handle to agent (pink) window
   com-window              ; communication channel for a PA
   private-interface       ; user defind interaction window
   thread                  ; unused
   h-pos                   ; horizontal position of lifeline in graphics window
   v-pos                   ; vertical position of lifeline in graphics window
comm
   input-messages          ; list of input-messages
   delayed-input           ; list, used for time-out messages
   output-messages         ; list of output messages
   delayed-output          ; list or results
   displays                ; list of attached windows
   input-log               ; log of all input messages
   output-log              ; log of all input messages
   internal-ip             ; for postman definition
control
   active-task-list        ; list of active tasks (each task in its own thread)
   agenda                  ; list of messages (waiting tasks)
   canceled-task-info      ;
   editing                 ; if true, creating or editing objects
   new-objects             ; when editing saves newly created objects
   pending-bids            ; active bids still pending
                           ; used by the CNet protocol
   process-list           ; list of processes except for scan, min, and assistant
   saved-answers           ; keeps the unprocessed received answers
   saved-changes           ; we store objects prior to modifying them to be able to
                           ; make an UNDO if needed
   status                  ; :idle
   task-in-progress        ; message corresponding to the task being executed
                           ; unused since each task has its own thread
ontologies
   agent-ontology          ; internal ontology for agent mechanisms
   base                    ; ontology database
   dialog-ontology         ; ? unused
   domain-ontology         ; e.g. addresses
   edit-layouts            ; set of descriptions of page layout for web editing
   edit-message-list       ; keeps editing comments to print at the end
   edit-program            ; program used to modify or create an object
   edited-object           ; id of object being created or modified (nb if temporary)
   edit-to-create          ; used by the editor
   edit-to-unlink          ; used by the editor
   language                ; ontology language (e.g. :en :fr or :all)
   master-ontology         ; ? unused
   moss-context            ; integer referring to MOSS version of the ontologies
   moss-system             ; contains a reference to the value of the special
                           ; *moss-system* special variable (usually $E-SYSTEM.1)
   moss-version-graph      ; version (configuration) graph  
   ontology                ; ? unused
   ontology-file           ; user defined unusual ontology file (e.g. shared file)
   ontology-package        ; agent package (for ontology symbols)
   ontology-window         ; window for displaying ontology
   temp-id-list            ; a list containing temporary object descriptions during
                           ; an editing session
   to-save                 ; contains a list of objects to be saved during the
                           ; next transaction
   ;update-fcn              ; special update function for dealing directly with the
                           ; changes (handle to the OMAS Web Editor). Used when th
                           ; structure of the object is very different from the 
                           ; fields posted in the Web page
self
   data                    ; area containing data (user's structured)
   features                ; features (e.g., :learning)
   goals                   ; long-term goals of the agent
   intentions              ; for implementing BDI agent?
   memory                  ; contains what the agent has learned so far
   moss-ref                ; handle to MOSS representation of agent
   ontology                ; ? unused
   persistency             ; if t agent is persistent
;skills
;   skills                  ; list of functions to be applied
;   rule-sets               ; list of rule sets to be applied
tasks
   projects                ; description of complex tasks (unused yet)
   waiting-tasks           ; description of task being processed (learning agents)
world
   acquaintances           ; other known agents
   environment             ; (obsolete: replaced by data in self sub-object)
   external-services       ; knowledge of other agents

=== specific to PAs
master-bins (implemented in the assistant file)
   answer                  ; answer to a send-request message
   answer-timeout          ; timeout to avoid waiting indefinitely
   answer-to-task-to-do    ; ?
   conversation            ; current conversation
   conversation-entry      ; unused (stored in the conversation object)
   conversation-context    ; unused (stored in the conversation object)
   conversation-state      ; unused (stored in the conversation object)
   details                 ; window to trace conversation
   dialog-file             ; user-defined unusual file (e.g. shared)
   dialog-header           ; start of the initial dialog
   dialog-history          ; unused
   discarded-messages      ; messages discarded by the assistant (scanner)
   error-contents          ; ?
   no-window               ; if t, indicates that the PA has no window
   output-font-size        ; size of font in the dialog window
   output-font-string      ; type of font
   pending-requests        ; list of requests waiting to be processed
   pending-task-id         ; unused?
   salient-features        ; stack of salient features
   salient-feature-max-length ; max length of the stack
   salient-feature-max-time   ; max time for keeping features
   show-dialog             ; deprecated
   task-file               ; user-defined unusual task file (e.g. shared)
   to-do                   ; input string from the master
   voice-input-port        ; port for voice input channel
   voice-input-socket      ; socket for voise input channel
   voice-io                ; if t, then voice is enabled
   voice-ip                ; ip of the machine hosting the voice software
   voice-max-message-length   ; max length of voice string messages
   voice-output-port       ; output port of the voice channel
   voice-output-socket     ; output socket of the voice channel
   voice-receiving process ; voice process
   waiting-answers         ; answers to be examined
   waiting-messages        ; messages to be processed by the master

=== specific to XAs
transfer (implemented in the POSTMAN file)
   destination             ; destination ip
   port                    ; transfer port
   transfer-protocol       ; tranfer protocol (default TCP)
   connected-postmen-info  ; list of info for connected postmen
                           ; e.g. ((:CIT <CIT name> <CIT-IP> :external)...)
   connection-type         ; type of connection     
   external-IP             ; IP to be seen from outside of the firewall
   external-name           ; external name of the OMAS server
   http                    ; true if http protocol is requested
   http-port               ; port for receiving (should be 80)
   ids-of-received-messages; list for keeping track of received messages to discard
                           ; messages coming in a second time through a different
                           ; path
   internal-name           ; internal name of the machine
   irm-max-size            ; max size of the queue of ids of received messages 
                           ; (default 100)
   know-postmen-info       ; list of site entries (triples),
                           ;    e.g. ((:utc "195.83.154.22" "extern")...)
   proxy                   ; proxy is needed when sending behing firewall using HTTP
   receiving-process       ; persistent process for receiving messages from ooutside
   receiving-socket        ; socket for receiving external messages
   receiving-function      ; handle for user-provided functions
   send-socket             ; socket for sending messages (volatile)
   site                    ; local site is a key identifying the site: e.g. :UTC
                           ; when two postmen are from the same site, no proxies
                           ; are used for HTTP
   site-counter            ; ? counter for foreign sites
   site-user               ; user is control panel
   status                  ; if :active means that the server is connected
   tcp-port                ; tcp-port for receiving (should be 52008)

skill are organized as follows:
   acknowledge-fcn         ; user defined handler to process returned acknowledgments
   bid-cost-fcn            ; user defined function for computing the cost of a bid
   bid-quality-fcn         ; user defined function for computing the quality of a bid
   bid-start-time-fcn      ; user define function for computing earliest start time
   description             ; description of the skill
   dynamic-fcn             ; handler for processing subtask answers
   how-long-fcn            ; user defined function returning time needed to compute
   how-long-left-fcn       ; user defined function returning time left to completion
   name                    ; name of the skill
   preconditions           ; user defined function to chack arguments
   select-best-answer-fcn  ; user defined function to select contract-net answers
   select-bid-fcn          ; user defined function to select contract-net bids
   static-fcn              ; function executed when activating the skill for the 
                           ; first time
   time-limit-fcn          ; handler for processing time limit interrupts
   timeout-handler         ; handler for processing timeouts

|#

(defClass appearance ()
  ((window :accessor window :initform nil)
   (com-window :accessor com-window :initform nil)
   (private-interface :accessor private-interface :initform nil)
   (thread :accessor thread :initform nil)
   (h-pos :accessor h-pos :initform 0)
   (v-pos :accessor v-pos :initform 0))
  (:documentation "an agent may appear in a window and be graphically represented ~
                   as a color thread"))

(defClass comm ()
  ((input-messages :accessor input-messages :initform nil :type list)
   (delayed-input :accessor delayed-input :initform nil :type list)
   (output-messages :accessor output-messages :initform nil :type list)
   (delayed-output :accessor delayed-output :initform nil :type list)
   (displays :accessor displays :initform nil :type list)
   (input-log :accessor input-log :initform nil :type list)
   ;; initarg is used for postman definition
   (internal-ip :accessor internal-ip :initform nil :initarg :internal-ip)
   (output-log :accessor output-log :initform nil :type list))
  (:documentation "communication part of an agent structure"))

(defClass control ()
  ((agenda :accessor agenda :initform nil)
   (active-task-list :accessor active-task-list :initform nil :type list)
   (canceled-task-info :accessor canceled-task-info :initform nil)
   (editing :accessor editing :initform nil)
   ;(last-subtask-result :accessor last-subtask-result :initform nil)
   (new-objects :accessor new-objects :initform nil)
   (pending-bids :accessor pending-bids :initform nil :type list)
   (processs-list :accessor process-list :initform nil)
   ;(saved-answers :accessor saved-answers :type message :initform nil)) ;JPB0805
   (saved-answers :accessor saved-answers :initform nil)
   (saved-changes :accessor saved-changes :initform nil)
   (status :accessor status :initform :idle)
   (task-in-progress :accessor task-in-progress :initform nil)
   )
  (:documentation "control structure of an agent"))

(defClass ontologies ()
  ((ontology :accessor ontology :initform nil)
   (agent-ontology :accessor agent-ontology :initform nil)
   (base :accessor base :initform nil)
   (domain-ontology :accessor domain-ontology :initform nil)
   (dialog-ontology :accessor dialog-ontology :initform nil)
   (edit-layouts :accessor edit-layouts :initform nil)
   (edit-message-list :accessor edit-message-list :initform nil)
   (edited-object :accessor edited-object :initform nil)
   (edit-to-create :accessor edit-to-create :initform nil)
   (edit-to-unlink :accessor edit-to-unlink :initform nil)
   (language :accessor language :initform :en)
   (last-selection :accessor last-selection :initform nil)
   (master-ontology :accessor master-ontology :initform nil)
   (moss-context :accessor moss-context :initform 0)
   (moss-system :accessor moss-system :initform nil)
   (moss-version-graph :accessor moss-version-graph :initform '((0)))
   (ontology-file :accessor ontology-file :initform nil)
   (ontology-package :accessor ontology-package :initform :omas)
   (ontology-window :accessor ontology-window :initform nil)
   (temp-id-list :accessor temp-id-list :initform nil) ; unused
   (to-save :accessor to-save :initform nil)
   )
  (:documentation "set of agent ontologies"))

(defClass self ()
  ((data :accessor data :initform nil)
   (features :accessor features :initform nil)
   (goals :type list :accessor goals :initform nil)
   (intentions :accessor intentions :initform nil)
   (memory :accessor memory :initform nil)
   (moss-ref :accessor moss-ref :initform nil)
   (ontology :accessor ontology :initform nil)
   (persistency :accessor persistency :initform nil))
  (:documentation "part of the inner self structure of an agent"))

(defClass skill ()
  ((name :accessor name :initarg :name)
   (acknowledge-fcn :accessor acknowledge-fcn :initform nil :initarg :acknowledge-fcn)
   (bid-cost-fcn :accessor bid-cost-fcn :initform nil :initarg :bid-cost-fcn)
   (bid-quality-fcn :accessor bid-quality-fcn :initform nil :initarg :bid-quality-fcn)
   (bid-start-time-fcn :accessor bid-start-time-fcn :initform nil 
                       :initarg :bid-start-time-fcn)
   (description :accessor description :initform nil :initarg :description)
   (dynamic-fcn :accessor dynamic-fcn :initform nil :initarg :dynamic-fcn)
   (static-pattern :accessor static-pattern :initform nil :initarg :static-pattern)
   (how-long-fcn :accessor how-long-fcn :initform nil :initarg :how-long-fcn)
   (how-long-left-fcn :accessor how-long-left-fcn :initform nil 
                      :initarg :how-long-left-fcn)
   (preconditions :accessor preconditions :initform nil :initarg :preconditions)
   (select-best-answer-fcn :accessor select-best-answer-fcn :initform nil
                           :initarg :select-best-answer-fcn)
   (select-bids-fcn :accessor select-bids-fcn :initform nil :initarg :select-bids-fcn)
   
   (static-fcn :accessor static-fcn :initform nil :initarg :static-fcn)
   (time-limit-fcn :accessor time-limit-fcn :initform nil :initarg :time-limit-fcn)
   (timeout-handler :accessor timeout-handler :initform nil :initarg :timeout-handler)
   )
  (:documentation "object describing an agents's particular skill"))

(defMethod print-object ((mm skill) stream)
  (format stream "#<SKILL ~A S:~A D:~A~A~A~A~A~A~A>"
          (name mm)
          (static-fcn mm)
          (dynamic-fcn mm)
          (if (how-long-fcn mm)(format nil " HL:~S" (how-long-fcn mm)) "")
          (if (how-long-left-fcn mm)(format nil " HLL:~S" 
                                            (how-long-left-fcn mm)) "")
          (if (time-limit-fcn mm)(format nil " TL:~S" (time-limit-fcn mm)) "")
          (if (timeout-handler mm)(format nil " TO:~S" (timeout-handler mm)) "")
          (if (preconditions mm)(format nil " PC:~S" (preconditions mm)) "")
          (if (acknowledge-fcn mm) " ACK" "")
          ))

(defClass subtask-info ()
  ((id :accessor id :initform nil)
   (to :accessor to! :initform nil)
   (start-time :accessor start-time :initform nil)
   (timeout :accessor timeout :initform nil)
   (repeat-count :accessor repeat-count :initform 3)
   (timeout-process :accessor timeout-process :initform nil)
   (time-limit-process :accessor time-limit-process :initform nil)
   (quality :accessor quality :initform nil)
   (duration :accessor duration :initform 100000000)
   (saved-answers :accessor saved-answers :initform nil))
  (:documentation "short description of a pending task used in control"))

(defMethod print-object ((mm subtask-info) stream)
  (format stream "#<SUBTASK-INFO ~A ~A ~A ~A ~A ~A bids:~A>"
          (id mm)(to! mm)(start-time mm)
          (or (timeout mm) "no-TO")
          (or (repeat-count mm) "no-RC")
          (if (duration mm) (format nil "duration:~S" (duration mm)) "")
          (length (saved-answers mm))))

(defClass task-info ()
  ((id :accessor id :initform nil)
   (process :accessor process :initform nil)
   (message :accessor message :initform nil)
   (time-limit-process :accessor time-limit-process :initform nil)
   (subtask-list :accessor subtask-list :initform nil))
  (:documentation "structure that describe a running task"))

(defMethod print-object ((mm task-info) stream)
  (format stream "#<TASK-INFO ~A ~A ~A ST: ~A>"
          (id mm)
          (process mm)
          (message mm)
          (format nil "~{~A ~}" (subtask-list mm))))

(defClass tasks ()
  ((projects :accessor projects :initform nil)
   (waiting-tasks :accessor waiting-tasks :initform nil))
  (:documentation "model of the tasks and projects in the world"))

(defClass world ()
  ((acquaintances :accessor acquaintances :initform nil :type list)
   (environment :accessor environment :type list :initform nil)
   (external-services :accessor external-services :initform nil))
  (:documentation "world representation structure of an agent"))

(defClass abilities ()
  ;((skills :accessor skills :initform nil :type skill)) ; JPB0805
  ((skills :accessor skills :initform nil :type list))
  (:documentation "agents's substructure for storing skills"))

;;;------------------------------------------------------------------------- AGENT

(defClass agent ()
  ((name :accessor name :initarg :name)
   (key :accessor key :initarg :key)
   (master-if-staff :accessor master :initarg :master :initform nil)
   (comm :accessor comm :type COMM :initarg :comm)
   (self :accessor self :type SELF :initarg :self)
   (ontologies :accessor ontologies :type ONTOLOGIES :initarg :ontologies)
   (world :accessor world :type WORLD :initarg :world)
   (control :accessor control :type CONTROL :initarg :control)
   (tasks :accessor tasks :type TASKS :initarg :tasks)
   (skills :accessor skills :type list :initarg :skills :initform nil)
   (appearance :accessor appearance :type APPEARANCE :initarg :appearance)
   ;(master-bins :accessor master-bins :type MASTER-BINS 
   ;             :initform (make-instance 'MASTER-BINS))
   (process :accessor process :initarg :process :initform nil)
   (mbox-process :accessor mbox-process :initarg :mbox-process :initform nil)
   (assistant :accessor assistant :initform nil) ; use for voice input
   ;; synchronizing gate when using HTTP server
   (gate :accessor gate :initform nil)
   ;(assistant-process :accessor assistant-process :initarg :assistant-process 
   ;                   :initform nil)
   (traced :accessor traced :initarg :traced :initform nil))
  (:documentation "an agent static structure"))

(defMethod print-object ((agent agent) stream)
  (format stream "#<AGENT ~A>" (name agent)))


;;; missing ontologies somewhere
(defMethod agent-clone ((ag agent))
  (let* ((ag1 (make-instance 'agent))
         (comm (make-instance 'comm))
         (control (make-instance 'control))
         (appearance (make-instance 'appearance)))
    (setf (input-messages comm) (input-messages ag))
    (setf (comm ag1) comm)
    
    (setf (status control) (status ag))
    (setf (agenda control) (agenda ag))
    (setf (task-in-progress control) (task-in-progress ag))
    (setf (control ag1) control)
    (setf (name ag1) (name ag))
    
    (setf (window appearance) (window ag))
    (setf (appearance ag1) appearance)
    ag1))

;;; Extending methods from parts of an agent to apply to the agent as a whole.
;(setf (input-messages mul-1:mul-1) '(33))
;--- APPEARANCE
(extend-method window appearance)
(extend-method com-window appearance)
(extend-method private-interface appearance)
(extend-method thread appearance)
(extend-method h-pos appearance)
(extend-method v-pos appearance)
;--- COMM
(extend-method displays comm)
(extend-method input-messages comm)
(extend-method input-log comm)
(extend-method internal-ip comm)
(extend-method output-log comm)
;--- CONTROL
(extend-method active-task-list control)
(extend-method agenda control)
(extend-method canceled-task-info control)
(extend-method editing control)
(extend-method new-objects control)
(extend-method pending-bids control)
(extend-method process-list control)
(extend-method saved-answers control)
(extend-method saved-changes control)
(extend-method status control)
(extend-method task-in-progress control)
;--- ONTOLOGIES
(extend-method agent-ontology ontologies)
(extend-method base ontologies)
(extend-method dialog-ontology ontologies)
(extend-method domain-ontology ontologies)
(extend-method edit-layouts ontologies)
(extend-method edit-message-list ontologies)
(extend-method edit-to-create ontologies)
(extend-method edit-to-unlink ontologies)
(extend-method edited-object ontologies)
(extend-method language ontologies)
(extend-method last-selection ontologies)
(extend-method master-ontology ontologies)
(extend-method moss-context ontologies)
(extend-method moss-system ontologies)
(extend-method moss-version-graph ontologies)
(extend-method ontology ontologies)
(extend-method ontology-file ontologies)
(extend-method ontology-package ontologies)
(extend-method ontology-window ontologies)
(extend-method temp-id-list ontologies)
(extend-method to-save ontologies)

;--- SELF
(extend-method data self)
(extend-method goals self)
(extend-method features self)
(extend-method memory self)
(extend-method moss-ref self)
(extend-method intentions self)
(extend-method persistency self)
;--- TASKS
(extend-method projects tasks)
(extend-method waiting-tasks tasks)
;--- WORLD
(extend-method acquaintances world)
(extend-method environment world)
(extend-method external-services world)

;;;======================== service functions ===================================

;;;------------------------------------------------------------------- keywordize

(defUn keywordize (agent-ref)
  "agent ref may be a keyword (:all :fac :<user> ...), an agent structure (i.e. a ~
   symbol with type agent), a symbol naming an agent structure (e.g. 'MUL-1) ~
   or a list of such things. In all cases the functon ~
   replaces the agent-id with the corresponding keyword, obtained from the key ~
   slot of the agent structure. if agent-ref is a symbol and its value is not an ~
   agent structure, signals an error.
Arguments:
   agent-ref: reference (:all, :fac, 'FAC '(:mul-1 MUL-2)) nil
Return:
   :ALL, :FAC, :FAC, (:MUL-1 :MUL-2) NIL"
  
  (cond
   ((keywordp agent-ref) agent-ref)
   ((stringp agent-ref) (intern (string-upcase agent-ref) :keyword)) ; JPB1603
   ((typep agent-ref 'agent) (key agent-ref))
   ((and (symbolp agent-ref)
         (boundp agent-ref)
         (typep (symbol-value agent-ref) 'agent))
    (key (symbol-value agent-ref)))
   ((and (symbolp agent-ref)(boundp agent-ref)(null agent-ref)) nil)
   ((symbolp agent-ref)
    (warn "~S is not an agent name in package ~S" agent-ref *package*)
    (intern (symbol-name agent-ref) :keyword))
   ((listp agent-ref) (mapcar #'keywordize agent-ref))
   (t (error "illegal value for agent-ref: ~S" agent-ref))))

#|
(keywordize albert::pa_albert)
:ALBERT

(keywordize '(albert::pa_albert albert::pa_albert))
(:ALBERT :ALBERT)

(keywordize 'pa_albert)
:PA_ALBERT

(keywordize "Albert")
:ALBERT
:EXTERNAL

(keywordize '("Alvert" john :fred))
(:ALVERT :JOHN :FRED)
|#
;;;========================= agent functions ====================================
;;; the following functions are low-level functions dealing with the agent 
;;; STRUCTURE as composed of several subobjects.
;;; they should make the structure (nearly) transparent in the agent code

;;;------------------------------------------------------------------- %AGENT-ADD

(defMacro %agent-add (agent value prop)
  "internal macro that adds a new value to the values associated with a property. ~
   users should use the agent-add function.
Ex: (%agent-add mul-1 mes-3 :input-messages)."
  (let ((prop-name (intern (symbol-name prop) :omas)))
    `(setf (,prop-name ,agent)
           (append (,prop-name ,agent) (list ,value)))))

;;;-------------------------------------------------------------------- AGENT-ADD

(defMethod agent-add ((agent agent) value prop)
  "method using the %agent-add macro. Necessary because the macro requires the ~
   actual name of the property to function correctly, and cannot use a variable ~
   whose value is this name.
Example:
   (agent-add mul-1 new-info :environment)
Arguments:
   agent: agent
   value: value to be added to the list of values associated with prop
   prop: keyword giving property name (will be transformed into the right symbol)."
  (eval `(%agent-add ',agent ',value ,prop)))

;;;----------------------------------------------------- AGENT-ADD-DEFAULT-SKILLS
;;; JPB 051001

(defUn agent-add-default-skills (agent)
  "function adding default skills to a given agent to install a minimal behavior. ~
   Currently only the possibility to send messages to other agents (used by PAs).
Arguments:
   agent: agent id or keyword
Return:
   :done"

  ;; adding a generic greeting function
  (make-skill :hello agent
              :static-fcn 'static-hello)
  ;; and a find skill JPB1110
  (make-skill :find agent
              :static-fcn 'static-find)
  ;; get the page layout description (web editor)
  (make-skill :get-edit-page-layout agent
              :static-fcn 'static-get-edit-page-layout)
  ;; get an object from its id-ref (web editor)
  (make-skill :get-object-from-id agent
              :static-fcn 'static-get-object-from-id)
  ;; locate an object (web editor)
  (make-skill :locate-object agent
              :static-fcn 'static-locate-object)
  ;; default edit handler skill (does nothing)
  (make-skill :modify-object agent
              :static-fcn 'static-modify-object)
  ;; and a ping skill
  (make-skill :ping agent
              :static-fcn 'static-ping)
  ;; the send-request skill is used by the PA to send messages to its staff or
  ;; other agents. It must have a fairly complete set of options since the
  ;; message could be point to point or broadcast, and strategy could be
  ;; take-first-answer or :collect-answers. The collect answer waits for some 
  ;; time and returns all the messages in a list.
  (make-skill :send-message agent
              :static-fcn 'send-message-static
              :dynamic-fcn 'send-message-dynamic
              :timeout-handler 'send-message-timeout
              ;:select-best-answer-fcn 'send-message-select-best-answer ; JPB1005
              )

  ;; start the editing process (web editor)
  (make-skill :start-editing agent
              :static-fcn 'static-start-editing)
  ;; update an object (web editor)
  (make-skill :update-object agent
              :static-fcn 'static-update-object)
  )

;;;---------------------------------------------------- AGENT-CREATE-MOSS-CLASSES
;;; executed in the agent package. MOSS stub must have been created.

;;; creates the following objects:
;;;   - an orphan representing the agent
;;;   - $E-OMAS-SKILL a class for agent skills
;;;   - $E-OMAS-GOAL a class for agent goals
;;; objects are marked as :no-save and :dont-save-instances. However, properties
;;; like ag-name, sk-name, gl-name, sk-doc, gl-doc will be saved and recreated each
;;; time the agent is loaded, which should be all-right.

(defMethod agent-create-moss-classes ((agent agent))
  "function to create moss classes for a specific agent. Done first time the agent ~
   is loaded, or when the agent persistent partition has been zapped.
Arguments:
   agent: agent object
Return:
   agent-id"
  (proclaim (list 'special (intern "*MOSS-SYSTEM*")))
  (let (id msg)
    ;; if package is not the agent package (key agent) and omas-skill already
    ;; exist, we are probably creating agents in the cg-user or cl-user package
    ;; do not try to recreate agent models
    (when (or (equal (find-package (key agent)) *package*)
              ;; for the first time around
              (not (moss::%pdm? (intern "$E-OMAS-AGENT"))))
      (setq msg 
            (catch 
             :error
             ;===== specific class for SKILLS
             (setq id (moss::%make-concept "OMAS-SKILL" 
                                           '(:att "sk-name")'(:att "sk-doc")))
             (definstmethod "=summary" "OMAS-SKILL" () 
               (send *self* '=get "SK-NAME"))
             (setf (get id :dont-save-instances) t)
             ;; tell the counter to save with a reset value
             (setf (get (moss::%make-id-for-counter id) :reset) t)
             
             ;;===== specific class for GOALS
             (setq id (moss::%make-concept "OMAS-GOAL" 
                                           '(:att "gl-name" )'(:att "gl-doc")))
             (definstmethod "=summary" "OMAS-GOAL" () 
               (send *self* '=get "gl-name"))
             (setf (get id :dont-save-instances) t)
             ;; tell the counter to save with a reset value
             (setf (get (moss::%make-id-for-counter id) :reset) t)
             
             ;===== specific class for AGENTS
             (setq id (moss::%make-concept "OMAS-AGENT" 
                                           '(:att "ag-name" (:entry))
                                           '(:att "ag-doc")
                                           '(:att "ag-key")
                                           '(:att "ag-language")
                                           '(:rel "ag-skill" (:to "omas-skill"))
                                           '(:rel "ag-goal" (:to "omas-goal"))))
             ;(format t "~%; agent-create-moss-classes / OMAS-AGENT: ~S" (eval id))
             (definstmethod "=summary" "OMAS-AGENT" () 
               (send *self* '=get "ag-name"))
             (setf (get id :dont-save-instances) t)    
             ;; tell the counter to save itself with a reset value
             (setf (get (moss::%make-id-for-counter id) :reset) t)
             ))
      (if (stringp msg)(warn msg))
      )
    
    ;; make a new instance in all cases (done in make-agent JPB1011
    ;; check language
    (dformat :make-agent 1 "~%;+++++ agent-create-moss-classes / language: ~S" (language agent))
    ;; adding language JPB1404
    (setq id (moss::%make-individual "omas-agent" 
                                     `("ag-name" ,(symbol-name (key agent)))
                                     `("ag-key" ,(key agent))
                                     `("ag language" ,(or (language agent) :EN))))
    (dformat :make-agent 0 "~%; agent-create-moss-classes / OMAS-AGENT instance: ~S" (eval id))
    ;; save id into moss-ref slot of the agent self description
    (setf (moss-ref agent) id)
    
    ;; return OMAS agent id
    agent
    ))

;;;---------------------------------------------------------------- AGENT-DESTROY
;;; used in the infere.lisp file

(defMethod agent-destroy ((agent agent))
  "kill an agent, removing all processes and making its name unbound.
  Can be used locally while debugging, or by Manager.
Arguments:
   agent: agent-id
Return:
   :done"
  (declare (special *OMAS-agent-window-slots*))
  ;; kill all processes, when they are referenced
  (when (mbox-process agent)
    (agent-process-kill agent (mbox-process agent)))
  (when (process agent)
    (agent-process-kill agent (process agent)))
  (when (and (assistant? agent)(assistant-process agent))
    (agent-process-kill agent (assistant-process agent)))
  ;; kill all tasks and timers belonging to the agent
  (when (process-list agent) ; hash table must be cleaned
    (maphash #'(lambda (x y) 
                 (declare (ignore y))
                 (agent-process-kill agent x))
             (process-list agent)))
  ;; remove agent from agent-lists
  (setf (local-agents *omas*) 
        (al-remove-prop (key agent) (local-agents *omas*)))
  (setf (names-of-agents-to-display *omas*) 
        (delete (key agent) (names-of-agents-to-display *omas*)))
  (setf (names-of-known-agents *omas*)
        (delete (key agent) (names-of-known-agents *omas*)))
  ;; must also kill its windows and update panel
  ;; should also work as a debugging environment for MSWindows
  #+MCL
  (progn
    (when (window agent)
      (window-close (window agent))
      ;; clear the position of the agent window (destructive)
      (nsubstitute :empty (window agent) *OMAS-agent-window-slots*))
    (when (com-window agent)
      (window-close (com-window agent)))
    ;; redisplay control-panel?
    (omas-window-update-agent-list)
    )
  ;; finally get rid of agent structure
  (makunbound (name agent))
  :done)

;;;-------------------------------------------------------------- %AGENT-FROM-KEY

(defUn %agent-from-key (key)
  "takes a keyword and returns the agent structure, using the local-agents list.
Arguments:
   key: keyword, e.g. :FAC
Return:
   agent structure, e.g. #<AGENT SA_FAC>."
  (cdr (assoc key (local-agents *omas*))))

;;;---------------------------------------------------------- %AGENT-FROM-PACKAGE

(defUn %agent-from-package (package)
  "takes a package and returns the agent structure, using the local-agents list.
   nil if package is not an agent package.
Arguments:
   package: package e.g. #<The ARISTIDE package>
Return:
   agent structure, e.g. #<AGENT ARISTIDE>, nil if package is not an agent package"
  (let ((key (intern (package-name package) :keyword)))
    (cdr (assoc key (local-agents *omas*)))))

#|
(%agent-from-package (find-package :cm))
#<AGENT SA_CSCWD-MEMBERS>
|#
;;;-------------------------------------- AGENT-GET-SUBTASK-FRAME-FROM-SUBTASK-ID

(defMethod agent-get-subtask-frame-from-subtask-id ((agent agent) subtask-id
                                                    &optional task-frame)
  "uses the active-task-list to obtain task-frames. ~
   For each task-frame tries to find the right subtask-frame.
Arguments:
   agent: agent id
   process: process structure
   task-frame (opt): task-frame of the subtask when known.
Return:
   nil on failure or
   subtask-frame as a 1st value
   task-frame as a second value"
  (let (subtask-frame)
    ;; first check whether the task-frame is provided
    (when task-frame
      (setq subtask-frame (car (member-if #'(lambda (xx) (eql subtask-id (id xx)))
                                          (subtask-list task-frame))))
      ;; return 2 values
      (return-from agent-get-subtask-frame-from-subtask-id
        (values subtask-frame task-frame)))
    ;; otherwise, look for subtask-id appearing in subtasks of all recorded tasks
    ;; check all tasks (task-frames from the active task list)
    (dolist (task-frame (active-task-list agent))
      ;; for each task look for the right subtask in the subtask list
      (setq subtask-frame (car (member-if #'(lambda (xx) (eql subtask-id (id xx)))
                                          (subtask-list task-frame))))
      ;; when found return 2 values 
      (when subtask-frame
        (return-from agent-get-subtask-frame-from-subtask-id 
          (values subtask-frame task-frame))))
    ;; otherwise return nil for both values
    (values  nil nil)))

;;;-------------------------------------------- AGENT-GET-TASK-FRAME-FROM-PROCESS

(defMethod agent-get-task-frame-from-process ((agent agent) process)
  "get the task-frame from the process hash table.
   For a standard process the task-frame is that associated with the process.
   For a time-limit timer the task-frame is the one executing the skill,.
   For a timeout timer the one that spawned the subtask for which the timer is ~
   active.
Arguments:
   agent: agent
   process: process in which the task is running
Return:
   task frame (nil for main, scan or assistant)"
  (agent-process-get-task-frame agent process))

;;;-------------------------------------------- AGENT-GET-TASK-FRAME-FROM-TASK-ID

(defMethod agent-get-task-frame-from-task-id ((agent agent) task-id sender)
  "uses the active-task-list to obtain the corresponding task-frame.
Arguments:
   agent: agent id
   task-id: task-number (e.g. 233 or -55)
   sender: id of the agent that sent the message (e.g. :FAC)
Return:
   nil on failure or task frame corresponding to task-id."
  (car (member-if #'(lambda (xx) (and (eql task-id (id xx))
                                      (eql sender (from! xx))))
                  (active-task-list agent))))

;;; ********** true for a task-id, might not be true for a SUBTASK-id
;;; and in particular for bid or answer messages 
;;; if agent receives a message one cannot recover the task-frame and subtask-frame
;;; solely from the subtask-id, since the subtask-id is a number that is assigned
;;; within a local coterie, meaning that different subtasks originating from different
;;; local coteries can have the same subtask number.
;;; We thus need to take into account the name of the agent that created the subtask
;;; which allows to identify the subtask in a unique fashion.
;;; However, the creating agent is maybe not the one that sent the message, in which
;;; case its name is put into the contents field of the message 

;;;----------------------------------------- AGENT-GET-TASK-FRAME-FROM-SUBTASK-ID

(defMethod agent-get-task-frame-from-subtask-id ((agent agent) subtask-id)
  "uses the active-task-list to obtain the corresponding task-frame. ~
   then looks into the subtask-list.
Arguments:
   agent: agent id
   process: process structure
Return (2 values):
   nil nil on failure
   task-frame
   subtask-frame"
  
  ;;******* debug
  ;(drformat :send 2 "~2%;++++++++ agent-get-task-frame-from-subtask-id / ~
  ;               active-task-list: ~%  ~S" (active-task-list agent))
  ;;*******
  
  ;; check all tasks
  (dolist (task-frame (active-task-list agent))
    ;(drformat :send 2 "~%;++++++++ task-frame: ~S" task-frame)
    ;; for each task look into the subtask-list
    (dolist (subtask-frame (subtask-list task-frame))
      ;(drformat :send 2 "~%;++++ subtask-frame: ~S" subtask-frame)
      (when (eql subtask-id (id subtask-frame))
        ;(drformat :send 2 "~%;++++ id (subtask-frame): ~S" subtask-id)
        (return-from agent-get-task-frame-from-subtask-id
          (values task-frame subtask-frame)))))
  ;; if we cannot find subtask we return nil for both task and subtask
  (values nil nil))

;;;--------------------------------------------------------- AGENT-PROCESS-CREATE

(defMacro agent-process-create (agent info name fn &rest args)
  "creates an agent process adding its entry to the agent process table.
Arguments:
 agent: agent
 info: a list containing info to be stored into the process table
 name: string qualifying the process to be created
 function: the function to be executed in the new process
 args: arguments to function
 the arguments must obey the syntax of process-run-function for MCL
     or mp:process-run-function for ACL.
Return:
 process-id of new process."
  `(progn
     (let ((proc-id
            (mp:process-run-function 
             (list :name ,name :initial-bindings cg:*default-cg-bindings*)
             ,fn ,@args)))
       ;; save it now
       (agent-process-record ,agent proc-id ,info)
       ;; return process
       proc-id)))

;;;--------------------------------------------- AGENT-PROCESS-CREATE-RESTARTABLE

(defMacro agent-process-create-restartable (agent info name fn &rest args)
  "creates an agent restartable process adding its entry to the agent process ~
   table.
Arguments:
 agent: agent
 info: a list containing info to be stored into the process table
 name: string qualifying the process to be created
 function: the function to be executed in the new process
 args: arguments to function
 the arguments must obey the syntax of mp:process-run-function for ACL.
Return:
 process-id of new process."
  `(progn
     (let ((proc-id
            ;; allows process to restart when it crashes... 
            (mp:process-run-restartable-function 
             (list :name ,name :initial-bindings cg:*default-cg-bindings*)
             ,fn ,@args))) 
       (mp:process-enable proc-id)
       ;; save it now
       (agent-process-record ,agent proc-id ,info)
       ;; return process
       proc-id)))

;;;-------------------------------------------- AGENT-PROCESS-CREATE-WITH-TIMEOUT
;;; this function creates a process that will run until a timeout is reached
;;; it runs in the main process of the agent

(defMacro agent-process-create-with-timeout (agent info name timeout fn &rest args)
  "creates an agent process adding its entry to the agent process table.
Arguments:
 agent: agent
 info: a list (:sender sender :task-id task-id :type :inform)
 name: string qualifying the process to be created
 fn: the function to be executed in the new process
 timeout: the number of seconds until timeout
 args (rest): arguments to fn
Return:
 process-id of new process."
  `(let ((task-id (getf ,info :task-id))
         (sender (getf ,info :sender))
         proc-id)
     ;; allows process to restart when it crashes...
     (setq proc-id 
           (mp:process-run-function 
            (list :name ,name :initial-bindings cg:*default-cg-bindings*)
            'agent-run-function-with-timeout             
            ,agent task-id sender ,timeout ,fn ,@args)
         )
     ;; save new process id on the list of processes of the agent
     (agent-process-record ,agent proc-id ,info)
     ;; return process
     proc-id))

;;;---------------------------------------------- AGENT-RUN-FUNCTION-WITH-TIMEOUT
;;; this function runs in the context of the thread being created, meaning that
;;; when it exits, the thread is killed

(defun agent-run-function-with-timeout (agent task-id sender delay fn &rest args)
  "function is run until it completes or until a timeout occurs. In case of ~
   timeout, the handler if any is executed.
Arguments:
   agent: ..
   task-id: current task id
   sender: agent that sent the original message
   delay: number of seconds to run before timeout
   fn: function to execute
   args: arguments to the function to execute
Return:
   whatever it returns, when it exits will kill the thread"
  ;; executes fn until it terminates or until timeout is reached, in which case
  ;; agent-process-task-time-limit is executed with 0 retry
  (mp:with-timeout (delay (agent-process-task-time-limit agent task-id 0))
    (apply fn args)))

;;;------------------------------------------------------------ AGENT-RECORD-CHANGE

(defMethod agent-record-change ((agent agent) obj-id old-value)
  "records the pair (<obj-id> . <old-value>) onto the saved-changes list, only if ~
   not alreqdy there.
Arguments:
   agent: agent
   obj-id: MOSS identifier
   old-value: value of the object prior to change
Return:
   the content of (saved-changes agent)"
  ;; change is recorded only of not already there
  (pushnew (cons obj-id old-value) (saved-changes agent) :key #'car))

;;;------------------------------------------------------------- %AGENT-REMOVE-IF

(defMacro %agent-remove-if (agent test prop)
  "macro removing a value from the list attached to a given property. prop must ~
   be a symbol or a keyword specifiying the name of the queue, not a variable.
Ex: (%agent-remove-if mul-1 #'(lambda(xx) (eql '(task-id xx) 'T-0)) :input-messages)."
  (let ((prop-name (intern (symbol-name prop) :omas)))
    `(setf (,prop-name ,agent)
           (remove-if ,test (,prop-name ,agent)))))

;;;---------------------------------------------------------- %AGENT-REMOVE-VALUE

(defMacro %agent-remove-value (agent value prop)
  "macro removing a value from the list attached to a given property. prop must ~
   be a symbol or a keyword specifiying the name of the queue, not a variable.
Ex: (%agent-remove-value mul-1 mes-3 :input-messages)."
  (let ((prop-name (intern (symbol-name prop) :omas)))
    `(setf (,prop-name ,agent)
           (remove ,value (,prop-name ,agent) :test #'equal))))

;;;----------------------------------------------------------- AGENT-REMOVE-VALUE

(defMethod agent-remove-value ((agent agent) value prop)
  "function removing a value from the list attached to a given property
Ex: (agent-remove-value mul-1 mes-3 queue)
Arguments:
   agent: agent
   value: value to be added to the list of values associated with prop
   prop: keyword giving property name (will be transformed into the right symbol)."
  (eval `(%agent-remove-value ',agent ',value ,prop)))

;;;--------------------------------------------------------------- %AGENT-REPLACE

(defMacro %agent-replace (agent old-value new-value prop)
  "internal macro that replaces an old value with a new value in the values associated ~
   with an agent sub-structure. If the new-value is nil then simply erase the old one. 
Arguments:
   agent: agent
   old-value: whatever was there (ckecked using equal)
   new-value: value to replace old one, except if NIL
   prop: refers to an agent's sub-structure, e.g. :memory, :input-messages
Returns:
   the resulting replaced value (normally useless).
Ex: (%agent-replace mul-1 mes-3 mes-4 :input-messages)."
  (let ((prop-name (intern (symbol-name prop) :omas)))
    `(let ((value-list (,prop-name ,agent)))
       (setq value-list (remove ,old-value value-list :test #'equal))
       (setf (,prop-name ,agent)
             (append value-list (if ,new-value (list ,new-value)))))))

;;;------------------------------------------------------------------ AGENT-RESET
;;; mostly unused

(defMethod agent-reset ((agent agent) &key reset-self (reset-world t) reset-tasks 
                        reset-skills no-position no-log &aux process)
  "reset an agent input/output trays (comm part), control part and appearance part.
Arguments:
   agent agent to be reset
   reset-self: (key) if t reset data, goals, memory, intentions
   reset-world: (key) if t reset acquaintances, environment, external-services
   reset-tasks: (key) if t reset projects, waiting-tasks
   reset-skills: (key) if t reset skills
   no-position: (key) if t does not reset agent position in graphics trace
   no-log: (key) if t does not reset logs (the user should|
;; communication (only for ACL)
  ;; in fact this does not apply to an agent but to an environment and, as such
  ;; should not be changed
  #+(or ALLEGRO-v6.1 MICROSOFT-32)
  (when *socket*
    (close *socket*)
    (setf *socket* (socket:make-socket :type :datagram :local-port *port* 
                                       :broadcast t)))
|#
 not reset clock or ~
   task-number)."
  ;(print (name agent))
  ;;=== comm part
  (setf (input-messages agent) nil)
  ;; normally reset logs, unless specified not to
  (unless no-log
    (setf (input-log agent) nil)
    (setf (output-log agent) nil))
  ;;=== control part
  (setf (agenda agent) nil)
  (setf (status agent) :idle)
  (setf (canceled-task-info agent) nil)
  (setf (pending-bids agent) nil) ; *** pending bids are now stored in task-inf
  (setf (saved-answers agent) nil)
  (setf (active-task-list agent) nil)
  (setf (process-list agent) nil)
  ;; should also kill any subsidiary process (carefully)
  ;;=== appearance part
  (unless no-position
    (setf (h-pos agent) 0) ; reset origin
    (setf (v-pos agent) 0))
  ;;=== self part (only if explicitely specified)
  (when reset-self
    (setf (data agent) nil)
    (setf (goals agent) nil)
    (setf (intentions agent) nil)
    (setf (memory agent) nil))
  ;;=== world model (here agent looses info about the world)
  (when reset-world
    (setf (acquaintances agent) nil)
    (setf (environment agent) nil)
    (setf (external-services agent) nil))
  ;;=== tasks description (more memory lost)
  (when reset-tasks
    (setf (projects agent) nil)
    (setf (waiting-tasks agent) nil))
  ;;=== skills (should not be used unless really necessary)
  (when reset-skills
    (setf (skills agent) nil))
  ;; by default reset processes
  (when
    (setq process (process agent))
    ;; kill process and recreate one
    ;; this should not be necessary... might be an overkill
    (agent-process-kill agent process)
    (setq process (agent-process-create 
                   agent
                   '(:type :main)
                   (agent-make-process-name agent "main" :fixed-name t)
                   #'agent-process-task 
                   agent))
    (setf (process agent) process))
  (when
    (setq process (mbox-process agent))
    (agent-process-kill agent process)
    ;; launch then scan process on input-messages queue
    (setq process (agent-process-create 
                   agent
                   '(:type :scan)
                   (agent-make-process-name agent "scan" :fixed-name t)
                   #'agent-scan-messages 
                   agent))
    (setf (mbox-process agent) process))
  :done)

;;;------------------------------------------------------------ MAKE-AGENT-MOSS-NAME

(defMethod make-agent-moss-name ((agent agent))
  "this function is needed to recover the agent object from the persistent database.
Argument:
   agent: agent object
Return:
   a symbol giving the name of the agent interned in the agent package."
  (intern (concatenate 'string (symbol-name (key agent))
                       "-AGENT") (ontology-package agent)))

#|
(make-agent-name address::SA_address)
ADDRESS::ADDRESS-AGENT
|#
;;;---------------------------------------------------- AGENT-SET-MOSS-ENVIRONMENT
;;; executed in the agent package

(defMethod agent-set-moss-environment ((agent agent) persistency)
  "set agent environment creating or loading MOSS objects, according to persistency.
Arguments:
   agent
   peristency: t if agent is declared persistent
Return:
   t or nil on failure."
  (let ((agent-name (name agent))
        (agent-key (key agent))
        database-pathname
        id)
    
    ;(omas-trace :make-agent 1)
    
    (dformat :make-agent 0
             "~%; agent-set-moss-environment / agent: ~S; persistency: ~S"
             (name agent) persistency)
    
    ;;========== non persistent agents
    (unless persistency    
      (dformat :make-agent 0 "~%; agent-set-moss-environment / agent: ~S, *package*: ~S"
        (name agent) *package*)
      (let (msg)
        (setq msg 
             ;(catch
              ;:error
             (progn
               ;; create MOSS classes and method proxies if not already there
               ;; package-key argument is agent package, unless agent is defined in listener
               (unless (equal *package* (find-package :cg-user))
                 (moss::%create-new-package-environment *package* 
                                                        (symbol-name agent-name)))
               ;; add a MOSS structure representing the agent and its skills
               (agent-create-moss-classes agent)
               ;; then we are ready for creating skills and goals
               (return-from agent-set-moss-environment t))
            ;)
            )
        ;; when there was an error, stop to tell user JPB1011
        (break "~S" msg)
        ))
    
    ;;========== agents declared as persistent
    ;; if the agent is defined interactively  in the cl-user or cg-user package
    ;; e.g. when running the tutorial, it will be considered non persistent. The
    ;; rationale is that we save the ontology and KB which should be defined in 
    ;; an ontology file. Thus we send a warning
    (when (not (equal (find-package (key agent)) *package*))
      (warn "agent ~S is not defined in its own package and cannot be persistent."
            (name agent))
      ;; do as in the non persistent case
      (unless (equal *package* (find-package :cg-user))
        (moss::%create-new-package-environment *package* (symbol-name agent-name)))
      ;; add a MOSS structure representing the agent and its skills
      (agent-create-moss-classes agent)
      ;; then we are ready for creating skills and goals
      (return-from agent-set-moss-environment t))
    
    ;;========== persistent agents
    ;; when persistent, more complex, mark the agent as persistent
    (setf (persistency agent) t)
    ;(break "agent-set-moss-environment, agent: ~S" (key agent))
    
    ;; open-database returns 
    ;; - nil if it can't create or open it
    ;; - :empty if opened but partition did not exist and was created
    ;; - :done if opened and partition existed
    
    (case (open-database agent)
      ;; open-database records the pathname in the (omas-database *omas*)
      (:done
       ;; if database and area exist, we are reloading agent environment
       (setq database-pathname (omas-database *omas*))
       ;; load a minimal number of objects from disk
       (moss::db-init-app  database-pathname agent-key)
       ;; OMAS classes have been saved and must not be recreated
       ;(agent-create-moss-classes agent)
       ;; recreate an instance of agent
       (dformat :make-agent 0 "~%; agent-set-moss-environment / OMAS-AGENT class:~%  ~S" 
                (eval (car (access "omas-agent"))))
       ;;********** should be saved in the database otherwise can't save data
       ;; associated with some properties.
       (setq id (moss::%make-individual "omas-agent" 
                                        `("ag-name" ,(symbol-name (key agent)))
                                        `("ag-key" ,(key agent))))
       (dformat :make-agent 0 "~%; agent-set-moss-environment / OMAS-AGENT instance:~%  ~S"
          (eval id))
       
       ;; save id into moss-ref slot of the agent self description
       (setf (moss-ref agent) id)
       
       ;; save database reference at the agent level
       (setf (base agent) database-pathname)
       
       ;; we must also tell OMAS not to load ontology file. Special mark
       (setf (persistency agent) :installed)
       )
      (:empty
       ;; create moss classes and method proxies
       (unless (equal *package* (find-package :cg-user))
         (moss::%create-new-package-environment *package* (symbol-name agent-name)))
       ;; create a MOSS structure representing the agent and its skills
       (agent-create-moss-classes agent)
       
       ;(break "agent-set-moss-environment /*package*" *package*)
       
       ;; record database in the agent structure, since we are going to fill it
       (setf (base agent) database-pathname)
       ;; continue, nothing has been saved, because we must wait for the 
       ;; ontology file to be loaded
       :empty
       )
      (otherwise
       (error "something wrong with opening or creating agent database")))
    ;(break "agent-set-moss-environment")
    ))

;;;------------------------------------------------------------- AGENT-SET-STATUS

(defMethod agent-set-status ((agent agent) status)
  "set the status of an agent, recording the current status in the :previous-status
slot"
  ;(setf (previous-status agent) (status agent))
  ;; check if we are showing a graphics trace. If so, display state
  (when (%window? (graphics-window *omas*)) (state-draw))
  (setf (status agent) status)
  ;; we redisplay the agent window everytime status changes
  ;; when the agent window is not present this does nothing
  (agent-display agent))

;;;-------------------------------------------------------------- AGENT-SET-TRACE

(defMethod agent-set-trace ((agent agent))
  "set the trace flag of an agent so that each function applied to the agent is ~
   traced. To remove use agent-untrace. Sets the :trace flag on the p-list ~
   of the agent key."
  (setf (get (key agent) :trace) t))

;;;---------------------------------------------------------------- AGENT-UNTRACE

(defMethod agent-untrace ((agent agent))
  "reset the :trace flag to nil, stopping the trace process on a particular agent."
  (setf (get (key agent) :trace) nil))

;;;--------------------------------------------------------------- AGENT-VISIBLE?

(defUn agent-visible? (agent-ref)
  "check if local agent should be posted
Arguments:
   agent-ref: should be a kayword or a symbol id of the agent object"
  (cond
   ((keywordp agent-ref)
    (not (get agent-ref :hide)))
   ((typep agent-ref 'agent)
    (not (get (key agent-ref) :hide)))
   ((and agent-ref (symbolp agent-ref)(boundp agent-ref))
    (not (get (key (symbol-value agent-ref)) :hide)))
   (t (warn "agent ref, ~S, is not an agent key nor a bound agent id" agent-ref)
      )))

#|
(agent-visible? :albert)
T
(agent-visible? 'albert::albert)
T
(agent-visible? albert::albert)
T
(agent-visible? 'nil)
Warning: agent ref, NIL, is not an agent key nor a bound agent id
NIL
|#

;;;==============================================================================
;;;
;;;                             Create AGENT
;;;
;;;==============================================================================

;;;------------------------------------------------------------------- MAKE-AGENT
;;; executes usually in the agent package. Could execute in other packages.

(defUn make-agent (name &key hide master persistency package-nickname
                        (language (default-language *omas*))
                        ontology-file
                        (context (or moss::*context* 0))
                        (version-graph (or moss::*version-graph* '((0))))
                        redefine learning ; kept for backward compatibility
                        )
                        ;&allow-other-keys) ; for redefine and learning
  "creates an agent, puts it onto the *agents* list, name must be an unused symbol ~
   or keyword unless the agent is to be redefined with the option (:redefine t).
Arguments:
   name: name given to the agent (e.g. MUL-3, or keyword :MUL-3)
   context (key): agent context (version, default 0)
   hide: (key) if true, hide the agent from the user (defaul is nil)
   language (key): agent language (default language is :EN)
   learning (key): unused, kept for backward compatibility
   master (key): name of an agent that is the master of the agent being created
   ontology-file (key): name of a file to be loaded as an ontology file
   package-nickname (key): nickname for the agent package
   persistency (key): if true, means that the agent has a persistent database
   redefined (key): unused, kept for backward compatibility
   version-graph (key): default version structure (default spedcifies version 0)
Return:
   agent id."
  (declare (ignore redefine learning) ; kept for backward compatibility
           (special *omas*))
  ;; adding the 2 next line to satisfy %ldif function, which allows working
  ;; with versions
  (set (intern "*CONTEXT*") context)
  (set (intern "*VERSION-GRAPH*") version-graph)
  
  (dformat :make-agent 0 "~%; make-agent /language: ~S" language)
  (let (key package)
    ;;=== check if agent package exists
    (unless  (setq package (find-package name))
      ;; if not, create it
      (setq package 
            (if package-nickname
                (make-package name :use '(:cl :moss :omas) 
                              :nicknames (list package-nickname))
              (make-package name :use '(:cl :moss :omas))))) 
    ;; set current package to agent package
    (setq *package* package)
	
    ;;=== set language now in case we need to open database
    ;; there is no need for such a variable, the language is kept in 
    ;; (language agent) 
    (set (intern "*LANGUAGE*" package) language)
	
    ;;=== check if name is a symbol or a keyword, then set name to be a symbol
    ;; and key the corresponding keyword
    (cond
     ((keywordp name)
      (setq key name
            name (create-agent-name key package)))
     ((symbolp name)
      (setq key (intern (symbol-name name) :keyword)
            name (create-agent-name name package)))
     (t (error "the name ~A for the agent must be a symbol or a keyword." name)))
    
     ;; if agent name is a symbol already bound to something, error
     (if (boundp name)
      (error "agent name, ~A, is already bound to ~A." 
        name (symbol-value name)))
    
    ;;=== create a whole new agent
    ;; ***** we should add ontologies
    (let ((comm (make-instance 'comm))
          (control (make-instance 'control))  ; default status is :idle
          (self (make-instance 'self))
          (world (make-instance 'world))
          (ontologies (make-instance 'ontologies))
          (appearance (make-instance 'appearance))
          (tasks (make-instance 'tasks))
          agent process)
      ;;=== create agent structure
      (setq agent (make-instance 'AGENT 
                    :name name
                    :key key
                    :master master  ; JPB0902
                    :comm comm
                    :control control
                    :self self
                    :ontologies ontologies
                    :world world
                    :tasks tasks
                    :appearance appearance
                    ))
      (dformat :make-agent 0 "~&; make-agent /agent: ~S" agent)
      ;; record structure as value of the agent name, e.g. SA_ADDRESS, PA_ALBERT
      (set name agent)
            
      ;; store a handle for load-agent (will stay there if agent loaded manually)
      (setf (agent-being-loaded *omas*) agent) ; JPB 0912
      ;; and add the name to the list of local agents if not there, before calling
      ;; agent-set-moss-environment, because moss::%ldif uses that info
      (unless (assoc key (local-agents *omas*))
        (setf (local-agents *omas*) 
              (append (local-agents *omas*) (list (cons key agent)))))     
      
      ;;=== save global info into agent structure
      (setf (ontology-package agent) package) ; default current package
      (setf (language agent) language) ; default :en 
      ;(break "agent: ~S, language: ~S" name language)
      (setf (ontology-file agent) ontology-file) ; possibly shared ontology file
      (setf (moss-context agent) context) ; default 0
      (setf (moss-version-graph agent) version-graph) ; default ((0))
      
      ;;== copy IP of local host from *omas* (used for WEB agent in web pages)
      (setf (internal-ip agent) (local-ip-address *omas*))
      ;  (socket:ipaddr-to-dotted
      ;   (socket:lookup-hostname (excl.osi:gethostname))))
      
      ;;=== set up agent environment taking persistency into account
      (agent-set-moss-environment agent persistency)
      
      ;;=== add default skills (used essentially by PAs (JPB 051001)
      (agent-add-default-skills agent)
      ;; when learning is present we add it to the agent features
      ;(when learning (%agent-add agent :learning :features))
      ;; use the key to indicate that agent is hidden (for displays)
      (when hide (setf (get key :hide) t))
      ;; add name to names of known agents
      (pushnew name (names-of-known-agents *omas*))
      (dformat :make-agent 0 "~&;=== make-agent; agent: ~S~&;  *names-of-known-agents*~&;  ~S"
              agent (names-of-known-agents *omas*))
      
      ;;=== launch agent process working on the agenda
      (setq process (agent-process-create-restartable
                     agent
                     '(:type :main) ; no task being executed, process is main 
                     (agent-make-process-name agent "main" :fixed-name t)
                     #'agent-process-task 
                     agent))
      (setf (process agent) process)
      ;;=== launch scanner process on input-messages queue
      (setq process (agent-process-create-restartable
                     agent
                     '(:type :scan) ; no task being executed, process is scan
                     (agent-make-process-name agent "scan" :fixed-name t)
                     #'agent-scan-messages 
                     agent))
      (setf (mbox-process agent) process)
      
      ;;=== update the control-panel
      (op-show-agents)
      ;#+MCL
      ;(omas-window-update-agent-list)
      ;; we return the internal name of the agent
      ;; ... with the current package set to the agent package
      agent)))

;;;--------------------------------------------------------------------- DEFAGENT
;;; There are two cases when creating an agent:
;;; - in the cg-user package, e.g. in the first tutorial
;;; - from an agent file
;;; In the first case a new package is created bearing the name of the agent
;;; and OMAS switches to this package
;;; In the second case, the package is defined in the agent file that requests
;;; an "in-package" expression at the beginning of the file
;;; Regarding language:
;;;   The default language for agents is English (default-language *omas*)
;;; However, the language may be set by the :language option. There is no check
;;; that the language is a legally accepted language.
;;; The language is kept in (language agent)
;;; Context and Version-graph:
;;;   Unless specified they should be those of MOSS.
;;; There may be a conflict between moss::*language* and the agent language

(defMacro defagent (name &rest ll)
  "creates an agent, puts it onto the *agents* list, name must be an unused symbol ~
   unless the agent is to be redefined with the option (:redefine t).
Arguments:
   name: name given to the agent (e.g. MUL-3 or keyword :MUL-3)
   ll (rest): options of the make-agent function."
  `(apply #'make-agent ',name ',ll))

;;;==============================================================================
;;;
;;;                              SKILLS
;;;
;;;==============================================================================

;;;------------------------------------------------------------------- make-skill

(defUn make-skill (skill-name agent-name &key static-fcn dynamic-fcn static-pattern
                                how-long-fcn how-long-left-fcn time-limit-fcn
                                bid-cost-fcn bid-quality-fcn bid-start-time-fcn
                                timeout-handler preconditions select-bids-fcn
                                select-best-answer-fcn acknowledge-fcn)
  "function that builds a specific skill and add it to the list of agent's skills
Arguments
   skill-name:                name of the skill (action parameter in the messages
   agent-name:                name or key of the agent receiving the skill
   static-function (key):     function implementing the static part of the skill
   dynamic-function (key):    function launched when a subtask answer comes back
   acknowledge-fcn (key):     function for handling acknowledments to sent messages
   static-pattern (key):         unused
   how-long-fcn (key):        function for estimating how long a task should take
   how-long-left-fcn (key):   same for estimating time left to run the task
   time-limit-fcn (key):      function for handling time-limit interruptions
   timeout-handler (key):     function to process timeout conditions
   preconditions (key):       function implementing any precondition, when it ~
   returns nil, then the skill cannot be executed
   select-best-answer-fcn (key):    function used for selecting the best answer coming from a broadcast
   select-bids-fcn (key):     function to select bids (unused)
Returns:
   a list with skill and agent name, e.g. (HELLO TEST)"
  (let (agent skill)
    ;; agent-name can be in fact an agent structure (e.g., for the manager agent)
    ;; thus, we take care of this case. Otherwise, it is a symbol or a key and
    ;; we recover the structure from the local-agents list. This should be
    ;; changed to distinguish between local-agents and agents to be displayed
    ;; *manager* is an invisible agent.
    (setq agent (if (typep agent-name 'agent) 
                  agent-name
                  (%agent-from-key (keywordize agent-name))))
    (unless agent
      (warn "cannot find agent: ~S, when attempting to define the ~S skill." 
            agent-name skill-name)
      (return-from make-skill nil))
    ;; executing defskill automatically redefines the skill. Thus, we first clean it
    ;; just in case we had no dynamic part and we decide to add one
    ;; if skill was already present we remove it
    (agent-remove-skill agent skill-name)
    ;; OK. Now we create a new skill object
    (setq skill 
          (make-instance 'skill
            :name skill-name
            :static-fcn static-fcn
            :dynamic-fcn dynamic-fcn
            :static-pattern static-pattern
            :bid-cost-fcn bid-cost-fcn
            :bid-quality-fcn bid-quality-fcn
            :bid-start-time-fcn bid-start-time-fcn
            :how-long-fcn how-long-fcn
            :how-long-left-fcn how-long-left-fcn
            :time-limit-fcn time-limit-fcn
            :timeout-handler timeout-handler
            :select-best-answer-fcn select-best-answer-fcn
            :select-bids-fcn select-bids-fcn
            :preconditions preconditions
            :acknowledge-fcn acknowledge-fcn))
    (setf (skills agent) (append (skills agent) (list skill)))
    ;; add the skill to the MOSS representation of the agent
    (make-moss-skill agent skill-name)
    ;; return the name of the agent
    (list skill-name agent-name)))

;;;-------------------------------------------------------------- make-moss-skill

(defMethod make-moss-skill ((agent agent) skill &aux skill-id agent-id)
  "creates a MOSS skill concept if needed. Create an instance of a skill concept 
   in the knowledge base of the agent. Adds the skill to the MOSS agent object 
   representing the agent.
Arguments:
   agent: agent
   skill-name: symbol or keyword"
  (let ((skill-name (symbol-name skill)))
    ;; check first if there is already an agent MOSS object
    (setq agent-id (moss-ref agent))
    ;; if null problem
    (unless agent-id
      (warn "no agent MOSS representation for agent ~S." agent)
      (return-from make-moss-skill nil))
    
    (dformat :make-agent 0 "~%; make-moss-skill /agent key: ~S" (symbol-name (key agent)))
    
    ;; otherwise we have agent and skill models
    ;; if already recorded don't do anything
    (when (access `("omas-agent" ("ag-name" :is ,(symbol-name (key agent)))
                    ("ag-skill" ("omas-skill" ("sk-name" :is ,skill-name)))))
      (return-from make-moss-skill :already-there))
    
    ;; create then the instance of skill
    ;; this can create it twice when redefining an agent
    ;; however the database could not be created yet
    
    ;;************ We must record it in case of a persistent agent
    (setq skill-id (moss::%make-instance "omas-skill" (list "sk-name" skill-name)))
    ;; add the skill to the agent object  
    (moss::send agent-id 'moss::=add-related-objects "ag-skill" (list skill-id))
    ;; return id
    (list agent-id skill-id)))

;;;--------------------------------------------------------------------- defskill

(defMacro defskill (&rest args)
  "macro that builds a specific skill and add it to the list of agent's skills
Arguments
   skill-name:                name of the skill (action parameter in the messages)
   agent-name:                name or key of the agent receiving the skill
   static-function: (key)     function implementing the static part of the skill
   dynamic-function: (key)    function launched when a subtask answer comes back
   static-pattern: (key)         unused
   bid-cost-fcn (key):        function to compue cost for a bid
   bid-quality-fcn (key):     function to compute task aulity for a id
   bid-start-time (key):      function to specify start-time delay for a task
   how-long-fcn: (key)        function for estimating how long a task should take
   how-long-left-fcn: (key)   same for estimating time left to run the task
   time-limit-fcn: (key)      function for handling time-limit interruptions
   timeout-handler: (key)     function to process timeout conditions
   preconditions: (key)       function implementing any precondition, when it returns ~
   nil, then the skill cannot be executed
   select-best-answer-fcn     function used for selecting the best answer coming from ~
   a broadcast
   acknowledge-fcn: (key)     function for handling acknowledments to sent messages
Returns:
   a list with skill and agent name, e.g. (HELLO TEST)"  
  `(apply #'make-skill ',args))

;;;==============================================================================
;;;
;;;                        Default Generic Skills
;;;
;;;==============================================================================

;;; clearly assistant agents should have generic skills
;;; we give an agent the capacity to send messages to other agents (?)
;;; i.e. we can ask an agent to send a message

;;; Methods are added by agent-add-default-skills

;;;================================================================== FIND SKILL

(defUn static-find (agent message args)
  "tries to find data corresponding to a class in the KB of the agent. Args ~
   should contain 
      ((:concept . <class ref>)(:data . <word-list>){(:pattern . <pattern>)})"
  (declare (ignore message))
  (let (results)
    (cond
     ((not (moss::alistp args))
      (format t "~%; static-find /~S should be an alist" args)
      )
     ;; tries to locate instances corresponding to data
     ((assoc :concept args)
      (setq results (moss::locate-objects (cdr (assoc :data args))
                                          (cdr (assoc :concept args))))
      (dformat :make-agent 0 "~%; static-find /results:~%  ~S" results)
      (when results 
        (setq results 
              (moss::fill-pattern results (cdr (assoc :pattern args)))))))
    (static-exit agent (or results :failure))))

#|
Example:
 (send-subtask :to :MISSION :action :find 
               :args '((:concept . "pays")(:data "à" "montreal")
                       (:pattern . ("pays" ("nom")))))
|#
;;;======================================================== GET-EDIT-PAGE-LAYOUT
;;; Returns the layout description of an edit page for the different objects that
;;; can be edited for an agent, e.g. CONTACT, COUNTRY, PERSON, etc.
;;; Computes selection lists dynamically, replacing the function by the result of
;;; its evaluation

(defun static-get-edit-page-layout (agent message args)
  "tries to return a page layout for the different classes of objects in the ~
   CONTACT environment. Error if not found. Processes the :selfcn clauses to ~
   include the list of values describing the objects to post.
Arguments:
   agent: CONTACT
   message: ignored
   args: e.g. ((:data . \"PERSONNE\"))
Return:
   layout list or failure"
  (declare (ignore message))
  (let* ((data (cdr (assoc :data args)))
         (class-ref (if data (moss::%string-norm data)))
         layout result)
    (dformat :edit 0 "~%; static-get-edit-page-layout /*package* ~S" *package*)
    (dformat :edit 0 "~%; static-get-edit-page-layout /agent: ~S" agent)
    (dformat :edit 0 "~%; static-get-edit-page-layout /class-ref: ~S" class-ref)
    ;; if language is specified use it to select values
    (set-language (cdr (assoc :language args)))
    ;; data represents the class
    (when data
      (setq layout (get-field class-ref (edit-layouts agent))))
    (dformat :edit 0 "~%; static-get-edit-page-layout /layout: ~% ~S" layout)
    (when layout
      ;; process the select clauses
      (dolist (item layout)
        (cond
         ((getf (cdr item) :selfcn)
          ;; we replace the row pattern with a new one by replacing the :selfcn 
          ;; option and the :selprop option by the :select option associated with
          ;; a list of values to select form. In case of multilingual values, the
          ;; list can be a list of multilingual values or a list of values 
          ;; extracted with respect to the specified language
          (push (replace-selfcn class-ref item) result))
         ;; if no select option, copy row specification
         (t (push item result))))
      (return-from static-get-edit-page-layout
        (static-exit agent (reverse result))))
    ;; if data does not allow to find the page description, return failure
    (static-exit 
     agent :failure 
     :reason 
     (format nil "OMAS: Page layout is not indexed with this class spelling: ~S."
       (string-trim '(#\space) data)))))

#|
(let ((*package* (find-package :an)))
  (static-get-edit-page-layout an::sa_anatole-news nil '((:data . "news item"))))
:failure
(let ((*package* (find-package :an)))
  (static-get-edit-page-layout an::sa_anatole-news nil '((:data . "info"))))
((:ROW :HEADER "ID (alloué par le système, read-only)" :NAME "id number" :READ-ONLY T :INITFCN
  CREATE-ID)
 (:ROW :HEADER "Titre*" :NAME "titre")
 (:AREA :HEADER "Auteur, ex. Barthès:Jean-Paul, Ramos ..." :NAME "auteur" :ROWS 1 :COLS "60%"
  :READ-ONLY T :PATH ("auteur" "Personne") :INITFCN CREATE-AUTHOR)
 (:ROW :SELECT
  ("" "Conférences" "Divers" "Livres" "Missions" "Personnalités" "Publications" "Recherche" "Sports"
   "Visites" "Voyages")
  :HEADER "Catégorie (max 1)" :NAME "catégorie" :PATH ("catégorie" "Catégorie" "libellé")
  :IF-DOES-NOT-EXIST :IGNORE)
 (:ROW :HEADER "Mots-clés" :NAME "mots clés" :PATH ("mots clés" "Mot Clé") :IF-DOES-NOT-EXIST :CREATE
  :LIST-FCN ("keywords?var=mots%20clés&language=FR" "label"))
 (:AREA :HEADER "Texte" :NAME "texte" :ROWS 3 :COLS "60%")
 (:ROW :HEADER "Date de création (read-only)" :NAME "date de création" :READ-ONLY T :INITFCN
  CREATE-DATE)
 (:ROW :HEADER "Date de modification (read-only)" :NAME "date de modification" :READ-ONLY T :POSTFCN
  CREATE-DATE)
 (:ROW :HEADER "Déjà Publié (read only)" :NAME "publié" :READ-ONLY T)
 (:ROW :HEADER "Archivé (read-only)" :NAME "archivé" :READ-ONLY T)
 (:USER-ACTION :HEADER "Publier..." :NAME "user-action" :VALUE "publish")) 
:DONE
|#

(defun replace-selfcn (class-ref row)
  (let* ((old-row (cdr row))
         (fn (getf old-row :selfcn))
         (name (getf old-row :name))
         (prop (getf old-row :selprop))
         new-row)
    ;; turn row into a-list
    (loop
      (unless old-row (return))
      (setq new-row (alist-set new-row (pop old-row)(if old-row (pop old-row)))))
    ;; remove selfcn and sel-prop entries
    (setq new-row (alist-rem new-row :selfcn))
    (setq new-row (alist-rem new-row :selprop))
    ;; add selection list
    (setq new-row (cons `(:select ,(funcall fn class-ref name prop)) new-row))
    ;; return new row
    (cons (car row) (reduce #'append new-row))))

#|
(defun gg (&rest ll) (list "a" "b" "c"))
(replace-selfcn "Category" '(:row :name "test" :selfcn gg :selprop "test" 
                                   :value "albert"))
(:ROW :SELECT ("a" "b" "c") :NAME "test" :VALUE "albert")
|#
;;;========================================================== GET-OBJECT-FROM-ID
  
(defun static-get-object-from-id (agent message args)
    "returns a description of an object from its id.
Arguments:
   agent: CONTACT
   message: ignored
   args: e.g. (:data . \"$e-contact.21\")
Return:
   a list of properties and values."
    (declare (ignore message))
    ;(format t "~2%;========== ~S: GET-OBJECT-FROM-ID" (name agent))
    (let* ((data (cdr (assoc :data args)))
           id result)
      ;; produce the list with properties and values, using =summary for relations
      (setq id (intern (string-upcase data)))
      (dformat :edit 0 "~%;--- *package*, id: ~S ~S" *package* id)
      (dformat :edit 0 "~%;--- id-l: ~S" (symbol-value id))
      
      (when id
        (setq result (send id 'moss::=make-object-description)))
      (dformat :edit 0 "~%;--- result: ~S" result)
      (if result
          (static-exit agent result)
        (static-exit agent :failure :reason "OMAS: can't find the object..."))
      ))

#|
(static-get-object-from-id contact::SA_CONTACT nil '((:data . "$e-contact.21")))
("Contact" 
 ("id" "$E-CONTACT.21")
 ("correspondant" 
  ("Sugawara: Kenji" "correspondant" "$E-PERSON.39")
  ("Fujita: Shigeru" "correspondant" "$E-PERSON.18"))
 ("partenaire"
  ("Chiba Institute of Technology" "partenaire" "$E-ORGANIZATION.2" "nom"))
 ("sigle partenaire" ("CIT" "partenaire" "$E-ORGANIZATION.2" "sigle"))
 ("ville" ("Tsudanuma" "ville" "$E-CITY.15" "nom")) 
 ("état")
 ("correspondant UTC" 
  ("Barthès: Jean-Paul" "correspondant UTC" "$E-PERSON.3")
  ("Moulin: Claude" "correspondant UTC" "$E-PERSON.27"))
 ("type de contact" "rencontre") 
 ("date de début" "2000") 
 ("commentaire" "RAS"))
|#
;;;================================================================= HELLO SKILL

(defUn static-hello (agent message &rest ll)
  "the HELLO skill simply prints Hello... into the Lisp console. No parameters."
  (declare (ignore message ll))
  (static-exit agent (format nil "~&Hello from ~S..." (key agent))))

;;;=============================================================== LOCATE-OBJECT

(defun static-locate-object (agent message args)
  "from class-ref and additional info tries to find objects.
Arguments:
   agent: CONTACT
   message: unused
   args: e.g. ((:data \"contact\" \"japon, Barthès\"))
Return:
   a list, possibly empty of object ids as strings"
  (declare (ignore message)(special *language*))
  ;(format t "~2%;========== ~S: LOCATE-OBJECT" (name agent))
  (let ((data (cdr (assoc :data args)))
        ;(*language* (or (intern (getvl args :language) :keyword) *language*))
        cues id-list)
    ;; if language is part of the argument sets the environment language
    ;; note that we allow :ALL to locate objects
    (set-language (getvl args :language) :allow-all t)
    (dformat :edit 0 "~%;-- data: ~S" data)
    
    ;; try first a brute force approach (this is because the HTML parameters require
    ;; a "-" between the words of an expression and locate-objects does not work if
    ;; the internal data does not have "-". However access works because it uses
    ;; entry points
    ;(setq id-list (access (cadr data)))
    ;(dformat :edit 0 "~%;-- id-list on direct access: ~S" id-list)
    ;;********** we avoid direct access because the result will not be of the
    ;; specified class. E.g. ("contact" "Chine") returns the country object
    
    ;; if brute force did not work try a more subtle approach
    
    ;; first, split the input string into a list of words
    (setq cues (split-val (cadr data)))
    (dformat :edit 0 "~%;-- cues: ~S" cues)
    
    (dformat :edit 0 "~%;--- *language*: ~S" *language*)
    
    (dformat :edit 0 "~%;-- class-ref: ~S, class-id: ~S" (car data)
             (moss::%%get-id (car data) :class))
    ;; check if (car data) is a bona fide class
    (unless (moss::%%get-id (car data) :class)
      (return-from static-locate-object
        (static-exit agent :failure :reason
                     (format nil "... Bad class (~A), check spelling." (car data)))))
    ;; have a try
    (setq id-list (remove-duplicates (locate-objects cues (car data))))
    (dformat :edit 0 "~%;-- id-list: ~S" id-list)
    
    
    ;; if still nil, then quit
    (unless id-list
      (return-from static-locate-object
        (static-exit agent :failure :reason "... I can't find this object."))) 
    
    ;; if non nil, return ((<id-string> <summary string>))    
    (when id-list
      (setq id-list 
            (mapcar
                #'(lambda(xx)
                    (cons (format nil "~S" xx)
                          (send xx '=summary)))
              id-list)))
    ;; return
    (static-exit agent id-list)
    ))

#|
(static-locate-object sa_contact nil '((:data "contact" "japon, Barthès")))
("$E-CONTACT.21")
(static-locate-object sa_contact nil '((:data "contact" "chine, Barthès")))
(("$E-CONTACT.16"
  "China (Nanjing, Jiangsu): Chen: Kejia, U. des Télécommunications de Nanjing, contact suivi (Barthès: Jean-Paul)")
 ("$E-CONTACT.15" "China (Beijing): Ma: Yushu, U. of Petroleum, contact suivi (Barthès: Jean-Paul)")
 ("$E-CONTACT.14" "China (Beijing): Lin: Zongkai, Academia Sinica, contact suivi (Barthès: Jean-Paul)"))
|#
;;;======================================================== STATIC-MODIFY-OBJECT

(defun static-modify-object (agent message &rest ll)
  "default function is to do nothing, leaving the default OMAS Web Edit functions ~
   in charge of the changes. However, when the object structure and the fields ~
   of the web page are different, then the user may want to handle the updating ~
   directly. Thus, it is a handle on the editing process."
  (declare (ignore message ll))
  (static-exit agent nil))

;;;================================================================== PING SKILL

(defUn static-ping (agent message)
  "the PING skill simply returns site ID. No parameters."
  (declare (ignore message))
  (static-exit agent (key agent)))


;;;=========================================================== SEND-MESSAGE SKILL
;;; The following functions are for sending a message to other agents. It may
;;; be triggered by the master who asked to send a message or by the PA that
;;; does not understand what the master wants and sends a message to its staff 
;;; (or to service agents). The two cases should correspond to distinct skills.
;;; Defined as:
;;;  (make-skill :send-message agent
;;;              :static-fcn 'send-message-static
;;;              :dynamic-fcn 'send-message-dynamic
;;;              :timeout-handler 'send-message-timeout
;;;              :select-best-answer-fcn 'send-message-select-best-answer
;;;              )

;;;---------------------------------------------------------- send-message-static

(defUn send-message-static (agent in-message &rest task-desc)
  "allows any agent to send a request message instantaneously.
   Called following a message like
      :from agent :to :agent :type :internal :action :send-message 
      :args ' (:type :inform :to :mul-1 :action :mutilply :args '(4 5) :delay 3)
Arguments:
   agent: agent
   task-desc (rest): an argument list for the send-subtask function
Return
   returns through static-exit."
  ;(declare (ignore in-message))
  (dformat :send 0 "~%;~A/ send-message-static / message:  ~S ~%; task-desc: ~S" 
    (key agent) in-message task-desc)
  ;(print task-desc)
  ;; clear answer slot (answer slot is no longer used)
  (setf (answer agent) nil)
  ;; clear to-do slot, because dialog is waiting on this slot
  (setf (to-do agent) nil) ; clear slot
  (let ((performative (cadr (member :type task-desc)))) ; JPB0807
    (case performative
      ((:request :call-for-bids)
       ;; build a request message, creating a subtask (needed to trigger the 
       ;; dynamic skill)
       (apply #'send-subtask agent task-desc)
       )
      (t
       ;; no answer expected, hence not necessary to create a subtask
       (send-message (apply #'make-instance 'message task-desc)))
      )
    (static-exit agent :done)))

;;;--------------------------------------------------------- send-message-dynamic
;;;JPB0905
;;; when an answer is returned it is put into the answer slot if the agent rather
;;; than in the to-do slot, decoupling the saving of the answer from the waking
;;; up of the converse process. However, we immediately wake up the process by
;;; inserting "answer-there" into the to-do slot.
;;; This seems quite redundant, all the more that the converse dialog must fetch
;;; the answer from the proper slot rather than obtaining it directly in the 
;;; FACTS/input area.
;;; 10/10 removed, answer is inserted directly into the to-do slot
;;; 11/03 This  not right. When a task sends a standard request, the answer is
;;; returned to the dynamic part of the skill. 
;;; Here we send a request but the dialog is waiting. Thus, when the answer
;;; comes back, it sets first the AGENT/TO-DO slot, waking up the waiting process
;;; If the answer is a failure, sets FACTS/INPUT to failure, otherwise moves the 
;;; content of AGENT/TO-DO into FACTS/INPUT. It could be interesting to distinguish
;;; between what the master is saying and what is returned by other agents. An
;;; input from the master would go into the FACTS/INPUT area, an answer from an
;;; external source to the FACTS/ANSWER area.
;;; If the agent is expecting an answer and the master types in something, one
;;; can deduce that either he starts a new turn or wants to abort the process.
;;; Currently there is no distinction between an answer and a master's input.
;;; Another problem is that an answer may have a structure quite different from
;;; a master's input (e.g. returning a filled pattern).
;;;
;;; When we have a single answer and it is a failure, we extract the reason for
;;; failure from the message and insert it into an agent slot for later use

(defUn send-message-dynamic (agent message answer)
  "get the answer to a request message, puts it into the to-do slot to wake up ~
   the dialog process. Return :failure in case all answers are failures."
  ;; if message was a broadcast, we must check for unprocessed answers
  ;; unprocessed means that we get the list of messages returned to the broadcast
  (drformat :send 0 "~2%;<<<<< ~A: Entering send-message-dynamic" (key agent))
  ;(format t "~%;--- message: ~S" message)
  (dformat :send 0 "~%;--- answer: ~S" answer)
  
  ;;== answer to a broadcast message
  (when (eql message :unprocessed) ; JPB1005
    ;; we extract answers from the non failure messages and ignore failures
    (setq answer 
          (remove nil 
                  (mapcar #'(lambda (xx) 
                              (unless (moss::is-answer-failure? (contents xx))
                                (contents xx)))
                          answer)))
    (dformat :send 0 "~%; ~A/ send-message-dynamic /answer to broadcast:~% ~S" 
      (key agent) answer)
    )
  ;;== failure
  ;; when we have a single answer and it is a failure, we insert the reason for the
  ;; failure into the error-contents slot of the agent
  (when (and (not (listp answer))(moss::is-answer-failure? answer)
             (typep message 'message) ; just in case
             )
    (setq answer :failure)
    (setf (error-contents agent) (error-contents message))
    (dformat :send 0 "~%; ~A/ send-message-dynamic reason for failure: ~S" 
                  (key agent) (error-contents message))
    )
  
  ;;== normal answer
  #+:converse-v2
  ;; :to marks the message as being returned to the calling agent
  (setf (to-do agent)  
    `(:from ,(from! message) :to ,(to! message) :answer ,answer 
            :reason ,(error-contents message)))
  #-:converse-v2
  ;; puts the answer or list of answers into the answer slot of the agent
  (setf (answer agent) answer
    ;; wake up dialog
    (to-do agent) :answer-there)

  ;; print a control trace 
  (dformat :send 0 "~%; ~A/ send-message-dynamic =====>>> ~S" (key agent) answer)
  ;; this completes the request
  ;; we do a special dynamic exit to avoid sending an answer message to ourselves
  (dynamic-exit agent :done :internal t)
  ;; kill current task and reset status
  )

;;;---------------------------------------------- send-message-select-best-answer
;;; default behavior directly encoded in agent-process-broadcast-answer

;;; maybe should return something like a marker (:unselected) and the list of
;;; messages in the value argument

;;;(defUn send-message-select-best-answer (agent message-list)
;;;  "when we collect answers we return the list of the content of the different 
;;;   messages. Will return nil if all answers are failures.
;;;Arguments:
;;;   agent: current agent
;;;   message-list: list of messages to select
;;;Return:
;;;   2 values:
;;;     a list of one message and list of non failure values
;;;     the number of non failure values"
;;;  (declare (ignore agent))
;;;  ;; first discard messages that return "failure" or :failure
;;;  (let ((result (remove nil 
;;;                        (mapcar #'(lambda (xx) 
;;;                                    (unless (moss::is-answer-failure? (contents xx))
;;;                                      (list xx (contents xx))))
;;;                          message-list))))
;;;    
;;;    (values
;;;     ;; if more than one answer left, return first message and list of answers
;;;     (if (cdr result) 
;;;         (list (caar result) (remove nil (mapcar #'cadr result)))
;;;       result)
;;;     (length result))
;;;    ))

#|
(send-message-select-best-answer 
 hdsri::pa_hdsri 
 (mapcar #'symbol-value
   (list (defmessage m1 :to :albert :contents (1 1 1)))))
((#<MESSAGE 11:26:0 NIL :ALBERT NIL NIL NIL (1 1 1) Tid:NIL :BASIC-PROTOCOL no-TO TL:3600 id:NIL> (1 1 1)))
1

(send-message-select-best-answer 
 hdsri::pa_hdsri 
 (mapcar #'symbol-value
   (list (defmessage m1 :to :albert :contents (1 1 1))
         (defmessage m2 :to :albert :contents :failure)
         (defmessage m3 :to :albert :contents "*failure*")
         (defmessage m4 :to :albert :contents (2 2 2)))))
(#<MESSAGE 11:25:5 NIL :ALBERT NIL NIL NIL (1 1 1) Tid:NIL :BASIC-PROTOCOL no-TO TL:3600 id:NIL> ((1 1 1) (2 2 2)))
2

(send-message-select-best-answer 
 hdsri::pa_hdsri 
 (mapcar #'symbol-value
   (list (defmessage m2 :to :albert :contents :failure)
         (defmessage m3 :to :albert :contents "*failure*")
         )))
NIL
0
|#

;;;------------------------------------------------- send-message-timeout-handler

(defUn send-message-timeout (agent message)
  "called on timeout. If assistant, puts failure into the to-do slot.
Arguments:
   agent: self
   message: message that was timed out
Return:
   :unprocessed to call the default timout behavior."
  ;; print a control trace 
  (dformat :send 0 "send-message-timeout =====>>> ~S" :failure)
  ;(assistant-display-text agent (format nil "~S" answer))
  ;; wake up dialog
  (cond
   ((member :converse-v2 *features*)
    (setf (to-do agent) `(:from ,(from! message) :answer :failure 
                                :reason "No answer"))
    ;(break "send-message-timeout/ (to-do agent) ~S" (to-do agent))
    )
   (t
    ;; puts a failure answer into the answer slot of the agent
    (setf (answer agent) :failure)
    ;; indicate a timeout failure
    (setf (error-contents agent) "No answer")
    (setf (to-do agent) :answer-there))
   )
  ;; this completes the request
  ;; kill current task and reset status
  :unprocessed  ; equivalent to a call-next-method...
  )

;;;================================================================ START-EDITING
;;; skill called when we start creating or modifying an object in the agent package
;;; (individual or orphan)

(defun static-start-editing (agent message args)
  "receives a note that we want to create an object of a given class, and we ~
   must execute all :initfcn to prepare the object.
Arguments:
   agent: this agent
   message: unused
   args: e.g. ((:data :CREATING \"news item\"))
Return:
   a list with the object temporary id and the object description."
  (declare (ignore message))
  (let* ((data (cdr (assoc :data args)))
         (class-ref (moss::%string-norm (cadr data)))
         (page-description (get-field class-ref (edit-layouts agent)))
         edit-object fn prop-val)
    (dformat :edit 0 "~%;---args:~%  ~S" args)
    (dformat :edit 0 "~%;~A static-start-editing /data: ~S" (name agent) data)
    (dformat :edit 0 "~%;~A static-start-editing /class-ref: ~S" (name agent) class-ref)
    (dformat :edit 0 "~%;~A static-start-editing /page-description:~% ~S" 
      (name agent) page-description)
    
    ;; if class is not a bona fide class, return an error
    (unless (moss::%%get-id class-ref :class)
      (return-from static-start-editing
        (static-exit agent :failure 
                     :reason (format nil "Class ~S is unknown." class-ref))))
    
    ;;=== loop for checking :initfcn
    (dolist (item page-description)
      ;; item is a particular field description of the edit form
      ;; check for an init function
      (setq fn (getf (cdr item) :initfcn))
      ;(format t "~%;~A static-start-editing /fn: ~S" (name agent) fn)
      (when fn 
        ;; execute the initfcn, putting eventual comments into the message-list
        (setq prop-val (cons (getf (cdr item) :name) (funcall fn)))
        ;(format t "~%;~A static-start-editing /prop-val: ~S" agent prop-val)
        ;; put the resulting pair (prop value(s)) into the edit-object
        (push prop-val edit-object)
        ))
    
    ;; return the object id and description with the created pairs
    (static-exit agent (cons nil (reverse edit-object)))))

#|
(let ((*package* (find-package :sn)))
  (omas::static-start-editing 
   sn::sa_stevens-news nil '((:data :CREATING "news item"))))
("NEWS-ITEM" ("id number" "LN-5") ("author" "barthès") ("creation date" "08/01/2014"))
NIL
|#
;;;================================================================ UPDATE-OBJECT

(defun static-update-object (agent message args)
  "we must execute changes from a list of orders.
Arguments:
   agent: CONTACT
   message: received message
   args: e.g. ((:data \"$E-CONTACT.17\" 
                 ((\"sigle partenaire\" :MOD \"CIT\")
                  (\"correspondant\" :DEL \"Sugawara: Kenji, Fujita: S.\")
                  (\"correspondant\" :ADD \"Sugawara: Kenji\")
                  (\"commentaire\" :DEL \"change\"))))
Return:
   message or :failure"
  (declare (special *language*)
           (ignore message))
  ;(dformat :edit 0 "~2%;---------- Entering :UPDATE-OBJECT")
  (dformat :edit 0 "~%;--- args: ~%~S" args)
  ;; we enter this function either for editing an existing object or for editing
  ;; a temporary object whose description was saved into the edit-temp-object slot
  ;; of the agent. In the second case the object ref is nil
  (let* (;; get object ref
         (ref (cadr (assoc :data args)))
         ;; get-update-program
         (program (caddr (assoc :data args)))
         ;; set up language if there (necessary for mln values)
         (*language* (or (cdr (assoc :language args)) *language*))
         object-description page-description class-ref id message-list)
    
    ;; get the object id either from the arguments or create a new object from
    ;; the saved object slot of the agent
    (setq id
          (if ref
              (intern (string-upcase ref))
            (apply #'make-individual (edited-object agent))))
    ;; compute object description (returns a list starting with class-ref)
    ;; this is the object to modify (onto which we apply program)
    (setq object-description (send id 'moss::=make-object-description))
    ;; get class-ref
    (setq class-ref (car object-description))

    ;; an a-list describing the edit page 
    (setq page-description 
          (get-field (moss::%string-norm class-ref) (edit-layouts agent)))
    
    ;; call an OMAS internal function, returns the object-description of the 
    ;; modified object
    (setq object-description
         (update-object agent class-ref object-description 
                        page-description program))
    ;; get message-list if any warning was posted
    (setq message-list (edit-message-list agent))
    ;; must clean-up edited-object and edit-message-list
    (setf (edited-object agent) nil)
    (setf (edit-message-list agent) nil)
    ;; select message "Object was modified" in the current language (default)
    (static-exit agent 
                 `(,(car (get-field "id" (cdr object-description)))
                   ,(or message-list (mln::extract *web-objmod*)))) ; jpb 1407
    ))


;;;==============================================================================
;;;
;;;                            EDIT LAYOUTS
;;;
;;;==============================================================================
;;; function and macro to define web page structure for editing objects
;;; the corresponding list of page descriptions is recorded into the edit-layouts
;;; slot of the agent

(defun make-edit-layout (class-ref agent-name field-list)
  "builds the structure of web pages (one per class) for editing objects. class-ref ~
   is normalized using the moss %string-norm function.
Arguments:
   agent-name: the agent name or keyword
   class-ref: a string referencing the class of objects
   field-list: the list of fields in the page
Return:
   the modified layout list"
  (let ((class-ref (moss::%string-norm class-ref))
        agent layouts)
    ;; first get a handle to the agent object
    (setq agent (if (typep agent-name 'agent) 
                  agent-name
                  (%agent-from-key (keywordize agent-name))))
    ;; then get the existing list of layouts
    (setq layouts (edit-layouts agent))
    ;; remove the one corresponding to class-ref
    (setq layouts (remove class-ref layouts :test #'moss::equal-gen :key #'car))
    ;; add new one
    (setf (edit-layouts agent)
      (cons (cons class-ref field-list) layouts))))

#|
(make-edit-layout "person test" :stevens-news
                  '((:row :header "Name*" :name "name")
                    (:row :header "First name" :name "first name")
                    (:row :header "Initials" :name "initials")
                    ))
|#
;;;---------------------------------------------------------------- defeditlayout

(defmacro defeditlayout (class-ref agent-name &rest field-list)
  "macro that builds a specific layout for structuring a web page for editing an ~
   object instance of a class.
Arguments:
   class-ref: a string referencing the class of objects
   agent-name: the agent name or keyword
   field-list: the list of fields in the page
Return:
   the modified layout list"
  `(apply #'make-edit-layout ,class-ref ,agent-name ',field-list))

#|
(defeditlayout "person" :stevens-news
  ((:row :header "Name*" :name "name")
   (:row :header "First name" :name "first name")
   (:row :header "Initials" :name "initials")
   (:row :header "Email" :name "email")))
|#
;;;==============================================================================
;;;
;;;                            AGENT STATE
;;;
;;;==============================================================================
;;; Not sure it is still usefull... JPB 1003

;;;======================== agent state structure ===============================
;;; we define a structure to pass the minimal state information for updating the
;;; agent window or the graphics trace.
;;; we need to know the date, the state (:busy, :idle, :dead), if there are
;;; time-limit timers (on tasks), if there are timeout timers (on subtasks)

(defClass agent-state ()
  ((date :accessor date :initarg :date :initform nil)
   (agent-key :accessor agent-key :initarg :agent-key :initform nil)
   (status :accessor status :initarg :status :initform :idle)
   (time-limits :accessor time-limits :initarg :time-limits :initform 0)
   (timeouts :accessor timeouts :initarg :timeouts :initform 0))
  (:documentation "short summary of an agent state (for drawing purposes)"))

;;;--------------------------------------------------- (agent-state) print-object

(defMethod print-object ((as agent-state) stream)
  (format stream "#<~A at ~A, ~S ~S ~S>"
          (agent-key as)
          (time-string (date as))
          (status as)
          (time-limits as)
          (timeouts as)))

;;;------------------------------------------------------------- %agent-set-state

(defMethod %agent-set-state ((agent agent) &key date)
  "set up agent state for display purposes. Puts the status, timeouts and time-limits
   onto the agent-key p-list."
  (let ((time-limit-count 0)(timeout-count 0) 
        (agent-key (key agent)))
    (dolist (task (active-task-list agent))
      ;; for a task check for time-limit presence. If there is a task, there
      ;; should be a time-limit (default one eventually)
      (when (timer-process task) (incf time-limit-count))
      (dolist (subtask (subtask-list task))
        ;; count timeouts
        (when (timeout-process subtask) (incf timeout-count))))
    ;; record stuff onto p-list
    (setf (get agent-key :date) (or date (get-universal-time)))
    (setf (get agent-key :status) (if (active-task-list agent) :busy :idle))
    (setf (get agent-key :time-limits) time-limit-count)
    (setf (get agent-key :timeouts) timeout-count)
    :done))

;;;-------------------------------------------------------------- agent-set-state

(defMethod agent-set-state ((agent agent) &key date)
  "set up agent state for display purposes"
  (let ((time-limit-count 0)(timeout-count 0))
    (dolist (task (active-task-list agent))
      ;; for a task check for time-limit presence. If there is a task, there
      ;; should be a time-limit (default one eventually)
      (when (timer-process task) (incf time-limit-count))
      (dolist (subtask (subtask-list task))
        ;; count timeouts
        (when (timeout-process subtask) (incf timeout-count))))
    (make-instance 'AGENT-STATE
      :agent-key (key agent)
      :date (or date (get-universal-time))
      :status (if (active-task-list agent) :busy :idle)
      :time-limits time-limit-count
      :timeouts timeout-count)))

;;;------------------------------------------------------- get-local-agent-states

(defUn get-local-agent-states (&key date)
  "builds an a-list giving the state of local agents.
Arguments:
   date (key): specified date
Return:
   alist like ((:MUL-1 . #<MUL-1 at 19:25:50, :IDLE 0 0>))"
  (mapcar #'(lambda (xx) (cons (car xx) (agent-set-state (cdr xx) :date date)))
          (local-agents *omas*)))

;;;-------------------------------------------------- %RECORD-MESSAGE-AGENTS-INFO

(defUn %record-message-agents-info ()
  "look at sender and receivers and for each local agent, save its state."
  (let (agent)
    (dolist (agent-key (names-of-agents-to-display *omas*))
      ;; :all :all-and-me :<USER> and nil are not local agents
      (if (setq agent (local-agent? agent-key))
        (%agent-set-state agent)))
    :done))


(format t "~&;*** OMAS v~A - agents loaded ***" *omas-version-number*)

;;; :EOF