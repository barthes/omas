﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;               O M A S - S E L F  (file self.lisp)
;;;
;;;===============================================================================
;;; This file contains the definitions and methods associated with the class SELF
;;; that models the self part of a given agent.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| 
History
-------
2020
 0202 restructuring OMAS as an asdf system
|#

#| the self part of an agent is structured as:
self
   data                    ; area containing data (user's structured)
   features                ; features (e.g., :learning)
   goals                   ; long-term goals of the agent
   intentions              ; for implementing BDI agent?
   memory                  ; contains what the agent has learned so far
|#

(in-package :omas)

;;;================================= service =====================================

(defun limit-text-length (text limit)
  "takes a text and limits its length.
Arguments:
  text: string to be checked
  limit: max length."
  (if (> (length text) limit)
    (concatenate 'string (subseq text 0 (- limit 3)) "...")
    text))

;;;================================== MEMORY =====================================
;;; format of an entry on the list of rmemory items

#|
A memory item is a piece of data that is recorded by means of a tag
It has the following propoerties:
   date:      time at which it was recorded
   tag:       recording key
   service:   type of service in which it was produced
   info-type: type of information (e.g., mailing-list)
   ontology:  ontology in which data is defined
   value:     data themselves
Most of the information can be obtained from the context object when the data is 
recorded.
|#

(defClass MEMORY-ITEM ()
  ((date :accessor date :initarg :date :initform (get-universal-time))
   (tag :accessor tag :initarg :tag :initform nil)
   (service :accessor service :initarg :service :initform nil)
   (info-type :accessor info-type :initarg :info-type :initform nil)
   (ontology :accessor ontology :initarg :ontology :initform nil)
   (content :accessor content :initarg :content :initform nil))
  (:documentation "entry describing a peice of information kept in memory"))

(defMethod print-object ((mm memory-item) stream)
  (format stream "#<M-ITEM ~S ~S ~A>"
          (time-string (date mm))
          (tag mm)
          (limit-text-length (format nil "~S" (content mm)) 30)))

(defun time-full-string (time)
  "produces a string with full time, e.g., \"11:15:49 2/12/2000\"."
  (multiple-value-bind  (sec min hr date month year a1 a2 a3)
                        (decode-universal-time time)
    (declare (ignore a1 a2 a3))
    (format nil "~A:~A:~A ~A/~A/~A" hr min sec date month year)))
;;;---

;;; the user had the forget function available from the API to wipe out memory

(defun agent-make-memory-item-key ()
  "cooks up a key for memory items if needed, e.g. :MI-21"
  (intern (symbol-name (gentemp "MI-")) :keyword))

(defun agent-memory-retrieve (agent input)
  "function called when trying to recover data from memory. Data is organized as ~
      an a-list. A piece of data is an object with a time-stamp and a value.
Arguments:
   agent: agent
   input: pattern for recovering data."
  ;; if item is recorded send back the object
  (cdr (assoc input (memory agent) :test #'equal)))

;(trace agent-memory-retrieve)
;(untrace agent-memory-retrieve)

(defun agent-memory-remember (agent key value &optional service info-type ontology)
  "saves data as an object in memory an a-list. The key is memorized in the tag ~
   slot.
   Anything remembered will erase the previous souvenir... (aka property-list).
Arguments:
   agent: agent
   key: pattern for recovering data
   value: data to be saved
   service: (opt) service that produced the value
   info-type: type of information
   ontology: ontology for which the terms have a meaning."
  (declare (ignore service info-type ontology))
  ;; if item is recorded erase it
  (forget agent key)
  #+MCL
  (%agent-add agent
              (cons key (make-instance 'MEMORY-ITEM
                          :tag key
                          :content value))
              :memory)
  #+(or ALLEGRO-V6.1 MICROSOFT-32)
  (agent-add agent ;;aqui
             (cons key (make-instance 'MEMORY-ITEM
                         :tag key
                         :content value))
             :memory)
  )

(defMacro deffact (agent fact key &key service info-type ontology)
  "saves some data (a fact) as a memory item, associated with a key. When no key ~
      is provided makes one and return it.
Arguments:
   agent: agent
   fact: the stuff to be saved with its own structure
   key (key): a keyword to retrieve the data
   service (key): the service that produced the value
   info-type (key): type of information (user-defined)
   ontology (key): ontology that allows to understand the fact
Return:
   key, either the one provded or a synthetic one."
  `(let ((key (or ,key (agent-make-memory-item-key))))
     (agent-memory-remember ,agent key ',fact ',service ',info-type ',ontology)
     key))

(defun forget (agent index)
  "removes some data from memory.
Arguments:
   agent: agent
   index: key associated with memory item
Return:
   irrelevant"
  (let ((item (recall agent index :field :all)))
    (when item
      (%agent-remove-value agent (cons index item) :memory))))


(defun remember (agent fact key &key service info-type ontology)
  "saves data as an object in memory an a-list. The key is memorized in the tag slot.
   If no key is present (i.e. is nil), one is synthesized.
Arguments:
   agent: agent
   fact: data to be saved
   key (key): pattern for recovering data (usually a keyword)
   service (key): service that produced the value
   info-type (key): type of information
   ontology (key): ontology for which the terms have a meaning.
Return
   key under which the fact is saved."
  (let ((tag (or key (agent-make-memory-item-key))))
    (agent-memory-remember agent tag fact service info-type ontology)
    tag))

(defun recall (agent index &key field)
  "function called when trying to recover data from memory. Data is organized as ~
   an a-list. A piece of data is an object with a time-stamp and a value.
Arguments:
   agent: agent
   index: pattern for recovering data
   field (key): field of interest to be returned (date, type, ontology, service,all ~
   none is data itself."
  (let ((info (agent-memory-retrieve agent index)))
    (when info
      (case field
        (:all info)
        (:date (date info))
        (:info-type (info-type info))
        (:ontology (ontology info))
        (otherwise (content info))))))

(format t "~&;*** OMAS v~A - self loaded ***" *omas-version-number*) 

;;; :EOF