﻿;;;-*- Mode: Lisp; Package: "SPY" -*-
;;;===============================================================================
;;;23/07/15
;;;                O M A S - S P Y  (file omas-W-spy.lisp)
;;;
;;;===============================================================================
;;; This file contains all the functions related to the spy graphics window for 
;;; tracing the messages in a OMAS execution. 

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
History
2020
 0204 restructuring OMAS as an asdf system
2023
 0715 adding draw-message-exception to avoid tracking unwanted messages
|#

(in-package :spy)

;;;===============================================================================
;;;
;;;                               GLOBALS
;;;
;;;===============================================================================

;;; graphics trace window for displaying agent threads and messages
;;; the following values are default values to be applied when the window is 
;;; actually created
;;; the window is positioned at the top left handside of the screen between the
;;; OMAS panel and the right margin.

;;; the display-list is a slot of SPY window (done in omas-spy
;(defparameter *spy-window* nil)

;;; the way to specify *screen changed from 6.1 to 8.1

(defParameter *screen* (cg:screen cg:*system*))

(defParameter *spy-left-position* omas::*panel-width*)
(defParameter *spy-top-position* (+ 0 #+MCL 44))
(defParameter *spy-width* 
  (-  (cg:width *screen*) omas::*panel-width* 10))
(defParameter *spy-height* 500)
(defParameter *spy-header-height* 18 "hight of the SW header") 

(defParameter *text-max-length* (max 255 (floor (/ (* 120 *spy-width*) 500))))
(defParameter *text-height* 12 "vertical positioning of message text")
(defParameter *text-spacing* #+MCL 17 #-MCL 22 "vertical spacing for message text")

(defParameter *spy-v-scroll* 0)
(defParameter *scroller-height* (- *spy-height* *spy-header-height*)
  "height of the graphics area")
(defParameter *scroller-sheet-max-length* 5000 "size of the drawing area")


;;;; used to size the display sheet (used in scrolling-window-scroller)
;;;(defParameter *simulation-time-limit* 1000 
;;;  "Maximum length of simulation is set to 1000 cycles, also used to dimension ~
;;;      field-size of scroller in graphics window.")
;;;(defParameter *scrolling-graphics* nil 
;;;  "set to true when graphics is done in a scrolling window")

(defParameter *arrow-head-size* 10)
(defParameter *message-arrow-thickness* 3)
(defParameter *life-line-idle-thickness* 1)
(defParameter *life-line-busy-thickness* 3)

(defParameter *square-thickness* 5)

;;; colors. When in doubt use (ask-user-for-color) ACL or (user-pick-color) MCL
(defParameter *black-color* cg:black)
(defParameter *blue-color* cg:blue)
(defParameter *green-color* cg:green)
(defParameter *red-color* cg:red)
(defParameter *light-blue-color* (cg:make-rgb :RED 66 :GREEN 160 :BLUE 255))
(defParameter *orange-color* (cg:make-rgb :RED 255 :GREEN 128 :BLUE 0))
(defParameter *tan-color* (cg:make-rgb :RED 128 :GREEN 64 :BLUE 0))
(defParameter *white-color* cg:white)
;; not really yellow that one cannot read
(defParameter *yellowish-color* cg:yellow)
(defParameter *grey-color* (cg:make-rgb :RED 128 :GREEN 128 :BLUE 128))
(defParameter *light-green-color* (cg:make-rgb :RED 0 :GREEN 255 :BLUE 64))
(defParameter *light-grey-color*(cg:make-rgb :RED 228 :GREEN 228 :BLUE 228))

;;;===============================================================================
;;;
;;;                          EXTERNAL FUNCTIONS
;;;
;;;===============================================================================

;;; functions called from processes other than the display process

(in-package :omas)

;;;-------------------------------------------------------------- REDISPLAY-WINDOW

(defun redisplay-window ()
  "redisplay the spy window. Use inparticular when the number of agents changes."
  nil)

;;;----------------------------------------------------------- AGENT-DISPLAY-ABORT

(defun agent-display-abort (agent &rest ll)
  "send a message to the spy agent"
  (declare (ignore ll))
  (%send-message 
   (make-instance 'message
     :from (key agent)
     :to (spy-name *omas*) 
     :type :sys-inform
     :action :mark-abort
     :args (list (key agent))
     )))

#|
? (agent-display-abort mul-1::mul-1)
|#
;;;------------------------------------------------------ AGENT-DISPLAY-TIME-LIMIT

;(agent-display-time-limit cl-user::fac)
(defun agent-display-time-limit (agent &rest ll)
  "send a message to the spy agent"
  (declare (ignore ll))
  (%send-message 
   (make-instance 'message
     :from (key agent)
     :to (spy-name *omas*)
     :type :sys-inform
     :action :mark-time-limit
     :args (list (key agent))
     )))

#|
? (agent-display-time-limit fac::fac)
|#
;;;--------------------------------------------------------- AGENT-DISPLAY-TIMEOUT

;(agent-display-timeout cl-user::fac)
(defun agent-display-timeout (agent &rest ll)
  "send a message to the spy agent"
  (declare (ignore ll))
  (%send-message 
   (make-instance 'message
     :from (key agent)
     :to (spy-name *omas*)
     :type :sys-inform
     :action :mark-timeout
     :args (list (key agent))
     )))

#|
? (agent-display-timeout mul-2::mul-2)
|#
;;;------------------------------------------------------------------ MESSAGE-DRAW

(defun message-draw (message &rest ll)
  "send a message to the spy agent"
  (declare (ignore ll))
  ;; use %send-message otherwise we get an infinite loop
  (%send-message 
   (make-instance 'message
     :from (from! message)
     :to (spy-name *omas*)
     :type :sys-inform
     :action :message-draw
     :args (list message)
     )))

#|
? (omas::message-draw 
     (omas::message-make :type :request :from :fac :to :albert :action :hello))
|#
;;;----------------------------------------------------------------- MESSAGE-TRACE

(defun message-trace (message &rest ll)
  "send a message to the spy agent"
  (declare (ignore ll))
  (%send-message 
   (make-instance 'message
     :from (from! message)
     :to (spy-name *omas*)
     :type :sys-inform
     :action :message-trace
     :args (list message)
     )))

;;;-------------------------------------------------------------------- STATE-DRAW

(defun state-draw (&rest ll)
  "send s system message to the spy agent to draw all states"
  (declare (ignore ll))
  (%send-message 
   (make-instance 'message
     :to (spy-name *omas*)
     :type :sys-inform
     :action :state-draw
     )))

#|
(omas::state-draw)
|#
;;;===============================================================================
;;;                               SCROLLING WINDOW (MCL)
;;;===============================================================================

(in-package :spy)

;;;===============================================================================
;;;                               SPY WINDOW
;;;===============================================================================

(defClass SPY-WINDOW (#+mcl v-scrolling-window #-mcl cg:frame-window)
  ((last-draw-time :accessor last-draw-time :initform nil)
   ;; absolute last position wrt vertical origin of drawing
   (last-v-position :accessor last-v-position :initform 0)
   ;; we start with a small offset with respect to the top of the window
   ;; current-v-position is absolute position
   (current-v-position :accessor current-v-position :initform 2)
   (display-list :accessor display-list :initform nil)
   ;; number of agents being displayed
   (count-for-names :accessor count-for-names :initform 0)
   ;; to be subtracted from current-v-position to obtain relative value
   ;; i.e. offset wrt the top of the window
   (pane-offset :accessor pane-offset :initform 0))
  (:documentation 
      "SPY window in which we draw arrows corresponding to messages exchanged ~
        among agents. It records the last time something was drawn on the pane in ~
        order to compute space between two successive messages."))

;;;--------------------------------------------------------------- MAKE-SPY-WINDOW
#|
(defparameter *spy-window* (make-spy-window :agents '(:albert :sa-address)))
|#

(defun make-spy-window (&key agents)
  "create a window in which we draw arrows corresponding to messages exchanged ~
      among agents.
arguments: 
   agents (key): list of agent names to display
return:
   spy window object."
  (declare (special *spy-window* omas::*omas* omas::*omas-version-number*))
  
  ;; if we have a window already open close it
  (omas::%window-close *spy-window*)
  
  ;; if the agents arg is present, then override the saved list
  (when agents
    (if (every #'keywordp agents)
      (setf (omas::names-of-agents-to-display omas::*omas*) agents)
      (warn "the agent argument when present ~S should be a list of keywords." 
            agents)))
  
  ;; create new window
  (setq *spy-window*
        (omas::cr-window
         :class 'spy-window
         :type :document
         :title (format nil "OMAS ~A - Message Diagrams"
                        omas::*omas-version-number*)
         :left *spy-left-position*
         :top *spy-top-position*
         :width *spy-width*
         :height *spy-height*
         ;; MCL
         :scroller-class 'scrolling-window-scroller
         ;; MCL
         :track-thumb-p nil
         ;; MCL window has no dialog items
         #-MCL :dialog-items #-MCL (make-spy-window-widgets)
         ))
  
  ;; initialize some stuff
  ;; set color for header
  #+MCL
  (draw-header)
  #-MCL
  (print-agent-names)
  *spy-window*)


;;;---------- test function
(defun tt ()
  (spy::make-spy-window :agents (mapcar #'car (omas::local-agents omas::*omas*)))
  (draw-message 
   (omas::message-make :type :request :from :albert :to :all :action :zz))
  )
#|
(tt)
|#
;;;------------------------------------------------------- MAKE-SPY-WINDOW-WIDGETS
;;; this only applies to ACL

(defun make-spy-window-widgets ()
  "install a header and drawing pane into the window"
  (list
   ;; header for displaying agent names
   (make-instance 'cg:drawable
     :font (cg:make-font-ex nil "Lucida Sans Unicode / ANSI" 11 nil)
     :height 18 :left 0 :name :header 
     :on-redisplay 'draw-header
     :scrollbars nil :top 0 :width *spy-width*
     :background-color (cg:make-rgb :red 255 :blue 128 :green 255))
   ;; graphics area
   (make-instance 'cg:drawable 
     :font (cg:make-font-ex nil "Lucida Sans Unicode / ANSI" 11 nil)
     :height (- *spy-height* 18) :left 0 :name :draw 
     :on-redisplay 'repaint
     :top 18 :width (- *spy-width* 21))
   ;; scroll side
   (make-instance 'cg:VERTICAL-SCROLL-BAR 
     :HEIGHT (- *spy-height* 43) :LEFT (- *spy-width* 20) 
     :NAME :VS :ON-CHANGE 'spy-scroll :RANGE '(0 100)
     :background-color *light-grey-color*
     :TOP 19 :WIDTH 16)))

;;;---------------------------------------------------------------- CG::USER-CLOSE

(defMethod cg:user-close ((ww spy-window))
  (setq *spy-window* nil)
  (call-next-method))


;;;===============================================================================
;;;
;;;                         DRAWING AND SERVICE FUNCTIONS
;;;
;;;===============================================================================

;;;--------------------------------------------------- ADJUST-POSITION-FOR-DRAWING

(defun adjust-position-for-drawing (&key no-minimum date (offset 0))
  "computes the integer value of the drawing clock in order to space the arrows ~
      accordingly. Anything less than 1 second gets an offset value of 10, after 5 ~
      seconds the value is set to 50, in between values are rounded to the next ~
      multiple of 10. The time difference is computed with respect to the last ~
      time-value that was stored in the last-draw-time of the graphics window slot.
   Updates last-draw-time to current-time, and current-v-position slots.
   When offset is present adds it to the updated absolute position.
Arguments: 
   no-minimum (key): if t the returned adjustment can be 0.
   date (key): can specify time
   offset (key): additional value to add to the absolute vertical position
Return:
   0 the first time, the updated absolute virtual time."
  (let ((delta 0)
        (current-time (or date (get-universal-time))))
    (unless (last-draw-time *spy-window*)
      ;; first time around, last-draw-time is nil
      ;; set current-time
      (setf (last-draw-time *spy-window*) current-time)
      ;; update current position
      (incf (current-v-position *spy-window*) offset)
      (return-from adjust-position-for-drawing 
        (current-v-position *spy-window*)))
    
    ;; otherwise compute time difference
    (setq delta (- current-time (last-draw-time *spy-window*)))
    ;; adjust delta
    (setq delta
          (+ offset
             (cond ((<= delta 1) (if no-minimum 0 10))
                   ((> delta 5) 50)
                   ((* 10 (round delta))))))
    ;; record current time
    (setf (last-draw-time *spy-window*) current-time)
    ;; and clock value
    (incf (current-v-position *spy-window*) delta)
    ;; and return position to which we want to draw
    (current-v-position *spy-window*)))

#|
(adjust-position-for-drawing :no-minimum t)
2 ; first time around (absolute vertical value)
;;; wait more than 5 seconds, call again
(adjust-position-for-drawing :no-minimum t)
52
|#
;;;----------------------------------------------------------------- ADJUST-SCROLL

(defun adjust-scroll ()
  "checks if we get near the bottom of the graphics window. If so, then scroll ~
      the window up some. 
Arguments: none."
  (declare (special *spy-window*))
  (let* ((pane (omas::%find-named-item :draw *spy-window*))
         (pane-height (omas::%height pane))
         (abs-v-pos (current-v-position *spy-window*))
         (pane-offset (pane-offset *spy-window*))
         (rel-v-pos (- abs-v-pos pane-offset))
         )    
    ;; if relative position is greater than 9/10 of the window, compute offset so 
    ;; that last line is at 3/4 of the window
    (when
      ;; when after last 10% of window, scroll up
      (>= rel-v-pos (floor (* 9/10 pane-height)))
      ;; move drawing up
      (setf (pane-offset *spy-window*) 
            (- abs-v-pos (floor (* 3/4 pane-height))))
      ;; redraw window      
      (cg:invalidate pane)
      ;; adjust scroll finger
      (omas::%set-value 
       (omas::%find-named-item :vs  *spy-window*)
       (floor (* 100 (/ abs-v-pos (+ abs-v-pos (floor (* 1/4 pane-height)))))))
      )
    )
  ;; otherwise do nothing
  ;; return pane-offset
  (pane-offset *spy-window*))

;;;--------------------------------------------------------------- DRAW-ALL-STATES

(defun draw-all-states (&key date (offset 0))
  "draw lines to indicate the state of agents. The function is called before ~
      drawing a message to fill in the life lines.
Arguments:
   date (key): current real date (if not specified use current time)
   offset (key): additional vertical space used when displaying message text"
  (declare (special *spy-window* omas::*omas*))
  (when (omas::%window? *spy-window*)
    (let ((old-v-position (current-v-position *spy-window*))
          new-v-position)
      ;; we must then adjust our vertical drawing position
      ;; if date is not specified, then use current time
      (setq date (or date (get-universal-time)))
      ;; we must then adjust drawing position (updates slot of *spy-window*)
      (setq new-v-position 
            (adjust-position-for-drawing :no-minimum t :date date :offset offset))
      ;; if no time difference do not draw anything
      ;#+ALLEGRO-V8.1
      ;(ide::format-debug "~&DRAW-ALL-STATES/ date: ~S, old-pos: ~S, new-pos: ~S" 
      ;                   date old-v-position new-v-position)
      (when (eql new-v-position old-v-position) (return-from draw-all-states nil))
      ;; check first if the next value (end of life line) is outside the window
      (adjust-scroll)
      
      ;; we must draw here the state of all agents
      (dolist (agent-key (omas::names-of-agents-to-display omas::*omas*))
        ;; absolute positions
        (draw-state agent-key old-v-position new-v-position)
        ;; do we draw that here or elsewhere?
        (when (omas::drawing-timeouts omas::*omas*)
          ;(print '++++++)
          (draw-timeout-processes agent-key old-v-position new-v-position)))
      )
    (cg:invalidate (spy-pane))
    :done))

#|
(draw-all-states)
:DONE
|#
;;;-------------------------------------------------------------------- DRAW-ARROW

(defun draw-arrow (stream start end &optional (color *black-color*) 
                            (thickness *message-arrow-thickness*))
  "draw a color arrow from position start to end.
Arguments:
   stream: stream corresponding to drawing pane
   start: beginning of the arrow (absolute)
   end: side of the arrow (absolute)
   color (opt): default *black-color*
   thickness (opt): default *message-arrow-thickness*
Return:
   unimportant."
  (unless (cg:position= start end)
    (let* ((x-start (aref end 1))
           (y-start (aref end 2))
           (dx (- x-start (aref start 1))) ; dx horizontal
           (dy (- Y-start (aref start 2))) ; dy vertical
           (ll (sqrt (+ (* dx dx)(* dy dy)))) ; length
           (wh1 (round (/ (* *arrow-head-size* dx) LL)))
           (wv1 (round (/ (* *arrow-head-size* dy) LL)))
           (wh2 (round (/ (* *arrow-head-size* dy) LL 5)))
           (wv2 (round (/ (* *arrow-head-size* dx) LL 5))))
      ;; draw main line
      (draw-line stream start end color thickness)
      ;; ... one side of the arrow
      (draw-line stream end (cg:make-position (- x-start WH1 WH2) 
                                              (- Y-start WV1 (- WV2)))
                 color thickness)
      ;; ... the small crossing line
      (draw-line stream (cg:make-position (- x-start WH1 WH2) 
                                          (- Y-start WV1 (- WV2)))
                 (cg:make-position (- x-start WH1 (- WH2)) (- Y-start WV1 WV2))
                 color thickness)
      ;; ... the other side
      (draw-line stream (cg:make-position (- x-start WH1 (- WH2))
                                          (- Y-start WV1 WV2))
                 end color thickness))))

;;;------------------------------------------------------------------- DRAW-HEADER

(defun draw-header (widget stream)
  "called to refresh header with agent names"
  (declare (ignore widget))
  (if (omas::names-of-agents-to-display omas::*omas*)
    (print-agent-names)
    (cg:clear-page stream)))

#|
(draw-header)
|#
;;;---------------------------------------------------------------- DRAW-LIFE-LINE

(defun draw-life-line (agent-key h-offset v-start v-end 
                                    &optional (color *blue-color*) 
                                    (thickness *life-line-idle-thickness*))
  "draw some line and save the result onto the *display-list*.
Arguments:
   agent: agent to which the line belongs
   start: start of line, a position (absolute)
   end: end of line, a position (absolute)
   color: color (default *blue-color* for :idle)
   thickness: thickness (default *life-line-idle-thickness*)"
  (draw-line  
   (spy-pane) 
   (omas::%make-point (+ (h-pos agent-key) h-offset) v-start)
   (omas::%make-point (+ (h-pos agent-key) h-offset) v-end)
   color thickness)
  ;; save onto the display-list (save absolute position)
  (push (list :LIFE-LINE v-start agent-key h-offset v-start v-end color thickness) 
        (display-list *spy-window*)))

#|
(draw-life-line :albert 0 100 200)
|#
;;;--------------------------------------------------------------------- DRAW-LINE

(defun draw-line (pane start end &optional (color *black-color*) 
                         (thickness *life-line-idle-thickness*))
  "draw a line from start to end of color color with optional thickness
Arguments:
   ww: graphics window
   start: the starting absolute position
   end: the segment end
   color (opt): a given color (default black)
   thickness (opt): a given thickness (default 1)
Return:
  :done"
  ;(format t "~&.... drawing line ~S ~S ~S" (point-string start) (point-string end)
  ;        (point-string size))
  (let* ((offset (cg:make-position 0 (pane-offset *spy-window*))))
    (cg:with-line-width (pane thickness)
      (cg:with-foreground-color (pane color)
        (cg:draw-line pane (cg:position- start offset)
                      (cg:position- end offset)))))
  :done)

#|
(draw-line (spy-pane) (cg:make-position 100 100)(cg:make-position 300 300)
           *red-color* 3)
(draw-line (spy-pane) (make-point 100 100) (make-point 300 330) *red-color*)
|#
;;;------------------------------------------------------------------ DRAW-MESSAGE
;;: must change the message-draw-function to accommodate messages from non-local
;;; agents or agents that are not displayed
;;; let a hidden agent be either a non local agent or a non active agent, i.e.,
;;; one whose name is not in the list *names-of-agents-to-display*
;;; For a broadcast nothing is changed except the small square it the sender is 
;;; hidden
;;; for a message from a hidden agent to a displayed agent, the arrow must
;;; originate from the right side of the window
;;; for a message from a displayed agent to a hidden agent the arrow goes to the
;;; left side of the window
;;; we do not display messages from a hidden agent to a hidden agent

(defUn draw-message (message)
  "draw a message from an agent to another one as a color arrow. ~
   Checks that the window is there and active.
   Called from the display process.
   blue :request
   green :answer
   red  :error
Arguments:
   message: message to be drawn."
  (declare (special *spy-window* *spy-width* *message-arrow-thickness*))
  
  ;(format t "~%;== draw-message / *package*: ~S" *package*)
  ;(omas::message-print message)
  
  (unless
    (omas::%window? *spy-window*)
    (return-from draw-message :no-drawing-window))
  
  (when (draw-message-exception message)
    (return-from draw-message :uninteresting-message))
  
  (let ((sender-key (or (omas::from! message) (omas::local-user omas::*omas*)))
        (receiver-key (or (omas::to! message) (omas::local-user omas::*omas*)))
        (date (omas::date message))
        cloned-message)
    
    ;; when the receiver is the sender do not draw
    (when (eql sender-key receiver-key)
      (draw-self-message sender-key message)
      (return-from draw-message :internal-message))
    
    ;; here the receiver is a list and can be conditional or multicast
    (cond
     ;; test if receiver is conditional e.g. (:_cond <query>)
     ((and receiver-key (listp receiver-key)(eql (car receiver-key) :_cond))
      ;; in that case fake a broadcast for drawing purposes
      (setq receiver-key :all))
     ;; here we have a multicast, sending to several agents simultaneously.
     ((and receiver-key (listp receiver-key)) ; just in case
      ;;***** this part of code is untested
      ;; first take a copy of the message, that we are going to alter
      (setq cloned-message (omas::message-clone message))
      (dolist (to receiver-key)
        ;; then fake a send message to each receiver
        (setf (omas::to! cloned-message) to)
        ;; recursive call with single receiver
        (draw-message cloned-message))
      (return-from draw-message :done)))
    
    ;; if the message is a bid and we decided not to display bids, we quit right 
    ;; now
    (when (and (eql :bid (omas::type! message)) 
               ;(omas::graphics-no-bids omas::*omas*))
               (null (omas::graphics-no-bids omas::*omas*))) ; JPB0908
      (return-from draw-message :not-drawing-bids))
    
    ;; if one of the agents is hiding we do not display message
    (when (or (get sender-key :hide)(get receiver-key :hide))
      (return-from draw-message nil))
    
    ;; if we do not know sender, add it to the list. redraw window
    (unless (or (member sender-key '(:all :all-and-me))
                (eql sender-key (omas::local-user omas::*omas*))
                (member sender-key (omas::names-of-agents-to-display omas::*omas*)))
      (setf (omas::names-of-agents-to-display omas::*omas*)
            (append (omas::names-of-agents-to-display omas::*omas*)
                    (list sender-key)))
      ;; invalidate window and redraw it NOW!
      (cg:invalidate *spy-window* :children t :erase t)
      (cg:update-window *spy-window*)
      )
    
    ;;OK now we draw the message
    (sleep 0.1) ; to avoir overlaping messages on fast machines...
    (let* ((broadcasting (member receiver-key '(:all :all-and-me)))
           sender-h-pos
           sender-v-pos
           receiver-h-pos
           receiver-v-pos
           start end color 
           (thickness *message-arrow-thickness*)
           ;; we must adjust drawing time according to the message date
           current-v-position
           (drawing-text (omas::trace-messages omas::*omas*))
           )
      
      ;; we must draw here the state of all agents (updates current-v-position)
      ;; updated last-draw-position if trace is on, then add text spacing
      (draw-all-states :date date :offset (if drawing-text *text-spacing* 0))
      
      ;; check first if the next value is outside the displaying window
      (adjust-scroll)
      
      ;; if tracing messages, print the corresponding text
      (when drawing-text (draw-message-text message))
      
      ;; get updated absolute vertical position (done by draw-all-state)
      (setq current-v-position (current-v-position *spy-window*))
      
      ;; if the receiver is not known we should reset its p-list
      (unless (or (eql receiver-key (omas::local-user omas::*omas*))
                  (member receiver-key 
                          (omas::names-of-agents-to-display omas::*omas*)))
        ;; set origin to right hand side taking scroll into account
        (setf (get receiver-key :h-pos) *spy-width*)
        ;(setf (get receiver-key :v-pos) current-v-position)
        )
      
      ;; special case for broadcast messages : we draw an arrowless line across all
      ;; agents, i.e. from the first one to the last one
      (when broadcasting
        (setq sender-h-pos 0
              sender-v-pos current-v-position
              receiver-h-pos *spy-width*
              receiver-v-pos current-v-position)
        ;; we draw the origin of the broadcast only if the sender is displayed
        (when (member sender-key (omas::names-of-agents-to-display omas::*omas*))
          ;; draw a small square if the agent life line is on screen
          (draw-square sender-key (message-color message))))
      
      (unless broadcasting
        ;; compute start and end of line
        ;(format t "~&broadcasting?: ~S start:~S end: ~S" broadcasting sender receiver)
        ;; if the sender is an agent to display, and has a position, we use it
        (setq sender-h-pos 
              (cond 
               ;; if sender is the user (debugging) draw from the left
               ((eql sender-key (omas::local-user omas::*omas*))
                (setq sender-key (omas::local-user omas::*omas*))
                ;; local user sends from the left hand side
                (setf (get sender-key :h-pos) 0)
                0)
               ;; standard agent
               ((member sender-key (omas::names-of-agents-to-display omas::*omas*))
                ;; get position from key plist
                (h-pos sender-key))
               ;; otherwise, draw from the right
               (*spy-width*)))
        (setq sender-v-pos current-v-position)
        (setq receiver-h-pos 
              (cond
               ((member receiver-key 
                        (omas::names-of-agents-to-display omas::*omas*))
                (h-pos receiver-key))
               ((eql receiver-key (omas::local-user omas::*omas*))
                2)
               ;; any unknown agent is at the right of the pane
               ;; note that we cannot have a message from local user to itself
               (t *spy-width*)))
        (setq receiver-v-pos current-v-position)
        )
      
      ;(format t "~&==> sender: ~S sender-h-pos: ~S receiver: ~S receiver-h-pos: ~S"
      ;        sender-key sender-h-pos receiver-key receiver-h-pos)
      
      ;; set the color
      (setq color (message-color message))
      
      ;; if new vertical position is the same as previous one, increase it to 
      ;; avoid overwriting
      (when (<= sender-v-pos (last-v-position *spy-window*))
        (incf (current-v-position *spy-window*) 2)
        (incf sender-v-pos 2)
        (incf receiver-v-pos 2))
      
      (setq start (omas::%make-point sender-h-pos sender-v-pos)
            end (omas::%make-point receiver-h-pos receiver-v-pos)
            thickness *message-arrow-thickness*)
      
      ;; now draw the arrow or simple line in case of a broadcast
      ;(format t "~&..... draw line or arrow: ~S" 
      ;        (omas::message-get-slot-value  message 'omas::to))
      (if broadcasting
        (progn
          (draw-line (spy-pane) start end color thickness)
          (push (list :broadcast receiver-v-pos color thickness)
                (display-list *spy-window*))
          )
        (progn
          (draw-arrow (spy-pane) start end color thickness)
          (push 
           (list :message receiver-v-pos sender-key receiver-key color thickness)
           (display-list *spy-window*))
          ))
      
      ;; record where we are
      (setf (last-v-position *spy-window*) receiver-v-pos)
      )
    :done))

#|

1. create a window with only :SPY
? (make-spy-window :agents nil)
:DONE
? (omas::names-of-agents-to-display omas::*omas*)
(:SPY)

2. Send a message form user to :albert should draw an arrow from left to right
? (draw-message (omas::message-make :type :request :to :albert :contents 120))

3. introduce a new agent that is sending a message
? (draw-message 
   (omas::message-make :type :request :from :fac :to :albert :action :hello))
:DONE

4. Send a message from this agent to the user: :ALBERT should appear 
 (draw-message (omas::message-make :type :answer :from :albert :contents 120))

5. Send a multicast to a known and unknown agent
? (draw-message (omas::message-make :type :inform :from :albert :task-id -2
                   :to '(:fac :biblio) :action :hello :contents "hi"))

6. Introduce the unknown agent
? (draw-message (omas::message-make :type :request :from :biblio :to :albert
            :contents 120))

7. Use now the external function to check message
? (omas::message-draw 
     (omas::message-make :type :request :from :fac :to :albert :action :hello))

 (draw-message (omas::message-make :type :request :from :albert :to :all :action :zz))
 (tt)
|#
;;;-------------------------------------------------------- DRAW-MESSAGE-EXCEPTION
;;; take a message argument and uses a seris of patterns to decide if we want to
;;; drw the message or not. Reasons may be
;;; we do not want messages aassociated with a skill, e.g. :draw-parameters
;;; we do not want messages from a specific agent (:from ...)
;;; we do not want a message addressed to a specific agent (:to ...)
;;; we do not want messages between 2 specific agents (:between ...)
;;; this could be more complex if needed

#+MICROSOFT
(defmethod draw-message-exception ((msg o::message))
  "check if we want to avoid msg to be drawn into the graphics popup"
  (let ((sl (o::suppress-list *omas*)))
    (when sl
      (or
       (member (o::action msg) (cdr (assoc :action sl)))
       (member (o::from! msg) (cdr (assoc :from sl)))
       (member (o::to! msg) (cdr (assoc :to sl)))
       ;; if the link (from to) is included into the list of betweens
       (member (list (o::from! msg)(o::to! msg)) (cdr (assoc :between sl))
               :test 'o::equal+)
       ))))

#|
(setf (o::suppress-list o::*omas*)
  '((:action :draw-parameters)
    (:from :jules :zoe)
    (:to :junk :garbage)
    (:between (:zoe :jules) (:junk :zoe))))

(setq m1 
  (make-instance 'o::message :from :junk :to :garbage :action :draw-parameters))
(setq m2 
  (make-instance 'o::message :from :zoe :to :garbage :action :draw))
(setq m3
  (make-instance 'o::message :from :albert :to :junk :action :draw))
(setq m4
  (make-instance 'o::message :from :junk :to :zoe :action :draw))
(setq m5
      (make-instance 'o::message :from :albert :to :simone :action :draw))

? (draw-message-exception m1)
(:DRAW-PARAMETERS)
? (draw-message-exception m2)
(:ZOE)
? (draw-message-exception m3)
(:JUNK :GARBAGE)
? (draw-message-exception m4)
((:JUNK :ZOE))
? (draw-message-exception m5)
NIL
|#
;;;------------------------------------------------------------- DRAW-MESSAGE-TEXT
;;; should have agent-states as arg??

(defun draw-message-text (message)
  "prints the message content using the proper color unless it is a bid message ~
      and it was decided not to show them.
Arguments:
   message: message whose text must be displayed."
  (declare (special *spy-window* *spy-width*))
  ;; ACL does not print text in colors
  (when (omas::%window? *spy-window*)
    (let ((pane (spy-pane))
          ;; absolute vertical position is position of arrow, past the text
          ;; subtract offset ot put text on top of line
          (abs-v-pos (- (current-v-position *spy-window*) *text-spacing*))
          ;; prepare a summary of the message under the corresponding arrow
          (text (omas::message-format message))
          pos text-box)
      ;; compute relative position within pane
      (setq pos (- abs-v-pos (pane-offset *spy-window*)))
      ;; compute a box in which to write
      (setq text-box (cg:make-box 10 pos (- *spy-width* 10) (+ pos 20)))
      ;; redraw content to see message text
      ;; take into account scrolling to draw text
      (cg:draw-string-in-box pane text nil nil text-box :left :center)
      ;; record what is printed for scrolling the window
      (push (list :TEXT abs-v-pos text) (display-list *spy-window*))
      )
    :done))

#|
draw-string-in-rect (string rect
                                &key
                                (start 0)(end (length string))
                                truncation justification compress-p
                                ff ms color)
 - new function, string can contain multiple lines

Draw a string in a rectangle in the coordinates of the current focused view. 
Draws using #_ATSUDrawText.

Start and end specify the range of the string to draw.

Truncation can be one of
:none or NIL  - dont truncate
:start 
:center or :middle  
:end

Justification can be one of
:none or :left or NIL
:center or :middle
:end or :right

If compress-p is true, the text is squished/squashed/compressed if truncation
other than :none or NIL is also specified.

If ff, ms, and color are nil the values are obtained from the current grafport.

Use font-codes-string-width or font-codes-string-width-with-eol to measure text
that will be drawn with draw-string-in-rect.
|#

#|
(draw-message-text (omas::message-make :type :request :to :albert :contents 120))
|#
;;;------------------------------------------------------------- DRAW-SELF-MESSAGE

(defun draw-self-message (sender-key message)
  "draws a square to show self message and associated text
Arguments:
   sendr-key: the key of the sender of the message
   message: the message to display
Return:
   NIL"
  (declare (special omas::*omas* *text-spacing* *square-thickness* *spy-window*))
  (let((drawing-text (omas::trace-messages omas::*omas*))
       (date (omas::date message))
       (color (message-color message))
       )
    ;; if the agent is hiding we do not display message
    (when (get sender-key :hide)
      (return-from draw-self-message nil))
    
    ;;draw the state of all agents
    (draw-all-states :date date :offset (if drawing-text *text-spacing* 0))
    ;; check first if the next value is outside the displaying window
    (adjust-scroll)
    
    ;; draw text
    (when drawing-text (draw-message-text message))	
    ;; draw a small square
    (draw-square sender-key color)
    nil))

#|
(draw-self-message :NURSE-1
 (omas::message-make :type :request :from :NURSE-1 :to :NURSE-1 :contents 120))
|#
;;;------------------------------------------------------------------- DRAW-SQUARE

(defun draw-square (agent-key &optional (color *orange-color*))
  "draw a small horizontal colored notch. Used to indicate broadcasts.
Arguments:
   agent: agent
   color: (opt) drawing color (default orange)."
  (declare (special *spy-window*))
  (when (and (omas::%window? *spy-window*) 
             (member agent-key (omas::names-of-agents-to-display omas::*omas*)))
    (let ((agent-h-pos (h-pos agent-key))
          (v-pos (current-v-position *spy-window*)))
      (draw-line (spy-pane)
                 (omas::%make-point (- agent-h-pos 3) (1- v-pos))
                 (omas::%make-point (+ agent-h-pos 3) (1- v-pos))
                 color
                 *square-thickness*)
      (push (list :SQUARE v-pos agent-key color *square-thickness*)
            (display-list *spy-window*))
      )))

#|
? (draw-square :albert)
((0 :AGENT :ALBERT 65534 2 16737282 262148))
? (draw-square :fac *black-color*)
|#
;;;-------------------------------------------------------------------- DRAW-STATE

(defun draw-state (agent-key old-v-pos new-v-pos)
  "draw a vertical color bar to indicate the agent state (unknown for non local
   agents):
   thin blue is :idle or unknown
   thick red is :busy (i.e. has at least a task being active)
Arguments:
   agent: agent
   old-v-pos: previous vertical position (absolute)
   new-v-pos: new vertical position (absolute)"
  (declare (special *spy-window*))
  (let ((stream (spy-pane)) status)
    (when (and stream 
               (member agent-key (omas::names-of-agents-to-display omas::*omas*)))
      ;; set agent status
      (setq status (or (get agent-key :status) :unknown))
      ;#+ALLEGRO-V8.1
      ;(ide::format-debug "~&...drawing state/ agent: ~S~&  state: ~S status: ~S" 
      ;                   agent-key (symbol-plist agent-key) status)
      
      ;; draw line according to status ****** how about a :dead status?
      (case status
        (:busy
         (draw-life-line agent-key 0 old-v-pos new-v-pos *red-color* 3))
        (:idle
         ;; otherwise agent is idle or dead
         (draw-life-line agent-key 0 old-v-pos new-v-pos *blue-color* 1))
        ;; if non local agent draw a red line...
        (t
         (draw-life-line agent-key 0 old-v-pos new-v-pos *red-color* 1))
        )
      (when (omas::drawing-timeouts omas::*omas*)
        (draw-time-limit-processes agent-key old-v-pos new-v-pos)
        (draw-timeout-processes agent-key old-v-pos new-v-pos))
      ;;
      ;(sleep 0.02)
      )
    :done))

#|
(draw-state :albert 312 362)
|#
;;;===== life lines

;(trace (draw-state :step t))
;(untrace agent-draw-state)
;;;---------------------------------------------------------- DRAW-TIME-LIMIT-MARK

(defun draw-time-limit-mark (agent-key &optional (color *red-color*))
  "draw a horizontal red notch"
  ;; adjust drawing to reflect passed time
  (draw-all-states)
  (draw-square agent-key color)
  )

;;;----------------------------------------------------- DRAW-TIME-LIMIT-PROCESSES

(defun draw-time-limit-processes (agent-key old-v-pos new-v-pos)
  "draw a thin vertical green color bar to show agent time-limit process. Can't 
   do that for non local agents.
Arguments:
   agent-key: e.g. :albert
   old-v-pos: previous vertical possition (absolute)
   new-v-pos: current vertical position (absolute)
Return
   :done"
  (let ((stream (spy-pane))
        (agent (omas::local-agent? agent-key))
        (delta 5))
    (when (and stream
               agent ; nil if not a local agent
               (get agent-key :time-limits))
      ;; now see if we have something to draw
      (dotimes (jj (get agent-key :time-limits))
        (draw-life-line agent-key (- delta) old-v-pos new-v-pos *green-color* 
                        *life-line-idle-thickness*)
        (incf delta 3) ; spacing between 2 lines
        )
      )
    :done))

#|
(setq *display-list* nil)
|#
;;;------------------------------------------------------------- DRAW-TIMEOUT-MARK

(defun draw-timeout-mark (agent-key &optional (color *orange-color*))
  "draw a horizontal orange notch"
  ;; adjust drawing to reflect passed time
  (draw-all-states)
  (draw-square agent-key color)
  )

;;;-------------------------------------------------------- DRAW-TIMEOUT-PROCESSES

(defun draw-timeout-processes (agent-key old-v-pos new-v-pos)
  "draw a thin vertical orange color bar to show agent timeout process. Does not
   work for non local agents.
Arguments:
   agent: agent
   old-v-pos: previous value of the pseudo-clock
   new-v-pos: current value of the pseudo clock
   no-reset (opt): if t does not reset info on agent keys"
  (let ((stream (spy-pane))
        (delta 5)
        )
    (when (and stream 
               (member agent-key (omas::names-of-agents-to-display omas::*omas*))
               (get agent-key :timeouts))      
      ;; draw thin lines to the right of agent life line (as many as timeout timers)
      (dotimes (nn (get agent-key :timeouts))
        (draw-life-line agent-key delta old-v-pos new-v-pos *orange-color* 
                        *life-line-idle-thickness*)
        (incf delta 3))
      )
    :done))

;;;------------------------------------------------------------------------- H-POS

(defun h-pos (agent-name)
  "recovers the agent horizontal position from the plist of the agent-name. ~
      If undefined it is set to 0."
  (or (get agent-name :h-pos) 0))

;;;----------------------------------------------------------------- MESSAGE-COLOR

(defun message-color (message)
  "returns a different color for different types of messages
Arguments:
   message: a message object
Return:
   a color code"
  (case (omas::type! message)
    (:alert *orange-color*)
    (:request *blue-color*)
    (:answer 
     (case (omas::contents message)
       (:error *orange-color*)
       (otherwise *green-color*)))
    (:abort *black-color*)
    (:acknowledge *light-green-color*)
    (:call-for-bids *light-blue-color*)
    (:bid *tan-color*)
    (:bid-with-answer *green-color*)
    (:grant *blue-color*)
    (:kill *black-color*)
    (:revive *green-color*)
    (:inform *yellowish-color*) ; yellow 16497193
    (otherwise *red-color*)))

;;;------------------------------------------------------------- PRINT-AGENT-NAMES
#|
(print-agent-names)
|#
(defun print-agent-names ()
  "prints the agent names into the header of the graphics window, assuming that ~
      their horizontal position has been set on the plist of their key.
Arguments: none."
  (declare (special *spy-window* omas::*omas*))
  (when (omas::%window? *spy-window*)
    (let ((stream (cg:drawable-stream (cg:find-component :header *spy-window*)))
          to-display-list width half-width agent-h-pos)
      (when stream
        ;; first clear the header
        (cg:clear-page stream)
        ;; then compute the width of the interval between 2 agents
        (setq to-display-list (omas::names-of-agents-to-display omas::*omas*))
        (when to-display-list
          (setq
           width (floor (/ (cg:width *spy-window*)(length to-display-list)))
           half-width (floor (/ width 2)))
          ;; recompute agents position before printing their names
          (set-agent-display-position :no-reset t)
          ;; each agent name is to be contained centered inside a box of width width
          (dolist (agent-key (omas::names-of-agents-to-display omas::*omas*))
            ;; get horizontal position of the agent
            (setq agent-h-pos (h-pos agent-key))
            (cg:draw-string-in-box 
             (cg:drawable-stream (cg:find-component :header *spy-window*))
             (symbol-name agent-key) nil nil 
             (cg:make-box (- agent-h-pos half-width) 
                          0 (+ agent-h-pos half-width) 20)
             :center :center))))
      ;(cg:invalidate (cg:find-component :header *spy-window*))
      :done)))
#|
(print-agent-names)
|#

;;;----------------------------------------------------------------------- REPAINT
;;; Display-list formats:
;;;   :SQUARE
;;;      (:SQUARE v-pos agent-key color thickness)
;;;   :BROADCAST
;;;      (:BROADCAST v-pos color thickness)
;;;   :LIFE-LINE
;;;      (:LIFE-LINE v-pos agent-key h-offset v-start v-end color thickness)
;;;   :MESSAGE
;;;      (:MESSAGE v-pos sender-key receiver-key color thickness)
;;;   :TEXT
;;;      (:TEXT v-pos text)
;;;
;;; our problem is to adjust the drawing to the window, clipping what is outside, 
;;;    v-pos = v-abs-pos - (pane-offset *spy-window*)
;;; and to the h-position of the agents that may have changed.
;;;    h-pos = (h-pos agent-key) + offset or 0 or *spy-width*
;;; when redrawing window the pane-offset slot contains offset of current window
;;; We must thus add v-offset to the v-coord and subtract pane-offset. If the 
;;; result is negative of greater than *spy-height* we do not draw the item 
;;; (clipping)

(defun repaint (widget stream)
  "called when redrawing display"
  (declare (ignore widget)(special *spy-window* *spy-width*))
  
  (when (and *spy-window* (display-list *spy-window*)) ; JPB1111
    ;; make sure that messages issued by local user start from the left
    (setf (get (omas::local-user omas::*omas*) :h-pos) 0)
    (let ((pane-offset (pane-offset *spy-window*))
          v-pos h-start h-end)
      ;#+ALLEGRO-V8.1
      ;(ide::format-debug "~&DRAW-WINDOW-/ pane-offset: ~S" pane-offset)
      
      (dolist (item (reverse (display-list *spy-window*)))
        ;; set v-pos to the relative vertical position inside the window
        (setq v-pos (- (nth 1 item) pane-offset))
        ;; clip item
        (when (or (and (> v-pos 0)(< v-pos (cg:height (spy-pane))))
                  (eql (car item) :LIFE-LINE) ; let system clip
                  )
          
          (case (car item)
            
            (:LIFE-LINE
             ;; (:LIFE-LINE v-pos agent-key h-offset v-start v-end color thickness)
             (cg:with-line-width (stream (nth 7 item))
               (cg:with-foreground-color (stream (nth 6 item))
                 (cg:draw-line 
                  stream 
                  (cg:make-position 
                   (+ (h-pos (nth 2 item))(nth 3 item)) 
                   (- (nth 4 item) pane-offset))
                  (cg:make-position 
                   (+ (h-pos (nth 2 item))(nth 3 item)) 
                   (- (nth 5 item) pane-offset))))
               )
             ;; if no time difference do not draw anything
             ;#+ALLEGRO-V8.1
             ;(ide::format-debug "~&DRAW-WINDOW-/ v-start: ~S, v-end: ~S" 
             ;                   (- (nth 4 item) pane-offset)
             ;                   (- (nth 5 item) pane-offset))
             )
            (:SQUARE ; draw square at agent position
             ;; (:SQUARE v-pos agent-key color thickness)
             (cg:with-line-width (stream (nth 4 item))
               (cg:with-foreground-color (stream (nth 3 item))
                 (cg:draw-line 
                  stream 
                  (cg:make-position (- (h-pos (nth 2 item)) 2) (1- v-pos))
                  (cg:make-position (+ (h-pos (nth 2 item)) 2) (1- v-pos)))))
             )
            (:MESSAGE
             ;; (:MESSAGE v-pos sender-key receiver-key color thickness)
             (setq h-start (nth 2 item)
                 h-end (nth 3 item))
             ;; h-start and h-end may be numbers (0 *spy-width* or agent keys
             (unless (numberp h-start) (setq h-start (h-pos (nth 2 item))))
             (unless (numberp h-end) (setq h-end (h-pos (nth 3 item))))
             
             (draw-arrow stream 
                         (cg:make-position h-start (nth 1 item)) ; sender
                         (cg:make-position h-end (nth 1 item)) ; receiver
                         (nth 4 item) ; color
                         (nth 5 item) ; thickness
                         ))
            (:BROADCAST
             ;; (:BROADCAST v-pos color thickness)
             (cg:with-line-width (stream (nth 3 item))
               (cg:with-foreground-color (stream (nth 2 item))
                 (cg:draw-line stream 
                               (cg:make-position 0 v-pos) 
                               (cg:make-position *spy-width* v-pos)))))
            (:TEXT
             ;; (:TEXT v-pos text)
             (cg:draw-string-in-box 
              stream 
              (nth 2 item) nil nil 
              (cg:make-box 10 v-pos (- *spy-width* 10) (+ v-pos 20)) 
              :left :center)
             ))
          ))))
  
  t)

#|
(defun repaint (self)
  "testing function"
  (set-fore-color *spy-window* *green-color*)
  (paint-oval self 75 75 200 200)
  (frame-rect self 20 20 100 100)
  (erase-oval self 50 50 135 135)
  (move-to self (make-point 100 100))
  (prin1 "Albert" self)
  (set-fore-color *spy-window* *red-color*)
  (move-to self (make-point 0 0))
  (line-to self (make-point 300 800)))

(repaint (spy-pane))
|#
;;;---------------------------------------------------- SET-AGENT-DISPLAY-POSITION

(defun set-agent-display-position (&key no-reset)
  "for each agent of the display list compute its horizontal position in window  ~
      and reset vertical position to 0. Reset display list.
Arguments: 
   agent-list: list of agent keywords
   no-reset (key): if t we keep display-list and scroll offset
Return:
   :done"
  (declare (special *spy-window*))
  (let ((agent-list (omas::names-of-agents-to-display omas::*omas*)))
    ;; do something only when there are agents to draw and a drawing window
    (when (and agent-list (omas::%window? *spy-window*))
      (let* ((number (length agent-list))
             (delta (round (/ (omas::%width *spy-window*) number)))
             (offset (- (floor (/ delta 2)))))
        
        ;; reset display list if new window (resetting window when adding new agent
        ;; is done by specifying no-reset)
        (unless no-reset
          (setf (display-list *spy-window*) nil)
          (setf (pane-offset *spy-window*) 0)
          
          )
        ;; recompute agent positions and set up agent position
        (dolist (agent-key agent-list)
          (setf (get agent-key :h-pos) (incf offset delta))
          )
        ;; reset user position
        (setf (get (omas::local-user omas::*omas*) :h-pos) 2)))
    :done))

;;;---------------------------------------------------------------------- SPY-PANE

(defun spy-pane ()
  "get the drawing stream for the graphics window"
  (when *spy-window*
    (cg:drawable-stream (cg:find-component :draw *spy-window*))
     ))

;;;-------------------------------------------------------------------- SPY-SCROLL

(defun spy-scroll (widget new-value old-value)
  "callback related to moving the scroll thumb"
  (declare (ignore old-value)(special *spy-window*))
  (let* ((pane (cg:find-sibling :draw widget))
         max-position)    
    (when pane 
      (setq max-position (max (current-v-position *spy-window*) (cg:height pane)))
      ;; update pane-offset to be the value set by the scroller mark
      (setf (pane-offset *spy-window*) (floor (/ (* new-value max-position) 100)))
      (cg:invalidate pane)
      ))
  t)


; (graphics-set-agent-display-position)

(format t "~&;*** OMAS v~A - spy window loaded ***" omas::*omas-version-number*)

;;; :EOF
