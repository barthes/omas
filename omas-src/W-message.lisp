﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;                O M A S - W - M E S S A G E  (file W-message.lisp)
;;;
;;;===============================================================================
;;; This file contains all the functions related to the message window for building
;;; or sending messages in the debug mode.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| 
2020
 0202 restructuring OMAS as an asdf system
|#

(in-package :omas)

;;;================================ Globals ======================================
;;; emw: edit-message-window

(defparameter *emw-font*  
  (cg:make-font-ex :swiss "MS Sans Serif" 11 nil)
  )
(defparameter *emw-label-font* 
  (cg:make-font-ex nil "Lucida Sans Unicode / ANSI" 11 nil)
  )
(defparameter *emw-value-font*
  (cg:make-font-ex nil "Lucida Sans Unicode / ANSI" 11 nil)
  )

;;;================================= Window ======================================
;;; MESSAGE-WINDOW is a subclass of dialog

(defClass MESSAGE-WINDOW (#+mcl dialog #-mcl cg:dialog)
  ((message-slot-list :accessor message-slot-list :initform nil)
   (message :accessor message :initform nil))
  (:documentation 
      "window to edit the various fields of a message prior to sendint it"))

;;;----------------------------------------------------------- MAKE-MESSAGE-WINDOW
#|
(make-message-window)
|#

(defun make-message-window (&key title)
  (let ((message-name (or title (%message-make-name))) properties height win)
    ;; get properties from class MESSAGE, using meta-protocol functions
    (setq properties 
          (mapcar #'mop::slot-definition-name
                  (mop::compute-slots (find-class 'message)))
          )
    
    ;; compute window height <actually bottom-right corner?>
    (setq height (+ 255 (* 20 (length properties))))
    
    ;; then, create window
    (setq win
          (cr-window
           :class 'message-window
           :left 200 :top 200 :right 500 :bottom height
           :title (symbol-name message-name)
           :name :message-window
           :font *emw-font*
           :background-color (%rgb (* 256 255) (* 256 255) (* 255 128))
           :subviews (make-message-widgets properties)
           ))
    ;; preset the name slot with the message name
    (omas::%set-value (omas::%find-named-item :name-slot win) 
                      (format nil "~S"  message-name))
    ;; store the list of message slots into window slot
    (setf (message-slot-list win) properties)
    ;; record the new window into the edit-message-window list
    (push win (edit-message-window-list *omas*))
    win))

;;; when closing the window we remove the message object from the list of such objects

(defMethod cg::user-close ((ww message-window))
  (setf (edit-message-window-list *omas*)
        (remove ww (edit-message-window-list *omas*) :test #'equal))
  (close ww))

;;;---------------------------------------------------------- MAKE-MESSAGE-WIDGETS

(defun make-message-widgets (properties)
  "build the slot entries for the message to be displayed using the list of ~
      properties. When this function is called, the list of properties is not ~
      yet saved into the winwow object.
Arguments:
   properties: fields of a message object
Return:
   a list of widgets for displaying the fields."
  (let ((count 0)
        slot-views)
    (append
     (dolist (item properties (reverse slot-views))
       ;;--- write label
       (push
        (cr-static-text 
         :left 5 :top (+ 5 (* count 20))
         :font *emw-label-font*
         :width 110
         :text (format nil "~A" item)
         )
        slot-views)
       ;;--- define input frame
       (push
        (cr-text-item
         :left 110 :top (+ 5 (* count 20)) :width 180 :height 18
         :name (intern
                (concatenate 'string (symbol-name item) 
                  (get-text *WMSG-slot-suffix*))
                :keyword)
         :font *emw-value-font*
         )
        slot-views)
       (incf count))
     ;; === button for sending message
     (list
      (cr-button
       :left 105 :top (+ 10 (* count 20)) :height 16
       :name :send-button
       :title (get-text *WMSG-send*)
       :on-click message-send-on-click
       :font *emw-label-font*
       )
      ))))

;;;================================ Functions ====================================

;;;------------------------------------------------------------- %MESSAGE-FROM-KEY

(defun %message-from-key (key) (cdr (assoc key (user-messages *omas*))))

;;;------------------------------------------------------- MESSAGE-BUILD-FROM-VIEW

(defun message-build-from-view (view)
  "called when sending message from a view in which values have been edited. ~
      Creates a new message object and returns a pointer to it.
Arguments:
   view: view for editing the message
Return:
   pointer to new message object."
  (let ((mm (make-instance 'message)) data message-name)
    ;; for each property read corresponding value
    (dolist (prop (message-slot-list view))
      (setq data 
            (%get-value 
             (%find-named-item
              (intern (concatenate 'string (symbol-name prop) 
                        (get-text *WMSG-slot-suffix*)) :keyword)
              view)))
      ;(print `(,prop ,data))
      ;; remove eventual trailing blanks
      (unless (equal (remove #\space data) "")
        (setf (slot-value mm prop)
              (message-parse-value prop data))))   
    
    ;; if message has been given a name, then use it, otherwise cook up a new  one
    (setq message-name (or (and (slot-boundp mm 'name) (name mm))
                           (gentemp "MM-")))
    ;; messages do not have names but keys recorded onto the *user-messages* list
    (setq message-name (keywordize message-name))
    (setf (user-messages *omas*) 
          (remove message-name (user-messages *omas*) :key #'car))
    (push (cons message-name mm) (user-messages *omas*))
    
    ;;;    ;; declare message name as a global variable
    ;;;    (eval (list 'defvar message-name))
    ;;;    ;; record message for further use
    ;;;    (set message-name mm)
    ;; record name (key) into the message (usefull when new)
    (setf (name mm) message-name)
    mm))

;;;-------------------------------------------------------- MESSAGE-DISPLAY-VALUES

(defun message-display-values (dialog message)
  "takes a message and displays the field values. When message is then ~
      change the title of the window to New Message.
Arguments:
  dialog: message window
  message: message containing values to display
Return:
  unimportant."
  (let (slot-name val)
    (when message
      ;; when message exists, then display its content 
      ;;first title is the message name
      (%set-item-text (%find-named-item :name-slot dialog) (string (name message))))
    ;; for each message slot, display its value
    (dolist (slot (message-slot-list dialog))
      (setq slot-name (intern (concatenate 'string (symbol-name slot) 
                                (get-text *WMSG-slot-suffix*)) 
                              :keyword))
      ;; get slot value from message object
      (setq val (message-get-slot-value message slot))
      ;; if there display it; otherwise, clear field
      (if val
        (%set-value (%find-named-item slot-name dialog)
                    (string-downcase (format nil "~S" val)))
        (%set-item-text (%find-named-item slot-name dialog) "")))
    ;; when message does not exist create it
    (unless message 
      (%set-item-text dialog (get-text *WMSG-new-message*)))
    ))

;;;------------------------------------------------------------------ MESSAGE-EDIT

(defun message-edit (message &aux ww)
  "edit a given message in a temporary window.
Arguments:
   message: message to edit
Return:
   a pointer to the window (that might be closed)."
  ;; create the tem?orary window or use an available one
  (setq ww (make-message-window :title (name message)))
  ;; record the message into the window structure (why not?)
  (setf (message ww) message)
  ;; call the function to display the initial values
  (message-display-values ww message)
  ;; return a pointer to the window
  ww)

;;;-------------------------------------------------------- MESSAGE-GET-SLOT-VALUE

(defun message-get-slot-value (message prop)
  "get info from a message slot to put it into the  message edit window ~
      in such a way that it can be read back.
Arguments:
   prop: name of slot
   message: object message
Return:
   the value as a string."
  ;; get value from the message object (mind reserved words, ACL!)
  (case prop
    (from (from! message))
    (to (to! message))
    (type (type! message))
    (otherwise
     ;; we should use accessors when defined, e.g. (funcall prop message)
     (slot-value message prop))
    ))

;;;------------------------------------------------------------------- MESSAGE-NEW

(defun message-new (&aux ww)
  "opens an edit temporary window to create a new message.
Arguments:
   none
Return:
   a pointer to the window (that might be closed)."
  ;; create the temporary window or use an available one
  (setq ww (make-message-window))
  ;; call the function to display the initial values
  (%set-item-text ww "New Message")
  ;; return a pointer to the window
  ww)

;;;----------------------------------------------------------- MESSAGE-PARSE-VALUE

(defun message-parse-value (prop data)
  "parsing string data attached to a particular message slot. Used when building ~
      messages interactively through the message window.
   Date slot always return the current time.
Arguments:
   prop: some property of the message
   data: string obtained from the subview area
Return:
   the value obtained from reading the string."
  (case prop
    (date
     (get-universal-time))
    ((from reply-to)
     (keywordize (read-from-string data)))
    (to 
     ;; do not keywordize conditional addressing (:_cond <query>)
     (let ((receiver (read-from-string data)))
       (if (and (listp receiver)(eql (car receiver) :_cond))
           receiver
         (keywordize receiver))))
    (otherwise
     (read-from-string data))))

;;;--------------------------------------------------------- MESSAGE-SEND-ON-CLICK

(defun message-send-on-click (dialog widget)
  "function called when clicking the send button. Creates a new message from the ~
      content of the temporary message window.
Arguments:
   dialog: message window
   widget: the send button
Return:
   t"
  (declare (ignore widget))
  (let* ((message (message-build-from-view dialog)))
    ;; record in the list of user messages
    ;(push message-name *user-messages*)
    ;; send it
    (send-message message)
    ;; kill temporary window
    (close dialog)
    ;; remove it from the list
    (setf (edit-message-window-list *omas*) 
          (remove dialog (edit-message-window-list *omas*)))
    ;; refresh message list in control panel?
    ;(display-selection-list *agent-window* *user-messages*)
    t))

(format t "~&;*** OMAS v~A - message window loaded ***" *omas-version-number*)

;;; :EOF
