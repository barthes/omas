﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;                O M A S - W - A G E N T  (file W-agent.lisp)
;;;
;;;===============================================================================
;;; This file contains all the functions related to agent windows for displaying
;;; the internals of an agent during a OMAS execution. Used by CONTROL.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| 
2020 
 0202 restructuring OMAS as an asdf system
|#

(in-package :omas)

#| 
;;;================================ Debug ========================================
(defParameter *display-command-queue* nil)
(defParameter *omas-version-number* 5.0)
(defclass agent ()
  ((agenda :accessor agenda :initarg :agenda :initform nil)
   (input-messages :accessor input-messages :initarg :input-messages :initform nil)
   (name :accessor name :initarg :name :initform nil)
   (window :accessor window :initform nil)
   (environment :accessor environment :initarg :environment :initform nil))
  )
(defparameter fac (make-instance 'agent :name 'fac :agenda '(MM1 MM2)
                    :input-messages '(MM3 MM4) :environment '(:albert t)))
(defclass task ()
  ((id :accessor id :initarg :id :initform nil)))
(defparameter T1 (make-instance 'task :id 'T-1))
(defparameter T2 (make-instance 'task :id 'T-2))
(defun active-task-list (agent) (declare (ignore agent)) (list T1 T2))
(defun message-format (mm) mm)
(defun message-limit-text-length (mm nn) (declare (ignore nn)) mm)
(defun string-at-most (strg nn) (declare (ignore nn)) strg)
;;;============================== End debug ======================================
|#

;;;=============================== Globals =======================================

(defParameter *agent-window-slots* '(:empty :empty :empty :empty :empty :empty) 
  "We can display at most 6 agent windows simultaneously")

(defParameter *aw-h-position* #+MCL (- *screen-left-offset* 4) #-MCL 0)
(defParameter *aw-v-position* (+ *panel-height-offset* *panel-height* 2 #+MCL 46))
(defParameter *aw-width* *panel-width*)
(defParameter *aw-height* 114)

(defParameter *aw-display-width* (floor (/ (- *aw-width* 18) 2)))

(defParameter *white* #+MCL #xFFFFFF #-MCL cg:white)

(defParameter *aw-label-font* 
  (cg:make-font-ex nil "Tahoma / ANSI" 11 nil)
  )

;;; used by ACL for tracking ontology windows
(defParameter *outline-window-info* nil)

;;;----------------------------------------------------------------- AGENT-DISPLAY

(defUn omas::agent-display (agent)
  "New version: append a display message to the display-command-queue ~
   to redisplay the content of the agent window.
      This function may be called anywhere during the computation. However, ~
   the display order is passed to the process that created the window on ~
   the display-command-queue (required by ACL)."
  (%agent-display agent))

;;;---------------------------------------------------------------- %AGENT-DISPLAY

;ACL (A changer)
(defMethod %agent-display ((agent agent))
  "update agent window by filling the various fields. This method should be ~
   called by the process that created the window. The window must exist.
Arguments:
   agent: agent to display
Return:
  unimportant."
  (let ((win (omas::window agent)))
    (when (omas::%window? win)
      (let* ((task-list (omas::active-task-list agent))
             ;(environment (omas::environment agent))
             message-log)
        ;; update status coloring the input-messages area
        (omas::%set-item-color (omas::%find-named-item :input-messages win)
                               (AW-status-color agent)) 
        ;; post task list (should limit the # of chars)
        (omas::%set-item-text 
         (omas::%find-named-item :task-list win)
         (if task-list 
           (omas::string-at-most (format nil "~{~S~^,~}" 
                                         (mapcar #'omas::id task-list))
                                 60)
           (get-text *WAG-tasks*)))
        ;; show action and arg-list, limiting to 60 characters
        ;; ... can't do that since agent is multi-threaded...
        ;; post agenda
        (omas::%set-item-sequence 
         (omas::%find-named-item :agenda win)
         (mapcar #'omas::message-format (omas::agenda agent)))
        ;; post last result (not yet)
        ;; post subtasks (not yet)
        ;; post environment (useless)
        ;; post the last 20 received messages
        (setq message-log (input-log agent))
        (if (> (length message-log) 20) 
          (setq message-log (reverse (nthcdr 20 message-log)))
          (setq message-log (reverse message-log)))
        (omas::%set-item-sequence 
         (omas::%find-named-item :input-messages win)
         (mapcar #'omas::message-format message-log))
        ))))

;;;----------------------------------------------------------------- DISPLAY-AGENT

(defUn display-agent (agent &aux pos win agent-list)
  "try to display an agent. Check *OMAS-agent-window-slots* for an empty slot. ~
   if none, then we cannot display the agent. The user must first make some room ~
   Otherwise, we take the first available space to display the agent."
  
  ;; check first if agent is not already displayed
  (setq agent-list (remove :empty *agent-window-slots*))
  ;(format t "~%; display-agent /agent: ~S agent-list~%  ~S" agent agent-list)
  
  (unless (member agent agent-list :key #'(lambda(xx)(agent xx)))
    (when (member :empty *agent-window-slots*)
      ;; get position of the :empty spot and create new agent window
      (setq pos (position :empty *agent-window-slots*))
      (setq win (cond
                 ;; a postman has a special indow
                 ((typep agent 'postman)
                  (make-postman-window agent (1+ pos)))
                 (t (make-agent-window agent (1+ pos)))))
      ;; record rank of newly created window
      (setf (rank win) pos)
      ;; record window name in agent
      (setf (omas::window agent) win)
      ;; reset list
      (setf (nth pos *agent-window-slots*) win)))
  ;; in any case show agent winndow
  (when (window agent)
    (cg:set-foreground-window (window agent)))
  )

;(omas::display-agent fac:fac)
;(omas::display-agent albert::albert)
;;;===================== Classes and methods =====================================

#|
;;; unused
(defClass OMAS-AGENT-BUTTON (#-mcl button #+mcl button-dialog-item)
  ())
|#

(defClass omas-text-area (#-mcl cg:editable-text #+mcl editable-text-dialog-item)
  ())

(defClass omas-read-only-text-area (#-mcl cg:static-text #+mcl static-text-dialog-item)
  ())

#|
;;; defined in W-control-panel

(defClass OMAS-BUTTON (#+mcl button-dialog-item #-mcl cg:button)
  ((dummy)))

(defClass AREA-FOR-VALUES (#+mcl sequence-dialog-item
                           #-mcl cg:multi-item-list)
  ((original-list)) ; list to be displayed in the table area
  )
|#

;; We define an agent window
(defClass omas-agent (#+mcl dialog #-mcl cg:dialog)
  ((rank :accessor rank)
   (agent :accessor agent)
   (known-postmen :accessor known-postmen :initform nil))
  (:documentation
   "Display an agent in a specific slot (rank) below control pannel"))

(defMethod print-object ((mm omas-agent) stream)
  (format stream "#<AGENT-WINDOW: ~A>"
          (name (agent mm))))


;;;===============================================================================
;;;
;;;                                 AGENT WINDOW
;;;
;;;===============================================================================

;;;------------------------------------------------------------- MAKE-AGENT-WINDOW
; (make-agent-window hello::hello 2)

(defUn make-agent-window (agent position &aux win)
  "Creates a window for displaying an agent"
  ;; create window in the agent package
  (setq win
        (cr-window 
         :class 'OMAS-agent
         ;:type :single-edge-box
         :type :document
         :title (format nil "~S" (name agent))
         :left *aw-h-position* 
         :top (aw-compute-vpos position)
         :right (+ *aw-h-position* *aw-width*)
         :bottom (+ (aw-compute-vpos position) *aw-height*)
         :close-button nil
         :title-bar nil
         :background-color (omas::%rgb 64764 48059 51143) ; pink
         :dialog-items (make-agent-window-widgets agent))
        )
  ;; record agent into the agent slot of the window
  (setf (agent win) agent)
  ;; record position
  (setf (rank win) position)
  ;; return instance id
  win)

#|
(make-agent-window albert::albert 1)
|#
;;;----------------------------------------------------- MAKE-AGENT-WINDOW-WIDGETS

(defUn make-agent-window-widgets (agent)
  "defines the different widgets and pane of an agent window."
  (let ((label-font 
         (cg:make-font-ex nil "Lucida Sans Unicode / ANSI" 11 nil)))
    (list  
     ;; === first row: exit, ontology, trace, export
     ;; close box, agent name is associated with this button
     (cr-check-box
      :left 5 :top 6 :height 15
      :title (format nil "~A" (omas::name agent))
      :title-width 110
      :font label-font
      :on-change aw-close-on-click)
     
     ;;=== RESTORE
     (cr-button 
      :class 'OMAS-button
      :left (- *aw-width* (* 5 80) 5) :top 6 :width 75 :height 16
      :title (get-text *WAG-restore*)
      :font label-font
      :on-click aw-restore-on-click
      :name :restore-button) 
     
     ;;=== DETAILS
     (cr-button 
      :class 'OMAS-button
      :left (- *aw-width* (* 4 80) 5) :top 6 :width 75 :height 16
      :title (get-text *WAG-details*)
      :font label-font
      :on-click aw-details-on-click
      :name :details-button)
     
     ;;=== ONTOLOGY
     (cr-button 
      :class 'OMAS-button
      :left (- *aw-width* (* 3 80) 5) :top 6 :width 75 :height 16
      :title "<unused>" ;(get-text *WAG-ontology*)
      :font label-font
      :on-click aw-ontology-on-click
      :name :ontology-button)
     
     ;;=== TRACE
     (cr-button 
      :class 'OMAS-button
      :left (- *aw-width* (* 2 80) 5) :top 6 :width 75 :height 16
      :title (if (typep agent 'omas::assistant)
                 (get-text *WAG-dialog*)
               (get-text *WAG-checkpoint*))
      :font label-font
      :on-click aw-trace-on-click
      :name :trace-button)
     
     ;;=== EXPORT
     (cr-button 
      :class 'OMAS-button
      :left (- *aw-width* (* 1 80) 5) :top 6 :width 75 :height 16
      :title (get-text *WAG-export*)
      :font label-font
      :on-click aw-export-on-click
      :name :export-button)
     
     ;;=== second row: print current list of tasks
     (cr-static-text 
      :class 'omas-read-only-text-area
      :left 7 :top 27 :width (- *aw-width* 12) :height 12
      :title (aw-make-task-id-list agent)
      :font *aw-label-font*
      :color *white*
      :name :task-list
      )
     
     ;; === third row: input-messages and agenda           
     ;;=== INPUT MESSAGES
     (cr-multi-item-list 
      :class 'area-for-values
      :left 5 :top 45 :height 60 :cell-width *aw-display-width*
      :title (get-text *WAG-input-messages*)
      :cell-height 15
      ;:cell-color (AW-status-color agent)
      :font *aw-label-font*
      :name :input-messages 
      :sequence (mapcar #'omas::message-format (omas::input-messages agent))
      :hscrollp nil :vscrollp t)
     
     ;;=== AGENDA
     (cr-multi-item-list 
      :class 'area-for-values
      :left (+ *aw-display-width* 12) :top 45 :height 60 
      :cell-width *aw-display-width* 
      :title (get-text *WAG-agenda*)
      :cell-height 15
      ;:cell-color (AW-status-color agent)
      :font *aw-label-font*
      :name :agenda 
      :sequence (mapcar #'omas::message-format (omas::agenda agent))
      :hscrollp nil :vscrollp t)
     )))

;;;===============================================================================
;;;                    Callbacks and service functions
;;;===============================================================================

;;;------------------------------------------------------------- AW-CLOSE-ON-CLICK

(defUn aw-close-on-click (item new-value old-value)
  "clear position on list and close agent window."
  (declare (ignore new-value old-value))
  (let ((dialog (%container item)))
    ;; reset global list of agent windows
    (setf (nth (rank dialog) *agent-window-slots*) :empty)
    ;; reset agent window slot
    (setf (omas::window (agent dialog)) nil)
    ;; and close window
    (%window-close dialog)))

;;;--------------------------------------------------------------- AW-COMPUTE-VPOS

(defUn aw-compute-vpos (rank)
  "computes the vertical position of an agent window according to its rank 1-6."
  (+ *aw-v-position* (* (1- rank) *aw-height*)))

;;;----------------------------------------------------------- AW-DETAILS-ON-CLICK

(defUn aw-details-on-click (dialog widget)
  "open a big temporary window into which internals of the agent are displayed"
  (declare (ignore widget))
  (let ((pg-width (cg:page-width (cg:screen cg:*system*)))
        (pg-height (cg:page-height (cg:screen cg:*system*)))
        dwin-height dwin-width box)
    ;; compute size of the detail screen with respect to the size of the screen
    (setq dwin-width (- pg-width 200)
          dwin-height (- pg-height 200))
    ;; when screen is too small, complain and quit
    (when (or (< dwin-width 0) (< dwin-height 0))
      (warn "page size too small to display detailed screen: ~Sx~S"
            pg-width pg-height)
      (return-from AW-details-on-click nil))
    ;; compute outside box
    (setq box (cg:make-box 100 100 (floor (* 3/4 pg-width))
                           (floor (* 3/4 pg-height))))
    ;; create and build new window to show the inside of an agent
    (make-agent-detail-window :exterior box :agent (agent dialog)
                              :title (symbol-name (name (agent dialog))))
    ;; we return nothing interesting
    :detail-window-created))

;;;------------------------------------------------------------ AW-EXPORT-ON-CLICK

(defUn aw-export-on-click (dialog widget)
  "if agent is persistent, exports ontology and KB using MOSS format into a file 
   <AGENT>-ONTOLOGY-<DATE>.lisp in the application folder."
  (declare (ignore widget))
  (let (file-pathname) 
    ;; execute only if agent is persistent
    (when (persistency (agent dialog))
      ;; if so make file pathname
      (setq file-pathname 
            (make-pathname 
             :name (format nil "~A-ONTOLOGY-~A" (key (agent dialog))
                     (moss::get-current-date :compact t))
             :type "lisp"
             :defaults (omas-application-directory *omas*)))
      ;; if file exists, complain
      (if (probe-file file-pathname)
          (progn
            (%beep)
            (warn "export file (~A) already exists" file-pathname))
        ;; otherwise, call the make function in the right package
        (with-package (find-package (key (agent dialog)))
          (moss::make-ontology-file :file-pathname file-pathname))))
    ))

;;;----------------------------------------------------------- AW-INSPECT-ON-CLICK

(defUn aw-inspect-on-click (dialog widget)
  "opens an inspection window for the agent."
  (declare (ignore widget))
  (inspect (agent dialog)))

;;;---------------------------------------------------------- AW-MAKE-TASK-ID-LIST

(defUn aw-make-task-id-list (agent)
  "makes a string for displaying task-id list."
  (let ((task-list (omas::active-task-list agent)))
    (if task-list
      ;(format nil "~S" (task-id task-list))
      (omas::message-limit-text-length
       (format nil "~{~S ~}" 
               (mapcar #'omas::id task-list))
       150)
      (get-text *WAG-tasks*)
      )))

;;;---------------------------------------------------------- AW-ONTOLOGY-ON-CLICK

(defUn aw-ontology-on-click (dialog widget)
  "ontology is triggered from the control panel."
  (declare (ignore dialog widget))
  (cg:beep)
  (warn "Please open ontology from the control panel")) 

;;;----------------------------------------------------------- AW-RESTORE-ON-CLICK

(defun aw-restore-on-click (dialog widget)
  "restore database from dump file"
  (declare (special moss::*verbose*))
  (let ((agent (agent dialog)))
    ;; call the proper function (will take care of persistency check)
    (restore-base agent)
    )
  t)

;;;--------------------------------------------------------------- AW-STATUS-COLOR

(defUn aw-status-color (agent)
  "pick up color according to activity"
  (if (omas::active-task-list agent)
    ;; red when busy
    (cg:make-rgb :red 255) 
    ;; white otherwise
    (cg:make-rgb :red 255 :blue 255 :green 255)))

;;;--------------------------------------------------------------- AW-STATUS-LABEL

(defUn aw-status-label (agent)
  "pick up short label according to activity"
  (if (active-task-list agent)
    "BUSY"
    "IDLE"))

;;;------------------------------------------------------------- AW-TRACE-ON-CLICK

(defUn aw-trace-on-click (dialog widget)
  "dialog trace mechanism for personal assistants, and dump database content for ~
   persistent service agents."
  (declare (special moss::*verbose*))
  (let ((agent (agent dialog)))
    ;; if personal assistant open dialog trace
    (when (typep agent 'omas::assistant)
      (setq moss::*verbose* t)
      (unless (text-window *omas*)
        (make-trace-dialog-window))
      (return-from aw-trace-on-click))
    ;; otherwise, dump ontology and knowledge base if agent is persistent
    (dump-base agent)
    )
  t)
	  
;;;===============================================================================
;;;
;;;                           AGENT DETAIL WINDOW
;;;
;;;===============================================================================

;;; build floating window for displaying detailed agent infos

(defClass agent-detail-window (#-mcl cg:frame-window #+mcl window)
  ((agent-object)))

;;;--------------------------------------------------------------- AD-BUILD-STRING

(defUn ad-build-string (agent)
  "builds a huge string to be displayed in the detail window"
  (concatenate 'string
               (ad-sub-items agent :COMM 
                             :input-messages)
               (ad-sub-items agent :SELF 
                             :data
                             :features
                             :goals
                             :intentions
                             :memory)
               (ad-sub-items agent :WORLD 
                             :acquaintances
                             :environment
                             :external-services)
               (ad-sub-items agent :CONTROL
                             :status
                             ;:previous-status
                             :agenda
                             ;:task-in-progress
                             ;:last-subtask-result
                             :active-task-list
                             :pending-bids
                             :process-list
                             :saved-answers)
               (ad-sub-items agent :TASKS
                             :projects
                             :waiting-tasks)
               (ad-sub-items agent :APPEARANCE
                             :window
                             :com-window
                             :thread)
               (ad-sub-items agent :SKILLS
                             :skills)
               (ad-sub-items agent :ONTOLOGIES
                             :domain-ontology
                             :ontology-package
                             :moss-context
                             :moss-version-graph
                             :language)
               ;; when the agent is an assistant se display master-bins
               (when (assistant? agent)
                 (ad-sub-items agent :MASTER-BINS
                               :waiting-messages
                               :pending-requests
                               :waiting-answers
                               :discarded-messages
                               :to-do
                               :conversation
                               :dialog-header
                               :conversation-state
                               :conversation-context
                               :conversation-entry
                               :answer
                               :pending-task-id))))

; (ad-build-string albert::albert)
;;;--------------------------------------------------------- AD-BUILD-SUB-ITEMS

;;; there should be a cleaner way to do that...
(defUn ad-sub-items (agent &rest item-list)
  "takes a list of keywords and transforms them into accessors to build a string 
to display the results.
Arguments:
   agent: agent
   item-list: e.g. :COMM :input-messages"
  (format nil "=== ~A~{~_~S~}~2&" 
          ;; name of agent block
          (pop item-list)
          ;; slots in that block
          (mapcar #'(lambda(xx)
                      (list xx 
                            (eval `(,(intern  (symbol-name xx) :omas) ',agent))))
                  item-list)))

;;;------------------------------------------------------ MAKE-AGENT-DETAIL-WINDOW
  
(defUn make-agent-detail-window
    (&key agent (name :agent-detail-window) 
          (exterior (cg:MAKE-BOX 100 100 600 500))
          (title "")
          (owner (cg:screen cg:*system*)))
  (cg:make-window name 
    :class 'AGENT-DETAIL-WINDOW
    :parent owner
    :title title
    :exterior exterior
    :dialog-items (make-agent-detail-widgets agent exterior)
    :scrollbars nil))
  
;;;----------------------------------------------------- MAKE-AGENT-DETAIL-WIDGETS

(defUn make-agent-detail-widgets (agent box)
  "includes a pane for displaying data"
  ;(print `(width ,(width box) height ,(height box)))
  (list (make-instance 
            'cg:MULTI-LINE-EDITABLE-TEXT :FONT
          (cg:MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL) :NAME :agent-detail-pane
          :TABS NIL :TEMPLATE-STRING NIL :UP-DOWN-CONTROL t
          :scrollbars :vertical
          :HEIGHT (- (cg:height box) 30) :WIDTH (- (cg:width box) 8)
          :VALUE (AD-build-string agent) )))

;;;------------------------------------------------------ AD-DISPLAY-AGENT-DETAILS

(defUn ad-display-agent-details (agent)
  "create a floating window for displaying the detailed info about an agent. This is ~
     discarded when the user clicks anywhere in the window. The window will appear ~
     in the middle of the screen.
Arguments:
   agent: agent to display
Return:
   not significant"
  (make-agent-detail-window 
   :exterior (cg:make-box 100 100 
                          (- (cg:page-width (cg:screen cg:*system*)) 100)
                          (- (cg:page-height (cg:screen cg:*system*)) 100))
   :title (name agent))
  :done)

(format t "~&;*** OMAS v~A - agent windows loaded ***" *omas-version-number*)

;;; :EOF
