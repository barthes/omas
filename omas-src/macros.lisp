;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;24/10/18
;;;                M A C R O S  (file macros.lisp)
;;;
;;;===============================================================================
;;; This file contains OMAMS macros 

;;; (c) Jean-Paul Barthès@HEUDIASYC, CNRS, UTC. See LICENSE file for permissions.

(in-package :omas)

;;;===============================================================================
;;; debugging macros
;;;===============================================================================

(defMacro ag (agent)
  "?"
  `(cdr (assoc ,agent (omas::local-agents omas::*omas*))))

(defMacro dc (agent) `(describe (comm ,agent)))
(defMacro dd (agent) `(describe (data ,agent)))
(defMacro dk (agent) `(describe (control ,agent)))

(defMacro ipm () `(in-package :moss))
(defMacro ipo () `(in-package :omas))
(defMacro ipg () `(in-package :common-graphics-user))

;;; used for debugging to switch packages, e.g. (inp :G1)
(defMacro inp (xx)
  `(progn (in-package ,xx)
          (use-package :omas)))

		  (defMacro sv (xx)
  `(symbol-value ,xx))

;;;===============================================================================
;;; printing traces
;;;===============================================================================

;;;---------------------------------------------------------------------------- DF
;;; This macro is to be used in place of dformat when we do not want the arguments
;;; to be evaluated before the code is applied

(defmacro df (tag level fstring &rest ll &aux result)
  ;; level is static
  (cond
   ((symbolp tag)
    `(if (and (get ,tag :trace)(<= ,level (get ,tag :trace-level)))
         (format *debug-io* ,(string+ "~%;-- " tag ": " fstring) ,@ll)))
   ((listp tag)
    `(mapc 
         #'(lambda(item)
             (if (and (get item :trace)(<= ,level (get item :trace-level)))
                 (format *debug-io* (string+ "~%;-- " item ": " ,fstring) ,@ll)))
             ',tag))))

#|
(df :tr 0 "not doing anything ~S" 'albert)
expands to
(IF (AND (GET :TR :TRACE) (<= 0 (GET :TR :TRACE-LEVEL)))
    (FORMAT *DEBUG-IO* "~%;-- TR: not doing anything ~S" 'ALBERT))

(df (:n1 :a1 :tr) 2 "not doing anything ~S" 'albert)
expands to 
(MAPC #'(LAMBDA (ITEM)
          (IF (AND (GET ITEM :TRACE) (<= 2 (GET ITEM :TRACE-LEVEL)))
              (FORMAT *DEBUG-IO*
                  (STRING+ "~%;-- " ITEM ": " "not doing anything ~S")
                'ALBERT)))
  '(:N1 :A1 :TR))
|#
;;;--------------------------------------------------------------------------- DTF
;;; used to print a trace when entering a function or a method

(defmacro dtf (tag level fstring &rest ll &aux result)
  ;; level is static
  (cond
   ((symbolp tag)
    `(if (and (get ,tag :trace)(<= ,level (get ,tag :trace-level)))
         (format *debug-io* ,fstring ,@ll)))
   ((listp tag)
    `(mapc 
         #'(lambda(item)
             (if (and (get item :trace)(<= ,level (get item :trace-level)))
                 (format *debug-io* ,fstring ,@ll)))
       ',tag))))

#|
(dtf :n1 0 "~2%;==== Entering special method ~S" '=youpeee)
expands to
(IF (AND (GET :N1 :TRACE) (<= 0 (GET :N1 :TRACE-LEVEL)))
    (FORMAT *DEBUG-IO* "~2%;==== Entering special method ~S" '=YOUPEEE))

(dtf (:a1 :n1) 0 "~2%;==== Entering special method ~S" '=youpeee)
expands to 
(MAPC #'(LAMBDA (ITEM)
          (IF (AND (GET ITEM :TRACE) (<= 0 (GET ITEM :TRACE-LEVEL)))
              (FORMAT *DEBUG-IO* "~2%;==== Entering special method ~S"
                '=YOUPEEE)))
  '(:A1 :N1))
|#
;;;----------------------------------------------------------------------- VFORMAT

(defMacro vformat (&rest arglist)
  "prints a message in the text tracing window when it is opened"
  `(when (and (omas-verbose *omas*) (text-window *omas*))
       (text-trace (concatenate 'string (format nil ,@arglist) "~%") cg::dark-green)
       ))

;;;===============================================================================
;;; other macros
;;;===============================================================================

;;;------------------------------------------------------------------------- EWCLE
;;; probably useless

;;;(defMacro ewcle (&rest body)
;;;  `(eval-when (compile load eval) ,@body))

;;;-------------------------------------------------------------------------- GETV

(defmacro getv (alist indicator &key test)
  (if test
  `(cadr (assoc ,indicator ,alist :test ,test))
  `(cadr (assoc ,indicator ,alist))))

#|
(getv '((a 1) (b 2)) 'b)
2
|#
;;;------------------------------------------------------------------------- GETVL

(defmacro getvl (alist indicator &key test)
  (if test
      `(cdr (assoc ,indicator ,alist :test ,test))
    `(cdr (assoc ,indicator ,alist))))

#|
(getvl '((a 1) (b 2)) 'b)
(2)
(getvl '((a 1) (b 2 22)) 'b)
(2 22)
|#
;;;------------------------------------------------------------- with-transaction

(defmacro with-transaction (agent &rest body)
  "setup a transaction and executes body committing the results onto database."
  `(progn
     (agent-start-changes ,agent)
     ,@body
     (agent-commit-changes ,agent)
     t))


;;;========================== ACL/MCL compatibility macros ========================

;;;********** since we are using ACL those should be removed **********

(defMacro acl/mcl-process-kill (process)
  #+MCL
  `(process-kill ,process)
  #+MICROSOFT-32
  `(mp:process-kill ,process) 
  )

(defMacro acl/mcl-process.name (process)
  #+MCL
  `(ccl::process.name ,process)
  #+MICROSOFT-32
  `(mp:process-name ,process))

(defMacro acl/mcl-process-run-function (name fn &rest args)
  #+MCL
  `(process-run-function ,name ,fn ,@args)
  #+MICROSOFT-32
  `(mp:process-run-function 
    (list :name ,name :initial-bindings cg:*default-cg-bindings*) 
    ,fn ,@args))

(defMacro acl/mcl-process-wait (whostate function &rest args)
  #+MCL
  `(process-wait ,whostate ,function ,@args)
  #+MICROSOFT-32
  `(mp:process-wait ,whostate ,function ,@args) 
  )

(defMacro acl/mcl-process-wait-with-timeout (whostate time function &rest args)
  #+MCL
  `(process-wait-with-timeout ,whostate (ceiling (* 60 ,time)) ,function ,@args)
  #+MICROSOFT-32
  `(mp:process-wait-with-timeout ,whostate , time ,function ,@args) 
  )

(defMacro acl/mcl-without-interrupts (&rest ll)
  #+MCL
  `(without-interrupts ,@ll)
  #-MCL
  `(excl:without-interrupts ,@ll))
  
;;;================================= Useful macros ================================


(format t "~&;*** OMAS v~A - macros loaded ***" *omas-version-number*)

;;; :EOF
