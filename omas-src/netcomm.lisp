;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;                O M A S - N E T C O M M  (file netcomm.lisp)
;;;
;;; v4.0 first created 2003
;;;===============================================================================
;;; (c) Jean-Paul Barthès@HEUDIASYC, CNRS, UTC. See LICENSE file for permissions.

#|
 This file contains functions dealing communication over the net as well as
 some functions for voice interface.

History
-------
2024
 1018 adapting to Windows 10
2020
 0202 restructuring OMAS as an asdf system
|#

(in-package :omas)

;; list of ids of the messages we already broadcast on the local network
(defparameter *ids-of-broadcast-messages* nil)
;; we keep only a certain number of them
(defparameter *irm-max-size* 100)

(defparameter *net-incoming-message-stack* nil)

;;;========================= Net Interface Functions =============================
;;; functions to send and receive a message over the net
;;; do the marshalling and demarshalling of the message objects and handle +MICROSOFT-32s
;;; when the length of the message exceeds the allowed length for UDP messages.
;;; Contains the following functions:

;;; OMAS-NET-INITIALIZE
;;;
;;; OMAS-NET-SEND
;;;
;;; OMAS-DISPATCH
;;;
;;; OMAS-NET-CLOSE
;;;
;;; When a message is short enough to be transmitted or received as a single
;;; fragment, then the process is simple and the actual transfer functions are
;;; called directly. When the message is too long it must be fragmented, each
;;; fragment being sent or received separately.
;;; - sending is done with a loop calling the function net-send.
;;; - receiving is more complex. A message process and a timer process are created
;;;   for each message to be received as several pieces.
;;;   Unless all the pieces are not received before the timer fires, the message
;;;   is abandonned. Otherwise, all the fragments are put together and the message
;;;   object is reconstructed.
;;; Receiving is done whenever a new message appears on the input stack
;;; *net-incoming-message-stack*

;;; Interface is used as follows:
;;; When starting OMAS, 
;;;     - execute omas-net-initialize
;;;     - create a process to monitor incoming messages *net-dispatch-process* 
;;;       executing the omas-net-dispatch function (that waits for incoming
;;;       messages on *net-incoming-message-stack* that receives fragments)
;;;     - send messages using the omas-net-send function
;;;     - received messages are automatically delivered to local agents
;;; When quitting OMAS
;;;     - execute the omas-net-close function
;;;     - kill the *net-dispatch-process*

;;; Message-frame-structure

(defClass MESSAGE-TASK-FRAME ()
  ((from-name :accessor from-name :initarg :from-name)
   (message-name :accessor message-name :initarg :message-name)
   (repeat-count :accessor repeat-count :initarg :repeat-count)
   (fragment-list :accessor fragment-list :initform nil)
   (timeout-process :accessor timeout-process :initform nil)
   (fragment-count :accessor fragment-count :initform nil))
  (:documentation "frame of task created to receive fragments of a UDP message"))

;;;---------------------------------------------------------- OMAS-NET-ACTIVE-FRAME?

(defun omas-net-active-frame? (sender-name message-name repeat-count)
  "looks into the message-task-frame list if a frame has already been created ~
      for the message.
Arguments:
   sender-name: a symbol, e.g., MUL-3
   message-name: a symbol, e.g., MM-34
Return:
   the task-frame or nil"
  (dolist (mtf (net-input-task-list *omas*))
    (when (and (eql sender-name (from-name mtf))
               (eql message-name (message-name mtf))
               (eql repeat-count (repeat-count mtf)))
      (return-from omas-net-active-frame? mtf))))

;;;----------------------------------------------- OMAS-NET-ADD-AND-PROCESS-FRAGMENT

(defun omas-net-add-and-process-fragment (message-task-frame 
                                                fragment-number fragment)
  "adds a new fragment and checks for message completion. If so, kills timeout ~
      timer and dispatces message. Otherwise wait for more fragments.
Arguments:
   message-task-frame: task-frame for processing the message-fragments
   fragment-number: number of current fragment
   fragment: message fragment
Return:
   unimportant."
  (let (fragment-list message-string message)
    (push (cons (abs fragment-number) fragment) (fragment-list message-task-frame))
    ;; if last fragment, then record number of fragments
    (if (< fragment-number 0)
      (setf (fragment-count message-task-frame) (- fragment-number)))
    ;(format t "~&...fragment in task: ~S" fragment)
    ;; check if we have all segments
    (when (and (numberp (fragment-count message-task-frame))
               (>= (length (fragment-list message-task-frame))
                   (fragment-count message-task-frame)))
      ;; kill the timeout process
      ;(process-kill (timeout-process message-task-frame))
      ;(format t "~&...unsorted fragment list:~& ~S" (fragment-list message-task-frame))
      ;; reorder fragments
      (setq fragment-list (mapcar #'cdr
                                  (sort (fragment-list message-task-frame)
                                        #'< :key #'car)))
      ;(format t "~&...fragment list: ~S" fragment-list)
      ;; merge them
      (setq message-string (apply #'concatenate 'string fragment-list))
      ;(format t "~&...message-string: ~S" message-string)
      ;; remove message-task-frame from the list
      (setf (net-input-task-list *omas*) 
            (remove message-task-frame (net-input-task-list *omas*) :test #'equal))
      ;; rebuild the message-object
      (setq message (net-string-to-message-object message-string))
      ;; normalize and distribute message to the local coterie
      (omas-net-distribute-message message)
      )
    ;; otherwise, return, wait for the missing fragments
    nil))

;;;------------------------------------------------------------------ OMAS-NET-CLOSE

(defun omas-net-close ()
  "terminates net processing, cleaning and closing open-transport (MCL) or releasing
   socket (ACL).
Arguments:
   none"
  (let ((dispatch-process (net-dispatch-process *omas*)))
    ;; kill the dispatching process created in this file
    (when dispatch-process 
      (process-kill dispatch-process)
      (setf (net-dispatch-process *omas*) nil)
      )
    ;; and close Open Transport (MCL)
    (net-terminate-broadcast)
    :done))
  

;;;--------------------------------------------------------------- OMAS-NET-DISPATCH

; (setq *net-input-task-list* nil)
; (setq *net-incoming-message-stack* *mes*)
; (omas-net-dispatch)

;;; process to receive data
(defun omas-net-dispatch ()
  "Waits until messages arrive on the *incoming-message-stack*. Then processes the ~
      last message. If it reads to a lisp expr, then distributes it into the input ~
      box of the local agents, otherwise simply ignore it. If it is a fragment, ~
      then dispaches it to the correponding message-task-frame, creating one ~
      if it is the first fragment.
Arguments:
   none
Return:
   unclear"
  (let (message-string message-al fragment-pair message-task-frame message)
    (loop
      ;(format t "~&omas-net-dispatch/ Waiting for something..." 
      ;  (net-incoming-message-stack *omas*))
      (process-wait "net-waiting" #'(lambda() *net-incoming-message-stack*))
      ;(process-wait "net-waiting" #'(lambda()(net-incoming-message-stack *omas*)))
      ;;
      ;(format t "~&omas-net-dispatch/ incoming-stack:~&  ~S" 
      ;  (net-incoming-message-stack *omas*))
      
      ;; remove message from stack
      (pop *net-incoming-message-stack*)
      
      (setq message-string (car (last (net-incoming-message-stack *omas*))))
      ;; when string is nil do nothing
      (when message-string 
        ;(format t "~&omas-net-dispatch/ message-fragment:~&  ~S" message-string)
        ;; remove oldest message from list
        (#+MCL without-interrupts #+MICROSOFT-32 excl::without-interrupts
         (setf (net-incoming-message-stack *omas*)
               (butlast (net-incoming-message-stack *omas*))))
        ;; check for fragment
        (setq fragment-pair (get-fragment message-string))
        ;(format t "~&fragment-pair: ~S" fragment-pair)
        (if 
          (eql 0 (car fragment-pair))
          ;; when message is whole, then distribute it and wait for some more
          (progn
            ;; transform string into alternated list
            (setq message-al (read-from-string (cdr fragment-pair) nil nil))
            ;; returns nil if it cannot parse the message,then forget it
            (when message-al
              ;; first make the list into a message
              (setq message (net-list-to-message-object message-al))
              ;; normalize and distribute message to the local coterie
              (omas-net-distribute-message message)
              ))
          ;; here we have a fragment. First get sender and message names
          (multiple-value-bind (sender-name message-name repeat-count rest-of-message)
                               (omas-net-get-message-ref (cdr fragment-pair))        
            
            (vformat "~&sender-name: ~S message-name: ~S~&   rest-of-message: ~S" 
                     sender-name message-name rest-of-message)
            
            ;; test then if a message task frame is active
            (setq message-task-frame 
                  (omas-net-active-frame? sender-name message-name repeat-count))
            ;; if so, then add fragment
            (if message-task-frame
              (omas-net-add-and-process-fragment 
               message-task-frame (car fragment-pair) rest-of-message)
              ;; otherwise create new task-frame
              (omas-net-set-up-message-frame sender-name message-name repeat-count
                                             (car fragment-pair) rest-of-message))
            ))))))

;;;----------------------------------------------------- OMAS-NET-DISTRIBUTE-MESSAGE

(defUn omas-net-distribute-message (message)
  "takes an incoming message. Modify it before distributing it to the local coterie:
   - change the sending time to the local time (avoid clock synchronization)
   - record the name of sender, when unknown
Arguments:
   message: incoming message
Return:
   :done"
  (unless (typep message 'message)
    (error "the argument ~S is not of type message" message))
  (let ((sender-name (from! message)))  
    ;; check if we get an echo (broadcast returns message to the sending socket)
    (cond
     ;; message is from our OMAS panel (?)
     ((or (null sender-name)
          (eql sender-name :<user>)
          (eql sender-name (local-user *omas*)))
      (format t "~&omas-net-distribute-message/ message from our panel (?):~&  ~S"
              message)
      (return-from omas-net-distribute-message nil))
     ;; message is from one of our local agents (echo)
     ((member sender-name (local-agents *omas*) :key #'car)
      (format t "~&omas-net-distribute-message/ echo from one of our agents:~&  ~S"
              message)
      (return-from omas-net-distribute-message nil))
     ;; one of our messages is coming back
     ;; if we sent the message, then we have kept its ID
     ((member (id message) *ids-of-broadcast-messages*)
      (format t "~&omas-net-distribute-message/ one of our message is coming back:~&  ~S"
              message)
      (return-from omas-net-distribute-message nil))
     )
    
    ;; otherwise, we got a bona fide message
    (format t "~&omas-net-distribute-message/ incoming message from loop:~&  ~S" 
            message)
    ;; look for the sender of the message and record its name if unknown  
    
    ;; when sender is not nil and is a symbol, then remember the sender
    (when (and sender-name 
               (symbolp sender-name)
               (not (member sender-name '(:all :all-and-me))))
      (pushnew sender-name (names-of-known-agents *omas*)))
    ;; replace the time-stamp
    (setf (date message)(get-universal-time))
    (vformat "~&omas-net-distribute-message/ message:~&  ~S" message)
    ;; dispatch the message object on our machine
    (send-message message :locally? t))
  :done)

;;;-------------------------------------------------------- OMAS-NET-GET-MESSAGE-REF

(defun omas-net-get-message-ref (message-string)
  "gets a message string as input. The front of the message should start with data ~
      identifying the sender and message e.g. \"MUL-3;MM-4;(NN ...\". The ~
      function extracts sender name and message name and checks ~
      in the list of message tasks whether a task is processing ~
      the message or not.
Arguments:
   message-string: message to process (must begin with agent-name
                   and message-name)
Return:
  3 values: sender-name, message-name, rest-of-message"
  (let* ((p1 (position #\; message-string))
         (sender-name (subseq message-string 0 p1))
         (p2 (position #\; message-string :start (incf p1)))
         (message-name (subseq message-string p1 p2))
         (p3 (position #\; message-string :start (incf p2)))
         (repeat-count (subseq message-string p2 p3)))
    ;(format t "~& ~S ~S ~S ~S ~S ~S" p1 sender-name p2 message-name p3 repeat-count)
    (values (read-from-string sender-name)
            (read-from-string message-name)
            (read-from-string repeat-count nil nil)
            (subseq message-string (incf p3)))))

;;;------------------------------------------------------------- OMAS-NET-INITIALIZE

; (omas-net-initialize)
(defun omas-net-initialize ()
  "creates a UDP net interface, with OpenTransport for MCL and sockets for ACL.
   Launches the listening process omas-net-dispatch."
  (net-initialize-broadcast)
  (setf (net-dispatch-process *omas*) 
        (acl/mcl-process-run-function "Net Dispatch" #'omas-net-dispatch)))

;;;------------------------------------------------------------------- OMAS-NET-SEND

(defUn omas-net-send (message)
  "sends a message, fragmenting it whenever necessary"
  ;; first get sender name and message-name
  (let ((sender-name (from! message))
        (message-name (name message))
        (repeat-count (repeat-count message))
        (count 1)
        key key-length fragment message-string message-length chars-to-send tag)
    ;; build message key by putting sender and name together
    (setq key (concatenate 'string (symbol-name sender-name) ";"
                           (symbol-name message-name) ";"
                           (if repeat-count
                             (format nil "~D;" repeat-count)
                             ";"))
          key-length (length key))
    
    ;(format t "~&;+++ omas-net-send / message:~&  ~S~&;... message id: ~S" 
    ;  (net-message-to-string message :no-packing t) (id message))
    ;; add an id to the message if it does not have one
    (unless (id message)
      (setf (id message) (create-message-id *ids-of-broadcast-messages*)))
    ;; record key into local list
    (setq *ids-of-broadcast-messages*
          (cons (id message) 
                (if (>= (length *ids-of-broadcast-messages*) *irm-max-size*)
                  (butlast *ids-of-broadcast-messages*)
                  *ids-of-broadcast-messages*)))
    
    ;; make a message-string
    (setq message-string (net-message-to-string message)
          message-length (length message-string))
    
    ;(print message-string)
    ;; is message-string short enough to be sent as one piece?
    (when (< message-length (udp-max-message-length *omas*))
      ;; yes send it
      (net-send (concatenate 'string "+" message-string))
      (return-from omas-net-send 1))  ; meaning sent in one piece
    ;; no, cut it into several pieces
    ;; first piece start with "001FAC-3;MM-33;message..."
    (setq chars-to-send (- (udp-max-message-length *omas*) 2 key-length)
          fragment (concatenate 'string "1;" key 
                                (subseq message-string 0 chars-to-send))
          message-string (subseq message-string chars-to-send))
    ;; send first piece
    (net-send fragment)
    ;; then enter a loop
    (loop
      ;; number tag of piece to send
      (setq tag (format nil "~D;" (incf count)))
      ;; compute how many chars we can send
      (setq chars-to-send (- (udp-max-message-length *omas*) (length tag) key-length))
      ;; test for last fragment If so get out of the loop
      ;; when the fragment fits exactly the buffer we can't mark it with a "-" sign
      ;; (no room in the buffer). In this special case we'll send a last empty
      ;; buffer...
      (when (<= (1+ (length message-string)) chars-to-send)
        (return nil))
      ;; intermediate fragment
      (setq fragment (concatenate 'string tag key 
                                  (subseq message-string 0  chars-to-send))
            message-string (subseq message-string chars-to-send))
      ;; send it 
      (net-send fragment)
      ;; try next one
      )
    ;; last fragment
    ;; it is quite possible that the final message be an empty string 
    (setq fragment (concatenate 'string "-" tag key message-string))
    ;; send it
    (net-send fragment)
    :done))

; (defparameter *mes* nil)
; (defun net-send (mm) (print mm)(push mm *mes*))
; (setq *udp-max-message-length* 40)
; (omas-net-send mm-0)
#| produces stuff like:
"1;FAC-3;MM-0;;(1 MM-0 2 :REQUEST 3 32758" 
"2;FAC-3;MM-0;;40314 4 FAC-3 5 MUL-2 6 MU" 
"3;FAC-3;MM-0;;LTIPLY 7 (5 7) 11 3600 13 " 
"-5;FAC-3;MM-0;;:BASIC-PROTOCOL 17 T-0)" 
|#
;;;--------------------------------------------------- OMAS-NET-SET-UP-MESSAGE-FRAME

(defun omas-net-set-up-message-frame (sender-name message-name repeat-count
                                                       fragment-number fragment)
  "creates a new message-task-frame, a new process and timer...
Arguments:
   sender-name: name of agent that sent the message e.g. FAC-2
   message-name: name of the message: e.g. MM-45
   repeat-count: repeat-count of the message, e.g. NIL or 3
   fragment-number: numero of the fragment, e.g. 7
   fragment: string representing the fragment (may be empty)
Returns:
  :done"
  (let (mtf)
    ;; create message task frame
    (setq mtf 
          (make-instance 'message-task-frame 
            :message-name message-name
            :from-name sender-name
            :repeat-count repeat-count))
    ;; chain on the task list
    (push mtf (net-input-task-list *omas*))
    ;; create the timeout timer
    ;; **********
    ;; record it
    ;; when last fragment change number and update count
    (when (< fragment-number 0)
      (setq fragment-number (- fragment-number))
      (setf (fragment-count mtf) fragment-number))
    ;; record fragment number
    (setf (fragment-list mtf) (list (cons fragment-number fragment)))
    ;; 
    :done))

; (omas-net-set-up-message-frame 'MUL-3 'MM-45 nil 2 "junk message")
; (omas-net-set-up-message-frame 'MUL-3 'MM-45  2 -4 "junk message")

;;;---------------------------------------------------------------- OMAS-NET-TIMEOUT

(defun omas-net-timeout ()
  "removes task-frame and kills timeout timer. Some fragments are missing..."
  nil)

;;;-------------------------------------------------------------------- GET-FRAGMENT

(defun get-fragment (message-string &aux count p1)
  "returns a pair whose car is
    0 for single fragment message
    nn>0 for an intermediate fragment
    nn<0 for the last message fragment
 and cdr is a string corresponding to the actual content of the fragment.
Arguments:
   message-string: a string coding the message
Return:
   a pair (nn . cleaned-message-string)"
  ;; if the first byte of the message is "+", then the message has no continuation
  ;; if the three first bytes are a number then this is the number of the fragment 
  ;; of the message
  ;; if the first message is a "-" then this is the last fragment of the message
  ;; and the 3 following bytes give the number of fragments   
  (cond
   ;; message starts with +? If so, then return rest of message
   ((eql #\+ (char message-string 0))
    (cons 0 (subseq message-string 1)))
   ;; otherwise
   (t
    ;; locate first semi-column and extract count
    (setq  p1 (position #\; message-string)
           count (subseq message-string 0 p1))
    ;; read fragment number
    (setq count (read-from-string count))
    ;; check that we have a number
    (unless (integerp count) 
      (error "bad count in input message format: ~&~S" message-string))
    ;; extract fragment number 
    (setq message-string (subseq message-string (incf p1)))
    ;; return count (last fragment has a negative count)
    (cons count message-string)))
  )

;;;------------------------------------------------------ NET-LIST-TO-MESSAGE-OBJECT

(defun net-list-to-message-object (message-alist &key no-packing)
  "transform an alternate list into a structured message object.
Arguments:
   message-alist: alternated list representing the message
   no-packing (key): if t then we look for symbols rather than codes for properties
Value:
   nil if message cannot be read
   message-object otherwise."
  (let (message)
    ;; if list is empty return nil
    (unless message-alist (return-from net-list-to-message-object nil))
	;; only now make an instance of message
	(setq message (make-instance 'message))
    (loop 
      ;; when finished return object
      (unless message-alist (return message))
      ;; fill each slot value with content of message
      (setf (slot-value  message
                         (if no-packing
                           ;; if raw test on symbols
                           (intern (symbol-name (pop message-alist)) :omas)
                           ;; otherwise test on codes
                           (car (rassoc 
                                 ;; here we have a code
                                 (pop message-alist)
                                 (message-property-dictionary *omas*)))))
            (pop message-alist)))))

#|
? (message-property-dictionary *omas*)
((OMAS::NAME . 1) (TYPE . 2) (OMAS::DATE . 3) (OMAS::FROM . 4) (OMAS::TO . 5) 
 (ACTION . 6) (OMAS::ARGS . 7) (OMAS::CONTENTS . 8) (OMAS::CONTENT-LANGUAGE . 9) 
 (OMAS::ERROR-CONTENTS . 10) (OMAS::TIMEOUT . 11) (OMAS::TIME-LIMIT . 12) 
 (OMAS::ACK . 13) (OMAS::PROTOCOL . 14) (OMAS::STRATEGY . 15) (OMAS::BUT-FOR . 16)
 (OMAS::TASK-ID . 17) (OMAS::REPLY-TO . 18) (OMAS::REPEAT-COUNT . 19) 
 (OMAS::TASK-TIMEOUT . 20) (OMAS::SENDER-IP . 21) (OMAS::SENDER-SITE . 22) 
 (OMAS::THRU . 23) (OMAS::ID . 24)) 

? (net-message-to-string mm :no-packing t)
"(:TYPE :INFORM :DATE 3394163100 :FROM :<USER> :TO :STEVENS :ACTION :HELLO :TIME-LIMIT 3600 :PROTOCOL :BASIC-PROTOCOL :STRATEGY :TAKE-FIRST-ANSWER :id :UTC-55)"

? (net-list-to-message-object (read-from-string *) :no-packing t)
#<MESSAGE 9:5:0 :<USER> :STEVENS :INFORM :HELLO NIL NIL Tid:NIL :BASIC-PROTOCOL no-TO TL:3600>
|#
;;;----------------------------------------------------------- NET-MESSAGE-TO-STRING

(defun net-message-to-string (message &key no-packing)
  "takes an object and builds a compact string resulting from an alternate list ~
      where property values are replaced by numbers and null properties are omitted.
Arguments:
   message: message object to marshal
   no-packing (key): if t properties are not coded as integers.
Return:
   a string representing the message."
  (let (result sv)
    ;; now we translate the message into a list
    (dolist (pair (message-property-dictionary *omas*))
      (setq sv (slot-value message (car pair)))
      (when sv 
        ;; send keywords through the net to avoid package problems
        (push (if no-packing (make-keyword (car pair)) (cdr pair)) result)
        (push sv result)))
    ;; turn into string on a single line
    (write-to-string (reverse result) :pretty nil)
    ))
#|
? *message-property-dictionary*
((NAME . 1) (TYPE . 2) (DATE . 3) (FROM . 4) (TO . 5) (ACTION . 6) (ARGS . 7) 
 (CONTENTS . 8)(CONTENT-LANGUAGE . 9) (ERROR-CONTENTS . 10) (TIMEOUT . 11) 
 (TIME-LIMIT . 12) (ACK . 13)(PROTOCOL . 14) (STRATEGY . 15) (BUT-FOR . 16) 
 (TASK-ID . 17) (REPLY-TO . 18) (REPEAT-COUNT . 19)(TASK-TIMEOUT . 20) (THRU . 21)

? (defparameter mm (message-make-inform :<user> :stevens :hello nil))
#<MESSAGE 9:4:42 :<USER> :STEVENS :INFORM :HELLO NIL NIL Tid:NIL :BASIC-PROTOCOL no-TO TL:3600>

? (defparameter mm (message-make-inform :<user> :stevens :hello nil))
MM

? MM
#<MESSAGE 9:5:0 :<USER> :STEVENS :INFORM :HELLO NIL NIL Tid:NIL :BASIC-PROTOCOL no-TO TL:3600>

? (net-message-to-string mm :no-packing t)
"(:TYPE :INFORM :DATE 3394163100 :FROM :<USER> :TO :STEVENS :ACTION :HELLO :TIME-LIMIT 3600 :PROTOCOL :BASIC-PROTOCOL :STRATEGY :TAKE-FIRST-ANSWER)"

? (net-list-to-message-object (read-from-string *) :no-packing t)
#<MESSAGE 9:5:0 :<USER> :STEVENS :INFORM :HELLO NIL NIL Tid:NIL :BASIC-PROTOCOL no-TO TL:3600>

? (net-message-to-string (message-make-inform nil :albert :send "fake message"))
"(2 :INFORM 3 3392356505 5 :ALBERT 6 :SEND 7 \"fake message\" 11 3600 13 :BASIC-PROTOCOL 14 :TAKE-FIRST-ANSWER)"

(net-message-to-string (message-make-inform nil :albert :send "fake message")
                       :no-packing t)
"(:TYPE :INFORM :DATE 3392356777 :TO :ALBERT :ACTION :SEND :ARGS \"fake message\" :TIME-LIMIT 3600 :PROTOCOL :BASIC-PROTOCOL :STRATEGY :TAKE-FIRST-ANSWER)"

(net-message-to-string 
 (make-instance 'message :from :fac :to :mul-1 :type :request :task-id -2 
   :id :UTC-5)
 :no-packing t)
"(:TYPE :REQUEST :DATE 3443963418 :FROM :FAC :TO :MUL-1 :TIME-LIMIT 3600 :PROTOCOL :BASIC-PROTOCOL :STRATEGY :TAKE-FIRST-ANSWER :TASK-ID -2 :ID :UTC-5)"
|#
;;;---------------------------------------------------- NET-STRING-TO-MESSAGE-OBJECT

(defun net-string-to-message-object (message-string)
  "transform an alternate list into a structured message object.
Arguments:
   string: string representing the message
Value:
   nil if message cannot be read
   message-object otherwise."
  (let* ((message-alist (read-from-string message-string nil nil))
          message)
    (unless message-alist (return-from net-string-to-message-object nil))
	(setq message (make-instance 'message))
    (loop 
      (unless message-alist (return message))
      (setf (slot-value  message
                         (car (rassoc (pop message-alist) 
                                      (message-property-dictionary *omas*))))
            (pop message-alist)))))

#|
(net-string-to-message-object
 "(1 :TRQ-1 2 :REQUEST 3 3436879183 4 :FAC-2 5 :MUL-3 6 :MULTIPLY 7 (4 5) 12 3600 14 :BASIC-PROTOCOL 15 :TAKE-FIRST-ANSWER 17 -8 21 (:UTC))"
 )
#<MESSAGE 17:39:43 :FAC-2 :MUL-3 :REQUEST :MULTIPLY (4 5) NIL T:-8 :BASIC-PROTOCOL no-TO TL:3600>
|#

;;;===============================================================================
;;;12/09/09
;;;                Voice interface
;;;
;;;===============================================================================
;;; The voice interface uses sockets for communication between the AP and the voice
;;; component that runs as a process parallel to Lisp on the same machine or on a
;;; different machine.
;;;
;;;    VC ---> (52010) PA   -> conversation process
;;;    PA ---> (52011) VC   for text to speech conversion
;;;
;;; Since all communications are done using UDP, the port of the sending socket on
;;; each side is not important.
;;; When the voice component runs on a different machine the same port can be used,
;;; which is not the case when the voice component runs on the same machine as the 
;;; PA

#|
2007
 0716 creation
 0909 changing to new PA definition
|# 

;;; only valid for Microsoft-32

;;;=============================== Globals =========================================

;;; global parameters are included in the assistant structure 

;;;========================== Service functions ====================================

;;;--------------------------------------------------------- MAKE-STRING-FROM-BUFFER

;;;(defun make-string-from-buffer (buffer nn)
;;;  "take nn bytes in a byte buffer and returns a Lisp string. The functions assumes ~
;;;   that the codes correspond to a valid string, i.e., it does not check if included ~
;;;   quotes are preceded with a backslash.
;;;Arguments:
;;;   buffer: a byte buffer
;;;   nn: the number of chars to extract from the buffer."
;;;  (let ((str (make-string nn :initial-element #\space)))
;;;    (loop for i from 0 to (1- nn) do
;;;          (setf (char str i) (code-char (aref buffer i))))
;;;    str))

;;;========================= External functions ====================================

;;;------------------------------------------------------------ NET-INITIALIZE-VOICE
#|
(omas::net-initialize-voice cit-stevens::pa_cit-stevens)
|#

(defun net-initialize-voice (agent)
  "This function opens a socket if necessary.
Arguments:
   agent: assisstant agent controling the vocal interface
Return:
   :done"
  ;; create a receiving socket to be used on the PA side
  (unless (voice-input-socket agent)
    (setf (voice-input-socket agent) 
      (socket:make-socket 
       :type :datagram 
       :local-port (voice-input-port agent)
       ))
    ;(format t "~%;*** net-initialize-voice / PA voice-input-socket: ~S"
    ;  (voice-input-socket agent))
    )
  ;; create a sending socket
  (unless (voice-output-socket agent)
    (setf (voice-output-socket agent)
      (socket:make-socket :type :datagram))
    (dformat :voice 0 "net-initialize-voice / PA voice-output-socket: ~S" 
      (voice-output-socket agent))
    )
  
  ;; set up receiving process unless it already exists
  (unless (voice-receiving-process agent)
    (setf (voice-receiving-process agent) 
      (mp:process-run-function "Net Voice Receive" #'voice-receiving agent)))
  
  :done)

;;;------------------------------------------------------------------ NET-VOICE-SEND
#|
(omas::net-voice-send cit-stevens::PA_cit-stevens "Bonjour, que puis-je faire pour vous?")
|#

(defun net-voice-send (agent message)
  "sends a message to the voice interface using the UDP protocol.
Arguments:
   message: a String less than *max-message-length* bytes long
Return
   code returned by the send-to fuction."
  ;; check arg
  (unless (stringp message)
    (error "message should be a string rather than: ~S" message))
  (if (> (length message) (voice-max-message-length agent))
      (error "message too long:~%~S" message))
  
  ;; trace 
  (dformat :voice 0 "net-voice-send / sending message to ~A:~S~%;  ~%;   ~S"
    (voice-ip agent) (voice-output-port agent) message)
  ;; should convert message string into a buffer of chara codes ??
  ;; send message
  (when (voice-output-socket agent)
    (socket:send-to (voice-output-socket agent) message (length message)
                    :remote-host (voice-ip agent)
                    :remote-port (voice-output-port agent))))

;;;----------------------------------------------------------------- VOICE-RECEIVING

(defun voice-receiving (agent &aux text)
  "receives a message from the voice input and puts it into the to-do slot of the 
agent."
  (loop
    (multiple-value-bind 
          (raw-buffer size)
        (socket:receive-from (voice-input-socket agent)
                             (voice-max-message-length agent)
                             :extract t)
      (dformat :voice 1 "+++ ~S ~S" raw-buffer size)
      ;; transform unicode string into internal string
      ;; truncate is added as a safety measure
      (setq text (excl::octets-to-string raw-buffer ;:external-format :fat
                                         :truncate t))
      ;; echo the received string into the master's pane
      ;; should add a termination char to trigger the analysis
      (omas::assistant-process-master-text agent text)
      )))

;;;------------------------------------------------------------- NET-TERMINATE-VOICE

#|
(omas::net-terminate-voice cit-stevens::pa_cit-stevens)
(omas::net-initialize-voice cit-stevens::pa_cit-stevens)
|#

(defun net-terminate-voice (agent)
  "kill receiving process, reset global-variables, closes sockets"
  (when (voice-receiving-process agent)
    (mp:process-kill (voice-receiving-process agent))
    (setf (voice-receiving-process agent) nil))
  
  (when (voice-input-socket agent)
    (close (voice-input-socket agent))
    (setf (voice-input-socket agent) nil))
  (when (voice-output-socket agent)
    (close (voice-output-socket agent))
    (setf (voice-output-socket agent) nil))
  (setf (voice-io agent) nil)
  :done)

#|
;;;===============================================================================
;;;                          Simple voice tests
;;;===============================================================================

;;;===== Simple Sending test
;;; host name, also #x7F000001 (not really needed, can use string as done below)
(defparameter *remote-host* (socket:dotted-to-ipaddr "127.0.0.1"))

;;; create a socket for sending messages to the vocal component
(defparameter *socket* 
  (socket:make-socket :type :datagram))

;;; send message to the vocal component

(socket:send-to *socket* "Hello there ?" (length "Hello there ?")
                :remote-host "127.1"
                :remote-port 52010)

;;; close socket
(progn
  (close *socket*)
  (setq *socket* nil)
  )

;;;===== Simple Receiving test
;;; set up a new process and receiving loop

;; create a receiving socket
(defparameter *rcv-socket* 
  (socket:make-socket 
   :address-family :internet
   :type :datagram 
   :local-host "127.1" ; to receive from the same machine
   :local-port 52010))

;;; start a receiving process using the receiving function
(defparameter *net-receive-process* 
  (mp:process-run-function "Net Receive" #'receiving))

;;; close socket resetting the global variable
(progn
  (close *rcv-socket*)
  (setq *rcv-socket* nil)
  )

;;; kill receiving process
(progn
  (mp:process-kill *net-receive-process*)
  (setq *net-receive-process* nil)
  )

;;; then create a receiving loop

(net-initialize-broadcast)

;;;================ Some information about sockets ===============================

make-socket  (from the ACL doc)

Arguments: &rest args &key type format connect address-family eol

:address-family :internet :type :datagram

    These additional keyword arguments are valid: :local-port, :local-host, 
    :remote-host, and :remote-port, :reuse-address, :broadcast.

    See the :internet :stream case above for the general meaning of the keywords. 

    :reuse-address sets the SO_REUSEADDR flag. This allows a particular port to be
    reopened in :connect  :passive mode even if there is an existing connection for
    the port. This is very useful when debugging a server program since without it
    you may have to wait up to a minute after closing a particular port to reopen
    the same port again (due to certain port-non-reuse requirements found in the
    TCP/IP protocol).

    :local-host may be specified to select the network device on which the datagram
    socket is created. Specifying "127.1" for example will put the datagram socket 
    on the loopback network and it will only receive datagrams from other processes 
    on the same machine. If :local-host is not specified then the datagram socket
    will be on all network devices simultaneously.

    :broadcast requests permission to send broadcast packets from this socket. 
    Whether permission is granted depends on the policy of the operating system. 
    To send a broadcast packet you must specify the broadcast IP address for the
    network on which you want to broadcast. The convention is that the broadcast
    address is the highest numbered host address on a network. For example, if the
    machine has a network interface with an IP address of 192.168.1 34 then it is 
    on network 192.168.1.0 and the broadcast address for that network will mostly 
    likely be 192.168.1.255.

    A datagram socket is never connected to a remote socket, it can send a message
    to a different host and port each time data is sent through it. However if you
    know that you'll be sending data to a particular host and port with this socket,
    then you can specify that :remote-host and :remote-port when you create the 
    socket. If you've done that then you can omit the :remote-host and :remote-port
    arguments to the send-to function. In other words, specifying the :remote-host
    and :remote-port just sets the default values for the :remote-host and 
    :remote-port arguments when a send-to is done.

;;;===============================================================================
;;;                          End simple voice tests
;;;===============================================================================
|#

#|
(omas::net-initialize-voice Albert::Albert)
|#

(format t "~&;*** OMAS v~A - netcomm loaded ***" *omas-version-number*)

;;; :EOF 
