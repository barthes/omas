;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/03/08
;;;                O M A S - A P I  (file API.lisp)
;;;
;;;===============================================================================
;;; This file contains the OMAS agent API functions 

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;; API interface functions and macros are the following :

;;; %remove-message-from-queue (property value queue)                   [MACRO]
;;; %ABORT-CANCEL-TASK (agent agent task-id sender                   [FUNCTION]
;;;                      &key task-frame no-subtasks ignore-queues 
;;;                           cancel no-mark)
;;; ABORT-CURRENT-TASK (agent &optional message)                     [FUNCTION]
;;; ABORT-TASK (agent task-id &optional message)                     [FUNCTION]
;;; ADD-TRACE-TAG (tag)                                              [FUNCTION]
;;; ANSWERING-AGENT (agent)                                          [FUNCTION]
;;; ASSISTANT? (agent)                                               [FUNCTION]
;;; CANCEL-ALL-SUBTASKS (agent &optional current-task)               [FUNCTION]
;;; CANCEL-CURRENT-TASK (agent &key message no-subtasks              [FUNCTION]
;;; CANCEL-ANSWERING-SUBTASK (agent)                                 [FUNCTION]
;;; CANCEL-SUBTASK (agent subtask-id)                                [FUNCTION]
;;; COMPARE-BIDS (message1 message2)                                 [FUNCTION]
;;; CREATE-MESSAGE-ID ()                                             [FUNCTION]
;;; %CREATE-MOSS-CLASSES-AND-PROXIES ()                              [FUNCTION]
;;; CREATE-SUBTASK-ID (agent)                                        [FUNCTION]
;;; CREATE-TASK-ID (agent)                                           [FUNCTION]
;;; DFORMAT (tag level format-string)                                [FUNCTION]
;;; DRFORMAT (tag level format-string)                               [FUNCTION]
;;; DYNAMIC-EXIT (agent result)                                      [FUNCTION]
;;; FORGET (agent)                                                   [FUNCTION]
;;; GATE-CLOSE (nn)                                                  [FUNCTION]
;;; GATE-CREATE ()                                                   [FUNCTION]
;;; GATE-GET-DATA (nn)                                               [FUNCTION]
;;; GATE-OPEN (nn)                                                   [FUNCTION]
;;; GATE-POP-DATA (nn)                                               [FUNCTION]
;;; GATE-PUSH-DATA (nn data)                                         [FUNCTION]
;;; GET-ENVIRONMENT (agent)                                          [FUNCTION]
;;; GET-FIELD (field a-list)                                         [FUNCTION]
;;; GET-INTERNAL-TIME-STRING ()                                      [FUNCTION]
;;; GET-STR (prop alist)                                             [FUNCTION]
;;; GET-TEXT (mln)                                                   [FUNCTION]
;;; GET-TIME-LIMIT (agent)                                           [FUNCTION]
;;; MASTER-TASK? (agent task-id)                                     [FUNCTION]
;;; OMAS ()                                                          [FUNCTION]
;;; OMAS-EXIT ()                                                     [FUNCTION]
;;; PA (package-key)                                                 [FUNCTION]
;;; PENDING-SUBTASKS? (agent)                                        [FUNCTION]
;;; PURELY-LOCAL-SKILL? (agent skill)                                [FUNCTION]
;;; RAL ()                                                              [MACRO]
;;; RECEIVING-AGENT ((message 'message))                               [METHOD]
;;; REM-FIELD (field alist)                                          [FUNCTION]
;;; REMOVE-TRACE-TAG (tag)                                           [FUNCTION]
;;; RESET-ALL-AGENTS ()                                              [FUNCTION]
;;; RESET-TRACE-SKILLS ()                                            [FUNCTION]
;;; SAVE-SELECTION (agent object-list)                               [FUNCTION]
;;; SELECT-BEST-BIDS (agent bid-list)                                [FUNCTION]
;;; SEND-INFORM (agent &key (action 'inform) to args (delay 0))      [FUNCTION]
;;; SEND-MESSAGE (message &aux target new-message)                   [FUNCTION]
;;; SEND-REQUEST-NO-WAIT (agent &key to task-id action args)         [FUNCTION]
;;; SEND-SUBTASK (agent &key to action args (repeat-count 2))        [FUNCTION]
;;; SENDING-AGENT ((message 'message))                                 [METHOD]
;;; SET-TRACE-SKILLS ()                                              [FUNCTION]
;;; SKILL-TRACE (aent message)                                       [FUNCTION]
;;; STATIC-EXIT (agent arg)                                          [FUNCTION]
;;; SLOW (delay)                                                     [FUNCTION]
;;; SPEAK (text)                                                     [FUNCTION]
;;; SPLIT-VAL (text &optional (separator #\,))                       [FUNCTION]
;;; SUBSTITUTE-SUBSTRING (new old sequence)                          [FUNCTION]
;;; TIME-STRING (time)                                               [FUNCTION]
;;; AGENT-TRACE (agent text &rest args)                              [FUNCTION]
;;; TRACE-AGENT (agent)                                              [FUNCTION]
;;; UNTRACE-AGENT (agent)                                            [FUNCTION]
;;; UPDATE-ENVIRONMENT (agent env)                                   [FUNCTION]

#|
History
-------
2015
 0403 adding add-trace-tag and remove-trace-tag
 0610 modifying dynamic-exit to include exit from inform messages
 1030 including synchronization mechanism using gates: gate-xxx functions
2016
 0314 modifying send-message to allow string references
 0320 modifying dformat to accept list of tags
 1127 adding :once-only key arg to send-subtask to avoid sending message 3 times
2020
 0202 restructuring OMAS as an asdf system
|#

(in-package :omas)

;;;========================== macros ===============================================

;;;------------------------------------------------------ %REMOVE-MESSAGE-FROM-QUEUE

(defMacro %remove-message-from-queue (property value queue)
  "macro to simplify the writing when removing messages from queues ~
   queue must be explicit (keyword)"
  `(dolist (item (,(intern (symbol-name queue) :omas) agent))
     (if (eq ,value (,(intern (symbol-name property) :omas) item))
         (%agent-remove-value agent item ,queue))))

;;;======================== debugging functions ===================================

(defun dformat (tag level fstring &rest ll)
  "allows selective tracing by inserting a keyword tag into the (debugging *omas*)~
   list of debugging tags (similar to *features*).
Arguments:
   tags: a tag or list of tags for inserting a format
   level: a level, the higher the more detailed (default is 0)
   fstring: format string
Return:
   nil"
  (cond
   ((and (symbolp tag)(get tag :trace)(<= level (get tag :trace-level)))
    (apply #'format *debug-io* (string+ "~%;-- " tag ": " fstring) ll))
   ;; if a list check items in turn
   ((listp tag)
    (dolist (item tag)
      (if (and (get item :trace)(<= level (get item :trace-level)))
          (apply #'format *debug-io* (string+ "~%;-- " item ": " fstring) ll))))
   ) 
  nil)

#|
(dformat :tr 0 "not doing anything ~S" 'albert)
;-- TR: not doing anything ALBERT
NIL

(dformat '(:lu :tr) 0 "not doing anything ~S" 'albert)
;-- TR: not doing anything ALBERT
NIL
|#
;;;------------------------------------------------------------------------ DRFORMAT

(defun drformat (tag level fstring &rest ll)
  "like dformat but raw: does not insert ~%;-- infront"
  (cond
   ((and (symbolp tag)(get tag :trace)(<= level (get tag :trace-level)))
    (apply #'format *debug-io* fstring ll))
   ;; if a list check items in turn
   ((listp tag)
    (dolist (item tag)
      (if (and (get item :trace)(<= level (get item :trace-level)))
          (apply #'format *debug-io* fstring ll))))
   )
  nil)

;;;================== user and scheduler functions =================================

;;;-------------------------------------------------------------- ABORT-CURRENT-TASK

(defUn abort-current-task (agent &key ignore-queues)
  "When in the process of executing a particular task, the agent might decide ~
   to abandon the task, killing all subtasks.
      This is useful when an agent has a particular skill but does not want to ~
   answer a request.
Arguments:
   agent: agent
   ignore-queues: if t does not clean queues (presumably already done)."
  (declare (ignore message))
  (let ((task-frame (agent-process-get-task-frame agent *current-process*)))
    ;; check again that we are executing some task
    ;; this should not happen since the function is called from the task process
    (unless task-frame
      (error "trying to abort the current task, but no task is associated with the ~
              current process ~S" *current-process*))
    ;; call the regular function
    (%abort-cancel-task agent (id task-frame) (from! task-frame) 
                        :task-frame task-frame :ignore-queues ignore-queues)
    ))

;;;-------------------------------------------------------------- %ABORT-CANCEL-TASK

(defUn %abort-cancel-task (agent task-id sender
                                 &key task-frame no-subtasks ignore-queues cancel
                                 no-mark)
  "code used to abort or to cancel a task. Aborting is the same as canceling except ~
   that an abort message is returned to the agent that asked for the task. Note ~
   that in case the answer should be sent to continuations (reply-to), it is not ~
   clear to whom we should send the message.
Arguments:
   agent: agent
   task-id: task-number of the task to be aborted
   sender: agent that sent the task
   no-subtasks (key): if true does not cancel subtasks (presumably there are none)
   ignore-queues (opt): when t does not check input-messages, agenda or waiting tasks
   no-mark (key): when t does not draw a black mark on the agent life
Return:
   does not return if called from the task process
   returns the id of the aborted task if called from somewhere else."
  ;; We are asked to abort a task knowing its task-number and sender or sometimes
  ;; its task-frame.  
  ;; The task may not be active yet. I.e., a request message or grant, or cancel-grant 
  ;; could be in the input messages mailbox or in the agenda, waiting to be 
  ;; processed, since abort message have priority over such messages. 
  ;; In addition, even if the task is active, there could be a repeated message
  ;; in those queues. 
  ;; Thus, we do the following (unless explicitly told otherwise by the 
  ;; ignore-queues arg):
  ;;   - we clean input-messages and agenda removing possible repeat messages
  ;;   - we also clear the possible pending bids (when the task was a broadcast
  ;;     and the agent submitted a bid)
  (unless ignore-queues  ; e.g. don't execute when called from agent-scan-abort
    ;(format t "~%;+++ %abort-cancel-task / input-messages:~%  ~S"
    ;  (input-messages agent))
    (excl::with-delayed-interrupts
        ;; check agenda
        (%agent-remove-if agent #'(lambda (xx) (and (eql (task-id xx) task-id)
                                                    (eql (from! xx) sender)))
                          :agenda)
      ;; ... and from tasks-to-do master bin (if none does not matter)
      ;; ... and pending bid
      (setf (pending-bids agent)
        (delete-if #'(lambda(xx) (eql task-id (task-id xx))) 
                   (pending-bids agent)))
      ;; ... and input mailbox
      ;; this is supposed to remove all messages that came from sender 
      ;; pertaining to specified task. But it does not work when the
      ;; sender is also the receiver
      ;; BUG: will remove answer message when returned from same agent as
      ;; an answer to subtask whose id is task-id!
      (%agent-remove-if agent #'(lambda (xx) (and (eql (task-id xx) task-id)
                                                  (eql (from! xx) sender)
                                                  (not (eql (type! xx) :answer))))
                        :input-messages)
      )
    ;; update various displays if needed
    (agent-display agent)
    (if (com-window agent)
        (assistant-display-tasks-to-do agent))
    )
  ;; now we have more time 
  ;;   - we then check for a task-frame indicating that the task has started    
  (let (time-limit-process task-process)
    ;; unless task-frame is provided, try to recover it from task-id and sender
    ;; that identify a task uniquely
    (unless task-frame 
      (setq task-frame (agent-get-task-frame-from-task-id agent task-id sender))
      ;; check again that we are executing some task, if  not, then task was never
      ;; started, or completed, or was canceled by somebody else. No problem.
      (unless task-frame
        (agent-trace agent "trying to abort task ~S that is not currently active." 
                     task-id)
        (return-from %abort-cancel-task nil)))
    
    ;;   - we kill all subtasks and all pieces of info related to the subtasks 
    ;;     (contained within the subtask-frame, e.g., timeout process)
    ;;     unless explicitly specified that it is not necessary (no-subtasks)
    (unless no-subtasks (cancel-all-subtasks agent task-frame))
    ;;   - we remove the special entry in case agent is learning
    (when (member :learning (features agent))
      ;;***** erase the record for the task, not so clear...
      (setf (waiting-tasks agent) nil))
    
    ;; send an error message to the agent that requested the task
    ;; unless we are canceling the task (no error message in cancel mode)
    ;; it recovers parameters from the original request message
    (unless cancel 
      (send-message (message-make-error agent (message task-frame))))
    
    ;; extend lifelines and display a mark unless told otherwise
    (unless no-mark
      ;; update graphics window, just before killing process
      (state-draw)
      ;; let process have time to get right task parameters killing timers
      (sleep 0.05) ; JPB0807
      ;; indicate the abort condition on the graphics trace (black square)
      (agent-display-abort agent))
    ;; remove task-frame from active-task-list
    (setf (active-task-list agent)
      (remove task-frame (active-task-list agent)))
    ;; update agent-display (since we modified the number of tasks)
    (agent-display agent)
    
    ;;   - we kill the task process, and related time-limit timer
    ;;     %abort-cancel-task can be called from the task process, from the time-limit 
    ;;     or from one of the timeout processes attached to a subtask.
    (setq task-process (process task-frame)
        time-limit-process (timer-process task-frame))
    ;; kill the process executing the task
    (cond
     ;; if called from task-process, kill first timer, then task
     ((eql task-process *current-process*)
      (agent-process-kill agent time-limit-process)
      (agent-process-kill agent task-process)
      ;; dead, does not return
      )
     ;; if called from the time-limit process kills the task process
     ;; the timer will later commit suicide in its own process
     ((eql time-limit-process *current-process*)
      (agent-process-kill agent task-process))
     ;; if called from somewhere else kill both timer and task processes
     (t
      (agent-process-kill agent time-limit-process)
      (agent-process-kill agent task-process)
      ;; returns
      ))
    ;; return killed task-id
    task-id))

;;;---------------------------------------------------------------------- ABORT-TASK

(defUn abort-task (agent task-id sender 
                           &key  task-frame no-subtasks ignore-queues no-mark)
  "Abort a task even if it has not started. The input queues are cleaned. ~
   If the task was started, the structures are removed, the processes ~
   killed, and the subtasks canceled.
Arguments:
   agent: agent
   task-id: task-number of the task to be aborted
   sender: agent that sent the task
   no-subtasks (key): if true does not cancel subtasks (presumably there are none)
   ignore-queues (opt): when t does not check input-messages, agenda or waiting tasks
   no-mark (key): when t does not draw a black mark on the agent life
Return:
   does not return if called from the task process
   returns the id of the aborted task if called from somewhere else."
  (%abort-cancel-task agent task-id sender 
                      :task-frame task-frame 
                      :no-subtasks no-subtasks
                      :ignore-queues ignore-queues
                      :no-mark no-mark))

;;;------------------------------------------------------------------- ADD-TRACE-TAG

(defun add-trace-tag (tag)
  "add a tag to the list of trace tags to trace corresponding execution."
  (declare (special *omas*))
  (pushnew tag (debugging *omas*)))

;;;----------------------------------------------------------------- ANSWERING-AGENT

(defUn answering-agent (agent)
  "gives the id of the agent corresponding to the received answer message being ~
   processed.
Arguments:
   agent: agent."
  ;; get subtask id and then cancel it
  (let ((current-task-frame (agent-get-task-frame-from-process agent *current-process*))
        answer-message)
    (when current-task-frame
      (setq answer-message (answer-being-processed current-task-frame))
      (when answer-message
        ;(name (from! answer-message))))))
        (from! answer-message)))))

;;;---------------------------------------------------------------------- ASSISTANT?

(defUn assistant? (agent)
  "check whether an agent is an assistant, in this case it returns t.
Arguments:
   agent: agent."
  (typep agent 'assistant))

;;;--------------------------------------------------------------------- BETTER-BID?

(defun better-bid? (msg1 msg2 &key test-equality)
  "compares two bids using messages. If msg2 is better than msg1 returns nil. 
   message contents is a list (start-time delay quality cost rigidity). Compares ~
   first the cost, then the completion time (start-time + delay), then the quality.
Arguments:
   msg1: bid message 1
   msg2: bid message 2
   test-equality (key): if t returns :same if both bids are equal (default nil)
return:
   nil if msg1 is better than msg2, :same if they are equal, t otherwise."
  (let (contents1 contents2 delta1 delta2)
    ;; if one of the message is not a bid message error
    (unless (and (eql (type! msg1) :bid)
                 (eql (type! msg2) :bid))
      (error "trying to compare messages that are not bids: ~%  ~S~%  ~S" msg1 msg2))
    ;; get contents
    (setq contents1 (contents msg1)
        contents2 (contents msg2))
    ;; compare costs
    (cond
     ;; if first has greater cost than second, then nil
     ((> (nth 3 contents1)(nth 3 contents2)) nil)
     ;; if first is lower than second then msg1
     ((< (nth 3 contents1)(nth 3 contents2)) t)
     ;; if equal must compare completion times
     (t
      ;; get completion times(nth 2 msg1)(nth 2 msg2))
      (setq delta1 (+ (car contents1)(cadr contents1))
          delta2 (+ (car contents2)(cadr contents2)))
      (cond
       ((> delta1 delta2) nil)
       ((< delta1 delta2) t)
       (t
        ;;compare quality
        (cond
         ;; msg1 has better quality?
         ((> (nth 2 contents1)(nth 2 contents2)) t)
         ;; worse?
         ((< (nth 2 contents1)(nth 2 contents2)) nil)
         ;; if both offers are equal and test-equality is t, returns :same
         (test-equality :same)
         ;; otherwise returns nil meaning that offer1 is not better than offer 2
         (t nil))))))))

#|
(setq cfbm   (make-instance 'message
    :type :call-for-bids
    :to :all
    :action :test
    :task-id :t-0
    :args '(1 2 3)
    ))
(setq msg1 (message-make-bid :mission '(0 0 100 20 100) cfbm))
(setq msg2 (message-make-bid :book-store '(0 0 100 10 100) cfbm))
(setq msg3 (message-make-bid :butcher '(0 10 100 20 100) cfbm))

? (better-bid? msg1 msg2)
NIL
? (better-bid? msg2 msg1)
T
? (better-bid? msg1 msg1)
NIL
(better-bid? msg1 msg1 :test-equality t)

? (better-bid? msg1 msg3)
T
? (better-bid? msg3 msg1)
NIL
? (better-bid? msg3 msg3 :test-equality t)
:SAME

? (better-bid? msg1 cfbm)
Error: trying to compare messages that are not bids: 

       #<MESSAGE 9:34:4 :MISSION NIL :BID :TEST (1 2 3) (0 0 100 20 100) Tid::T-0 :BASIC-PROTOCOL no-TO TL:3600 id:NIL>

       #<MESSAGE 9:33:1 NIL :MISSION :CALL-FOR-BIDS :TEST (1 2 3) NIL Tid::T-0 :BASIC-PROTOCOL no-TO TL:3600 id:NIL>
|#
;;;------------------------------------------------------------- CANCEL-ALL-SUBTASKS

(defUn cancel-all-subtasks (agent &optional task-frame)
  "cancel all subtasks launched by a particular agent. Does not cancel ~
   the task that spawned the subtasks. Kill timeout processes related to the ~
   subtasks if active.
   When the optional task-frame argument is not present, then it is assumed ~
   that the concerned task is that corresponding to current process.
Arguments:
   agent: agent.
   task-frame (opt): task-frame of the task that spawned the subtasks."
  ;; first check if task-frame is specified
  (unless task-frame 
    (setq task-frame (agent-process-get-task-frame agent *current-process*)))
  ;; if still no task frame, then error
  (unless task-frame 
    (error "cannot find task-frame corresponding to current process ~S" 
           *current-process*))
  ;; loop canceling each subtask
  (dolist (subtask-frame (subtask-list task-frame))
    (cancel-subtask agent subtask-frame))
  ;; returns. Normally called by %abort-cancel-task
  :done)

;;;-------------------------------------------------------- CANCEL-ANSWERING-SUBTASK

(defUn cancel-answering-subtask (agent)
  "cancel subtask corresponding to the received answer message being processed.
   Should also remove the subtask-frame from the subtask-list slot of the task-frame.
   Should be called from the task process, e.g. while executing a skill.
Arguments:
   agent: agent."
  ;; get subtask id and then cancel it
  (let ((current-task (agent-get-task-frame-from-process agent *current-process*))
        answer-message subtask-id subtask-frame)
    (unless current-task
      (warn  "no task associated with current process ~S" *current-process*)
      ;; no subtask left to cancel...
      (return-from cancel-answering-subtask nil))
    ;; otherwise remove the task
    ;; status of answer-being-processed slot is unclear
    (setq answer-message (answer-being-processed current-task))
    (when answer-message
      (setq subtask-id (task-id answer-message)
            subtask-frame (agent-get-subtask-frame-from-subtask-id 
                           subtask-id current-task))
      ;; cancel subtask and remove corresponding entry from the subtask-list
      (cancel-subtask agent subtask-frame)
      ;; remove the cancelled subtask from the slot
      (setf (answer-being-processed current-task) nil))
    :done))

;;;------------------------------------------------------------- CANCEL-CURRENT-TASK

(defUn cancel-current-task (agent &key no-subtasks task-frame no-mark)
  "same function as abort-current-task, but does not send error message to the ~
   agent that required the task. The task to be canceled is the one corresponding ~
   to the process being executed. 
 Arguments:
   agent: agent
   no-subtaks: (key) if t, indicates that no subtasks have been spawned and that it ~
   is not necessary to send cancel messages
   task-frame (key): corresponding task-frame if known.
   no-mark (key): when t, does not draw a black mark on the agent life line
Return:
   does not return"
  (let ((task-frame (or task-frame
                        (agent-process-get-task-frame agent *current-process*))))
    ;(format t "~%:++++ cancel-current-task /*current-process*: ~S, task-frame: ~S"
    ;  *current-process* task-frame)
    ;; check that we are executing some task. If not quit.
    ;; in fact the task may be terminated when the cancel arrives
    (unless task-frame
      (warn "trying to cancel the current task, but no task is associated with the ~
             current process ~S" *current-process*)
      (return-from cancel-current-task nil))
    (%abort-cancel-task agent (id task-frame) (from! task-frame) 
                        :task-frame task-frame :no-subtasks no-subtasks :cancel t 
                        :no-mark no-mark)))

;(trace cancel-subtask)
;(untrace cancel-subtask)

;;;------------------------------------------------------------------ CANCEL-SUBTASK

(defUn cancel-subtask (agent subtask-frame)
  "Cancel the subtask corresponding to subtask-frame. Any info put into the ~
   user-managed environment area should be cleaned by the user. We clean ~
   input-messages, agenda, timeout process if any, and send a message to ~
   all agents that were executing the subtask as taken from the subtask-frame ~
   in the subtask list of current-task.
   Removes the subtask-frame from the list of subtasks.
   The function is usually called from the process executing the task that spawned ~
   the subtask, but could be called from a timeout or a time-limit process.
Arguments:
   agent: agent
   subtask-frame: subtask-frame of the subtask to be cancelled."
  (let ((subtask-id (id subtask-frame)) process current-task-frame)
    ;; tell user we are killing a subtask
    (agent-trace agent "killing subtask ~S" subtask-id)
    ;; remove all messages referencing the subtask from all possible agent queues
    ;; should be carefull here not to remove request that could originate from 
    ;; subtasks in other coteries. Eliminate only returning messages.
    (#+MCL without-interrupts #+MICROSOFT-32 excl::without-interrupts
     (setf (agenda agent)
           (remove-if #'(lambda (xx) 
                          (and (eql (task-id xx) subtask-id)
                               (member (type! xx) 
                                       '(:acknowledge :cancel :abort :bid
                                         :bid-with-answer :answer))))
                      (agenda agent)))
     (setf (input-messages agent)
           (remove-if #'(lambda (xx) 
                          (and (eql (task-id xx) subtask-id)
                               (member (type! xx) 
                                       '(:acknowledge :cancel :abort :bid
                                         :bid-with-answer :answer))))
                      (input-messages agent))))
    ;; returned bids that have been processed are in the subtask-frame
    ;; send cancel message to the agent(s) executing the subtask
    (send-message 
     (message-make-cancel agent (to! subtask-frame) (id subtask-frame)))
    ;; get current task-frame
    (setq current-task-frame (parent-task subtask-frame))
    ;; remove the subtask from the list of pending subtasks
    (setf (subtask-list current-task-frame) 
          (remove subtask-frame (subtask-list current-task-frame)))
    ;; if a timeout process was associated with the subtask, then kill it
    ;; but we could be executing this timer, which kills the current process
    (when 
      (setq process (timeout-process subtask-frame))
      ;; if called from this timeout process we must not kill it otherwise
      ;; we'll never return from the function
      ;; it will commit suicide
      (when (eql process *current-process*)
        (return-from cancel-subtask nil))
      ;; oterwise kill timeout process
      (agent-trace agent "killing timeout timer ~S associated with ~S"
                   (acl/mcl-process.name process) (id subtask-frame))
      ;; when called from a timeout process, then kills it (does not return)
      (agent-process-kill agent process))          
    
    ;; when returning
    :done))

;;;--------------------------------------------------------------------- CANCEL-TASK

(defUn cancel-task (agent task-id sender &key task-frame no-subtasks no-mark)
  "same function as abort-task, but does not sent error message to the ~
   agent that required the task. 
 Arguments:
   agent: agent
   task-id : id of the task to be canceled
   sender: agent that sent the task
   task-frame (opt): task-frame if known
   no-subtaks (key): if t, indicates that no subtasks have been spawned and that it ~
   is not necessary to send cancel messages.
   no-mark (kkey): when t, no black mark is drawn on agent life
Return:
   When called from task process, does not return, otherwise returns the task-id."
  (%abort-cancel-task agent task-id sender :no-subtasks no-subtasks :cancel t 
                      :task-frame task-frame :no-mark no-mark))

;;;--------------------------------------------------------------- CREATE-AGENT-NAME

(defUn create-agent-name (given-name &optional (package *package*))
  "create an internal agent name by prefixing the given name with SA_ One should ~
   use the keywords anyway in the application code.
Argument:
   given-name: e.g. 'MUL-1
Return:
   SA_MUL-1"
  (intern (concatenate 'string "SA_" 
                       (cond ((symbolp given-name) (symbol-name given-name))
                             ((string-upcase given-name))
                             (t (error "bad argument: ~S for agent  name" given-name))))
          package))

#|
(create-agent-name "albert")
SA_ALBERT
(create-agent-name 'albert)
SA_ALBERT
(create-agent-name :albert :albert)
ALBERT::SA_ALBERT
|#
;;;----------------------------------------------------------- CREATE-ASSISTANT-NAME

(defUn create-assistant-name (given-name &optional (package *package*))
  "create an internal assistant name by prefixing the given name with PA_ One should ~
   use the keywords anyway in the application code.
Argument:
   given-name: e.g. 'MUL-1
Return:
   PA_MUL-1"
  (intern (concatenate 'string "PA_" 
                       (cond ((symbolp given-name) (symbol-name given-name))
                             ((string-upcase given-name))
                             (t (error "bad argument: ~S for agent  name" given-name))))
          package))

#|
(create-assistant-name "albert")
PA_ALBERT
(create-assistant-name 'albert)
PA_ALBERT
(create-assistant-name :albert :albert)
ALBERT::PA_ALBERT
|#
;;;------------------------------------------------------------- CREATE-INFERER-NAME

(defUn create-inferer-name (given-name &optional (package *package*))
  "create an internal assistant name by prefixing the given name with IA_ One should ~
   use the keywords anyway in the application code.
Argument:
   given-name: e.g. 'CONTROLLER
Return:
   IA_CONTROLLER"
  (intern (concatenate 'string "IA_" 
                       (cond ((symbolp given-name) (symbol-name given-name))
                             ((string-upcase given-name))
                             (t (error "bad argument: ~S for agent  name" given-name))))
          package))

#|
(create-inferer-name "controller")
OMAS::IA_CONTROLLER
|#
;;;--------------------------------------------------------------- CREATE-MESSAGE-ID

(defUn create-message-id (list-of-used-ids)
  "creates a new message id that will be used for avoiding infinite loops or echos.
Arguments:
   list-of-used-ids: a list of ids we do not want
Return:
   an integer"
  ;; update counter and return subtak-id (bad when reloading)
  (let (id)
    (loop 
      ;; most positive-fixnum is 536870911
      ;; add part of clock time
      (setq id (+ (random 100000000)
                  (mod (get-internal-real-time) 100000000)))
      (unless (member id list-of-used-ids)
        (return-from create-message-id id)))))

#|
(create-message-id '(1 2 3 4 5))
404611249
|#
;;;------------------------------------------------------------- CREATE-POSTMAN-NAME

(defUn create-postman-name (given-name &optional (package *package*))
  "create an internal assistant name by prefixing the given name with PA_ One should ~
   use the keywords anyway in the application code.
Argument:
   given-name: e.g. 'UTC
Return:
   XA_MUL-1"
  (intern (concatenate 'string "XA_" 
                       (cond ((symbolp given-name) (symbol-name given-name))
                             ((string-upcase given-name))
                             (t (error "bad argument: ~S for agent  name" given-name))))
          package))

#|
(create-postman-name "albert")
PA_ALBERT
(create-postman-name 'albert)
XA_ALBERT
(create-postman-name :albert :albert)
ALBERT::XA_ALBERT
|#
;;;--------------------------------------------------------------- CREATE-SUBTASK-ID

(defUn create-subtask-id ()
  "called from skills to start a new subtask. Simply provides a subtask id. This ~
   version uses a global counter rather than gentemp so that we can reset task ids ~
   when debugging. A subtask-id is simply a moisitive number.
Arguments:
   agent: agent (ignored)
Return:
   a positive subtask number, e.g. 233"
  ;; update counter and return subtak-id
  (incf (subtask-number *omas*)))

;;;------------------------------------------------------------------ CREATE-TASK-ID

(defUn create-task-id ()
  "a task id is simply a negative number. The task counter is updated
  Arguments:
  agent: agent (ignored)
  Return:
  a negative task number (e.g. -231)"
  ;; update counter and return the id
  (decf (task-number *omas*)))

#|
? (create-task-id)
-1
|#
;;;-------------------------------------------------------------------- DYNAMIC-EXIT

;;; we have a problem when we call dynamic-exit from a process that is not the
;;; task process, e.g. timer handler or broadcast timer handler. We must kill the
;;; timer process but also the task process
;;; A worse problem is that the system refuses to kill the task process...

(defUn dynamic-exit (agent result &key internal reason)
  "user-called function that takes the result from a task, builds up a message ~
   to forward the answer as required and sends it.
   However, if the task was an internal task no answer is sent back (presumably ~
   the dynamic part of the skill will have processed the result.
   The dynamic-exit is called normally by a process executing the skill, thus it ~
   commits suicide. It will not work if called from a different process (e.g. a ~
   timeout process). It kills the time-limit timer process.
Arguments:
   agent: agent
   result: result to send back to the caller or to the continuation.
   internal (key): if t means that we are getting out of an internal process, it
                   is not necessary to send an answer message
   reason (key): reason for error or failure to be inserted into the answer message
Return:
   we never return from this function. 
   Either the process is killed or we have an error."
  (let* (current-task message answer task-process)
    ;;;    (break "active-task-list: ~S; result: ~S; process: ~S" 
    ;;;           (active-task-list agent) result *current-process*)
    ;; current-process is not necessarily that of the task, it may be a timeout
    ;; process, a broadcast-timeout process, a time-limit process...
    ;; But the function returns the current task-frame
    (setq current-task (agent-process-get-task-frame agent *current-process*))
    ;(format t "~%; dynamic-exit /process: ~S,~%; task-frame: ~S"
    ;  *current-process* current-task)
    (unless current-task
      ;; if we ever get here, then there is something wrong
      (error "attempting a dynamic exit and the task corresponding to process ~S ~
              cannot be found" *current-process*))
    
    ;; we do not return answer messages when the message is an internal message
    (unless internal
      (setq message (message current-task))
      ;; message contains the original message that started the main task
      ;; we return an answer back to the sender of the original 
      ;; message (NIL is supposed to be the external user)
      ;; unless the original message was an inform message JPB1506
      (unless (eql (type! message) :inform) ; JPB1506
        (setq answer
              (make-instance 'MESSAGE
                :from (keywordize agent)
                :type :answer
                :to (or (reply-to current-task)
                        (from! current-task))
                :date (get-universal-time)
                :contents result
                :error-contents reason
                :action (action message)
                :task-id (id current-task)
                :args (args message)
                ))
        ;; send it
        (send-message answer))
      )
    ;; give some time to do the drawings
    (sleep 0.05)
    ;; kill agent time-limit process corresponding to the executed task
    (agent-process-kill agent (timer-process current-task))
    ;; if the agent is learning, then we record the task results
    (when (member :learning (features agent))
      ;; record the task being started (function in the task.lisp file)
      (agent-remember-task agent answer))
    ;; remove task frame from active task list
    (agent-remove-value agent current-task 'active-task-list)
    ;; update agent window
    (agent-display agent)
    ;; get the task process 
    (setq task-process (process current-task))
    
    ;(format t "~%; dynamic-exit /task-process: ~S" task-process)
    
    ;; if we are not calling this function from the task process, we must kill the 
    ;; task process, but for some reason we cannot do it directly...
    (unless (equal task-process *current-process*)
      ;(format t "~%; dynamic-exit /killing current task")
      (agent-process-kill agent task-process) ;jpb1105
      )
    
    ;(format t "~%; dynamic-exit /killing current process...")
    ;;kill current-process, even if it is the time-limit timer process
    (agent-process-kill-current-process agent)
    ;; return nothing since process has been killed
    ;; possible time-limit timer is not killed but will fire harmlessly
    ))

;(agent-display cl-user::mul-2)
;;;=================================================================================
;;;                             ENVIRONMENT
;;;=================================================================================
;;;
;;; Environment is a specific area attached to a particular task of the agent that
;;; can be used to store information to be shared by successive calls to the same 
;;; skill. It only lasts for the duration of the task process. It works as a 
;;; temporary p-list.
;;; The agent memory cannot be used to this purpose, because there is a risk of
;;; confusion among different tasks.
;;; Environment cannot be used with inform messages, since such messages do not
;;; create tasks but are processed by the scan process.

;;;------------------------------------------------------------------ ENV-ADD-VALUES

(defUn env-add-values (agent values tag)
  "replace the agent environment with env.
   Must be called from a task process executing the right task.
Arguments:
   agent: agent
   env: environment part of the agent
   values: list of values to add to existing value
   tag: property
Return:
   new list of values."
  (let* ((current-task-frame (agent-process-get-task-frame agent *current-process*))
         env new-values)
    (if current-task-frame
      (progn
        (setq env (environment current-task-frame))
        ;; get previous values
        (setq new-values (append (getf env tag) values))
        (setf (getf env tag) new-values)
        ;; record them
        (setf (environment current-task-frame) env)
        ;; return new values
        new-values)
      ;; otherwise error
      (error "a task process should be executing when updating environment for ~A" 
             agent))))

;;;------------------------------------------------------------------------- ENV-GET

(defUn env-get (agent tag)
  "gets the value attached to tag from the agent environment.
   Must be called from a task process executing the right task.
Arguments:
   agent: agent
   env: environment part of the agent
   values: list of values to add to existing value
   tag: property
Return:
   new list of values."
  (let* ((current-task-frame (agent-process-get-task-frame agent *current-process*)))
    (if current-task-frame
      (getf (environment current-task-frame) tag) 
      ;; otherwise error
      (error "a task process should be executing when updating environment for ~A" 
             agent))))

;;;------------------------------------------------------------------ END-REM-VALUES

(defUn env-rem-values (agent values tag &key test)
  "removes the list of values in task environment.
   Must be called from a task process executing the right task.
Arguments:
   agent: agent
   env: environment part of the agent
   values: list of values to be removes
   tag: property
   test (key): fonction for the :test option of remove
Return:
   new list of values."
  (let* ((current-task-frame (agent-process-get-task-frame agent *current-process*))
         env new-values)
    (if current-task-frame
      (progn
        (setq env (environment current-task-frame))
        ;; remove values from previous list
        (setq new-values (set-difference (getf env tag) values :test (or test #'eql)))
        ;; update environment
        (setf (getf env tag) new-values)
        ;; record them
        (setf (environment current-task-frame) env)
        ;; return new values
        new-values)
      ;; otherwise error
      (error "a task process should be executing when updating environment for ~A" 
             agent))))

;;;------------------------------------------------------------------------- ENV-SET

(defUn env-set (agent values tag)
  "replace the specified property and values in task environment.
   Must be called from a task process executing the right task.
Arguments:
   agent: agent
   env: environment part of the agent
   values: list of values
   tag: property
Return:
   new list of values."
  (let* ((current-task-frame (agent-process-get-task-frame agent *current-process*))
         env)
    (if current-task-frame
      (progn
        (setq env (environment current-task-frame))
        ;; replace values only for that tag in the env alternated list
        (setf (getf env tag) values)
        ;; record result
        (setf (environment current-task-frame) env)
        ;; return new values
        values)
      ;; otherwise error
      (error "a task process should be executing when updating environment for ~A" 
             agent))))

;;;=================================================================================
;;;                                End ENVIRONMENT
;;;=================================================================================

;;;=================================================================================
;;;                               Synchronizing mechanism
;;;=================================================================================
;;; we use gates for synchronizing local process among threads
;;; *omas* contains 2 lists:
;;;   gate-list: a list of pairs (<nn> . <gate>)
;;;   gate-answers: a list of pairs (<nn> . <answer expr>)
;;; the latter contains some result to transfer to the process that set the gate

;;;---------------------------------------------------------------------- GATE-CLOSE

(defun gate-close (nn)
  "close the specific gate and remove it from the gate list"
  (declare (special *omas*))
  (let ((gate (cadr (assoc nn (gate-list *omas*)))))
    ;; if the gate has already been closed and removed, do nothing
    (when gate
      (mp:close-gate gate)
      (setf (gate-list *omas*)
        (remove nn (gate-list *omas*) :key #'car)))))

#|
(gate-close 1)
NIL
|#
;;;--------------------------------------------------------------------- GATE-CREATE

(defun gate-create ()
  "create a new gate put it into the gate list, return its index (serial nb)"
  (declare (special *omas*))
  (push (list (incf (gate-counter *omas*)) (mp:make-gate nil))
        (gate-list *omas*))
  ;; return is an integer
  (gate-counter *omas*))

#|
(gate-create)
1
|#
;;;------------------------------------------------------------------- GATE-GET-DATA

(defun gate-get-data (nn)
  "recovers data (answer) from gate list using index"
  (declare (special *omas*))
  (cdr (assoc nn (gate-answers *omas*))))

#|
(gate-get-data 1)
"Albert"
|#
;;;----------------------------------------------------------------------- GATE-OPEN

(defun gate-open (nn)
  "used to indicate that the work was completed"
  (declare (special *omas*))
  ;; only do that when gate exists
  (let ((gate (cadr (assoc nn (gate-list *omas*)))))
    (when gate
      (mp:open-gate gate))))

#|
(gate-open 1)

|#
;;;------------------------------------------------------------------- GATE-POP-DATA

(defun gate-pop-data (nn)
  "return the result associated with gate nn if there"
  (declare (special *omas*))
  (let ((answer (cdr (assoc nn (gate-answers *omas*)))))
    (setf (gate-answers *omas*)
      (remove nn (gate-answers *omas*) :key #'car))
    answer))

#|
(gate-pop-data 1)
"Albert"
(gate-answers *omas*)
NIL
|#
;;;------------------------------------------------------------------ GATE-PUSH-DATA

(defun gate-push-data (nn data)
  (declare (special *omas*))
  (push (cons nn data) (gate-answers *omas*)))

#|
(gate-push-data 1 "Albert")
((1 . "Albert"))
|#
;;;=================================================================================
;;;                         End Synchronizing mechanism
;;;=================================================================================

;;;---------------------------------------------------------------------- FORGET-ALL

(defUn forget-all (agent)
  "used to wipe-out agent's memory
Arguments:
   agent: agent."
  (setf (memory agent) nil))

;;;------------------------------------------------------ GET-AGENT-NUMBER-OF-TIMERS

;(get-agent-number-of-timers)
(defUn get-agent-number-of-timers ()
  "builds an a-list with the number of timers of the agents from the local coterie. ~
   An agent has currently one timer per task. Uses the *local-agents* list. 
   Used by the display process, asynchronous with respect to calls.
Arguments:
   none
Return:
   an a-list, e.g. ((:FAC . 2)(:MUL . 1))"
  (mapcar #'(lambda (xx) (cons (car xx) (length (active-task-list (cdr xx)))))
          (local-agents *omas*)))

;;;---------------------------------------------------------------- GET-AGENT-STATES

;(get-agent-states)
(defUn get-agent-states ()
  "builds an a-list with the state of the agents from the local coterie. An agent ~
   is either :busy if it is executing some task, :idle otherwise. Uses the ~
   *local-agents* list.
Arguments:
   none
Return:
   an a-list, e.g. ((:FAC . :BUSY)(:MUL . :IDLE))"
  (mapcar #'(lambda (xx) (if (active-task-list (cdr xx)) 
                           (cons (car xx) :busy)
                           (cons (car xx) :idle)))
          (local-agents *omas*)))

;;;----------------------------------------------------------------- GET-ENVIRONMENT
;;; deprecated, use env-get

(defUn get-environment (agent)
  "get the environment area contained in the task frame representing the current ~
   task. If no task is present declares an error. To be called from a requested ~
   skill, not an informed one since inform executes in the scan process.
Arguments:
   agent: agent."
  (let ((current-task (agent-get-task-frame-from-process agent *current-process*)))
    (if current-task
      (environment current-task)
      (error 
       "process ~S should be a task process when getting environment for ~S"
       *current-process* agent))))

#|
      (defun get-internal-time-string ()
        "produces a string with hr:min:sec from internal time"
        (multiple-value-bind (sec min hr)(get-decoded-time) 
          (format nil "~S:~S:~S" hr min sec)))
|#
;;;--------------------------------------------------------------------- GET-FIELD

(defun get-field (field-name alist)
  "to be used when properties are strings.
Arguments:
   field-name: strings e.g. \"first name\" or symbols or keywords
   alist: a-list in which properties are strings
Return:
   the value associated with the property (usually a string or NIL"
   (cdr (assoc field-name alist :test #'string-equal)))

#|
(get-field "titre" 
           '(("name" . "sais pas")("titre" . "Le bon choix")("reste" . "junk")))
"Le bon choix"
(get-field "title" 
           '(("name" . "sais pas")("titre" . "Le bon choix")("reste" . "junk")))
NIL
(get-field '((:en "title")(:fr "une" "titre"))
           '(("name" . "sais pas")("titre" . "Le bon choix")("reste" . "junk")))
Error: EXCL::STRING1 argument should be a string
(get-field 'titre
           '(("name" . "sais pas")("titre" . "Le bon choix")("reste" . "junk")))
"Le bon choix"
(get-field :titre
           '((:name . "sais pas")(:titre . "Le bon choix")(:reste . "junk")))
"Le bon choix"
|#
;;;?--------------------------------------------------------------------- GET-FIELD+

(defun get-field+ (field-name a-list)
  "used to retrieve data from a list of prop values, even when field-name is mln.
Arguments:
   field-name: a symbol, string or mln
   a-list: a list of properties and values where values can be mln
Return:
   the value if a prop is part of the mln"
  (cdr (assoc field-name a-list :test #'equal+)))

;;;  (cond
;;;   ;; if field-name is nln try equal+
;;;   ((mln::mln? field-name)  ; jpb 1406
;;;    (dolist (pair a-list)
;;;      (if (equal+ field-name (car pair))
;;;          (return-from get-field+ (cdr pair)))))
;;;   (t
;;;    (cdr (assoc field-name a-list :test #'equal+)))))

#|
(let ((*language* :EN))
  (get-field+ '((:en "name") (:fr "nom")) '(("a" 1)(2 22)("name" "John")
                                        ("nom" "Jean"))))
("John")

(let ((*language* :EN))
  (get-field+ '((:en "name" "label") (:fr "nom")) 
              '(("a" 1)(2 22)("name" "John")("nom" "Jean"))))
("John")

(let ((*language* :EN))
  (get-field+ '((:en "name" "label") (:fr "nom")) 
              '(("a" 1)(2 22)("label" "John")("nom" "Jean"))))
("John")

(let ((*language* :EN))
  (get-field+ '((:en "name" "label") (:fr "nom")) 
              '(("a" 1)(2 22)(((:fr "libellé") (:en "tag" "label")) "John")
                ("nom" "Jean"))))
("John")
(let ((*language* :fr))
  (get-field+ '((:en "name" "label") (:fr "nom")) 
              '(("a" 1)(2 22)("label" "John")("nom" "Jean"))))
("Jean")
|#
;;;------------------------------------------------------------------------- GET-STR

(defun get-str (prop alist)
  "same as getstr but returns '(\"\") instead of nil on failure"
  (or (cdr (assoc prop alist :test #'equal+)) '("")))
  
;;;-------------------------------------------------------------------------- GETSTR

(defun getstr (prop alist)
  (if (every #'(lambda (xx)(stringp (car xx))) alist) 
      (cdr (assoc  prop alist :test #'string-equal))
    ;; warn returns nil
    (error "getstr: some of the properties are not strings in: ~S" alist)))
#|
(getstr "c" '(("a" 1)("b" 2)("c" "albert")("d" 4)("c" 5)))
("albert")
(getstr "c" '(("a" 1)("b" 2)("c" "albert")("d" 4)(c 5)))
<error>
|#
;;;------------------------------------------------------------------------ GET-TEXT

(defUn get-text (mln)
  "Get the string corresponding to *language* from  multilingual string. If not ~
   present, get the English one, if not present get a random one.
Argument:
   mln: a multilingual name (deined in MOSS)
Return:
   2 values: first is a string, second is a language tag
Error:
   if not an MLN."
  (mln::get-canonical-name mln))

;;;------------------------------------------------------------------ GET-TIME-LIMIT

(defUn get-time-limit (agent)
  "obtain the time-limit associated with the executing task. If none, then returns ~
   most positive integer in the system.
Arguments:
   agent: agent."
  (let ((current-task (agent-get-task-frame-from-process agent *current-process*)))
    ;; if no current task then error
    (unless current-task
      (error "no task execting in process ~S" *current-process*))
    ;; if we have a tile-limit value on current-task, then return it
    (or (and current-task (time-limit current-task))
        (highest-time-limit *omas*))))

;;;----------------------------------------------------------- GET-VISIBLE-AGENT-IDS

(defUn get-visible-agent-ids ()
  "get the list of ids of the non hidden local agents
Arguments:
   none
Return:
   a list of agent ids."
  (mapcan #'(lambda (xx) (if (agent-visible? (car xx)) (list (cdr xx))))
          (local-agents *omas*)))
#|
(get-visible-agent-ids)
(#<AGENT ALBERT> #<AGENT SA-ADDRESS> #<AGENT BIBLIO>)
|#
;;;-------------------------------------------------------------------- LOCAL-AGENT?

(defUn local-agent? (agent-key)
  "test if an agent represented by a keywod is a local agent, i.e., it is in the ~
   *local-agents* a-list.
Arguments:
   agent-name: a symbol, presumably an agent name
Return
   nil or the agent structure"
  ;; outgoing messages normally contain keywords as agent names
  (cdr (assoc agent-key (local-agents *omas*))))

;;;----------------------------------------------------------------- MAKE-HTML-TABLE

(defun make-html-table 
    (prop-titles entries &key header-color text-color body-bgcolor)
  "builds an HTML string that represents a table with the specified columns.
Arguments:
   prop-titles: alist like ((\"pays\" \"Pays\")...)
   header-color (key): background color of header
   body-bg-color (key): background color of the main table cells
Return:
   an HTML string."
  (let ((table
         `("  <TABLE WIDTH=\"100%\" BORDER=1>"
           ,@(make-html-table-body prop-titles entries header-color text-color
                                   body-bgcolor)
            "  </TABLE>")))
    (format nil "~{~A~^~%~}" table)))

#|
(make-html-table prop-titles entries :header-color "blue" :text-color "white"
                 :body-bgcolor "white")
"  <TABLE WIDTH=\"100%\" BORDER=1>
    <tr bgcolor=\"blue\">
      <td><font color=\"white\">Pays</font></td>
      <td><font color=\"white\">Contact</font></td>
      <td><font color=\"white\">Partenaire</font></td>
      <td><font color=\"white\">URL, HTML</font></td>
      <td><font color=\"white\">Contact UTC</font></td>
    </tr>
    <tr color=\"white\">
      <td> Chine</td>
      <td> Z. Lin</td>
      <td> Academia Sinica</td>
      <td> </td>
      <td> J.-P. Barthès</td>
    </tr>
    <tr color=\"white\">
      <td> Japon</td>
      <td> K. Sugawara</td>
      <td> CIT</td>
      <td><a href=\"http://www.utc.fr/~barthes\">http://www.utc.fr/~barthes</a></td>
      <td> J.-P. Barthès, C. Moulin</td>
    </tr>
  </TABLE>"
|#
;;;------------------------------------------------------------- MAKE-HML-TABLE-BODY

(defun make-html-table-body 
    (prop-titles entries header-color &optional text-color body-bgcolor)
  "make the list of rows in the table
Arguments:
   prop-titles: an a-list of pairs (<prop> <title>)
   entries: a list of a-list entries
   header-color: the background color of the header
   text-color (opt): the text color in the header
   body-bg-color (opt): the background color in the table body
Return:
   an HTML string for displaying the table to be inserted into a web page"
  (let (result)
    ;; put title
    (push
     (make-html-table-header prop-titles header-color text-color)
     result)  
    ;; then each fragment
    (dolist (item entries)
      (push (make-html-table-row prop-titles item body-bgcolor) result)
      )
    (reverse result)))

#|
(make-html-table-body prop-titles entries "yellow" "black" "white")
("    <tr bgcolor=\"yellow\">
      <td><font color=\"black\">Pays</font></td>
      <td><font color=\"black\">Contact</font></td>
      <td><font color=\"black\">Partenaire</font></td>
      <td><font color=\"black\">URL, HTML</font></td>
      <td><font color=\"black\">Contact UTC</font></td>
    </tr>" "    <tr>
      <td> Chine</td>
      <td> Z. Lin</td>
      <td> Academia Sinica</td>
      <td> </td>
      <td> J.-P. Barthès</td>
    </tr>" "    <tr>
      <td> Japon</td>
      <td> K. Sugawara</td>
      <td> CIT</td>
      <td><a href=\"http://www.utc.fr/~barthes\">\"http://www.utc.fr/~barthes\"</a></td>
      <td> J.-P. Barthès, C. Moulin</td>
    </tr>")
|#
;;;----------------------------------------------------------- MAKE-HTML-TABLE-ENTRY

(defun make-html-table-entry (prop-pattern entry)
  "builds a single cell of the table according to prop-title.
Arguments
   prop-pattern: a list like (<prop><prop-title>{<html-tag>}{format-fcn})
   entry: alist describing the entry
Return:
   an HTML string with eventual http reference"
  (let ((val (getstr (car prop-pattern) entry))
        (fn (cadddr prop-pattern)))
    ;; if the 4th element of pattern is present it mustbe a formatting function
    (when fn
      (setq val (funcall fn val)))
    (cond 
     ;; if third element (html tag) is present, make an html pointer
     ((and val (caddr prop-pattern))
      (format nil "      <td><a href=~S>~A</a></td>" (car val) (car val)))
     (t
      (format nil "      <td> ~{~A~^, ~}</td>" 
        (or val '("")))))))

#|
(setq entry '(("pays" "Chine") ("correspondant" "Z. Lin")
              ("partenaire" "Academia Sinica")
              ("correspondant UTC" "J.-P. Barthès")
              ("URL" "http://www.utc.fr/~barthes")))
(make-html-table-entry '("URL" "URL" :html) entry)
"      <td><a href=\"http://www.utc.fr/~barthes\">\"http://www.utc.fr/~barthes\"</a></td>"
|#
;;;---------------------------------------------------------- MAKE-HTML-TABLE-HEADER

(defun make-html-table-header (prop-titles color &optional text-color)
  (let (result)
    (unless text-color (setq text-color "black"))
    (push (format nil "    <tr~A>" 
            (if color (format nil " bgcolor=~S" color) ""))
          result)
    (dolist (item prop-titles)
      (push (format nil "      <td><font color=~S>~A</font></td>" 
              text-color (cadr item)) result))
    (push "    </tr>" result)
    (format nil "~{~A~^~%~}" (reverse result))))

#|
(make-html-table-header prop-titles "yellow" "blue")
"    <tr bgcolor=\"yellow\">
      <td><font color=\"blue\">Pays</font></td>
      <td><font color=\"blue\">Contact</font></td>
      <td><font color=\"blue\">Partenaire</font></td>
      <td><font color=\"blue\">Contact UTC</font></td>
    </tr>"
|#
;;;------------------------------------------------------------- MAKE-HTML-TABLE-ROW

(defun make-html-table-row (prop-list entry &optional bgcolor)
  "builds a single row of the HTML table."
  (let (result)
    (push (if bgcolor
              (format nil "    <tr bgcolor=~S>" bgcolor)
            "    <tr>")
          result)
    (dolist (prop-pattern prop-list)
      (push (make-html-table-entry prop-pattern entry) result))
    (push "    </tr>" result)
    (format nil "~{~A~^~%~}" (reverse result))))

#|
(make-html-table-row prop-titles (car entries))
"    <tr>
      <td>Chine</td>
      <td>Z. Lin</td>
      <td>Academia Sinica</td>
      <td> </td>
      <td>J.-P. Barthès</td>
    </tr>"
(make-html-table-row prop-titles (cadr entries))
"    <tr>
      <td> Japon</td>
      <td> K. Sugawara</td>
      <td> CIT</td>
      <td><a href=\"http://www.utc.fr/~barthes\">\"http://www.utc.fr/~barthes\"</a></td>
      <td> J.-P. Barthès, C. Moulin</td>
    </tr>"
(make-html-table-row prop-titles (cadr entries) "white")
"    <tr bgcolor=\"white\">
      <td> Japon</td>
      <td> K. Sugawara</td>
      <td> CIT</td>
      <td><a href=\"http://www.utc.fr/~barthes\">http://www.utc.fr/~barthes</a></td>
      <td> J.-P. Barthès, C. Moulin</td>
    </tr>"

(setq prop-titles '(("pays" "Pays") ("correspondant" "Contact")
                ("partenaire" "Partenaire")("URL" "URL" :html)
           ("correspondant UTC" "Contact UTC")))

(setq entries '((("pays" "Chine") ("correspondant" "Z. Lin")
                ("partenaire" "Academia Sinica")
                 ("correspondant UTC" "J.-P. Barthès"))
                (("pays" "Japon")("correspondant" "K. Sugawara")
                 ("partenaire" "CIT")("URL" "http://www.utc.fr/~barthes")
                 ("correspondant UTC" "J.-P. Barthès" "C. Moulin"))))
|#    
;;;-------------------------------------------------------------------- MAKE-KEYWORD

(defUn make-keyword (input)
  "takes a symbol or a string in imput and returns a keyword"
  (cond
   ((null input) (error "null input"))
   ((keywordp input) input)
   ((symbolp input) (intern (symbol-name input) :keyword))
   ((stringp input) (intern (string-upcase input) :keyword))
   (t (error "bad input: ~S" input) nil)
   ))

#|
(make-keyword nil)
> Error in process XA_SPY-23587804-scan: null input
> While executing: MAKE-KEYWORD
(make-keyword :albert)
:ALBERT
(make-keyword 'moss::albert)
:ALBERT
:EXTERNAL
(make-keyword "Albert")
:ALBERT
:EXTERNAL
(make-keyword '(a b c))
> Error in process XA_SPY-23587804-scan: bad input: (A B C)
> While executing: MAKE-KEYWORD
|#
;;;-------------------------------------------------------------------- MASTER-TASK?

(defUn master-task? (agent task-id)
  "checks if the task with id task-id is one of the master's task.
Arguments:
   agent: agent
   task-id: id of the task to be checked."
  ;; pending request is one of the master's bin
  (member-if #'(lambda (xx) (eql task-id (task-id xx))) 
             (pending-requests agent)))

#|
      (defun modify-time-limit (agent delta)
        "modify the current task time-limit, setting it to current-time + delta.
Arguments:
   agent: agent
   delta: delay to add to the existing time-limit."
        ;; to do that we must first recover old time-limit kill the previous task-timer
        ;; and install a new one
        :done))

(defun modify-time-limit-absolute (agent date)
  "modify the current task time-limit, setting it to date.
Arguments:
   agent: agent
   date: absolute date for the time-limit."
  ;; same as modify-time-limit
  :done))
|#
;;;---------------------------------------------------------------------------- OMAS

(defUn omas ()
  "start function. Set up a special process that creates a control panel. ~
   If the local coterie is connected to the net (*net-broadcast* is true) ~
   then initializes net UDP interface."
  ;(acl/mcl-process-run-function "display" #'omas-set-display)
  (when (net-broadcast *omas*)
    ;; This start a receiving process *omas-net-receive*
    ;; and a dispatching process *omas-net-dispatch* that delivers the messages
    ;; to the local agents
    (omas-net-initialize))
  ;; due to bug in ACL 8.2
  #+MICROSOFT-32
  (initialize-message-property-dictionary)
  ;; open the initial window
  (make-omas-init-window)
  :done)

;;;----------------------------------------------------------------------- OMAS-EXIT

(defUn omas-exit ()
  "exits from omas, closing net communications and cleaning whatever needs ~
   to be cleaned."
  (omas-net-close)
  ;; should also close persistent databases for agents that have a database
  ;; a single base is opened...
  (moss::db-close)
  ;; should close any opened socket; *omas* should contain the list of opened sockets
  ;; should also close all active windows
  )

;;;------------------------------------------------------------------------------ PA

(defUn pa (pa-key)(setq *package* (find-package pa-key)))

;;;--------------------------------------------------------------- PENDING-SUBTASKS?

(defUn pending-subtasks? (agent)
  "return the list of pending tasks. If processing an answer message, then ~
   the list contains at least 1, the one corresponding to the answer being processed.
   The function must be called from the process executing the task for which subtasks ~
   are checked, typically from a skill.
Arguments:
   agent: agent."
  (let ((task-frame (agent-get-task-frame-from-process agent *current-process*)))
    (if task-frame
      (subtask-list task-frame)
      (warn "No task active when calling pending-subtasks?"))))

;;;------------------------------------------------------------- PROCESS-NEXT-TASK

(defun process-next-task (agent task-list args)
  "takes a list of tasks and executes the first one that has the properties ~
    where-to-ask and action. Remaining task list is returned as second value.
Arguments:	
   agent: current agent
   task-list: a list of tasks
   args: arguments for the message to be sent
Return:
   if a message was sent returns 2 values :message-sent and the rest of task-list, 
   nil otherwise"
  (when task-list
    (let ((selected-task (car task-list))
          to-arg action-arg)
      (loop
        ;; for the next line to execute one must be inside a task process...
        (when (and (setq to-arg (car (send selected-task '=get "where to ask")))
                   (setq action-arg (car (send selected-task '=get "message action"))))
          (format t "~%; process-next-task /activating subtask: ~S" selected-task)
          ;; set pattern to (:pattern . :html) to ask for an html string in return
          ;(setq args (cons '(:pattern . :html) (remove :pattern args :key #'car)))
          (send-subtask agent :to to-arg :action action-arg :args (list args))
          ;; save rest of task-list
          ;(env-set agent (cdr task-list) :next-tasks)
          (return-from process-next-task (values :message-sent (cdr task-list))))
        
        (format t "~%; process-next-task / task ~S failed." selected-task)
        
        ;; if task-list empty return
        (unless task-list 
          (format t "~%; process-next-task /no more task we quit.")
          (return-from process-next-task nil))
        
        ;; otherwise pop pair list
        (setq selected-task (pop task-list))
        ;; go to the start of the loop
        )
      )))

;;;------------------------------------------------------------- PURELY-LOCAL-SKILL?

(defUn purely-local-skill? (agent skill)
  "checks if a skill operates locally, i.e., does not spawn any subtask. This is ~
   verified when there is no dynamic part to the skills.
   Returns nil if agent does not have the skill.
Arguments:
   agent: agent
   skill: skill to check."
  (and (agent-has-skill? agent skill)
       (null (agent-get-dynamic-skill agent skill))))

;;;----------------------------------------------------------------- RECEIVING-AGENT

(defMethod receiving-agent ((message message))
  "getting the key of the receiver of the message.
Argument:
   message: shoulc be a message object
Return:
   the key of the sender or nil if message is not a message."
  (to! message))

;;;--------------------------------------------------------------------- REM-FIELD

(defun rem-field (field-name alist)
  "to be used when properties are strings; removes a clause from the list.
Arguments:
   field-name: e.g. \"first name\"
   alist: a-list in which properties are strings
Return:
   the value associated with the property (usually a string or NIL"
  (remove field-name alist :test #'string-equal :key #'car))


#|
(rem-field "titre" 
           '(("name" . "sais pas")("titre" . "Le bon choix")("reste" . "junk")))
(("name" . "sais pas") ("reste" . "junk"))
(rem-field "title" 
           '(("name" . "sais pas")("titre" . "Le bon choix")("reste" . "junk")))
(("name" . "sais pas") ("titre" . "Le bon choix") ("reste" . "junk"))
|#
;;;---------------------------------------------------------------- REMOVE-TRACE-TAG

(defun remove-trace-tag (tag)
  "removes tag from the list of trace tags, disalling tracing the corresponding ~
   execution."
  (declare (special *omas*))
  (let ((tag-list (debugging *omas*)))
    (setf (debugging *omas*) (remove tag tag-list))))

;;;---------------------------------------------------------------- RESET-ALL-AGENTS

(defMacro ral () `(reset-all-agents))

(defUn reset-all-agents ()
  "clear all agents, reseting input and output trays, and emptying tasks; ~
   also reseting number of tasks to 0; and clock to 0"
  ;(dolist (agent (cons *scriptor* *agents*))
  (dolist (agent (mapcar #'cdr (local-agents *omas*)))
    (agent-reset agent :reset-tasks t :no-position t))
  ;(setq *clock* 0)
  ;; reset processes (i.e., kill all extra processes)
  (dolist (process *active-processes*)
    (unless (member process (basic-processes *omas*))
      (process-kill process)))
  ;; reset the task number generator so that debugging is made easier 
  (setf (task-number *omas*) 0
        (subtask-number *omas*) 0)
  :done)

;;;-------------------------------------------------------------- RESET-TRACE-SKILLS

(defun reset-trace-skills ()
  (setf (trace-skills *omas*) nil))

;;;------------------------------------------------------------------ SAVE-SELECTION

(defun save-selection (agent object-list)
  "save the list of object-ids into the last-selection slot of the agent."
  (setf (last-selection agent) object-list))

;;;---------------------------------------------------------------- SELECT-BEST-BIDS

;(trace (select-best-bids :step t))
;(untrace select-best-bids)

(defUn select-best-bids (agent bid-list)
  "select best bids in a list of bids according to job-parameters found in :contents. ~
   Currently they are: start-time execution-time quality.
   For now best bid is the earlier job.
Arguments:
   agent: agent
   bid-list: list of submitted bids (message objects)
Return:
   the list of best top bids."
  (declare (ignore agent))
  ;; since we could select several bids, we return a list
  (let* ((sorted-bid-list (sort bid-list #'better-bid?))
         (ref (car sorted-bid-list)))
    ;(format t "~%; select-best-bids /sorted-bid-list:~%  ~S" sorted-bid-list)
    ;; we should keep only the bids that have the best values
    (setq sorted-bid-list 
          (remove-if-not
           #'(lambda (xx) (eq (better-bid? ref xx :test-equality t) :same))
           sorted-bid-list))
    ;(format t "~%; select-best-bids /result:~%  ~S" sorted-bid-list)    
    ;; return result
    sorted-bid-list))

;;;--------------------------------------------------------------------- SEND-INFORM

(defUn send-inform (agent &key (action :INFORM) to args (delay 0))
  "prepares a message containing the parameters for issuing an inform to ~
   another agent. Default action is :INFORM.
   Does not set up a subtask.
Arguments:
   agent: agent
   action: (key) skill to be invoked (default: INFORM)
   to: (key) receiver
   args: (key) arguments to the inform message
   delay: additional delay in seconds (default 0)."
  (declare (ignore delay))
  ;; prepare to send the message
  (send-message (message-make-inform agent to action args))
  :done)

;;;----------------------------------------------------------- SEND-INTERNAL-MESSAGE
;;; this is a special function since we do not send the message to anybody else, we
;;; do not display it but simply put it into our own mailbox

(defun send-internal-message (agent message)
  "puts a message into our own mailbox provately.
Arguments:
   agent: current agent
   message: message to deliver
Return:
   :done"
  (%agent-add agent message :input-messages)
  :done)

;;;------------------------------------------------------------------- %SEND-MESSAGE
;;; before displaying a message, we must draw the life lines of the agents. 
;;; They are drawn by the span process of the spy agent. At the time they are drawn
;;; the state of an agent may no longer be the one in which it was before launching
;;; the message. Thus we record the state of local agents before the message is sent
;;; and put it onto the p-list of the agent key. Then, when drawing life lines, we
;;; will remove the information (status, time-limit processes, timeout processes)
;;; First we try something simpler: delay the sending process

(defUn %send-message (message &optional (locally? nil))
  "does the actual send as follows:
   - if the message is emitted by a local agent and the to-field is not :all-and-me ~      
     then we remove the from agent from the local agent list
   We send a copy of the message to all agents left in the local-agent-list.
   When (net-broadcast *omas*) is on, we also broadcast on the net
Argument:
   message: message to send (unchecked)
   locally? (opt): if T do not send through the network (default is nil)."
  (drformat :send 0 "~2%;---------- Entering %send-message")
  (dformat :send 0 "message ~S" message)
  ;; first get the list of local agent structures from global list
  (let* ((local-agent-list (mapcar #'cdr (local-agents *omas*)))
         (local-agent (local-agent? (from! message)))
         ;(spy-agent (%agent-from-key (spy-name *omas*))))
         (spy-agent (symbol-value (spy-name *omas*)))
         (receiver (to! message)))
    
    ;; first check if the sender and receiver are the same, which means an internal
    ;; message. In that case we deliver the message and quit
    (when (equal (from! message) receiver)
      ;; if local-agent is nil, this means that either there is an error on the
      ;; sender, or the sender is not part of the local site
      ;(format t "~%;+++ %send-message /from: ~S, local agent: ~S" 
      ;  (from! message) local-agent)
      (unless local-agent
        (error "sender: ~S in message ~S is not a local agent" 
          (from! message) message))
      ;; don't make a copy of the message
      ;(%agent-add local-agent message :input-messages)
      ;; however send the message to :SPY for drawing the message         
      (%agent-add spy-agent (message-clone message) :input-messages)
      (push message (input-messages local-agent))
      
      ;(format t "~%;+++ %send-message /input-messages(~S): ~S~%" 
      ;  (key local-agent) (input-messages local-agent))
      ;;update receiver's display, agent must not be keyword
      (agent-display local-agent)
      ;; exit
      (return-from %send-message :done))
    
    ;; remove sender from the list whenever :all is specified or if we have a
    ;; conditional addressing
    (when (and local-agent 
               (or (eql receiver :all)
                   (and (listp receiver)(eql (car receiver) :_cond))))
      (setq local-agent-list (remove local-agent local-agent-list :test #'equal)))
    
    ;;;    (when (and local-agent (not (eql (to! message) :all-and-me)))
    ;;;      (setq local-agent-list (remove local-agent local-agent-list :test #'equal)))
    
    ;; first we record the state of all local agents, putting date, status, 
    ;; time-limits, and timeouts onto the plist of the agent keys
    (%record-message-agents-info)
    
    ;; send a copy to the spy agent so that it can display it
    (%agent-add spy-agent (message-clone message) :input-messages)
    ;(format t "~&; %send-message /adding message to spy mailbox: ~&  ~S" message)
    ;; let SPY display something
    (sleep 0.1)
    
    ;; send a copy to all our local agents, except maybe to ourselves
    (dolist (to-agent (remove spy-agent local-agent-list))
      (%agent-add to-agent (message-clone message) :input-messages)
      ;;update receiver's display
      (agent-display to-agent))
    
    ;; if network on, then broadcast to other machines
    ;; but do not broadcast internal or system messages
    (when (and (net-broadcast *omas*) 
               (not locally?)
               (not (member (type! message) '(:internal :sys-inform))))
      (dformat :send 0 "sending message over the net")
      ;(ignore-errors
      (omas-net-send message))
    ;)
    ;; end
    :done))

;;;-------------------------------------------------------------------- SEND-MESSAGE
;;; in practice this function:
;;;  - if :from is nil set it to local user
;;;  - if :to is local user, post result into control panel
;;;  - if :from is not local user, trace message eventually
;;;  - put date, task-id if necessary, ship message to %send-message

;(dformat-set :send 0)

(defUn send-message (message &key (locally? nil))
  "send a message to other agents.
   - If the target is nil then we assume that the agent is the user. We currently ~
   print the answer in the lisp listener (debugging mode) and show it in the ~
   control panel
   - if the target is :all then we send the message to all local agents
   - if the target is :all-and-me we do the same as for :all including ourselves
   - if the target is a list of agents, it must be a list of agent names,
      then we send to each agent of the list
   - if the target is a single agent name, then we send the message to the target.
   In practice we send the message to all agents, but draw only what was intended ~
   in order to keep a legible graph. In addition we note the current state of the ~
   agents, since posting will be done asynchronously, and thus the agent state ~
   cannot be recovered at that time.
Arguments:
   message: message to send (either structure or key)
   locally? (opt): if true does not send the message through the network
Return:
   :message-sent"
  (let (to-address sender receiver message-copy)
    ;; if key recover structure
    (when (keywordp message) (setq message (%message-from-key message)))
    ;;check first validity of message
    (unless (typep message 'message)
      (error "Bad message format: ~S in process ~S" message *current-process*))
    
    ;; if sender is nil, change it to local-user, e.g. :<ODIN-USER>
    ;(unless (from! message) (setf (from! message) (local-user *omas*)))
    
    ;; get sender and target, when target is nil, change it to local user
    ;; keywordize sender and receivers
    (setq to-address (to! message))
    (setq sender (keywordize (or (from! message)(local-user *omas*)))
        ;; receiver should never be nil
        receiver 
          ;; careful: must not keywordize :_cond queries!
          (if (and (listp to-address)
                   (eql (car to-address) :_cond))
              to-address
            (keywordize (or to-address (local-user *omas*)))))
    (dformat :send 0 "receiver: ~S" receiver)
    
    ;; update message structure
    (setf (from! message) sender)
    (setf (to! message) receiver)
    
    ;; show messages when agent is not the local USER, e.g. :<ODIN-USER>
    ;; because local USER is not an agent structure...
    (unless (eql sender (local-user *omas*))
      ;; print in the ACL TEXT window
      (agent-trace sender "sending: ~S" (message-format message)))
    
    ;; done by the SPY agent JPB 1404
    ;; if receiver is local user, then post the result into the control panel
    (when (eql receiver (local-user *omas*))
      ;; post message into the upper left slot of the control panel
      (OP-post-message message))
    
    ;; copy the original message (why is this useful? JPB0904)
    (setq message-copy (message-clone message))
    (if (get :send :trace) (message-print message-copy))
    
    ;; we set the date to current time
    (setf (date message-copy) (get-universal-time))
    ;; and add a computed task-id if none is there
    ;; when message has no task-id slot, we create one
    (when (or (null (task-id message-copy))
              (eql (task-id message-copy) :new))
      ;; and put it into message
      (setf (task-id message-copy) (create-task-id)))
    (if (get :send :trace) (message-print message-copy))
    
    ;(format t "~%; send-message /message-copy: ~%  ~S" message-copy)
    ;; now send effectively the message, unless locally? is T
    (%send-message message-copy locally?)
    ;; when no error, then show message
    ;(message-draw message-copy)
    ;(sleep 0.001)
    ;; print a color version of the message
    ;(when *trace-messages* (message-trace message-copy))

    :message-sent))

;;;-------------------------------------------------------------------- SEND-SUBTASK
;;; if there is a timeout on a subtask, then the timeout will interrupt the thread
;;; note that a timeout handler is common to all subprocesses; its code will have
;;; to test where the timeout comes from
;;; we cannot use the with-timeout macro since the subtask is launched from the
;;; thread running the skill, thus we have to create a specific timer

(defUn send-subtask (agent &key from to action args (repeat-count 2) task-id delay
                           timeout time-limit 
                           (protocol :basic-protocol)
                           (strategy :take-first-answer) 
                           ack once-only
                           type)
  "prepares a message containing the parameters for issuing a subtask to ~
   another agent. Builds a subtask-frame and adds it to the subtask-list slot.
   Checks the :protocol variable and determines the output message accordingly.
   Function is executed from a skill.
Arguments
   agent: agent sending the subtask
   from (key): ignored, since message is sent from the current agent
   to: (key) agent name key
   action: (key) name of the skill required
   args: (key) arguments for the skill
   ack (key): if t, wants an acknowledgement message
   once-only (key): (deprecated)
   protocol: (key) protocol for the message, default is :basic-protocol.
   repeat-count (key): (deprecated, as a kludge is set to 2)
   strategy (key): how to process broadcast answers: can be :take-first-answer
       (default), :collect-answers or (:take-first-n-answers nn) 
   subtask-id (key): specific id to identify the subtask. It should be unique. 
       by default it is created by OMAS
   timeout: (key) timeout delay allowed for executing the subtask default is none
Return:
   :done"
  (declare (ignore from delay type))
  (drformat :send 2 "~2%;---------- Entering send-subtask")
  ;; if agent is IDE (user) then error
  (if (or (null agent)
          (eql agent :<user>)
          (eql agent (local-user *omas*)))
      (error "agent sending subtask cannot be the IDE user"))
  
  (let (message subtask-frame current-task-frame timeout-process
                (subtask-id task-id))
    ;; create subtask identifier if none is provided
    (unless subtask-id (setq subtask-id (create-subtask-id)))
    (dformat :send 3 "subtask-id: ~S" subtask-id)
    ;; make up message (KLUDGE: we set repeat-count to 2 before we eliminate it
    ;; completely in order to send messages only once)
    (setq message
          (make-instance 'message
            :type :request :protocol protocol :from (keywordize agent) :to to
            :date (get-universal-time) :time-limit time-limit
            :timeout timeout :repeat-count 2 :strategy strategy
            :action action :task-id subtask-id :args args :ack ack))
    (dformat :send 2 "message: ~S" message)
    
    ;; task-frame is that of current process
    (setq current-task-frame 
          (agent-process-get-task-frame agent *current-process*))
    ;; must exist
    (unless current-task-frame 
      (error "a main task must exist for agent ~S to be able to send subtasks" 
        agent))
    
    ;; creating a new subtask frame
    (setq subtask-frame
          (make-instance 'SUBTASK-FRAME
            :id subtask-id
            :parent-task current-task-frame
            :message message
            :to to
            :start-time (get-universal-time)))
    ;; add an entry into the subtask-list slot of the task-frame
    (setf (subtask-list current-task-frame)
      (agent-replace-task-object-in-list subtask-frame 
                                         (subtask-list current-task-frame)))
    
    ;;=== TIMEOUT
    ;; if broadcast or multicast we set up a special timer
    (cond
     ((or (member to '(:all :all-but-me)) ; broadcast
          (listp to))                     ; multicast
      (setq timeout (or timeout (broadcast-default-timeout *omas*)))
      ;; we set up a broadcast timeout
      (setq timeout-process 
            (set-broadcast-timeout agent subtask-frame timeout)))
     
     ;; otherwise we set a regular timeout timer if timeout specified
     (timeout
      ;; set timer returns a process object (delay is in seconds)
      (setq timeout-process 
            (set-timeout-timer agent subtask-frame timeout))))
    
    ;; install the timeout-timer in the subtask-frame of the task
    (when timeout-process
      (setf (timeout-process subtask-frame) timeout-process
        (timeout subtask-frame) timeout
        ;; we want to send message once thus do as we sent it already twice!
        (repeat-count subtask-frame) 2))       
    
    ;;=== CONTRACT NET, but SINGLE DESTINATION
    ;; when contract-net but single "to" agent, remove the contract-net feature
    ;; since this is stupid
    (when (and (eq :contract-net protocol)
               (symbolp to)
               (not (member to '(:all :all-but-me)))
               )
      (setf (protocol message) :basic-protocol))                 
    
    ;;=== CONTRACT NET
    ;; when contract-net, broadcast or multicast change type of message to 
    ;; :call-for-bids
    ;; phase 1 for the attribution of the job
    (when (and (eq :contract-net protocol)
               (or (member to '(:all :all-but-me))
                   (listp to)))
      ;; change message type 
      (setf (type! message) :call-for-bids)
      ;; set call-for-bids timer
      ;; adding task-frame as arg to set-call... JPB1011
      (setq timeout-process
            (set-call-for-bids-timeout 
             agent current-task-frame subtask-frame 
             (default-call-for-bids-timeout-delay *omas*)
             (default-call-for-bids-repeat-count *omas*)))
      ;; record timeout process in the subtask frame
      (setf (cfb-timeout-process subtask-frame) timeout-process)
      ;; strategy should not be :take-first-answer. Set it to
      ;; :collect-answers for a broadcast and :take-first-n-answers for a multicast
      ;; if strategy is already a list, e.g. (:take-first-n-answers) do nothing
      (unless (listp (strategy message))
        (if (listp to)
            ;; on multicast do not wait if we received all answers
            (setf (strategy message) (list :take-first-n-answers (length to)))
          ;; on broadcast, use timeout for collecting answers
          (setf (strategy message) :collect-answers)))
      )
    
    ;; when protocol is :collect-first-n-answers we must record nn
    (if (and (listp strategy)(eql (car strategy) :collect-first-n-answers))
        (setf (broadcast-max-answers subtask-frame) (cadr strategy)))
    
    ;; === send the message
    (send-message message)
    :done))

;(trace send-subtask)
;(untrace send-subtask)
;;;------------------------------------------------------------------- SENDING-AGENT

(defMethod sending-agent ((message message))
  "getting the key of the sender of the message.
Argument:
   message: should be a message object
Return:
   the key of the sender or nil if message is not a message."
  (from! message))

;;;---------------------------------------------------------------- SET-TRACE-SKILLS

(defun set-trace-skills ()
  (setf (trace-skills *omas*) t))

;;;---------------------------------------------------------------------------- SLOW

(defUn slow (delay)
  "put the process in a wait state during delay time. 
Arguments:
   delay: time to wait in second."
  (sleep delay)
  )

;;;--------------------------------------------------------------------- SKILL-TRACE

(defun skill-trace (agent message)
  "if (trace-skills *omas* is true prints a message when entering the skill"
  (dformat :skill-header 0
           "~2%;========== ~S: ~A ==========" (name agent) (action message)))


;;;--------------------------------------------------------------------------- SPEAK

(defUn speak (text)
  "when the voice interface is activated, voice the argument text. The text must ~
   not be too long. Only for the Windows version.
Arguments:
   text: a string
Return:
   nil"
  (net-voice-send text)
  nil)

#|
(net-voice-send :dummy "this is a test, dummy.")
(speak "this is a test, dummy.")
|#
;;;--------------------------------------------------------------------- SPLIT-VAL

(defUn split-val (text &optional (separator #\,))
  "Takes a string, consider it as a synonym string and extract items separated ~
   by a semi-column. Returns the list of string items.
Arguments:
   text: a string
Return
   a list of strings."
  (let (pos result word)
    (unless (and text (stringp text)) (return-from split-val text))
    (loop
      ;; remove trailing blanks
      (setq text (string-trim '(#\space) text))
      ;; is there any space left?
      (setq pos (position separator text))
      (unless pos
        (push text result)
        (return-from split-val (reverse result)))
      ;; extract first word
      (setq word (string-trim '(#\space) (subseq text 0 pos))
          text (subseq text (1+ pos)))
      (push word result)
      )))

#|
(split-val "Barthès: J.-P., Sugawara: Kenji, Fujita: S.")
("Barthès: J.-P." "Sugawara: Kenji" "Fujita: S.")
(omas::split-val " Barthès: J.-P. " #\space)
("Barthès:" "J.-P.")
|#
;;;--------------------------------------------------------------------- STATIC-EXIT

(defUn static-exit (agent arg &key reason)
  "passes the list of arguments to the answering function.
Arguments:
   arg: result to pass
   reason (key): usually a single argument
Return:
   if arg has more than one value returns two value car and cdr"
  (declare (ignore agent))
  ;; when a dynamic skill is not available, then kills process
  ;; must return arg in case it is an :abort tag
  (values arg reason))

#|
OMAS(8): (static-exit nil "albert")
"albert"
OMAS(9): (static-exit nil :failure :reason "I don't like it")
:FAILURE
"I don't like it"
|#
;;;------------------------------------------------------------ SUBSTITUTE-SUBSTRING

(defun substitute-substring (new old sequence)
  "replaces all occurences of old by new in sequence"
  (let (pos)
    (cond
     ((null (setq pos (search old sequence :test #'string-equal)))
      sequence)
     (t
      (concatenate 'string (subseq sequence 0 pos) new
        (substitute-substring new old (subseq sequence (+ (length old) pos))))))))

#|
(SUBSTITUTE-SUBSTRING "ZOE" "xxxxx" "albertine a vu xxxxx chez xxxxx")
"albertine a vu ZOE chez ZOE"
(SUBSTITUTE-SUBSTRING "ZOE" "xxxxx" "")
""
|#
;;;----------------------------------------------------------------- SYSTEM-MESSAGE?

(defUn system-message? (message)
  "test if message is a system message, i.e. type is :sys-XXX.
Arguments:
   message: to test
Return:
   message or nil."
  (cond ((not (typep message 'message)) nil)
        ((member (type! message) '(:sys-inform)) message)))

#|
(system-message? df-1)
(system-message? setup-cit)
|#
;;;--------------------------------------------------------------------- TIME-STRING

(defUn time-string (time &optional no-seconds)
  "get an integer representing universal time and extracts a string giving
   hour:minutes:seconds"
  (multiple-value-bind (second minute hour date month year time-zone)
      (decode-universal-time time)
    (declare (ignore date month year time-zone))
    (if no-seconds
        (format nil "~2D:~2,'0D" hour minute)
      (format nil "~2D:~2,'0D:~2,'0D" hour minute second))))
    ;(format nil "~s:~s:~s" hour minute second)))

#|
(time-string (get-universal-time))
"17:19:07"

(time-string (get-universal-time) :no-seconds)
"10:57"
|#
;;;--------------------------------------------------------------------- DATE-STRING

(defUn date-string (time)
  "get an integer representing universal time and extracts a string giving
   year:month:day"
  (multiple-value-bind (second minute hour date month year time-zone)
                       (decode-universal-time time)
    (declare (ignore hour minute second time-zone))
    (format nil "~2,'0D/~2,'0D/~2,'0D" (mod year 100) month date)))

#|
(date-string (get-universal-time))
"08/11/26"
|#
;;;---------------------------------------------------------------- DATE-TIME-STRING

(defUn date-time-string (time)
  "get an integer representing universal time and extracts a string giving
   year/month/day hour:minute:second"
  (multiple-value-bind (second minute hour date month year time-zone)
                       (decode-universal-time time)
    (declare (ignore time-zone))
    (format nil "~2,'0D/~2,'0D/~2,'0D ~s:~s:~s" 
            (mod year 100) month date hour minute second)))

#|
(date-time-string (get-universal-time))
"08/11/26 20:46:58"
|#
;;;--------------------------------------------------------------------- AGENT-TRACE

;;; agent-trace should print the number of current processes executed by the agent
;;; not including the basic ones
;;; agent-trace, trace and untrace should work with agent keys not agent objects

;;; ********** One could assign a particular color to each agent...

(defUn agent-trace (agent-ref text &rest args)
  "function used to trace agent's behavior.
Arguments: 
   agent-ref: agent object, agent name or agent-key
   text: text for string format
   args: arguments for the format variables."
  ;; first check whether first arg is an agent or the name of an agent
  (cond
   ;; if keyword, then OK
   ((keywordp agent-ref))
   ;; if not keyword, agent object?
   ((typep agent-ref 'agent)
    ;; set ref to keyword
    (setq agent-ref (key agent-ref)))
   ;; symbol that could be agent name
   ((and (symbolp agent-ref)
         (boundp agent-ref)
         (typep (symbol-value agent-ref) 'agent))
    ;; set ref to keyword
    (setq agent-ref (key (symbol-value agent-ref))))
   ;; otherwise error
   (t
    (warn "agent-ref ~S is not a reference to a known agent. Ignoring trace." 
          agent-ref))
   )
  (if (get agent-ref :trace)
    (text-trace 
     (concatenate 'string "~&" 
                  (apply #'format nil (concatenate 'string "~A... ~A " text)
                         (time-string (get-universal-time)) agent-ref
                         args)))))

;;;--------------------------------------------------------------------- TRACE-AGENT

(defUn trace-agent (agent)
  "set the agent traced property.
Arguments:
   agent: agent to trace."
  (setf (traced agent) t))

;;;------------------------------------------------------------------- UNTRACE-AGENT

(defUn untrace-agent (agent)
  "reset the agent traced property.
Arguments:
   agent: agent to untrace."
  (setf (traced agent) nil))

;;;-------------------------------------------------------------- UPDATE-ENVIRONMENT
;;; deprecated, use env-set

(defUn update-environment (agent env)
  "replace the agent environment with env.
   Must be called from a task process executing the right task.
Arguments:
   agent: agent
   env: environment part of the agent."
  (let ((current-task-frame (agent-process-get-task-frame agent *current-process*)))
    (if current-task-frame
      (setf (environment current-task-frame) env)
      (error "a task should be executing when updating environment for ~A" agent))))

;;;---------------------------------------------------------------------------------

(format t "~&;*** OMAS v~A - API loaded ***" *omas-version-number*)

;;; :EOF