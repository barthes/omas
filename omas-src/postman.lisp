﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;               O M A S - P O S T M A N (file omas-postman.lisp)
;;;
;;;===============================================================================
;;; This file contains all functions related to the functioning of an agent as a
;;; postman or transfer agent (XA), i.e. an agent acting as a gateway between two 
;;; platforms. The term postman was proposed by Fabricio Enembreck in 2001.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
The postman transfers messages to a remote platform, i.e. a platform that is not
on the same LAN loop.
It functions sligthly differently from a standard service agent, forwarding all 
messages for which it is not specifically the receiver and ignoring system messages.

The postman when created initializes a transfer protocol, e.g.UDP,  TCP, HTTP, or
SOAP, and creates a new process to receive answers.
The postman has predefined skills:
   :connect setting up the communication channels (defaults to TCP)
   :disconnect halting all transfers
   :reset cleans the site
   :status returns the status of the postman
Currently only TCP and HTP are implemented.

Its status may be
   :inactive structure created but socket not created
   :active working normally
   
Connections
===========
There are 3 ways of connecting a postman to other postmen:
   - as a server-to-server TCP connection
   - as a server-to-server HTTP connection
   - as a client to a TCP server
   
A connection on a given postman has a description:
  (<remote-postman-key> <remote-postman-name> <remote-postman-IP> 
   <protocol> <site>)
For example:
  (:JPB "JEAN-PAULBAC4F6" nil :TCP :UTC)
meaning that we can connect to postman :JPB (a notebook) at machine named 
JEAN-PAULBAC4F6 in TCP mode on the site :UTC. 
  (:TECPAR nil "200.183.132.15" :TCP :TECPAR)
here the target postman and the site have the same name.

Connections (postman descriptions) are used to set up remote connections at a given
site.

Standard TCP protocol
=====================
This is a remote connection using standard TCP/IP protocol and port 52008. It works
well but one must take care of specifying external addresses when trying to reach
a machine behind a firewall. Each side (each postman) is a server and has a 
receiving loop. Sockets are closed after each message.

HTTP protocol
=============
When HTTP is requested, then the postman uses Allegroserve to set up an HTTP 
server, listening on the /omascc channel. Each side is a server using Allegroserve.
Sending is done through the server and is to other remote HTTP sites
For sending messages to a given site, the site must be first connected.

CLIENT protocol
===============
This is an asymmetrical connection used for sites behind a firewall. Let A be the 
site behind the firewall and B the server (a known reachable postman). Then A
must first connect to be on the 52008 port. Then A and B can exchange messages.
A and be are constantly waiting for messages on their opened socket. One must be
careful when closing the sockets in the right order to avoid socket errors. This is 
done by the :disconnect skill.

Example
=======
(defpackage :test (:use :moss :omas :cl))
(in-package :test)
(defpostman :test 
    :server t
  :external-name "nat-omas.utc.fr" :internal-name "smikonos"
  :known-postmen ((:UTC nil "193.23.154.22" :TCP :UTC) ; external address
                  (:TECPAR nil "200.183.132.15" :HTTP :TECPAR)))

IMPORTANT
=========
When a postmen uses HTTP, all postmen should use HTTP. HTTP and direct connection 
through port 52008 are not really compatible, due to the antilooping features.

A postman can also be used as a proxy for controlling a device that will appear as 
a specific agent. In that case it uses the :raw option and all skills have to be
redefined in the postman file.

;;;----- globals
*print-codes* nil

;;;----- macros
defpostman (name &rest args)
def-xa-set (prop sub-object-name)
def-xa-set-no-check (prop sub-object-name)
extend-method-for-postman (method subobject)

;;;----- classes
postman (agent)
transfer ()

;;;----- functions
check-postman-description-syntax (info)
client? (message-string)
connect-static-check-target (agent info)
connect-static-client (agent info)
connect-static-get-hostname (agent)
connect-static-http (agent info)
connect-static-tcp (agent info)
disconnect-static (agent message info &optional cleanup)
get-remote-host-ip (info)
is-site-active? (agent target-key)
make-postman (name &key hide http (http-port 80) (tcp-port 52008) redefine
                          (server t) known-postmen receiving-fcn proxy raw site 
                          (connection-type :tcp)
                          internal-name internal-IP external-name external-IP
                          (package *package*) 
                          (language (default-language *omas*)) 
                          (context (or moss::*context* 0)) 
                          (version-graph (or moss::*version-graph* '((0)))))
make-postman-description (key &key name IP protocol site port)
open-active-socket (agent remote-ip info)
postman-add-default-skills (agent)
postman-IP (postman-description)
postman-key (postman-description)
postman-name (postman-description)
postman-protocol (postman-description)
postman-receiving (agent port)
postman-receiving-as-client (agent client-socket info)
postman-receiving-check-identity (agent message protocol)
postman-receiving-from-client (agent socket-id info)
postman-receiving-http (agent port message-string)
postman-receiving-process-message (agent message-string protocol)
postman-receiving-propagate (agent message)
postman-send (agent message)
postman-send-choosing-protocol (agent message target)
postman-site (postman-description)
postman-port (postman-description)
postman-set-ip (postman-description value)
postman-set-name (postman-description value)
postman-set-port (postman-description value)
postman-set-protocol (postman-description value)
postman-set-site (postman-description value)
read-fat-message (stream)
reset-static (agent message &aux msg)
send-fat-message (message-string stream)
send-remote-client (agent message agent-key)
send-remote-http (agent message site-key)
send-remote-tcp (agent message site-key)
send-static (agent in-message message-string message)
setnth (ll nn value)
shut-down-static (agent message)
start-http-server (agent port)
start-server (agent port)
start-server-if-needed (agent port)
start-static (agent message)

History
-------
2020
0202 restructuring OMAS as an asdf system
2023
 0608 removing FAT coding for transmitions, now assuming UTF8
|# 

(in-package :omas)

;;;================================== Globals ====================================

(defparameter *print-codes* nil
                "if t will print the codes being transferred and received")
                         
;;;============================= macros ==========================================
;;; macro for defining set methods

(defMacro def-xa-set (prop sub-object-name)
  `(defMethod (setf ,prop) (val (self postman) )
     (let ((item (,sub-object-name self)))
       (if item
           (setf (,prop item) val)
         (error ,(concatenate 'string "non existing "
                   (symbol-name sub-object-name)
                   " part in postman ~S") (name self)))
       val)))

;;; same with no check

(defMacro def-xa-set-no-check (prop sub-object-name)
  `(defMethod (setf ,prop) (val (self postman) )
     (setf (,prop (,sub-object-name self)) val)
     ))

#|
;;; Example of use: (def-xa-set input-messages comm "COMM")
;;; should produce

(defmethod (setf output-messages) (val (self postman) )
  (let ((item (comm self)))
    (if item
        (setf (output-messages item) val)
      (error "non existing COMM part in postman ~S" (name self))))
  val)
|#

;;; macro to extend the method applying to a subpart of an agent, so that it can
;;; be applied to the agent as a whole. Includes the definition for defsetf

(defMacro extend-method-for-postman (method subobject)
  `(progn
     (defMethod ,method ((self postman))
       (,method (,subobject self)))
     (def-xa-set-no-check ,method ,subobject)))

;;;=================================== Class =====================================
;;;   - connected-postmen-info: list of info for connected postmen, e.g. 
;;;    ((:CIT <CIT-name> <CIT-IP> <protocol> <CIT site>)...) empty at first
;;;   - ids-of-received-messages: list for keeping track of received messages to
;;;     discard messages coming in a second time through a different path
;;;   - external IP: address where the agent can be reached if behind firewall
;;;   - external name: name of the agent seen from outside the firewall
;;;   - internal IP: address to reach the agent inside the firewall
;;;   - internal name: name of the machine as seen from inside the firewall
;;;   - irm-max-size: max size of the queue of ids of received messages (default 100)
;;;   - known-postmen-info: list of site entries (pairs), e.g. 
;;;     ((:utc nil "195.83.154.22" :external)...), empty at creation
;;;   - local-site: local site is a key identifying the site: e.g. :UTC (unused)
;;;   - postman-stream: ??
;;;   - receiving-process: persistent process for receiving messages from ooutside
;;;   - receiving-fcn: receiving function, handle for user-provided functions
;;;   - receive-socket: receiving socket
;;;   - send-socket: socket for sending messages (volatile)
;;;   - site: name of the local site, e.g. :UTC, :TECPAR (same as the name of the
;;;     postman?)
;;;   - site-counter: counter for foreign sites ??
;;;   - site-port: default port for remote communications is 52008
;;;   - status: :active (:http)
;;;   - transfer-protocol: :tcp (default) or :http

(defClass postman (agent)
  ((transfer :accessor transfer :type transfer :initarg :transfer)
   (postman :accessor postman :initform nil)
   (postman-process :accessor postman-process :initarg :postman-process 
                    :initform nil))
  (:documentation "agent for making inter-platform transfers"))

(defClass transfer ()
  (;; list of info for connected postmen, e.g. ((:CIT <CIT name> <CIT-IP> :external)...)
   (connected-postmen-info :accessor connected-postmen-info :initform nil)
   ;; type of connection
   (connection-type :accessor connection-type :initform nil)
   ;; external-IP
   (external-IP :accessor external-IP :initform nil :initarg :external-IP)
   ;; external-name
   (external-name :accessor external-name :initform nil :initarg :external-name)
   ;; http true if http protocol is requested
   (http :accessor http :initform nil)
   ;; http-port for receiving (should be 80)
   (http-port :accessor http-port :initform nil)
   ;; list for keeping track of received messages to discard messages coming
   ;; in a second time through a different path
   (ids-of-received-messages :accessor ids-of-received-messages :initform nil)
   ;; internal-IP, move to agent structure JPB 1301
   ;(internal-IP :accessor internal-IP :initform nil :initarg :internal-ip)
   ;; internal name
   (internal-name :accessor internal-name :initform nil :initarg :internal-name)
   ;; max size of the queue of ids of received messages (default 100)
   (irm-max-size :accessor irm-max-size :initform 100)
   ;; list of site entries (triples), e.g. ((:utc "195.83.154.22" "extern")...)
   (known-postmen-info :accessor known-postmen-info :initform nil)
   ;; used for TCP/IP connections ?
   ;(postman-stream :accessor postman-stream :initform t) ;default: listener
   ;; proxy is needed when sending behing firewall using HTTP
   (proxy :accessor proxy :initform nil)
   ;; persistent process for receiving messages from ooutside
   (receiving-process :accessor receiving-process :initform nil)
   ;; receiving socket 
   (receive-socket :accessor receive-socket :initform nil)
   ;; receiving function, handle for user-provided functions
   (receiving-fcn :accessor receiving-fcn :initform nil)
   ;; socket for sending messages (volatile)
   (send-socket :accessor send-socket :initform nil)
   ;; local site is a key identifying the site: e.g. :UTC 
   ;; when two postmen are from the same site, no proxies are used for HTTP
   (site :accessor site :initform nil :initarg :site)
   ;; counter for foreign sites ??
   (site-counter :accessor site-counter :initform 0)
   ;; user is control-panel
   (site-user :accessor site-user :initform (local-user *omas*))
   ;; if :active means that the server is connected
   (status :accessor status :initform :inactive :initarg :status)
   ;; tcp-port for receiving (should be 52008)
   (tcp-port :accessor tcp-port :initform nil)
   )
  (:documentation "object containing the transfer parameters"))

;;; Extending methods from parts of an agent to apply to the agent as a whole.

;--- TRANSFER
(extend-method-for-postman connected-postmen-info transfer)
(extend-method-for-postman connection-type transfer)
(extend-method-for-postman external-IP transfer)
(extend-method-for-postman external-name transfer)
(extend-method-for-postman http transfer)
(extend-method-for-postman http-port transfer)
(extend-method-for-postman ids-of-received-messages transfer)
;(extend-method-for-postman internal-ip transfer)
(extend-method-for-postman internal-name transfer)
(extend-method-for-postman irm-max-size transfer)
(extend-method-for-postman known-postmen-info transfer)
(extend-method-for-postman proxy transfer)
(extend-method-for-postman receive-socket transfer)
(extend-method-for-postman receiving-fcn transfer)
(extend-method-for-postman receiving-process transfer)
(extend-method-for-postman send-socket transfer)
(extend-method-for-postman site transfer)
(extend-method-for-postman site-http-port transfer)
(extend-method-for-postman site-tcp-port transfer)
(extend-method-for-postman site-user transfer)
(extend-method-for-postman site-counter transfer)
(extend-method-for-postman status transfer)
(extend-method-for-postman tcp-port transfer)

;;;===============================================================================
;;;                            SERVICE FUNCTIONS
;;;===============================================================================
;;; A postman description could be a structure describing a postman connection
;;;  postdesc
;;;     key
;;;     name
;;;     ip
;;;     protocol
;;;     site
;;;     {port}
;;; for transfers it would need to be transformed into a list
;;;  (<postman key> <remote-machine name> <remote-machine-IP> 
;;;                 <transfer-protocol> <remote-machine-site>)
;;; examples:
;;;  (:jpb "JEAN-PAULBAC4F6" nil :TCP :UTC) ; :jpb is the postman key on a UTC loop
;;;  (:tecpar nil "200.183.132.15" :TCP :TECPAR)
;;; If the remote postman site is the same as the receiving postman, then no proxy
;;; is needed for the HTTP protocol.

(defun check-postman-description-syntax (info)
  "checks the syntax of a postman description"
  (unless (listp info)
    (error "postman description should be a list rather than ~S" info))
  (when (< (length info) 5)
    (error "some parameter(s) missing in postman description ~S" info))
  (when (> (length info) 6)
    (error "too many parameters in postman description ~S" info))
  (unless (keywordp (nth 0 info))
    (error "postman description key should be a keyword rather than ~S in ~S"
      (nth 0 info) info))
  (when (and (nth 1 info) (not (stringp (nth 1 info))))
    (error "postman description name should be a string rather than ~S in ~S"
      (nth 1 info) info))
  (when (and (nth 2 info) (not (socket:dotted-address-p (nth 2 info))))
    (error "postman description ip missing or should be a dotted address rather ~
            than ~S in ~S"
      (nth 2 info) info))
  (unless (or (nth 1 info)(nth 2 info))
    (error "postman description name or IP protocol should not be null in ~S"
      info))
  ;; we add :UDP for raw postmen
  (unless (member (nth 3 info) '(:tcp :http :client :udp))
    (error "postman description protocol should be a :HTTP or :TCP rather than ~S in ~S"
      (nth 3 info) info))
  (unless (keywordp (nth 4 info))
    (error "postman description site should be a keyword rather than ~S in ~S"
      (nth 4 info) info))
  (if (and (> (length info) 5) (not (numberp (nth 5 info))))
      (error "postman description port should be a number rather than ~S in ~S"
        (nth 5 info) info))
  info)

(defun make-postman-description (key &key name IP protocol site port)
  (list key name IP (or protocol :TCP) site 
        (or port (if (eql protocol :http) 80 52008))))

(defun postman-key (postman-description)
  "extracts the agent key from a postman description"
  (nth 0 postman-description))

(defun postman-name (postman-description)
  "extracts the machine name from a postman description"
  (nth 1 postman-description))

(defun postman-IP (postman-description)
  "extracts the machine IP from a postman description"
  (nth 2 postman-description))

(defun postman-protocol (postman-description)
  "extracts the exchange protocol from a postman description"
  (nth 3 postman-description))

(defun postman-site (postman-description)
  "extracts the agent site from a postman description"
  (if (>= (length postman-description) 5)
      (nth 4 postman-description)))   

(defun postman-port (postman-description)
  "extracts the agent (target) port from a postman description"
  (cond
   ((>= (length postman-description) 6)
    (nth 5 postman-description))
   ((eql (postman-protocol postman-description) :http)
    80)
   (t 52008)))

(defun setnth (ll nn value)
  "replaces the nth position in the list by value"
  (cond 
   ((<= nn 0) (cons value (cdr ll)))
   (t (cons (car ll) (setnth (cdr ll) (1- nn) value)))))

(defun postman-set-name (postman-description value)
  "set the machine name for a postman description"
  (setnth postman-description 1 value))

(defun postman-set-ip (postman-description value)
  "set the machine ip for a postman description"
  (setnth postman-description 2 value))

(defun postman-set-protocol (postman-description value)
  "set the protocol for a postman description"
  (setnth postman-description 3 value))

(defun postman-set-site (postman-description value)
  "set the site for a postman description"
  (setnth postman-description 4 value))

(defun postman-set-port (postman-description value)
  "set the port for a postman description"
  (setnth postman-description 5 value))

#|
(make-postman-description :jpb :name "JEAN-PAULBAC4F6" :site :UTC)
(:JPB "JEAN-PAULBAC4F6" NIL :TCP :UTC 52008)

(make-postman-description :tecpar :ip "200.183.132.15" :site :tecpar)
(:TECPAR NIL "200.183.132.15" :TCP :TECPAR 52008)

(make-postman-description :jpb :name "JEAN-PAULBAC4F6" :protocol :http :site :UTC)
(:JPB "JEAN-PAULBAC4F6" NIL :HTTP :UTC 80)

(postman-port '(:jpb "JEAN-PAULBAC4F6" nil :TCP :UTC))
52008

(postman-port '(:jpb "JEAN-PAULBAC4F6" nil :TCP :UTC 4000))
4000

(postman-port '(:jpb "JEAN-PAULBAC4F6" nil :HTTP :UTC))
80
|#
;;;----------------------------------------------------------------------- CLIENT?

(defun client? (message-string)
  "checks if the message is from a client wanting to set a new connection.
Argument:
   message-string: the text to examine
Return:
   nil or a postman description of the client."
  (let (input)
    (unless (stringp message-string)
      (return-from client?))
    ;; look for a marker in the beginning of the message
    (when (search ":cnct" message-string :end2 8 :test #'string-equal)
      ;; extract the list from the message
      (setq input (ignore-errors (read-from-string message-string)))
      (when (or (null input)(not (listp input)))
        (warn "bad syntax in connecting message: ~S" message-string)
        (return-from client?))
      ;; extract info from message
      (setq info (cdr input))
      ;; check that we indeed gat an info format
      (unless (check-postman-description-syntax info)
        (error "bad connection message: ~S" message-string))
      (format t "~%; client / returned info: ~S" (cdr input))
      (cdr input))))

#|
CG-USER(20): (client? "(:cnct :JP nil nil :client :HOME 52008)")
(:JP NIL NIL :CLIENT :HOME 52008)
CG-USER(30): (client? "(:cnct :JP nil nil :client :HOME 52008")
Warning: bad syntax in connecting message: "(:cnct :JP nil nil :client :HOME 52008"
NIL
CG-USER(37): (client? ":eof")
NIL
|#	
;;;------------------------------------------------------------ GET-REMOTE-HOST-IP

(defun get-remote-host-ip (info)
  "takes a postman description and checks if the remote postman is reachable. If ~
   the IP is included in the description, uses the IP, otherwise ask the DNS using ~
   the remote-host name.
Argument:
   info: postman description of the remote postman to connect to
Return:
   IP or nil"
  (let (remote-ip test errno)
    (cond
     ;; try first to get it from the plist of the remote postman
     ((get (postman-key info) :ip))
     ;; try to get it from the postman description
     ((setq remote-ip (postman-ip info))
      ;; cache it onto the plist of the postman key
      (setf (get (postman-key info) :ip) remote-ip)
      remote-ip)
     ;; otherwise, try to open a socket onto the remote site, catching socket errors
     ;; first get IP of remote site, info may not contain IP but name
     (t
      (multiple-value-setq (test errno)
        (ignore-errors
         (setq remote-ip
               (socket:ipaddr-to-dotted
                (socket:lookup-hostname ; works on names and IPs
                 (postman-name info))))))
      ;; check if error, and if so return nil
      (when (and (null test) errno)
        (return-from get-remote-host-ip nil))
      ;; OK, cache it onto the plist of the postman key
      (setf (get (postman-key info) :ip) remote-ip)
      remote-ip)
     )))
	
;;;--------------------------------------------------------------- IS-SITE-ACTIVE?
;;; There might be a problem here. We assume that the name of the site is the same
;;; as the name of the postman. That may not be true
;;; a better name would be is-connection-active?

(defUn is-site-active? (agent target-key)
  "checks if a site is active, i.e. its postman is recorded onto the connected-~
   postmen-info list. 
Argument:
   agent: postman
   target-key: e.g. key of the remote postman (usually same as site key)
Return:
   the info related to the target site postman if active, nil otherwise."
  ;; a postman cannot appear more than once in the connected-postmen-info list
  ;; i.e., a postman cannot be connected by two different protocols.
  (assoc target-key (connected-postmen-info agent)))
  
;;;---------------------------------------------------------- MAKE-CONNECTION-INFO
;;; changing the display list of connections in omas-W-agent.lisp
;;; redefined in omas-W-postman.lisp jpb 1411

;;;(defUn make-connection-info (agent win)
;;;  "make a list of strings <site> <ON/OFF> to be posted to the postman window."
;;;  (let ((agent-key (key agent))
;;;        known-postmen connected-postmen results postmen-to-display)
;;;    
;;;    (setq known-postmen (mapcar #'car (KNOWN-postmen-INFO agent)))
;;;    (setq connected-postmen (mapcar #'car (CONNECTED-postmen-INFO agent)))
;;;    
;;;    ;; collect all sites to display: known sites and connected sites we did not know
;;;    (setq postmen-to-display 
;;;          (remove-duplicates (append known-postmen connected-postmen)))
;;;    ;; remove local site and save result
;;;    (setq postmen-to-display (remove agent-key postmen-to-display))
;;;    (setf (known-postmen win) postmen-to-display)
;;;    
;;;    ;; make list of site info
;;;    (dolist (postman postmen-to-display)
;;;      (push
;;;       (format nil "~&~S/~A ~A" 
;;;         postman 
;;;         (postman-protocol (assoc postman (known-postmen-info agent)))
;;;         (if (member postman connected-postmen) 
;;;             (get-text *WAG-on*) 
;;;           (get-text *WAG-off*)))
;;;       results))
;;;    ;(format t "~&+++ make-connection-info /results: ~S" results)
;;;    
;;;    ;; set sequence
;;;    (%set-item-sequence (%find-named-item :connections win) (reverse results))
;;;    ;; return list of connections
;;;    :done))

#|
(make-connection-info xa-utc::xa-utc)
(make-postman-window xa-utc::xa-utc 2)
|#
;;;------------------------------------------------------------ OPEN-ACTIVE-SOCKET

(defun open-active-socket (agent remote-ip info)
  "try to open an active socket onto a remote postman using its IP. Display the
    result in the postman window (green or red).
Arguments:
   agent: current postman
   remote-ip: ip of the target postman
   info: postman description concerning the remote postman
Return:
   socket-id or nil."
  (let (socket-id test errno)
    ;; try to open an active socket
    (multiple-value-setq (test errno)
      (ignore-errors
       ;; create a socket to connect with the remote host, TCP port or HTTP port
       (setq socket-id
             (socket:make-socket :remote-host remote-IP
                                 :remote-port (postman-port info)))))
    ;; test for error (usually timeout)
    (when (and (null test) errno)
      ;; could not create socket: nobody is listening on specified port
      ;; paint the connecting pane red for 2/10s to indicate failure
      (pw-show-failure agent)
      ;; refresh postman window, which will show connections
      (agent-display agent)
      (return-from open-active-socket nil))
    
    ;; show success
    (pw-show-success agent)
    (agent-display agent)
    ;; return socket-id
    socket-id))

;;;-------------------------------------------------------------- READ-FAT-MESSAGE
#|
(defun read-fat-message (stream)
  "receives a set of bytes representing 16 bit UNICODE codes. The first 2 bytes are ~
   the length of the string. The function returns a Lisp string.
Arguments:
   stream: a byte stream (e.g. a TCP socket)
Return:
   the received Lisp string."
  (let (nn buffer ll code)
    ;; message length is coded on the 2 first bytes
    (setq high (read-byte stream nil :eof))
    (when (eql high :eof)
      (warn "bad input: EOF on first message byte, we quit.")
      (return-from read-fat-message nil))    
    ;; first byte is length of string to be read
    (setq low (read-byte stream nil :eof))
    ;; one of the problems is that when we close the connected socket on the other
    ;; side, the other machine sends something which is not formatted as a fat
    ;; message. Thus temporarily disable the error message
    (when (eql low :eof)
      (warn "bad input: EOF on second message byte, we quit.")
      (return-from read-fat-message nil))
    
    ;; OK, compute message length
    (setq nn (+ (* 256 high) low))
    (dotimes (jj nn)
      (setq code (read-byte stream nil :eof))
      (if (eql code :eof)(error "bad input: EOF at ~S position, received: ~S" jj ll))
      (push code ll))
    ;; make a vector of codes (buffer)
    (setq buffer (coerce (reverse ll) '(array (unsigned-byte 8))))
    ;; return string
    (excl:octets-to-string buffer :external-format :fat)))
|#
;;;-------------------------------------------------------------- SEND-FAT-MESSAGE
#|
(defun send-fat-message (message-string stream)
  "takes a string and sends it as a UNICODE 16 bit (fat) format. The string must ~
   be less than 32700 characters long.
Arguments:
   message-string: a string of unicode characters
   stream: a byte stream (e.g. a TCP socket)
Return:
   t"
  (let (nn buffer ll high low)
    ;; make a list of integer codes
    (setq buffer (excl:string-to-octets message-string :external-format :fat))
    ;(format t "~%; send-fat-message /length: ~S, buffer:~%;   ~S"
    ;  (length buffer) buffer)
    (setq ll (coerce buffer 'list)
        nn (length ll))
    ;(format t "~%; send-fat-message /length: ~S, ll:~%;   ~S" nn ll)
    ;; error if string too long
    (if (> nn 65400) (error "string too long: length: ~S" nn))
    ;; the length of the string takes 2 bytes
    (multiple-value-setq (high low) (floor nn 256))
    ;; send length of list
    (write-byte high stream)
    (write-byte low stream)
    ;; now send message
    (dolist (code ll)
      (write-byte code stream))
    ;; send buffer
    (force-output stream)
    t))
|#
;;;=============================== Creating functions ============================

;;;------------------------------------------------------------------ MAKE-POSTMAN
;;; defining a regular postman is done as follows:
;;;    (defpostman :UTC-HTTP 
;;;                :site :UTC
;;;                :Server T
;;;                :internal-name "MIKONOS"
;;;                :internal-IP "172.17.130.153" ; fixed IP
;;;                :external-name "nat-omas.utc.fr"
;;;                :external-IP "195.83.154.22" 
;;;                :Known-Postmen 
;;;                   ((:Cit nil "219.166.183.59" :tcp :cit)
;;;                    (:Tecpar nil "200.183.132.15" :tcp :tecpar)
;;;                    (:jpb "jean-paulbaC4F6" nil :tcp :utc))
;;;                )
;;; adding an HTTP server is done by adding the following line
;;;                :http t
;;;                :proxy "proxyweb.utc.fr:3128"
;;; The main difference is with the proxy. By default HTTP messages will be sent to
;;; port 80 and the receiving addresses will be http://".../acc" where acc stands
;;; for agent communication channel, e.g. http://nat-omas.utc.fr:80/acc
;;; In both cases messages will be forwarded to remote sites until all the 
;;; connected nodes are covered, without redundancy.
;;;
;;; default port are 80 for HTTP and 52008 for TCP

;;; the postman window can be used and a CONNECT click will send a :connect message
;;; to the postman. A disconnect will send a :disconnect message. Connecting to
;;; a remote site consists in trying to open a TCP/IP socket, which will succeed
;;; only if there is a receiving process at the other end.

;;; a :raw option means that the postman is created without any skills and without
;;; any connections, i.e. does not try to fetch internal machine IP.



(defUn make-postman (name &key hide http (http-port 80) (tcp-port 52008) redefine
                          (server t) known-postmen receiving-fcn proxy raw site 
                          (connection-type :tcp)
                          internal-name internal-IP external-name external-IP
                          (package *package*) 
                          (language (default-language *omas*)) 
                          (context (or moss::*context* 0)) 
                          (version-graph (or moss::*version-graph* '((0)))))
  "creates an agent, puts it onto the *agents* list, name must be an unused symbol.
Arguments:
   name: name given to the agent (e.g. UTC or :UTC)

   connection-type (key): :HTTP, :TCP, :CLIENT (default :TCP)
   external-name (key): external name of the local computer, e.g. \"nat-omas.utc.fr\"
   external-IP (key): external IP of the local computer if known and fixed
   http (key): if t, adds an HTTP server port 80 channel /omasacc
   http-port (key): sending port (default 80)
   internal-name (key): internal name of the local computer, e.g. \"MIKONOS\"
   internal-IP (key): internal IP of the local computer (can be computed if needed)
   known-postmen (key): list of known postmen descriptions
   proxy (key): proxy when sending behind firewall using HTTP protocol
   raw (key): do not use default skills. To be provided by the user
   receiving-fcn (key): optional user defined receiving function
   server (key): declare the postman to be a server, creates a receiving process
                 default is t
   site (key): local site (e.g. behind a firewall needing a proxy), e.g. :UTC
   tcp-port (key): default 52008

   context (key): context (version) of the ontology
   language (key): postman language (default: default language of omas)
   package (key): default current (package object) or keyword
   version-graph (key): version (configuration) graph of the ontology
Return:
   the agent internal CLOS object reference."
  (declare (ignore redefine))
  (format t "~%;--- creating a new postman: ~S" name)
  ;(omas-trace :xp 0)
  (let (key)
    ;; check if name is a symbol or a keyword, if not complain
    (unless (or (keywordp name) (symbolp name))
      (error "the name ~A for the agent must be a symbol or a keyword." name))
    
    ;; site is an essential parameter and must be specified unless :raw option
    (unless (or raw site)
      (error "the site parameter must be specified for agent ~S" name))
    
    ;(format t "~%;=== make-postman: *package*: ~S" *package*)
    
    ;; check syntax of known postmen before creating the agent
    (mapcar #'check-postman-description-syntax known-postmen)
    
    ;; make sure we have a key
    (setq key (keywordize name))
    ;; create a postman name
    (setq name (create-postman-name name package))
    (if (boundp name)
        (error "name ~A for the agent already bound to ~A" name (symbol-value name)))
    
    ;; here name is not bound we must declare it as a global variable
    (eval `(defVar ,name))
    
    ;; then we create a whole new agent
    (let ((comm (make-instance 'comm))
          (control (make-instance 'control))  ; default status is :idle
          (self (make-instance 'self))
          (world (make-instance 'world))
          (ontologies (make-instance 'ontologies))
          (appearance (make-instance 'appearance))
          (tasks (make-instance 'tasks))
          (transfer (make-instance 'transfer))
          agent process)
      ;; create agent structure
      (setq agent (make-instance 'POSTMAN 
                    :name name
                    :key key
                    :comm comm
                    :control control
                    :self self
                    :ontologies ontologies
                    :world world
                    :tasks tasks
                    :appearance appearance
                    :transfer transfer
                    ))
      ;; make agent name point to the lisp object
      (set name agent)
      
      
      ;; store a handle for load-agent (will stay there if agent loaded manually)
      (setf (agent-being-loaded omas::*omas*) agent) ; JPB 0912
      
      ;; save global info into agent structure         
      (setf (ontology-package agent) package)
      (setf (language agent) language)
      (setf (moss-context agent) context)
      (setf (moss-version-graph agent) version-graph)
      (setf (receiving-fcn agent) receiving-fcn)
      (setf (known-postmen-info agent) known-postmen)
      (setf (proxy agent) proxy)
      (setf (tcp-port agent) tcp-port)
      (setf (http-port agent) http-port)
      (setf (http agent) http)
      ;; for backward compatibility keep http
      (setf (connection-type agent) (or connection-type (if http :http) :tcp))
      (setf (site agent) site)
      
      ;;=== take care of the problems of internal and external IP
      ;; record any passed parameter including nil
      (setf (internal-ip agent) internal-ip)
      (setf (external-ip agent) external-ip)
      (setf (internal-name agent) internal-name)
      (setf (external-name agent) external-name)
      ;; now check if essential parameters are not initialized, unless :raw option
      (unless raw
        ;; internal name might be nil
        (setf (internal-name agent) internal-name) ; redundant
        ;; if the internal IP is not given, try to compute it
        (unless internal-ip
          (setf (internal-ip agent)
            (socket:ipaddr-to-dotted
             (socket:lookup-hostname (excl.osi:gethostname)))))
        ;; if there is no external name nor external IP, we set external IP to
        ;; be the internal one, meaning that there is no firewall for the machine
        (unless (or external-name external-ip)
          ;; should maybe issue a warning...
          (setf (external-ip agent) (internal-ip agent)))
        )
      
      ;(break "make-postman /*package*: ~S" *package*)
      
      ;; create MOSS classes and method proxy if not already there - JPB 0912
      ;; create a MOSS instance representing the agent
      ;; second argument is nil since postmen so far are not persistent
      ;(break "before agent-set-moss-environment")
      
      (agent-set-moss-environment agent nil)
      
      ;(break "after agent-set-moss-environment")

      ;; add the name to the *local-agents* list if not there
      ;; if agent name is in the list we remove it before adding it again
      ;; because the asociated structure is probably obsolete
      (if (assoc key (local-agents *omas*))
          (setf (local-agents *omas*) 
            (remove key (local-agents *omas*) :key #'car)))
      
      (setf (local-agents *omas*) 
        (append (local-agents *omas*) (list (cons key agent))))
      
      (if hide (setf (get key :hide) t))
      ;; add name to names of known agents
      (pushnew name (names-of-known-agents *omas*))

      ;; launch agent process working on the agenda
      (setq process (agent-process-create-restartable 
                     agent
                     '(:type :main) ; no task being executed, process is main 
                     (agent-make-process-name agent "main" :fixed-name t)
                     #'agent-process-task 
                     agent))
      (dformat :xp 0 "~%;--- process: ~S" process)
      (setf (process agent) process)
      ;; launch then scan process on input-messages queue
      (setq process (agent-process-create-restartable 
                     agent
                     '(:type :scan) ; no task being executed, process is scan
                     (agent-make-process-name agent "scan" :fixed-name t)
                     #'agent-scan-messages 
                     agent))
      (dformat :xp 0 "~%;--- mbox process: ~S" process)
      (setf (mbox-process agent) process)
      
      ;; mark the agent as postman
      (setf (postman agent) t)
      
      ;; add necessary skills, unless raw option is specified
      ;; :connect, :disconnect, :reset, :status
      ;; when raw is true, skills must be defined in the agent file
      (unless raw
        (postman-add-default-skills agent)
        
        ;; create receiving process, hence server are started
        (when server
          ;; unless otherwise stated, all agent have a TCP receiving process
          (start-server agent tcp-port)
          ;; start http server only if http is requested to avoid setting up
          ;; Allegroserve if not needed
          (when http
            (start-http-server agent http-port))))
      
      agent)))

;;;-------------------------------------------------------------------- DEFPOSTMAN

(defMacro defpostman (name &rest args)
  "creates an agent, puts it onto the *agents* list, name must be an unused ~
   symbol.
   Arguments are those of the make-postman function.
Return:
   the agent identifier"
  `(apply #'make-postman ',name ',args))

;;;===============================================================================
;;;                       RECEIVING FUNCTIONS
;;;===============================================================================

;;;------------------------------------------------------------- POSTMAN-RECEIVING
;;; The function should intercept messages sent to the postman itself, e.g. when a
;;; remote site is disconnecting
;;;
;;; when receiving a message there are two important informations:
;;;    - the IP of the sending machine
;;;    - the site from which the message has been sent
;;; If the sender is known in the active postmen or in the known-postmen-info
;;; then we simply update the IP when different from the one we have.
;;; If the sender is not known, we create a new postman-description and add it to 
;;; the known-postmen-info. To do so requires:
;;;    - the postman key (from field of the message)
;;;    - the IP of the machine (sender-ip field of the message)
;;;    - the port info 52008
;;;    - the location (computed from the site field)
;;;    - the protocol (here :TCP may remain nil)
;;;
;;; when the protocol is HTTP the receiving function is done by AllegroServe

(defUn postman-receiving (agent port)
  "function used by the process that waits for incoming messages.
Arguments:
   agent: postman
   port: receiving port
Return:
   never returns, wait in a loop foor messages"
  (unwind-protect 
      ;; create a passive socket, listen to port "port" on localhost
      (let ((p (socket:make-socket :connect :passive :local-port port :backlog 50))
            message-string s info)
        ;; record socket object
        (setf (receive-socket agent) p)
        (format t "~&; ~S/ receiving/ passive socket created: ~&;  ~S" 
          (key agent) p)
        
        (loop           
          ;; create stream for connection requests 
          (setq s (socket:accept-connection p)) 
          ;; get incoming message, returning nil if message is empty (:eof reached)
          (setq message-string (read s nil nil))
          ;; print trace into Lisp console
          ;(format t "~&<<=== ~S/ receiving/ incoming message: ~&  ~S"
          ;  (key agent) message-string)
          
          ;; check if message is the connecting message of a client
          ;; (:client <sender> <site> <port>)
          ;; will allow to build postman description (:JP nil nil :CLIENT :HOME 52008)
          (setq info (client? message-string))
          ;; is a client asking for a connection?
          (when info
            ;; if it is a request for a client connection, then create a specific
            ;; receiving process and save process reference to be able to kill it
            (setf (get (car info) :receiving-process)
              (mp:process-run-function
               (format nil "~S Receiving client ~A" (key agent) (car info))
               #'postman-receiving-from-client agent s info))
            ;; do not close the stream as it will be used by client and server
            ;; but go wait for another possible connection
            )
          
          ;; otherwise close connection and process message
          (unless info
            ;; before closing the stream socket, get the IP of the remote host
            (setq remote-ip (socket:ipaddr-to-dotted (socket:remote-host s)))
            ;(format t "~%; postman-receiving /remote ip from socket: ~S" remote-ip)
            (close s)          
            ;; if message-string is nil, then the message was empty, give up, go 
            ;; wait for the next one (this is an unlikely case). Otherwise go 
            ;; process it
            (when message-string 
              ;; process message converting it to an OMAS object and broadcasting it
              (postman-receiving-process-message agent message-string :TCP))
            
            ;;============
            ;; before sending to the local loop a message gets an id, and when it
            ;; comes back if the id is in the list of messages that have been sent,
            ;; then it is simply discarded...
            )))
    
    ;;=== unwind-protect clause, used when the receiving process is aborted, or the
    ;; plateform exits
    (progn
      ;; not sure that streams should be closed...
      ;(if (postman-stream agent) (close (postman-stream agent)))
      ;; not clear whether we should close passive socket too
      (if (receive-socket agent) (close (receive-socket agent)))
      (setf (receive-socket agent) nil)
      )
    ))

;;;--------------------------------------------------- POSTMAN-RECEIVING-AS-CLIENT
;;; function called when the postman is connecting to an external server by the
;;; CONNECT skill

(defUn postman-receiving-as-client (agent client-socket info)
  "function used by the process that waits for incoming messages in a client postman.
Arguments:
   agent: postman
   client-socket: socket of particular client connection
   info: target postman description
Return:
   never returns, wait in a loop for messages"
  (let (message-string target-key)
    (loop     		
      ;; get incoming message, returning nil if message is empty (:eof reached)
      (setq message-string (read client-socket))
      ;; print trace into Lisp console
      (format t "~&<<--- ~S/ postman-receiving-as-client/ incoming message: ~&  ~S"
        (key agent) message-string)
      ;; if we receive a message containing :eof, this means that the server has
      ;; terminated the connection
      (when (string-equal message-string ":eof")
        ;; close connection
        ;; key of remote-postman
        (setq target-key (car info))
        ;; close socket
        (close client-socket)
        ;; remove socket from property list
        (setf (get target-key :socket) nil)
        ;; clean connected list
        (setf (connected-postmen-info agent)
          (remove target-key (connected-postmen-info agent) :key #'car)) 
        ;; refresh postman window, which will show connections
        (agent-display agent)
        ;(close-connection-as-client agent client-socket info)
        (return-from postman-receiving-as-client))
      
      ;; if message-string is nil, then the message was empty, give up, go wait
      ;; for the next one (this is an unlikely case). Otherwise go process it
      (when message-string 
        ;; process message converting it to an OMAS object and broadcasting it
        (postman-receiving-process-message agent message-string :CLIENT))
      )	; end of loop
    ))

;;;---------------------------------------------- POSTMAN-RECEIVING-CHECK-IDENTITY

(defun postman-receiving-check-identity (agent message protocol)
  "we check the message parameters to verify that the connection description is ~
   correct. If the sender is not known, we create a specific connection.
   In case of failure the function returns nil
Arguments:
   agent: receiving postman
   message: incoming message a an omas object
   protocol: :TCP or :HTTP (no identity check for :CLIENT)"
  (let ((sender-site (car (sender-site message)))
        (sender-key (cadr (sender-site message)))
        (sender-ip-or-name (sender-ip message))
        sender-description sender-ip sender-name)
    ;(format t "~&;  ~S/ postman-receiving-check-identity/ ...message is from postman ~
    ;           ~S of site ~S with IP ~S." 
    ;  (key agent) sender-key sender-site (or sender-ip-or-name :unknown) )
    
    ;; forget about known-postmen-info, it is only used for initiating connections
    ;; create a new description and add it to the connected postmen list
    
    ;; first check if we have enough info to do it, if not failure
    (unless (and sender-key sender-ip-or-name sender-site)
      (return-from postman-receiving-check-identity nil))
    
    ;;=== get IP if needed
    ;; save sender-ip-or-name to be reused if it is an IP
    (setq sender-ip sender-ip-or-name)
    ;; if we have a name rather than an IP, then try to resolve name now rather 
    ;; than when sending messages back
    (unless (socket:dotted-address-p sender-ip-or-name)
      (setq sender-ip (socket:lookup-hostname sender-ip-or-name))
      (unless sender-ip 
        (return-from postman-receiving-check-identity nil))
      ;; otherwise get ip string
      (setq sender-ip (socket:ipaddr-to-dotted sender-ip))
      (setq sender-name sender-ip-or-name))
    
    ;; OK we can recreate description
    (setq sender-description 
          (make-postman-description sender-key 
                                    :ip sender-ip
                                    :name sender-name  ; could be nil
                                    :protocol protocol
                                    :site sender-site
                                    ))
    ;; add it to connected postmen info (active sites)
    (setf (connected-postmen-info agent)
      (append 
       (remove sender-key (connected-postmen-info agent) :key #'car)
       (list sender-description)))
    ;; return true
    t))

;;;------------------------------------------------- POSTMAN-RECEIVING-FROM-CLIENT
;;; is in the scope of the unwind-protect of the calling function. It uses the
;;; stream socket opened in the TCP receiving loop. The stream stays opened, which
;;; allows for exchanges both ways until it is closed. The function executes in
;;; a separate process. When the connection is closed the process is killed.

(defun postman-receiving-from-client (agent socket-id info)
  "Sets up a receiving loop as a TCP server connected to the TCP client. It uses ~
   the TCP stream socket opened in the TCP receiving loop.
Arguments:
   agent: the current postman
   socket-id: the connection stream
   info: a postman description of the calling client 
Return:
   stays in a loop until :eof signal is received"
  (let ((client-key (car info))
        message-content)
    ;; agent is the server structure, client-key is the remote postman key
    ;; record socket for sending messages on the plist of remote postman key
    (setf (get client-key :socket) socket-id)
    ;; declare the connection to be active
    (setf (connected-postmen-info agent)
      (append (connected-postmen-info agent)(list info)))
    ;; show success in the server window
    (pw-show-success agent)
    (agent-display agent)
    
    (loop
      ;; wait for next message
      (setq message-content (read socket-id nil nil))
      ;; print trace into Lisp console
      (format t "~&<<--- ~S/ postman-receiving-from-client/ incoming message: ~&  ~S"
        (key agent) message-content)
      ;; if the message is :eof quit and close socket
      (when (string-equal message-content ":eof")
        ;; we got the message from the client
        ;; close socket
        (close socket-id)
        ;; remove socket from property list
        (setf (get client-key :socket) nil)
        ;; clean connected list
        (setf (connected-postmen-info agent)
          (remove client-key (connected-postmen-info agent) :key #'car))
        ;; refresh postman window, which will show connections
        (agent-display agent)
        ;; exiting will kill the process
        (return-from postman-receiving-from-client))
      
      ;;=== otherwise process the answer
      ;; process message converting it to an OMAS object and broadcasting it
      (when message-content
        (postman-receiving-process-message agent message-content :CLIENT))
      ;; end of loop go wait for next message
      )
    ))

;;;-------------------------------------------------------- POSTMAN-RECEIVING-HTTP

(defun postman-receiving-http (agent port message-string)
  "Takes the string coming from AllegroServe (payload of the HTTP message) and ~
    distributes it to the local network."
  (declare (ignore port))
  ;; print trace into Lisp console
  (format t "~&<<+++ ~S/ receiving http/ incoming message: ~&  ~S"
    (key agent) message-string)
  ;; if message-string is nil, then the message was empty, give up, go wait
  ;; for the next one
  (unless message-string (return-from postman-receiving-http nil))
  
  ;; process message converting it to an OMAS object and broadcasting it
  (postman-receiving-process-message agent message-string :HTTP)
  ;;===========
  ;; here the sender of the message is NOT the local postman. Thus the local 
  ;; postman will receive the message again from the local loop and will then 
  ;; send it to other connected sites, using the THRU parameter to avoid
  ;; sending twice at the same place...
  )

;;;--------------------------------------------- POSTMAN-RECEIVING-PROCESS-MESSAGE

(defun postman-receiving-process-message (agent message-string protocol)
  "takes a received message as a string and processes it. If addressed to us, then ~
    puts it into our mailbox, otherwise broadcasts it onto the local loop, updating ~
    the connected-postmen-info if necessary.
Arguments:
   agent: receiving postman
   message-string: received message as a string
   protocol: TCP or HTTP according to who is callin the function
Return:
   :done"
  (let (message (error-flag t))  
    (ignore-errors
     ;;;+++++ printing codes for debugging Japanese encoding
     (when *print-codes*
       (let (ll cc)
         (dotimes (nn (length message-string))
           (push (char message-string nn) ll))
         (setq cc (mapcar #'char-code ll))
         (format t "~%+++ codes of the message being received: ~%  ~S" (reverse cc))))
     ;;;+++++
     
     ;; convert message-string into OMAS object
     (setq message (net-string-to-message-object message-string))
     ;; if result is nil then syntax error, warn and ignore message
     (unless message
       ;; syntax error, ignore message
       (format t "~%; postman-receiving-process-message /Syntax error in message ~
                 received by ~S: ~&;   ~S" 
         (key agent) message-string)
       (return-from postman-receiving-process-message nil))
     
     ;(format t "~%;  ~S/ postman-receiving-process-message/ ...is message (id: ~S) ~
     ;          already recorded?:  ~S"
     ;  (key agent) (id message)
       ;; using not null to get a boolean answer
     ;  (not (null (member (id message) (ids-of-received-messages agent)))))
     
     ;;=== check if we have already received this message from elsewhere
     (if (member (id message) (ids-of-received-messages agent))
         ;; yes, then forget it
         (return-from postman-receiving-process-message nil)
       ;; otherwise, add id to list, keeping the length under irm-max-size
       (setf (ids-of-received-messages agent)
         (cons (id message) 
               (if (>= (length (ids-of-received-messages agent)) 
                       (irm-max-size agent))
                   (butlast (ids-of-received-messages agent))
                 (ids-of-received-messages agent)))))
     
     ;;=== if the message is for us, just deliver it 
     (when (eql (to! message) (key agent))
       ;; deliver it, don't put it on the loop, don't forward it
       ;; we set the date to current time of receiving machine
       (setf (date message) (get-universal-time))
       ;; put message directly into the receiving postman mailbox
       (%agent-add agent message :input-messages)
       ;;update receiver'S display
       (agent-display agent)
       (return-from postman-receiving-process-message :done))
     
     ;;=== here we check the identity of the sending postman to update the 
     ;; connected-postmen-info list (useless for client connections)
     (unless (or (eql protocol :client)
                 (postman-receiving-check-identity agent message protocol))
       ;; if this fails, we give up
       (return-from postman-receiving-process-message nil))
     
     ;;=== we need to send to active postmen that did not yet see this message
     (postman-receiving-propagate agent message)
     
     ;;=== then send message to the local coterie
     ;(format t "~&;  ~S/ postman-receiving-process-message / ...calling send-message ~
     ;    to distribute the message (id: ~S) locally and on lan."
     ;  (key agent) (omas::id message))
     (send-message message)
     ;; if we got there, there was no error
     (setq error-flag nil)
     )
    
    (if error-flag
        (format t "~&;  ~S/ postman-receiving-process-message / error in processing ~
	            message"  (key agent)))
    :done))

;;;--------------------------------------------------- POSTMAN-RECEIVING-PROPAGATE
;;; this function is very similar to postman-send

(defun postman-receiving-propagate (agent message)
  "propagate a received message to active postmen that have not seen it yet"
  (let* (;; active postmen to which the message has already been through
         (thru-postmen (thru message))
         (active-postmen (mapcar #'postman-key (connected-postmen-info agent)))
         (target-postmen 
          ;; we will send to sites that are not in the thru property
          ;; making sure that we do not send to ourselves
          (remove (key agent) (set-difference active-postmen thru-postmen)))
         )
    ;; quit if no target left
    (unless target-postmen (return-from postman-receiving-propagate))
    
    (format t "~%; postman-receiving-propagate / target-postmen: ~S" target-postmen)
    
    ;;=== now we are ready to send				 
    ;; add postman list to the :THRU message parameter to prevent other sites
    ;; to send it back
    (setf (omas::thru message)
      (delete-duplicates
       (cons (key agent) 
             (append thru-postmen target-postmen))))
    
    ;; we change the sender site because this is used when sending the message to
    ;; decide if we need a local or external address, and/or proxy
    (setf (sender-site message) (list (site agent) (key agent)))
    
    ;; message should have already an id since it was sent by a postman
    
    ;; then, send to each listed remote plaform 
    (dolist (target target-postmen)
      (postman-send-choosing-protocol agent message target))
    
    :done))

;;;===============================================================================
;;;                       SENDING FUNCTIONS
;;;===============================================================================

;;;------------------------------------------------------------------ POSTMAN-SEND
;;; standard function that can be used for transferring messages to other OMAS
;;; sites. Does not reformat messages. Does not send messages to postmen through
;;; which the message has already been. Adds fields to the message to avoid
;;; infinite looping.
;;; If the postman is behind a firewall and sends to an external site, it should 
;;; include in the message its external address, so that the answer will be directed
;;; to the right machine (the address may be an IP or a name like "nat-omas.utc.fr")

;;; recording postmen keys in (thru message) garantees that a postman will not receive
;;; the message twice. However, a specific site may receive it more than once if it
;;; has several postmen connected to other loops. Therefore, it is better to have only
;;; one postman acting as a gateway per site, which makes thing more brittle.
;;; The choice of the protocol is specified in the remote postman description
;;; meaning that we can send to a remote postman using TCP and another one using HTTP
;;; (not sure this is wise...)

(defUn postman-send (agent message)
  "function that sends every message seen by the postman to active remote sites, 
   with the exception of answers to the user, messages addressed specifically
   to the postman and system messages.
Arguments:
   agent: postman
   message: message to send (an object)
Return:
   :done"
  (let* (;; when receiver is nil, message was sent to the control panel, don't pass it
         (receiver (or (to! message) (site-user agent)))
         ;; active postmen to which the message has already been through
         (thru-postmen (thru message))
         (active-postmen (mapcar #'postman-key (connected-postmen-info agent)))
         (target-postmen 
          ;; we will send to sites that are not in the thru property
          ;; making sure that we do not send to ourselves
          (remove (key agent) (set-difference active-postmen thru-postmen)))
         )
    ;; quit if no target left
    (unless target-postmen (return-from postman-send))
    
    ;(format t "~%; postman-send / target-postmen: ~S" target-postmen)
    
    ;; otherwise, check message		
    (when (or 
           ;; don't send messages from or to the OMAS panel
           ;(eql receiver (site-user agent))
           ;; don't send messages addressed to us!
           (eql receiver (key agent))
           ;; don't transfer system messages
           (system-message? message)
           )
      (return-from postman-send))
    
    ;;=== now we are ready to send				 
    ;; add postman list to the :THRU message parameter to prevent other sites
    ;; to send it back
    (setf (omas::thru message)
      (delete-duplicates
       (cons (key agent) 
             (append thru-postmen target-postmen))))
    
    ;; add sender site to the message, e.g. (:UTC :JPB) when notebook is connected
    ;; internally
    (setf (sender-site message) (list (site agent) (key agent)))
    ;;... and message identifier if none exists. If one exists we want to keep it
    ;; (message identifier is a random tag different from message name)
    (unless (id message)
      (setf (id message) 
        (create-message-id (ids-of-received-messages agent))))
    
    ;; then, send to each listed remote plaform 
    (dolist (target target-postmen)
      (postman-send-choosing-protocol agent message target))
    
    :done))

;;;------------------------------------------------ POSTMAN-SEND-CHOOSING-PROTOCOL

(defun postman-send-choosing-protocol (agent message target)
  "call a remote send function according to the protocol used for the connection.
Arguments:
  agent: our postman
  message: message to be sent
  target: remote postman key
Return:
  nothin interesting."
  (let ((target-description (assoc target (connected-postmen-info agent))))
    ;; one can only have a single active connection with a remote postman
    (unless target-description
      (error "target ~S should have a description in the connected-postmen-info of ~S"
        target agent))
    
    ;;=== put the return address into the message being sent
    ;; if the site of the target postman is the same as our site, then we put
    ;; our internal address into the message for proper return
    ;;***** actually this is not useful since the receiving postman can retrieve the
    ;; ip from the stream socket
    (setf (sender-ip message)
      (if (eql (site agent)(postman-site target-description))
          (internal-ip agent)
        (or (external-ip agent)(internal-ip agent))))
    
    ;;=== call the proper send-remote according to protocol
    (case (postman-protocol target-description)
      (:http
       ;; will use Allegroserver threads
       (send-remote-http agent message target))
      (:client
       ;; send directly using opened socket
       (mp:process-run-function
        (format nil "~S sending to ~S using CLIENT" (key agent) target)
        #'send-remote-client agent message target))
      ;; TCP is the default protocol		  
      (otherwise
       ;; for each message create a thread to avoid waiting for timeouts
       (mp:process-run-function 
        (format nil "~S sending to ~S using TCP" (key agent) target) 
        #'send-remote-tcp agent message target)))
    :done))

;;;----------------------------------------------------------- SEND-REMOTE-CLIENT

(defun send-remote-client (agent message agent-key)
  "sending a message either from client to server or from server to client
Arguments:
   agent: local postman
   message: message object to send
   agent-key: remote postman key
Return:
   nothing of interest"
  ;; just in case process fails
  (let ((stream-socket (get agent-key :socket))
        (error-flag t))
    (unwind-protect
        (let (message-string)
          (unless stream-socket
            ;; this should never happen, since either the connection has not been
            ;; opened yet, or it has been closed and clean up hopefully done
            (warn "connection to ~S is closed?" agent-key)
            (return-from send-remote-client))
          ;; otherwise make a message string
          (setq message-string (net-message-to-string message))
          (format t 
              "~&--->> ~S/ send-remote-client/ message (ID: ~S) sent to: ~A. ~
         Message:~&  ~S" 
            (key agent) (omas::id message) agent-key message-string)
          
          ;; allow 30 seconds to make connection, otherwise declare connection lost
          ;; and clean up local data. This is to avoid hanging processes
          ;(mp:with-timeout 
              ;(30 (format t  "~& send-remote-client/ message (ID: ~S) timeout error,~
              ;                closing connection after 30s" (omas::id message))
               ;(close stream-socket)
               ;; clean remote postman p-list
               ;(setf (get agent-key :socket) nil)
               ;(setf (get agent-key :process) nil)
               ;; and remove postman description from connected-postmen-info
               ;(setf (connected-postmen-info agent)
               ;  (remove agent-key (connected-postmen-info agent) :key #'car))
               ;(return-from send-remote-client nil)
               ;)
            (write message-string :stream stream-socket)
            ;)
          (setq error-flag nil)
          )
      ;; in case of error do some clean up
      (progn
        ;; if error-flag is t then the previous form did not execute normally
        ;; we do not want to close the socket normally
        (if (and error-flag stream-socket)
            (close stream-socket))
        )
      )))

;;;------------------------------------------------------------- SEND-REMOTE-HTTP

(defun send-remote-http (agent message site-key)
  "sending message to the remote site using AllegroServe. Remote site must be 
   connected and active. Uses port 80.
Arguments:
   agent: postman (ignored)
   message: message object to send
   site-key: remote site, e.g. :TECPAR
Return:
   unimportant."
  ;; if remote site is not active don't send (verified when called from SEND)
  (unless (is-site-active? agent site-key)
    (format t "~&~S/ send-remote-http/ Error. Unconnected destination ~S." 
      (key agent) site-key)
    (return-from send-remote-http nil))
  
  (let* ((site-info (assoc site-key (connected-postmen-info agent)))
         address remote-ip
         ;; transform message object into a string
         (message-string (net-message-to-string message))
         )
    
    ;; get ip of the remote machine hosting the postman
    (unless (setq remote-ip (get-remote-host-ip site-info))
      ;; could not get ip for some reason
      (format t "~&~S/ send-remote-HTTP/ Can't connect with ~S."
        (key agent) site-info)
      (return-from send-remote-http nil))
    
    ;; build the address of the omas channel
    (setq address (format nil "http://~A:~A/omascc" remote-ip
                    (postman-port site-info)))
    (format t 
        "~&+++>> ~S/ send-remote-http/ message (ID: ~S) sent to: ~A proxy: ~S. ~
         Message:~&  ~S" 
      (key agent) (omas::id message) address (proxy agent) message-string)
	  
	;;;+++++ debugging Japanese encoding
	(when *print-codes*
	 (let (ll cc)
	   (dotimes (nn (length message-string))
	      (push (char message-string nn) ll))
	   (setq cc (mapcar #'char-code ll))
	   (format t "~%+++ codes of the message being sent: ~%  ~S" (reverse cc))))
	 ;;:+++++
    
    ;; must specify proxies for sites outside the firewall
    (if (eql (postman-site site-info)(car (sender-site message)))	  
        ;; same site: no proxy
        (NET.ASERVE.CLIENT:DO-HTTP-REQUEST address 
          :method :post
          :external-format (excl:crlf-base-ef :utf-8)
          :content message-string)
      ;; otherwise use a proxy
      (NET.ASERVE.CLIENT:DO-HTTP-REQUEST address 
        :method :post
        :proxy (proxy agent)
        :external-format (excl:crlf-base-ef :utf-8)
        :content message-string))
    t))

;;;--------------------------------------------------------------- SEND-REMOTE-TCP

(defUn send-remote-tcp (agent message site-key)
  "sending message to the remote site by opening a socket. Remote site must be 
   connected and active, otherwise opening socket will fail.
Arguments:
   agent: postman (ignored)
   message: message object to send
   site-key: remote site, e.g. :TECPAR
Return:
   :done or nil in case of error"
  
  ;; if remote site is not in the connected-postmen-info list, do not send
  (unless (is-site-active? agent site-key)
    (format t "~&~S/ send-remote-TCP/ Error. Unconnected destination ~S." 
      (key agent) site-key)
    (return-from send-remote-tcp nil))
  
  ;; otherwise prepare to send
  (let* ((site-info (assoc site-key (connected-postmen-info agent)))
         (ip (postman-ip site-info))
         (port (postman-port site-info))
         (message-string (net-message-to-string message))
         socket-id)
    
    ;; make sure we have a receiving process to accept answers
    (start-server-if-needed agent port)
    
    ;; get remote host ip if not there
    (unless ip
      (setq ip (connect-static-check-target agent site-info))
      (unless ip (return-from send-remote-tcp nil))
      )
    
    ;; open an active socket for sending message
    (setq socket-id (open-active-socket agent ip site-info))
    
    (unless socket-id
      ;; could not create socket for some reason
      (cg:beep)
      (format t "~&~S/ send-remote-TCP/ Can't connect with ~S. Message:~&  ~S" 
        (key agent) site-info message-string)
      ;; not necessary to close socket since opening it failed
      (format t "~&~S/ send-remote-TCP/ ...removing ~S from connected-postmen-info" 
        (key agent) site-key)
      ;; remove site from the active list
      (setf (connected-postmen-info agent) 
        (remove site-key (connected-postmen-info agent) :key #'car))
      ;; refresh postman window (done by open-active-socket)
      (return-from send-remote-tcp nil))
    
    ;; when socket is non nil, we can send
    (write message-string :stream socket-id)
    ;; close the socket before leaving
    (close socket-id)
    ;; print trace into the Lisp console
    ;(format t 
    ;    "~&===>> ~S/ send-remote-TCP/ message (ID: ~S) sent to: ~A:~S. Message:~&  ~S" 
    ;  (key agent) (omas::id message) ip port message-string)
    :done))

;;;==============================================================================
;;;                            START SERVER
;;;==============================================================================

;;;------------------------------------------------------------ START-HTTP-SERVER

(defun start-http-server (agent port)
  "asks AllegroServe to start. reset port to 80"
  ;; when status is active, it means that we have a set of Allegroserve processes 
  ;; already waiting on the specified port. This is the case when the :server 
  ;; option was specified when creating the postman (default)
  ;;********** this should be recorded at the OMAS level not at the agent level
  ;(unless (eql (status agent) :active)
  (unless (and (http-server *omas*)
               (eql (http-server-port *omas*) port)
               (eql (status agent) :active))
    ;; must activate allegroserve
    (eval-when (:compile-toplevel :execute)
      (require :aserve))
    ;; call allegroserve 
    (setf (http-server *omas*)
      (net.aserve:start 
       :port port 
       :external-format (excl:crlf-base-ef :utf-8)))
    (setf (http-server-port *omas*) port)
    ;; increase the number of agents using the server (from 0 to 1)
    (incf (http-server-reference-count *omas*))
    )
  ;(break "start-http-server, EF: ~S" net.aserve:*default-aserve-external-format*)
  ;; timeout after 2 seconds, since we are not interested in the answer to the
  ;; sent messages
  (setf NET.ASERVE:*HTTP-RESPONSE-TIMEOUT* 2)
  ;; indicate that the server is running
  (setf (status agent) :active)
  ;; set up an entity that will receive the outside requests
  (NET.ASERVE:publish 
   :path "/omascc"
   ;:port port
   :function #'(lambda (req ent)
                 (NET.ASERVE:with-http-response (req ent)
                   (NET.ASERVE:with-http-body (req ent)
                     (progn
                       ;; here we transfer the received data to the LAN
                       (postman-receiving-http 
                        agent port  
                        ;(NET.ASERVE:GET-REQUEST-BODY :external-format :UTF-16BE))
                        (NET.ASERVE:GET-REQUEST-BODY 
                         req :external-format (excl:crlf-base-ef :utf-8)))
                       ;; and answer something (will be ignored)
                       (format NET.HTML.GENERATOR::*html-stream*
                           "UTC-HTTP got the message!")
                       (force-output NET.HTML.GENERATOR::*html-stream*))))))
  t)

;;;----------------------------------------------------------------- START-SERVER
;;; called when using TCP exchange to set up the receiving process

(defun start-server (agent port)
  "assuming that no process is already on, starts a receiving process listening ~
   on port 52008 (default)."
  (format t "~%;===== Entering start-server; connection type ~S" 
    (connection-type agent))
  (cond 
   ((eql (connection-type agent) :tcp)
    (format t "~%;----- creating receiving process")
    (setf (receiving-process agent)
      ;; start listening loop (also used to accept client connections)
      (mp:process-run-function 
       (format nil "~S Receiving" (key agent)) 
       (or (receiving-fcn agent) #'postman-receiving) agent port))
    (format t "~%;----- receiving process created")
    )
   )
  ;; exit
  t)

;;;------------------------------------------------------- START-SERVER-IF-NEEDED

(defun start-server-if-needed (agent port)
  "creates a process for receiving messages, if not already done."
  
  ;; if process exists and socket is OK, quit, nothing to do
  (when (and (receiving-process agent) (receive-socket agent))
    ;(format t "~%; ~S start-server-if-needed /server already active." (name agent))
    (return-from start-server-if-needed t))
  
  ;; if process exists but socket was killed, kill process
  (when (receiving-process agent)
    (mp:process-kill (receiving-process agent)))
  
  ;; and recreate process and socket
  (start-server agent port))

;;;==============================================================================
;;;                         POSTMAN DEFAUL SKILLS
;;;==============================================================================

;;;--------------------------------------------------- POSTMAN-ADD-DEFAULT-SKILLS
;;; JPB 100409

(defUn postman-add-default-skills (agent)
  "function adding default skills to a given postman to install a minimal ~
   behavior, in addition to the standard agent default skills.
Arguments:
   agent: agent id or keyword
Return:
   :done"
  ;; the CONNECT skill is used to reconnect a distant site when the site for
  ;; some reason has been disconnected.
  (make-skill :connect agent
              :static-fcn 'connect-static)
  
  ;; the DISCONNECT skill is used to disconnect a distant site whenever we do
  ;; not want to send messages to this site (however the site could get the
  ;; messages through another path through other connected sites. Thus, the
  ;; skill is not very useful.
  (make-skill :disconnect agent
              :static-fcn 'disconnect-static)
  
  ;; the RESET skill kills everything and resets global variables. It restarts
  ;; the server, cleaning all temporary data.
  ;(make-skill :reset agent
  ;            :static-fcn 'reset-static)
  
  ;; the SEND skill sends local messages to remote connected sites using a
  ;; direct protocol or an http protocol
  (make-skill :send agent
              :static-fcn 'send-static)
  
  ;; the STATUS skill returns some data about the state of the connections
  ;(make-skill :status agent
  ;            :static-fcn 'status-static)
  ;; for HTTP protocol we add two skills
  (when (http agent)
    (make-skill :shut-down agent
                :static-fcn 'shut-down-static)
    ;(make-skill :start agent
    ;            :static-fcn 'start-static)
    )
  )
;;;==============================================================================
;;;                               CONNECT
;;;==============================================================================

;;;--------------------------------------------------------------- CONNECT-STATIC
;;; two cases according to the protocol parameter of the postman-description of the
;;; known-postmen-info :
;;; 1. TCP client
;;; in that case we simply open an active socket on the target computer, wait for
;;; the connection and do the following things:
;;;   - save the socket onto the p-list of the postman key
;;;   - add the postman description to the list of connected-postmen-info
;;;   - set up the receiving loop based on the socket
;;; 2. TCP dual servers
;;; there is no need to connect to a remote site for receiving messages. Therefore
;;; the connect skill is more a test for making sure that somebody is listening 
;;; at the other end and that we are ready to receive its answer. If the defpostman
;;; did not include the :server option, then we must set up the receiving process.
;;; When a link has been set onto a remote site, the *connected-sites-info* has
;;; the corrresponding site name and IP.
;;; hence the actions should be:
;;;  - check the availability of the remote site by opening an active socket
;;;  - add the postman description to the list of connected-postmen-info
;;;  - set up a receiving loop if this was not done previously

(defUn connect-static (agent message remote-postman-info 
                             &key name-or-ip (protocol :TCP))
  "the CONNECT skill checks if the remote site can be reached by opening a socket
   and if so makes the remote site active by including its key into the
   connected-sites-info list. 
Arguments:
   remote-postman-info: the remote postman description
   name-or-ip (key): name or ip of the target machine; ignored
   protocol (key): :TCP, :HTTP or :CLIENT; ignored
Return:
   :done"
  (declare (ignore message name-or-ip protocol))
  ;; if site is already in the active list, give up
  (if (is-site-active? agent (car remote-postman-info))
      (return-from connect-static (static-exit agent :done)))
  ;; clean target plist to remove potential errors
  (setf (get (car remote-postman-info) :ip) nil)
  
  (case (postman-protocol remote-postman-info)
    (:http 
     (connect-static-http agent remote-postman-info))
    (:tcp 
     (connect-static-tcp agent remote-postman-info))
    (:client  
     (connect-static-client agent remote-postman-info)))
  (static-exit agent :done))

;;;-------------------------------------------------- CONNECT-STATIC-CHECK-TARGET

(defun connect-static-check-target (agent info)
  "when the specified target is reachable, returns its IP otherwise nil.
Arguments:
   agent: the postman
   info: a postman description
Return:
   target IP or nil on failure."
  (let ((remote-ip (postman-ip info))
        test errno socket-id)
    ;; === try to open a socket onto the remote site, catching socket errors
    ;; first get IP of remote site, info may not contain IP but name
    (unless remote-ip
      (multiple-value-setq (test errno)
        (ignore-errors
         (setq remote-ip
               (socket:ipaddr-to-dotted
                (socket:lookup-hostname ; works on names and IPs
                 (postman-name info))))))
      ;; check if error
      (when (and (null test) errno)
        (format t "~&~S/ connect-static-check-target/ can't get IP for remote~
                   postman: ~S" 
          (key agent) (postman-name info))
        (cg:beep)
        (return-from connect-static-check-target nil))
      )
    
    ;; here we got remote IP, try to open socket
    (multiple-value-setq (test errno)
      (ignore-errors
       ;; create a socket to connect with the remote host, TCP port or HTTP port
       (setq socket-id
             (socket:make-socket :remote-host remote-IP
                                 :remote-port (postman-port info)))))
    
    ;; test for error (usually timeout)
    (when (and (null test) errno)
      ;; could not create socket: nobody is listening on port 52008...
      (cg:beep)
      (format t "~&~S/connect-static-check-target/ Cannot reach ~S.~&  Error: ~S." 
        (name agent) remote-IP errno)
      (return-from connect-static-check-target nil))
    
    ;; otherwise return target IP
    (close socket-id)
    remote-IP))

;;;-------------------------------------------------------- CONNECT-STATIC-CLIENT
;;; when connecting by pushing the button...
;;; as a client sets up a specific receiving loop (receiving-as-client)
;;; as a server should spawn a specific receiving loop using the current stream
;;; socket

(defun connect-static-client (agent info)
  "checks the existence of the target postman by creating an active socket and
   save the socket and update the known-postmen-info.
Arguments:
   agent: our postman
   info: the postman description of the target postman
Return:
   :done"
  (let (remote-ip socket-id receiving-process target-info)
    (unless (or (postman-ip info)(postman-name info))
      (error "postman ~S cannot use the current postman description: ~S"
        (name agent) info))
    
    ;; get the IP of the remote target
    (unless (setq remote-ip (get-remote-host-ip info))
      (format t "~%; connect-static-client / can't get remote host IP from: ~S"
        info)
      ;; paint the connecting pane red for 2/10s to indicate failure
      (pw-show-failure agent)
      ;; refresh postman window, which will show connections as a safety measure
      (agent-display agent)
      (return-from connect-static-client))
    
    ;;=== try to open active socket
    (unless (setq socket-id (open-active-socket agent remote-ip info))
      (format t "~%; connect-static-client /can't open a socket on remote host: ~S"
        remote-ip)
      (return-from connect-static-client))
    
    ;;=== OK we have an open active socket
    ;; save socket-id on plist of remote postman key
    (setf (get (car info) :socket) socket-id)
    (format t "~&; ~S/ Connect-static-client/ TCP connection active: ~&;  ~S" 
      (key agent) socket-id)
    ;; add to connected-postmen-info
    (setf (connected-postmen-info agent)
      (append (connected-postmen-info agent) (list info)))
    
    ;; send a connect message with info for server describing us!
    ;; e.g. (:JPB "JEAN-PAULBAC4F6" nil :CLIENT :HOME)
    ;; port is assigned automatically by system
    ;; name, ip, port won't be used for connection
    (setq target-info 
          (make-postman-description (key agent)
               :name (socket:ipaddr-to-hostname (socket:local-host socket-id))
               :protocol :client
               :port (socket:local-port socket-id)
               :ip (socket:ipaddr-to-dotted (socket:local-host socket-id))
               :site (site agent)))
    (write (format nil "~S" (cons :cnct target-info)) :stream socket-id)
    
    ;;=== create receiving loop
    (setq receiving-process
          (mp:process-run-function
           (format nil "~S Receiving client" (key agent))
           (or (receiving-fcn agent) #'postman-receiving-as-client) 
           agent socket-id info))
    ;; record process onto the plist of the target server
    (setf (get (car info) :receiving-process) receiving-process)
    ;;===
    :done))

;;;-------------------------------------------------- CONNECT-STATIC-GET-HOSTNAME

(defun connect-static-get-hostname (agent)
  "gets the local agent IP"
  ;; === if our local IP is not yet initialized, get it using socket package
  (or (internal-ip agent)
      (socket:ipaddr-to-dotted
       (socket:lookup-hostname (excl.osi:gethostname)))))

#|
(omas::connect-static-get-hostname jpb::xa_jpb)
"172.26.138.237"
|#
;;;---------------------------------------------------------- CONNECT-STATIC-HTTP

(defun connect-static-http (agent info)
  "the CONNECT skill checks if the remote site can be reached by opening a socket
   and if so makes the remote site active by including its key into the
   *connected-sites-info* list. 
Arguments:
   agent: postman
   info: a postman description
Return:
   :done"
  (let ((remote-ip (postman-ip info)))
    ;; set up Allegrocache server, which creates several processes
    ;; if already active will do nothing
    (start-http-server agent (postman-port info))
    
    ;; === check remote site by opening a socket onto it
    ;; returns IP if OK nil otherwise
    (setq remote-ip (connect-static-check-target agent info))
    
    (unless remote-ip
      ;; paint the connecting pane red for 2/10s to indicate failure
      (pw-show-failure agent)
      (return-from connect-static-http nil)
      )
    
    ;; === now we must add the pair (site-key . remote-IP) to connected-sites-info
    ;; this is only a problem when our site is unknown and we must provide our IP
    (setf (connected-postmen-info agent)
      (append (connected-postmen-info agent) (list info)))
    
    ;; and we are happy
    (format t "~&~S/ connect-http/ connected-http-sites-info: ~A." 
      (key agent) (format nil "~S" (connected-postmen-info agent)))
    
    ;; paint the connection pane green for 1/10s showing OK
    (pw-show-success agent)
    
    ;; refresh postman window, which will show connections
    (agent-display agent)
    :done))

;;;----------------------------------------------------------- CONNECT-STATIC-TCP

(defun connect-static-tcp (agent info)
  (let ((remote-ip (postman-ip info)))
    
    ;; === start the server if needed
    (start-server-if-needed agent (postman-port info))
    
    ;; === check remote site by opening a socket onto it
    ;; returns IP if OK nil otherwise
    (setq remote-ip (connect-static-check-target agent info))
    
    (unless remote-ip
      ;; paint the connecting pane red for 2/10s to indicate failure
      (pw-show-failure agent)
      ;; and update postman window
      (agent-display agent)
      (return-from connect-static-tcp nil)
      )
    
    ;; === now we add the postman description to connected-sites-info
    (setf (connected-postmen-info agent)
      (append (connected-postmen-info agent) (list info)))
    
    ;; and we are happy
    (format t "~&~S/ static-connect/ connected-sites-info: ~S." 
      (key agent) (connected-postmen-info agent))
    
    ;; paint the connection pane green for 1/10s showing OK
    (pw-show-success agent)
    
    ;; refresh postman window, which will show connections
    (agent-display agent)
    :done))

;;;==============================================================================
;;;                            DISCONNECT
;;;======================================================================== skill

;;;------------------------------------------------------------ DISCONNECT-STATIC
;;; we remove the target postman from our connected-postmen-info list and send
;;; a disconnect message to the remote postman to remove us from its list.

(defUn disconnect-static (agent message info &optional cleanup)
  "the DISCONNECT skill removes a remote site from the list of active sites and ~
   sends a message to the postman to be disconnected to ask it to remove us from ~
   its connected-postmen list.
Arguments:
   agent: current postman
   message: received message
   info: postman description of the remote postman
   cleanup (opt): if true the message is a request to update the
                  connected-postmen-info when the connection has been broken on
				  the other side
Return:
   :done"
  (declare (ignore message))
  (let ((postman-key (car info))
        msg socket-id process-id)
    ;; if already disconnected do nothing
    ;; here we assume that a postman cannot be connected to another postman using
    ;; more than one protocol, thus appearing only once in the list of connected
    ;; postmen info
    (unless (assoc postman-key (connected-postmen-info agent))
      (return-from disconnect-static (static-exit agent :done)))
    
    ;; client-server have a special disconnect protocol
    ;; the skill is called when clicking the disconnect button
    (case (postman-protocol info)
      (:client
       ;; get local socket
       (setq socket-id (get postman-key :socket))
       ;(format t "~%; disconnect-static /socket-id: ~S" socket-id)
       ;; send message to the other side telling we are closing
       (write ":eof" :stream socket-id)
       ;; clear plist
       (setf (get postman-key :socket) nil)
       ;; close socket
       (close socket-id)
       ;; and kill process on the client side
       (setq process-id (get postman-key :receiving-process))
       ;(format t "~%; disconnect-static /process-id: ~S" process-id)
       (when process-id
         (setf (get postman-key :receiving-process) nil)
         (mp:process-kill process-id)
         )
       )
      (otherwise  
       ;; make a disconnect message to remote postman
       ;; careful the receiving disconnect skill expect info as first arg
       ;; we simulate by sending a list of the agent keyword
       (unless cleanup
         (setq msg (make-instance 'message :type :inform :to postman-key 
                     :action :disconnect :args (list (list(key agent)) :cleanup)))
         (setf (id msg)(create-message-id (ids-of-received-messages agent)))
         ;; send it to remote postman (depends on the type of connection)
         (postman-send-choosing-protocol agent msg postman-key) 
         
         ;; wait some time to allow sending message
         (sleep .3)
         )))
    
    ;; now and only now remove entry
    (setf (connected-postmen-info agent)
      (remove postman-key (connected-postmen-info agent) :key #'car))  
    
    ;; refresh postman window
    (agent-display agent)
    (static-exit agent :done)))

;;;==============================================================================
;;;                               RESET
;;;======================================================================== skill

(defUn reset-static (agent message &aux msg)
  "the RESET skill closes ALL connections with remote sites and kills ~
   the TCP receiving process. Does not shut down the HTTP server. 
   It then restarts the TCP receiving process."
  (declare (ignore message))
  ;; close all connections
  (dolist (postman-description (connected-postmen-info agent))
    (setq msg (make-instance 'message
                :type :inform :from (key agent) :to (key agent)
                :action :disconnect :args (list postman-description)))
    (send-message msg :locally? t))
  ;; wait for disonnection to be done
  (sleep 2)
  
  ;; reset connected postmen info
  (setf (connected-postmen-info agent) nil)
  ;; take care of TCP connections
  (if (receive-socket agent) (close (receive-socket agent)))
  (setf (receive-socket agent) nil)
  (if (receiving-process agent) (mp:process-kill (receiving-process agent)))
  (setf (receiving-process agent) nil)
  
  ;; restarts TCP server
  (setf (receiving-process agent)
    (mp:process-run-function 
     (format nil "~S Receiving" (key agent)) 
     #'postman-receiving agent (TCP-port agent)))
  ;; console trace
  (format t "~&~S/ static-reset/ new receiving process: ~S" 
    (key agent) (receiving-process agent))
  
  ;; refresh postman window
  (agent-display (%agent-from-key (key agent)))
  (static-exit agent :done))

;;;==============================================================================
;;;                                 SEND
;;;======================================================================== skill

(defun send-static (agent in-message message-string message)
  "skill that sends every message seen by the postman to active remote sites, 
   with the exception of the local user and system messages.
Arguments:
   agent: postman
   in-message: incoming message
   message-string: message to send, as a string (ignored)
   message: message to send, as an object"
  (declare (ignore in-message message-string))
  ;; call the default postman send function
  (postman-send agent message)
  (static-exit agent :done))

;;;==============================================================================
;;;                              SHUT-DOWN
;;;==============================================================================

;;;-------------------------------------------------------------------- SHUT-DOWN

(defun shut-down-static (agent message)
  "shuts down the HTTP Allegroserve server"
  (declare (ignore message))
  (when (eql (status agent) :active)
    ;; close server killing the receiving processes
    (NET.ASERVE:SHUTDOWN :server NET.ASERVE:*wserver*)
    (setf (status agent) :inactive)
    )
  (static-exit agent :done))

;;;=============================================================================
;;;                               START
;;;=============================================================================

;;;------------------------------------------------------------------------ START

(defun start-static (agent message)
  "starts an Allegroserve server unless it is already active."
  (declare (ignore message))
  (unless (eql (status agent) :active)
    (start-http-server agent (http-port agent))
    (setf (status agent) :active)
    )
  (static-exit agent :done))

(format t "~&;*** OMAS v~A - postman loaded ***" *omas-version-number*)

;;; :EOF


