﻿;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;                O M A S - W - A S S I S T A N T  (file W-assistant.lisp)
;;;
;;;===============================================================================
;;; This file contains all the functions related to assistant windows for communicating
;;; with a user during a omas simulation.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de CompiègneTC (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;; Still missing: 
;;;   - dialog to set up pending requests
;;;   - process for handling discarded messages

#|
History
-------
2020
 0202 restructuring OMAS as an asdf system
|#

(in-package :omas)

;;;===================================================================================
;;;
;;;                                GLOBALS
;;;
;;;===================================================================================

;(setf (com-window albert::albert) (make-master-window albert::albert))

;;; this is useless if there are several assistants in the same environment
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defParameter *master-window* nil)
  )

;;;================== dimensions for the assistant window ============================
;;;
;;;
;;;               _____________________________________________________________
;;; title-height |_____________________________________________________________|
;;;              |         ________    _______                                 |
;;;              |        |________|  |_______|                                |
;;;              |
;;;              |left-tile-h <title-size>  
;;;              |left-margin            center-margin             right-margin|
;;;              |   _________________________     _______________________     |
;;;              |  |                         |   |                       |    |
;;;              |  |      left-width         |   |     right-width       |    |

(eval-when (:compile-toplevel :load-toplevel :execute)
  
  (defParameter *mw-default-font* 
    (cg:make-font-ex :SWISS "MS Sans Serif / ANSI" 11 NIL)
    "seems to be used only by MCL"
    )
       
  (defParameter *master-window-color*
    (cg:make-rgb :RED 202 :GREEN 228 :BLUE 255)
    )
  
  (defParameter *mw-position* '(52 89) "position of the assistant window")
  (defParameter *mw-width* 1006 "width of the assistant window")
  (defParameter *mw-inside-width* 1000)
  (defParameter *mw-height* 602 "height of the assistant window")
  (defParameter *mw-inside-height* 600)
  
  ;;; normally all positioning of the various areas follow from the window size
  ;;; Therefore, in principle the window could be resized
  (defParameter *mw-side-margin* #+MCL 8 #-MCL 4 "left and right space")
  (defParameter *mw-half-center-margin* 2 "1/2 space between the 2 sides")
  (defParameter *mw-half-width* (ceiling (/ *mw-inside-width* 2)))
  (defParameter *mw-left-width* (- *mw-half-width* *mw-side-margin*
                                   *mw-half-center-margin*))
  
  (defParameter *mw-button-width* 70)
  (defParameter *mw-button-height* 16)
  (defParameter *mw-left-left-button-h* 4 "?")
  
  (defParameter *mw-left-title-h* 40)
  (defParameter *mw-title-width* 215)
  (defParameter *mw-title-height* 18)
  
  (defParameter *mw-inter-button-h* 10)
  
  (defParameter *mw-command-button-width* 70)
  (defParameter *mw-command-button-size*
    (list *mw-command-button-width* 19))
  ;(make-point *mw-command-button-width* 16))
  
  ;;; left vertical arrangements
  (defParameter *mw-top-margin* 2)
  (defParameter *mw-title-bar-height* 18) ; only for ACL
  (defParameter *mw-bottom-margin* 2)
  (defParameter *mw-left-inter-area-v* #+MCL 30 #-MCL 24)
  (defParameter *mw-additional-offset-v* #+MCL 4 #-MCL 4)
  (defParameter *mw-small-list-v* 
    (floor (/ (- *mw-inside-height*
                 #-MCL *mw-title-bar-height*
                 (* 4 *mw-left-inter-area-v*)
                 *mw-top-margin*
                 *mw-bottom-margin*)
              6)))
  (defParameter *mw-large-list-v* (* 2 *mw-small-list-v*))
  
  ;;; right part
  (defParameter *mw-right-width* *mw-left-width*)
  (defParameter *mw-right-title-h* (+ *mw-half-width* 40))
  ;;; right vertical arrangement
  
  ;; align top right area on left positions
  (defParameter *mw-dialog-v* 
    (+ *mw-large-list-v*
       *mw-left-inter-area-v*
       *mw-large-list-v*
       *mw-left-inter-area-v*
       *mw-small-list-v*
       ))
  )

;;;================ Functions to compute positions and sizes =========================

;(eval-when (compile load eval)
(defUn compute-count-position (side area-number)
  "compute where the count should be give its side and its sequence number.
Arguments:
   side: :left or :right
   area-number: 1, 2, 3, o? 4 (left), 1, 2 (right)
Return:
   a list (hh vv)"
  (case side
    (:left
     (case area-number
       ((1 2 3)
        (list *mw-side-margin*
              (+ *mw-top-margin*
                 *mw-additional-offset-v*
                 (* (1- area-number)
                    (+ *mw-large-list-v* 
                       *mw-left-inter-area-v*)))))
       (4 
        (list *mw-side-margin*
              (+ *mw-top-margin* *mw-additional-offset-v*
                 (+ (* 2 (+ *mw-large-list-v* 
                            *mw-left-inter-area-v*))
                    (+ *mw-small-list-v*
                       *mw-left-inter-area-v*)))))
       (otherwise (error "bad arguments. side: ~S area-number: ~S" side area-number))))
    (:right
     (case area-number
       (1
        (list  (+ *mw-half-width* *mw-half-center-margin*)
              (+ *mw-top-margin*  *mw-additional-offset-v*)))
       (2
        (list  (+ *mw-half-width* *mw-half-center-margin*)
              (+ *mw-top-margin* 4  ; height adjustment
                 (+ (* 2 (+ *mw-large-list-v* 
                            *mw-left-inter-area-v*))
                    (+ *mw-small-list-v*
                       *mw-left-inter-area-v*)))))))
    (otherwise 
     (error "bad arguments. side: ~S area-number: ~S" side area-number))))

#|
? (compute-count-position :left 1)
(4 2)
(compute-count-position :right 1)
(502 2)
(compute-count-position :left 4)
(4 489)
(compute-count-position :right 2)
(502 489)
|#
  
  
(defUn compute-title-position (side area-number)
  "compute where the title should be, given its side and its sequence number.
Arguments:
   side: :left or :right
   area-number: 1, 2, 3, or 4 (left), 1, 2 (right)
Return:
   a list (hh vv)"
  (let ((count-position (compute-count-position side area-number)))
    (list (+ (car count-position) *mw-left-title-h*)
          (cadr count-position))))
  
#|
? (compute-title-position :left 2)
(50 190)
? (compute-title-position :right 1)
(542 2)
|#
  
(defUn compute-area-position (side area-number)
  "compute where the title should be give its side and its sequence number.
Arguments:
   side: :left or :right
   area-number: 1, 2, 3, or 4 (left), 1, 2 (right)
Return:
   a list (hh vv)"
  (let ((count-position (compute-count-position side area-number)))
    (case side
      (:left
       (list (car count-position)
             (+ (cadr count-position) 
                (- *mw-left-inter-area-v* *mw-additional-offset-v*))))
      ;; for some reason this stupid system does display editable items as the other ones
      ;; we must correct by decreasing the size of the area and moving the origin (2 1)
      (:right
       (list (car count-position)
             (+ (cadr count-position) 
                (- *mw-left-inter-area-v* 5)))))))
  
  ;(make-point 2  (- *mw-left-inter-area-v* *mw-additional-offset-v* 5))))))
  
#|
? (compute-area-position :left 2)
(10 210)
|#
  
(defUn compute-area-size (side area-number)
  (case side
    (:left
     (case area-number
       ((1 2) (list *mw-left-width* *mw-large-list-v*))
       ((3 4) (list *mw-left-width* *mw-small-list-v*))
       (otherwise (error "bad area-number: ~S" area-number))))
    (:right
     (case area-number
       (1 #-MCL (list *mw-right-width* *mw-dialog-v* )
        #+MCL (list *mw-right-width* (- *mw-dialog-v* 2)))
       (2 (list *mw-left-width* *mw-small-list-v*))))
    (otherwise
     (error "bad arguments. side: ~S area-number ~S" side area-number))))
  
#|
? (compute-area-size :left 2)
(488 156)
(compute-area-size :left 4)
(494 83)
(compute-area-size :right 2)
(494 83)
|#
  
(defUn compute-button-position (side area-number button-number)
  "buttons are numbered RIGHT TO LEFT and are right-justified
Arguments:
   side: :left or right
   area-number: 1, 2, 3, or 4 (left), 1, 2 (right)
   button-position: 1, 2, 3, ... counting from right to left"
  (let ((pos (compute-count-position side area-number))
        delta)
    (setq delta (- *mw-left-width* 
                   *mw-half-center-margin*
                   (* button-number *mw-command-button-width*)
                   (* (1- button-number) *mw-inter-button-h*)))
    (list (+ (car pos) delta) (cadr pos))))
  
#|
? (compute-button-position :left 2 2)
(344 190)
|#
  
;  ) ; end eval-when

;(point-string (compute-title-position :left 4))

;;; Here we define functions to handle the dialog item areas of the master's 
;;; window
;;; This has two advantages:
;;;  it reduces the length of the make-master-window function
;;;  the position and aspect of the various areas are factored out in the macros 
;;;    and can easily be changed

;;;-------------------------------------------------------------------- MAKE-TITLE

(defParameter *assistant-label-font*
  (cg:make-font-ex NIL "Tahoma / ANSI" 11 NIL)
  )

(defMacro make-title (side area-number text)
  ;; when side is :left then there are 4 areas 
  `(let ((pos (compute-title-position ,side ,area-number)))
     (cr-static-text
      :left (car pos)
      :top (cadr pos)
      :width ,*mw-title-width*
      :height ,*mw-title-height*
      :title ,text
      :font *assistant-label-font*
      )
     ))

#|
(make-title :left 1 "albert")
(LET ((POS (COMPUTE-TITLE-POSITION :LEFT 1)))
  (MAKE-INSTANCE 'CG.STATIC-TEXT:STATIC-TEXT :TOP (CADR POS) :LEFT (CAR POS)
    :HEIGHT 18 :WIDTH 235 :VIEW-FONT *assistant-label-font* :VALUE "albert"))
|#
;;;------------------------------------------------------------- MAKE-COUNT-BUTTON

#|
(defMacro make-count-button (side area-number text nick-name)
  ;; side is a key, area-number an integer, text a text and nick-name a key
  ;; we do not need quotes inside the macro...
  `(let ((pos (compute-count-position ,side ,area-number)))
     ;(format t "~&...Ref: ~S side: ~S area: ~S"  pos ,side ,area-number)
     (cr-button
      :class 'OMAS-BUTTON
      :left (car pos)
      :top #-MCL (cadr pos) #+MCL (+ (cadr pos) 20) ; adjustment
      :width ,*mw-button-width*
      :height ,*mw-button-height*
      :title ,text
      :font *assistant-label-font*
      :name ,nick-name)))
|#

#|
? (make-count-button :left 1 "0" :ae-count)
;; expands to:
(MAKE-DIALOG-ITEM
 'OMAS-BUTTON
 (MAKE-POINT (MAX (- (OR 10 3) 3) 0) (OR 2 0))
 (MAKE-POINT (OR 70 100) (OR 14 20))
 "0"
 NIL
 :VIEW-NICK-NAME
 :AE-COUNT
 :VIEW-FONT
 *assistant-label-font*)
;; for ACL
(LET ((POS (COMPUTE-COUNT-POSITION :LEFT 1)))
  (FORMAT T "~&...Ref: ~S side: ~S area: ~S" POS :LEFT 1)
  (MAKE-INSTANCE 'OMAS-BUTTON :FONT *assistant-label-font* :LEFT (CAR POS) :TOP (CADR POS)
    :HEIGHT 16 :WIDTH 70 :NAME :AE-COUNT :TITLE "0" :ON-CLICK 'NIL))
|#
;;;--------------------------------------------------------------- MAKE-COUNT-TEXT

(defMacro make-count-text (side area-number text nick-name)
  `(let ((pos (compute-count-position ,side ,area-number)))
     ;(format t "~&...Ref: ~S side: ~S area: ~S"  pos ,side ,area-number)
     (cr-static-text ;(cr-text-item
      :left (car pos)
      :top #-MCL (cadr pos) #+MCL (+ (cadr pos) 4) ; adjustment
      :width 25
      :height ,*mw-button-height*
      :text ,text
      :justification :center
      :font *assistant-label-font*
      :color *white-color*
      
      :name ,nick-name)))

#|
? (make-count-text :left 1 "0" :ae-count)
;; expands to:
(MAKE-INSTANCE
    'EDITABLE-TEXT-DIALOG-ITEM
  :VIEW-POSITION
  (MAKE-POINT (OR 10 0) (OR 2 0))
  :VIEW-SIZE
  (MAKE-POINT (OR 15 100) (OR 14 16))
  :VIEW-NICK-NAME
  :AE-COUNT
  :VIEW-FONT
  *assistant-label-font*
  :WORD-WRAP-P
  NIL
  :ALLOW-RETURNS
  NIL
  :DIALOG-ITEM-TEXT
  "0")
;; for ACL
(LET ((POS (COMPUTE-COUNT-POSITION :LEFT 1)))
  (MAKE-INSTANCE 'CG.EDITABLE-TEXT:EDITABLE-TEXT :LEFT (CAR POS) :TOP (CADR POS)
    :HEIGHT 16 :WIDTH 25 :NAME :AE-COUNT :FONT *assistant-label-font* :VALUE
    "0"))
|#
;;;------------------------------------------------------------------- MAKE-BUTTON

(defMacro make-button (side area-number button-number text fn nick-name)
  `(let ((pos (compute-button-position ,side ,area-number ,button-number)))
     ;(format t "~&...Button: ~S side: ~S area: ~S # ~S" 
     ;       (point-string pos) side area-number button-number)
     (cr-button 
      :class 'OMAS-BUTTON
      :left (car pos)
      :top (cadr pos)
      :width ,*mw-button-width*
      :height ,*mw-button-height*
      :title ,text
      :on-click ,fn
      :font *assistant-label-font*
      :name ,nick-name
      )))

#|
(make-button :left 1 2 "youppee..." my-function :test)
;; expands to:
(MAKE-DIALOG-ITEM
 'OMAS-BUTTON
 (MAKE-POINT (MAX (- (OR 344 3) 3) 0) (OR 2 0))
 (MAKE-POINT (OR 70 100) (OR 14 20))
 "youppee..."
 #'(LAMBDA (ITEM) (MY-FUNCTION (VIEW-CONTAINER ITEM) ITEM))
 :VIEW-NICK-NAME
 :TEST
 :VIEW-FONT
 *assistant-label-font*)
;; for ACL
(LET ((POS (COMPUTE-BUTTON-POSITION :LEFT 1 2)))
  (MAKE-INSTANCE 'OMAS-BUTTON :FONT *assistant-label-font* :LEFT (CAR POS) :TOP (CADR POS)
    :HEIGHT 16 :WIDTH 70 :NAME :TEST :TITLE "youppee..." :ON-CLICK
    'MY-FUNCTION))
|#
;;;---------------------------------------------------------------- MAKE-LIST-AREA

(defMacro make-list-area (side area-number text fn table-fn nick-name)
  `(let ((pos (compute-area-position ,side ,area-number))
         (size (compute-area-size ,side ,area-number)))
     ;(format t "~&...Areas: ~S side: ~S area: ~S # ~S" 
     ;                pos ,side ,area-number  size)
     (cr-multi-item-list 
      :class 'area-for-values
      :left (car pos)
      :top (cadr pos)
      :cell-width (car size)
      :height (cadr size)
      :title ,text
      :on-double-click ,fn
      :cell-width 480
      :cell-height 15
      :hscrollp nil
      :vscrollp t
      :selection-type :disjoint
      :name ,nick-name
      ,@(if table-fn `(:sequence ,table-fn)))))

#|
(make-list-area :left 1 "area 1" my-function () :test)
;; expands to:
(MAKE-DIALOG-ITEM
 'AREA-FOR-VALUES
 (MAKE-POINT (MAX (- (OR 10 3) 3) 0) (OR 30 0))
 (MAKE-POINT (OR 480 100) (OR 156 32))
 "area 1"
 #'(LAMBDA (ITEM) (IF (DOUBLE-CLICK-P) (MY-FUNCTION (VIEW-CONTAINER ITEM) ITEM)))
 :VIEW-NICK-NAME
 :TEST
 :CELL-SIZE
 (MAKE-POINT (OR 480 488 100) (OR 15 16))
 :SELECTION-TYPE
 :DISJOINT
 :TABLE-HSCROLLP
 NIL
 :TABLE-VSCROLLP
 T
 :TABLE-SEQUENCE
 NIL)
;; for ACL
(LET ((POS (COMPUTE-AREA-POSITION :LEFT 1)) (SIZE (COMPUTE-AREA-SIZE :LEFT 1)))
  (MAKE-INSTANCE 'AREA-FOR-VALUES :LEFT (CAR POS) :TOP (CADR POS) :WIDTH
    (COND ((CAR SIZE)) (T 480)) :HEIGHT (CADR SIZE) :NAME :TEST
    :ON-DOUBLE-CLICK 'MY-FUNCTION :RANGE NIL :TITLE "area 1"))

(make-list-area :left 1 "area 1" my-function (gg agent) :test)

(MAKE-DIALOG-ITEM
 'AREA-FOR-VALUES
 (MAKE-POINT (MAX (- (OR 10 3) 3) 0) (OR 30 0))
 (MAKE-POINT (OR 480 100) (OR 156 32))
 "area 1"
 #'(LAMBDA (ITEM) (IF (DOUBLE-CLICK-P) (MY-FUNCTION (VIEW-CONTAINER ITEM) ITEM)))
 :VIEW-NICK-NAME
 :TEST
 :CELL-SIZE
 (MAKE-POINT (OR 480 488 100) (OR 15 16))
 :SELECTION-TYPE
 :DISJOINT
 :TABLE-HSCROLLP
 NIL
 :TABLE-VSCROLLP
 T
 :TABLE-SEQUENCE
 (GG AGENT))
|#
;;;-------------------------------------------------------- MAKE-INPUT-DIALOG-AREA

(defMacro make-input-dialog-area (side area-number nick-name fn text)
  `(let ((pos (compute-area-position ,side ,area-number))
         (size (compute-area-size ,side ,area-number)))
     ;(size (- (compute-area-size side area-number)
     ;         (make-point 6 6))))
     ;(format t "~&...Areas: ~S side: ~S area: ~S # ~S" pos side area-number size)
     (cr-text-item  
      :class 'OMAS-dialog-item
      :left (car pos) 
      :top (cadr pos)
      :width (car size)
      :height (cadr size)
      :text ,text
      :word-wrap t
      ;; this is for ACL only, MCL uses a key handler
      :on-change ,fn
      :allow-returns t
      :name ,nick-name
      :font *assistant-label-font*)))

#|
? (make-input-dialog-area :left 1 :list mw-input-on-change "so what?")
;; expands to:
(LET ((POS (COMPUTE-AREA-POSITION :LEFT 1)) (SIZE (COMPUTE-AREA-SIZE :LEFT 1)))
  (CR-TEXT-ITEM :CLASS
                'OMAS-DIALOG-ITEM
                :LEFT
                (CAR POS)
                :TOP
                (CADR POS)
                :WIDTH
                (CAR SIZE)
                :HEIGHT
                (CADR SIZE)
                :NO-OUTLINE
                T
                :TEXT
                "so what?"
                :WORD-WRAP
                T
                :ALLOW-RETURNS
                T
                :NAME
                :LIST
                :FONT
                *ASSISTANT-LABEL-FONT*))
;; for ACL
(LET ((POS (COMPUTE-AREA-POSITION :LEFT 1)) (SIZE (COMPUTE-AREA-SIZE :LEFT 1)))
  (MAKE-INSTANCE 'CG.MULTI-LINE-EDITABLE-TEXT:MULTI-LINE-EDITABLE-TEXT :LEFT
    (CAR POS) :TOP (CADR POS) :HEIGHT (CADR SIZE) :WIDTH (CAR SIZE)
    :NAME :LIST :FONT *assistant-label-font* :NOTIFY-WHEN-SELECTION-CHANGED T
    :ON-CHANGE 'MW-INPUT-ON-CHANGE :VALUE "so what?"))
|#
;;;------------------------------------------------------- MAKE-OUTPUT-DIALOG-AREA

(defMacro make-output-dialog-area (side area-number nick-name)
  `(let ((pos (compute-area-position ,side ,area-number))
         (size (compute-area-size ,side ,area-number)
               ))
     ;(size (- (compute-area-size side area-number)
     ;         (make-point 6 6))))
     ;(format t "~&...Areas: ~S side: ~S area: ~S # ~S" pos side area-number size)
     (cr-text-item  
      :class 'cg:rich-edit
      :left (car pos) 
      :top (cadr pos)
      :width (car size)
      :height (cadr size)
      :text ""
      :word-wrap t
      :allow-returns t
      ;:H-SCROLLP nil
      :NAME ,nick-name
      :font *assistant-label-font*)))

#|
? (make-output-dialog-area :left 1 :list)
;; expands to:
(MAKE-INSTANCE
    'SCROLLING-FRED-VIEW
  :VIEW-POSITION
  (MAKE-POINT (OR 10 0) (OR 30 0))
  :VIEW-SIZE
  (MAKE-POINT (OR 488 100) (OR 156 16))
  :VIEW-NICK-NAME
  :LIST
  :VIEW-FONT
  *assistant-label-font*
  :WORD-WRAP-P
  T
  :ALLOW-RETURNS
  T
  :DIALOG-ITEM-TEXT
  "")
;; for ACL
(LET ((POS (COMPUTE-AREA-POSITION :LEFT 1)) (SIZE (COMPUTE-AREA-SIZE :LEFT 1)))
  (MAKE-INSTANCE 'CG.MULTI-LINE-EDITABLE-TEXT:MULTI-LINE-EDITABLE-TEXT :LEFT
    (CAR POS) :TOP (CADR POS) :HEIGHT (CADR SIZE) :WIDTH (CAR SIZE)
    :NAME :LIST :FONT *assistant-label-font* :VALUE ""))
|#

;;;===================================================================================
;;;
;;;                       WINDOW and METHODS
;;;
;;;===================================================================================


(defMethod display-assistant-window ((agent agent) &aux win)
  "we build and display a window for the dialog with the master.
Arguments:
   agent: agent."
  (setq win (make-master-window agent))
  ;; record window name in agent
  (setf (com-window agent) win)
  ;; initialize master-window
  ;(MW-refresh agent win)
  :done)

;;;===============================================================================
;;;                     MASTER/ASSISTANT WINDOW INTERFACE
;;;===============================================================================

;;; We define a panel for OMAS assistants

(defClass omas-assistant-panel (cg:dialog)
  ((agent :accessor agent)
   (answer-to-task-to-do :accessor answer-to-task-to-do :initform nil)
   (pass-every-char :accessor pass-every-char :initform nil)
   ;(user-input :accessor user-input :initform nil) ; JPB 1009
   )
  (:documentation
   "Defines the format and behavior of the?OMAS ASSISTANT PANEL"))

;;; adding MOSS methods for handling dialogs

;;;----------------------------------- (OMAS-ASSISTANT-PANEL) MOSS::ACTIVATE-INPUT

(defMethod moss::activate-input ((win omas-assistant-panel) &key erase)
  "prints a text into the output pane of the window.
Arguments:
   erase (key): if true erase the content, otherwise select it
Return: unimportant."
  (let ((input-pane (%find-named-item :master win)))
    ;; activate the user panel
    ;; the user will enter its data that will be transferred into the to-do
    ;; slot, waking up the resume process
    (%select-window win)
    ;; erase pane unless we want to display the whole dialog history
    (unless (show-dialog (agent win))
      (moss::display-text win "" :erase erase))
    ;; activate the master pane
    ;(%select-pane win (%find-named-item :master win))
    (%select-pane win input-pane)
    ;; and clear the input pane JPB1110
    (%set-value input-pane "")
    ))

;;;------------------------------------- (OMAS-ASSISTANT-PANEL) MOSS::DISPLAY-TEXT
;;; we should add to the window a property registering how many lines there are in
;;; the assistant pane (depending on the haight of the font) so that we can scroll
;;; to keep newly printed stuff in view.

(defMethod moss::display-text ((win omas-assistant-panel) text &key erase newline
                               final-new-line (header "") clean-pane 
                               (color moss::*black-color*)
                               &allow-other-keys)
  "prints a text into the output pane of the assistant panel.
Arguments:
   text: text to display (must be a simple string)
   clean-pane (key): erase the assistant pane in all cases
   erase (key): if t, erase previous text, otherwise append to it
   final-new-line (key): if t, add newline command at the end of text
   header (key): a string that will be printed in front of the text 
                 (default \"\")
   newline (key): it t, add a new line in front of the header
Return: 
   nil."
  (let* ((output-pane (%find-named-item :assistant win))
         #+MICROSOFT-32 (output-window (cg:window output-pane))
         (agent (agent win))
        nb-of-lines)
    (unless (and (stringp text)(stringp header))
      (error "text ~S and header ~S should be strings." text header))
    
    (if final-new-line (setq text (concatenate 'string text "~%")))
    (if newline (setq header (concatenate 'string "~%" header)))
    
    ;; because text or header could contain format directives, we process it
    (setq text (format nil (concatenate 'string header text)))
    
    ;(moss::vformat "OMAS-ASSISTANT-WINDOW display-text /erase: ~S" erase)
    ;; record  the number of lines
    #+MICROSOFT-32
    (setq nb-of-lines (cg:number-of-text-lines output-pane))
    ;(print nb-of-lines)
    
    (when clean-pane
      (%set-value output-pane "")
      (return-from moss::display-text))
    
    (format t "~% assistant moss::display-text /text: ~S" text)

    (if (and erase (not (show-dialog (agent win))))
      (setf (cg:rich-edit-range output-window) 
        (cg:plain-to-rich-text (format nil text) 
                               (or (output-font-string agent)
                                   (cg:font-face (cg:font (cg:screen cg:*system*))))
                               (output-font-size agent) :color cg:red))
      
      ;; add more text
      (setf (cg:rich-edit-range output-window)
        (cg:concatenate-rich-text 
         (cg:rich-edit-range output-window)
         ;(cg:plain-to-rich-text (format nil text) "Tahoma / ANSI" *output-font-size*
         (cg:plain-to-rich-text (format nil text) 
                                (or (output-font-string agent)
                                   (cg:font-face (cg:font (cg:screen cg:*system*))))
                                (output-font-size agent) :color color)))
      )
    ;(print (cg:rich-edit-range (cg:window output-pane)))
    ;; should scroll the pane so that we see the new values, i.e. so that the first
    ;; added line is now the top line
    (setq nb-of-lines (max 0 (- nb-of-lines 15)))
    ;(print nb-of-lines)
    (cg:set-first-visible-line output-pane nb-of-lines)
    nil))

#|
(moss::display-text (com-window albert::PA_albert) "... encore du texte")
(moss::display-text (com-window albert::PA_albert) "... encore du texte"
                    :header "~%Master> ")
|#
;;;------------------------------------------------------------ MAKE-MASTER-WINDOW

;(setf (com-window jp) (omas::make-master-window stevens::stevens))
; (setf (com-window albert::albert)(omas::make-master-window albert::albert))
#|
(omas::make-master-window albert::albert)
|#

(defUn make-master-window (agent)
  "define the master's interface window, capable of handling dialogs and viewing ~
            various types of info like the contents of the master's bins."
  (let ((font (output-font-string agent))
        win master-pane)
    ;; font is non nil if defassistant contained a font option
    (setq win
          (cr-window 
           :class 'omas-assistant-panel
           :type :document
           :font 
           (if font
                     (cg:make-font-ex :swiss font (output-font-size agent))
                   ;; otherwise we take the system font
                   (cg:font (cg:screen cg:*system*)))
           :title (symbol-name (name agent))
           :left (car *mw-position*)
           :top (cadr *mw-position*)
           :width #+MCL *mw-inside-width* #-MCL *mw-width*
           :height *mw-height*
           :background-color *master-window-color*
           :font *master-window-font*
           :subviews (make-master-widgets agent)
           ))
    ;; record agent
    (setf (agent win) agent)
    ;; select text from the master pane
    (setq master-pane (%find-named-item :master win))
    ;; select text
    (%select-all master-pane)
    ;; select pane
    (%select-pane win master-pane)
    ;; record windo into agent
    (setf (com-window agent) win)
    ;; return window object, useless if several PAs compete
    (setq *master-window* win)
    ))

;;;----------------------------------------------------------- MAKE-MASTER-WIDGETS
; (make-master-widgets albert::albert)
; (setq agent albert::albert)
; (omas::make-master-window albert::albert)
(defUn make-master-widgets (agent)
  (declare (special *agent*))
  (setq *agent* agent)
  (list   
   ;;---------------------------------------------------------- answers to examine
   ;; List of answers to be processed by the master
   (make-title :left 1 (mln::extract-to-string *WASS-ae*)) ; jpb 1406
   (make-count-text :left 1 (format nil "~S" (length (waiting-answers agent)))
                    :ae-count)
   (make-button :left 1 3 (mln::extract-to-string *WASS-examine*)
                MW-AE-Examine-on-click :AE-EXAMINE) 
   (make-button :left 1 2 (mln::extract-to-string *WASS-discard*)
                MW-AE-discard-on-click :AE-DISCARD)
   (make-button :left 1 1 (mln::extract-to-string *WASS-save*)
                MW-AE-save-on-click :AE-SAVE)
   
   (make-list-area :left 1 " " MW-AE-list-on-double-click  
                   (MW-AE-list-initial-sequence agent) :AE-LIST)
   
   ;;----------------------------------------------------------------- tasks to do
   (make-title :left 2 (mln::extract-to-string *WASS-td*)) ; jpb 1406
   (make-count-text :left 2 (format nil "~S" (length (waiting-messages agent)))
                    :TD-count)
   (make-button :left 2 3 (mln::extract-to-string *WASS-examine*)  ; jpb 1406
                MW-TD-Examine-on-click :TD-EXAMINE)
   (make-button :left 2 2 (mln::extract-to-string *WASS-discard*) ; jpb 1406
                MW-TD-discard-on-click :TD-DISCARD)
   (make-button :left 2 1 (mln::extract-to-string *WASS-process*) ; jpb 1406
                MW-TD-process-on-click :TD-PROCESS)
   
   (make-list-area :left 2 " " MW-TD-list-on-double-click
                   (MW-TD-list-initial-sequence agent) :TD-LIST)
   
   ;;------------------------------------------------------------ pending requests   
   ;; this is a list of tasks sent by the master that have not yet received
   ;; an answer
   (make-title :left 3 (mln::extract-to-string *WASS-pr*)) ; jpb 1406
   (make-count-TEXT :left 3  (format nil "~S" (length (pending-requests agent)))
                    :PR-COUNT)
   (make-button :left 3 3 (mln::extract-to-string *WASS-examine*) ; jpb 1406
                MW-PR-Examine-on-click :PR-EXAMINE)
   (make-button :left 3 2 (mln::extract-to-string *WASS-discard*) ; jpb 1406
                MW-PR-Discard-on-click :PR-DISCARD)
   
   (make-list-area :left 3 " " MW-PR-list-on-double-click
                   (MW-PR-list-initial-sequence agent) :PR-LIST)
   
   ;;---------------------------------------------------------- discarded messages
   (make-title :left 4 (mln::extract-to-string *WASS-dm*)) ; jpb 1406
   (make-count-text :left 4 (format nil "~S" (length (discarded-messages agent)))
                    :DM-count)
   (make-button :left 4 3 (mln::extract-to-string *WASS-examine*) ; jpb 1406
                MW-DM-Examine-on-click :DM-EXAMINE)
   (make-button :left 4 2 (mln::extract-to-string *WASS-discard*) ; jpb 1406
                MW-DM-discard-on-click :DM-DISCARD)
   (make-button :left 4 1 (mln::extract-to-string *WASS-revive*) ; jpb 1406
                MW-DM-revive-on-click :DM-REVIVE)
   
   (make-list-area :left 4 " " MW-DM-list-on-double-click
                   (MW-DM-list-initial-sequence agent) :DM-LIST)
   
   ;;------------------------------------------------------------------- assistant
   (make-title :right 1 (mln::extract-to-string *WASS-a*))
   (make-button :right 1 1 (mln::extract-to-string *WASS-clear*)
                MW-A-clear-on-click :A-CLEAR)
   (make-button :right 1 3 (mln::extract-to-string *WASS-a-switch*)
                MW-A-switch-on-click :A-SMALL-WINDOW)
   (let ((pane (make-output-dialog-area :right 1 :ASSISTANT)))
     #+MOSS (setq moss::*moss-output* pane)
     pane)
   ;;---------------------------------------------------------------------- master
   (make-title :right 2 (mln::extract-to-string *WASS-m*)) ; jpb 1406
   (make-button :right 2 1 (mln::extract-to-string *WASS-clear*) ; jpb 1406
                MW-M-clear-on-click :M-CLEAR)
   (make-button :right 2 2 (mln::extract-to-string *WASS-done*) ; jpb 1406
                MW-M-done-on-click :M-DONE)
   (make-input-dialog-area :right 2 :MASTER mw-input-on-change 
                           (mln::extract-to-string *WASS-m-msg*)) ; jpb 1406
   ;---
   ))
#|
(make-instance 'cg:MULTI-LINE-EDITABLE-TEXT 
  :delayed nil 
  :FONT (cg:make-font-ex NIL "Tahoma / ANSI" 11 NIL)
  :HEIGHT 100 :LEFT 435 :NAME :MASTER 
  :notify-when-selection-changed t :on-change 'mw-input-on-change
  :TEMPLATE-STRING NIL :TOP 500 :UP-DOWN-CONTROL NIL 
  :VALUE (mln::extract-to-string *WASS-m-msg*) ; jpb 1406
  :WIDTH 400)
(LET ((POS (COMPUTE-AREA-POSITION :RIGHT 2))
      (SIZE (COMPUTE-AREA-SIZE :RIGHT 2)))
  (MAKE-INSTANCE 'CG.MULTI-LINE-EDITABLE-TEXT:MULTI-LINE-EDITABLE-TEXT :LEFT
    (CAR POS) :TOP (CADR POS) :HEIGHT (CADR SIZE) :WIDTH (CAR SIZE)
    :NAME :MASTER :FONT *assistant-label-font* :NOTIFY-WHEN-SELECTION-CHANGED
    T :ON-CHANGE 'MW-INPUT-ON-CHANGE :UP-DOWN-CONTROL NIL :VALUE
    (mln::extract-to-string *WASS-M-MSG*)))
|#
;;;-------------------------------------------------------------------- USER-CLOSE

(defMethod user-close ((win omas-assistant-panel))
  "when closing the window we should remove agent pointer"
  (setf (com-window (agent win)) nil)
  (call-next-method))


;;;===============================================================================
;;;
;;;                            EVENT FUNCTIONS
;;;
;;;===============================================================================

;;;===============================================================================
;;;                             ASSISTANT PANE
;;;===============================================================================

;;;----------------------------------------------------------- MW-A-CLEAR-ON-CLICK

(defUn mw-a-clear-on-click (dialog widget)
  "clears the assistant answer pane"
  (declare (ignore widget))
  (moss::display-text dialog "" :clean-pane t)
  ;(%set-value (%find-named-item :assistant dialog) "")
  t)
  
;;;---------------------------------------------------------- MW-A-SWITCH-ON-CLICK
;;; rich edit text obliges to do some acrobatics...

(defun mw-a-switch-on-click (dialog widget)
  "switches to a small interaction window"
  (declare (ignore widget))
  (let ((agent (agent dialog))
        (pane (%find-named-item :assistant dialog))
        text win output-window conversation new-pane)
    ;; get the rich text actual pane
    (setq output-window (cg:window pane))
    ;; copy assistant history
    (setq text (cg:rich-edit-range output-window))
    ;; then close assistant current window
    (%window-close dialog)
    ;; create small window
    (setq win (make-PA-small-window agent))
    ;; make it the com-window
    (setf (com-window agent) win)
    ;; get the assistant pane of the new window
    (setq new-pane (%find-named-item :assistant win))
    ;; copy history text
    (setf (cg:rich-edit-range (cg:window new-pane)) text)
    ;; since agent is a PA, update the I/O dialog channels
    ;; get the conversation object
    (if (member :converse-v2 *features*)
        (setq conversation (cadr (assoc (key agent) (conversation agent))))
      (setq conversation (conversation agent)))
    ;; update I/O channels
    (send conversation '=replace "moss-input-window" win)
    (send conversation '=replace "moss-output-window" win)
    ;; get out
    t
    ))
	
;;;===============================================================================
;;;                            end assistant pane
;;;===============================================================================

;;;===============================================================================
;;;                            ANSWERS TO EXAMINE
;;;===============================================================================

;;;-------------------------------------------------------- MW-AE-DISCARD-ON-CLICK

(defUn mw-ae-discard-on-click (dialog widget)
  "discard the answer that has been selected in the answers to examine bin. An ~
   answer is a message object."
  (declare (ignore widget))
  (let ((agent (agent dialog))
        (pane (%find-named-item :ae-list dialog))
        pos)
    (unless (or (null (waiting-answers agent))
                (assistant-complain-if-nothing-selected agent pane))
      ;; get position of selected item
      (setq pos (%get-selected-cell-index pane))
      (if (null pos) (return-from mw-ae-discard-on-click nil))
      ;; remove corresponding message from the message list
      (setf (waiting-answers agent)(remove-at-pos pos (waiting-answers agent)))
      ;; and remove corresponding string from range
      (%set-item-sequence pane (remove-at-pos pos (%get-item-sequence pane)))
      ;; if resulting list is empty, then insert special text
      (unless (%get-item-sequence pane)
        (%set-item-sequence pane (list (mln::extract-to-string *WASS-ae-msg*)))) ; jpb 1406
      ;; update count
      (%set-value (%find-named-item :ae-count dialog)
                  (format nil "~A" (length (waiting-answers agent))))
      ))
  t)

;;;-------------------------------------------------------- MW-AE-EXAMINE-ON-CLICK

(defUn mw-ae-examine-on-click (dialog widget)
  "examine the answer that has been selected in the answers to examine bin. The ~
   answer is a message, and we should display the sender, date, content in the ~
   assistant area."
  (declare (ignore widget))
  (let ((agent (agent dialog))
        (pane (%find-named-item :ae-list dialog))
        pos message)
    (unless (or (null (waiting-answers agent))
                (assistant-complain-if-nothing-selected agent pane))
      ;; get position of selected item
      (setq pos (%get-selected-cell-index pane))
      ;; and message
      (setq message (nth pos (waiting-answers agent)))
      ;; print a detailed version of message into assistant pane
      (assistant-detail-answer-to-examine (agent dialog) message)
      ;; save message into the FACTS/message area
      (replace-fact (conversation agent) :message message)
      ))
  t)

;;;--------------------------------------------------- MW-AE-LIST-INITIAL-SEQUENCE

(defUn mw-ae-list-initial-sequence (agent)
  (if (waiting-answers agent)
      (mapcar #'assistant-answer-summary (waiting-answers agent))
    (list (mln::extract-to-string *WASS-ae-msg*)))) ; jpb 1406

;;;---------------------------------------------------- MW-AE-LIST-ON-DOUBLE-CLICK

(defUn mw-ae-list-on-double-click (dialog widget) 
  "posts a list of waiting answers on double-click"
  (let ((agent (agent dialog))
        (index (%get-selected-cell-index widget))
        message)
    (when index
      (setq message (nth index (waiting-answers agent)))
      (if message
          (assistant-detail-answer-to-examine agent message)
        (%beep)))
    (unless index (%beep)))
  t)

;;;----------------------------------------------------------- MW-AE-SAVE-ON-CLICK
;;; save where? in agent's memory? permanently?
;;; this should trigger a dialog asking for some contextual info and/or keywords
;;; or be linked with the Kejia's CB

(defUn mw-ae-save-on-click (dialog widget)
  "Currenly does nothing. Should save the selected message somewhere in the agent ~
   memory"
  (declare (ignore dialog widget))
  t)

;;;===============================================================================
;;;                         end answers to examine
;;;===============================================================================

;;;------------------------------------------------------------ MW-ASSISTANT-HERE?

(defUn mw-assistant-here? (dialog)
  "check for the presence of the assistant"
  (or (agent dialog)
      (progn 
        (%beep) ; always returns T
        (warn "your assistant is not available!")
        nil)))

;;;===============================================================================
;;;                            DISCARDED MESSAGES
;;;===============================================================================

;;; the discarded-message list contains items (<message> . <process>) where process
;;; may be nil (e.g. for :tell messages or answer messages)
;;; <process> corresponds to the waiting process for an :ask message

;;;-------------------------------------------------------- MW-DM-DISCARD-ON-CLICK

(defUn mw-dm-discard-on-click (dialog widget)
  "remove a message from the list. If no message is selected complain."
  (declare (ignore widget))
  (let ((agent (agent dialog))
        (pane (%find-named-item :dm-list dialog))
        pos)
    (unless (or (null (discarded-messages agent))
                (assistant-complain-if-nothing-selected agent pane))
      ;; get position of selected item
      (setq pos (%get-selected-cell-index pane))
      ;; remove corresponding message from the message list
      (setf (discarded-messages agent)
        (remove-at-pos pos (discarded-messages agent)))
      ;; and remove corresponding string from range
      (%set-item-sequence pane (remove-at-pos pos (%get-item-sequence pane)))
      ;; if resulting list is empty, then insert special text
      (unless (%get-item-sequence pane)
        (%set-item-sequence pane (mln::extract *WASS-dm-msg*))) ; jpb 1406
      ;; update count
      (%set-value (%find-named-item :dm-count dialog)
                  (format nil "~A" (length (discarded-messages agent))))
      ))
  t)

;;;-------------------------------------------------------- MW-DM-EXAMINE-ON-CLICK

(defUn mw-dm-examine-on-click (dialog widget)
  "examine the message that has been selected in the discarded messages bin."
  (declare (ignore widget))
  (let ((agent (agent dialog))
        (pane (%find-named-item :dm-list dialog))
        pos)
    (unless (or (null (discarded-messages agent))
                (assistant-complain-if-nothing-selected agent pane))
      ;; get position of selected item
      (setq pos (%get-selected-cell-index pane))
      ;; print a detailed version of message into assistant pane
      (assistant-detail-discarded-message (agent dialog) 
                                          (nth pos (discarded-messages agent)))
      ))
  t)

;;;--------------------------------------------------- MW-DM-LIST-INITIAL-SEQUENCE

(defUn mw-dm-list-initial-sequence (agent)
  (if (discarded-messages agent)
      (mapcar #'assistant-answer-summary (discarded-messages agent))
    (list (mln::extract-to-string *WASS-dm-msg*)))) ; jpb 1406

;;;---------------------------------------------------- MW-DM-LIST-ON-DOUBLE-CLICK

(defUn mw-dm-list-on-double-click (dialog widget) 
  "posts a list of waiting answers on double-click"
  (let ((agent (agent dialog)))
    (assistant-detail-discarded-message
     agent
     (nth (%get-selected-cell-index widget) (discarded-messages agent))))
  t)

;;;--------------------------------------------------------- MW-DM-REVIVE-ON-CLICK

(defUn mw-dm-revive-on-click (dialog widget)
  "revive a discarded message putting it into the right bin."
  (let ((agent (agent dialog))
        (pane (%find-named-item :dm-list dialog))
        pos item)
    (unless (or (null (discarded-messages agent))
                (assistant-complain-if-nothing-selected agent pane))
      ;; get position of selected item
      (setq pos (%get-selected-cell-index pane))
      ;; get message
      (setq item (nth pos (discarded-messages agent)))
      ;; move it into the ad hoc bin
      (case (getf (content (car item)) :action)
        (:ask
         ;; add to tasks to do
         (assistant-add-task-to-do agent item)
         )
        (:otherwise
         ;; add to answers to examine
         (assistant-add-answer-info agent item)
         )
        )
      ;; remove it from the discarded messages bin
      (mw-dm-discard-on-click dialog widget) 
      ))
  t)

;;;===============================================================================
;;;                            end discarded messages
;;;===============================================================================

;;;===============================================================================
;;;                                MASTER PANE
;;;===============================================================================

;;;------------------------------------------------------------ MW-INPUT-ON-CHANGE
;;; does not seem to work with multiple-line-editable-text
;;; this function is done by the key handler associated with the pane in MCL

(defUn mw-input-on-change (item new-value old-value)
  "Called whenever the content of the input pane (a string) changes.
   We check whether the new char is a period or a question mark 
   if so we terminate input and process it."
  ;(declare (ignore old-value))
  ;(format t "~&mw-input-on-change /text: ~S old-value: ~S" new-value old-value)
  ;; extract last char and text before the last char, when text is not empty
  (when (> (length new-value) 0) ; just in case (should always be true)
    (let* ((size (length new-value))
           (char (char new-value (1- size)))
           (text (subseq new-value 0 (1- size)))
           (prev (if (> size 1) (char new-value (- size 2)) '#\A))
           (dialog (%container item))
           (pass-all (pass-every-char dialog))
           (agent (agent dialog))
           (language (language agent))
           )
      ;(format t "~&mw-input-on-change /new char: ~S" char)
      ;; analyse situation:
      ;; - if char is nil do nothing
      ;; - if char is question mark selet the text and call process-master-text
      ;;   adding question mark to the text
      ;; - if char is a period, then select the text and call process-master-text
      ;; - if text is a linefeed, beep ??
      ;; - otherwise do nothing.
      (cond 
       ;; when we pass all chars we nevertheless check for "?." ending the text JPB1108
       ((and pass-all
             (char-equal char '#\.)
             (char-equal prev '#\?)
             )
        ;(format t "~&mw-input-on-change /end char: ~S text ~S" char text)
        ;; select text
        (cg:set-selection item 0 (1+ (length text)))
        (assistant-process-master-text (agent dialog) (subseq text 0 (- size 2)))
        )
       ;; otherwise we quit JPB1108
       (pass-all)
       ;; a null char should not appear unless we erase area
       ((null char)) ; do nothing
       ;; if the char is a question mark, insert a space
       ;((char-equal '#\? char)
       ((member char (getf *question-markers* language))
        ;; select text
        (cg:set-selection item 0 (1+ (length text)))
        ;; add a " ?" at the end of the text
        (assistant-process-master-text 
         (agent (cg:parent item)) 
         (format nil "~A ~A" text char)))
       ;(concatenate 'string text " ?")))
       ;; when a period, end of the sentence (ACL does not know #\Enter!)
       ;((char-equal char '#\.)
       ((member char (getf *full-stop-markers* language agent))
        (format t "~%; mw-input-on-change /char: ~S item: ~S" char item)
        ;; select text
        ;(cg:set-selection item 0 (1+ (length text)))
        (%select-all item)
        (assistant-process-master-text (agent (cg:parent item)) text)
        )
       ;; why should we cg:beep on linefeed (because it is not an enter command?)
       ((char-equal '#\Linefeed char)
        (cg:beep))
       )))
  ;; return t to accept changes ?
  t)

;;;----------------------------------------------------------- MW-M-CLEAR-ON-CLICK

(defUn mw-m-clear-on-click (dialog widget)
  "clears the master input pane"
  (declare (ignore widget))
  (let ((pane (%find-named-item :master dialog)))
    (%set-value pane "")
    (%select-pane dialog pane))
  t)

;;;------------------------------------------------------------ MW-M-DONE-ON-CLICK
;;;********** should be reviewed seriously...

(defUn mw-m-done-on-click (dialog widget)
  "ends the input into master pane, calling the process function"
  (declare (ignore widget))
  (let* ((item (%find-named-item :master dialog))
         (text (%get-value item))
         ;; get last char
         (last-char (if (> (length text) 0) (char text (1- (length text)))))
         (short-text (if (>= (1- (length text)) 0) 
                         (subseq text 0 (1- (length text))) ""))
         (agent (agent dialog))
         (language (language agent))
         )
    ;; in case we were inputting a long text from master passing all chars,
    ;; reset flag
    (setf (pass-every-char dialog) nil)
    (cond 
     ;; a null char should not occur unless we erase area
     ((null last-char)) ; do nothing
     ;; if the char is a question mark, insert a space
     ;((char-equal '#\? last-char)
     ((member last-char (getf *question-markers* language))
      ;; select text
      (%select-all item)
      ;; add ? " ?" at the end of the text
      (assistant-process-master-text 
       (agent dialog) 
       (concatenate 'string short-text " ?")))
     ;; when a period end of the sentence, pass the text without it
     ;((char-equal last-char '#\.)
     ((member last-char (getf *full-stop-markers* language))
      ;; select text so the user can overwrite it
      (%select-all item)
      (assistant-process-master-text (agent dialog) short-text))
     ;; otherwise pass the whole text
     (t (assistant-process-master-text (agent dialog) text))
     ))
  t)

;;;===============================================================================
;;;                            end master pane
;;;===============================================================================

;;;------------------------------------------------------------ MW-MESSAGE-SUMMARY

(defUn mw-message-summary (message &key (maxlength 80) &aux)
  "produces a string for displaying a line summary of the messages in the various ~
   panes of the assistant window.
Arguments:
   message: message to display
   maxlength (key): max number of chars to be displayed (default 80)
Return:
   a string."
  (if (typep message 'omas::message)
      (let ((content (car (args message))))
        (message-limit-text-length
         (format nil "~A ~A/ ~A/ ~A ~A/ ~A"
           (date-string (date message))
           (time-string (date message))
           (or (getf content :priority) (mln::extract-to-string *WASS-normal*)) ; jpb 1406
           (mln::extract-to-string *WASS-from*) ; jpb 1406
           (from! message)
           (or (getf content :object) (mln::extract-to-string *WASS-no-object*)) ; jpb 1406
           )
         maxlength))
    ;; otherwise return unprocessed object
    message))

#|
? (in-package :omas)
#<Package "OMAS">
? (defmessage :MSG01 :type :request :from :<USER> :to :albert :action :tell 
:args ((:priority :low :object "test message" :text "This is a test message))
to test the posting function.")))
:MSG01
? (mw-message-summary msg01)
"08/09/11 8:13:39/ LOW/ from: <USER>/ test message"
|#
;;;===============================================================================
;;;                            PENDING REQUESTS
;;;===============================================================================

;;; Pending requests correspond to messages that were sent by the PA on behalf of
;;; its master to other agents and are waiting for an answer. The corresponding
;;; list contains pairs (<message> . <process>) where <process> is the current
;;; waiting process. If a pending request is canceled, than we must kill the
;;; process and send an :abort message concerning the task.
;;; Entries on the pending request list are created as the result of a dialog not
;;; implemented yet.

;;;-------------------------------------------------------- MW-PR-DISCARD-ON-CLICK

(defUn mw-pr-discard-on-click (dialog widget)
  "kills a pending request"
  (declare (ignore widget))
  (moss::display-text dialog "*** Not implemented yet" :clean-pane t)
  ;(%set-value (%find-named-item :assistant dialog) ";*** Not implemented yet")
  t)

;;;-------------------------------------------------------- MW-PR-EXAMINE-ON-CLICK

(defUn mw-pr-examine-on-click (dialog widget)
  "examine the content of the pending request message to decide whether ~
   process it or not"
  (declare (ignore widget))
  (let ((agent (agent dialog))
        (pane (%find-named-item :pr-list dialog))
        pos)
    (unless (or (null (pending-requests agent))
                (assistant-complain-if-nothing-selected agent pane))
      ;; get position of selected item
      (setq pos (%get-selected-cell-index pane))
      ;; print a detailed version of message into assistant pane
      (assistant-detail-pending-request (agent dialog) 
                                        (nth pos (pending-requests agent)))
      ))
  t)


;;;--------------------------------------------------- MW-PR-LIST-INITIAL-SEQUENCE

(defUn mw-pr-list-initial-sequence (agent)
  (if (pending-requests agent)
      (mapcar #'assistant-answer-summary (pending-requests agent))
    (list (mln::extract-to-string *WASS-pr-msg*)))) ; jpb 1406

;;;---------------------------------------------------- MW-PR-LIST-ON-DOUBLE-CLICK

(defUn mw-pr-list-on-double-click (dialog widget) 
  (let ((agent (agent dialog))
        (index (%get-selected-cell-index widget))
        message)
    (when index
      (setq message (nth index (pending-requests agent)))
      (if message (assistant-detail-pending-request agent message)
        (%beep)))
    (unless index (%beep))
    )
  t)

;;;===============================================================================
;;;                           end pending requests
;;;===============================================================================

;;;---------------------------------------------------------------- MW-RESET-FONTS

(defUn mw-reset-fonts ()
  "fake function"
  nil)

;;;===============================================================================
;;;                            TASKS TO DO
;;;===============================================================================

;;; The tasks to do bin contains request messages (:ask) to the master. The tasks
;;; to do list entries are pairs (<message> . <process>) where <process> is the
;;; process-id of the task that was create to prepare the answer to the message.
;;; When an entry is delete we must kill the corresponding task and associated 
;;; timer.
;;; To retrieve the task when the master processes a message an prepares an answer
;;; a special a-list associated with the window is maintained. The waiting dynamic
;;; skill process waits on this a-list and is awaken as soon as a pair with the
;;; process-id tag is inserted on this list.

;;;-------------------------------------------------------- MW-TD-EXAMINE-ON-CLICK

(defUn mw-td-examine-on-click (dialog widget)
  "displays the content of the to do (:ask) message."
  (declare (ignore widget))
  (let ((agent (agent dialog))
        (pane (%find-named-item :td-list dialog))
        pos)
    (unless (or (null (waiting-messages agent))
                (assistant-complain-if-nothing-selected agent pane))
      ;; get position of selected item
      (setq pos (%get-selected-cell-index pane))
      ;; print a detailed version of message into assistant pane
      (assistant-detail-task-to-do (agent dialog) 
                                   (car (nth pos (waiting-messages agent))))
      ))
  t)

;;;-------------------------------------------------------- MW-TD-DISCARD-ON-CLICK

(defUn mw-td-discard-on-click (dialog widget)
  "remove a message from the list. If no message is selected complain. Resume the ~
   static-ask, passing an abort message to return to the caller."
  (declare (ignore widget))
  (let ((agent (agent dialog))
        (pane (%find-named-item :td-list dialog))
        pos selected-item gate-index)
    (unless (or (null (waiting-messages agent))
                (assistant-complain-if-nothing-selected agent pane))
      ;; get position of selected item
      (setq pos (%get-selected-cell-index pane))
      ;; get the corresponding item (<message> . <process>)
      (setq selected-item (nth pos (waiting-messages agent)))
      ;; remove corresponding message from the message list
      (setf (waiting-messages agent)(remove-at-pos pos (waiting-messages agent)))
      ;; and remove corresponding string from range
      (%set-item-sequence pane (remove-at-pos pos (%get-item-sequence pane)))
      ;; if resulting list is empty, then insert special text
      (unless (%get-item-sequence pane)
        (%set-item-sequence pane (list (mln::extract-to-string *WASS-td-msg*)))) ; jpb 1406
      ;; update count
      (%set-value (%find-named-item :td-count dialog)
                  (format nil "~A" (length (waiting-messages agent))))
      ;; must wake up static exit, first get gate-index
      (setq gate-index (cdr selected-item))
      ;; open the gate, returning nothing (NIL will trigger a :failure answer)
      (gate-open gate-index)
      ))
  t)

;;;--------------------------------------------------- MW-TD-list-initial-sequence

(defUn mw-td-list-initial-sequence (agent)
  (if (waiting-messages agent)
      (mapcar #'(lambda (xx) (assistant-answer-summary (car xx)))
        (waiting-messages agent))
    (list (mln::extract-to-string *WASS-td-msg*)))) ; jpb 1406

;;;---------------------------------------------------- MW-TD-LIST-ON-DOUBLE-CLICK

(defUn mw-td-list-on-double-click (dialog widget) 
  (let ((agent (agent dialog))
        (index (%get-selected-cell-index widget))
        message)
    (when index
      (setq message (nth index (waiting-messages agent)))
      (if message
          (assistant-detail-waiting-message agent message)
        (%beep))
      )
    (unless index (%beep))
    ))

;;;-------------------------------------------------------- MW-TD-PROCESS-ON-CLICK

(defUn mw-td-process-on-click (dialog widget)
  "if a pending request message is selected, transfers it to the todo-message
   area in FACTS and launches the process-ask dialog."
  (declare (ignore widget))
  (let ((agent (agent dialog))
        (pane (%find-named-item :td-list dialog))
        pos selected-item)
    (unless (or (null (waiting-messages agent))
                (assistant-complain-if-nothing-selected agent pane))
      ;; get position of selected item
      (setq pos (%get-selected-cell-index pane))
      ;; get the corresponding selected-item (<message> . <process>)
      (setq selected-item (nth pos (waiting-messages agent)))
      (format t "~%; mw-td-process-on-click /selected-item: ~S" selected-item)
      ;; save the message into FACTS
      ;(replace-fact (conversation agent) :todo-message (car selected-item))
      (replace-fact (conversation agent) :todo-message selected-item) ; JPB1511
      ;; start the ASK conversation
      (if (member :converse-v2 *features*)
          (setf (to-do agent) `(:from (from! selected-item) :data ":process-ask"))
      (setf (to-do agent) (list :interrupt ":process-ask")))
      )
    t))

;;;===============================================================================
;;;                            end tasks to do
;;;===============================================================================

;;;----------------------------------------------------------- MW-USE-BIGGER-FONTS

(defUn mw-use-bigger-fonts ()
  "fake function"
  nil)



;;;===============================================================================
;;;
;;;                Assistant Methods linked to Assistant Panel
;;;
;;;===============================================================================

;;; Since the methods or functions are linked to the agent and not to the window,
;;; we prefix their name with "assistant" rather than "MW", which agrees with the 
;;; MCL version

;;;--------------------------------------------- ASSISTANT-ADD-ANSWER-INFO (AGENT)

(defMethod assistant-add-answer-info ((agent agent) item)
  "add a (<message>) item to WAITING-ANSWERS agent slot and ~
   redisplays."
  (setf (waiting-answers agent) (append (waiting-answers agent)(list item)))
  (assistant-display-answers-to-examine agent)
  )

;;;--------------------------------------- ASSISTANT-ADD-DISCARDED-MESSAGE (AGENT)

;;;------------------------------------------ ASSISTANT-ADD-PENDING-REQUET (AGENT)

;;;---------------------------------------------- ASSISTANT-ADD-TASK-TO-DO (AGENT)

(defMethod assistant-add-task-to-do ((agent agent) item)
  "add an item to WAITING-MESSAGES agent slot and redisplays.
Argument: 
   item: a pair (<message>. <process-id>) so that we can recover the id of the 
         thread that posted the message"
  (setf (waiting-messages agent) (append (waiting-messages agent)(list item)))
  (assistant-display-tasks-to-do agent)
  )

;;;------------------------------------------------------ ASSISTANT-ANSWER-SUMMARY

(defUn assistant-answer-summary (message)
  "take an answer message and builds up a string that summarizes the message ~
   content.
Arguments:
   message: message to display."
  (message-limit-text-length
   (format nil "~S ~S ~S ~S ~S ~S" 
     (time-string (date message))
     (task-id message)
     (from! message)
     (to! message) 
     (action message)
     (if (eql :answer (type! message))
         (contents message)
       (args message)))
   80))

;;;-------------------------------------- ASSISTANT-CLEAR-ASSISTANT-OUTPUT (AGENT)

(defMethod assistant-clear-assistant-output ((agent agent))
  "clear master's input area.
Arguments:
   agent: agent."
  (let ((win (com-window agent)))
    (when (%window? win)
      (moss::display-text win "" :clean-pane t)
      ;(%set-value (%find-named-item :assistant win) "")
      )))

;;;------------------------------------------ ASSISTANT-CLEAR-MASTER-INPUT (AGENT)

(defMethod assistant-clear-master-input ((agent agent))
  "clear master's input area.
Arguments:
   agent: agent."
  (let ((win (com-window agent)))
    (when (%window? win)
      (%set-value (%find-named-item :master win) "")
      )))

;;;-------------------------------- ASSISTANT-COMPLAIN-IF-NOTHING-SELECTED (AGENT)

(defMethod assistant-complain-if-nothing-selected ((agent agent) item)
  "service function testing if something has been selected in display area ~
   represented by item. If not complain by beeping and sending a message.
Arguments:
   agent: agent
   item: pane in which something must be selected
Return:
   t : nothing is selected
   nil something is selectd, no complaining"
  (cond (#+MCL (selected-cells item)
               ;; fetch-control-value garantees an update
               #-MCL (and (cg:fetch-control-value item)
                          (cg:value item))   
               nil)
        (t
         (assistant-display-text agent "Please select something!")
         (%beep)
         t)))

;;;------------------------------------ ASSISTANT-DETAIL-ANSWER-TO-EXAMINE (AGENT)

(defMethod assistant-detail-answer-to-examine ((agent agent) message)
  "shows the details of an info or answer message. Simple display. No dialog.
Arguments:
   agent: current agent
   message: message to display
Return:
   unimportant"
  (let (sender content)
    ;; get sender identity
    (setq sender (from! message))
    ;; get content of the message
    (setq content (car (args message)))
    (assistant-display-text 
     agent
     (format nil "~A ~A ~%~A ~A ~%~A ~A ~2% ~A"
       (mln::extract-to-string *WASS-sender*) ; jpb 1406
       sender
       (mln::extract-to-string *WASS-priority*) ; jpb 1406
       (or (getf content :priority) 
           (mln::extract-to-string 
            (mln::make-mln '((:en "Normal") (:fr "Normal")))))
       (mln::extract-to-string *WASS-object*)
       (or (getf content :object) "")
       (or (getf content :text) 
           (mln::extract-to-string 
            (mln::make-mln 
             '((:en "<empty message>") (:fr "<message vide>")))))
       ))
    ;; quit
    :done))

;;;------------------------------------ ASSISTANT-DETAIL-DISCARDED-MESSAGE (AGENT)

(defMethod assistant-detail-discarded-message ((agent agent) &rest ll)
  "shows the details of a discarded message."
  (format t "~&Here we should show the details of a discarded message ~S" ll))

;;;-------------------------------------- ASSISTANT-DETAIL-PENDING-REQUEST (AGENT)

(defMethod assistant-detail-pending-request ((agent agent) &rest ll)
  "shows the details of a pending request."
  (format t "~&Here we should show the details of a pending request ~S" ll))

;;;------------------------------------------- ASSISTANT-DETAIL-TASK-TO-DO (AGENT)
;;; currently the same function as answer to examine

(defMethod assistant-detail-task-to-do ((agent agent) message)
  "shows the details of an ASK message. Simple display. No dialog.
Arguments:
   agent: current agent
   message: message to display
Return:
   unimportant"
  (let (sender content)
    ;; get sender identity
    (setq sender (from! message))
    ;; get content of the message
    (setq content (car (args message)))
    (assistant-display-text 
     agent
     (format nil "~A ~A ~%~A ~A ~%~A ~A ~2% ~A"
       (mln::extract-to-string *WASS-sender*) ; jpb 1406
       sender
       (mln::extract-to-string *WASS-priority*) ; jpb 1406
       (or (getf content :priority) 
           (mln::extract-to-string *WASS-normal*)) ; jpb 1406
       (mln::extract-to-string *WASS-object*) ; jpb 1406
       (or (getf content :object) "")
       (or (getf content :text) 
           (mln::extract-to-string *WASS-empty-message*)))) ; jpb 1406                   
    ;; quit
    :done))

;;;-------------------------------------- ASSISTANT-DETAIL-WAITING-MESSAGE (AGENT)

(defMethod assistant-detail-waiting-message ((agent agent) &rest ll)
  "shows the details of a waiting message (task to execute)."
  (format t "~&Here we should show the details of a waiting message ~S" ll))

;;;---------------------------------------------- ASSISTANT-DISCARD-ANSWER (AGENT)

(defMethod assistant-discard-answer ((agent agent) &rest ll)
  "eliminates a specific answer."
  (declare (ignore ll))
  (print "discarding answer"))

;;;--------------------------------- ASSISTANT-DISCARD-SELECTED-TO-DO-TASK (AGENT)

(defMethod assistant-discard-selected-to-do-task ((agent agent))
  "removes a selected item from the list of TO-DO tasks."
  (let ((widget (%find-named-item :td-list (com-window agent))))
    (unless (assistant-complain-if-nothing-selected agent widget)
      (setf (waiting-messages agent)
        (remove-at-pos (%get-selected-cell-index widget)
                       (waiting-messages agent)))
      (assistant-display-tasks-to-do agent))))

#|
(assistant-discard-selected-to-do-task albert::albert)
|#
;;;------------------------------------- ASSISTANT-DISPLAY-ADDITIONAL-TEXT (AGENT)

(defMethod assistant-display-additional-text ((agent omas::agent) text)
  "takes a string and displays it in the display pane of the assistant com window ~
   appending it to the already displayed text.
Arguments:
   agent: agent
   text: string to be displayed in the com window
   view-name: (opt) name of the destination view (default is output)."
  (let ((com-win (com-window agent)))
    ;; check that the window is still there
    (if (%window? com-win)
        (moss::display-text com-win text)
      ;; else warns user
      (progn
        (%beep)
        (warn "Agent ~S has no window for displaying ~S" agent text)))
    ;; quit
    :done))

;;;---------------------------------- ASSISTANT-DISPLAY-ANSWERS-TO-EXAMINE (AGENT)
;;; should display text when no answers

(defMethod assistant-display-answers-to-examine ((agent agent))
  "displays waiting answers in the corresponding pane of the assistant com window. ~
   upgrades the count in special count window.
Arguments:
   agent: agent."
  (let ((com-win (com-window agent)) panel)
    ;; check that the window is still there
    (if (%window? com-win)
        (progn
          ;; get display panel handle
          (setq panel (%find-named-item :ae-list com-win))
          ;; show entries
          (%set-item-sequence panel 
                              (mapcar #'assistant-answer-summary 
                                (waiting-answers agent)))
          ;; give count
          (%set-value (%find-named-item :ae-count com-win)
                      (format nil "~S" (length (waiting-answers agent)))))
      ;; else warns user
      (warn "Agent ~S has no window for displaying waiting-answers" agent ))
    ;; quit
    :done))

#|
(assistant-display-answers-to-examine albert::albert)
|#
;;;---------------------------------- ASSISTANT-DISPLAY-DISCARDED-MESSAGES (AGENT)
;;; should display text when no answers, e.g. <corbeille vide>

(defMethod assistant-display-discarded-messages ((agent agent))
  "displays discarded messages in the corresponding pane of the assistant com window. ~
   upgrades the count in special count window.
Arguments:
   agent: agent."
  (let (com-win panel)
    ;; get window handle
    (setq com-win (com-window agent))
    ;; check that the window is still there
    (if (%window? com-win)
        (progn
          ;; get display panel handle
          (setq panel (%find-named-item :dm-list com-win))
          ;; show entries
          (%set-item-sequence panel 
                              (mapcar #'assistant-answer-summary 
                                (discarded-messages agent)))
          ;; give count
          (%set-value (%find-named-item :dm-count com-win)
                      (format nil "~S" (length (discarded-messages agent)))))
      ;; else warns user
      (warn "Agent ~S has no window for displaying discarded-messages" agent ))
    ;; quit
    :done))

;;;------------------------------------ ASSISTANT-DISPLAY-PENDING-REQUESTS (AGENT)

(defMethod assistant-display-pending-requests ((agent agent))
  "displays pending requests in the corresponding pane of the assistant com window. ~
   upgrades the count in special count window.
Arguments:
   agent: agent."
  (let (com-win panel)
    ;; get window handle
    (setq com-win (com-window agent))
    ;; check that the window is still there
    (if (%window? com-win)
        (progn
          ;; get display panel handle
          (setq panel (%find-named-item :pr-list com-win))
          ;; show entries
          (%set-item-sequence panel 
                              (mapcar #'assistant-answer-summary 
                                (pending-requests agent)))
          ;; give count
          (%set-value (%find-named-item :pr-count com-win)
                      (format nil "~S" (length (pending-requests agent)))))
      ;; else warns user
      (warn "Agent ~S has no window for displaying pending-requests" agent ))
    ;; quit
    :done))

;;;------------------------------------- ASSISTANT-DISPLAY-REQUEST-RESULTS (AGENT)

(defMethod assistant-display-request-results ((agent agent) message)
  "displays the results from an answer message. In order to do a good job of ~
   presentation, the assistant should know the nature of the answer, which may ~
   not be the case.
Arguments:
   agent: agent
   message: message containing the answer."
  (let (com-win panel)
    ;; get window handle
    (setq com-win (com-window agent))
    ;; check that the window is still there
    (if (%window? com-win)
        (moss::display-text com-win (format nil "~S" (contents message)))
      ;; else warns user
      (warn "Agent ~S has no window for displaying ~S" agent (contents message)))
    ;; quit
    :done))

;;;----------------------------------------- ASSISTANT-DISPLAY-TASKS-TO-DO (AGENT)

(defMethod assistant-display-tasks-to-do ((agent agent))
  "displays waiting messages in the corresponding pane of the assistant com window. ~
   upgrades the count in special count window.
Arguments:
   agent: agent."
  (when (assistant? agent)
    (let (com-win panel)
      ;; get window handle
      (setq com-win (com-window agent))
      ;; check that the window is still there
      (if (%window? com-win)
          (progn
            ;; get display panel handle
            (setq panel (%find-named-item :td-list com-win))
            ;; if the window has no panel for posting tasks to do, give up
            (unless panel
              (return-from assistant-display-tasks-to-do))
            ;; show entries
            (if (waiting-messages agent)
                (%set-item-sequence panel 
                                    (mapcar #'assistant-make-item-display-string 
                                      (waiting-messages agent)))
              ;; post a message: nothing to do
              (%set-item-sequence panel (list (mln::extract-to-string *wass-td-msg*)))) ; jpb 1406
            ;; give count
            (%set-value (%find-named-item :td-count com-win)
                        (format nil "~S" (length (waiting-messages agent)))))
        ;; else warns user
        (warn "Agent ~S has no window for displaying waiting-messages" agent ))
      ;; quit
      :done)))

;;;------------------------------------------------ ASSISTANT-DISPLAY-TEXT (AGENT)

(defMethod assistant-display-text ((agent agent) text)
  "takes a string and displays it in the display pane of the assistant com window ~
   using the moss::display-text method.
Arguments:
   agent: agent
   answer: string to be displayed in the com window
Return:
   :done"
  (let ((com-win (com-window agent)))
    (cond
     ;; if pane is a window print text
     ((%window? com-win)
      (moss::display-text com-win text :erase t))
      ;; else warns user
      (t (warn "Agent ~S has no window for displaying ~S" agent text)))
    ;; quit
    :done))

#|
(omas::assistant-display-text albert::PA_albert "tout va bien")
|#
;;;--------------------------------- ASSISTANT-DISPLAY-UNPROCESSED-ANSWERS (AGENT)
;;; to be done


;;;----------------------------------------- ASSISTANT-GET-CANONICAL-ENTRY (AGENT)

(defMethod assistant-get-canonical-entry ((agent agent) xx ontology)
  "examines if xx is a word corresponding to an entry of the ontology.
Arguments:
   agent: agent
   XX: entry to normalize
   ontology: alist representing ontologies."
  (if (stringp xx) (return-from assistant-get-canonical-entry (list xx)))
  (dolist (item ontology)
    (cond ((eql (car item) xx) 
           (return-from assistant-get-canonical-entry (list xx)))
          ((member xx (getf (cdr item) :synonym)) 
           (return-from assistant-get-canonical-entry (list (car item))))))
  ;; if the word is unknown then return nil
  nil)

;;;-------------------------------------------------- ASSISTANT-GET-STRING (AGENT)

(defMethod assistant-get-string ((agent agent) text)
  "opens a temporary window for getting text from user"
  #+MCL (get-string-from-user text)
  #-MCL (multiple-value-bind (text text2 button text-OK)
            (cg:ask-user-for-string text "" "OK" "CANCEL")
          (declare (ignore text2 button))
          (if text-ok text nil))
  )

;;;---------------------------------------------- ASSISTANT-GET-USER-INPUT (AGENT)

(defMethod assistant-get-user-input ((agent agent) text)
  "get input from user transforming it into a list.
Arguments:
   agent: agent
   text: text to be printed in the input box."
  (let ((input (assistant-get-string agent text)))
    ;; check first for balanced s-expr (rough check)
    (unless (eql (count #\( text)(count #\) text))
      ;; when unbalanced, can't translate text into list
      (return-from assistant-get-user-input nil))
    ;; translate text into list
    (read-from-string (concatenate 'string "(" input ")" ))))

;;;------------------------------------ ASSISTANT-MAKE-ITEM-DISPLAY-STRING (AGENT)
;;; defined as a function to be applied simply

(defUn assistant-make-item-display-string (item)
  "take an entry of the waiting-messages agent slot and builds up a string that ~
   summarizes the message content limiting its length to 80 chars.
Arguments:
   item: e.g. (message . gate-index)
   message: message to display."
  (let* ((message (car item))
         (content (car (args message))))
    (message-limit-text-length
     (format nil "~A ~A/ ~A/ ~A/ from: ~A/ ~A"
       (date-string (date message))
       (time-string (date message))
       (if (eql (action message) :ask) "ASK" "TELL ")
       (or (cadr (member :priority content)) "NORMAL")
       (from! message)
       (or (cadr (member :object content)) "<No object specified>")
       )
     80)))

#|
? (in-package :omas)
#<Package "OMAS">
? (defmessage :MSG01 :type :request :from :<USER> :to :albert :action :tell 
    :args ((:priority :low :object "test message" :text "This is a test message))
to test the posting function.")))
:MSG01
? (assistant-make-item-display-string albert::albert (list msg01 ))
"08/08/17 17:5:57/ LOW/ from: <USER>/ test message"
|#
;;;--------------------------------------------------- ASSISTANT-NORM-TEXT (AGENT)
;;; ??? This step is taken care of by the dialog process;;; ???;;; ???

(defMethod assistant-norm-text ((agent agent) expr)
  "expr is a list representing a text typed in bye the master. The function uses ~
            the local ontology to clean the text, removing empty words, replacing symbol by ~
            their canonical equivalents (taken from the ontology), and leaving data ~
            strings alone. It the result consists only of data strings, i.e., of all ~
            non-strings have been removed, then this is considered a failure and the expr ~
            is returned unprocessed.
Arguments:
   agent: agent
   expr: a list to be normalized."
  (let ((initial-expr (copy-tree expr))
        (ontology (ontology agent)))
    (unless ontology (return-from assistant-norm-text expr))
    ;; first remove all empty words
    (setq expr
          (mapcan #'(lambda (xx) 
                      (if (member xx (getf (cdr (assoc :empty-words ontology))
                                           :values))
                          nil
                        (list xx)))
            expr))
    ;(print expr)
    ;; use canonical entries
    (setq expr
          (mapcan #'(lambda (xx) (assistant-get-canonical-entry agent xx ontology)) expr))
    ;(print expr)
    ;; if no more non string entries give up
    (dolist (item expr)
      (unless (stringp item) (return-from  assistant-norm-text expr)))
    ;; failure we return initial list
    initial-expr))

;;;---------------------------------------- ASSISTANT-PARSE-INFORM-MESSAGE (AGENT)
;;; ??? This is mixing the type of message (way it is delivered) with the type of
;;; action, here inform!

(defMethod assistant-parse-inform-message ((agent agent) text &aux input args to)
  "parse a text into a message structure. E.g.,
      albert watch the price increase of the technological bonds
      (:date *now* :type :inform :from <agent> :to albert :action () 
       :args '(\"watch the price increase of the technological bonds\"))
Arguments:
   agent: agent
   text: text to be parsed.
Returns:
   an inform message of type internal and action SEND-INFORM"
  ;; check first for balanced s-expr (rough check)
  (unless (eql (count #\( text)(count #\) text))
    ;; when unbalanced, can't translate text into list
    (assistant-display-text 
     agent "Can't process text because of unbalanced parentheses")
    (return-from assistant-parse-inform-message nil))
  ;; translate text into list
  (setq input (read-from-string (concatenate 'string "(" text ")" )))
  ;; remove anything that is not in the ontology at first level
  ;; except for agent names.
  ;
  ;; assume now that first word is destination, second args text
  (setq to (pop input) args input)
  ;; check that to is the name of an agent
  (unless (or (member to '(ALL ALL-AND-ME))
              (and (boundp to) 
                   (member (setq to (eval to)) (local-agents *omas*))))
    ;; tell user
    (return-from assistant-parse-inform-message nil))
  ;; construct and send message
  (message-make :date (get-universal-time) :type :internal :action 'SEND-INFORM
                :args `(:to ,to :args ,(list (format nil "~{~S ~}" args)))
                ))

;;;------------------------------------------- ASSISTANT-PARSE-MASTER-TEXT (AGENT)
;;; ???

(defMethod assistant-parse-master-text ((agent agent) text)
  "parse the input text into some sort of structrured list, taking into account ~
            the ontology, the master's model, the conversation context, or any other ~
            usable info. Currently acts as a noop.
Arguments:
  agent: agent
  text: text as a list extracted from the master's area."
  ;; check first for balanced s-expr (rough check)
  (unless (eql (count #\( text)(count #\) text))
    ;; when unbalanced, can't translate text into list
    (return-from assistant-parse-master-text nil))
  ;; translate text into list
  (read-from-string (concatenate 'string "(" text ")" )))

;;;--------------------------------------- ASSISTANT-PARSE-REQUEST-MESSAGE (AGENT)
;;; ???

(defMethod assistant-parse-request-message ((agent agent) text &aux input)
  "parse a text into a message structure. E.g.,
      ASK mul-1 MULTIPLY  (2 3)
      (:date *now* :type :request :from <agent> :to mul-1 :action 'MULTIPLY 
       :args '(2 3))
Arguments:
   agent: agent
   text: text to be parsed.
Return:
    a message with type :internal and action SEND-REQUEST"
  ;; check first for balanced s-expr (rough check)
  (unless (eql (count #\( text)(count #\) text))
    ;; when unbalanced, can't translate text into list
    (return-from assistant-parse-request-message nil))
  ;; translate text into list
  (setq input (read-from-string (concatenate 'string "(" text ")" )))
  ;; remove anything that is not in the ontology at first level except for agent names.
  ;; since we send a message in free style there is no action
  ;; we could insert a default action verb like PROCESS
  (message-make :date (get-universal-time) :type :internal :action 'SEND-REQUEST
                :args `(:to ALL :args ,(list input) :protocol :free-style)
                ))

;;;---------------------------------------------- ASSISTANT-PROCESS-ANSWER (AGENT)
;;; ???

(defMethod assistant-process-answer ((agent agent) item)
  "process an answer message found in the waiting-answer box of the assistant agent. ~
            We should process data, then kill pending-requests entry, then remove the answer ~
            message from the waiting-answers list.
Arguments:
   agent: agent
   item: answer message to be processed."
  ;; an answer message when processed will wake up the dynamic function of the
  ;; assistant skill that sent the corresponding subtask
  ;; thus the code will know what to do
  (let ((data (contents item)))
    ;; quick and dirty function to print result in the agent's com window
    (case (car data)
      (:list
       ;; in fact the result should be printed out into an HTML page
       (assistant-display-text agent (format nil "~{~A~^, ~}" 
                                       (mapcar #'cadr (cdr data))))))))

;;;----------------------------------------- ASSISTANT-PROCESS-MASTER-TEXT (AGENT)

(defMethod assistant-process-master-text ((agent assistant) text)
  "processes the input found in the master input pane, removing characters that ~
   could pose problems for building a lisp list. Currently, take only parents ~
   into account. Put processed text into the TO-DO conversation slot.
Arguments:
   agent: agent
   text: text string as extracted from the master input area."
  ;; first add the master text to the assistant pane whenever we are tracing the
  ;; dialog, insert a new line
  (when (show-dialog agent)
    (moss::display-text (com-window agent) text 
                        :header "Master> "
                        :color moss::*red-color*))
  ;; then reinstall stop characters
  (setf (pass-every-char (com-window agent)) nil)
  ;; because of the different languages, all processing is done by the PA
  ;; move raw text into the working area, which wakes up converse process
  (if (member :converse-v2 *features*)
      ;; in version 2 we need more info
      (setf (to-do agent) `(:from ,(key agent) :text ,text))
    (setf (to-do agent) text))
  :done)

;;;------------------------------------------------- ASSISTANT-SAVE-ANSWER (AGENT)

(defMethod assistant-save-answer ((agent agent) &rest ll)
  "saves a specific answer. Should start some sort of dialog to see what to do."
  ;; currently does nothing
  (format t "~&~S's Master is saving answer ~S somewhere..."
    (name agent) ll))

;;;------------------------------------- ASSISTANT-PROCESS-TEXT-FOR-ANSWER (AGENT)

(defMethod assistant-process-text-for-answer ((agent agent) text &aux message)
  "agent is waiting for an answer to a specific task. Parse text and, if it makes ~
            sense put the answer onto the saved-answer slot."
  (declare (ignore text message))
  :done)

;;;------------------------------------- ASSISTANT-PROCESS-TEXT-FOR-INFORM (AGENT)

(defMethod assistant-process-text-for-inform ((agent agent) text &aux message)
  "parse text and if this makes sense sends the inform message."
  ;; build internal message to be inserted into the delayed-input box
  (setq message (assistant-parse-inform-message agent text))
  ;; result is nil if parsing can't be done
  (if (null message)
      ;; tell user we can't make sense
      (progn
        (assistant-display-text 
         agent "I can't make sense of this message. Please try again."))
    ;; add message to results ready for processing
    (progn
      ;; display resulting message
      (assistant-display-text agent (format nil "~S" message))
      ;; and prepare to process message
      (%agent-add agent message :agenda)))
  :done)

;;;------------------------------------ ASSISTANT-PROCESS-TEXT-FOR-REQUEST (AGENT)

(defMethod assistant-process-text-for-request ((agent agent) text &aux message)
  "get a string from master and builds a request message to be inserted into the ~
            agenda of the assistant agent with the action SEND-REQUEST. This will set-up a ~
            task to some other agent or a broadcast task. The answer will be sent back to the ~
            master.
Arguments:
   agent: agent
   text: text typed in by the user"
  ;; build internal message to be inserted into the delayed-input box
  (setq message (assistant-parse-request-message agent text))
  ;; result is nil if parsing can't be done
  (if (null message)
      ;; tell user we can't make sense
      (progn
        (assistant-display-text
         agent "I can't make sense of this message. Please try again.")
        )
    ;; add message to results ready for processing
    (progn
      ;; display resulting message
      (assistant-display-text agent (format nil "~S" message))
      ;; and prepare to process message
      (%agent-add agent message :agenda)))
  :done)

;;;------------------------------------- ASSISTANT-PROCESS-WAITING-MESSAGE (AGENT)

(defMethod assistant-process-waiting-message ((agent agent) item)
  "get an item (pair message, process-id) from the list of waiting messages and ~
   process it, i.e. launches an ASK or a TELL dialog.
Arguments:
   agent: agent
   item: pair <message . process-id>"
  (let ((message (car item))
        (conversation (conversation agent)))
    ;; save the item pair into the FACTS
    (replace-fact conversation :item item)
    ;; put info into the details conversation slot
    ;(setf (has-details conversation) (list (list :item item)))
    
    (unless (member :converse-v2 *features*)
      ;; start the right conversation according to message type
      (case (type! message)
        (:request 
         (setf (to-do agent) ":process-ask")
         )
        (:inform 
         (setf (to-do agent) ":process-tell")
         )))
    
    (when (member :converse-v2 *features*)
      ;; start the right conversation according to message type
      (case (type! message)
        (:request 
         (setf (to-do agent) `(:from ,(from! message) :data ":process-ask"))
         )
        (:inform 
         (setf (to-do agent) `(:from ,(from! message) :data ":process-tell"))
         )))
    :done))

#|
(defmessage :MSG01 :type :inform :from :<USER> :to :albert :action :tell 
  :args ((:priority :low :object "test message" :text "This is a test message
to test the posting function.")))
:MSG01
(assistant-process-waiting-message albert::albert (list MSG01))
:DONE
|#
;;;-------------------------------------- ASSISTANT-READ-FROM-MASTER-INPUT (AGENT)

(defMethod assistant-read-from-master-input ((agent agent))
  "reads some text from master's input area and returns a string.
Arguments:
   agent: agent.
   test: text to input into the area"
  (let ((win (com-window agent)))
    (when (%window? win)
      (coerce (%get-value (%find-named-item :master win)) 'string)
      )))

;;;-------------------------------------------------- ASSISTANT-RESET-FONT (AGENT)

(defMethod assistant-reset-font ((agent assistant))
  "in case agent is an assistant agent resets the font back to the default in the ~
   master and assistant parts of the window. Otherwise do nothing."
  (let ((ww (com-window agent))
        input output)
    (when ww
      (setq output (cg:find-component :assistant ww)
          input (cg:find-component :master ww))
      ;; set new font 
      (setf (cg::font output)(cg:make-font-ex NIL "Tahoma / ANSI" 11 NIL))
      (setf (cg::font input)(cg:make-font-ex NIL "Tahoma / ANSI" 11 NIL))
      ;; record new size into the agent's parameters
      (setf (output-font-size agent) 11)
      :done)))

; (omas::assistant-reset-font albert::pa_albert)
;;;------------------------------------------------ ASSISTANT-RESET-WINDOW (AGENT)

(defMethod assistant-reset-window ((agent agent))
  "reset win-com windows for assistant agents. Called by reset.
Arguments: 
   agent: agent."
  (let ((win (com-window agent)))
    ;; check if it is still there
    (when (%window? win)
      ;; if so blank out output and text-area
      (moss::display-text win "" :clean-pane t)
      ;(%set-value (%find-named-item :assistant win) "")
      (%set-value (%find-named-item :master win) ""))
    ;; exit
    :done))

;;;------------------------------------ ASSISTANT-REVIVE-DISCARDED-MESSAGE (AGENT)

(defMethod assistant-revive-discarded-message ((agent agent) &rest ll)
  "should revive a discarded message."
  (format t "~&Here we should show the details of a discarded message ~S" ll))


;;;--------------------------------------------- ASSISTANT-USE-LARGER-FONT (AGENT)
;;; this function is replaced by changing the size of the font in the slot
;;; (output-font-size agent), e.g. through the dialog.

(defMethod assistant-use-larger-font ((agent assistant) &optional (more 2))
  "in case agent is an assistant agent resets the font to a larger size in the ~
   master and assistant parts of the window. Otherwise do nothing.
Arguments:
   agent: current agent
   more (opt): increase in size of the new font (default 2)
Return:
   :done"
  (let ((ww (com-window agent))
        (size (output-font-size agent))
        input output)
    (when (%window? ww)
      (setq output (%find-named-item :assistant ww)
          input (%find-named-item :master ww))
      ;; record new size into the agent's parameters
      (setf (output-font-size agent) (incf size more))
      ;; set new font 
      (setf (cg::font output)(cg:make-font-ex NIL "Tahoma / ANSI" size NIL))
      (setf (cg::font input)(cg:make-font-ex NIL "Tahoma / ANSI" size NIL))
      :done)))

;(assistant-use-larger-font stevens::PA_stevens)
; (omas::assistant-use-larger-font albert::PA_albert)

;;;-------------------------------------------- ASSISTANT-USE-SMALLER-FONT (AGENT)

(defMethod assistant-use-smaller-font ((agent assistant) &optional (more 2))
  "in case agent is an assistant agent resets the font to a larger size in the ~
   master and assistant parts of the window. Otherwise do nothing.
Arguments:
   agent: current agent
   more (opt): increase in size of the new font (default 2)
Return:
   :done"
  (let ((ww (com-window agent))
        (size (output-font-size agent))
        input output)
    (when (%window? ww)
      (setq output (%find-named-item :assistant ww)
          input (%find-named-item :master ww))
      ;; record new size into the agent's parameters
      (setf (output-font-size agent) (decf size more))
      ;; set new font 
      (setf (cg::font output)(cg:make-font-ex NIL "Tahoma / ANSI" size NIL))
      (setf (cg::font input)(cg:make-font-ex NIL "Tahoma / ANSI" size NIL))
      :done)))

#|
(omas::assistant-use-smaller-font albert::pa_albert)
|#
;;;--------------------------------------- ASSISTANT-WRITE-TO-MASTER-INPUT (AGENT)

(defMethod assistant-write-to-master-input ((agent agent) text)
  "puts text into master's input area.
Arguments:
   agent: agent.
   test: text to input into the area"
  (let ((win (com-window agent)))
    (when (%window? win)
      (%set-value (%find-named-item :master win) (coerce text 'string))
      )))

;---

(format t "~&;*** OMAS v~A - assistant windows loaded ***" *omas-version-number*)

;;; :EOF
