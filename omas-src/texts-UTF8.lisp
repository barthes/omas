;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;20/02/02
;;;            O M A S - T E X T S - U T F 8  (file texts-UTF8.lisp)
;;;
;;;===============================================================================
;;; (c) Jean-Paul Barthès@HEUDIASYC, CNRS, UTC. See LICENSE file for permissions.

#|
 This file contains all the strings that must be displayed in the OMAS windows.
 It must be saved and loaded using UTF-8 encoding. 

History
-------
2024
 1018 adapting to Windows 10
2020
 0202 restructuring OMAS as an asdf system
|#

(in-package :omas)

;;;===============================================================================
;;;
;;;                             TEXT MARKERS
;;;
;;;===============================================================================
;;; used by the window callbacks to detect the end of the sentence.
;language tags (:BR :CN :EN :ES :FR :IT :JP :LU :PL :UNKNOWN)

(defParameter *question-markers* 
  '(:br (#\?) :cn (#\?) :en (#\?) :es (#\?) :fr (#\?) :it (#\?) :jp (#\?)
        :lu (#\?) :pl (#\?) :unknown (#\?)))

(defParameter *full-stop-markers*
  '(:br (#\. #\!) :cn (#\. #\!) :en (#\. #\!) :es (#\. #\!) :fr (#\. #\!) 
        :it (#\. #\!) :jp (#\. #\!) :lu (#\. #\!) :pl (#\. #\!) 
        :unknown (#\. #\!) ))

;;; because of many problems with Japanese input, we specify the delimiters in
;;; a brute force manner, providing the UNICODE code
(defParameter *japanese-delimiters*
  (mapcar #'code-char
    '(65294  ; period
      12289  ; comma
      12300  ; left parent
      12301  ; right parent
      12540  ; hyphen
      12290  ; hollow period
      )))

;;;===============================================================================
;;;
;;;                             AGENT WINDOW
;;;
;;;===============================================================================

;;; Agent window is in English

(defVar *wag-tasks* 
    '((:en "<TASKS>")))
(defVar *wag-details*
    '((:en "details")))
(defVar *wag-export* 
    '((:en "export")))
(defVar *wag-ontology* 
    '((:en "ontology")))
(defVar *wag-trace* 
    '((:en "trace")))
(defvar *wag-dialog* 
    '((:en "dialog")))
(defvar *wag-checkpoint* 
    '((:en "checkpoint")))
(defVar *wag-inspect* 
    '((:en "inspect")))
(defVar *wag-input-messages* 
    '((:en "Input Messages")))
(defVar *wag-agenda* 
    '((:en "Agenda")))
(defVar *wag-no-selection* 
    '((:en "<no selection>")))
(defVar *wag-connect* 
    '((:en "connect")))
(defVar *wag-disconnect* 
    '((:en "disconnect")))
(defVar *wag-on* 
    '((:en "ON")))
(defVar *wag-off* 
    '((:en "OFF")))
(defvar *wag-restore* 
    '((:en "restore")))
(defVar *wag-unknown-agent* 
    '((:en "unknown selected agent: ~S")))

;;;===============================================================================
;;;
;;;                            ASSISTANT WINDOW
;;;
;;;===============================================================================

;;; Assistant window is in the Assistant language

(defVar *wass-a* 
    '((:en "Dialog history") 
      (:fr "Historique du dialogue")
      (:br "Assistente")
      (:jp "アシスタント（織部）")))
(defVar *wass-a-msg* 
    '((:en "<text from assistant>" )
      (:fr "<panneau de l'assistant>")
      (:br "<área do assistente>")
      (:jp "<アシスタントからの表示")))
(defvar *wass-a-switch*
    '((:fr "réduire fenêtre")
      (:en "small window")))
(defvar *wass-a-big-interface*
    '((:fr "Interface complète")
      (:en "Big window")))
(defVar *wass-ae* 
    '((:en "Answers to examine" )
      (:fr "Réponses/Information")
      (:br "Respostas/Informações")
      (:jp "応答")))
(defVar *wass-ae-msg* 
    '((:en "<no anwer to examine currently...>")
      (:fr "<rien pour l'instant...>")
      (:br "<nada a examinar por enquanto...>")
      (:jp "現在実行すべき応答無し")))
(defVar *wass-clear* 
    '((:en "Clear" )
      (:fr "Effacer")
      (:br "Limpar")
      (:jp "消す")))
(defVar *wass-dm* 
    '((:en "Discarded messages (by assistant)")
      (:fr "Messages jetés (par l'assistant)")
      (:br "Mensagens descartadas (pelo assistente)")
      (:jp "メッセージを無視 (by assistant")))
(defVar *wass-dm-msg* 
    '((:en "<waste basket is empty...>")
      (:fr "<corbeille vide...>")							                
      (:br "<lixeira vazia...>")
      (:jp "ゴミ箱は空")))
(defVar *wass-discard* 
    '((:en "Discard" )
      (:fr "Supprimer")
      (:br "Apagar")
      (:jp "消す")))
(defVar *wass-done* 
    '((:en "Done" )
      (:fr "Terminer")
      (:br "Terminar")
      (:jp "入力完了")))
(defVar *wass-examine* 
    '((:en "Examine" )
      (:fr "Afficher")
      (:br "Mostrar")
      (:jp "実行")))
(defVar *wass-m* 
    '((:en "Master" )
      (:fr "Maître")
      (:br "Mestre")
      (:jp "マスター")))
(defVar *wass-m-msg* 
    '((:en "<master input>" )
      (:fr "<panneau du maître>")
      (:br "<área do mestre>")
      (:jp "<マスターの入力>")))
(defVar *wass-pr* 
    '((:en "Pending Master requests" )
      (:fr "Questions en attente")
      (:br "Tarefas pendentes")
      (:jp "未実行の指示")))
(defVar *wass-pr-msg* 
    '((:en "<no pending request...>")
      (:fr "<rien pour l'instant...>")
      (:br "<nenhuma  tarefa pendente...>")
      (:jp "<未実行の支持は無し>")))
(defVar *wass-process* 
    '((:en "Process" )
      (:fr "Traiter")
      (:br "Processar")
      (:jp "処理")))
(defVar *wass-revive* 
    '((:en "Revive" )
      (:fr "Extraire")
      (:br "Extrair")
      (:jp "再実行")))
(defVar *wass-save* 
    '((:en "Save" )
      (:fr "Conserver")
      (:br "Salvar")
      (:jp "保存")))
(defVar *wass-td* 
    '((:en "Tasks to do") 
      (:fr "A faire")
      (:br "tarefas a realizar")
      (:jp "予定作業")))
(defVar *wass-td-msg* 
    '((:en "<no more task to do currently...>")
      (:fr "<rien pour l'instant...>")
      (:br "<nada a fazer por enquanto...>")
      (:jp "予定作業無し")))

;;; pour affichage des messages
(defvar *was-empty-message*
    '((:en "empty message")
      ( :fr "message vide")))
(defVar *wass-from* 
    '((:en "from:" )
      (:fr "de:"	)
      (:br "de:")
      (:jp "から")))
(defVar *wass-no-object* 
    '((:en "<No object specified>" )
      (:fr "<Pas d'objet>")
      (:br "<nenhum objeto especificado>")
      (:jp "<特にオブジェクトはありません>")))
(defVar *wass-normal* 
    '((:en "NORMAL" )
      (:fr "NORMAL")
      (:br "NORMAL")
      (:jp "通常")))
(defVar *wass-object* 
    '((:en "Object:" )
      (:fr "Objet :")
      (:br "Objeto:")
      (:jp "オブジェクト")))
(defVar *wass-priority* 
    '((:en "Priority:" )
      (:fr "Urgence :")
      (:br "Prioridade:")
      (:jp "優先順位")))
(defVar *wass-sender* 
    '((:en "From:" )
      (:fr "Expéditeur :")
      (:br "De:")
      (:jp "から")))

;;;===============================================================================
;;;
;;;                              CONTROL PANEL
;;;
;;;===============================================================================

;;; Control panel is in English

(defVar *wcp-title* 
    '((:en "OMAS-MOSS v~A - Control Panel")))
(defVar *wcp-trace-messages* 
    '((:en "trace messages")))
(defVar *wcp-draw-bids* 
    '((:en "draw bids")))
(defVar *wcp-draw-timers* 
    '((:en "draw timers")))
(defVar *wcp-verbose* 
    '((:en "verbose")))
(defVar *wcp-kill-msg* 
    '((:en "kill msg")))
(defVar *wcp-new-msg* 
    '((:en "new msg")))
(defVar *wcp-send-msg* 
    '((:en "send msg")))
(defVar *wcp-agents/msg* 
    '((:en "agents/msg")))
(defVar *wcp-load-agent* 
    '((:en "load agent")))
(defVar *wcp-reset-graphics* 
    '((:en "reset graphics")))
(defVar *wcp-found-objects* 
    '((:en "List of found objects")))
(defVar *wcp-trace* 
    '((:en "trace")))
(defVar *wcp-untrace* 
    '((:en "untrace")))
(defVar *wcp-reset* 
    '((:en "reset")))
(defvar *wcp-ontology*
    '((:en "ontology")))
(defVar *wcp-quit* 
    '((:en "quit")))
(defVar *wcp-sure?*
    '((:en "Are you sure?")))
(defVar *wcp-yes* 
    '((:en "YES")))
(defVar *wcp-no* 
    '((:en "NO")))
(defVar *wcp-select-message*
    '((:en "you must first select a message from the list")))
(defVar *wcp-select-agent* 
    '((:en "you must first select an agent from the list")))

;;;===============================================================================
;;;
;;;                               INIT WINDOW
;;;
;;;===============================================================================

;;; Init window is in English

(defVar *wini-title* 
    '((:en  "OMAS v ~A")))
(defVar *wini-local-reference* 
    '((:en "LOCAL REFERENCE")))
(defVar *wini-appli* 
    '((:en "APPLI / COTERIE")))
(defVar *wini-not-available* 
    '((:en "*** option not available ***")))
(defVar *wini-folder* 
    '((:en "FOLDER")))
(defvar *wini-hide* 
    '((:en "HIDE")))
(defVar *wini-ip* 
    '((:en "IP ADDRESS")))
(defVar *wini-port* 
    '((:en "PORT NUMBER")))
(defVar *wini-interaction* 
    '((:en "INTERACTION")))
(defVar *wini-save* 
    '((:en "SAVE")))
(defVar *wini-load* 
    '((:en "LOAD")))

;;;==============================================================================
;;;
;;;                              MESSAGE WINDOW
;;; 
;;;===============================================================================

;;; Message Window is in English

(defVar *wmsg-slot-suffix* 
    '((:en "-SLOT")))
(defVar *wmsg-new-message* 
    '((:en "New Message")))
(defVar *wmsg-send* 
    '((:en "SEND")))

;;;===============================================================================
;;;
;;;                              ONTOLOGY WINDOW
;;; 
;;;===============================================================================

;;; Ontology window is in ?

;;;===============================================================================
;;;
;;;                               SPY WINDOW
;;;
;;;===============================================================================

;;; Spy window title is in English
;;; No specific text in the SPY window

;;;===============================================================================
;;;
;;;                               WEB EDITOR
;;;
;;;===============================================================================
;;; use (mln::extract omas::*web-object-class*) to call proper label

(defvar *web-abort* 
    '((:en "Editing aborted.") 
      (:fr "Édition interrompue.")))
(defvar *web-again* 
    '((:en "Again...")
      (:fr "Encore...")))
(defvar *web-bad-language-code*
    '((:en "Illegal language code, should be like :XX")
      (:fr "Code langage illegal, devrait avoir la forme :XX")))
(defvar *web-cannot-find*
    '((:en "I can't find this object.")
      (:fr "Je ne trouve pas cet objet.")))
(defvar *web-create/update* 
    '((:en "Create/Update...")
      (:fr "Créer/Mettre à jour...")))
(defvar *web-create/update/publish* 
    '((:en "Create/Update/Publish...") 
      (:fr "Créer/Mettre à jour/Publier...")))
(defvar *web-delete* 
    '((:en "Delete...")
      (:fr "Supprimer...")))
(defvar *web-edit* 
    '((:en "Edit new object...")
      (:fr "Editer un  nouvel objet...")))
(defvar *web-editor-title*
    '((:en "OMAS WEB EDITOR") 
      (:fr "OMAS ÉDITEUR WEB")))
(defvar *web-error-back*
    '((:en "To correct, please use your browser to go back to previous page.")
      (:fr "Pour corriger, revenez à la page précédente à l'aide du browser.")))
(defvar *web-error-title*
    '((:en "OMAS WEB EDITOR - Error")
      (:fr "OMAS ÉDITEUR WEB - Erreur")))
(defvar *web-error-zap*
    '((:en " ... or abort all and start all aver again.")
      (:fr " ... ou bien ... On efface tout, on recommence.")))
(defvar *web-locate-data*
    '((:en "Data needed to locate the object, e.g. N-4, Barthès,...")
      (:fr "Élements de localisation, ex: japon, Barthès :")))
(defvar *web-locate-title*
    '((:en "OMAS WEB EDITOR - Selection")
      (:fr "OMAS ÉDITEUR WEB - Sélection")))
(defvar *web-missing-agent*
    '((:en "Owner agent missing.")
      (:fr "Agent concerné non renseigné.")))
(defvar *web-missing-class*
    '((:en "Class (concept) missing.")
      (:fr "Classe (concept) non renseignée.")))
(defvar *web-need-info* 
    '((:en "Please give info to locate object.")
      (:fr "Merci de donner l'information pour localiser l'objet.")))
(defvar *web-no-change*
    '((:en "No change.")
      (:fr "Pas de changement.")))
(defvar *web-no-PA*
    '((:en "Sorry, can't reach your personal assistant.")
      ( :fr "Désolé je ne trouve pas votre assistant personnel.")))
(defvar *web-object-class*
    '((:en "Object class:") 
      (:fr "Classe de l'objet :")))
(defvar *web-objmod* 
    '((:en "Object was modified") 
      (:fr "L'objet a été modifié.")))
(defvar *web-owner-agent*
    '((:en "Agent owning the object:")
      (:fr "Agent concerné (gérant cet objet) :")))
(defvar *web-quit* 
    '((:en "Abort editing this object...") 
      (:fr "Abandonner les modifications...")))
(defvar *web-return* 
    '((:en "Home")
      (:fr "Retour vers la page d'accueil")))
(defvar *web-send*
    '((:en "Execute") 
      (:fr "Envoyer")))
(defvar *web-unreachable-agent*
    '((:en "Unreachable agent. Check spelling.")
      (:fr "Agent non joignable. Vérifiez l'orthographe.")))

(format t "~&;*** OMAS v~A - texts loaded ***" omas::*omas-version-number*)

;;; :EOF
