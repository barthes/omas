;;;-*- Mode: Lisp; Package: "OMAS" -*-
;;;===============================================================================
;;;24/10/15
;;;                P A R A M E T E R S  (file parameters.lisp)
;;;
;;;===============================================================================
;;; This file contains the parameters pertaining to a specific version of OMAS
;;; alike system environment variables

;;; (c) Jean-Paul Barthès@HEUDIASYC, CNRS, UTC. See LICENSE file for permissions.

(in-package :omas)

;;;=========== OMAS directory path
;;; this assumes that the omas files have been installed in the corresponding
;;; folder, using quicklisp inside Allegro folder: 
;;; #P"C:\\Users\\barthes\\Allegro CL 9.0\\quicklisp\\local-projects\\omas\\"
;;; It is used to access files in the applications and sample-applications
;;; folders, or the omas.properties file

(defParameter *omas-directory-string* 
  "~/Allegro CL 9.0/quicklisp/local-projects/omas/")

(defParameter *omas-directory-pathname* (pathname *omas-directory-string*))

(defparameter *omas-application-directory* "~/OMAS-projects/")
  
;;;=========== Current version of OMAS
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defparameter *omas-version-number* "14.0")
  ;; do this before loading :moss to avoid having a stand alone MOSS system
  (pushnew :omas *features*)
  ;; indicate that conversations are handled by OMAS skills
  (pushnew :converse-v2 *features*)
  )

;;;=========== Loading MOSS after we pushed :OMAS into *features*

;(ql:quickload :moss)

(format t "~&;*** OMAS v~A - parameters loaded ***" *omas-version-number*)

;;; :EOF
