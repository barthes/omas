﻿;;;===============================================================================
;;;20/02/07
;;;                N U L L (file :THOR-NULL:agents.lisp)
;;;
;;;                copyright JP BArthès @UTC, 2004
;;;
;;;===============================================================================
;;; This file is a null file used to set up an empty OMAS environment with no agents.

(in-package :omas)

;;; the global variable *local-coterie-agents* is defined in the OMAS load file

(setq *local-coterie-agents* '()) 

;;; :EOF