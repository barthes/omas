;;;===============================================================================
;;;09/07/26
;;;                FACTORIAL (file factorial.lisp) 
;;;
;;;===============================================================================

;;; The FACTORIAL application as described in the "OMAS v7 Primer" document

#|
2009
 0726 adjusting for OMAS v 7.11
|#

#+MCL (in-package :cl-user)
#+MICROSOFT-32 (in-package :cg-user)


(eval-when (compile load eval)
  (use-package :OMAS) ; use keyword to avoid creating OMAS symbol in package FAC
  ) ; reexport it for use in other packages  


;;;===============================================================================
;;;
;;;                                  AGENTS
;;;
;;;===============================================================================

(defagent :FAC)
(defagent :MUL-1)
(defagent :MUL-2)
(defagent :MUL-3)

;;;===============================================================================
;;;
;;;                                  SKILLS
;;;
;;;===============================================================================

(defskill :multiply :mul-1
  :static-fcn static-multiply
  :how-long-fcn static-multiply-how-long
  )

(defskill :multiply :mul-2
  :static-fcn static-multiply
  :how-long-fcn static-multiply-how-long
  )

(defskill :multiply :mul-3
  :static-fcn static-multiply
  :preconditions static-multiply-preconditions  ; test on static part of the skill
  :how-long-fcn static-multiply-how-long
  )

;;;==================================  FACTORIAL =================================

(omas::defskill :dumb-fac :FAC
  :static-fcn static-dumb-fac
  :dynamic-fcn dynamic-dumb-fac
  )

(omas::defskill :dumb-fac-with-time-limit :FAC
  :static-fcn static-dumb-fac
  :dynamic-fcn dynamic-dumb-fac
  :time-limit-fcn time-limit-dumb-fac
  )

(defskill :greedy-fac :fac
  :static-fcn static-greedy-fac
  :dynamic-fcn dynamic-greedy-fac
  :timeout-handler timeout-greedy-fac
  )

(defskill :contract-net-fac :fac
  :static-fcn static-contract-net-fac
  :dynamic-fcn dynamic-contract-net-fac
  )

;;;============================ SKILL FUNCTIONS ==================================

;;;========== MULTIPLY

(defun static-multiply (agent message n1 n2)
  (declare (ignore message))
  ;; slow down the process so that we can follow on the trace
  (sleep (1+ (random 3)))
  (static-exit agent (* n1 n2)))

(defun static-multiply-how-long (&rest ll)
  "Artificial delay between 1 and 3 seconds set randomly."
  (declare (ignore ll))
  (1+ (random 3)))

(defun static-multiply-preconditions (agent message n1 n2)
  "Agent MUL-3 does not know how to multiply numbers greater than 1000."
  (declare (ignore  agent message))
  (if (or (> n1 1000)(> n2 1000))
    (progn
      (format t "~& **** mul-1 can't compute args too big ~S, ~S" n1 n2)
      nil)
    t))

;;;========== FACTORIAL

;;;=============================================================== DUMB STRATEGY 
;;; using always MUL-1 to multiply

(defun static-dumb-fac (agent message nn)
  "Sends first multiply and set up ennvironment to keep track of where we are."
  (declare (ignore message))
  ;; if nn is less than or equal to 1, then return 1 immediately
  (if (< nn 2) (static-exit agent 1)
      ;; otherwise, create a subtask for computing the product of the first 
      ;; two top values
      (let ()
        ;; ship subtask to agent MUL-1 to compute
        (send-subtask agent :to :MUL-1 :action :multiply 
                      :args (list nn (1- nn)))
        ;; define a tag (:n) in the environment to record the value of the next
        ;; products to compute. e.g., nn-2 -> (nn - 2)!
        ;; use the special environment area of the agent (associated with the task)
        (env-set agent (- nn 2) :n)
        ;; quit
        (static-exit agent :done))))

(defun dynamic-dumb-fac (agent message answer)
  "this function is called whenever we get a result from a subtask. This ~
      approach is not particularly clever, since the computation is linear ~
      and uses the same multiplying agent, i.e., MUL-1."
  (declare (ignore message))
  (let ((nn (env-get agent :n)))
    ;; if the recorded value is 1 or less, then we are through
    (if (< nn 2)
      ;; thus we do a final exit (in fact we should never go there ??
      (dynamic-exit agent answer)
      ;; otherwise we multiply the answer with the next high number
      ;; creating a subtask
      (progn
        (send-subtask agent :to :MUL-1 :action :multiply
                      :args (list answer nn))
        ;; update environment
        (env-set agent (1- nn) :n)
        ;; then the function returns a value to nobody in particular
        answer
        ))))

;;;=============================================== DUMB STRATEGY WITH TIME LIMIT
;;; same as dumb strategy but introduces a timeout on the computation of FAC
;;; Time limit handler: cancels job on timeout

(defun time-limit-dumb-fac (agent dummy message)
  "when reaching a time-limit for computing the function we simply abort all ~
      subtasks and quit."
  (declare (ignore dummy message))
  ;; cancel current subtasks
  (cancel-all-subtasks agent)
  ;; and return with :error flag
  (dynamic-exit agent :error))

;;;============================================================= GREEDY STRATEGY

(defParameter *multiply-timeout* 20 "should be no problem with 20 seconds")

(defun static-greedy-fac (agent message nn)
  "Here we assume that we have 3 acquaintances that know how to multiply and ~
      we try to use them as best as we can. I.e., we distribute the first three ~
      multiplications, then each time an answer comes back we send a new multiplication ~
      to the agent that perfomed the subtask."
  (declare (ignore message)(special *multiply-timeout*))
  ;; if nn is less than or equal to 1, then return 1 immediately
  (if (< nn 2) (static-exit agent 1)
    ;; otherwise, create a series of subtasks for computing the product of the  
    ;; first top values
    (let ()
      ;; we initialize a :res value to 1 in the environment (used by dynamic function)
      (env-set agent 1 :res)
      ;; ship subtask to agent MUL-1 to compute
      (send-subtask agent :to :MUL-1 :action :multiply 
                    :args (list nn (1- nn)) :timeout *multiply-timeout*)
      ;; decrease nn
      (decf nn 2)
      ;; when nn is greater or equal than 2 continue
      (when (> nn 3)
        (send-subtask agent :to :MUL-2 :action :multiply 
                      :args (list nn (1- nn)) :timeout *multiply-timeout*)
        ;; decrease nn
        (decf nn 2))
      ;; when nn is greater or equal than 2 continue
      (when (> nn 3)
        (send-subtask agent :to :MUL-3 :action :multiply 
                      :args (list nn (1- nn)) :timeout *multiply-timeout*)
        (decf nn 2))
      ;; define a tag (:n) in the environment to record the value of the next
      ;; products to compute
      (env-set agent nn :n)
      ;; quit
      (static-exit agent :done))))

(defun dynamic-greedy-fac (agent message answer)
  "this function is called whenever we get a result from a subtask. "
  (declare (special *multiply-timeout*))
  (let ((nn (env-get agent :n))
        (res (env-get agent :res))
        (list-of-subtasks (pending-subtasks? agent)))
    ;(format t "~&Dynamic: nn=~s - (pending-subtasks? agent) ~S"
    ;        (cadr (member :res environment)) (pending-subtasks? agent))
    ;; we are finished when this was the last substasks and we do not have
    ;; partial result waiting (res=1)
    (cond 
     ((and (< nn 2) (null (cdr list-of-subtasks))(eql res 1))
      ;; thus we do a final exit, we just received the final answer
      (dynamic-exit agent answer))
     ;; if the recorded value is greater than 1, then we send a new subtask to the
     ;; agent that sent the answer
     ((> nn 1)
      (send-subtask agent :to (sending-agent message) :action :multiply
                    :args (list answer nn) :timeout *multiply-timeout*)
      ;; update environment
      (env-set agent (1- nn) :n)
      ;; then return an answer (actually to nobody in particular)
      answer)
     ;; otherwise we exhausted primary values, we must gather results of MUL
     ;; agents and multiply them together
     ((eql 1 res)
      ;; if 1 then we store partial result for next time around
      (env-set agent answer :res)
      ;; then return an answer (actually to nobody in particular)
      answer)
     ;; otherwise we have a partial result in the environment
     ((not (eql 1 res))
      ;; use it to send a new multiply job
      (send-subtask agent :to (sending-agent message) :action :multiply
                    :args (list answer (env-get agent :res))
                    :timeout *multiply-timeout*)
      ;; reset environment
      (env-set agent 1 :res)
      ;; then return an answer (actually to nobody in particular)
      answer)
     ;; otherwise we are in trouble (something wrong with the algorithm)
     (t (error "bad greedy fac algorithm")))))

(defun timeout-greedy-fac (agent message)
  "function for handling timeout errors"
  (declare (special *multiply-timeout*))
  (let (dead-agent new-choice)
    (case (omas::action message)
      (:multiply
       ;; if a multiply agent is timed out, then reallocate to another one
       ;; first get the name of the agent who was suppose to do the job
       (setq dead-agent (receiving-agent message))
       ;; pick up another agent from the list of acquaintances
       (setq new-choice 
             ((lambda (xx)(nth (random (1- (length xx)))
                               (remove dead-agent xx)))
              '(:MUL-1 :MUL-2 :MUL-3)))
       (format t "~&dead agent: ~S - new-choice: ~S" 
               dead-agent new-choice)
       ;; reallocate the work
       (send-subtask agent :to new-choice :action :multiply
                     :args (omas::args message)
                     :timeout *multiply-timeout*)
       ;; exit
       :done
       )
      (otherwise 
       ;; for any other skill we let the system process the timeout condition
       :unprocessed))
    ))

; (trace (timeout-greedy-fac :step t))
; (trace (timeout-greedy-fac))
; (untrace timeout-greedy-fac)
;;;======================================================= CONTRACT NET STRATEGY

(defun static-contract-net-fac (agent message nn)
  "this skill works by first creating n/2 tasks for distribution to the ~
      multiplying agents, i.e., (* n n-1) (* n-2 n-3) ...
  if n is even we end up with (* 2 1)
   if n is odd we end up with (� 3 2)
  It then broadcasts those tasks, record the left over number (1 or 2) and quits."
  (declare (ignore message))
  ;; we want to control the call for bid delay for the contract net to prevent early
  ;; aborts
  (if (< nn 2) (static-exit agent 1)
      ;; this line should be replaced with (if (< nn 3) (return (max nn 1))
      ;; to avoid subcontracting (* 2 1)
      (let ((delay 0))
        ;; try to produce as many initial subtasks as possible
        (loop
          ;; create n/2 tasks for distribution to the multiplying agents
          ;; create a subtask for computing the product of the next two values
          ;; broadcast
          (send-subtask agent :to :ALL :action :multiply 
                        :args (list nn (decf nn)) :delay delay 
                        :protocol :contract-net)
          (decf nn) ; adjust nn for next round
          ;(incf omas::*default-call-for-bids-timeout-delay* 3) ; increase waiting time
          ;;... to let agents make their bids
          ;(incf delay) ; we assume 1 as cost of setting up subtask
          ;; if nn becomes less than 3 don't multiply 2 by 1! get out of the loop
          (if (< nn 3)(return nil)))
        ;; define the :n tag in the environment to record the value of the next
        ;; products to compute. When exiting the loop it can be 2, 1, or 0. We set
        ;; it to 2 or 1
        (env-set agent (if (< nn 2) 1 nn) :res)
        ;; quit
        (static-exit agent :done))))

(defun dynamic-contract-net-fac (agent message answer)
  "this function is called whenever we get a result from a subtask.
   We structured the environment as follow:
     :res contains a partial result (nil or 2 at the beginning)
   We use :res as follows:
     if 1 we save the result into it
     otherwise, we broadcast the multiplication of :res with the result."
  (declare (ignore message))
  (let (res)
    ;(format t "~&Dynamic: nn=~s - (pending-subtasks?) ~S"
    ;        (cadr (member :res environment)) (pending-subtasks? agent))
    ;; we are finished when there are no more active substasks.
    (cond 
     ((not (pending-subtasks? agent))
      ;; thus we do a final exit
      (dynamic-exit agent answer))
     ((eql 1 (setq res (env-get agent :res)))
      ;; we have still subtasks in progress and res is nil
      ;; store the answer
      (env-set agent answer :res)
      ;; check if this was the last subtask in the list
      (if (null (cdr (pending-subtasks? agent)))
        (return-from dynamic-contract-net-fac (dynamic-exit agent answer)))
      ;; otherwise return answer
      answer)
     (t
      ;; res is not 1, i.e., it contains a partial result
      ;; we multiply the answer with res creating a subtask
      ;; ... broadcasting it
      (send-subtask agent :to :ALL :action :multiply
                    :args (list answer res) :protocol :contract-net)
      ;; update environment
      (env-set agent 1 :res)
      answer))))

;;;===============================================================================
;;;
;;;                            PREDEFINED MESSAGES
;;;
;;;===============================================================================

;;; dumb strategy
(defmessage :DF :to :FAC :type :request :action :dumb-fac :args (4))

;;; dumb strategy with timeout on FAC
(defmessage :DFWTL :to :FAC :type :request 
  :action :dumb-fac-with-time-limit :args (9) :time-limit 5)

;;; greedy strategy
(defmessage :GF :to :FAC :type :request :action :greedy-fac :args (9))

;;; contract-net strategy
(defmessage :CNF :to :FAC :type :request :action :contract-net-fac 
  :args (5))

:EOF 
