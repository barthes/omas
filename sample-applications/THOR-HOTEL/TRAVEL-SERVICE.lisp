﻿;;;===============================================================================
;;;10/02/23
;;;                AGENT TRAVEL-SERVICE
;;;                Barthès, Copyright@UTC, 2004
;;;===============================================================================

(eval-when (compile load eval)
  (unless (find-package :travel-service) (make-package :travel-service)))

;;; define name in the loading environment, so that it can be used without prefix,
;;; export it to the agent package

(in-package :travel-service)

(eval-when (compile load eval)
  (use-package :omas)
  (export '(travel-service))) ; reexport it for use in other packages 

(omas::defagent :travel-service :redefine t)

;;; =============================== skill section ==============================

(defskill :hotel-info-request :travel-service
  :static-fcn static-hotel-info-request
  :dynamic-fcn dynamic-hotel-info-request
  :select-best-answer-fcn best-answer-hotel-info-request
  )

(defun static-hotel-info-request (agent message request)
  "when recieving a request the agent broadcasts it to objects that can answer ~
      an :info-request"
  (declare (ignore message))
  ;; request contains the needed parameters. Keep them in the task environment
  ;; they will be used for selecting the best answer
  (print `(static-hotel-info-request request ,request))
  ;; broadcast demand
  (send-subtask agent 
                :to :all
                :action :info-request
                :args '(:dummy)
                :strategy :collect-answers
                )
  (static-exit agent nil))

(defun dynamic-hotel-info-request (agent environment result)
  "called when an answer returns"
  (declare (ignore environment))
  ;; return the first answer that comes back
  (format t "~&Possible offers: ~S" result)
  (dynamic-exit agent result))

;(defvar *ml*)
(defun  best-answer-hotel-info-request (agent environment message-list)
  "selects the best answer for the travel agent, with default values."
  (declare (ignore agent))
  ;(format t "~&;=== bahir; environment:~&~S" environment)
  ;(setq *ml* message-list)
  ;(format t "~&;=== best-answer-hotel-info-request; message-list:~{~& - ~S~}" 
  ;        message-list)
  (let ((breakfast (omas::alt-list-get environment :breakfast))
        (distance (omas::alt-list-get environment :distance))
        (price (omas::alt-list-get environment :price))
        info ref)
    ;; extract a list with answering agent and content
    (setq info (mapcar #'(lambda (xx) (cons (omas::from! xx) (omas::contents xx)))
                       message-list))
    ;; when breakfast is specified remove entries are not suitable
    (when breakfast
      (setq info (remove-if #'(lambda (xx) 
                                (not (eql breakfast (cadr (member :breakfast xx)))))
                            info)))
    ;; when price condition, get the best-price hotel
    (when price
      (case price
        (:lowest
         (setq info (sort info #'< :key #'(lambda (xx) (cadr (member :price xx)))))
         ;; get reference price AFTER the sort!
         (setq ref (cadr (member :price (car info))))
         ;; remove entries more expensive than the first one
         ;; remove those that are too expensive
         (setq info (remove-if #'(lambda (xx) (> (cadr (member :price xx)) ref)) 
                               info)))
        (:highest
         (setq info (sort info #'> :key #'(lambda (xx) (cadr (member :price xx)))))
         ;; get reference price
         (setq ref (cadr (member :price (car info))))
         ;; remove those that are too expensive
         (setq info (remove-if #'(lambda (xx) (< (cadr (member :price xx)) ref)) 
                               info)))
        ))
    ;; when distance is important, then sort on the distance
    (when distance
      (case distance
        (:shortest
         (setq info (sort info #'< :key #'(lambda (xx) (cadr (member :distance xx)))))
         ;; set ref AFTER the sort!
         (setq ref (cadr (member :distance (car info))))
         ;; remove those that are too far
         (setq info (remove-if #'(lambda (xx) (> (cadr (member :distance xx)) ref)) 
                               info)))
        (:longest
         (setq info (sort info #'> :key #'(lambda (xx) (cadr (member :distance xx)))))
         ;; set ref
         (setq ref (cadr (member :distance (car info))))
         ;; remove those that are not far enough
         (setq info (remove-if #'(lambda (xx) (< (cadr (member :distance xx)) ref)) 
                               info)))
        ))
    ;; return a list of best offers
    info))

:ENV

#+MCL (in-package "COMMON-LISP-USER")
#+MICROSOFT-32 (in-package "COMMON-GRAPHICS-USER")

(eval-when (compile load eval)
  (use-package :travel-service))

;;; ================================= environment ==============================

(defParameter *travel-service-environment* 
  '((:skills :hotel-info-request)))

:EOF