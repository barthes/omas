﻿;;;===============================================================================
;;;10/02/23
;;;                AGENT hotel-2
;;;
;;;=============================================================================== 

(eval-when (compile load eval)
  (unless (find-package :hotel-2) (make-package :hotel-2)))

;;; define name in the loading environment, so that it can be used without prefix,
;;; export it to the agent package

(in-package :hotel-2)

(eval-when (compile load eval)
  (use-package :omas)
  (export '(hotel-2))) ; reexport it for use in other packages 

(omas::defagent :hotel-2 :redefine t)

;;;================================ data section ===============================

(defParameter info
  '(:price 8000 :breakfast :yes :distance 10))

;;; =============================== skill section ==============================

(defskill :info-request :hotel-2
  :static-fcn static-info-request
  )

(defun static-info-request (agent message &rest query)
  (declare (ignore message query))
  (sleep (1+ (random 2)))
  (static-exit agent info))

:EOF