;;;===============================================================================
;;;04/09/28
;;;                H O T E L - A G E N T S (file :THOR-HOTEL:agents.lisp)
;;;
;;;                copyright JP BArth�s @UTC, 2004
;;;
;;;===============================================================================

#| This file contains the name of the agents that must be loaded into the local 
coterie. It is called when OMAS is initialized (v5 and above)
The HOTEL application is an example that was proposed by Prof. Fujita.
|#

(in-package :omas)

;;; the global variable *local-coterie-agents* is defined in the OMAS load file

(setq *local-coterie-agents*
      '(:HOTEL-1 :HOTEL-2 :HOTEL-3 :TRAVEL-SERVICE))

:EOF