﻿;;;===============================================================================
;;;10/02/23
;;;                AGENT HOTEL-1
;;;
;;;===============================================================================

(eval-when (compile load eval)
  (unless (find-package :hotel-1) (make-package :hotel-1)))

;;; define name in the loading environment, so that it can be used without prefix,
;;; export it to the agent package

(in-package :hotel-1)

(eval-when (compile load eval)
  (use-package :omas)
  (export '(hotel-1))) ; reexport it for use in other packages 

(omas::defagent :hotel-1 :redefine t)

;;;================================ data section ===============================

(defParameter info
  '(:price 10000 :breakfast :yes :distance 5))

;;; =============================== skill section ==============================

(defskill :info-request :hotel-1
  :static-fcn static-info-request
  )

(defun static-info-request (agent message query)
  (declare (ignore message query))
  (sleep (1+ (random 2)))
  (static-exit agent info))

:EOF