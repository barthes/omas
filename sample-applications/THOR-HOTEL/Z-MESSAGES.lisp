﻿;;;===============================================================================
;;;04/09/28
;;;                FAC MESSAGES 
;;;
;;;===============================================================================

;;; loaded in the package of the loading environment (usually OMAS)

(in-package :omas) 

(defmessage :test-0 :from :<user> :to :all :type :request :action :info-request
  :args ((:price :lowest)))
(defmessage :test-1 :from :<user> :to :travel-service :type :request 
  :action :hotel-info-request :args ((:price :lowest)))
(defmessage :test-2 :from :<user> :to :travel-service :type :request 
  :action :hotel-info-request :args ((:price :lowest :breakfast :yes)))
(defmessage :test-3 :from :<user> :to :travel-service :type :request 
  :action :hotel-info-request :args ((:price :highest :breakfast :no)))

:EOF