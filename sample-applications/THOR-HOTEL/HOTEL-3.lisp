﻿;;;===============================================================================
;;;10/02/23
;;;                AGENT hotel-3
;;;
;;;===============================================================================

(eval-when (compile load eval)
  (unless (find-package :hotel-3) (make-package :hotel-3)))

;;; define name in the loading environment, so that it can be used without prefix,
;;; export it to the agent package

(in-package :hotel-3)

(eval-when (compile load eval)
  (use-package :omas)
  (export '(hotel-3))) ; reexport it for use in other packages 

(omas::defagent :hotel-3 :redefine t)

;;;================================ data section ===============================

(defParameter info
  '(:price 5000 :breakfast :no :distance 10))

;;; =============================== skill section ==============================

(defskill :info-request :hotel-3
  :static-fcn static-info-request
  )

(defun static-info-request (agent message query)
  (declare (ignore message query))
  (sleep (1+ (random 2)))
  (static-exit agent info))

:EOF