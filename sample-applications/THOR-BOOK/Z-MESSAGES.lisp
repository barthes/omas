﻿;;;===============================================================================
;;;10/02/23
;;;                                  APP MESSAGES 
;;;
;;;===============================================================================

;;; loaded in the package of the loading environment (usually OMAS)

(in-package :omas)

(defmessage :GET-ALICE :type :request :to :book-buyer 
  :action :buy
  :args ("Alice in Wonderland"))

(defmessage :GET-COOKBOOK :type :request :to :book-buyer 
  :action :buy
  :args ("Indian Cooking"))
  
(defmessage :ADD-COOKBOOK :type :inform :to :book-seller-1
  :action :add-book
  :args ("Indian Cooking" 77))

:EOF