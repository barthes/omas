;;;===============================================================================
;;;04/08/07
;;;               Creating the BOOK Application
;;;
;;;===============================================================================

1. Make a copy of the application-template folder.

2. change the name of the folder to <coterie>-<application> e.g. THOR-BOOK
   In our case the name of the local coterie is te same as the name of oour machine.

3. Open the agents.lisp file
  
   3.1 Change the name of the header A P P  -> B O O K
   3.2 Change the name of the file directory   COTERIE-APP -> THOR-BOOK
   3.3 Insert the current date (last modification date)
   3.4 Change Copyright

   3.5 replace the content of the *local-coterie-agents* list by the keywords 
       of the BOOK application agents
       '(:book-buyer :book-seller-1 :book-seller-2)
   3.6 update and close the agents:lisp file

4. Copy the agent-template.lisp file renaming it BOOK-BUYER.lisp, opening it for 
   editing

   4.1 Replace AAA everywhere by the agent name BOOK-BUYER

   4.2 Skills
       The skill section is ready to receive skills
     4.2.1 Copy a skill section as many times as there are skills
     4.2.2 define the skill and associated functions, removing unwanted features
     4.2.3 make sure that the skill name is in the environment variable at the end
           of the file
     4.2.3 if for a skill we need specific macros, globals or function, we insert 
           them into the skill section and add them to the global parameter a-list
           attached to the section

     Example: BOOK-SELLER-1
     A book-seller can receive three types of messages:
        - :offer-request, to answer a request about the price of a title
        - :purchase-order, to make a sale
        - :add-book, to add a new book to the stock
     All skills are straightforward and only require static functions. We assume
     to keep things very simple that the title of the bookis kepts on a list of
     books in memory of the form ((<title> . <price>*)).
     We copy the skill section 3 times, keeping only the static function.
     In the first copy, we replace XXX with OFFER-REQUEST, and fill in the necessary 
     code, i.e.,

(defun static-offer-request (agent message title)
  "service a request for a particular title. If there return its price, ~
      otherwise does not bother answering."
  (let ((stock (assoc title (recall agent :books))))
    (if stock
      (static-exit agent (cdr stock))
      (static-exit agent :abort))))

     We do the same for a purchase order and the add-book skills.

     Example: BOOK-BUYER
     The book-buyer is slightly more complex since it has to carry out a conversation.
     We need a skill that asks for the availability of a book :REQUEST-PERFORMER
     The skill will have a static and dynamic part. Thus we copy a skill section,
     replace XXX with REQUEST-PERFORMER, keep only the :static-fcn and :dynamic-fcn 
     options. 
     We need a second skill that we process messages coming from the user specifying
     the title of the nex book to buy. We call it BOOK-TO-BUY. It only requires a
     :static-fcn option

   4.3 Goals
       Only the buyer has a goal, that of asking for a title every minute until it gets 
       an answer.

   4.4 Initial conditions
       We can use the final part of the file (initial conditions to specify some data
       at the creation of the agent.

       Example: BOOK-SELLER-1
       We give two books to the first seller, by adding the line
     
     (deffact book-seller-1 '(("Alice in Wonderland" . 55)
                              ("Agents Unleashed" . 70))
       :books)
 
       and note that it does not have any money yet
       (deffact book-seller-1 0 :money)

   4.5 Initial messages
       We can define several messages to test our agents, e.g.

       (defmessage :GET-ALICE :type :request :to :book-buyer 
         :action :REQUEST-PEFORMER
         :args ("Alice in Wonderland"))

       (defmessage :GET-COOKBOOK :type :request :to :book-buyer 
         :action :REQUEST-PEFORMER
         :args ("Indian Cooking"))
       

5. Setting up the application
   This is done by duplicating the seller file to as many sellers as needed, updating 
   the list of agents and preparing some messages for debugging in the Z-messages.lisp 
   file.
   Then one launches the OMAS environment and loads the BOOK application. The debug 
   panel will help to visualize what is going on.