﻿;;;===============================================================================
;;;04/08/07
;;;               B O O K - A G E N T S (file :THOR-BOOK:agents.lisp)
;;;
;;;                copyright JP Barthès @UTC, 2004
;;;
;;;===============================================================================

;;; This file contains the name of the agents that must be loaded into the local 
;;; coterie. It is called when OMAS is initialized (v5 and above)

(in-package :omas)

;;; the global variable *local-coterie-agents* is defined in the OMAS load file
;;; agents should be referred to by a keyword (e.g. :book-buyer)

(setq *local-coterie-agents* 
      '(:book-buyer :book-seller-1 :book-seller-2))

:EOF
