﻿;;;-*- Mode: Lisp; Package: "BOOK-BUYER" -*-
;;;===============================================================================
;;;10/05/29
;;;                               AGENT BOOK-BUYER
;;;
;;;===============================================================================

(defpackage :BOOK-BUYER (:use :cl #+MCL :ccl :moss :omas))
(in-package :BOOK-BUYER)

(omas::defagent BOOK-BUYER :redefine t)

;;;===============================================================================
;;; 
;;;                         service macros and functions 
;;;
;;;===============================================================================


;;; ==================================== globals =================================
;;; Globals  can be used within the agent space to simplify programming, although
;;; it is better to use the agent memory.


;;;==============================================================================
;;; 
;;;                                    goals
;;;
;;;==============================================================================

;;; ================================= goal section ==============================
"macro that builds a goal structure for an agent.
Arguments:
   goal-name:              name of the goal that will be used to create task-ids
   agent-name:             name of the agent to receive the goal
keys arguments:
   mode:                   activation mode (:rigid or :flexible)
   type:                   type of goal (:permanent :cyclic :1-shot)
   period:                 period for cyclic goals (default is 20s)
   expiration-date:        date at which the goal dies
   expiration-delay:       time to wait until we kill the goal
   importance:             on a 1-100 scale (not yet available)
   urgency:                on a 1-100 scale (not yet available)
   activation-date:        date at which the goal should fire (default is now)
   activation-delay:       time to wait before activating the goal
   activation-level:       on a 1-100 scale (default 50)  (not yet available))
   activation-threshold:   on a 1-100 scale (default 50)  (not yet available)
   activation-change-fcn:  optional function called at each cycle (not yet available)
   status:                 waiting, active, dead,... 
                              may not be useful with activation
   goal-enable-fcn:        function checking the goal enabling conditions
   script:                 script a function producing a message list
Return:
   goal-name."

(defgoal  :BUY-BOOK :BOOK-BUYER
  :type             :cyclic
  :period           20           ; seconds
  :goal-enable-fcn  enable-buy-book
  :script           goal-buy-book)

(defun goal-buy-book (agent)
  (list (make-instance 'omas::message :type :request
                       :from (omas::key agent) :to (omas::key agent)
                       :action :request-performer
                       :args (list (car (recall agent :books-to-buy)))
                       :task-id :BUY-BOOK
                       )))

;;; return T to enable the goal NIL ti inhibit it

(defun enable-buy-book (agent script)
  "goal is enable each time there is something in the books-to-buy"
  (declare (ignore script))
  (recall agent :books-to-buy)
  )

;;;===============================================================================
;;; 
;;;                                    skills 
;;;
;;;===============================================================================

;;; ================================= skill section ==============================

(defskill :BUY :BOOK-BUYER
  :static-fcn static-BUY
  )

(defun static-buy (agent message title)
  "Simply adds the title of the book to buy to the :books-to-buy items"
  (declare (ignore message))
  (remember agent (cons title (recall agent :books-to-buy)) :books-to-buy)
  (static-exit agent :will-buy))

;;; ================================= skill section ==============================

(defskill :REQUEST-PERFORMER :BOOK-BUYER
  :static-fcn static-REQUEST-PERFORMER
  :dynamic-fcn dynamic-REQUEST-PERFORMER
  :select-best-answer-fcn select-best-answer-REQUEST-PERFORMER
  )

(defun static-request-performer (agent message title &aux number-of-attempts)
  "Tries to buy a book by broadcasting an offer, and taking the ~
      cheapest answer. Quits after 3 tries."
  (declare (ignore message))
  
  ;; check the number of attempts
  (setq number-of-attempts (recall agent :number-of-attempts))
  (cond 
    ((and number-of-attempts (> number-of-attempts 3))
     ;; if too high quit, removing the book from the list of books to purchase
	 (remember agent (remove title (recall agent :books-to-buy) :test #'equal) 
                :books-to-buy)
	 ;; reset number of attempts for the next purchase
	 (remember agent nil :number-of-attempts)
	 (static-exit agent :abort))
	;; if not the first attempt, then update the number of attempts
	(number-of-attempts
	  (remember agent (1+ number-of-attempts) :number-of-attempts))
	;; if first attempt, then initialize
	(t (remember agent 1 :number-of-attempts)))
	
  ;; ask now about the prices of the book
  (send-subtask  agent :to :all 
                 :action :offer-request 
                 :args (list title)
                 :strategy :collect-answers 
                 :timeout 4   ; allow,4 seconds to answer
                 )
  ;; write title of the book to buy in the task memory
  (env-set agent title :trying-to-buy)
  (static-exit agent :done))

(defun dynamic-request-performer (agent message data)
  "Whenever we get an answer here, we send a purchase order."
  ;; the first time we enter this function is after the broadcast timeout
  ;; the second time is when we get an answer from sellers
  ;; we can detext that by the presence of a :purchasing marker in the environment
  (let ((title (env-get agent :trying-to-buy)))
    (omas::vformat "~&;=== dynamic-request-performer; data: ~S" data)
    (cond
     ;; first time around
     ((null (env-get agent :purchasing))
      ;; send purchase order whenever data is non nil
      (if data
	     (progn
          (send-subtask agent :to (omas::from! message)
                        :action :purchase-order
                        :args (list title))
          ;; set up marker in the environment
          (env-set agent t :purchasing)
		  ;; reset number of attempts
		  (remember agent nil :number-of-attempts)
		  )
        ;; when no data (e.g. on timeout) then quit
        (dynamic-exit agent :abort)))
     
     ;; second time around, answer to the purchasing order
     ((eql data :done)
      ;; record that we own now the book
      (remember agent (cons title (recall agent :my-books)) :my-books)
      ;; remove it from the list of books to buy
      (remember agent (remove title (recall agent :books-to-buy) :test #'equal) 
                :books-to-buy)
      ;; should also decrease the amount of money... (not done)
      (dynamic-exit agent  "book purchased"))
     ;; otherwise we have an error and give up, we'll try some other time
     (t
      (dynamic-exit agent :abort)))))

(defun select-best-answer-request-performer (agent answer-message-list)
  "keep the cheapest offer"
  ;; order the collected answer messages by increasing prices
  ;; we do that only when there is more tan one answer
  (print answer-message-list)
  ;; remove messages with a non numeric answer
  (setq answer-message-list 
       (remove-if-not #'numberp answer-message-list :key #'omas::contents))
  ;; if nothing left return nil
  (unless answer-message-list 
       (return-from select-best-answer-request-performer nil))
	   
  ;; otherwise rank answers                        
  (when (cdr answer-message-list)
    (setq answer-message-list 
          (sort answer-message-list #'<= :key #'omas::contents)))
  ;; select the message corresponding to the lowest price 
  (list (car answer-message-list)(omas::contents (car answer-message-list)))
  )

;;;===============================================================================
;;; 
;;;                               initial conditions 
;;;
;;;===============================================================================

;;; none

:EOF