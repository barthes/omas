﻿;;;-*- Mode: Lisp; Package: "BOOK-SELLER-2" -*-
;;;===============================================================================
;;;10/05/29
;;;                               AGENT BOOK-SELLER-2
;;;
;;;===============================================================================

(defpackage :BOOK-SELLER-2 (:use :cl #+MCL :ccl :moss :omas))
(in-package :BOOK-SELLER-2)

(omas::defagent BOOK-SELLER-2 :redefine t)

;;;===============================================================================
;;; 
;;;                         service macros and functions 
;;;
;;;===============================================================================

(defun norm (string)
  "removes all spaces and capitalize title"
  (remove '#\space (string-upcase string)))

;;; ==================================== globals =================================
;;; Globals  can be used within the agent space to simplify programming, although
;;; it is better to use the agent memory.

;;; ================================= skill section ==============================

(defskill :OFFER-REQUEST :BOOK-SELLER-2
  :static-fcn static-OFFER-REQUEST
  )

(defun static-offer-request (agent message title)
  "service a request for a particular title. If there return its price, ~
      otherwise does not bother answering."
  (declare (ignore message))
  (let ((stock (assoc title (recall agent :books) :test #'string-equal)))
    (if stock
      (static-exit agent (cdr stock))
      (static-exit agent nil))))

;;; ================================= skill section ==============================

(defskill :PURCHASE-ORDER :BOOK-SELLER-2
  :static-fcn static-PURCHASE-ORDER
  )

(defun static-purchase-order (agent message title)
  "service a request for a particular title. If there return its price, ~
      otherwise return an error"
  (declare (ignore message))
  (let* ((books (recall agent :books))
         (stock (assoc title books :test #'string-equal)))
    (when stock
      ;; remove the book from the stock
      (setq books (remove (car stock) books :key #'car :test #'string-equal))
      ;; increase the amount of money
      (remember agent (+ (cdr stock) (recall agent :money)) :money)
      ;; update memory
      (remember agent books :books)
      (return-from static-purchase-order
        (static-exit agent :done)))
    ;; otherwise we send back an error answer
    (static-exit agent '(:sorry "book exhausted"))))

;;; ================================= skill section ==============================

(defskill :ADD-BOOK :BOOK-SELLER-2
  :static-fcn static-ADD-BOOK
  )

(defun static-add-book (agent message title cost)
  "add the specific book to the stock"
  (declare (ignore message))
  (let ((books (recall agent :books)))
    (push (cons title cost) books)
    (remember agent books :books)
    (static-exit agent :done)))

;;;===============================================================================
;;; 
;;;                               initial conditions 
;;;
;;;===============================================================================

(deffact SA_BOOK-SELLER-2 (("Alice in Wonderland" . 60)
                         ("MAD MAX" . 35))
  :books)

(deffact SA_BOOK-SELLER-2 0 :money)

:EOF