﻿THOR BOOK Example
=================
This example is intended to demonstrate the goal mechanism.

Agent BUYER has a cyclic goal of purchasing a book as long as the request is in its memory.

When sending the first message (purchase Alice) it will store the request. The goal will 
then fire and send a message to the sellers. The book is available and will be purchased.

In the second case "Indian cooking" the book is not available and the buyer will keep asking 
periodically the sellers (here indefinitely since the purchasers do not have the book), 
unless we send an :add-book message with the title and the cost to one of the sellers.