﻿;;;-*- Mode: Lisp; Package: "BOOK-SELLER-1" -*-
;;;===============================================================================
;;;10/05/29
;;;                               AGENT BOOK-SELLER-1
;;;
;;;===============================================================================

(defpackage :BOOK-SELLER-1 (:use :cl #+MCL :ccl :moss :omas))
(in-package :BOOK-SELLER-1) 

(omas::defagent :BOOK-SELLER-1 :redefine t)

;;;===============================================================================
;;; 
;;;                         service macros and functions 
;;;
;;;===============================================================================

(defun norm (string)
  "removes all spaces and capitalize title"
  (remove '#\space (string-upcase string)))

;;; ==================================== globals =================================
;;; Globals  can be used within the agent space to simplify programming, although
;;; it is better to use the agent memory.

;;; ================================= skill section ==============================

(defskill :OFFER-REQUEST :BOOK-SELLER-1
  :static-fcn static-OFFER-REQUEST
  )

(defun static-offer-request (agent message title)
  "service a request for a particular title. If there return its price, ~
      otherwise does not bother answering."
  (declare (ignore message))
  (let ((stock (assoc title (recall agent :books) :test #'string-equal)))
    (sleep (/ (1+ (random 5)) 5))
    (if stock
      (static-exit agent (cdr stock))
      (static-exit agent :abort))))

;;; ================================= skill section ==============================

(defskill :PURCHASE-ORDER :BOOK-SELLER-1
  :static-fcn static-PURCHASE-ORDER
  )

(defun static-purchase-order (agent message title)
  "service a request for a particular title. If there return its price, ~
      otherwise return an error"
  (declare (ignore message))
  (let* ((books (recall agent :books))
         (stock (assoc title books :test #'string-equal)))
    (omas::vformat "~&requested title: ~S~&  Available books: ~S~&  Stock: ~S"
      title (recall agent :books) stock)
    (sleep (/ (1+ (random 5)) 2))
    (when stock
      ;; remove the book from the stock
      (setq books (remove stock books :test #'equal))
      ;; increase the amount of money
      (remember agent (+ (cdr stock) (recall agent :money)) :money)
      ;; update memory
      (remember agent books :books)
      (return-from static-purchase-order
        (static-exit agent :done)))
    ;; otherwise we send back an error answer
    (static-exit agent '(:sorry "book exhausted"))))

;;; ================================= skill section ==============================

(defskill :ADD-BOOK :BOOK-SELLER-1
  :static-fcn static-ADD-BOOK
  )

(defun static-add-book (agent message title cost)
  "add the specific book to the stock"
  (declare (ignore message))
  (let ((books (recall agent :books)))
    (push (cons title cost) books)
    (remember agent books :books)
    (static-exit agent :done)))

;;;===============================================================================
;;; 
;;;                               initial conditions 
;;;
;;;===============================================================================

(deffact SA_book-seller-1 (("Alice in Wonderland" . 55)
                         ("Agents Unleashed" . 70))
  :books)

(deffact SA_book-seller-1 0 :money)

:EOF