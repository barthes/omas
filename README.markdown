﻿# Omas
OMAS is a platform for building multi agent systems. It requires ACL, QUICKLISP for loading subsystems
## Usage
OMAS is used for designing systems of relatively small complex cognitive agents. It requires the MOSS knowledge representation system for implementing agent ontologies and allow agent reasoning. OMAS allows persistency and versioning inherited from the MOSS system.
## Installation
Running OMAS requires installing ACL from Franz Lisp, which requires a licence. Tests can be done with AllegroExpress.