﻿OMAS / MOSS 14.0 note (April 2019)
==================================

OMAS has a complex mechanism of re-sending messages on timeouts automatically, up to a number 
of times. This was inspired by what most OS do, but leads to a complex code due to the 
asynchronism of the messages and the various delays in the transmission of messages. 
In version 14 I decided to simplify the code by moving the responsibility to assign various 
timeouts to the application. I.e. if there is no answer before a timeout fires, then either 
a timeout handler has been provided, or the task is aborted (default). This makes sense 
including when using Contract Net, since the application is more knowledgeable about the 
various timeout delays it should assign in the different protocols.
Another point induced by the repeated sends was the cluttering of messages in case of no 
answer.

190405: Saving a running version of OMAS/MOSS in which time limits have been replaced by 
        with-timeout before removing :repeat-count. This decreases the number of threads that
        must be created by leaving the timing count to the OS.


Changes visible at user's level
-------------------------------
- none


OMAS / MOSS 13.1 note (October 2017)
====================================


Changes visible at user's level
-------------------------------
- 

Dialogs are being changed



Internal changes
----------------


OMAS / MOSS 13.0 note (April 2016)
==================================


Changes visible at user's level
-------------------------------
- versioning should now work with this version.
- Forking dialogs ha been reinstalled, which seems to be a problem with Japanese dialogs.
- MOSS: a single method =add-values has been developed. It can handle attributes and relations.

Internal changes
----------------
Many bugs hava been corrected to let versioning work. A number of MOSS and OMAS library functions have been modified.

OMAS / MOSS 12.1.0 note (June 2015)
==================================

Changes visible at user's level
-------------------------------
- a tracing facility has been installed so that one can trace selectively some parts of
  the code using the dformat function (d for debug), controlling the trace level
- omas code has been modified so that inform messages can send any king of messages to other agents. They are now similar to request messages with the difference that they do not generate an answer

Internal changes
----------------
- a handle has been added to the make-row function for building a row of the edit page;
  this allows to compute the value to be posted by calling a user-defined function
- inform messages are now added to the agenda and processed like request messages
- tracing internal omas function requires using :O-xxx labels

OMAS / MOSS 12.0.0 note (July 2014)
===================================

Changes visible at user's level
-------------------------------
- Multilingual facilities have been rewritten and the internal format has been changed 
  according to recommendations of ELS 2014. The format is now:
  ((:en "wine" "liquor")(:fr "vin" "picrate"))
  All functions are not in a separate package named :MLN. MOSS functions starting with 
  %mln- have been remove and replaced by functions of the :MLN package. To ease 
  transition the mln::make-mln function translate the old format into the new one.
  However, there is no upward compatibility with the previous versions
- An OMAS Web Editor has been developed allowing editing objects from a web interface.
- defagent, defassistant have new additional arguments, allowing to run them from
  a web interface
- agents can share dialog, ontology or tasks files
- Personal Assistants can be created dynamically, which combined with the sharing of 
  files allows having a number of people to interface with the same application
- the NEWS application has been completed (alpha state) and can be examined as an
  example of a complex asynchronous appplication, using the features already mentioned

Internal changes
----------------
- A number of bugs have been corrected
- new functions and methods have been introduced


OMAS / MOSS 10.0.5 note (March 2014)
====================================

Changes visible at user's level
-------------------------------
- the overview window displaying an ontology is now updated after editing an instance
  of a class (individual of the ontology). The Editor is now prevented from editing
  classes.



OMAS / MOSS 10.0.4 note (February 2014)
=======================================

Changes visible at user's level (concerns mainly the OMAS Web Editor)
-------------------------------
- This release contains an experimental version of the OMAS Web Editor in a better shape
  then the previous one in version 10.0.3, but is still in bêta version
- There is a user manual and programming manual associated with the Web Editor

Internal changes
----------------
- some bugs have been corrected in functions used by the Web Editor
- a new MOSS method =make-object-description has been added to the kernel to allow sharing
  objects among agents
- default methods have been added to each agent to allow editing objects in their package

OMAS / MOSS 10.0.2 note (November2013)
======================================

Changes visible at user's level
-------------------------------
- It is now possible to access a set of agents through a Web agent. OMAS uses AllegroServe
  to publish a page that will accept queries from a Web browser.
- It is possible to remotely edit or create MOSS objects using a web browser. However, no
  check-in or check-out mechanism is provided
- A new postman as been developed to facilitate connections between OMAS and JADE.
  Exchanges may be coded using JSON objects.
- A moss.web file has been added to let application display data as dynamic graphs or 
  pie charts or histograms (M. Fuckner). The UTC-GRAPH_CHART_EXAMPLE has been added to 
  the sample-application folder.

- When using the JADE/OMAS postman, the arguments to a skill take the format of a p-list
  Hence the skill may use keywords or the &rest construct to retrieve the values of the
  arguments.
- This new syntax is recommended for organizing the arguments of a skill. Example:
  (<agent> <message> :data ("have we contacts in India?) :language :en 
           :pattern ("contact" ("correspondent")))
  The new syntax organizes the arguments as a p-list after the first 2 compulsory ones
  Values associated with the arguments can be retrieved by declaring
  (defun static-<skill> (agent message &key data language pattern) ...)
- when this syntax is adopted it is no longer possible to access old agents unless their
  skills are modified.

Internal changes
----------------
- exchanges with JADE currently use latin-1 encoding. That should be modified in the next
  future to allow UTF-8.
- an omas-json.lisp library has been added to the OMAS files to encode and decode JSON
  strings.
- a new moss-list-to-json has been added to the library omas-json.lisp
- a set of internal functions has been added to OMAS to support Web editing. Additional
  default skills have been added to the agent model. A new =make-object-description has
  been added to the set of MOSS kernel methods, to produce a string expansion of any
  object.


OMAS / MOSS 10.0.1 note (April 2013)
====================================

Changes visible at user's level
-------------------------------
- content-language has been added to messages
- redefine and learning options have been added to defxxx macros for backward
  compatibility but have no effect
- when starting OMAS, there are now several possible cases:
  - if the SHIFT key is hold, then the green window is shown to select application
  - if the CTRL key is hold, then nothing is loaded ans Lisp is started raw
  - if the ALT key is hold, then MOSS is started without OMAS
  - otherwise, OMAS is started as usual and either the green window is shown, or the
    last selected application is loaded automatically

	
Internal changes
----------------
- install and upgrade files have been modified to add allegrostore when ACL 9.0 is 
  used. This is needed for agent persistency.
  
  

OMAS / MOSS 10.0.0 note (January 2013)
======================================

Changes visible at user's level
-------------------------------
- new OMAS now runs under ACL 9.0 and Allegro Express 9.0
- the defpostman syntax has been modified to include local site and transfer protocol


OMAS / MOSS 9.1.3 note (January 2013)
=====================================

Changes visible at user's level
-------------------------------
- some options for the definition of agents have been modified:
  - learning and redefine options have been removed because unused
  - package has been removed, an agent is now automatically created in its own package
    and its package becomes the current package (this changes the first tutorial)
- SA agent window have now 3 more buttons, only valid if the agent is persistent:
  - a checkpoint button that produces a textual dump of the ontology and knowledge base 
    partition
  - a restore button to reload a checkpointed file into the database (no effect on PAs)
  - an export button that produced a textual definition file for ontology and knowledge
    base
	
Internal changes
----------------
- a number of bugs have been corrected.
- the omas::*omas* variable is now exported to agent packages
	

OMAS / MOSS 9.1.2 note (October 2012)
=====================================

Changes visible at user's level
-------------------------------
- MOSS has been upgraded to allow resizing the ONTOLOGY window that is used by
  OMAS agents
- clicking the TRACE button on a PA window opens a window in which the dialog trace
  is printed
  
Internal changes
----------------
- the vformat tracing printing macro has been changed to a function, which requires
  recompliling the code for a new release
- some of the vformat instructions have been taken out for improving dialog traces

OMAS / MOSS 9.1.1 note (October 2012)
=====================================

This version introduces additional protocol mechanisms for communications (see the
OMAS documentation for detailed information).

Changes visible at user's level
-------------------------------
- protocols are now :TCP, :HTTP, :CLIENT
  - TCP is the standard inter-postman protocol and uses port 52008
  - HTTP uses Allegroserve and port 80
  - CLIENT is a client connection to an external postman running Allegroserve and
    uses port 80. It can be used from behind a firewall.
- the syntax of defpostman changed as a resut of introducing the new protocols
  The main difference is that each connection can now use its own protocol.
Minor changes
- the active connections now appear first in the postman window (at the top of
  the list)
  
Internal changes
----------------
- a bug has been corrected for taking care of disconnections. It is no longer
  necessary to restart OMAS after a transfer (socket) error, except in rare
  cases
  
Remaining problems
------------------
- Japanese strings are not exchanged correctly. Needs ACL 9.0 for a UNICODE 
  environment

OMAS / MOSS 9.0.3 note (September 2012)
=======================================

This version cleans up the postman mechanism and the voice interface. A tentative 
HTTP protocol is implemented for the postmen (transfer agents). the released
version has been compiled for ACL 8.2 and for the new ACL 9.0.

Changes visible at user's level
-------------------------------
- the defassistan macro has been modified to allow choosing the socket ports for
  communicating with the voice mechanism.
- the defpostman macro has been modified to allow specifying an HTTP protocol (uses
  port 80) and uses AllegroServe.
- the defpostman macro has been modified to let the user specify if the target agent
  is a local agent (situated on a loop inside the firewall) or if the machine is
  outside the firewall. A postman description format was introduced to accomodate
  the new information.

OMAS / MOSS 9.0.0 note (April 2012)
===================================

OMAS v9 introduces the notion of FIPA compliance, allowing OMAS to be called using 
the FIPA standards. A chapter was added to the documentation explaining what has been
dome to make OMAS FIPA compliant.
In addition a number of improvements have been done to the system.

Changes visible at user's level
-------------------------------
- the init window has been modified to allow the application to load directly after
  the HIDE button has been clicked
- the default assistant interface has been simplified to show only two areas, with
  the possibility to call the more detailed window if needed.
- a salient feature mechanism has been introduced to allow dialogs to interpret 
  referent pronouns or undo commands.
- a new protocol for postmen, HTTP, has been implemented to use port 80 rather than
  port 52008. It uses Allegroserve as a server mechanism.
- a new FIPA transfer agent has been developed to allow interacting with OMAS as if
  it were a FIPA platform.

Upgrading to a new version
--------------------------
Perform the following steps:
1. Load the new released version into the same folder as the previous one.
2. Start ACL which should load current version of OMAS. If not, load current version
   of OMAS
3. Go to the File menu, select the "compile and load ..." entry
   Select the "upgrade.lisp" file in the OMAS-MOSS folder
4. You will be asked to select the folder of the new released version
5. You will be asked to allow clobbering the startup.cl file. Answer yes, unless 
   you are using it for launching things other than OMAS
6. The rest should be automatic. For your information, your MOSS and OMAS 
   application files will be transferred to the new release, and the startup.cl 
   file will be upgraded, so that next time you start Lisp the new version of OMAS
   is started automatically
7. Exit
8. Check that your applications have been transferred.
9. Restart ACL. It should boot on the new version and your applications folders 
   should contain your applications.


OMAS / MOSS 8.2.0 note (December 2011)
======================================

Changes visible at user's level
-------------------------------
Several changes are now implemented in this version of OMAS.
- a new install mechanism is available when installing the platform for the first time
- a new upgrade mechanism is available for upgrading to a higher version
- the application folder has been split into applications and sample-applications.
  Sample applications contains examples of applications that can be opened and run. 
  The application folder is meant to contain personal applications that will be
  transferred when upgrading to a new platform version.


OMAS / MOSS 8.1.5 note (December 2011)
======================================

Changes visible at user's level
-------------------------------
- upgrading is now done by means of a new function called (omas-upgrade). The new
  released folder must be installed next to the current one in the ACL folder. The
  update function is called from the cg-user package in the IDE. It transfers the 
  current application files to the new release folder and upates the startup.cl
  file in the ACL file.
- a new mechanism is installed to simplify upgrading to a new version, or installing
  for the first time
 
  We want to simplify the upgrade to a new version of the platform. We'd like to
  upgrade the OMAS/MOSS version with  an intelligent update of the set of applications.
  The best procedure is to copy the MOSS/applications and the OMAS/applications folder
  into the new OMAS-MOSS release folder, transferring the local applications.
  The startup.cl file should also be updated to point to the new release.
  Two special folder entitled sample-applications should be part of the distribution,
  for people who want to view some already running applications.
  Procedure
  ---------
  Assume that we are running the "OMAS-MOSS Release 8.1.5 (ACL 8.2)" and we want to
  upgrade to "OMAS-MOSS Release 8.1.6 (ACL 8.2)"
  1. Copy the new released file into the Program File/Allegro 8.2 folder
  2. open the File menu and select "compile and load ..."
  3. Select the upgrade.lisp file in the OMAS-MOSS folder, e.g. "OMAS-MOSS Release 8.1.6
    (ACL 8.2)"
  4. The rest should be automatic (transfer of the current application folders, MOSS
     and OMAS, and update of the startup.cl file)
  5. Then quit the current version and restart ACL. It should boot on the new version.

  Special cases
  -------------
  First installation
  1. Start ACL
  2. go to the File menu, select compile and load...
  3. select the install.lisp file from the Release folder
  4. If the startup.cl file exists, then either it must be deleted or upgraded manually to select the right starting procedure.
  5. If the startup file is upgraded OMAS will start automatically next time lisp is
     opened.
	 
- Automatic loading: If one wants to bypass the "green init window" and load an 
  application directly, one must add a line into the OMAS configuration file 
  (omas.parameters) as follows:
  AUTOMATIC = T
  To cancel the mechanism, one mush press the shift key before launching the Lisp
  
Internal Changes
----------------
- adding an install file containing the install and an upgrade file containing the
  upgrade functions.
- a bug concerning the order of the fields in an OMAS message has been corrected,
  reinstalling compatibility with prior versions.


OMAS / MOSS 8.1.3 note (August 2011)
====================================

Changes visible at user's level
-------------------------------
- the major change is the possibility to use a web interface to access any agent in the system, although this should be used only with PAs.
  Please refer to the documentation
- patterns in dialogs have been simplified, e.g. (:* "contacts" "en" ?x) is equivalent to ((?* #:g0023) "contact" "en" (?* ?x)) :* allows skiping any part of the sentence and ?x being a phrase variable.
- complete PA dialog shows in assistant pane by default, removing the need to use the :show-dialog option. Consequently it is not necessary to start questions with newlines.
- the "pass every char" mode of the PA can be terminated by typing the combination "?." Thus, it is no longer necessary to click the TERMINATE button.
- new options have been added to the defstate macro
   :clean-fact <FACT slot>, clean-facts <list of FACT slots>, :clean-all-facts
   :exec can be used inside a transition clause
- a KILL button has been added to the MOSS object editor, which allows to clean agent ontologies.
- a timeout option has been added to the messages sent by the PA. When the timeout expires, the agent gets a :failure message and control returns to the master
- some bugs have been corrected

Internal Changes
----------------
- adding an omas-web file to start the Allegro aserve web server.
- a preview of ACL v9 has been tested, which handles UNICODE files. Thus, now UNICODE files can be edited in the ACL editor. Furthermore, all languages can be printed using UTF-8 encoding. E.g., one can mix Japanese, Brasilian and French, which was not possible previously.


OMAS / MOSS 8.1.1 note (March 2011)
===================================

Changes visible at user's level
-------------------------------
- A number of changes have been implemented to take into account Japanese requirements.
This concerns mainly PAs.
- When creating a PA, one can now specify the font to be used for printing. Standard default font "Tahoma / ANSI" did not print Japanese characters. Another way to do that is to change the default font in the ACL options. It is activated by using the :font and :size options when creating the assistant: e.g.
   (omas::defassistant :HDSRI :redefine t :language :font "Tahoma / ANSI" :size 8)
For Japanese "Arial Unicode MS" can be used.
- IMPORTANT: When using the ":language :jp" option when defining a PA, one has to make sure that the "aclmecab" folder is present at the same level as the "OMAS-MOSS vxxx" folder. The aclmecab folder contains the necessary files for segmenting Japanese inputs.

- The possibility of tracing the dialog was also introduced in the Assistant pane of the interface window. It is activated by specifying the :show-dialog option when creating the assistant: e.g.
   (omas::defassistant :HDSRI :redefine t :language :fr :show-dialog t)   
The result is a printout in the assistant pane alternating master's requests (printed in red) with PA's answers (printed in black).

- Both previous options can be specified jointly.

- When defining an assistant with the :language option set to :jp, the system uses the Japanese segmentation algorithm to process the input. However, for the segmentation to work, the folder "aclmecab" should be installed in the same folder as the "OMAS-MOSS" folder.

Note also that inputting Japanese sentences requires to set the XP locale to Japanese.

- Agent windows: It is now possible to define windows attached to service agents. Such windows are created by the scan process to avoid their disappearance when created by a transient process (e.g. executing a skill). The :display performative is a convenient mechanism intended to be called internally (i.e. :from and :to refer to the current agent).
Agent windows are defined in the agent package and can use any feature they want. However the window class must be a subclass of the OMAS::OMAS-WINDOW to allow bookkeeping when the window is closed.
Example of skill creating a window:

(defun static-start (agent message)
  "initialize the bidding, creating a window"
  (declare (ignore message))
  ;; create a message for opening the window
  (let ((msg (make-instance 'omas::message  :type :display :action :create
                            :from :DELOS-bidder :to :DELOS-bidder
                            :args '(:bidder-window BIDDER-WINDOW 
					           create-bidder-window))))
    ;; create window
    (send-message msg)
    ;; exit
    (static-exit agent :done)))
The agent :DELOS-BIDDER sends the message to itself, with action :create-window and arguments:
  :bidder-window : the name of the window to be crated
  BIDDER-WINDOW : the name of the window class
  create-bidder-window : the name of a function to apply to the previous args to actually create the window.
Note that we use symbols here, which is OK since we stay in the :delos-bidder package.

- Actions associated with the :display performative are:
  :create : creates the window
  :close : closes the window (usually the window is closed by cicking the close button)
  :erase : erase the content of the window (?) 
  :show : brings the window to the from
  :execute : executes an action (presumably to draw or print something into the window
Please, refer to the documentation for more details.

The AUCTION application gives an example of defining and using SA windows.

Internal Changes
----------------
- assistant pane has been changed from multitext to rich text.
- new performative (:display has been added to the list) as well as the finction to deal with the various possible actions
- a new file omas-display.lisp has been added to the lot.



OMAS / MOSS 8.0.9 note (February 2011)
======================================

Changes visible at user's level
-------------------------------
- the patch mechanism has been changed to simplify adding new patches. Instead of having a single file omas-patches.lisp, we put patches into a folder named omas-patches-v8.0.9 the last numbers changing with each version. Inside the folder patches are numbered: P8.0.9.0, P8.0.9.1, etc
- the omas-patches.lisp file must be changed to be able to load the content of the patch folder. It appears temporarily as the patch file old style. I.e., the omas-patches.lisp file in the OMAS source folder contains the mechanism that is needed to load the patches new style.
- in the next versions, this omas-patches.lisp file will not be needed.

Internal changes
----------------
IMPORTANT: patches P8.0.9.1 change the encoding used to exchange messages on the local network. Encoding was external ASCII Latin-1 code. It does not work for Japanese. New encoding is UTF-16le (low endian), which happens to be the internal encoding used by ACL. This should be upward compatible with the previous approach.


OMAS / MOSS 8.0.9 note (December 2010)
======================================

Changes visible ar users' level
-------------------------------
- A new macro defretry-subdialog is provided to simplify dialogs
- Persistency has been improved
- MOSS editor has been rewritten with added options
- MCL assistant interface window changed color from pink to blue

Internal Changes
----------------
- Editor code is now shared between ACL and MCL thanks to the windows macros


OMAS / MOSS 8.0.8 note (November 2010)
======================================

Changes visible ar users' level
-------------------------------
- The Contract Net protocol has been modified. Bids can specify start time, delay, cost, quality and rigidity)
- contracts are allocated to all best bidders (multicast) and first answer is considered


OMAS / MOSS 8.0.7 note (October 2010)
=====================================

Changes visible ar users' level
-------------------------------
- In the ontology window, instances are now displayed by alphabetical order, based on the result of applying the =summary method.
- allowing to use strings as method names when defining a method: e.g.
  (defuniversalmethod "test 3" (var) "essai" (list var))
- SKILLS, GOALS, and AGENTS are now displayed by their names in the ontology window

Internal Changes
----------------
- agent-start-changes and agent-commit-changes now contain a test for agent persistency. They can be called even if agent in not persistent.
- the MOSS::db-dump has been added to dump the content of a database


OMAS / MOSS 8 note (May 2010) 
=============================
The new version of OMAS/MOSS is developed with a number of changes. 

Changes visible ar users' level
-------------------------------
- persistency is introduced at the agent level
- the defrelation syntax is modified to use :from and :to options and discard the positional notation; i.e.
  (defrelation position person company ...) is replaced by
  (defrelation position (:from person)(:to company)...)
:from and :to options can contain references, i.e. symbols, strings or mln (multilingual names).
ALL ONTOLOGY and TASK files must be checked for complying with the new syntax.

- Version 8.0 includes the changes that were brought to the 7.16 version concerning the definition of postmen (transfer agents). Most of the code needed for setting up a postman between 2 OMAS local coteries is now included in the OMAS system, which simplifies writing a postman.

- Version 8.0 also includes a new type of "logical agent" that executes rules, meaning that an agent behavior can be defined without writing any Lisp code, but writing rules instead. This is a very preliminary agent using a forward chaining inference engine.

Internal changes
----------------
- MOSS functions are simplified
- booting the MOSS system is treated now independently from building an ontology
- orphans are defined now as the instances of the NULL-CLASS (id = *none*).
Thus the counter for orphans is no longer a special counter attached to the system object *moss-system* but the counter of the *none* class.
- a new function %create-new-package-environment allows creating a new application space related to the specified package. It creates a new package, copies the $SYS model, the *none* and *any* classes as subclasses of the ones in the MOSS package.
- I/O channels are now
   *moss-input*
   *moss-output*
*moss-output* indicates the output view that has a display-text associated for printing the corresponding text. It is used by kernel printing functions with default value *moss-window*
*moss-input* is the view we are reading from
*moss-window* is the current window for interacting with MOSS
*moss-overview-window* is used for browsing the ontology

OMAS / MOSS 7 note (June, 2009)
===============================
The OMAS and MOSS files included in this file run in the Windows XP environment and in 
the Mac OS X environment.
They are provided without any guarantee of any sort.

WINDOWS XP
----------
They were compiled using Allegro Common Lisp v 8.1 and a regular use requires a Lisp 
license from Franz Lisp. They run in the IDE (:cg-user package). They won't run with 
older versions of ACL.

Since an ACL license is quite expensive, it is recommended to do a test run on a trial
copy of the environment before deciding to buy it.

Note that 
   OMAS loads with load-omas-moss.fasl. It contains the MOSS representation language.
   MOSS can be loaded independently from OMAS by loading moss-load.fasl from the MOSS 
folder.
The needed files are in the fasl directory.

Mac OS X
--------
They were compiled using MCL 5.2 from Digitools now an open source version.

Note that
   OMAS loads with load-omas-moss.lisp. It will load MOSS automatically.	
   MOSS can be loaded from the MOSS folder.
The needed files are in the cfsl directory.

MISCELLANEOUS
-------------
Two files for adding patches are provided respectively in the MOSS and OMAS directory.

Other files are provided including examples of applications. 

DOCUMENTATION
-------------
Documentation is available on the site:
   - http://www.utc.fr/~barthes


Release Notes
-------------

OMAS / MOSS 7.10 note (June 15, 2009)
=====================================
The following changes / improvements have been done:

- the second argument of the skill functions, that was environment, is replaced with 
the message that has just been received triggering the skill. Thus, one can exploit the 
content of the message directly if needed. On the other hand the environment area is
no longer useful since it can be replaced by the memory mechanism (remember/recall).
- the second argument of the select-best-answer function has been removed. Thus, the
select-best-answer accepts now 2 arguments: agent and message-list.


OMAS / MOSS 7.4 note (January 13, 2009)
=======================================
The following changes / improvements have been done:

- The ACL and MCL graphics files have been merged for a better maintenance of the 
various windows. A specific macros-W.lisp file defines macros and function to smoothe 
out the differences between the 3 windowing systems. The resulting shapes of the
 different windows may differ slightly from the previous versions.
- MCL 5.2 accepts UTF-8 files, which simplifies the encoding problems between Macs 
and PCs. Storing all files using UTF-8 should make the transfer easier.
- Postmen agents for connections between various international sites have been 
modified to avoid distributing messages that take different paths, several times.

OMAS / MOSS 7.1 note (December 19, 2008)
========================================
The following changes / improvements have been done:

- The ACL (message structure) has been modified. The net result is that OMAS messages 
are now incompatible with previous versions. New parameters include:
   - thru keeping a list of sites that the message has visited
   - the sender IP
- Messages broadcast locally come back to the broadcasting site. A filter eliminating 
echo messages has been set, avoiding to process the same message twice.
- A single structure attached to the *omas* global variable groups all previouly 
scattered global variables.
- Numbering versions now drops the subsub version number.

OMAS / MOSS 7.0.5 note (November 16, 2008)
==========================================
The following changes / improvements have been done:

- Graphics window is now controled by a specific agent called SPY. SPY is an instance
of postman. It receives all messages and draws them into a graphics window. Whenever 
a message comes in from outside containing an unknown sender, SPY reorganizes the 
displaying window to account for the new agent. SPY is a hidden agent and does not
appear in the graphics window. The display window is created by the SCAN process of 
the SPY agent as everything to be drawn.
- As a consequence, the special display process has been removed, as well as the 
graphics window file.
- Agent windows now display the message log in their left pane.
- A special postman has been added to each site for sending messages to remote sites 
like TECPAR or CIT. Messages are sent through a TCP/IP connection using port 52008.
The postman file is part of the application and must be declared in the application 
folder.
- A special mechanism has been set up to avoid sending messages in infinite loops when 
several sites are connected making loops.
- The timeout for call-for-bids has been increased to 1 second to allow remote site 
to bid (average message travel time between France and Brasil or Japan is 350ms).

OMAS / MOSS 7.0.2 note (August 1, 2008)
=======================================
version for Mac and Windows. Corrects some minor bugs.






