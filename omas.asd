;;;=================================================================================
;;;24/10/15
;;;	              O M A S - A S D F - (File omas.asdf)
;;;	
;;;=================================================================================
;;;
;;; File defining the OMAS system to be loaded in the ACL environment

;;; (c) Jean-Paul Barthès@HEUDIASYC, CNRS, UTC. See LICENSE file for permissions.

#|
2024
 1016 addapting to Windows 10
2020
 0201 creation from OMAS-version-info 
|# 

(defsystem "omas"
    :version "14.0"
  :author "Barthès"
  :license "CeCILL-B"
  :depends-on (:quicklisp :moss)
  :components ((:module "omas-src"
                        :serial t
                        :components
                        ((:file "packages")
                         (:file "parameters")
                         (:file "macros")
                         (:file "utils")
                         (:file "globals")
                         (:file "texts-utf8")
                         (:file "netcomm")
                         (:file "agents")
                         (:file "assistant")
                         (:file "postman")
                         (:file "inferer")
                         (:file "messages")
                         (:file "comm")
                         (:file "netUDP")
                         (:file "self")
                         (:file "world")
                         (:file "API")
                         (:file "processes")
                         (:file "control")
                         (:file "task")
                         (:file "goals")
                         (:file "persistency")
                         (:file "json")
                         (:file "web-services")
                         ;;=== windows
                         (:file "W-macros") ; compatibility macros
                         (:file "W-control-panel")
                         (:file "W-text")
                         (:file "W-agent")
                         (:file "W-dialog-trace") ; window associated to a PA to trace dialogs
                         (:file "W-message")
                         (:file "W-assistant") ; assistant interface window
                         (:file "W-postman")
                         (:file "W-small-window") ; assistant reduced window
                         (:file "W-init") ; initial window for loading an application
                         (:file "spy") ; agent in charge of the graphics trace window
                         (:file "W-spy")
                         (:file "W-warn")
                         (:file "display")
                         (:file "load-app")
                         (:file "main")
                         )))
  :description "OMAS is a multi-agent platform"
  :in-order-to ((test-op (test-op "moss/tests"))))

;;; :EOF