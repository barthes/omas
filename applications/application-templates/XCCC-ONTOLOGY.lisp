﻿;;;-*- Mode: Lisp; Package: "XCCC" -*-
;;;===============================================================================
;;;10/11/10
;;;             AGENT XCCC ONTOLOGY (file XCCC-ontology.lisp)
;;;             Copyright Barthès@UTC, 2010
;;;
;;;=============================================================================== 

#|
XCCC is a transfert agent or postman.
Its ontology is used to process data contained in messages and to send 
 messages to other agents.
The current ontology contains nothing.

The ontology is defined using the PDM/MOSS formalism. However it is compat-
ible with SOL and this can produce OWL, HTML, JENA or SPARQL outputs.
Remember that if the agent is persistent, macros are not saved, global 
variables should be defined by using defvariable and functions by using
deffunction.
Once the persistent database is created, this file becomes useless.

2010
 0720 Creation (copy from a previous file)
|#

(in-package :XCCC)


(defontology 
  (:title "XCCC ontology")  ; currently ignored
  (:version "1.0")  ; currently ignored
  (:package "XCCC") ; currently ignored
  (:language :all)  ; required for a PA could be single language, e.g. :FR
  )

;;;---------------------------------------------------------------------- CONCEPTS

#| Example:
Monolingual concept (defontology language option :FR)  
(defconcept "Adresse Postale"
  (:is-a "adresse")
  (:doc "L'ADRESSE POSTALE est l'adresse où le courrier est distribué.")
  (:att "rue")
  (:att "ville" (:unique))
  (:att "pays" (:unique))
  (:att "code postal" (:unique)))
  
Multilingual concept (defontology language option :ALL)
(defconcept (:en "Postal Address" :fr "Adresse Postale")
  (:is-a "address")
  (:doc :en "A POSTAL ADDRESS is the information needed to locate ~
             the place where a person is living or working."
        :fr "L'ADRESSE POSTALE est l'adresse ou le courrier est distribué.")
  (:att (:en "street and number; street" :fr "rue"))
  (:att (:en "town; city" :fr "ville") (:unique))
  (:att (:en "country" :fr "pays") (:unique))
  (:att (:en "zip; postal code" :fr "code postal") (:unique)))  
|#


;;;=============================================================================== 
;;;
;;;                                INDIVIDUALS
;;;
;;;=============================================================================== 

#| Example:
(defindividual "documentation"  ; class is defined in MOSS
  ("title" "global help")
  ("documentation" 
   (:en 
    "This is the global help... As your assistant I can do a certain number of ~
     things for you. They correspond to tasks I know of. The easiest way to ~
     use me is to ask me using simple sentences what you want to do.
     But remember, my vocabulary and understanding is quite limited."
    :fr 
    "Aide globale... En tant qu'assistant, je peux faire un certain nombre ~
     de choses pour vous. La meilleure façon de m'utiliser est de faire des ~
     phrases courtes. Toutefois, je vous rappelle que mon vocabulaire est  ~
     limité."))
  )
Monolingual
(defindividual "adresse postale"
	("ville" "Compiègne")
	("rue" "25, rue de Paris")
	("code postal" "60200")
	)
|#

;;;=============================================================================== 

:EOF