﻿;;;-*- Mode: Lisp; Package: "PBBB" -*-
;;;===============================================================================
;;;<date>
;;;             AGENT PBBB: DIALOG (file PBBB-dialog.lisp)
;;;             Copyright Barthès@UTC, 2006-2012
;;;
;;;=============================================================================== 

;;; Note that this file must be in UTF-8 format (like the other agent files)
#|
PBBB is an anonymous French assistant agent. Its dialog file is minimal
and contains the following conversations
              _MAIN-CONVERSATION (top level)
              _PRINT-HELP-CONVERSATION
              _PROCESS-CONVERSATION (top level)
              _SET-FONT-SIZE-CONVERSATION
              _TRACE-CONVERSATION

The current file includes its library of dialogs.

Questions asked to PBBB:
   - lettres plus gros.
   - help.
   - SOS.
   - Bonjour.
   - ...
2012
 0104 Creation of the PBBB dialog 
|#

(in-package :PBBB)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (setq *language* :fr)
  )

(format t "~&loading PBBB dialogs, package is: ~S" *package*)

;;; The folowing comments intend to help you in building dialogs by giving a list
;;; of possible options to the defstate macro

;;;=================================== macros ==================================

;;;-------------------------------------------------------------------- DEFSTATE
;;; The defstate macro is fairly complex and defined the dialog language.
;;; The use of the various options is non trivial.
;;; When a state is entered, the =execute method is applied to the new state.
;;; Then the =resume method is called on the state.
;;; If a question to the user was asked (:question, :ask, options),
;;; then processing the answer waits until something appears in the
;;; to-do slot of the conversation object.
;;;
;;; the =execute method
;;; ===================
;;; The method normally processes the content of the :INPUT area of the FACTS slot
;;; of the conversation. It may have a single option :question. Without options 
;;; the method does nothing and the =resume method is called immediately.
;;; The FACTS/INPUT area should contain a list of words or the symbol :FAILURE.
;;; The return value is :WAIT or :RESUME.
;;;
;;;   :answer-type  
;;;   ------------
;;; Can be used to specify the type of the expected answer message. If the answer
;;; is not of that type, it shows a rupture in the dialog.
;;;
;;;   :clear-all-facts
;;;   ----------------
;;; Clears the content of the FACT base (removes everything)
;;;
;;;   :clear-fact (item)
;;;   ------------------
;;; Sets the value associated with item to NIL. Usually item is a keyword.
;;;
;;;   :clear-facts (item-list)
;;;   ------------------------
;;; Same as clear-fact but takes a list of items
;;;
;;;   :eliza  
;;;   ------
;;; Forced call to ELIZA. We expect an answer from the master. 
;;;
;;;   :execute-preconditions  
;;;   ----------------------
;;; Escape mechanism to execute Lisp code directly in the =execute method, e.g. 
;;; to set flags. 
;;;
;;;   :question | :question-no-erase
;;;   ------------------------------
;;; The associated value is either a string or a list of strings. When 
;;; we have a list of strings, one is chosen randomly for printing.
;;; Once the question is asked, the user is supposed to answer. The answer is
;;; inserted into the INPUT area of the FACTS slot in the conversation object 
;;; that is the link between the external system and MOSS.
;;; The question is processed by the =resume method.
;;;
;;;   :reset-conversation
;;;   -------------------
;;; Restarts the main conversation, cleaning all values of the conversaiton oject.
;;;
;;;   :send-message args
;;;   ------------------
;;; Send a message to other agents. Args are those for an OMAS message. An answer
;;; is expected. A timeout may be specified.
;;;
;;;   :send-message-no-wait args
;;;   --------------------------
;;; Send a message to other PAs. Since the message is similar to an e-mail, we do
;;; not wait for the answer in the dialog (not in the process).
;;;
;;;   :text {:no-erase}
;;;   -----------------
;;; Print text e.g. prior to asking a question.
;;;
;;;   :text-exec expr
;;;   ---------------
;;; If expr is a list starting with format, execute it before printing it.
;;;
;;;   :wait
;;;   -----
;;; Force the dialog crawler to wait for an input even if no question was put to
;;; the master by letting the =execute method return a :wait tag
;;;
;;; The =resume method
;;; ==================
;;; Called after the =resume method. The user data are in the :INPUT area of the 
;;; current conversation, or in different field of the FACTS slot of the 
;;; conversation object.
;;; Options are the following.
;;;
;;; Whenever the input contains ":quit :abort :exit :reset :cancel" a transition
;;; to the global _Q-abort state occurs.
;;;
;;;   :answer-analysis
;;;   ----------------
;;; Indicates that a special method named =answer-analysis will be applied to
;;; the data (INPUT). The method must return a transition state or
;;; the :abort keyword, in which case the conversation is locally restarted.
;;; All other options are ignored.
;;;
;;;   :execute function args
;;;   --------
;;; Executes the corresponding FUNCTION and args
;;;
;;;   :transitions
;;;   ------------
;;; transitions are clauses (transition rules) with an IF part, an INTERMEDIATE part
;;; and a TRANSITION part, each with options.
;;; IF part options of a transition clause:
;;;
;;; :always
;;;    always execute this clause, any clause following is ignored
;;; :contains <list of words>
;;;    test if the input contains one of the words
;;; :empty 
;;;    test if the input is nil (not very useful?)
;;; :no 
;;;    test if the input is a negation (does not work for Asian languages)
;;; :on-failure
;;;    tests if the returned value from the master or from the agents was a failure
;;;    by checking if FACTS/INPUT is (:failure) 
;;; :otherwise
;;;    always fire (should be the last option)
;;; :patterns ({<pattern> {<action>}+}+)
;;;    test each pattern against the content of INPUT, if it matches, executes 
;;;    actions
;;; :rules ({{<pattern>}{<answer>}*}* {<action>}+}+)
;;;    tries to apply a set of rules. If one applies, then computes an answer ELIZA
;;;    style, should use :display-result to display it
;;; :starts-with 
;;;    test if the answer starts with a word (use rather patterns)
;;; :test expr
;;;    fires if the test applies (expr evaluates to non nil)
;;; :yes 
;;;    test if the answer is yes (does not work with Asian languages)
;;;
;;; THEN options for intermediate actions (before a transition):
;;; :display-answer
;;;    send the content of the FACTS/ANSWER area to the output channel for
;;;    printing 
;;; :exec {sexpr}
;;;    executes the sexpr in the context of =resume
;;; :display-result
;;;    displays the sentence resulting from applying the set of rules to the input
;;;    used only for AI agents
;;; :keep
;;;    used with patterns, saves the value of the pattern variable into FACTS/INPUT 
;;; :print-answer print-function
;;;    prints the content of the FACTS/ANSWER area applying the print funtion
;;; :replace {value}
;;;    replaces the content of INPUT with the specified value (word list)
;;; :set-performative {:request|:command|:assert}
;;;    set the FACTS/PERFORMATIVE area (canbe used to check if we are waiting for an
;;;    answer rather than a request
;;;
;;; THEN options for transitions:
;;; :failure
;;;    exit from state with :failure marker, anything following that is ignored 
;;; :reset
;;;    restarts the local (sub-)conversation, incompatible with anything else
;;; :sub-dialog dialog-header
;;;    specifies a sub-dialog
;;;    :success state : transfer state in case of success
;;;    :failure state : transfer state in case of failure
;;; :success
;;;    exit from state with success marker, anything following that is ignored
;;; :target state
;;;    specifies the transition state
;;; Default is to restart the conversation at the beginning.

;;;--------------------------------------------------------- DEFSIMPLE-SUBDIALOG

;;; This macro is provided for simplifying the writing of very simple dialogs 
;;; when the conversation graph contains a single node and we want to send a 
;;; message to another agent to get some data or information tthat we are going
;;; to print. 
;;; The macro produces the declarations of the global variables, the subdialog 
;;; headers, the state, and the =execute and =resume methods.
;;; E.g.
;;;  (defsimple-subdialog "get-financing" "_gfi"
;;;     :explanation "Master is trying to obtain info about a financing program."
;;;     :from PA_HDSRI :to :FINANCING :action :get-financing
;;;     :language :fr
;;;     :pattern ("financement" ("pays")("titre")("date limite") ("URL"))		 
;;;     :sorry "- Désolé, je ne trouve pas le programme de financement demandé."
;;;     :print-fcn #'print-financing
;;;     )
;;; This macro call builds a graph for sending a message to the FINANCING agent
;;; with action :get-financing, pattern for the answer, a message to print in
;;; case of failure and a function to print the results in case of success. The
;;; data arguments are read from the conversation :input slot.
;;; Default printing function is display-answer

;;;================================================================================

;;; here the strategy is to develop a specific dialog for each task. However, we 
;;; could also use a generic dialog consisiting in:
;;;    - getting the list of required parameters
;;;    - for each parameter, trying to extract info from data using predefined
;;;      rules or extraction patterns (Note that this is not much different than
;;;      having a predefined dialog, albeit focused on a single parameter)
;;;    - if some required parameters are missing, ask master about them using 
;;;      predefined questions
;;;    - shipping result to ad hoc staff agent

;;; The generic dialog could be an option offered to the programmer, which requires
;;; a suitable modeling of the tasks.


;;;================================================================================
;;;
;;;                              Sub-Conversations
;;;
;;;================================================================================

;;; Sub-conversations are defined by a conversation header, that points to the
;;; entry state.
;;; There are two output states:
;;;   SUCCESS: the result is stored into the RESULTS slot of the state-context
;;;   FAILURE: no result could be achieved
;;; In case of bad error there is a throw to an :error label
;;; data are obtained from the HAS-DATA slot of the context object

;;;================================================================================
;;;
;;;                      GENERIC TASK CONVERSATION
;;;
;;;================================================================================

;;; this subconversation takes a task model and fills in the required parameters
;;; before shipping the data (for possible optional args) and filled pattern to the 
;;; specialist

;;; probably not very useful

;;;================================================================================
;;;
;;;                      PRINT DATE CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to help the master by giving general information:
;;; what tasks are available.

(defsubdialog 
  _print-date-conversation 
  (:label "Print date conversation")
  (:explanation "Date was asked.")
  (:states _pd-entry-state  ; required by defstate
           )
  )

(defstate
  _pd-entry-state
  (:label "Print date")
  (:explanation "return date and time.")
  (:entry-state _print-date-conversation )
  (:execute
     (print-time moss::conversation :fr))
  (:transitions
   (:always :success)))

;;;================================================================================
;;;
;;;                       PRINT HELP CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to help the master by giving general information:
;;; what tasks are available.

(defsubdialog 
  _print-help-conversation 
  (:label "Help conversation")
  (:explanation "Help was asked.")
  (:states _ph-entry-state  ; required by defstate
           _ph-print-global-help)
  )

;;;-------------------------------------------------------------- (PH) ENTRY-STATE

(defstate 
  _ph-entry-state
  (:label "Explain dialog entry")
  (:entry-state _print-help-conversation )
  (:explanation "Master is asking for help.")
  (:transitions
   ;; make sure we have a question
   (:patterns (((?* ?x) "au" "secours" (?* ?y))
               ("sos")
               ("help")
               ("?")
               ((?* ?x) "Je" "suis" (?* ?y) "perdu" (?* ?z))
               ((?* ?x) "Je" "suis" (?* ?y) "perdue" (?* ?z))
               ("quels" (?* ?x) "vous" "proposez" (?* ?y))
               ((?* ?x) "services" (?* ?y) "vous" "offrez" (?* ?z))
               ("que" "pouvez-vous" "faire" (?* ?y))
               ("quels" "services" (?* ?y))
               )
              :target _ph-print-global-help)
   (:otherwise :failure)
   ))

;;;-------------------------------------------------------- (PH) PRINT-GLOBAL-HELP

(defstate
  _ph-print-global-help
  (:label "Print general help")
  (:explanation "No specific subject was included.")
  (:execute
     (let
         ((obj-id (car (send '>-global-help '=get 'is-title-of)))
          (*language* :fr)
          (task-list (access '(task)))
          (conversation (omas::conversation PBBB::PA_PBBB))
          )
         (when obj-id (send obj-id '=get-documentation)
               (send conversation '=display-text *answer*))
         (send conversation '=display-text
               "~2%Je peux faire les choses suivantes :")
         (dolist (task task-list)
           (send task '=get-documentation :lead "   - ")   
           (send conversation '=display-text *answer*)
           )
         (send conversation '=display-text "~2%")
         ))
  (:transitions
   (:always :success)))


  
;;;================================================================================
;;;
;;;                       SET FONT SIZE CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to modify the size of the dialog font, usually to
;;; increase it or to return to the default value.

(defsubdialog 
  _set-font-size-conversation
  (:short-name sfs)
  (:label "Font size conversation")
  (:explanation 
   "The data is searched for the presence of words like increase, ~
    bigger, smaller, defaults, reset,...")
  (:states _sfs-entry-state  ; required by defstate
           _sfs-dont-understand
           _sfs-try-again
           _sfs-sorry)
  ;(:action _sfs-action)
  )

;;;--------------------------------------------------- (SFS) FONT-SIZE-ENTRY-STATE

(defstate 
  _sfs-entry-state
  ;(:conversation _set-font-size-conversation)
  (:label "Font size dialog entry")
  (:entry-state _set-font-size-conversation)
  (:explanation "The data is searched for the presence of words like ~
                increase, bigger, defaults, reset,...")
  (:transitions
   (:patterns ((* "augmenter" *)
               (* "plus" "grand" *)
               (* "trop" "petit" *)
               (* "plus" "gros" *))
              :exec (omas::assistant-use-larger-font PA_PBBB) :success)
   (:patterns ((* "diminuer" *)
               (* "plus" "petit" *)
               (* "moins" "grand" *)
               (* "moins" "gros" *))
              :exec (omas::assistant-reset-font PA_PBBB) :success)
   (:patterns ((* "utiliser" * "défaut" *)
               (* "reset" *)
               (* "défaut" *))
              :exec (omas::assistant-reset-font PA_PBBB) :success)
   (:otherwise :target _sfs-dont-understand)
   )
  )

;;;--------------------------------------------------------- (SFS) DONT-UNDERSTAND

(defstate 
  _sfs-dont-understand
  (:label "Did not understand. Ask master.")
  (:explanation "Assistant is asking master for font-size clarification.")
  (:question "~%- Vous voulez que j'écrive plus gros, plus petit, ou que ~
              j'utilise la taille par défaut ?")
  (:transitions
   (:always :target _sfs-try-again)))

;;;------------------------------------------------------------------- (SFS) SORRY

(defstate 
  _sfs-sorry
  (:label "Font dialog failure.")
  (:explanation "We cannot make things out from the data.")
  (:reset)
  (:text "~%- Désolé, je ne vois pas ce que vous voulez faire avec les lettres.~%")
  (:transitions
   (:always :failure))
  )

;;;--------------------------------------------------------------- (SFS) TRY-AGAIN

(defstate 
  _sfs-try-again
  (:label "Font size try again")
  (:explanation "we asked master and try to pick up the answer.")
  (:transitions
   (:patterns ((* "augmenter" *)
               (* "plus" "grand" *)
               (* "plus" "gros" *))
              :exec (omas::assistant-use-larger-font PA_PBBB) :success)
   (:patterns ((* "diminuer" *)
               (* "plus" "petit" *)
               (* "moins" "grand" *)
               (* "moins" "gros" *))
              :exec (omas::assistant-reset-font PA_PBBB) :success)
   (:patterns ((* "utiliser" * "défaut" *)
               (* "reset" *)
               (* "défaut" *)
               (* "defaut" *))
              :exec (omas::assistant-reset-font PA_PBBB) :success)
   (:otherwise :target _sfs-sorry)
   )
  )

;;;================================================================================
;;;
;;;                              TRACE CONVERSATION
;;;   
;;;================================================================================

;;; this conversation is intended to print a trace of the traversed states

(defsubdialog 
  _trace-conversation
  (:label "Trace conversation")
  (:explanation 
   "Command to toggle the dialog trace.")
  (:states _tr-entry-state)
  )
  
;;;-------------------------------------------------------- (TR) TRACE-ENTRY-STATE

(defstate 
  _tr-entry-state
  (:label "Trace dialog entry")
  (:entry-state _trace-conversation)
  (:explanation "We search the input for the trace command.")
  
  (:transitions
   ;; system commands
   (:patterns ((* "afficher" * "transitions" *)
               (* "tracer" * "transitions" *)
               (* "imprimer" * "transitions" *))
              :exec (setq moss::*verbose* t) :success)
   (:patterns ((* "pas" "de" "trace" *)
               (* "supprimer" "la" "trace" *)
               (* "arrêter" "la" "trace" *)
               (* "stop" "trace" *))
              :exec (setq moss::*verbose* nil) :success)
   (:otherwise :failure)
   )
  )

;;;================================================================================
;;;
;;;                      WHAT-IS CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to obtain and print information about something
;;; corresponding to unformatted data

(defParameter *empty-words* 
  '("the" "a" "an" "of" "from" "to" "those" "these" "that" "s" "?"
    "le" "la" "les" "de" "du" "des" "à" "aux" 
    "ce" "ces")
  "used by =answer-analysis of state get-info")

;;;================================================================================

(defsubdialog 
  _what-is-conversation
  (:label "Get info dialog")
  (:explanation "Master is trying to get info about some data.")
  (:states _wai-entry-state
           _wai-ask-staff
           _wai-get-info
           _wai-process-staff-answer
           _wai-sorry)
  )

;;;------------------------------------------------------------- (WAI) ENTRY-STATE

(defstate 
  _wai-entry-state
  (:label "What is conversation")
  (:entry-state _what-is-conversation)
  (:explanation "Assistant is trying to get object info from its knowledge base.")
  (:transitions
   ;;keep only relevant data
   (:patterns ((* "qu" "est" "ce" "que" ?y "?")
               (?y "c" "est" "quoi" "?" *)
               (* "c" "est" "quoi" ?y "?")
               (?y)  ; when entering from print-concept-documentation
               )
              :keep ?y
              :target _wai-get-info)
   (:otherwise :target _wai-sorry)
   )
  )

;;;--------------------------------------------------------------- (WAI) ASK-STAFF
;;; changing strategy to :collect-answers
;;; rather than sending a broadcast right away, maybe we should
;;; 1. look into local KB
;;; 2. look into our agenda (sa-address)
;;; 3. ask BIBLIO
;;; This would avoid strange answers resulting from misinterpretation of the
;;; query.

(defstate 
  _wai-ask-staff
  (:label "Ask staff.")
  (:explanation "Ask the staff for help.")
  (:send-message :to :all :self PA_PBBB :action :what-is :timeout 2
                 :args `(((:data . ,(read-fact moss::conversation :input))
                          (:language . :fr)
                          ))
                 :strategy :collect-answers
                 )
  (:transitions 
   (:always :target _wai-process-staff-answer))
  )

;;;---------------------------------------------------------------- (WAI) GET-INFO

(defstate 
  _wai-get-info
  (:label "Looking into local KB info.")
  (:explanation "Look into own KB to get info.")
  (:answer-analysis)
  )

(defownmethod =answer-analysis _wai-get-info (conversation input)
  "we try first to get info locally and if it fails ask all agents.
Arguments:
   agent: assistant."
  (declare (special *empty-words*))
  (let* (entry-list)
    (catch :return
      ;; clean input of parasitic words
      (setq input (mapcan #'(lambda (xx) 
                              (if (member xx *empty-words* :test #'string-equal)
                                nil
                                (list xx)))
                          input))
      (moss::vformat "JEAN-PAUL: _wai-get-info /+++ cleaned input:~& ~S" input)
      ;; if nothing left, then go ask staff
      (unless input
        (throw :return `(:transition ,_wai-ask-staff)))
      ;; check whether the resulting words correspond to entry points
      (setq entry-list (moss::find-best-entries input))
      (moss::vformat "JEAN-PAUL: _wai-get-info /entry list:~&    ~S" entry-list)
      ;; if nothing left go ask staff
      (unless entry-list 
        (throw :return `(:transition ,_wai-ask-staff)))
      ;; if more than one result, ask for help (not yet)
      ;; get documentation or CV
      ;; print it
      (dolist (item entry-list)
        (send item '=get-documentation :lead "   - ")   
        (send conversation '=display-text *answer*)
        )
      ;; transfer to :success    
      (list :success) 
      )
    ))

;;;------------------------------------------------------ (WAI) PROCESS-STAFF-ANSWER

(defstate 
  _wai-process-staff-answer
  (:label "Process staff answer.")
  (:explanation "Process anwers from the staff agents.")
  (:answer-analysis)
  )

(defownmethod =answer-analysis _wai-process-staff-answer (conversation input)
  "we got an answer from some staff or else the master got impatient.
Arguments:
   agent: assistant."
  (declare (ignore input))
  (let* ((agent (car (HAS-MOSS-AGENT conversation)))
         (answer-list (omas::answer agent))
         (*language* :fr)
         )
    ;(format *debug-io* "~&+++ =resume; HAS-TO-DO: ~S" (has-to-do conversation))
    ;; clean up HAS-TO-DO (contains "answer-there")
    ;(setf (has-to-do conversation) nil)
    ;; answer contains either the word "failure" or a list of lists of strings
    ;; (("Jean-Paul Barthes is a professor at UTC ..."
    ;;  "Dominique Barthes-Biesel is a professor at UTC ?..")
    ;;  ("Jean-Paul Barthes has NN publications on:  ...")
    ;;  ...)
    ;; 
    (moss::vformat "~&>>> WAIdialog /Answer to the send-request:~% ~S" answer-list)
    ;; failure if :failure is returned or :error on timeout
    (cond
     ((or (equal "failure" answer-list) (eql :error answer-list))
      `(:transition ,_wai-sorry))
     ((null answer-list)
      (send conversation '=display-text "- Je ne sais pas...")
      (list :success))
     (t 
      (moss::vformat " _wai-ask-staff /answer-list: ~& ~S" answer-list)
      (send conversation '=display-text "~%C'est:")
      ;; each item of the answer list is like 
      ;;    ("personne" ("nom" "...")("prénom" "..."))
      (dolist (answer answer-list)
        ;; answer could be "failure" rather than a list of strings
        (unless (and (stringp answer) (string-equal answer "failure"))
          (dolist (item answer)
;            (send conversation '=display-text 
;                  (concatenate 'string "~%" (car item)))
;            (send conversation '=display-text
;                  (format nil ": ~{~%     ~{~A~^, ~}~}" (cdr item))))))
           (send conversation '=display-text item))))
      (list :success)))
    ))

;;;------------------------------------------------------------------- (WAI) SORRY

(defstate 
  _wai-sorry
  (:label "Failure to get info.")
  (:explanation "We could not find anything.")
  (:reset)
  (:text "~%- Désolé, je n'ai aucune information correspondant à ces données.~%")
  (:transitions 
   (:failure))
  )

;;;================================================================================
;;;
;;;                    WHO IS (GET PERSON INFO) CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to print information about a person.

(defsubdialog 
  _get-person-info-conversation
  (:label "Get person info dialog")
  (:explanation "Master is trying to obtain a telephone number.")
  (:states _gpi-entry-state
           _gpi-ask-staff
           _gpi-get-info
           _gpi-sorry)
  )

  
;;;================================================================================
;;;                  WHO IS CONVERSATION SERVICE FUNCTIONS
;;;================================================================================

;;;------------------------------------------------------------ CHECK-ENTRY-POINTS

(defun check-entry-points (input)
  "tries to get entry points by combining words.
Arguments:
   input: list of one or more words
Return:
   nil or a list like 
     (\"barthes\" ($T-PERSON-NAME.OF . $E-PERSON.1) ($T-PERSON-NAME.OF . $E-PERSON.2))"
  (let (result entry entry-list expr-list)
    ;; combine all possible words
    (setq expr-list (make-word-combinations input))
    ;; try all possibilities
    (dolist (expr expr-list)
      ;; entry point ?
      (when (setq entry (moss::%is-entry? expr))
        ;; extract corresponding objects
        (setq result (moss::%%get-objects-from-entry-point entry))
        ;; return to the executing environment
        (moss::vformat "...trying ~S in package ~S as entry...~&   => ~S" 
                       expr *package* result)
        (if result (setq entry-list (append entry-list result)))
        ))
    ;; clean list, eliminating tasks whose performative is different of the required
    ;; one
    (setq entry-list (delete-duplicates entry-list :test #'equal))
    (moss::vformat "check-entry-points /Resulting entry list ~&  => ~S" entry-list)
    (mapcar #'cdr entry-list)))

#|
? (JEAN-PAUL::check-entry-points '("telephone" "barthes" ))
;***** ...trying "telephone" in package #<Package "SA-ADDRESS"> as entry...
   => ((MOSS::$ENAM.OF . $E-PHONE))
;***** ...trying "barthes" in package #<Package "SA-ADDRESS"> as entry...
   => (($T-PERSON-NAME.OF . $E-PERSON.2) ($T-PERSON-NAME.OF . $E-PERSON.3)
       ($T-PERSON-NAME.OF . $E-PERSON.4))
;***** check-entry-points /Resulting entry list 
  => ((MOSS::$ENAM.OF . $E-PHONE) ($T-PERSON-NAME.OF . $E-PERSON.2)
      ($T-PERSON-NAME.OF . $E-PERSON.3) ($T-PERSON-NAME.OF . $E-PERSON.4))
($E-PHONE $E-PERSON.2 $E-PERSON.3 $E-PERSON.4)
|#
;;;-------------------------------------------------------- MAKE-WORD-COMBINATIONS

(defun make-word-combinations (word-list)
  ;; when empty list or only one word return
  (unless (cdr word-list) (return-from make-word-combinations word-list))
  ;; otherwise build word combinations
  (let ((input-length (length word-list)) ; e.g. 5
        result)
    (dotimes (nn (1+ input-length)) ; e.g. nn: 0 -> 4
      ;; build cursor to move through the list
      (dotimes (jj nn) ; e.g. jj: 0, then jj: 0->1
        (push
         (apply #'moss::%make-phrase 
                (subseq word-list jj (+ (- input-length nn) jj 1)))
         result)))
    (reverse result)))

#|
? (make-word-combinations '("a" "b" "c" "d"))
("a b c d" "a b c" "b c d" "a b" "b c" "c d" "a" "b" "c" "d")
? (moss::%%make-name "a b c" :plain)
A-B-C
|#
;;;================================================================================
;;;                      WHO IS CONVERSATION STATES
;;;================================================================================

(defstate 
  _gpi-entry-state
  (:label "Get person info conversation")
  (:entry-state _get-person-info-conversation)
  (:explanation 
    "Assistant is trying to get the info from its knowledge base.")
  (:transitions
   ;;keep only relevant data
   (:patterns ((* "qui" "est" ?y "?")
               (* "qui" "est" ?y)
               (?y "c" "est" "qui" "?" *)
               (* "c" "est" "qui" ?y "?")
               )
              :keep ?y
              :target _gpi-get-info)
   (:otherwise :target _gpi-sorry)
   )
  )

;;;--------------------------------------------------------------- (WOI) ASK-STAFF
;;; changing strategy to :collect-answers
;;; rather than sending a broadcast right away, maybe we should
;;; 1. look into local KB
;;; 2. look into our agenda (ADDRESS)
;;; 3. ask BIBLIO
;;; This would avoid strange answers resulting from misinterpretation of the
;;; query.

(defstate 
  _gpi-ask-staff
  (:label "Ask other agents for help.")
  (:explanation "Ask the staff for help.")
  (:send-message :to :all :self PA_PBBB :action :who-is :timeout 4
                 :args `(((:data . ,(read-fact moss::conversation :input))
                          (:language . :fr)
                          ))
                 :strategy :collect-answers
                 )
  (:transitions
   (:on-failure :target _gpi-sorry)
   (:otherwise
    :exec (dolist (answer (read-fact moss::conversation :answer))
            ;; answer could be a failure rather than a list of strings
            (unless (moss::is-answer-failure? answer)
              (dolist (item answer)
                (send moss::conversation '=display-text item))))
    :success))
  )

;;;---------------------------------------------------------------- (WOI) GET-INFO

(defstate 
  _gpi-get-info
  (:label "Looking into local KB info.")
  (:explanation "Look into own KB to get answer to who-is?.")
  (:answer-analysis)
  )

(defownmethod =answer-analysis _gpi-get-info (conversation input)
  "we got an answer from some staff or else the master got impatient.
Arguments:
   conversation: the conversation object
   input: a copy of the FACTS/input area"
  (let* (entry-list)
    (catch :return
      ;; clean input of parasitic words
      (setq input (mapcan #'(lambda (xx) 
                              (if (member xx *empty-words* :test #'string-equal)
                                nil
                                (list xx)))
                          input))
      (moss::vformat "JEAN-PAUL: _gpi-get-info /+++ cleaned input:~& ~S" input)
      ;; if nothing left, then ask staff
      (unless input
        (throw :return `(:transition ,_gpi-ask-staff)))
      ; check whether the resulting words correspond to entry points
      (setq entry-list (check-entry-points input))
      (moss::vformat "JEAN-PAUL: _gpi-get-info /entry list:~& ~S" entry-list)
      ;; if nothing left go ask staff
      (unless entry-list 
        (throw :return `(:transition ,_gpi-ask-staff)))
      ;; if more than one person ask for help (not yet)
      ;; get documentation or CV
      ;; print it
      (dolist (item entry-list)
        (send item '=get-documentation)
        (send conversation '=display-text *answer*))
      ;; transfer to :success    
      (list :success) 
      )
    ))

;;;------------------------------------------------------------------- (WOI) SORRY

(defstate 
  _gpi-sorry
  (:label "Sorry: no info.")
  (:explanation "We could not find anything.")
  (:reset)
  (:text ("- Désolé, je n'en ai pas la moindre idée.~%"
          "- Je ne sais pas.~%"))
  (:transitions 
   (:failure))
  )


;;;================================================================================
;;;================================================================================
;;;
;;;                      TOP-LEVEL CONVERSATION
;;;
;;;================================================================================
;;;================================================================================

;;; rewritten Jan 2012 to avoid calling process subdialog, which inserts a spurious
;;; entry on the salient-features queue. We this reorganize the state to be at the
;;; same level.

;;; we declare the top-level conversations
;;; the entry point of the dialog is _MAIN-CONVERSATION

(eval-when (:load-toplevel :compile-toplevel :execute)
  (proclaim '(special 
              _MAIN-CONVERSATION
              )))

;;; create a dialog header to be used for the main conversation

(defindividual 
  MOSS-DIALOG-HEADER
  (HAS-MOSS-LABEL "Main conversation loop")
  (HAS-MOSS-EXPLANATION "This is the main conversation loop.")
  (:var _main-conversation))

;; declare that conversation, replacing default conversation in the agent structure
(setf (omas::dialog-header PA_PBBB) _main-conversation)

;;;===============================================================================
;;;                            Service functions
;;;===============================================================================

;;; debugging function
(defun cc () (moss::start-conversation _main-conversation))

;;;-------------------------------------------------------------------- PRINT-TIME

(defun print-time (conversation &optional (language *language*))
  (multiple-value-bind (second minute hour date month year)
                       (decode-universal-time (get-universal-time))
    (send conversation '=display-text
          (case language
            (:en
             (format nil "Current time is ~S:~S:~S and today is the ~S/~S/~S~%"
                     hour minute second date month year))
            (:fr
             (format nil 
                     "Il est actuellement ~S:~S:~S et nous sommes aujourd'hui le ~
                      ~S/~S/~S~%"
                     hour minute second date month year)))
          )))

;;;===============================================================================
;;;                            Conversation states
;;;===============================================================================

;;;===== states are declared as global variables
;;; the set of states can be considered as a plan to be executed for conducting the
;;; conversation with the master

;;;--------------------------------------------------------------------------------
(eval-when (:compile-toplevel :load-toplevel :execute)
  (proclaim '(special
              _mc-eliza
              _mc-entry-state
              _mc-failure
              _mc-find-performative
              _mc-find-task
              _mc-get-input
              _mc-more?
              _mc-process
              _mc-select-task
              _mc-sleep
              _mc-task-dialog)))
           
;;;--------------------------------------------------------------------------------

;;;--------------------------------------------------------------- (MC) ENTRY-STATE
;;; ENTRY STATE
;(defun ff () (start-conversation _main-conversation :first-time t))
;(defun gg () (start-conversation _main-conversation))

;;; define an entry instance and record it in a global variable.
;;; ****** we should enter a frame on the conversation frame list, to handle
;;; success or failure returns from sub-dialogs. I.e. we should make a transition
;;; on the success or failure state of the main conversation.
;;; entry (_mc-failure _mc-success _mc-entry-state nil) ; no goal 

;;; Action:
;;;   =EXECUTE
;;;   - get ACTION if any from the dialog header
;;;   - reset conversation: HAS-MOSS-FACTS, HAS-MOSS-TASK, HAS-MOSS-TASK-LIST
;;;   - if ACTION IS non NIL, set conversation HAS-GOAL with a pattern obtaines from
;;;        sending ACTION a =create-pattern message
;;;     otherwise, reset conversation HAS-GOAL
;;;   - display welcome message
;;;
;;;   <Master enters text>
;;;
;;;   =RESUME
;;;   - if INPUT contains one of the abort commands, throws to :DIALOG-ERROR
;;;   - makes a transition to _MC-PROCESS

(defstate _mc-entry-state
  (:entry-state _main-conversation)
  (:label "Début du dialogue")
  (:explanation 
   "Initial state when the assistant starts a conversation. Send a welcome message ~
    and wait for data. Also entered on a restart following an abort.")
  (:reset-conversation)
  (:text "Attention! Ce dialogue est limité. Mon QI est trés faible...~2%")
  (:question-no-erase
   ("- Bonjour ! que puis-je faire pour vous ?"
     "- Salut ! Alors on fait quoi aujourd'hui ?" 
     "- Bonjour ! Je vais faire de mon mieux pour répondre à vos questions."))
  (:transitions 
   (:always :target _mc-process))
  )

;;;--------------------------------------------------------------------- (MC) ELIZA

(moss::defstate _mc-ELIZA
  (:label "ELIZA")
  (:explanation
   "Whenever MOSS cannot interpret what the user is saying, ELIZA is called to ~
    do some meaningless conversation to keep the user happy. It then record the ~
    master's input and transfers to find performative state.")
  (:eliza)
  (:transitions
   (:always :target _mc-find-performative))
  )

;;;------------------------------------------------------------------- (MC) FAILURE

(defstate _mc-failure
  (:label "Failure state of main conversation")
  (:explanation
   "Failure state is entered when we return from a sub-dialog with a :failure tag. ~
    We check for more tasks to perform (listed in the task-list slot of the ~
    conversation object). If there are more, we execute the first one. If there ~
    are no more, we return with a failure tag.")
  (:answer-analysis)
  )

(defownmethod
  =answer-analysis _mc-failure (conversation input)
  (declare (ignore input))
  (let* ((task-list (HAS-MOSS-TASK-LIST conversation))
         task)
    (if task-list
      (progn
        ;; record next task
        (setq task (pop task-list))
        ;; remove it from task-list
        (send conversation '=replace 'HAS-MOSS-TASK-LIST task-list)
        ;; add it to conversation
        (send conversation '=replace 'HAS-MOSS-TASK (list task))
        ;; transfer to sub-dialog
        `(:transition ,_mc-task-dialog))
      ;; when no more tasks, we go ask for another command or request
      `(:transition ,_mc-more?)
      )))

;;;--------------------------------------------------------- (MC) FIND-PERFORMATIVE

(defstate _mc-find-performative
  (:label "Find performative")
  (:explanation 
   "Process what the user said trying to determine the type of performative ~
    among :request :assert :command. Put the result if any into the performative ~
    slot of the conversation object.")
  (:transitions 
   ;; now really select performatives
   (:patterns (("qui" (?* ?x))
               ("quel" "est" (?* ?x))
               ("quelle" "est" (?* ?x))
               ("quels" "sont" (?* ?x))
               ("quelles" "sont" (?* ?x))
               ("qu" "est-ce" "que" (?* ?x))
               ("quoi" (?* ?x))
               ("quand" (?* ?x))
               ("où" (?* ?x))
               ("pourquoi" (?* ?x))
               ("qui" (?* ?x))
               ("combien" (?* ?x))
               ("comment" (?* ?x))
               ("est-ce" "que" (?* ?x))
               ("est-il" (?* ?x))
               ("est" "il" (?* ?x))    ; interface vocal ?
               ("est-elle" (?* ?x))
               ("est" "elle" (?* ?x))
               ("sont-ils" (?* ?x))
               ("sont" "ils" (?* ?x))
               ("sont-elles" (?* ?x))
               ("sont" "elles" (?* ?x))
               ("a-t-il" (?* ?x))
               ("a-t-elle" (?* ?x))
               ("ont-ils" (?* ?x))
               ("ont-elles" (?* ?x))
               ((?* ?x) "c" "est" "quoi" (?* ?y))
               ((?* ?x) "c" "est" "qui" (?* ?y))
               ((?* ?x) "?")
               )
              :set-performative (list :request)
              :target _mc-find-task)
   (:patterns (((?* ?x) "notez" (?* ?y))
               ((?* ?x) "noter" (?* ?y)))
              :set-performative (list :assert)
              :target _mc-find-task)
   (:otherwise 
    :set-performative (list :command)
    :target _mc-find-task))
  )

;;;----------------------------------------------------------------- (MC) FIND-TASK

(defstate _mc-find-task
  (:label "Find task")
  (:explanation 
   "Process the input to determine the task to be undertaken. Combines the words ~
    from the sentence to see if they are entry points for the index property of ~
    any task. Collect all tasks for which there is an index in the sentence.")
  (:answer-analysis)
  )

(defownmethod =answer-analysis _mc-find-task (conversation input)
  "Using input to find tasks If none failure, if one, OK, if more, must ask user to ~
   select one. Checks for an entry point for an index of a task.
Arguments:
   conversation: current conversation
   input: list containing the input as a list of words"
  (let* ((performative (read-fact conversation :PERFORMATIVE))
         task-list)
    (moss::vformat "_mc-find-task /performative: ~S, package: ~S, input:~%  ~S" 
                   performative *package* input)
    ;; try to find a task from the input text
    (setq task-list (moss::find-objects 
                     '(TASK 
                       (HAS-INDEX-PATTERN 
                        (TASK-INDEX (HAS-INDEX :is :?))))
                     input :all-objects t)) 
    (moss::vformat "_mc-find-task /possible tasks:~%  ~S" task-list)
    ;; filter tasks that do not have the right performative
    (setq task-list
          (mapcan #'(lambda (xx) 
                      (if (intersection performative (HAS-PERFORMATIVE xx))
                          (list xx)))
            task-list))
    (moss::vformat "_mc-find-task /task list after performative check: ~S" task-list)
    (cond 
     ;; if empty, ask ELIZA
     ((null task-list)
      `(:transition ,_mc-ELIZA))
     ;; if one, then OK
     ((null (cdr task-list))
      ;; save results, make the task the conversation task
      (setf (HAS-MOSS-TASK conversation) task-list)
      `(:transition ,_mc-task-dialog))
     (t 
      ;; otherwise must select one from the results
      (setf (HAS-MOSS-TASK-LIST conversation) task-list)
      `(:transition ,_mc-select-task)))
    ))

;;;----------------------------------------------------------------- (MC) GET-INPUT

(defstate _mc-get-input
  (:label "Get input")
  (:explanation
   "State in which the user inputs his request.")
  (:reset)
  (:question "- Je vous écoute....")
  (:transitions
   (:always :target _mc-process)))

;;;-------------------------------------------------------------------- (MC) MORE?
;;; Action:
;;;   - If answer is "rien", "non" transition to SLEEP
;;;   - if answer is "oui" transition to GET-INPUT
;;;   - anything else sends to PROCESS

(defstate _mc-more?
  (:label "More?")
  (:explanation "Asking the user it he wants to do more interaction.")
  (:reset-conversation)
  (:question-no-erase
   ("~%- Que puis-je faire d'autre pour vous ?"
    "~%- Y a-t-il autre chose que je puisse faire pour vous ?"
    "~%- Avez-vous d'autres questions ?"
    "~%- OK.")
   ) 
  (:transitions 
   (:starts-with ("rien") :target _mc-sleep)
   (:no :target _mc-sleep)
   (:yes :target _mc-get-input)
   (:otherwise :target _mc-process)))

;;;------------------------------------------------------------------- (MC) PROCESS
;;; we keep this useles state as a virtual entry point to processing a task

(defstate _mc-process
  (:label "Process state of main conversation")
  (:explanation
   "A virtual entry point to processing master's input.")
  (:transitions
   (:always :target _mc-find-performative)))

;;;--------------------------------------------------------------- (MC) SELECT-TASK

(defstate 
  _mc-select-task
  (:label "mc select task")
  (:explanation "we have located more than one task. We rank the tasks by computing ~
                 the average weight of the terms in the task index slot. We then ~
                 record the list of tasks into the statte-context task-list slot ~
                 and activate the highest ranking task.")
  (:answer-analysis) 
  )

(defownmethod =answer-analysis _mc-select-task (conversation input)
  "we have located more than one task. We rank the tasks by computing ~
   the MYCIN combination weight of the terms in the task index slot. We then ~
   record the list of tasks into the conversation task-list slot ~
   and activate the highest ranking task."
  ;(declare (ignore input))
  ;; the list of tasks is in the HAS-MOSS-TASK-LIST slot. Tasks are defined in the application
  ;; package
  (let* ((task-list (HAS-MOSS-TASK-LIST conversation))
         patterns weights result pair-list selected-task level word-weight-list)
    (moss::vformat "_select-task /input: ~S~&  task-list: ~S" input task-list)
    ;; first compute a list of patterns (combinations of words) from the input
    (setq patterns (mapcar #'car (moss::generate-access-patterns input)))
    (moss::vformat "_select-task /input: ~S~&  generated patterns:~&~S" 
                   input patterns)
    ;; then, for each task
    (dolist (task task-list)
      (setq level 0)
      ;; get the weight list
      ;(setq weights (HAS-INDEX-WEIGHTS task))
      (setq weights (moss::%get-INDEX-WEIGHTS task))
      (moss::vformat "_select-task /task: ~S~&  weights: ~S" task weights)
      ;; check the patterns according to the weight list
      (setq word-weight-list (moss::%get-relevant-weights weights patterns))  
      (moss::vformat "_select-task /word-weight-list:~&  ~S" word-weight-list)
      ;; combine the weights
      (dolist (item word-weight-list)
        (setq level (+ level (cadr item) (- (* level (cadr item))))))
      (moss::vformat "_select-task /level: ~S" level)
      ;; push the task and weight onto the result list
      (push (list task level) result)
      )
    (moss::vformat "_select-task /result:~&~S" result)
    ;; order the list
    (setq pair-list (sort result #'> :key #'cadr)) 
    (moss::vformat "_select-task /pair-list:~&  ~S" pair-list)
    ;; keep the first task whatever its score
    (setq selected-task (caar pair-list))
    ;; remove the task that have a weight less than task-threshold (default 0.4)
    (setq pair-list
          (remove nil 
                  (mapcar 
                   #'(lambda (xx) 
                       (if (>= (cadr xx) (omas::task-threshold omas::*omas*)) xx))
                   pair-list)))
    ;; if task-list is empty then return the first saved task
    ;; this may not be a good policy if the score is too low
    (if (null pair-list)
      (progn
        ;; reset the task-list slot of the conversation object
        (setf (HAS-MOSS-TASK-LIST conversation) nil)
        ;; put the saved task into the task slot
        (setf (HAS-MOSS-TASK conversation) (list selected-task))
        ;; go to task-dialog
        `(:transition ,_mc-task-dialog)
        )
      (progn
        ;; remove the weights
        (setq task-list (mapcar #'car pair-list))
        ;; select the first task of the list
        (setq selected-task (pop task-list))
        (moss::vformat "_select-task /selected task: ~S" selected-task)
        ;; save the popped list in the task-list slot of the conversation object
        (setf (HAS-MOSS-TASK-LIST conversation) task-list)
        (setf (HAS-MOSS-TASK conversation) (list selected-task))
        ;; go to task-dialog
        `(:transition ,_mc-task-dialog)))
    ))

;;;--------------------------------------------------------------------- (MC) SLEEP

(defstate _mc-SLEEP
  (:label "Nothing to do")
  (:explanation
   "User said she wanted nothing more.")
  (:reset)
  (:text "- OK. J'attends que vous soyez prêt.~%")
  (:question-no-erase 
   "- Réveillez moi en tapant ou en disant quelque chose...")
  (:transitions
   (:always :target _mc-process)))

;;;--------------------------------------------------------------- (MC) TASK-DIALOG

(defstate _mc-task-dialog
  (:label "Task dialog")
  (:explanation 
   "We found a task to execute (in the GOAL slot of the conversation). We activate ~
    the dialog associated with this task.")
  ;; we launch the task dialog as a sub-conversation
  ;; the task contains the name of a sub-conversation header, e.g. _get-tel-nb
  (:answer-analysis)
  )

;;;********** must check for possible problems
(defownmethod =answer-analysis _mc-task-dialog (conversation input)
  "We prepare the set up to launch the task dialog.
Arguments:
   conversation: current conversation"
  (declare (ignore input))
  (let* ((task-id (car (send conversation '=get 'HAS-MOSS-TASK))))
    (moss::vformat "_mc-task-dialog /task-id: ~S dialog: ~S" 
                   task-id (HAS-DIALOG task-id))
    ;; we launch the task dialog as a sub-conversation
    ;; the task contains the name of a sub-conversation header, e.g. _get-tel-nb
    `(:sub-dialog ,(car (HAS-DIALOG task-id)) :failure ,_mc-failure 
                  :success ,_mc-more?)
    ))


;;;================================================================================
;;;
;;;                             ELIZA CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to do small talk.


(defParameter *eliza-rules*
  '((("hello")
     "Bonjour ! Je m'appelle GASTON et je suis l'Assistant Personnel de JEAN-PAUL."
     "Hello there?")
    (("Bom" "dia")
     "Desculpe ! Não falo Portugues."
     "Bom dia. Por favor fale em frances.")
    (("Salut" *)
     "Salut ! Je m'appelle GASTON et je suis l'Assistant Personnel de JEAN-PAUL."
     "Salut ?")
    (("bonjour")
     "Bonjour ! Je m'appelle GASTON et je suis l'Assistant Personnel de JEAN-PAUL.")
    (("bonjour" "GASTON" *)
     "Bonjour, Boss !")
    (("hi")
     "Bonjour ! Je m'appelle Je m'appelle GASTON et je suis l'Assistant Personnel ~
      de JEAN-PAUL. Mon anglais n'est pas bon.")     
    (("comment" "allez-vous" (?* ?x))
     "Très bien merci."
     "Vraiment, ma santé vous intéresse ? J'en suis trés flatté.")
    (("qui"  * "Barthès")
     "Jean-Paul Barthès est professeur émérite d'intelligence artificielle à ~
      l'Université de Technologie de Compiègne.")
    (("qui"  * "Barthes")
     "Jean-Paul Barthès est professeur émérite d'intelligence artificielle à ~
      l'Université de Technologie de Compiègne.
      Notez toutefois que Barthès s'écrit avec un è.")
    (("laissez" "tomber")
     "OK. Pas de problème.")
    (("merci" *)
     "Avec plaisir.")
    (("tout" *)
     "Vraiment ?"
     "Quelle exigence !")
    ((* "machine" *)
     "Est-ce que les machines vous dérangent ?"
     "Que pensez-vous des machines ?"
     "Pourquoi mentionnez-vous les machines ?"
     "Les machines ont-elles un rapport avec votre problème ?")
    ((* "stupide" *)
     "S'il vous plaît, restez correct !"
     "Je suis peiné par votre remarque.")
    ((?x "ne" "marche" "pas" *)
     "Bien sûr que ?x marche !"
     "Pourquoi dites-vous que ?x ne marche pas ?")
    (("à" "l" "aide")
     "Vous voulez de l'aide à quel sujet?")
    (("help")
     "Vous voulez de l'aide à quel sujet?")
    ((* "désolé" *)
     "S'il vous plaît, ne vous excusez pas."
     "Des excuses ne sont pas nécessaires"
     "Que ressentez-vous quand vous vous excusez ?")
    ((* "Je" "me" "souviens" ?y)
     "Vous souvenez-vous souvent ?y ?"
     "Vous souvenir ?y vous rappelle-t-il autre chose ?"
     "De quoi d'autre vous souvenez-vous ?"
     "Pourquoi vous souvenez-vous ?y maintenant ?"
     "Qu'est-ce qui dans la situation présente vous fait vous souvenir ?y ?"
     "Quel est le lien entre moi et ?y ?")
    ((* "vous" "rappelez-vous" ?y)
     "Pensez-vous que j'aurais oublié ?y ?"
     "Pourquoi pensez-vous que je devrais me rappeler ?y maintenant ?"
     "Qu'y a-t-il à propos de ?y ?"
     "Vous avez mentionné ?y ?")
    ((* "si"  ?y)
     "Pensez-vous que ?y soit probable ?"
     "Souhaitez-vous ?y ?"
     "Que pensez-vous de ?y ?"
     "Vraiment -- si ?y ?")
    (("que" (?* ?x) "faire" "pour" "moi")
     "Tant de choses..."
     "A ce moment précis, je ne vois pas."
     "Qu'est-ce qui vous ferait plaisir ?")
    ((* "J" "ai" "rêvé"  ?y)
     "Vraiment -- ?y"
     "Avez-vous déjà rêvé ?y tout eveillé ?"
     "Avez-vous déjà ?y auparavant ?")
    ((* "rêve" *)
     "Qu'est-ce que ce rêve vous suggère ?"
     "Rêvez-vous souvent ?"
     "Quelles personnes apparaissent dans vos rêves ?"
     "Ne pênsez-vous pas que les rêves sont liés à votre problème ?")
    ((* "ma" "mère" *)
     "Qui d'autre dans votre famille ?y ?"
     "Parlez moi de votre famille.")
    ((* "mon" "père" *)
     "Votre père ?"
     "A-t-il une grande influence sur vous ?"
     "Qu'est-ce qui vous vient à l'esprit quand vous pensez à votre père ?")
    ((* "Je" "voudrais" ?y)
     "Que signifierait ?y ?"
     "Pourquoi voudriez-vous ?y ?")
    ((* "Je" "suis" "content" ?y)
     "Vous ai-je aidé à être content ?y ?"
     "Qu'est-ce qui vous rend heureux en ce moment ?"
     "Pouvez-vous m'expliquer pourquoi vous êtes tout à coup content ?y ?")
    ((* "Je" "déprime" *)
     "J'en suis fort désolé."
     "je suis sûr que ce n'est pas drôle d'être déprimé.")
    (( ?x "est" "comme" ?y)
     "Quelle resemblance voyez-vous entre ?x et ?y ?"
     "En quel sens ?x est comme ?y ?"
     "Pensez-vous qu'il y ait vraiment un lien ?"
     )
    ((* "pareils" *)
     "Comment ça ?"
     "Quelle resemblance y a-t-il ?")
    ((* "pareil" *)
     "Quel autre lien voyez-vous ?")
    
    ((* "J" "étais" ?y)
     "L'étiez-vous vraiment ?"
     "Je me doutais que vous étiez ?y"
     "Pourquoi me dites-vous que vous étiez ?y maintenant ?")
    ((* "Je" "suis" ?y)
     "En quel sens êtes-vous ?y ?"
     "Souhaitez vous être ?y ?")
    ((* "suis-je" ?y)
     "Croyez-vous être ?y ?"
     "Désirez-vous être ?y ?"
     "Qu'est-ce que cela ferait si vous étiez ?y ?")
    ((* "suis" *)
     "Pourquoi dites-vous \"SUIS\" ?"
     "Je ne comprends pas cela.")
    ((* "êtes-vous" ?y)
     "Pourquoi êtes-vous intéressé par savoir si je suis ?y ou pas ?"
     "Préfèreriez-vous que je ne sois pas ?y ?")
    ((* "vous" "êtes" ?y)
     "Qu'est-ce qui vous fait penser que je suis ?y ?")
    ((* "parce" "que" *)
     "Est-ce la seule raison ?"
     "Quelle autre raison pourrait-il y avoir ?"
     "Est-ce que cette raison peut expliquer autre chose ?")
    ((* "étiez-vous" ?y)
     "Peut-être étais-je ?y"
     "Qu'en pensez-vous ?"
     "Et si j'avais été ?y ?")
    ((* "je" "ne" "peux" "pas" ?y)
     "Peut-être pouvez-vous ?y maintenant ?"
     "Et si vous pouviez ?y ?")
    (((?* ?x) "Je" "ressents" (?* ?y))
     "Ressentez-vous ?y souvent ?")
    ((* "J" "ai" "ressenti" *)
     "Qu'avez-vous ressenti d'autre ?")
    ((* "pourquoi" "ne" "pas" ?y)
     "Ne devriez-vous pas ?y vous-même ?"
     "Pensez-vous que je pourrais ?y ?"
     "Peut-être pourrais-je ?y en temps utile")   
    ((* "oui" *)
     "Vous me semblez très positif."
     "Vos êtes sûr ?"
     "Je comprends.")
    ((* "non" *)
     "Pourquoi non ?"
     "Vous me semblez très négatif."
     "Est-ce que vous dites \"NON\" simplement pour être négatif ?")
    ((* "quelqu" "un" *)
     "Pouvez-vous être plus précis ?")
    ((* "tout" "le" "monde" *)
     "Sûrement pas tout le monde."
     "Pensez-vous à quelqu'un en particulier ?"
     "Qui par exemple ?"
     "Vous pensez à quelqu'un de précis ?")
    ((* "toujours" *)
     "Pouvez-vous donner un exemple précis ?"
     "Quand ?"
     "Toujours -- vraiment ?")
    ((* "peut-être" *)
     "Vous n'avez pas l'air tout à fait sûr.")
    ((* "est" ?y)
     "Vous pensez qu'il ne pourrait pas être ?y ?"
     "Il est peut-être ?y")
    ((* "OK" *)
     "Bon, comme vous voudrez.")
    ((* "zouave" *)
     "Bachibouzouk."
     "Moule à gaufre"
     "Restez poli, tonnerre de Brest!")
    ((* "connard" *)
     "Je me plaindrai à mon syndicat..."
     "C'est ça, c'est ça...")
    ((* "imbécile" *)
     "Oh!"
     "Crâne d'oeuf!"
     "Crétin des Alpes!")
    ((* "idiot" *)
     "Pas tant que ça..."
     "Pas plus que la moyenne."
     "Si vous le dites...")
    ((* "téléfon" *)
     "Oui, je sais : y a l'téléfon qui son..."
     "M'énerve l'téléfon."
     "C'est pas mon problème.")    
    
    ((?x)
     "Je ne vois pas ce que vous voulez exactement."
     "Je ne suis pas sûr de comprendre ce que vous voulez."
     "Pouvez-vous reformuler votre question s'il vous plaît ?"
     "Désolé, je n'ai pas compris."
     "Aïe ! Je n'ai pas compris."
     "Pouvez-vous dire cela différemment ?"
     "Pardon ?"
     "Quoi ?"
     "Comment ?"
     "J'ai entendu ?x , mais je ne comprends pas."
     )
    ))

(format t "~%;*** MOSS v~A - JEAN-PAUL dialogs loaded ***" moss::*moss-version-number*)

;;;=============================================================================== 

:EOF