﻿;;;-*- Mode: Lisp; Package: "XCCC" -*-
;;;===============================================================================
;;;12/09/17
;;;                           AGENT POSTMAN :XCCC
;;;
;;; Postman to transfer messages to remote OMAS platforms
;;;===============================================================================

;;; the postman is only valid for ACL
;;; Note that sites protected by a firewall should specify the GLOBAL IP in the 
;;; send message, NOT the IP of the physical machine that sends the message.
;;; This uses the new v.9.1 implementation of the postmen

#|
2010
 0501 Re-creation
 0720 transform into template
2012
 0917 upgrading to version 9.1
|#

(defpackage :XCCC (:use :moss :omas :cl :ccl))
(in-package :XCCC)

;;;===============================================================================
;;;                      Creating postman
;;;===============================================================================
 
;;; the following is an example for linking UTC current platform to CIT (Japan) and
;;; TECPAR (Brazil) using the external IPs*

(omas::defpostman :XCCC
    :server t
  :http t  ; creates also an HTTP server (not required)
  :site :UTC  ; needed
  :external-name "nat-omas.utc.fr"
  :external-ip "195.83.154.22"
  :known-postmen ((:CIT nil "219.166.183.59" :TCP :CIT)
                  (:TECPAR nil "200.183.132.15" :TCP :TECPAR))
  :proxy "webproxy.utc.fr:3128"
  )

;;;===============================================================================
;;;                                    skills 
;;;===============================================================================

;;; use default skills

:EOF