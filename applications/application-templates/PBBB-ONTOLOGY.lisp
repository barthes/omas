﻿;;;-*- Mode: Lisp; Package: "DOCTOR" -*-
;;;===============================================================================
;;;11/04/16
;;;             AGENT DOCTOR: ONTOLOGY (file DOCTOR-ontology.lisp)
;;;             Copyright Barthès@UTC, 2010
;;;
;;;=============================================================================== 

#|
DOCTOR is a French speaking assistant agent.
Its ontology is used to process dialogs, answer requests or trigger actions, 
i.e. send messages to other agents.
The current ontology contains nothing.
However, DOCTOR is not a specialist of those subjects and its ontology is 
rather shallow. It contains enough vocabulary to ask a specialist (one of 
the staff agent).

The ontology is defined using the PDM/MOSS formalism. However it is compat-
ible with SOL and this can produce OWL, HTML, JENA or SPARQL outputs.
Remember that if the agent is persistent, macros are not saved, global 
variables should be defined by using defvariable and functions by using
deffunction.
Once the persistent database is created, this file becomes useless.
|#
(in-package :DOCTOR)


(defontology 
  (:title "DOCTOR ontology")  ; currently ignored
  (:version "1.0")  ; currently ignored
  (:package "DOCTOR") ; currently ignored
  (:language :EN)  ; required for a PA
  )

  
;;;---------------------------------------------------------------------- CONCEPTS
#| 
It is recommended to organize concepts by alphabetical order, then provide
any necessary function (using deffunction), then provide methods

Example:

;;;-------------------------------------------------------------------------------
;;;                          ADRESSE POSTALE
;;;-------------------------------------------------------------------------------

(defconcept "Adresse Postale"
  (:is-a "adresse")
  (:doc "L'ADRESSE POSTALE est l'adresse où le courrier est distribué.")
  (:att "rue")
  (:att "ville" (:unique))
  (:att "pays" (:unique))
  (:att "code postal" (:unique)))
|#
;;;===============================================================================
;;;
;;;                               METHODS
;;;
;;;===============================================================================
  
;;;------------------------------------------------------------ (ADDRESS) =summary
#|
(definstmethod =summary ADDRESS ()
    ...)
|#

;;;===============================================================================
;;;=============================================================================== 
;;;
;;;                                INDIVIDUALS
;;;
;;;=============================================================================== 
;;;===============================================================================

#| Example:

(defindividual "adresse postale"
	("ville" "Compiègne")
	("rue" "25, rue de Paris")
	("code postal" "60200")
	)
|#

;;;=============================================================================== 

:EOF