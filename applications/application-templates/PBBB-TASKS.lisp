﻿;;;-*- Mode: Lisp; Package: "PBBB" -*-
;;;===============================================================================
;;;10/11/10
;;;             AGENT PBBB: TASKS (file PBBB-tasks.lisp)
;;;             Copyright Barthès@UTC, 2008-2010
;;;
;;;=============================================================================== 

#|
PBBB is a French speaking assistant agent.

Its ontology is used to process dialogs, answer requests or trigger actions, 
i.e. send messages to other agents.
The current file is its task library. It contains a model of all tasks the 
agent can execute, related to questions asked to PBBB. It can be used as a 
seed file for actual assistants.

A task can be coupled with a dialog used to fill the task pattern (T-CONTEXT). 
The mechanism is similar to the ACTION mechanism of the MOSS dialogs.

The ontology file should be loaded before the tasks file when loaded manually.

Note that the task language is defined with multilingual environment,
although the assistant is speaking French.

Four tasks are predefined and serve as examples:
   - HELP that will send a general message
   - SET FONTS to change the size of the font in the dialog window
   - TRACE that can be used for debugging dialogs
   - WHAT IS that will try to explain the meaning of some expression

2008
 0728 Creation
|#

(in-package :PBBB)

;;;=============================================================================== 
;;;   
;;;                               TASK MODEL
;;;
;;;===============================================================================

;;; The task model is used in programming mode (following an email request) or
;;; interaction mode through the assistant window.
;;; It is necessary to define the following concepts in the agent space.

(defconcept (:en "task index")
  (:doc :en "A TASK INDEX is a property of a task. The index attribute specifies ~
             the term being used, the weight attribute gives its importance for the ~
             task.")
  (:att (:en "index"))
  (:att (:en "weight"))
  )

(definstmethod =summary TASK-INDEX ()
  (list (has-index)(has-weight)))


(defconcept (:en "task")
  (:is-a moss::$QTSKE) ; used for linking inheritage ?
  (:doc :en "A TASK is a model of task to be performed by the assistant.")
  (:att "task name" (:unique)(:entry))
  (:att (:en "dialog"))
  (:att (:en "where to ask")) ; e.g. :ADDRESS
  (:att (:en "fail question"))
  (:rel (:en "index pattern")(:to "task index"))
  (:att "performative" (:id $PRFT)(:one-of :request :assert :command :answer))
  (:att (:en "message action")) ; e.g. :get-address
  )

(definstmethod =summary TASK ()
  (HAS-TASK-NAME))

;;;=============================================================================== 
;;;   
;;;                               TASK LIBRARY
;;;
;;;=============================================================================== 

;;;========================================================================== DATE

(deftask "help"
  :doc "Tâche (question) permettant d'imprimer une information générale."
  :dialog _print-date-conversation
  :indexes ("aujourd'hui" .3 "jour" .3 "date" .7 "est-on" .2 "sommes-nous" .2
            "quelle" .2 "heure" .4 "est-il" .2)
  )

;;;========================================================================== HELP 

(deftask "help"
  :doc "Tâche (question) permettant d'imprimer une information générale."
  :performative :command
  :dialog _print-help-conversation
  :indexes ("au secours" .5 "SOS" .6 "help" .7 "perdu" .4 "perdue" .4
            "que pouvez-vous faire" .4 "quels services" .4)
  )

(deftask "help"
  :doc "Tâche (ordre) permettant d'imprimer une information générale."
  :performative :request
  :dialog _print-help-conversation
  :indexes ("au secours" .5 "SOS" .6 "help" .7 "perdu" .4 "perdue" .4
            "que pouvez-vous faire" .4 "quels services" .4)
  )
  
;;;===================================================================== SET FONTS 

(deftask "font handler"
  :doc "Tâche permettant de modifier la taille des caractères sur l'écran."
  :performative :command
  :dialog _set-font-size-conversation
  :indexes ("police" .5 "texte" .3 "lire" .3 "lettres" .5 "écrire" .4 "gros" .2
            "grosse" .2)
  )
  
;;;========================================================================= TRACE 

(deftask "set trace"
  :doc "Tâche permettant d'activer ou de supprimer la trace."
  :performative :command
  :dialog _trace-conversation
  :indexes ("transition" .5 "transitions" .5 "trace" .5 "verbose" .5
            "bavard" .5)
  )

  ;;;======================================================================= WHAT-IS 

(deftask "what is"
  :doc "Tâche permettant d'obtenir des infos sur une expression."
  :dialog _what-is-conversation
  :indexes ("qu" .2 "est" .2 "qu est ce" .7 "c est quoi" .7 "qu est-ce" .7)
  )
  
;;;=============================================================================== 
	 
:EOF