﻿;;;===============================================================================
;;;10/07/21
;;;                               AGENT IA-DDD
;;;
;;;===============================================================================

;;; This file is a template for creating an experimental OMAS logical agent.
;;; A logical agent name starts with IA- and is defined by a set of rules
;;; the inference engine runs currently in forward chaining and has a crude 
;;; rule format

#|
2010
 0721 Creation
|#

;;;===============================================================================
;;; 
;;;                               RULES
;;;
;;;===============================================================================

#|
Examples of rules

(defrule R1 
  :if (("d" . ">5")) 
  :then ("moyen" . "voiture"))
  
(defrule R2 
  :if (("d" . ">1") ("t" ."<15")) ; logical AND
  :then ("moyen" . "voiture"))
|#
:EOF