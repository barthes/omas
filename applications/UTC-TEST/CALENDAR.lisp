﻿;;;-*- Mode: Lisp; Package: "CALENDAR" -*-
;;;==============================================================================
;;;20/02/11
;;;                               AGENT CALENDAR
;;;
;;;==============================================================================

;;; This file is a template for creating an OMAS agent.
;;; How to use it :
;;;   - replace CALENDAR with the name of your agent
;;;   - for each skill define the necessary functions, replacing XXX by the name
;;;     of the skill
;;;   - add one or more goals if needed, providing the functions and scripts
;;;   - delete whatever is not necessary.
;;; Skills and goals are commented out using a pair #| ... |# To make the code 
;;; visible to the comiler, one must remove the commenting pair.

;;;==============================================================================
;;; 
;;;                         defining agent package 
;;;
;;;==============================================================================
	 
(defpackage :CALENDAR (:use :moss :omas :cl))
(in-package :CALENDAR)

;;;==============================================================================
;;; 
;;;                              defining the agent 
;;;
;;;==============================================================================
;;; :CALENDAR is a keyword that will designat the agent and be used in the messages
;;; the actual name of the agent is built automatically and is the symbol
;;; CALENDAR::SA_CALENDAR, i.e. the symbol SA_CALENDAR defined in the "CALENDAR" package. It points
;;; to the lisp structure containing the agent data

(omas::defagent :CALENDAR)

;;;==============================================================================
;;; 
;;;                         service macros and functions 
;;; Place here the macros and finctions used by the skills and goals
;;;==============================================================================



;;; ================================ globals ====================================
;;; Globals  can be used within the agent space to simplify programming, although
;;; it is better to use the agent memory.


;;;==============================================================================
;;; 
;;;                                    SKILLS 
;;; put as many skill sections as there are skills to be defined
;;; organizing them by alphabetical order is more efficient
;;; remove the commenting marks #|  |# to make the code active
;;;==============================================================================
;;; 
;;; =============================================================================
;;;                                 Skill  GET-DATE
;;; =============================================================================

(defskill :GET-DATE :CALENDAR
  :static-fcn static-GET-DATE
  )
     
;;; Keep only those functions that are useful for your application
;;; agent and message are variables set by OMAS
;;; agent is the current agent
;;; message is the message that trigerred the function  

(defun static-get-date (agent message &rest ll)
  "return date and time"
  (declare (ignore ll))
  (let (hour minute second)
    (multiple-value-setq (second minute hour day month year) 
      (decode-universal-time (get-universal-time)))
    (static-exit agent 
	         (format nil "~S/~S/~S ~S:~S:~S" 
                         day month year hour minute second))))

;;;=============================================================================
;;; 
;;;                                    goals
;;;
;;;=============================================================================

;;; Define goals only if it is useful for your application

;;; ================================ goal section ==============================
;;; defgoal: macro that builds a goal structure for an agent.
;;;Arguments:
;;;   goal-name:              name of the goal that will be used to create task-ids
;;;   agent-name:             name of the agent to receive the goal
;;;keys arguments:
;;;   mode:                   activation mode (:rigid or :flexible)
;;;   type:                   type of goal (:permanent :cyclic :1-shot)
;;;   period:                 period for cyclic goals (default is 20s)
;;;   expiration-date:        date at which the goal dies
;;;   expiration-delay:       time to wait until we kill the goal
;;;   activation-date:        date at which the goal should fire (default is now)
;;;   activation-delay:       time to wait before activating the goal
;;;   status:                 waiting, active, dead,... 
;;;                              may not be useful with activation
;;;   goal-enable-fcn:        function checking the goal enabling conditions
;;;   script:                 script describing the goal, name of a function returning
;;;                              a list of messages
;;;Return:
;;;   goal-name.

#|
;;; defgoal must be called last since the goal takes effect immediately, thus the
;;; enable function must be defined
;;; return T to enable the goal NIL to inhibit it

(defun enable-ggg (agent script)
  "documentation"
  t)

(defun script-ggg (agent)
  "documentation"
  (declare (ignore agent))
 (list
   (make-instance 'omas::message :type :request :from :CALENDAR :to :CALENDAR
              :action <action>
              :args <arg-list>
              )))
			  
(defgoal :GGG :CALENDAR
  :mode   
  :type 
  :period 
  :expiration-date 
  :expiration-delay 
  :activation-date 
  :activation-delay 
  :status 
  :goal-enable-fcn enamge-ggg
  :script script-ggg
  )
|#

;;;==============================================================================
;;; 
;;;                               initial conditions 
;;;
;;;==============================================================================

;;; insert here whatever initial conditions need to be provided in the agent memory
;;; Ex. (deffact SA_book-seller-1 0 :money)

#|
(deffact SA_CALENDAR <any value> <key>)
|#


:EOF