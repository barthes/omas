﻿;;;-*- Mode: Lisp; Package: "PBBB" -*-
;;;===============================================================================
;;;20/02/11
;;;                               AGENT OSCAR
;;;
;;;===============================================================================

;;; This file is a template for creating an OMAS personal assistant.
;;; How to use it :
;;;   - replace OSCAR with the name of your agent
;;;   - for each skill define the necessary functions, replacing XXX by the name
;;;     of the skill
;;;   - add one or more goals if needed, providing the functions and scripts
;;;   - delete whatever is not necessary.
;;; Skills and goals are commented out using a pair #| ... |# To make the code 
;;; visible to the compiler, one must remove the commenting marks.

;;;===============================================================================
;;; 
;;;                         defining agent package 
;;;
;;;===============================================================================

(defpackage :OSCAR (:use :moss :omas :cl))
(in-package :OSCAR)

;;;===============================================================================
;;; 
;;;                              defining the agent 
;;;
;;;===============================================================================
;;; :OSCAR is a keyword that will designat the agent and be used in the messages
;;; the actual name of the agent is built automatically and is the symbol
;;; OSCAR::SA_OSCAR, i.e. the symbol SA_OSCAR defined in the "OSCAR" package. It points
;;; to the lisp structure containing the agent data

(omas::defassistant :OSCAR :redefine t :language :fr)

;;;===============================================================================
;;; 
;;;                         service macros and functions 
;;; Place here the macros and finctions used by the skills and goals
;;;===============================================================================



;;; ================================= globals ====================================
;;; Globals  can be used within the agent space to simplify programming, although
;;; it is better to use the agent memory.


;;;===============================================================================
;;; 
;;;                                    SKILLS 
;;; put as many skill sections as there are skills to be defined
;;; organizing them by alphabetical order is more efficient
;;; remove the commenting marks #|  |# to make the code active
;;;===============================================================================
;;; 
;;; ==============================================================================
;;;                                 Skill  XXX
;;; ==============================================================================
#|
(defskill :XXX :OSCAR
  :static-fcn static-XXX
  :dynamic-fcn dynamic-XXX
  :acknowledge-fcn acknowledge-XXX
  :bid-cost-fcn bid-cost-XXX
  :bid-quality-fcn bid-quality-XXX
  :bid-start-time-fcn bid-start-time-XXX
  :how-long-fcn how-long-XXX
  :preconditions preconditions-XXX
  :select-best-answer-fcn select-best-answer-XXX
  :time-limit-fcn time-limit-XXX
  :timeout-handler timeout-handler-XXX
  )
     
;;; Keep only those functions that are useful for your application
;;; agent and message are variables set by OMAS
;;; agent is the current agent
;;; message is the message that trigerred the function  

(defun static-xxx (agent message <args>)
  "documentation"
  (static-exit agent <result>))

(defun dynamic-xxx (agent message <args>)
  "documentation"
  (dynamic-exit agent <result>))

(defun acknnowledge-xxx (agent message <args>)
    "documentation"
  )

;;; <arg-list> is the list of args contained in the call-for-bids message, i.e. 
;;; the args corresponding to the task to be done

(defun bid-cost-fcn bid-cost-xxx (agent <arg-list>)
    "documentation"
  <return a list (<cost> <rigidity>)>)

(defun bid-quality-xxx (agent <arg-list>)
    "documentation"
  )

(defun bid-start-time-xxx (agent <arg-list>)
    "documentation"
  )

(defun how-long-xxx (agent <arg-list>)
    "documentation"
  )
  
(defun preconditions-xxx (agent message <args>)
    "documentation"
  )

(defun select-best-answer-xxx (agent answer-message-list)
    "documentation"
  )
  
(defun time-limit-xxx (agent message message)
    "documentation"
  )

(defun timeout-handler-xxx (agent message)
    "documentation"
  )
|#
;;; ==============================================================================
;;;                                 Skill  GET-INFO
;;; ==============================================================================

(defskill :GET-INFO :OSCAR
  :static-fcn static-get-info
  :dynamic-fcn dynamic-get-info
  :timeout-handler timeout-handler-get-info
  )
     
(defun static-get-info (agent message args)
  "will send a broadcast to get info"
  (declare (ignore message))
  (omas::dformat-set :send 2)
  (send-subtask 
   agent :type :request :to :ALL :from :OSCAR :action :get-date :args (list args)
   :strategy :collect-answers :timeout 5)
  (static-exit agent :done))

(defun dynamic-get-info (agent message data)
  "documentation"
  (declare (ignore message))
  (print `("***** dynamic-get-info data:" ,data))
  (dynamic-exit agent data))

(defun timeout-handler-get-info (agent message)
  (print `("***** timeout-handler-get-info agent and message" ,agent ,message)))
  

;;;==============================================================================
;;; 
;;;                                    goals
;;;
;;;==============================================================================

;;; Define goals only if it is useful for your application

;;; ================================= goal section ==============================
;;; defgoal: macro that builds a goal structure for an agent.
;;;Arguments:
;;;   goal-name:              name of the goal that will be used to create task-ids
;;;   agent-name:             name of the agent to receive the goal
;;;keys arguments:
;;;   mode:                   activation mode (:rigid or :flexible)
;;;   type:                   type of goal (:permanent :cyclic :1-shot)
;;;   period:                 period for cyclic goals (default is 20s)
;;;   expiration-date:        date at which the goal dies
;;;   expiration-delay:       time to wait until we kill the goal
;;;   activation-date:        date at which the goal should fire (default is now)
;;;   activation-delay:       time to wait before activating the goal
;;;   status:                 waiting, active, dead,... 
;;;                              may not be useful with activation
;;;   goal-enable-fcn:        function checking the goal enabling conditions
;;;   script:                 script describing the goal, name of a function returning
;;;                              a list of messages
;;;Return:
;;;   goal-name.

#|
;;; defgoal must be called last since the goal takes effect immediately, thus the
;;; enable function must be defined
;;; return T to enable the goal NIL to inhibit it

(defun enable-ggg (agent script)
  "documentation"
  t)

(defun script-ggg (agent)
  "documentation"
  (declare (ignore agent))
 (list
   (make-instance 'omas::message :type :request :from :OSCAR :to :OSCAR
              :action <action>
              :args <arg-list>
              )))
			  
(defgoal :GGG :OSCAR
  :mode   
  :type 
  :period 
  :expiration-date 
  :expiration-delay 
  :activation-date 
  :activation-delay 
  :status 
  :goal-enable-fcn enamge-ggg
  :script script-ggg
  )
|#

;;;===============================================================================
;;; 
;;;                               initial conditions 
;;;
;;;===============================================================================

;;; insert here whatever initial conditions need to be provided in the agent space
;;; Ex. (deffact PA_albert 0 :worry)

#|
(deffact PA_OSCAR <any value> <key>)
|#


:EOF