;;;===============================================================================
;;;<date>
;;;                 PREDEFINED MESSAGES (file: Z-messages.lisp)
;;;
;;;===============================================================================

;;; loaded in the package of the loading environment (usually OMAS)

(in-package :omas)

;;; insert messages you want to load with the application, e.g.
;;;    (defmessage :DF-0 :to :UTC-FAC-1 :type :request :action :dumb-fac :args (5))

(defmessage :TFA-no-TO :to :all :type :request :action :get-info 
  :args ("qui est Einstein?"))

:EOF